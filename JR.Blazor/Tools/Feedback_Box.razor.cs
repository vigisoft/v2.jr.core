﻿using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Logging;
using MudBlazor;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JR.Blazor.Tools
{
    public partial class Feedback_Box : ComponentBase
    {

        [CascadingParameter] MudDialogInstance MudDialog { get; set; }

        string Current_Message = "";
        int Current_Step = 0;
        int Max_Step = 100;

        public void Set_Message(string Message, int Step, int Max)
        {
            Current_Message = Message;
            Current_Step = Step;
            Max_Step = Max;
            StateHasChanged();
        }

        string Step_Text => Current_Step == 0 ? "" : $"[ {Current_Step} / {Max_Step} ]";
        public void Close() => MudDialog.Cancel();
        void Submit() => MudDialog.Close(DialogResult.Ok(true));
        void Cancel() => MudDialog.Cancel();

        protected override void OnInitialized()
        {
            MudDialog.SetOptions(new DialogOptions { MaxWidth = MaxWidth.Large, CloseButton = false, BackdropClick = false});
        }

    }
}
