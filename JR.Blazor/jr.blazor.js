﻿function force_download(Url) {
    window.open(Url, '_self');
}

function Navigate_To(Url) {
    window.location.href = Url;
}

var Global_Ref = '';
// Intercepte des touches globales (Escape par défaut) et appelle une méthode sur la vue principale
function Global_KeyDown() {
    switch (event.key) {
        case 'Escape':
            break;
        default: return;
    }
    var Key = event.key;    // Ne pas passer l'event au timer, car il n'existe plus au moment de l'appel
    setTimeout(function () { Global_Ref.invokeMethodAsync('JS_Keydown', Key); }, 1);
}

// Appelle une méthode sur touche enter
// Ne fonctionne pas avec un blazored modal !! : le texte n'est pas enregistré et le close devient inopérent 
function Global_Enter(Method) {
    switch (event.key) {
        case 'Enter':
            break;
        default: return;
    }
    setTimeout(function () { Global_Ref.invokeMethodAsync('JS_Enter', Method); }, 1); // Petit timer pour laisser passer la mise à jour des valeurs (onchange)
}

// Force le click sur un bouton si enter
function Enter_Click(Button) {
    switch (event.key) {
        case 'Enter':
            break;
        default: return;
    }
    setTimeout(function () { $('#' + Button).click(); }, 1); // Petit timer pour laisser passer la mise à jour des valeurs (onchange)
}

function Store_Global_Ref(Ref) {
    Global_Ref = Ref;
}

function Set_Focus_On(Element) {
    document.getElementById(Element).focus()
}


function Cookie_Set(Key, Value, Days) {
    var d = new Date();
    d.setTime(d.getTime() + (Days * 24 * 60 * 60 * 1000));
    document.cookie = Key + "=" + Value + ";expires=" + d.toUTCString() + ";path=/";
}

function Cookie_Del(Key) {
    document.cookie = Key + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
}

function Eval_Xhr(xhr) {
    eval(xhr.responseText);
}

function Show_Modal(Element) {
    $('#' + Element).modal('show');
}
function Hide_Modal(Element) {
    $('#' + Element).modal('hide');
}

function Toastr_Ok(Reason) {
    toastr.success(Reason, "", { preventDuplicates: true, timeOut: 10000 });
}
function Toastr_Warning(Reason) {
    toastr.warning(Reason, "", { preventDuplicates: true, timeOut: 10000 });
}
function Toastr_Error(Reason) {
    toastr.error(Reason, "", { preventDuplicates: true, timeOut: 10000 });
}

function Ping_Blazor() {
    Global_Ref.invokeMethodAsync('JS_Ping', 'hello');
}

function Display_Blazor_Warning() {

    $.get("/blazor_warning", { user: User_Id, url: document.location.href, lang: User_Lang, agent: window.navigator.userAgent }, function (data) {
        $("#Warning_Div").html(data);
    });
}

function Run_Blazor_Check() {
    setTimeout(function () {
        if (Global_Ref === '') {
            Display_Blazor_Warning();
        } else {
            setInterval(Ping_Blazor, 10 * 60 * 1000);
        }
    }, 10000);
}

function Download_Bytes(filename, bytesBase64, mimeType) {
    var link = document.createElement('a');
    link.download = filename;
    link.href = "data:" + mimeType + ";base64," + bytesBase64;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
