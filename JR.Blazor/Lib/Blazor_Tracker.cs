﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;

namespace JR.Blazor
{

    public class Component_Tracker
    {
        private readonly List<IComponent> Components = new();

        void Trace (IComponent c, bool Is_Remove)
        {
            var Name = "?";
            var BL = c as Blazor_Layout;
            if (BL != null) Name = BL.Name + BL.Uid;
            var BM = c as Blazor_Module;
            if (BM != null) Name = BM.Name + BM.Uid;
            Console.WriteLine($"{(Is_Remove ? "comp remove":"comp add")} {c.GetType().Name} {Name}");
        }
        public void Register(IComponent component)
        {
            if (!Components.Contains(component))
            {
                Components.Add(component);
                //Trace(component, false);
            }
        }

        public void Unregister(IComponent component)
        {
            if (Components.Contains(component))
            {
                Components.Remove(component);
                //Trace(component, true);
            }
        }

        public void Loop_Components(Func<IComponent, bool> On_Component)
        {
            foreach (var C in Components)
            {
                if (On_Component(C)) return;
            }
        }

    }


}
