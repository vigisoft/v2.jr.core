﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Http;
using Microsoft.JSInterop;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace JR.Blazor
{
    public partial class Blazor_Module : ComponentBase, IDisposable, I_Component
    {

        public Blazor_View V { get; set; } = null;

        [Parameter]
        public string Name { get; set; } = "";
        public string Uid { get; set; } = "";

        public void Trace(string Mess)
        {
            Debug.WriteLine($"{DateTime.Now:HH:mm:ss.fff}:M_{Uid} : {Mess}");
        }
        public virtual IJSRuntime JS { get; set; }
        public virtual Blazor_Session WS { get; set; }
        public virtual NavigationManager NM { get; set; }

        [Inject]
        public Component_Tracker TrackerService { get; set; }

        [Inject] public IHttpContextAccessor Http_Context { get; set; }

        public Blazor_User WU => WS.User;

        public bool Is_Prerender => !Http_Context.HttpContext.Response.HasStarted;
        protected override void OnInitialized()
        {
            TrackerService.Register(this);
            V = Http_Context.HttpContext.Items["View"] as Blazor_View;
            if (V != null)
            {
                V.Register(this);
                JS = V.JS;
                WS = V.WS;
                NM = V.NM;
            }
            else
            {
                Uid = $"{GetType().Name}_{Name}";
            }

            //Trace("OnInitialized");
            On_Init();
        }

        public virtual Task On_Init_Async()
        {
            return Task.CompletedTask;
        }
        protected override Task OnInitializedAsync()
        {
            //Trace("OnInitializedAsync");
            return On_Init_Async();
        }

        public virtual void On_Init()
        {
        }

        protected override void OnParametersSet()
        {
            //Trace("OnParametersSet");
            On_Load();
        }

        public virtual void On_Load()
        {
        }

        public virtual void On_Render(bool First_Pass)
        {
        }
        public virtual Task On_Render_Async(bool First_Pass)
        {
            return Task.CompletedTask;
        }

        protected string Focus = null;
        public void Focus_On(string Element) => Focus = Element;

        public virtual bool Should_Render()
        {
            return false;
        }

        public virtual void On_Global_Key(string Key)
        {
        }

        protected override void OnAfterRender(bool firstRender)
        {
            //Trace($"OnAfterRender[{firstRender}]");
            On_Render(firstRender);
            V?.Check_Focus(ref Focus);

        }

        protected override Task OnAfterRenderAsync(bool firstRender)
        {
            return On_Render_Async(firstRender);
        }

        protected override bool ShouldRender()
        {
            //Trace($"ShouldRender?");
            return base.ShouldRender() || Should_Render();
        }

        public virtual void Raise(string Event, object Context, bool Me_Too = false)
        {
            if (V != null)
            {
                V.On_Event(Event, Context);
                foreach (var M in V.Modules)
                {
                    if (M == this) continue;
                    M.On_Event(Event, Context);
                }
            }
            if (Me_Too) On_Event(Event, Context);
        }

        public long Version = 1;
        public void Force_Update(bool Async = false)
        {
            Version++;
            if (Async)
            {
                InvokeAsync(() => StateHasChanged());
            }
            else
            {
                StateHasChanged();
            }
        }

        public virtual void On_Event(string Event, object Context)
        {
        }

        public virtual void On_Dispose()
        {
        }

        public virtual void Dispose()
        {
            //Trace("Dispose");
            On_Dispose();
            TrackerService.Unregister(this);
            if (V != null) V.Unregister(this);
        }

        public string LNG => V?.LNG ?? "FR";
        public bool French => LNG == "FR";

        //public int QY_Count => View?.Args.Count ?? 0;

        //public string QY(string Key, string Default = "") => View?.Arg(Key, Default);

        public string URL(string Page, bool Canonical = false) => Blazor_Application.URL(LNG, Page, Canonical);

        string Expanded_Key(string Key)
        {
            if (Key.Length < 2) return Key;
            switch (Key[0])
            {
                case '.': return string.Concat(V?.Name ?? Name, Key);
            }
            return Key;
        }

        public string Def_Ui(string Key, string Default_Value = "") => Blazor_Application.Local_Ui(Expanded_Key(Key), LNG, Default_Value);

        public string Def_NUi(string Key, string Default_Value = "") => Blazor_Application.Local_Ui(Expanded_Key(Key), null, Default_Value);

        public string Ui(string Key, params object[] Params)
        {
            if (Params.Length > 0)
            {
                return string.Format(Blazor_Application.Local_Ui(Expanded_Key(Key), LNG), Params);
            }
            return Blazor_Application.Local_Ui(Expanded_Key(Key), LNG);
        }

        public string NUi(string Key, params object[] Params)
        {
            if (Params.Length > 0)
            {
                return string.Format(Blazor_Application.Local_Ui(Expanded_Key(Key)), Params);
            }
            return Blazor_Application.Local_Ui(Expanded_Key(Key));
        }

        public MarkupString AutoRaw(string Html) => (MarkupString)Html.To_Html_Auto();
        public MarkupString Raw(string Html) => (MarkupString)Html;

        public void Remove_Cookie(string Name, bool User_Level = false) => WS.Remove_Cookie(Name, User_Level);

        public void Persist_Cookie(string Name, string Value, bool User_Level = false, int Days = 30000) => WS.Persist_Cookie(Name, Value, User_Level, Days);

        public string Get_Cookie(string Name, bool User_Level = false) => WS.Get_Cookie(Name, User_Level);

        public void Flush_Cookies() => WS.Treat_Pending_Cookies((Key, Value, Days) => JS.InvokeVoidAsync("Cookie_Set", Key, Value, Days), (Key) => JS.InvokeVoidAsync("Cookie_Del", Key));


        public void Loop_Components(Func<IComponent, bool> On_Component) => TrackerService?.Loop_Components(On_Component);
    }
}
