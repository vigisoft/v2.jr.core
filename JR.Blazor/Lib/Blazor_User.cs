﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JR.Blazor
{
    public class Blazor_User
    {
        public Blazor_User(Blazor_Session Session)
        {
            this.Session = Session;
        }

        public Blazor_Session Session { get; } = null;
        public bool Connected { get; set; } = false;
        public int Level { get; set; } = 0;

        public object Context = null;
        public virtual void Auto_Connect() { }
        public void Connect(string Name, string Uid, int Level = 1, object Context = null)
        {
            this.Name = Name;
            this.Uid = Uid;
            Connected = true;
            this.Level = Level;
            this.Context = Context;
            Session.Change_User();
            On_Connect();
        }

        public virtual void On_Connect()
        {
        }

        public virtual void On_Disconnect()
        {
        }

        public void Disconnect()
        {
            Name = "?";
            Uid = "";
            Connected = false;
            Level = 0;
            Context = null;
            On_Disconnect();
            Session.Change_User();
        }
        public string Name { get; private set; } = "?";
        public string Uid { get; private set; } = "";

    }
}
