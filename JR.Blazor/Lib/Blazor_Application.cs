﻿using JR.Saf;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace JR.Blazor
{
    public class Blazor_Application
    {

        public static bool Send_Mail(string To, string From, string Sender, string Subject, string Body, params string[] Attachments)
        {
            try
            {
                using (var Mail = new MailMessage())
                {
                    Mail.Subject = Subject;
                    Mail.Body = Body;
                    if (Body.StartsWith("<") && Body.EndsWith(">"))
                    {
                        Mail.IsBodyHtml = true;
                        Mail.BodyEncoding = System.Text.Encoding.Default;
                    }
                    else
                    {
                        Mail.IsBodyHtml = false;
                        Mail.BodyEncoding = System.Text.Encoding.Default;
                    }
                    Mail.To.Add(To);
                    Mail.From = new MailAddress(From);
                    if (Sender != null)
                    {
                        Mail.Sender = new MailAddress(Sender);
                    }
                    else
                    {
                        Mail.Sender = Mail.From;
                    }
                    foreach (string File in Attachments)
                    {
                        Mail.Attachments.Add(new Attachment(File));
                    }
                    return Blazor_Application.Send(Mail);
                }
            }
            catch
            {
                return false;
            }
        }

        public static Ref_Parameter SMTP_Server = new Ref_Parameter("SMTP.Server");

        // Envoi d'un mail avec un flag qui indique en retour si ca s'est bien passé
        public static bool Send(System.Net.Mail.MailMessage Mail)
        {
            try
            {
                string Mail_Redirection = JR.Saf.Main.Parameter("Mail_Redirection");
                if (Mail_Redirection != "")
                {
                    MailAddress Addr = new MailAddress(Mail_Redirection, Mail.To.ToString().Replace("@", "_AT_"));
                    Mail.To.Clear();
                    Mail.To.Add(Addr);
                }
                using (var Client = new System.Net.Mail.SmtpClient(SMTP_Server.Value))
                {
                    var Client_Port = Main.Int_Parameter("SMTP.port");
                    var User_Name = Main.Parameter("SMTP.username");
                    if (Client_Port != 0) Client.Port = Client_Port;
                    if (User_Name != "")
                    {
                        Client.UseDefaultCredentials = false;
                        var Cred = new NetworkCredential(User_Name, Main.Parameter("SMTP.password"));
                        Client.Credentials = Cred;
                    }
                    var Use_SSL = Main.Bool_Parameter("SMTP.usessl");
                    if (Use_SSL) Client.EnableSsl = true;
                    Client.Send(Mail);
                }
                return true;
            }
            catch
            {
            }
            return false;

        }

        public class Dic_File
        {
            DateTime Date = DateTime.MinValue;
            string File_Name;
            public void Check()
            {
                var D = JR.Files.Safe_Date(File_Name);
                if (Date == D) return;
                JR.Saf.Main.Dico.Load_Xls_Dictionary(File_Name);
                Date = D;
            }
            public Dic_File(string File)
            {
                File_Name = JR.Saf.Main.Real_File_Name(File);
                Check();
            }
        }
        static List<Dic_File> Dics = new List<Dic_File>();

        public static void Register_Dic(string File_Name)
        {
            Dics.Add(new Dic_File(File_Name));
        }

        public static void Reload_Dics()
        {
            lock (Dics)
            {
                foreach (var D in Dics)
                {
                    D.Check();
                }
            }
        }

        public static DateTime Reconf_Version = DateTime.Now;

        public static bool Check_Reconf(ref DateTime Current_Version, Action Check)
        {
            if (Current_Version == Reconf_Version) return false;
            Check();
            Current_Version = Reconf_Version;
            return true;
        }

        static Dictionary<string, Action> Clearables = new Dictionary<string, Action>();
        public static void Register_Clearable(string Key, Action A) => Clearables[Key] = A;
        public static void Reconf(string Dependencies = "")
        {
            lock (Dics)
            {
                if (Dependencies == "")
                {
                    Reconf_Version = DateTime.Now;
                    Reload_Dics();
                    foreach (var A in Clearables) A.Value();
                }
                else
                {
                    if (Clearables.TryGetValue(Dependencies, out Action A)) A();
                }
            }
        }
        public static void Restart_Web_Server(bool Hard_Reboot = false)
        {
            if (Hard_Reboot) JR.OS.Kill_Program();
            if (Web_App == null)
            {
                JR.Files.Force_Date("web.config", DateTime.UtcNow);
            }
            else
            {
                Web_App.Lifetime.StopApplication();
            }
        }

        static Dictionary<string, string> Dic_Routes = new Dictionary<string, string>();
        protected static void Declare_Route_Translation(string Lang, string Route, string New_Route)
        {
            Route = Route.Scan().Next("/?");
            New_Route = New_Route.Scan().Next("/?");
            var Key = Lang + Route;
            Dic_Routes[Key] = New_Route;
            Dic_Routes[New_Route] = Route;
        }
        public static string Translate_Route(string Lang, string Route)
        {
            if (Dic_Routes.Count == 0) return Route;
            var C = Route.Scan();
            var Key = Lang + C.Next("/?");
            var Sep = C.Separator;
            string New_Page;
            if (!Dic_Routes.TryGetValue(Key, out New_Page)) return Route;
            var Args = C.Tail();
            if (Args.Length > 0) return New_Page + Sep + Args;
            return New_Page;
        }

        public static Ref_Parameter Can_URL = new Ref_Parameter("Canonical_URL");
        public static string Canonical_URL => Can_URL.Value;

        public static string Get_Official_Verb(string URL_Path)
        {
            var P = URL_Path.ToLower().Split('/');
            if (P.Length < 2) return null;
            if (P.Length == 2) return "";
            if (!Dic_Routes.TryGetValue(P[2], out string R)) return P[2];
            return R;
        }

        public static string URL(string Lang, string Page, bool Canonical = false)
        {
            var SB = new StringBuilder();
            if (Canonical) SB.Append(Canonical_URL);
            SB.Append("/");
            SB.Append(Lang.ToLower());
            if (Page.Length > 1)    // permet de gérer "" ou "~"
            {
                SB.Append("/");
                SB.Append(Translate_Route(Lang, Page));
            }
            return SB.ToString().ToLower();
        }

        public static string Local_Ui(string Key, string Language = null, string Default_Value = null)
        {
            if (Default_Value == null)
            {
                return Main.Get_String(Language, "UI", Key);
            }
            if (!Main.Try_Get_String(out string Val, Language, "UI", Key)) return Default_Value;
            return Val;
        }

        static WebApplication Web_App = null;

        public delegate void App_Event();
        public static App_Event App_Stopped;

        public static void Init_App(WebApplication App)
        {
            Web_App = App;
            App.Lifetime.ApplicationStopped.Register(() => { if (App_Stopped != null) App_Stopped(); });
        }

        public static void Init_Services(IServiceCollection services)
        {
            services.AddScoped<Component_Tracker>();
            services.AddDistributedMemoryCache();   // Requis pour stocker les états de session
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromHours(24);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });
            services.AddRazorComponents()
                           .AddInteractiveServerComponents();
            services.AddRazorPages();
            services.AddServerSideBlazor(o => o.DetailedErrors = true).AddHubOptions(options =>
            {
                //options.ClientTimeoutInterval = TimeSpan.FromSeconds(60);
                //options.EnableDetailedErrors = true;
                //options.HandshakeTimeout = TimeSpan.FromSeconds(10);
                //options.KeepAliveInterval = TimeSpan.FromSeconds(10);
                //options.MaximumParallelInvocationsPerClient = 1;
                options.MaximumReceiveMessageSize = 10 * 1024 * 1024;   // Pour pouvoir recevoir des gros textes
                //options.StreamBufferCapacity = 10;
            });
            services.AddHttpContextAccessor();
        }
    }
}
