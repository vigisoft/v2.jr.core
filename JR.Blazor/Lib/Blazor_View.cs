﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;
using JR;
using Microsoft.JSInterop;
using System.Reflection;
using System.Dynamic;
using Microsoft.AspNetCore.WebUtilities;

namespace JR.Blazor
{
    public class Blazor_View : Blazor_Module
    {
        static long Id_Gen = 0;
        long M_Id_Gen = 0;

        public HashSet<I_Component> Modules = new HashSet<I_Component>();

        public void Trace(I_Component M, string Mess) => Debug.WriteLine($"{DateTime.Now:HH:mm:ss.fff}:{Uid}:{M.Uid} : {Mess}");

        public virtual bool Disconnected_Redir => false;
        protected override void OnInitialized()
        {
            TrackerService.Register(this);
            Http_Context.HttpContext.Items["View"] = this;
            Dotnet_Ref = DotNetObjectReference.Create(this);
            if ((Uid??"") != "")
            {
                Trace("!!! Already initialized!!!");
                return;
            }
            Id_Gen++;
            Uid = $"V_{this.GetType().Name}@{Id_Gen}";
            //Trace("OnInitialized");
            On_Init();

            var L = Http_Context.HttpContext.Items["Layout"] as Blazor_Layout;
            L?.On_View_Init(this);

        }

        public new string LNG = "FR";
        public override Task On_Init_Async()
        {
            //Trace("OnInitializedAsync");
            return Task.CompletedTask;
        }

        [Inject] public override IJSRuntime JS { get; set; }
        [Inject] public override Blazor_Session WS { get; set; }
        [Inject] public override NavigationManager NM { get; set; }

        protected override void OnParametersSet()
        {
            //Trace("OnParametersSet");
            JS.InvokeVoidAsync("Store_Global_Ref", Dotnet_Ref);
            On_Load();
            var L = Http_Context.HttpContext.Items["Layout"] as Blazor_Layout;
            L?.On_View_Load(this);
        }

        protected override void OnAfterRender(bool firstRender)
        {
            //Trace($"OnAfterRender[{firstRender}]");
            On_Render(firstRender);
            Check_Focus(ref Focus);
        }

        public void Check_Focus(ref string Focus)
        {
            if (Focus == null) return;
            JS.InvokeVoidAsync("Set_Focus_On", Focus);
            Focus = null;
        }

        protected override bool ShouldRender()
        {
            //Trace($"ShouldRender");
            return base.ShouldRender() || Should_Render();
        }

        public override void Raise(string Event, object Context, bool Me_Too = false)
        {
            V?.On_Event(Event, Context);
            foreach (var M in Modules)
            {
                if (M == this) continue;
                M.On_Event(Event, Context);
            }
            if (Me_Too) On_Event(Event, Context);
        }

        private IDisposable Dotnet_Ref;

        public override void Dispose()
        {
            Dotnet_Ref?.Dispose();
            Dotnet_Ref = null;
            base.Dispose();
        }

        [JSInvokable]
        public string JS_Ping(string Key)
        {
            return Key;
        }

        [JSInvokable]
        public void JS_Keydown(string Key)
        {
            On_Global_Key(Key);
            foreach (var M in Modules) M.On_Global_Key(Key);
        }

        [JSInvokable]
        public void JS_Enter(string Method)
        {
            // Chercher une méthode candidate dans la vue et dans les modules
            bool Invoke(object O)
            {
                var M = O.GetType().GetMethod(Method);
                if (M == null) return false;
                M.Invoke(O, null);
                return true;
            }
            if (Invoke(this)) return;
            foreach (var M in Modules)
            {
                if (Invoke(M)) return;
            }
        }

        public void Register(I_Component M)
        {
            M.V = V ?? this;
            if (Modules.Contains(M))
            {
                Trace(M, "Module already registered");
                return;
            }
            Modules.Add(M);
            M_Id_Gen++;
            M.Uid = $"{M.GetType().Name}_{M.Name}@{M_Id_Gen}";
            //Trace(M, $"Module registered : count={Modules.Count}"); 
        }

        public void Unregister(I_Component M)
        {
            if (!Modules.Contains(M))
            {
                Trace(M, "Module not registered");
                return;
            }
            Modules.Remove(M);
            M.V = null;
            //Trace(M, $"Module unregistered : count = {Modules.Count}");
        }

        public string QY(string Parameter, string Default = "")
        {
            var uri = NM.ToAbsoluteUri(NM.Uri);
            if (QueryHelpers.ParseQuery(uri.Query).TryGetValue(Parameter, out var V))
            {
                return System.Convert.ToString(V);
            }
            return Default;
        }

        public async Task Download_Bytes(string filename, byte[] data, string Mime_Type = "application/octet-stream")
        {
            await JS.InvokeAsync<object>(
                "Download_Bytes",
                filename,
                System.Convert.ToBase64String(data));
        }

    }

    public interface I_Component
    {
        Blazor_View V { get; set; }
        string Name { get; set; }
        string Uid { get; set; }
        void On_Event(string Event, object Context)
        {
        }
        void On_Global_Key(string Key)
        {
        }
    }

}
