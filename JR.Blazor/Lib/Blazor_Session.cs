﻿using JR.Saf;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Components.Server.Circuits;
namespace JR.Blazor
{
    public class Blazor_Session
    {
        public string Ip = "";

        public virtual void On_Clear()
        {
        }

        // Nettoie les ressources du portail avant fermeture
        public void Clear()
        {
            On_Clear();
        }

        // Méthode à spécialiser pour retourner un utilisateur
        public virtual Blazor_User On_New_User(Blazor_Session Session)
        {
            return new Blazor_User(Session);
        }

        Blazor_User _User = null;	// Utilisateur courant
        public Blazor_User User
        {
            get
            {
                if (_User == null)
                {
                    _User = On_New_User(this);
                }
                return _User;
            }
        }

        public virtual void After_Change_User()
        { }

        public void Change_User()
        {
            After_Change_User();
        }

        // Ressources de la session, accessible sous forme d'un indexer
        Dictionary<string, object> _Depot = new Dictionary<string, object>();
        public object this[string Index]
        {
            get
            {
                object Value = null;
                _Depot.TryGetValue(Index, out Value);
                return Value;
            }

            set
            {
                if (value == null)
                {
                    _Depot.Remove(Index);
                    return;
                }
                _Depot[Index] = value;
            }
        }

        public int User_Level => User == null ? 0 : User.Level;


        public string Language { get; set; } = "FR";
        public string Browser_Language { get; set; }
        public bool Explicit_Language { get; set; }

        public bool French => Language == "FR";

        public virtual void Force_Language(string Langue) => Language = Langue;

        public string Default_Language => Main.Language_List[0];


        // Retourne une chaine UI localisée
        public string Local_Ui(string Key, bool Neutral = false, string Default_Value = null) =>
            Blazor_Application.Local_Ui(Key, Neutral ? null : Language, Default_Value);

        // Dernier fichier à télécharger
        public class Download_Info
        {
            public string Physical_File_Name { get; set; }
            public string Client_File_Name { get; set; }
            public string Mime_Type { get; set; }
            public bool Auto_Delete { get; set; }
            public string Encode
            {
                get
                {
                    return HttpUtility.UrlEncode(string.Format("{0}~{1}~{2}~{3}", Physical_File_Name, Client_File_Name, Mime_Type, Auto_Delete));
                }
            }
            public static Download_Info Decode(string Param)
            {
                string[] Chars = JR.Strings.Split(HttpUtility.UrlDecode(Param));
                if (Chars.Length < 4) return null;
                Download_Info Dl = new Download_Info();
                Dl.Physical_File_Name = Chars[0];
                Dl.Client_File_Name = Chars[1];
                Dl.Mime_Type = Chars[2];
                Dl.Auto_Delete = JR.Convert.To_Boolean(Chars[3]);
                return Dl;
            }
        }

        Download_Info _File_Download = null;

        public Download_Info Get_Requested_Download()
        {
            Download_Info Info = _File_Download;
            //_File_Download = null;
            return Info;
        }

        public static Download_Info Get_Requested_Download(HttpRequest Request)
        {
            string File = Request.Query["File"];
            if (File == null) return null;
            return Download_Info.Decode(File);
        }


        DateTime Expiration;
        public bool Is_Expired(DateTime Now)
        {
            if (Now < Expiration) return false;
            On_Remove();
            return true;
        }

        public void On_Remove()
        {
        }

        static Dictionary<string, Blazor_Session> Dic = new Dictionary<string, Blazor_Session>();

        // Retourne soit une nouvelle session, soit une session existante selon l'ide de la session HTTP

        public static Web_Session From_Context<Web_Session>(IHttpContextAccessor Context) where Web_Session : Blazor_Session, new()
        {
            var S = Context?.HttpContext?.Session;
            var Now = DateTime.UtcNow;
            if (S == null) return new Web_Session();

            // si la session est vide, il faut créer au moins une valeur pour qu'elle soit persistée
            if (Context?.HttpContext.Response.HasStarted == false)
            {
                if (!S.TryGetValue("SESSION", out byte[] B))
                {
                    try
                    {
                        S.Set("SESSION", new byte[] { });
                    }
                    catch { }
                }
            }

            // Supprimer les sessions expirées
            void Purge_Sessions()
            {
                var Purge_List = new List<string>();
                lock (Dic)
                {
                    foreach (var DS in Dic)
                    {
                        if (DS.Value.Is_Expired(Now)) Purge_List.Add(DS.Key);
                    }
                    foreach (var P in Purge_List) Dic.Remove(P);
                }
            }

            // Si pas de session pour l'id ou session expirée créer une nouvelle session
            Web_Session WS;
            if (!Dic.TryGetValue(S.Id, out Blazor_Session SS) || SS.Is_Expired(Now))
            {
                Purge_Sessions();
                WS = new Web_Session();
                lock (Dic)
                {
                    Dic[S.Id] = WS;
                }
                // Charger l'adresse IP
                WS.Ip = Context.HttpContext.Connection.RemoteIpAddress.ToString();
                // Mettre en cache les cookies
                foreach (var Cook in Context.HttpContext.Request.Cookies)
                {
                    if (Cook.Key.StartsWith("PUI"))
                    {
                        WS.Cookies.Add(Cook.Key, Cook.Value);
                    }
                }
                WS.User.Auto_Connect();
            }
            else
            {
                WS = SS as Web_Session;
            }
            WS.Expiration = Now.AddMinutes(60);

            return WS;
        }


        // Cookie cache
        Dictionary<string, string> Cookies = new Dictionary<string, string>();
        List<(string Key, string Value, int Days)> Pending_Cookies = new List<(string Key, string Value, int Days)>();

        string Get_PUI_Key(string Name, bool User_Level)
        {
            if (User_Level) return $"PUI.{User.Uid}.{Name}";
            return $"PUI.{Name}";
        }

        public void Treat_Pending_Cookies(Action<string, string, int> Add, Action<string> Remove)
        {
            lock (Pending_Cookies)
            {
                foreach (var C in Pending_Cookies)
                {
                    if (C.Value == null)
                    {
                        Remove(C.Key);
                    }
                    else
                    {
                        Add(C.Key, C.Value, C.Days);
                    }
                }
                Pending_Cookies.Clear();
            }
        }

        public string Remove_Cookie(string Name, bool User_Level)
        {
            var Key = Get_PUI_Key(Name, User_Level);
            Cookies.Remove(Key);
            lock (Pending_Cookies)
            {
                Pending_Cookies.Add((Key: Key, Value: null, Days: 0));
            }
            return Key;
            //// La seule manière d'effacer un cookie est de mettre son expiration dans le passé
            //HttpContext.Current.Response.Cookies.Set(new HttpCookie(Key, null) { Expires = DateTime.Now.AddYears(-10) });

        }

        public string Persist_Cookie(string Name, string Value, bool User_Level, int Days = 30000)
        {
            var Key = Get_PUI_Key(Name, User_Level);
            Cookies[Key] = Value;
            lock (Pending_Cookies)
            {
                Pending_Cookies.Add((Key: Key, Value: Value, Days: Days));
            }
            return Key;
            //HttpContext.Current.Response.Cookies.Set(new HttpCookie(Key, Value) { Expires = DateTime.Now.AddDays(Days) });
        }

        public string Get_Cookie(string Name, bool User_Level)
        {
            string Key = Get_PUI_Key(Name, User_Level);
            if (Cookies.TryGetValue(Key, out string Value)) return Value;
            return null;
        }

    }
}
