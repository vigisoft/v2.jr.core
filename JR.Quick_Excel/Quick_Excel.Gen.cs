﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JR.XL
{
    public partial class Quick_Excel
    {

        // si T est une classe : seules les properties sont prises en compte
        //   dans ce cas, le header peut être le nom de la property (si keep_headers = -1)
        // Si T est un tableau d'object, passer le nom des colonnes en premiere ligne
        public static bool Generate<T>(IEnumerable<T> Rows, string Template_Path, string Output_Path, int Keep_Headers = 0)
        {
            if ((Template_Path??"") == "")
            {
                Template_Path = JR.Saf.Main.File_Parameter("JR.XL.Default_Template");
            }
            JR.Files.Check_Directory(Output_Path);
            try
            {
                using (var fastExcel = new FastExcel.FastExcel(new FileInfo(Template_Path), new FileInfo(Output_Path)))
                {
                    if (Keep_Headers < 0)
                    {
                        fastExcel.Write(Rows, 1, true);
                    }
                    else
                    {
                        fastExcel.Write(Rows, 1, Keep_Headers);
                    }
                }
                return true;
            }
            catch
            {
            }
            return false;
        }
    }
}
