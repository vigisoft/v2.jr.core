using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp.Formats.Jpeg;

namespace JR.Images
{
    // Manipulation d'images
    public class Images
    {

        public static bool Save(Image Image, string File_Name, int Quality)
        {
            try
            {
                Image.Save(File_Name);
                return true;
            }
            catch
            {

            }
            return false;
            //string Ext = JR.Files.Get_Extension(File_Name).ToLower();
            //foreach (ImageCodecInfo Codec in ImageCodecInfo.GetImageEncoders())
            //{
            //    if (Codec.FilenameExtension.ToLower().Contains (Ext))
            //    {
            //        try
            //        {
            //            Image.Save(File_Name, Codec, Encoder_Parameters(Quality));
            //            return true;
            //        }
            //        catch (Exception e)
            //        {
            //            if (e.InnerException == null)
            //            {
            //                using (Bitmap Bmp = new Bitmap(Image.Width, Image.Height))
            //                {
            //                    using (Graphics G = Graphics.FromImage(Bmp))
            //                    {
            //                        G.DrawImage(Image, 0, 0, Image.Width, Image.Height);
            //                    }
            //                    Bmp.Save(File_Name, Codec, Encoder_Parameters(Quality));
            //                    return true;
            //                }
            //            }
            //        }
            //    }
            //}
            //return false;
        }

        public static bool Rewrite_Bitmap(Image Image, string File_Name, int Quality)
        {
            var P = File_Name.Paths();
            string New_Name = Path.Combine(P.Directory, Guid.NewGuid().ToString() + P.Extension);
            if (Quality > 0)
            {
                Save(Image, New_Name, Quality);
            }
            else
            {
                Image.Save(New_Name);
            }
            Image.Dispose();
            File.Delete(File_Name);
            File.Move(New_Name, File_Name);
            return true;
        }

        //public static void Init_Lib ()
        //{
        //    if (ICodecInfo != null) return;
        //    ICodecInfo = GetEncoderInfo("image/jpeg");
        //    Default_EncParameters = Encoder_Parameters(90);
        //}

        // Compresse l'image
        //public static bool Compress_Jpg(string Source_File_Name, string Dest_File_Name)
        //{
        //    Init_Lib();
        //    try
        //    {
        //        using (Bitmap Bmp = new Bitmap(Source_File_Name))
        //        {
        //            Bmp.Save(Dest_File_Name, ICodecInfo, Default_EncParameters);
        //        }
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //    return true;
        //}

        // Fait tourner un jpg ou bien fait un flip
        //public static bool Rotate_Jpg(string Source_File_Name, string Dest_File_Name, RotateFlipType Transformation)
        //{
        //    Init_Lib();
        //    try
        //    {
        //        using (Bitmap Bmp = new Bitmap(Source_File_Name))
        //        {
        //            Bmp.RotateFlip(Transformation);
        //            Bmp.Save(Dest_File_Name, ICodecInfo, Default_EncParameters);
        //        }
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //    return true;
        //}

        // Fait tourner un jpg ou bien fait un flip
        // Mais pr�serve le bas
        //public static bool Rotate_Jpg(string Source_File_Name, string Dest_File_Name, RotateFlipType Transformation, int Preserve_Bottom)
        //{
        //    if (Preserve_Bottom <= 0)
        //    {
        //        return Rotate_Jpg(Source_File_Name, Dest_File_Name, Transformation);
        //    }
        //    Init_Lib();
        //    try
        //    {
        //        using (Bitmap Bmp = new Bitmap(Source_File_Name), Top = Bmp.Clone(new Rectangle(0, 0, Bmp.Width, Bmp.Height - Preserve_Bottom), PixelFormat.Format24bppRgb))
        //        {
        //            Top.RotateFlip(Transformation);
        //            using (Bitmap Dest = new Bitmap(Top.Width, Top.Height + Preserve_Bottom))
        //            {
        //                using (Graphics Gd = Graphics.FromImage(Dest))
        //                {
        //                    Gd.DrawImage(Top,
        //                       new Rectangle(0, 0, Top.Width, Top.Height),   // dest rectangle
        //                       new Rectangle(0, 0, Top.Width, Top.Height), // source rectangle
        //                       GraphicsUnit.Pixel);
        //                    Rectangle Bottom_Rect = new Rectangle(0, Top.Height, Top.Width, Preserve_Bottom);
        //                    // On preleve un pixel en bas � droite pour completer au cas ou
        //                    Color Background = Bmp.GetPixel(Bmp.Width - 3, Bmp.Height - 3);
        //                    Gd.FillRectangle(new SolidBrush(Background), Bottom_Rect);
        //                    Gd.DrawImage(
        //                       Bmp,
        //                       new Rectangle(0, Top.Height, Bmp.Width, Preserve_Bottom),   // dest rectangle
        //                       new Rectangle(0, Bmp.Height - Preserve_Bottom, Bmp.Width, Preserve_Bottom), // source rectangle
        //                       GraphicsUnit.Pixel);
        //                    Dest.Save(Dest_File_Name, ICodecInfo, Default_EncParameters);
        //                }
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //    return true;
        //}


        public static bool Resize_Image(string Source_File_Name, string Dest_File_Name, int Max_Size, bool Best_Quality = false, int Dpi = 0, int Quality = 0)
        {
            return Resize_Image(Source_File_Name, Dest_File_Name, Max_Size, Max_Size, Best_Quality, Dpi, Quality);
        }

        public static bool Get_Image_Info(string Source_File_Name, out int Width, out int Height)
        {
            Width = 0; Height = 0;
            try
            {
                using (Image Image = Image.Load(Source_File_Name))
                {
                    Width = Image.Width;
                    Height = Image.Height;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool Get_Image_Info(byte [] Source_Bytes, out int Width, out int Height)
        {
            Width = 0; Height = 0;
            try
            {
                using (var Im = Image.Load(Source_Bytes))
                {
                    Width = Im.Width;
                    Height = Im.Height;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool Resize_Image(string Source_File_Name, string Dest_File_Name, int Max_Width, int Max_Height, bool Best_Quality = false, int Dpi = 0, int Quality = 0, string Mode = "")
        {
            try
            {
                using (Image Src = Image.Load(Source_File_Name))
                {
                    return Resize_Bitmap(Src, Dest_File_Name, Max_Width, Max_Height, Best_Quality, Dpi, Quality, Mode);
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool Resize_Image(string Source_File_Name, Stream Dest_Stream, int Max_Width, int Max_Height, bool Best_Quality = false, int Dpi = 0, int Quality = 0)
        {
            try
            {
                using (Image Src = Image.Load(Source_File_Name))
                {
                    return Resize(Src, Dest_Stream, Max_Width, Max_Height, Best_Quality, Dpi, Quality);
                }
            }
            catch
            {
                return false;
            }
        }

        public static void Reduce(ref int Width, ref int Height, int Max_Width, int Max_Height)
        {
            // r�duire en X
            if (Max_Width > 0 && Width > Max_Width)
            {
                Height = (Height * Max_Width) / Width;
                Width = Max_Width;
            }

            // r�duire en Y
            if (Max_Height > 0 && Height > Max_Height)
            {
                Width = (Width * Max_Height) / Height;
                Height = Max_Height;
            }
        }

        // Dans le cas du zoom max_width et max_height doivent �tre respect�s
        public static void Reduce_Zoom(ref int Width, ref int Height, int Max_Width, int Max_Height, out int Z_X, out int Z_Y, out int Z_Width, out int Z_Height)
        {
            Z_X = 0;
            Z_Y = 0;
            Z_Width = Width;
            Z_Height = Height;

            // r�duire en X
            if (Max_Width > 0)
            {
                int Proposed_Height = (Height * Max_Width) / Width;
                if (Max_Height <= 0 || Proposed_Height >= Max_Height)
                {
                    if (Max_Height > 0)
                    {
                        Z_Height = (Max_Height * Width) / Max_Width;
                        Z_Y = (Height - Z_Height) / 2;
                        Height = Max_Height;
                    }
                    else
                    {
                        Height = Proposed_Height;
                    }
                    Width = Max_Width;
                    return;
                }
            }

            // r�duire en Y
            if (Max_Height > 0)
            {
                int Proposed_Width = (Width * Max_Height) / Height;
                if (Max_Width <= 0 || Proposed_Width >= Max_Width)
                {
                    if (Max_Width > 0)
                    {
                        Z_Width = (Max_Width * Height) / Max_Height;
                        Z_X = (Width - Z_Width) / 2;
                        Width = Max_Width;
                    }
                    else
                    {
                        Width = Proposed_Width;
                    }
                    Height = Max_Height;
                    return;
                }
            }
        }

        static bool Resize(Image Src, object Output, int Max_Width, int Max_Height, bool Best_Quality = false, int Dpi = 0, int Quality = 0, string Mode = "")
        {
            try
            {
                bool Zoom_Mode = Mode == "Z";
                int Nwidth = Src.Width;
                int Nheight = Src.Height;
                if (Zoom_Mode)
                {
                    Reduce_Zoom(ref Nwidth, ref Nheight, Max_Width, Max_Height, out int Z_X, out int Z_Y, out int Z_Width, out int Z_Height);
                    Src.Mutate(x => x.Crop(new Rectangle(Z_X, Z_Y, Z_Width, Z_Height)));
                }
                else
                {
                    Reduce(ref Nwidth, ref Nheight, Max_Width, Max_Height);
                }
                var RS = new ResizeOptions { Mode = ResizeMode.Stretch, Sampler = KnownResamplers.Bicubic, Size = new Size(Nwidth, Nheight) };
                Src.Mutate(x => x.Resize(RS));
                var jpegEncoder = new JpegEncoder { Quality = Quality == 0 ? 90 : Quality };

                string Dest_File_Name = Output as string;
                if (Dest_File_Name != null)
                {
                    JR.Files.Check_Directory(Dest_File_Name);
                    System.IO.File.Delete(Dest_File_Name);
                    Src.Save(Dest_File_Name, jpegEncoder);
                    return true;
                }

                Stream Dest_Stream = Output as Stream;
                if (Dest_Stream != null)
                {
                    Src.Save(Dest_Stream, jpegEncoder);
                    return true;
                }

            }
            catch
            {
                return false;
            }
            return true;
        }

        public static bool Resize_Bitmap(Image Src, string Dest_File_Name, int Max_Width, int Max_Height, bool Best_Quality = false, int Dpi = 0, int Quality = 0, string Mode = "")
        {
            return Resize(Src, Dest_File_Name, Max_Width, Max_Height, Best_Quality, Dpi, Quality, Mode);
        }

    }
}
