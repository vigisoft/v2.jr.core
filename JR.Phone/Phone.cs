﻿using PhoneNumbers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using JR;
using System.Globalization;

namespace JR.PHONE
{
    public class Phone
    {

        public int Country_Id = 0;
        public Phone (int Country)
        {
            Country_Id = Country;
        }
        // Classe utilitaire phonenumber
        static PhoneNumberUtil _PNU  = null;
        PhoneNumberUtil PNU
        {
            get
            {
                if (_PNU == null) _PNU = PhoneNumberUtil.GetInstance();
                return _PNU;
            }
        }

        List<string> Numbers = new List<string>();
        public void Add (string Number)
        {
            // Normaliser le numéro sur +[11-14] chiffres
            StringBuilder SB = new StringBuilder();

            // Lire la liste de numéros
            // Supprimer les premiers zéros
            bool Keep_Zeros = false;
            bool Inter = false;
            foreach (var C in Number)
            {
                if (C == '+')
                {
                    Inter = true;
                    continue;
                }
                if (!char.IsDigit(C)) continue;
                if (!Keep_Zeros)
                {
                    if (C == '0') continue;
                    Keep_Zeros = true;
                }
                SB.Append(C);
            }

            // Si numéro 9 chiffres ajouter 33 pour les français
            if (SB.Length == 9 && Country_Id == 65 && !Inter)
            {
                SB.Insert(0, "33");
            } else if (SB.Length > 15||SB.Length<9)
            {
                return;
            }
            Number = "+"  + SB.ToString();
            Numbers.Add(Number);
        }
        public string First_Mobile
        {
            get
            {
                foreach (var N in Numbers)
                {
                    // La recherche de type ne fonctionne que si on a le country + le numéro national
                    // Il faut donc les extraire
                    var defaultRegionMetadata = new PhoneNumbers.PhoneMetadata();
                    var National_Number = new StringBuilder();
                    var Phone_Number = new PhoneNumber.Builder();
                    try
                    {
                        var Country_Id = PNU.MaybeExtractCountryCode(N, defaultRegionMetadata, National_Number, true, Phone_Number);
                        ulong L_Num = 0;
                        if (!ulong.TryParse(National_Number.ToString(), out L_Num)) continue;
                        var Num = Phone_Number.SetNationalNumber(L_Num).Build();
                        if (PNU.GetNumberType(Num) != PhoneNumberType.MOBILE) continue;
                        return N;
                    }
                    catch
                    {
                    }
                }
                return "";
            }
        }

        public IEnumerable<string> Valid_Numbers
        {
            get { return Numbers; }
        }

        public static string Normalized (string Number, int Country_Id = 65)
        {
            var Ph = new Phone(Country_Id);
            Ph.Add(Number);
            if (Ph.Numbers.Count == 0) return "!!" + Number;
            return Ph.Numbers[0];
        }
    }
}