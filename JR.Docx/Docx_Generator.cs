﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using XD = DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Drawing.Wordprocessing;

using System;
using System.Linq;
using System.Xml;
using JR;
using System.IO;
using System.Net;
using System.Drawing;

namespace JR.DOCX
{
    public static class Docx_Generator
    {
        static void Treat_Fragment(MainDocumentPart Main_Part, OpenXmlElement Fragment, XmlNode Data_Node)
        {
            bool Bool_Value(string V)
            {
                if (V == null) return false;
                if (V == "0") return false;
                if (V == "") return false;
                return true;
            }
            string Node_Value(string Data)
            {
                var N = Data_Node.SelectSingleNode(Data.Trim());
                if (N == null) return null;
                if (N.ChildNodes.Count == 1 && N.FirstChild.ChildNodes.Count == 0) return N.InnerText;
                return "1";
            }

            void Remove_Parent(OpenXmlElement Tag_Sdt, string End_Tag)
            {
                if (End_Tag.Length > 0)
                {
                    foreach (var Remove_Tag in Tag_Sdt.Ancestors().Where((B) => B.LocalName == End_Tag))
                    {
                        Remove_Tag.Remove();
                        return;
                    }
                }
                Tag_Sdt.Remove();
            }

            bool Generate_Value(OpenXmlElement Tag_Sdt, string Data)
            {
                // Isoler la condition
                var D = Data.Split('?');

                // Evaluer la valeur
                var Value = Node_Value(D[0]);

                // Si condition vraie, supprimer jusqu'au tag indiqué
                if (D.Length > 1)
                {
                    var Ok = Bool_Value(Value);
                    if (!Ok)
                    {
                        Remove_Parent(Tag_Sdt, D[1].Trim());
                        return true;
                    }
                }

                // Générer la valeur
                var Content = Tag_Sdt.Elements().Where((B) => B.LocalName == "sdtContent").FirstOrDefault();
                if (Content == null) return false;

                // Localiser le t
                var Tag_T = Content.Descendants<Text>().First();
                if (Tag_T == null) return false;
                var Tag_R = Tag_T.Parent;
                Tag_T.Remove();

                // Traiter les retours à la ligne
                int Row = 0;
                foreach (var L in JR.Strings.Read_Lines(Value))
                {
                    if (Row > 0) Tag_R.AppendChild(new Break());
                    Tag_R.AppendChild(new Text(L));
                    Row++;
                }
                //if (Row == 0) Tag_R.AppendChild(new Text(""));

                // Supprimer tout sauf le parent du T
                foreach (var CC in Content.Elements().ToArray())
                {
                    if (CC == Tag_R || Tag_R.Ancestors().Contains(CC)) continue;
                    CC.Remove();
                }
                return true;
            }
            bool Generate_Picture(OpenXmlElement Tag_Sdt, string Data)
            {
                // Isoler la condition
                var D = Data.Split('?');

                // Evaluer la valeur
                var Value = Node_Value(D[0]);

                if ((Value ?? "") == "")
                {
                    Remove_Parent(Tag_Sdt, "");
                    return true;
                }

                // Si condition vraie, supprimer jusqu'au tag indiqué
                if (D.Length > 1)
                {
                    var Ok = Bool_Value(Value);
                    if (!Ok)
                    {
                        Remove_Parent(Tag_Sdt, D[1].Trim());
                        return true;
                    }
                }

                // Générer l'image
                var Content = Tag_Sdt.Elements().Where((B) => B.LocalName == "sdtContent").FirstOrDefault();
                if (Content == null) return false;

                // Récupérer la taille du modèle
                var Extent = Content.Descendants<Extent>().First(); // Taille de la boite
                var R_Width = (double)Extent.Cx;
                var R_Height = (double)Extent.Cy;

                // Element définissant la taille de l'image
                var Xfrm = Content.Descendants<XD.Transform2D>().First();

                var Picture_Ref = Add_Picture_Ref(Value, out double Width, out double Height);
                if (Picture_Ref == "")
                {
                    Remove_Parent(Tag_Sdt, "");
                    return true;
                }

                var New_Width = R_Width;
                var New_Height = R_Height;

                double Rect_Ratio = R_Width / R_Height;
                double Pic_Ratio = Width / Height;

                // Comparer les ratios
                if (Rect_Ratio > Pic_Ratio)
                {
                    // La hauteur est référence
                    New_Width = (Width * R_Height) / Height;
                    New_Height = R_Height;
                }
                else
                {
                    // La largeur est référence
                    New_Height = (Height * R_Width) / Width;
                    New_Width = R_Width;
                }



                // Recalculer la taille pour que l'image de soit pas déformée
                Extent.Cx = Xfrm.Extents.Cx = (long)New_Width;
                Extent.Cy = Xfrm.Extents.Cy = (long)New_Height;


                // Localiser le a:blip
                var Tag_T = Content.Descendants<XD.Blip>().First();
                var R_Embed = Tag_T.Embed; //.GetAttributes().Where((A) => A.LocalName == "embed").First();
                R_Embed.Value = Picture_Ref;
                //Tag_T.SetAttribute(R_Embed);

                return true;
            }

            bool Generate_If(OpenXmlElement Tag_Sdt, char Key, string Data)
            {
                bool Negate = Key == '!';

                // Isoler la portee
                var D = (Data + "/").Split('/');

                // Evaluer la condition
                var Ok = Bool_Value(Node_Value(D[0]));
                if (Negate) Ok = !Ok;

                // Si condition vraie, supprimer jusqu'au tag de portee
                if (!Ok)
                {
                    Remove_Parent(Tag_Sdt, D[1].Trim());
                }
                else
                {
                    // Dans tous les cas supprimer le tag
                    Tag_Sdt.Remove();
                }
                return true;
            }

            bool Generate_Loop(OpenXmlElement Tag_Sdt, string Data)
            {

                // Isoler la portee
                var D = (Data + "/tr").Split('/');

                var Tag_Template = Tag_Sdt.Ancestors().Where((B) => B.LocalName == D[1].Trim()).FirstOrDefault();
                if (Tag_Template == null) return false;
                Tag_Sdt.Remove(); // Attention à toujours laisser au moins un item dans le parent (par exemple un espace), sinon erreur de chargement de fichier

                // Evaluer les éléments
                // Générer les tags
                foreach (XmlNode Item in Data_Node.SelectNodes(D[0].Trim()))
                {
                    // Instancier et insérer le template pour chaque ligne de data
                    var Row = Tag_Template.CloneNode(true);
                    Treat_Fragment(Main_Part, Row, Item);
                    Tag_Template.Parent.InsertBefore(Row, Tag_Template);
                }

                // Supprimer le template
                Tag_Template.Remove();
                return true;
            }

            string Add_Picture_Ref(string Value, out double Width, out double Height)
            {
                var IP = Main_Part.AddImagePart(ImagePartType.Jpeg);
                try
                {
                    byte[] Bytes;
                    using (var WC = new WebClient())
                    {
                        Bytes = WC.DownloadData(Value);
                        JR.Images.Images.Get_Image_Info(Bytes, out var W, out var H);
                        Width = W; Height = H;
                    }
                    using (var MS = new MemoryStream(Bytes, false))
                    {
                        //using (var Bmp = new Bitmap(MS))
                        //{
                        //    Width = Bmp.Width;
                        //    Height = Bmp.Height;
                        //}
                        //MS.Seek(0, SeekOrigin.Begin);
                        IP.FeedData(MS);
                    }
                }
                catch
                {
                    Width = Height = 0;
                    return "";
                }

                var Rel_Id = Main_Part.GetIdOfPart(IP);
                return Rel_Id;
            }

            bool Treat_Next_Tags()
            {
                foreach (var Tag_Sdt in Fragment.Descendants().ToArray().Where((B) => B.LocalName == "sdt"))
                {
                    // Chercher le tag v:tag
                    var Tag_Tag = Tag_Sdt.Descendants<Tag>().FirstOrDefault();
                    if (Tag_Tag == null) continue;
                    var Text = Tag_Tag.Val.InnerText.Trim();
                    Tag_Tag.Remove();
                    if (Text.Length < 2) continue;
                    var Key = Text[0];
                    switch (Key)
                    {
                        case '>':
                            if (Generate_Picture(Tag_Sdt, Text.Substring(1))) return true;
                            break;
                        case '=':
                            if (Generate_Value(Tag_Sdt, Text.Substring(1))) return true;
                            break;
                        case '?':
                        case '!':
                            if (Generate_If(Tag_Sdt, Key, Text.Substring(1))) return true;
                            break;
                        case '*':
                            if (Generate_Loop(Tag_Sdt, Text.Substring(1))) return true;
                            break;
                    }
                }
                return false;
            }
            while (Treat_Next_Tags()) { }
        }

        static void Generate_Doc(WordprocessingDocument Doc, XmlDocument Source)
        {
            XmlNode Get_Source_Root()
            {
                XmlNode Root = Source;
                foreach (XmlNode N in Source.ChildNodes)
                {
                    if (N.LocalName == "xml") continue; // Eviter le <?xml ...>
                    Root = N;
                    break;
                }
                return Root;
            }

            var Body = Doc.MainDocumentPart.Document.Body;
            Treat_Fragment(Doc.MainDocumentPart, Body, Get_Source_Root());

            // Supprimer les controles de contenu mais garder leur contenu
            // Pour chaque content controler on différencie le traitement
            // 3+ types : sdtRUN sdtBlock et sdtCell
            foreach (var Element in Body.Descendants<SdtElement>().ToArray())
            {
                var Sub_Items = Element.Elements().Where((B) => B.LocalName == "sdtContent").FirstOrDefault().Elements().ToArray();
                if (Element is SdtBlock && (Sub_Items.Length > 1 || Sub_Items[0].LocalName != "p")) // SI un seul P, ne pas l'imbriquer
                {
                    var Para = new Paragraph();
                    var R = new Run();
                    Para.Append(R);
                    Element.Parent.ReplaceChild(Para, Element);
                    foreach (var Item in Sub_Items)
                    {
                        R.Append(Item.CloneNode(true));
                    }
                }
                else
                {
                    foreach (var Item in Sub_Items)
                    {
                        Element.Parent.InsertBefore(Item.CloneNode(true), Element);
                    }
                    Element.Remove();
                }
            }
        }


        // Syntaxe des tags
        //  Tous les tags sont dans des textes bruts (menu développeur, passer en mode création, insérer des contrôles de texte brut) 
        //  Pour définir le comportement : clic froit sur le texte et Propriétés => dans Balise
        // Les comportements sont :
        //    !condition[/portee] = if not condition
        //    ?condition[/portee] = if condition
        //    *champ[/portee] = itération de l'element en cours selon la variable champ
        //    =variable  = remplace le contenu var la valeur de la variable
        //    =variable?[portee]  = variable conditionnelle
        //  Portée : 
        //    la portée définit le bloc parent à prendre en compte pour le if ou pour l'itération
        //    les codes possibles sont tc, tr, p, tbl. La portée par défaut est le tag lui-même
        //

        static public bool Generate(string Template, string Dest, XmlDocument Source)
        {
            // Copier le template dans la destination
            if (!JR.Files.Safe_Copy(Template, Dest))
            {
                return false;
            }
            // Traiter le document principal
            // On pourrait traiter aussi l'entête et le pied de page
            try
            {
                using (var Document = WordprocessingDocument.Open(Dest, true))
                {
                    Generate_Doc(Document, Source);
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        static public bool Generate(string Template, Stream Dest, XmlDocument Source)
        {
            try
            {
                using (var Stream = File.OpenRead(Template))
                {
                    Stream.CopyTo(Dest);
                }
                using (var Document = WordprocessingDocument.Open(Dest, true))
                {
                    Generate_Doc(Document, Source);
                }
            }
            catch
            {
                return false;
            }
            Dest.Position = 0;
            return true;
        }
    }
}
