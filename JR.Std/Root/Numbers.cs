using System;

namespace JR
{
	/// <summary>
	/// 
	/// </summary>
	public class Numbers
	{
		public static bool In_Range (int Number, int Lower_Bound, int Upper_Bound)
		{
			if (Number < Lower_Bound) return false;
			if (Number > Upper_Bound) return false;
			return true;
		}
	}
}
