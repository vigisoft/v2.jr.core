﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;

namespace JR
{
    public static class Net_File
    {

        public static bool Is_Remote(string Path)
        {
            return Path.ToUpper().Contains("TP:");
        }

        static bool Is_FTP(string Path)
        {
            return Path.ToUpper().StartsWith("FTP:");
        }

        static FtpStatusCode Get_Response(string Url, string Method)
        {
            try
            {
                FtpWebRequest FTP = (FtpWebRequest)WebRequest.Create(Url);
                FTP.Method = Method;
                using (FtpWebResponse Rsp = (FtpWebResponse)FTP.GetResponse())
                {
                    Rsp.Close();
                    return Rsp.StatusCode;
                }
            }
            catch
            {
                return FtpStatusCode.Undefined;
            }
        }

        static bool Check_FTP_Directory (string Path)
        {
            int Last = Path.LastIndexOf('/');
            if (Last <= 5) return false;
            string Dir = Path.Substring(0, Last);
            if (Dir.LastIndexOf('/') < 8) return true;  // Pas de nom de répertoire
            if (Dir_Exists(Dir)) return true;
            // Creation de toute la chaine de répertoire
            if (!Check_FTP_Directory(Dir)) return false;
            return Get_Response(Dir, WebRequestMethods.Ftp.MakeDirectory) == FtpStatusCode.PathnameCreated;
        }

        public static bool Copy(string Local_Source, string Remote_Dest)
        {
            try
            {
                if (Is_FTP(Remote_Dest))
                {
                    if (!Check_FTP_Directory(Remote_Dest)) return false;
                    using (WebClient Request = new WebClient())
                    {
                        Request.UploadFile(Remote_Dest, Local_Source);
                        return true;
                    }
                }
                else
                {
                    if (!JR.Files.Check_Directory(Remote_Dest)) return false;
                    File.Copy(Local_Source, Remote_Dest, true);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool Delete(string Path)
        {
            try
            {
                if (Is_FTP(Path))
                {
                    return Get_Response(Path, WebRequestMethods.Ftp.DeleteFile) == FtpStatusCode.FileActionOK;
                }
                else
                {
                    File.Delete(Path);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool Dir_Exists(string Path)
        {
            try
            {
                if (Is_FTP(Path))
                {
                    return Get_Response(Path, WebRequestMethods.Ftp.ListDirectory) != FtpStatusCode.Undefined;

                    //FTP.Method = WebRequestMethods.Ftp.GetDateTimestamp;
                    //using (FtpWebResponse Rsp = (FtpWebResponse)FTP.GetResponse())
                    //{
                    //    bool Done = Rsp.StatusCode == FtpStatusCode.FileStatus;
                    //    Rsp.Close();
                    //    return Done;
                    //}
                    //FTP.Method = WebRequestMethods.Ftp.ListDirectory;
                    //using (FtpWebResponse Rsp = (FtpWebResponse)FTP.GetResponse())
                    //{
                    //    bool Done = Rsp.StatusCode == FtpStatusCode.OpeningData;
                    //    Rsp.Close();
                    //    return Done;
                    //}
                }
                else
                {
                    return Directory.Exists(Path);
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool File_Exists(string Path)
        {
            try
            {
                if (Is_FTP(Path))
                {
                    return Get_Response(Path, WebRequestMethods.Ftp.GetDateTimestamp) == FtpStatusCode.FileStatus;
                }
                else
                {
                    return File.Exists(Path);
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool Download(string URL, string Local_File)
        {
            try
            {
                if (!JR.Files.Check_Directory(Local_File)) return false;
                using (WebClient WC = new WebClient())
                {
                    WC.DownloadFile(URL, Local_File);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static string Read_All (string URL)
        {
            try
            {
                using (WebClient WC = new WebClient())
                {
                    return WC.DownloadString(URL);
                }
            }
            catch
            {
                return null;
            }
        }

    }
}
