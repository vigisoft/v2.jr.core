using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace JR
{

    public interface I_Expirable_Item
    {
        DateTime? Expiration_UTC { get; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class Time_List<T> where T : I_Expirable_Item
    {
        public object Lock = new object();
        HashSet<T> L = new HashSet<T>();
        public DateTime? Next_Expiration = null;

        public void Add(T Item)
        {
            var Exp = Item.Expiration_UTC;
            if (Exp == null) return;
            lock (Lock)
            {
                if (Next_Expiration == null) Next_Expiration = Exp;
                else if (Exp < Next_Expiration) Next_Expiration = Exp;
                L.Add(Item);
            }
        }

        public void Remove(T Item)
        {
            lock (Lock)
            {
                L.Remove(Item);
            }
        }

        public bool Contains(T Item)
        {
            lock (Lock)
            {
                return L.Contains(Item);
            }
        }

        public void Clear()
        {
            lock (Lock)
            {
                L.Clear();
                Next_Expiration = null;
            }
        }

        public void Treat_Expired(Action<T> On_Expired)
        {
            var E = new List<T>();
            lock (Lock)
            {
                if (Next_Expiration == null) return;
                var Now = DateTime.UtcNow;
                if (Now <= Next_Expiration) return;
                E = L.Where((Item) => Item.Expiration_UTC <= Now).ToList();
                foreach (var Item in E) L.Remove(Item);
                Next_Expiration = null;
                foreach (var Item in L)
                {
                    if (Next_Expiration != null && Item.Expiration_UTC > Next_Expiration) continue;
                    Next_Expiration = Item.Expiration_UTC;
                }
            }
            foreach (var Item in E) On_Expired(Item);
        }

    }
}
