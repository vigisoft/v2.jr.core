using System;

namespace JR
{


	/// <summary>
	/// Description r�sum�e de OS.
	/// </summary>
	public static class OS
	{

        public class Registry_Manager
        {
            public virtual void Register(string Sub_Register, string Key_Name, string Name, object Value)
            {
            }

            //  Delete the Name / Value pair at Key_Name
            //  If the Key does not exist it is created
            public virtual void Delete_Value(string Sub_Register, string Key_Name, string Name)
            {
            }

            //  Get value for Name
            public virtual string Get_Value(string Sub_Register, string Key_Name, string Name)
            {
                return JR.Saf.Main.Parameter(Sub_Register + "." + Key_Name + "." + Name);
            }

            //  Removes a key and its Name / Value pairs from the Windows NT Registry
            //  All child keys of a Key_Name must first be removed before Unregister
            //  will remove a Key_Name from the registry.
            public virtual void Unregister(string Sub_Register, string Key_Name)
            {
            }

        }


        static Registry_Manager Registry = new Registry_Manager();

        //public enum Sub_Registry
        //{
        //    KEY_CLASSES_ROOT,
        //    KEY_CURRENT_USER,
        //    KEY_LOCAL_MACHINE,
        //    KEY_USERS,
        //    KEY_PERFORMANCE_DATA,
        //    KEY_CURRENT_CONFIG,
        //    KEY_DYN_DATA
        //}

		//  Place a Name / Value pair in the Windows NT Registry
		//  A blank name implies the default value for the key
		//  If the Key does not exist it is created
		public static void Register (string Sub_Register, string Key_Name, string Name, object Value)
		{
            Registry.Register(Sub_Register, Key_Name, Name, Value);
        }

		//  Delete the Name / Value pair at Key_Name
		//  If the Key does not exist it is created
		public static void Delete_Value (string Sub_Register, string Key_Name, string Name)
		{
            Registry.Delete_Value(Sub_Register, Key_Name, Name);
        }

        //  Get value for Name
        public static string Get_Value (string Sub_Register, string Key_Name, string Name)
		{
            return Registry.Get_Value(Sub_Register, Key_Name, Name);
		}

		//  Removes a key and its Name / Value pairs from the Windows NT Registry
		//  All child keys of a Key_Name must first be removed before Unregister
		//  will remove a Key_Name from the registry.
		public static void Unregister (string Sub_Register, string Key_Name)
		{
            Registry.Unregister(Sub_Register, Key_Name);
		}


		//  Return the module directory (where exe file is)
		public static string Module_Directory
		{
			get {return System.IO.Directory.GetCurrentDirectory ();}
		}

		//  Return the path to current working directory (from where exe file has been launched)
		public static string Working_Directory
		{
			get {return System.IO.Directory.GetCurrentDirectory ();}
		}

		//  Modify the current working directory
		public static void Set_Working_Directory (string Path)
		{
			System.IO.Directory.SetCurrentDirectory (Path);
		}

		// return the current task ID
		public static UInt64 Current_Task_Id
		{
			get {return 1;}
		}

        public static string Current_Program_File_Name
        {
            get { return System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName; }
        }

        public static string Current_Program_Name
        {
            get { return System.IO.Path.GetFileName (System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName); }
        }

        // return the current process ID
		public static UInt64 Current_Process_Id
		{
			get {return 1;}
		}

        public static void Kill_Program()
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }

        public static void Explore (string Url, bool Use_Explorer = false)
        {
            System.Diagnostics.ProcessStartInfo Info;
            if (Use_Explorer)
            {
                Info = new System.Diagnostics.ProcessStartInfo("explorer.exe", $"\"{Url}\"");
                Info.UseShellExecute = false;
            }
            else
            {
                Info = new System.Diagnostics.ProcessStartInfo($"\"{Url}\"");
                Info.Verb = "Open";
                Info.UseShellExecute = true;
            }
            System.Diagnostics.Process P = System.Diagnostics.Process.Start(Info);
        }

        public static bool Execute (string Command)
        {
            var Info = new System.Diagnostics.ProcessStartInfo();
            Info.Verb = "runas";
            Info.FileName = Command;
            Info.UseShellExecute = false;
            var P = System.Diagnostics.Process.Start(Info);
            return P.WaitForExit(10000);
        }

    }
}

/*
 * 
 
  ////////////////////////////////////////////////////////
  // current directory access
  ////////////////////////////////////////////////////////
   function Module_Directory return String;

   function Working_Directory return String;

   procedure Set_Working_Directory (Path : in String);

  ////////////////////////////////////////////////////////
  // Arguments check and read
  ////////////////////////////////////////////////////////
   function Argument_Count return Integer;
   //  Return the number of arguments

   function Argument (Row : Integer) return String;
   //  Return the full argument

   function Argument_Exist (Name : String;Alias : String := "") return Boolean;
   //  Check for /Name /Name=value or -Name or -Name=value or /A or -A
   // where A is one of Alias chars

   function Argument_Value (Name : String;Alias : String := "") return String;
   //  Check for /Name /Name=value or -Name or -Name=value ou /A= or -A=
   // where A is one of Alias chars 
   //   and return value


  // Scan a directory or a pattern of files
  generic
    with procedure Treat_Entry (Name : in String;
                                Stop : in out Boolean);
  procedure Scan_Directory
    (Name    : in String;
     Files   : in Boolean := True;
     Subdirs : in Boolean := False);

  function Base_Name (File : in String) return String;
  // Removes path and extension

  function Extension  (File : in String) return String;
  // Removes path and Name

  function File_Exist (Name : String) return Boolean;
  // File or pattern presence check

  type Ty_Plateform is (Windows, VMS, UNIX, NET, JVM);

  function Plateform return Ty_Plateform;
  // Return the current plateform


  function New_Guid return String;
  // Return a new GUID

  function Get_Utc_Time return Jst_Time;
  // Return Current UTC Time

  function New_Console_Name return String;
  // Create a new console and return its file name if no console exists
  //   retrun "" if console already allocated


 */
