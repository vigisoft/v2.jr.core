using System;
using System.Globalization;
using System.Data;
using System.IO;

namespace JR
{
	/// <summary>
	/// 
	/// </summary>
	public static class Compare
	{
        public static bool Is_Equal (this object Me, object To)
        {
            if (Me == To) return true;
            if (Me == null || To == null) return false;
            if (Me.Equals(To)) return true;
            Type Me_Type = Me.GetType();
            Type To_Type = To.GetType();
            if (Me_Type != To_Type)
            {
                try
                {
                    return Me.Equals(System.Convert.ChangeType(To, Me_Type));
                }
                catch { }
            }
            return false;
        }
	}
}
