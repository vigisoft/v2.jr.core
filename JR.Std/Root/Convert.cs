using System;
using System.Globalization;
using System.Data;
using System.IO;

namespace JR
{
	/// <summary>
	/// 
	/// </summary>
	public static class Convert
	{
		public const string Format_Time = "yyyy'-'MM'-'dd HH':'mm':'ss'.'fff";

        public static object Default<T>()
        {
            return Default(typeof(T));
        }

        public static bool Is_Default(object Value)
        {
            if (Value == null) return true;
            return Value.Equals(Default(Value.GetType()));
        }

        public static object Default(Type Type)
        {
            if (Type == typeof(int)) return 0;
            if (Type == typeof(long)) return 0;
            if (Type == typeof(double)) return 0.0;
            if (Type == typeof(decimal)) return 0m;
            if (Type == typeof(DateTime)) return Time.Null_Time;
            if (Type == typeof(bool)) return false;
            if (Type == typeof(string)) return "";
            if (Type == typeof(Guid)) return Guid.Empty;
            return default(Type);
        }

        public static T To_Type<T>(object Value)
        {
            return (T)(To_Type (typeof (T), Value));
            //if (Value == null) return (T)Default(typeof(T));
            //bool Error;
            //if (typeof(T) == typeof(int)) return (T)(object)To_Integer(Value, out Error);
            //if (typeof(T) == typeof(long)) return (T)(object)To_Long(Value, out Error);
            //if (typeof(T) == typeof(double)) return (T)(object)To_Float(Value, out Error);
            //if (typeof(T) == typeof(decimal)) return (T)(object)To_Decimal(Value, out Error);
            //if (typeof(T) == typeof(DateTime)) return (T)(object)To_Time(Value, out Error);
            //if (typeof(T) == typeof(bool)) return (T)(object)To_Boolean(Value, out Error);
            //if (typeof(T) == typeof(string)) return (T)(object)To_String(Value, out Error);
            //if (typeof(T) == typeof(Guid)) return (T)(object)To_Guid(Value, out Error);
            //try
            //{
            //    return (T)Value;
            //}
            //catch { return (T)Default(typeof(T)); };
        }

        public static object To_Type(Type T, object Value)
        {
            if (Value == null) return Default(T);
            bool Error;
            if (T == typeof(int)) return (object)To_Integer(Value, out Error);
            if (T == typeof(long)) return (object)To_Long(Value, out Error);
            if (T == typeof(double)) return (object)To_Float(Value, out Error);
            if (T == typeof(decimal)) return (object)To_Decimal(Value, out Error);
            if (T == typeof(DateTime)) return (object)To_Time(Value, out Error);
            if (T == typeof(bool)) return (object)To_Boolean(Value, out Error);
            if (T == typeof(string)) return (object)To_String(Value, out Error);
            if (T == typeof(Guid)) return (object)To_Guid(Value, out Error);
            try
            {
                return System.Convert.ChangeType (Value,T);
            }
            catch { return Default(T); };
        }

        public static int To_Integer(this object Value)
        {
            bool Error;
            return To_Integer(Value, out Error);
        }
        public static int? To_Integer_Null(this object Value)
        {
            bool Error;
            return To_Integer_Null(Value, out Error);
        }

        public static long To_Long(this object Value)
        {
            bool Error;
            return To_Long(Value, out Error);
        }

        public static Guid To_Guid(this object Value)
        {
            bool Error;
            return To_Guid(Value, out Error);
        }

        public static decimal To_Decimal(this object Value)
        {
            bool Error;
            return To_Decimal(Value, out Error);
        }

        public static double To_Float(this object Value)
        {
            bool Error;
            return To_Float(Value, out Error);
        }

        public static DateTime To_Time(this object Value)
		{
			bool Error;
			return To_Time (Value, out Error);
		}

        public static bool To_Boolean(this object Value)
        {
            bool Error;
            return To_Boolean(Value, out Error);
        }

        public static bool? To_Boolean_Null(this object Value)
        {
            bool Error;
            return To_Boolean_Null (Value, out Error);
        }

        public static string To_String (this object Value)
		{
			bool Error;
			return To_String (Value, out Error);
		}

        static readonly char[] Dots = {'.',','};
        public static int To_Integer(this object Value, out bool Error)
        {
            Error = false;
            try
            {
                //if (Value is int) return (int)Value;
                //if (Value is bool) return (int)System.Convert.ToInt64((bool)Value);
                //if (Value is long) return (int)Value;
                //if (Value is double) return (int)System.Convert.ToInt64((double)Value);
                //if (Value is decimal) return (int)Value;
                //if (Value is byte) return (int)Value;
                //if (Value is short) return (int)Value;
                if (Value is string)
                {
                    if (Value.Equals("")) return 0;
                    string V = (string)Value;
                    if (V.IndexOfAny(Dots) > 0) return (int)(To_Float(Value, out Error));
                    if (V.IndexOf('\x00A0') > 0) V = V.Replace("\x00A0", ""); // Parfois des espaces ins�cables
                    return (int)System.Convert.ToInt64(V);
                }
                return System.Convert.ToInt32(Value);
            }
            catch { };
            Error = true;
            return 0;
        }

        public static int? To_Integer_Null(this object Value, out bool Error)
        {
            Error = false;
            try
            {
                //if (Value is int) return (int)Value;
                //if (Value is bool) return (int)System.Convert.ToInt64((bool)Value);
                //if (Value is long) return (int)Value;
                //if (Value is double) return (int)System.Convert.ToInt64((double)Value);
                //if (Value is decimal) return (int)Value;
                //if (Value is byte) return (int)Value;
                //if (Value is short) return (int)Value;
                if (Value is string)
                {
                    if (Value.Equals("")) return null;
                    string V = (string)Value;
                    if (V.IndexOfAny(Dots) > 0) return (int)(To_Float(Value, out Error));
                    if (V.IndexOf('\x00A0') > 0) V = V.Replace("\x00A0", ""); // Parfois des espaces ins�cables
                    return (int)System.Convert.ToInt64(V);
                }
                return System.Convert.ToInt32(Value);
            }
            catch { };
            Error = true;
            return null;
        }

        public static long To_Long(object Value, out bool Error)
        {
            Error = false;
            try
            {
                //if (Value is int) return (long)(int)Value;
                //if (Value is bool) return (long)System.Convert.ToInt64((bool)Value);
                //if (Value is long) return (long)Value;
                //if (Value is double) return (long)System.Convert.ToInt64((double)Value);
                if (Value is string)
                {
                    if (Value.Equals("")) return 0;
                    string V = (string)Value;
                    if (V.IndexOfAny(Dots) > 0) return (long)(To_Float(Value, out Error));
                    return (long)System.Convert.ToInt64((string)Value);
                }
                return System.Convert.ToInt64(Value);
            }
            catch { };
            Error = true;
            return 0;
        }

        public static Guid To_Guid (object Value, out bool Error)
        {
            Error = false;
            try
            {
                if (Value is Guid) return (Guid)Value;
                if (Value is string)
                {
                    if (Value.Equals("")) return Guid.Empty;
                    Guid Val;
                    if (Guid.TryParse((string)Value, out Val)) return Val;
                    Error = true;
                    return Guid.Empty;
                }
            }
            catch { };
            Error = true;
            return Guid.Empty;
        }

        public static double To_Float(object Value, out bool Error)
        {
            Error = false;
            try
            {
                //if (Value is int) return System.Convert.ToDouble((int)Value);
                //if (Value is long) return System.Convert.ToDouble((long)Value);
                //if (Value is bool) return System.Convert.ToDouble((bool)Value);
                //if (Value is double) return (double)Value;
                if (Value is string)
                {
                    if (Value.Equals("")) return 0.0;
                    return System.Convert.ToDouble(((string)Value).Replace(",", "."), CultureInfo.InvariantCulture);
                }
                return System.Convert.ToDouble(Value);
            }
            catch { };
            Error = true;
            return 0.0;
        }

        public static decimal To_Decimal(object Value, out bool Error)
        {
            Error = false;
            try
            {
                //if (Value is int) return System.Convert.ToDouble((int)Value);
                //if (Value is long) return System.Convert.ToDouble((long)Value);
                //if (Value is bool) return System.Convert.ToDouble((bool)Value);
                //if (Value is double) return (double)Value;
                if (Value is string)
                {
                    if (Value.Equals("")) return 0;
                    return System.Convert.ToDecimal(((string)Value).Replace(",", "."), CultureInfo.InvariantCulture);
                }
                return System.Convert.ToDecimal(Value);
            }
            catch { };
            Error = true;
            return 0;
        }

        public static bool To_Boolean(object Value, out bool Error)
        {
            Error = false;
            try
            {
                if (Value is bool) return (bool)Value;
                //if (Value is int) return ((int)Value != 0);
                //if (Value is long) return ((long)Value != 0);
                //if (Value is bool) return (bool)Value;
                //if (Value is double) return ((double)Value != 0.0);
                if (Value is string)
                {
                    string S_Val = (string)Value;
                    if (S_Val == "") return false;
                    switch (S_Val[0])
                    {
                        case 'f':
                        case 'F':
                        case 'n':
                        case 'N':
                        case '0':
                        case ' ':
                            return false;
                    }
                    return true;
                }
                if (Value is DateTime) return ((DateTime)Value != Time.Null_Time);
                return To_Long(Value) != 0;
            }
            catch { };
            Error = true;
            return false;
        }
        public static bool? To_Boolean_Null(object Value, out bool Error)
        {
            Error = false;
            try
            {
                if (Value is bool) return (bool)Value;
                //if (Value is int) return ((int)Value != 0);
                //if (Value is long) return ((long)Value != 0);
                //if (Value is bool) return (bool)Value;
                //if (Value is double) return ((double)Value != 0.0);
                if (Value is string)
                {
                    string S_Val = (string)Value;
                    if (S_Val == "") return null;
                    switch (S_Val[0])
                    {
                        case 'f':
                        case 'F':
                        case 'n':
                        case 'N':
                        case '0':
                            return false;
                        case ' ':
                            return null;
                    }
                    return true;
                }
                if (Value is DateTime) return ((DateTime)Value != Time.Null_Time);
                return To_Long(Value) != 0;
            }
            catch { };
            Error = true;
            return null;
        }
        /*
                        -- syntax is "YYYY-MM-DD HH:MM:SS.SS...SSSS"
                        --   or      "YYYY-MM-DD HH:MM:SS"
                        --   or      "HH:MM:SS.SS...SSSS"
                        --   or      "HH:MM:SS"
                        --   or      "SS.SSSS....SSS"
                        --   when day is not provided, 1970-01-01 is assumed
                                                                                                                     -- Default value is 1970-01-01 00:00:00
                        function To_Time(Value : in String) return Jst_Time is
                        Seconds  : Jst_Float := 0.0;
                        Year     : Integer  := 2000;
                        Month    : Integer  := 1;
                        Day      : Integer  := 1;
                        begin

                            -- First search the seconds separator, which is always the last ':'
                        for I in reverse Value'range
                        loop
                            if Value(I) = ':'
                        then
                            if I < Value'first+5
                        then
                            return EPOCH;
                        end if;

                        Seconds := To_Float(Value(I+1..Value'last));
                        Seconds := Seconds + 60.0 * To_Float(Value(I-2 .. I-1)); -- Add minutes
                                                                                                                                                    Seconds := Seconds + 3600.0 * To_Float(Value(I-5 .. I-4)); -- Add hours

                                                                                                                                                                                                                                                                                    if I > Value'first +16
                        then
                            Year  := To_Integer(Value(I-16 .. I-13));
                        Month := To_Integer(Value(I-11 .. I-10));
                        Day   := To_Integer(Value(I-8 .. I-7));
                        end if;

                        return Time_Of(Year,Month,Day,Day_Duration(Seconds));

                        end if;
                        end loop;

                        -- If no ':' was found, consider this is a number of seconds
                                                                                                                                Seconds := To_Float(Value);
                        return Y_2000 + Duration(Seconds);

                        exception
                            when others => return Y_2000;
                        end To_Time;

                */

        public static DateTime To_Time (object Value, out bool Error)
		{
			Error = false;
			try
			{
                //if (Value is int) return System.Convert.ToDateTime ((int)Value);
                //if (Value is bool) return System.Convert.ToDateTime ((bool)Value);
                //if (Value is double) return System.Convert.ToDateTime ((double)Value);
                if (Value is DateTime) return (DateTime)Value;
                if (Value is string)
				{
                    if (Value == null) return Time.Null_Time;
                    var Val = ((string)Value).Trim();
                    if (Val.Length < 3) return Time.Null_Time;
                    string[] Textes = ((string)Value).Split ('-', '-', ' ', ':', ':', '.');
                    if (Textes.Length < 3) return Time.Null_Time;
					int Year = 2000;
					int Month = 1;
					int Day = 1;
					int Hours = 0;
					int Minutes = 0;
					int Seconds = 0;
					int Millis = 0;
					Year = Convert.To_Integer(Textes[0]);
					Month = Convert.To_Integer(Textes[1]);
					Day = Convert.To_Integer(Textes[2]);
                    if (Textes.Length > 5)
                    {
                        Hours = Convert.To_Integer(Textes[3]);
                        Minutes = Convert.To_Integer(Textes[4]);
                        Seconds = Convert.To_Integer(Textes[5]);
                    }
					if (Textes.Length > 6 && Textes[6] != "")
					{
						string C_Millis = Textes[6];
						switch (C_Millis.Length)
						{
							case 1:Millis = 100*Convert.To_Integer(C_Millis);break;
							case 2:Millis = 10*Convert.To_Integer(C_Millis);break;
							default:Millis = Convert.To_Integer(C_Millis.Substring(0,3));break;
						};
					};
					return new DateTime (Year, Month, Day, Hours, Minutes, Seconds, Millis);
				};
			}
			catch {};
			Error = true;
			return Time.Null_Time;
		}

		public static string To_String (object Value, out bool Error)
		{
			Error = false;
			try
			{
                //if (Value is int) return ((int)Value).ToString ();
                //if (Value is bool) return ((bool)Value).ToString ();
                //if (Value is string) return (string)Value;
                if (Value is double) return System.Convert.ToString((double)Value, CultureInfo.InvariantCulture);
                if (Value is DateTime)
                {
                    if ((DateTime)Value == Time.Null_Time) return "";
                    return (((DateTime)Value).ToString(Format_Time));
                }
                if (Value is Guid) return (Value.ToString ());
                return Value.ToString();
			}
			catch {};
			Error = true;
			return "";
		}

        // Tente d'interpreter une date selon diff�rents formats propos�s
        public static DateTime To_Format_Date(this string Source, params string [] Formats)
        {
            if (Formats.Length == 0) return To_Time(Source);
            foreach (string Format in Formats)
            {
                DateTime D;
                if (DateTime.TryParseExact(Source, Format, null, DateTimeStyles.None, out D)) return D;
            }
            return DateTime.MinValue;
        }

        public static DataTable To_Table(string Source, bool With_Header = false)
        {
            DataTable DT = new DataTable();
            int Row = 0;
            foreach (string S in JR.Strings.Read_Lines (Source, true))
            {
                int Col = 0;
                if (Row == 0)
                {
                    foreach (string C in JR.Strings.Split(S))
                    {
                        if (With_Header)
                        {
                            DT.Columns.Add(C, typeof (string));
                        }
                        else
                        {
                            DT.Columns.Add("C" + Col.ToString(), typeof(string));
                        }
                        Col++;
                    }
                    if (With_Header) continue;
                }
                DataRow DR = DT.NewRow ();
                Col = 0;
                foreach (string C in JR.Strings.Split(S))
                {
                    DR[Col] = C;
                    Col++;
                }
                DT.Rows.Add(DR);
                Row++;
            }
            return DT;
        }
	}
}
