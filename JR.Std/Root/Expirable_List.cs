using System;
using System.Collections;
using System.Collections.Generic;

namespace JR
{

    public interface I_Expirable
    {
        DateTime Expiration_Date { get; set; }
    }

	/// <summary>
	/// 
	/// </summary>
	public class Expirable_List<K, T> : Dictionary<K,T> where T:I_Expirable
	{
        public TimeSpan Time_Out = new TimeSpan(1, 0, 0);// Une heure
        public Action<K> On_Purge = null;
        public DateTime? Next_Purge = null;
        public void Check_Expired ()
        {
            if (Next_Purge == null) return;
            var Now = DateTime.UtcNow;
            if ((DateTime)Next_Purge > Now) return;
            lock (this)
            {
                Next_Purge = null;
                var Remove_Keys = new List<K>();
                foreach (var Exp in this)
                {
                    if (Exp.Value.Expiration_Date > Now)
                    {
                        if (Next_Purge == null || Exp.Value.Expiration_Date < (DateTime)Next_Purge)
                        {
                            Next_Purge = Exp.Value.Expiration_Date;
                        }
                        continue;
                    }
                    Remove_Keys.Add(Exp.Key);
                }
                foreach (var Exp in Remove_Keys)
                {
                    if (On_Purge == null)
                    {
                        this.Remove(Exp);
                    }
                    else
                    {
                        On_Purge(Exp);
                    }
                }
            }
        }
        public void Append (K Key, T Value, TimeSpan? Time_Out = null)
        {
            var Exp_Time = DateTime.UtcNow.Add(Time_Out == null ? this.Time_Out : (TimeSpan)Time_Out);
            lock (this)
            {
                if (Next_Purge == null)
                {
                    Next_Purge = Exp_Time;
                }
                Value.Expiration_Date = Exp_Time;
                this[Key] = Value;
            }
        }

        public T Get_Value(K Key)
        {
            T Val;
            lock (this)
            {
                if (!TryGetValue(Key, out Val)) return default(T);
                return Val;
            }
        }
        public bool Try_Get_Value<TE>(K Key, out TE Value) where TE : class,T 
        {
            T Val;
            lock (this)
            {
                if (!TryGetValue(Key, out Val))
                {
                    Value = null;
                    return false;
                }
                Value = Val as TE;
                return true;
            }
        }
        public bool Remove_Key(K Key)
        {
            lock (this)
            {
                return Remove(Key);
            }
        }
    }
}
