using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Threading;

namespace JR
{
    public static class Mails
    {
        public static string Get_Type (string File_Name)
        {
            switch (JR.Files.Get_Extension(File_Name).ToUpper())
            {
                case ".JPG":
                case ".JPEG":
                    return MediaTypeNames.Image.Jpeg;
                case ".GIF":
                    return MediaTypeNames.Image.Gif;
                case ".TIFF":
                    return MediaTypeNames.Image.Tiff;
                case ".HTM":
                case ".HTML":
                    return MediaTypeNames.Text.Html;
                case ".XML":
                    return MediaTypeNames.Text.Xml;
                case ".TXT":
                case ".LOG":
                    return MediaTypeNames.Text.Plain;
                case ".ZIP":
                    return MediaTypeNames.Application.Zip;
                case ".PDF":
                    return MediaTypeNames.Application.Pdf;
                case ".RTF":
                    return MediaTypeNames.Application.Rtf;
                case ".XLS":
                    return "application/excel";
            }
            return MediaTypeNames.Application.Octet;
        }

        public static bool Is_Valid_Address(this string Email)
        {
            return Regex.IsMatch(Email, "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");
            //return Regex.IsMatch(Email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,63}|[0-9]{1,3})(\]?)$"); 
        }

    }
}
