using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;

namespace JR
{
    // Effectue un parsing sur la base d'un script de commande
    public class Script_Parser
    {
        string[] _Script = null;
        public Script_Parser(string[] Script)
        {
            _Script = Script;
        }

        Dictionary<string, string> Dic;

        string _Text;
        int From = 0;
        Match M = null;
        void Execute(string Code)
        {
            if (Code.Length < 3) return;
            char Command = char.ToUpper (Code[0]);
            switch (Command)
            {
                case 'F': // Force Key Value
                    {
                        string[] Vals = JR.Strings.Split (Code.Substring(1), 2);
                        Dic[Vals[0]] = Vals[1];
                        break;
                    }
                case 'M': // Match expression
                    {
                        Regex R = new Regex(Code.Substring(2), RegexOptions.IgnoreCase);
                        M = R.Match(_Text, From);
                        if (M.Success)
                        {
                            From = M.Index + M.Length;
                            break;
                        }
                        break;
                    }
                case 'C': // Capture Key
                    {
                        if (M == null) return;
                        if (M.Captures.Count == 0) return;
                        string Key = Code.Substring(2).Trim();
                        string Val = M.Groups[Key].Value.Trim();
                        if (Val == "") return;
                        Dic[Key] = Val;
                        break;
                    }
                case 'S': // Store Key [Prefix] (ne fait rien si existe deja)
                case 'A': // Append Key [Prefix] (append de la valeur)
                case 'R': // Replace Key [Prefix] (�crasement de la valeur m�me si elle existe)
                    {
                        if (M == null) return;
                        if (M.Captures.Count == 0) return;
                        string Val = M.Groups["VAL"].Value.Trim();
                        if (Val == "") return;
                        string Key = Code.Substring(2).Trim();
                        int I_Prefix = Key.IndexOf(' ');
                        if (I_Prefix >= 0)
                        {
                            Val = Code.Substring(I_Prefix + 3) + Val;
                            Key = Key.Substring(0, I_Prefix);
                        }
                        if (Dic.ContainsKey(Key))
                        {
                            if (Command == 'S') return;
                            if (Command == 'R')
                            {
                                Dic[Key] = Val;
                            }
                            else
                            {
                                Dic[Key] += ("\r\n" + Val);
                            }
                        }
                        else
                        {
                            Dic[Key] = Val;
                            // On oublie la valeur en mode store
                            if (Command == 'S') M = null;
                        }
                        break;
                    }
            }
        }

        public Dictionary<string, string> Analyse(string Text)
        {
            Dic = new Dictionary<string, string>();
            _Text = Text;
            foreach (string Code in _Script)
            {
                Execute(Code);
            }
            return Dic;
        }

    }
}
