﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace JR
{
    public static class Files
    {

        public static string Mime_String(string File_Name)
        {
            switch (JR.Files.Get_Extension(File_Name).ToLower())
            {
                case ".pdf":
                    return "application/pdf";
                case ".json":
                    return "application/json";
                case ".txt":
                    return "text/plain";
                case ".jpg":
                    return "image/jpeg";
                case ".gif":
                    return "image/gif";
                case ".png":
                    return "image/png";
                case ".zip":
                    return "application/zip";
                default:
                    return "application/octet-stream";
            }
        }
        public static bool Check_Directory(string File)
        {
            try
            {
                string D_Name = Get_Directory_Name(File);
                if (!System.IO.Directory.Exists(D_Name))
                {
                    System.IO.Directory.CreateDirectory(D_Name);
                }
                return System.IO.Directory.Exists(D_Name);

            }
            catch { return false; }
        }

        public static bool Check_Directory(string File, out bool Created)
        {
            Created = false;
            string D_Name = Get_Directory_Name(File);
            if (!System.IO.Directory.Exists(D_Name))
            {
                Created = true;
                System.IO.Directory.CreateDirectory(D_Name);
            }
            return System.IO.Directory.Exists(D_Name);
        }

        // Lecture d'un fichier en un seul appel
        public static string[] Read_File(string File_Name, Encoding Encoding = null)
        {
            if (System.IO.File.Exists(File_Name))
            {
                try
                {
                    return System.IO.File.ReadAllLines(File_Name, Encoding ?? Encoding.Default);
                }
                catch { }
            }
            return new string[0];
        }

        // Retourne un nom de fichier normalisé (sans accent ni autres bizarreries et tout en minuscules)
        const string Accent = "àáâãäåòóôõöøèéêëìíîïùúûüÿñç";
        const string Lower = "aaaaaaooooooeeeeiiiiuuuuync";
        public static string Normalized(string File_Name, bool Preserve_Slash = false, string Preserve = null)
        {
            char[] New_Name = File_Name.ToLower().Trim().ToCharArray();
            int Row = 0;
            void Norm(char C)
            {
                if (char.IsLetterOrDigit(C))
                {
                    int S_Row = Accent.IndexOf(C);
                    if (S_Row >= 0) New_Name[Row] = Lower[S_Row];
                    return;
                }
                switch (C)
                {
                    case '.':
                        return;
                    case '/':
                        New_Name[Row] = Preserve_Slash ? '\\' : '_';
                        return;
                    case '\\':
                        if (Preserve_Slash) return;
                        break;
                    default:
                        if (Preserve != null && Preserve.IndexOf(C) >= 0) return;
                        break;
                }
                New_Name[Row] = '_';
            }
            foreach (char C in New_Name)
            {
                Norm(C);
                Row++;
            }
            return new string(New_Name);
        }

        public static long Safe_Size(string Source)
        {
            try
            {
                return new FileInfo(Source).Length;
            }
            catch { }
            return 0;
        }

        public static DateTime Safe_Date(string Source, bool Creation_Date = false)
        {
            try
            {
                if (Creation_Date) return new FileInfo(Source).CreationTime;
                return new FileInfo(Source).LastWriteTime;
            }
            catch { }
            return DateTime.MinValue;
        }

        public static bool Force_Date(string Source, DateTime Date, bool Creation_Date = false)
        {
            try
            {
                if (Creation_Date)
                {
                    new FileInfo(Source).CreationTime = Date;
                }
                else
                {
                    new FileInfo(Source).LastWriteTime = Date;
                }
                return true;
            }
            catch { }
            return false;
        }

        public static bool Safe_Move(string Source, string Dest)
        {
            try
            {
                if (!File.Exists(Source)) return false;
                if (!Check_Directory(Dest)) return false;
                File.Delete(Dest);
                File.Move(Source, Dest);
                return true;
            }
            catch { }
            return false;
        }

        public static bool Safe_Move_Dir(string Source, string Dest)
        {
            try
            {
                if (!Directory.Exists(Source)) return false;
                Directory.Move(Source, Dest);
                return true;
            }
            catch { }
            return false;
        }

        public static bool Safe_Remove_Dir(string Source)
        {
            try
            {
                if (!Directory.Exists(Source)) return false;
                Directory.Delete(Source, true);
                return true;
            }
            catch { }
            return false;
        }

        public static bool Is_Empty(string Dir)
        {
            try
            {
                var DI = new DirectoryInfo(Dir);
                if (!DI.Exists) return true;
                foreach (var F in DI.EnumerateFiles("*", SearchOption.AllDirectories))
                {
                    return false;
                }
                return true;
            }
            catch { }
            return true;
        }

        public static bool Safe_Copy(string Source, string Dest)
        {
            try
            {
                bool Is_Local = !Source.ToUpper().Contains("TP:");
                if (Is_Local && !File.Exists(Source)) return false;
                if (!Check_Directory(Dest)) return false;
                File.Delete(Dest);
                if (Is_Local)
                {
                    File.Copy(Source, Dest);
                }
                else
                {
                    using (System.Net.WebClient C = new System.Net.WebClient())
                    {
                        C.DownloadFile(Source, Dest);
                    }
                }
                return true;
            }
            catch { }
            return false;
        }

        public static bool Safe_Append(string Dest, string[] Content, Encoding Encoding = null)
        {
            try
            {
                if (!Check_Directory(Dest)) return false;
                File.AppendAllLines(Dest, Content, Encoding ?? Encoding.Default);
                return true;
            }
            catch { }
            return false;
        }

        public static bool Safe_Append(string Dest, string Content, Encoding Encoding = null)
        {
            try
            {
                if (!Check_Directory(Dest)) return false;
                File.AppendAllText(Dest, Content, Encoding ?? Encoding.Default);
                return true;
            }
            catch { }
            return false;
        }

        public static bool Safe_Write(string Dest, string[] Content, Encoding Encoding = null)
        {
            try
            {
                if (!Check_Directory(Dest)) return false;
                File.Delete(Dest);
                File.WriteAllLines(Dest, Content, Encoding ?? Encoding.Default);
                return true;
            }
            catch { }
            return false;
        }

        public static bool Safe_Write(string Dest, string Content, Encoding Encoding = null)
        {
            try
            {
                if (!Check_Directory(Dest)) return false;
                File.Delete(Dest);
                File.WriteAllText(Dest, Content, Encoding ?? Encoding.Default);
                return true;
            }
            catch { }
            return false;
        }

        public static bool Safe_Delete(string Source)
        {
            try
            {
                if (!File.Exists(Source)) return false;
                File.Delete(Source);
                return true;
            }
            catch { }
            return false;
        }

        public static string Safe_Read_All(string Source, Encoding Encoding = null)
        {
            var Lines = Safe_Read(Source, false, Encoding);
            return Lines.Count == 0 ? "" : Lines[0];
        }

        public static IList<string> Safe_Read(string Source, bool Read_Lines = true, Encoding Encoding = null)
        {
            try
            {
                if (File.Exists(Source))
                {
                    if (Read_Lines)
                    {
                        return File.ReadAllLines(Source, Encoding ?? Encoding.Default);
                    }
                    else
                    {
                        return new string[] { File.ReadAllText(Source, Encoding ?? Encoding.Default) };
                    }
                }
            }
            catch { }
            return new string[0];
        }

        public static IList<string> Safe_Dir(string File_Path, bool Recurse = false)
        {
            List<string> L = new List<string>();
            try
            {
                var P = File_Path.Paths();
                string D_Name = P.Directory;
                if (!System.IO.Directory.Exists(D_Name)) return L;
                string Filter = P.File;
                foreach (string F in Directory.GetFiles(D_Name, Filter, Recurse ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly))
                {
                    L.Add(F);
                }
            }
            catch { }
            return L;
        }


        // Liste les fichiers d'un répertoire
        public static List<string> Simple_Dir(string File_Path)
        {
            List<string> L = new List<string>();
            var P = File_Path.Paths();
            string D_Name = P.Directory;
            if (!System.IO.Directory.Exists(D_Name)) return L;
            string Filter = P.File;
            foreach (string F in Directory.GetFiles(D_Name, Filter))
            {
                L.Add(Get_File_Name(F));
            }
            return L;
        }


        public static byte[] Full_Stream(Stream Stream)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    Stream.CopyTo(ms);
                    return ms.ToArray();
                }
            }
            catch { }
            return new byte[0];
        }

        public static byte[] Full_File(string File_Name)
        {

            if (System.IO.File.Exists(File_Name))
            {
                try
                {
                    return System.IO.File.ReadAllBytes(File_Name);
                }
                catch { }
            }
            return new byte[0];
        }

        public class Path_Split
        {
            public string Directory;
            public string File;
            public string Extension;
            public string File_Prefix;
            public string Directory_URL
            {
                get
                {
                    return System.Web.HttpUtility.UrlEncode(Directory);
                }
            }

            public Path_Split(string File_Name)
            {
                int I = File_Name.LastIndexOfAny(Dir_Seps);
                if (I == -1)
                {
                    Directory = "";
                    File = File_Name;
                }
                else
                {
                    Directory = File_Name.Substring(0, I);
                    File = File_Name.Substring(I + 1);
                }
                I = File.LastIndexOf('.');
                if (I == -1)
                {
                    Extension = "";
                    File_Prefix = File;
                }
                else
                {
                    File_Prefix = File.Substring(0, I);
                    Extension = File.Substring(I);
                }
            }
        }

        public static Path_Split Paths(this string File_Name)
        {
            return new Path_Split(File_Name);
        }

        static char[] Dir_Seps = @"/\".ToCharArray();

        // Utile car Path.GetDirectoryName plante si on passe un chemin de type file: ou #SPACE:
        public static string Get_Directory_Name(string File_Name)
        {
            int I = File_Name.LastIndexOfAny(Dir_Seps);
            if (I == -1) return "";
            return File_Name.Substring(0, I);
        }
        public static string Get_File_Name(this string File_Name)
        {
            int I = File_Name.LastIndexOfAny(Dir_Seps);
            if (I == -1) return File_Name;
            return File_Name.Substring(I + 1);
        }
        public static string Get_Extension(this string File_Name)
        {
            string File = "";
            int I = File_Name.LastIndexOfAny(Dir_Seps);
            File = File_Name.Substring(I + 1);
            I = File.LastIndexOf('.');
            if (I == -1)
            {
                return "";
            }
            else
            {
                return File.Substring(I);
            }
        }
    }

}
