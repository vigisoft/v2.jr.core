using System;
using System.IO;
using System.Data;
using System.Collections;
using System.Diagnostics;
using System.Xml;

namespace JR
{

    /// <summary>
    /// C# implementation of ADA JMD
    /// </summary>
    public class Jmd
    #region body
    {

        public delegate void On_Load(Jmd_Object Object);

        public static Jmd_Object Get_Object(string Name)
        // Retrieves the object knowing its name
        {
            #region body
            return (Jmd_Object)Public_Objects.Item(Name);
            #endregion
        }

        public static Jmd_Object New_Object(string Name, string Nature)
        //	Create a new object
        //	if Name is "", a anonymous object is created (doesn't appear in global list)
        {
            #region body
            Jmd_Type Typ = Get_Internal_Type(Nature);
            if (Typ == null) return null;
            return New_Object(Name, Typ);
            #endregion
        }

        public static Jmd_Object New_Object(string Name, Jmd_Type Nature)
        //	Create a new object
        //	if Name is "", a anonymous object is created (doesn't appear in global list)
        {
            #region body
            bool Public_Object = (Name != "");
            Jmd_Object Obj = null;

            Jmd_Type Typ = Nature;
            if (Typ == null) return null;

            // Try to find an existing name in public list
            if (Public_Object)
            {
                Obj = Get_Object(Name);
                if (Obj != null)
                {
                    if (Obj.Nature != Typ)
                    {
                        return null; // Error : Incompatible type
                    };
                    return Obj;
                };
            };

            // Create a new empty object
            Obj = new Jmd_Object(Name, Typ);

            if (Public_Object)
            {
                Public_Objects.Add(Name, Obj);
            }
            else
            {
                //Public_Objects.Add(Strings.New_Guid(), Obj);      // ne semble pas necessaire et evite de g�rer le delete
            };

            return Obj;
            #endregion
        }

        public static Jmd_Type Get_Type(string Name)
        // return existing type by name
        {
            #region body
            return (Jmd_Type)Public_Types.Item(Name);
            #endregion
        }

        public static Jmd_Type New_Type(string Name, string Description)
        // return new type from name and definition
        {
            #region body
            Jmd_Type Typ = null;
            bool Public_Type = (Name != "");

            // -- Try to find an existing name in public list
            if (Public_Type)
            {
                Typ = Get_Type(Name);
                if (Typ != null)
                {
                    if (!JR.Strings.Like(Typ.Definition, Description))
                    {
                        Debug.WriteLine($"Invalid type redefinition (name={Name})-(definition={Description})");
                        return null; // -- Error : Incompatible type
                    }
                    else
                    {
                        return Typ;
                    }
                }

            }

            // Analyse and create a new type
            Type_Builder Builder = new Type_Builder();
            Typ = Builder.Get_Type(Description);

            if (Typ == null)
            {
                Debug.WriteLine("Invalid type (name={Name})-(definition={Description})");
                return null;
            };
            Typ.Public = Public_Type;
            Typ.Definition = Description;

            // Add to public types list
            if (Public_Type) Public_Types.Add(Name, Typ);
            return Typ;
            #endregion
        }


        public static void Delete(ref Jmd_Type Type)
        // Removes a type from public list
        {
            #region body
            if (Type == null) return;
            if (Type.Public)
            {
                Public_Types.Remove(Type);
            };
            Type = null;

            #endregion
        }

        public static void Delete(ref Jmd_Object Object)
        // Removes an object from public list
        {
            #region body
            if (Object == null) return;
            if (Object.Public)
            {
                Public_Objects.Remove(Object);
            }
            else
            {
                // Public_Objects.Remove(Object);       // ne semble plus necessaire
            };
            Object = null;
            #endregion
        }

        public static Jmd_Cursor Cursor(string Expression, bool Read_Only)
        // return a cursor associated with this public expression
        {
            #region body
            Jmd_Cursor Curs = new Jmd_Cursor();
            Curs.Point(Expression, Read_Only);
            return Curs;
            #endregion
        }

        public static Jmd_Cursor Cursor(string Expression)
        // return a read only cursor associated with this public expression
        {
            #region body
            return Cursor(Expression, true);
            #endregion
        }

        public static bool Save(string File, params object[] Objects)
        // Save list of Jmd_Object and Jmd_Type to a file
        //  return operation success
        {
            #region body
            try
            {
                XmlTextWriter Writer = new XmlTextWriter(File, System.Text.Encoding.UTF8);
                Save(Objects, Writer);
                Writer.Close();
                return true;
            }
            catch
            {
                return false;
            };
            #endregion
        }

        public static string To_Xml(object[] Objects)
        // Save list of Jmd_Object and Jmd_Type to a XML string
        {
            #region body
            try
            {
                using (MemoryStream File = new MemoryStream())
                {
                    XmlTextWriter Writer = new XmlTextWriter(File, System.Text.Encoding.UTF8);
                    Save(Objects, Writer);
                    Writer.Close();
                    Byte[] Bytes = File.ToArray();
                    return System.Text.Encoding.UTF8.GetString(Bytes, 3, Bytes.Length-3);
                    //Char[] Buffer = new Char[Bytes.Length - 3]; // First three chars are strange
                    //for (int I = 3; I < Bytes.Length; I++)
                    //{
                    //    Buffer[I - 3] = System.Convert.ToChar(Bytes[I]);
                    //};
                    //return new string(Buffer);
                }
            }
            catch
            {
                return "";
            };
            #endregion
        }

        public static bool Load(string File)
        // Load objects and types from a file
        // If File begins with < and ends with a >, it is assumed to be an XML string
        {
            #region body
            return Load(File, null);
            #endregion
        }

        class Load_Notify
        {
            public Jmd_Object Local_Object;
            public Load_Notify()
            {
            }

            public void On_Load(Jmd_Object Object)
            {
                Local_Object = Object;
            }

        }
        public static Jmd_Object Load_Object(string File)
        // Load the first object from a file
        // If File begins with < and ends with a >, it is assumed to be an XML string
        {
            #region body
            Load_Notify Notifier = new Load_Notify();
            if (Load(File, new On_Load(Notifier.On_Load))) return Notifier.Local_Object;
            return null;
            #endregion
        }
        public static bool Load(string File, On_Load Notify)
        // Load objects and types from a file
        // If File begins with < and ends with a >, it is assumed to be an XML string
        {
            #region body

            try
            {
                XmlTextReader Reader;
                if (File.StartsWith("<") && File.EndsWith(">"))
                {
                    Reader = new XmlTextReader(File, XmlNodeType.Document, null);
                }
                else
                {
                    if (!System.IO.File.Exists(File)) return false;
                    Reader = new XmlTextReader(File);
                };

                int Level = 0;
                Jmd_Cursor Cursor = null;
                int Version = 0;

                Level_Info[] Infos = new Level_Info[100];
                Jmd_Object Current_Object = null;
                Jmd_Type Current_Type = null;

                while (Reader.Read())
                {
                    switch (Reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            Level++;

                            Infos[Level].In_Array = false;
                            Infos[Level].Next_Row = 1;

                            if (Level == 1)
                            {
                                if (Reader.LocalName == "JMD")
                                {
                                    Version = int.Parse(Reader.GetAttribute("V"));
                                };
                            }
                            else if (Level == 2)
                            {

                                if (Reader.LocalName == "OBJECT")
                                {
                                    string Object_Name = Reader.GetAttribute("NAME");
                                    string Object_Type = Reader.GetAttribute("TYPE");
                                    Current_Object = Jmd.New_Object(Object_Name, Object_Type);
                                    Cursor = Current_Object.Cursor(false);
                                }
                                else if (Reader.LocalName == "TYPE")
                                {
                                    string Type_Name = Reader.GetAttribute("NAME");
                                    string Type_Def = Reader.ReadString();
                                    Current_Type = Jmd.New_Type(Type_Name, Type_Def);
                                    Level--;

                                };

                            }
                            else if (Level > 2)
                            {
                                // Apply the tag
                                switch (Cursor.Base_Type.Root_Type)
                                {
                                    case Jmd_Root.Record:
                                        Cursor.Step_Into(Reader.LocalName);
                                        break;
                                    case Jmd_Root.Value:
                                        Cursor.Define(Jmd.Get_Type(Reader.GetAttribute("TYPE")));
                                        Cursor.Step_Into();
                                        break;
                                    case Jmd_Root.Array:
                                        if (Reader.LocalName == "ARRAY" || Reader.LocalName == "A")
                                        {
                                            string Size = Reader.GetAttribute("SIZE");
                                            if (Size != null) Cursor.Presize(int.Parse(Size));
                                            Infos[Level].In_Array = true;
                                            Infos[Level].Next_Row = 1;
                                        }
                                        else if (Reader.LocalName == "ITEM" || Reader.LocalName == "I")
                                        {
                                            int Row = Infos[Level - 1].Next_Row;
                                            if (Reader.HasAttributes) Row = int.Parse(Reader.GetAttribute("ROW"));
                                            Cursor.Step_Into(Row);
                                            Infos[Level - 1].Next_Row = Row + 1;
                                        };
                                        break;
                                };
                                if (Reader.IsEmptyElement)
                                {
                                    goto case XmlNodeType.EndElement;
                                };
                            };
                            break;
                        case XmlNodeType.Text:
                            if (Level >= 2)
                            {
                                Cursor.Write_Text(Reader.Value);
                            };
                            break;
                        case XmlNodeType.EndElement:
                            if (Level == 2)
                            {
                                if (Notify != null)
                                {
                                    Notify(Current_Object);
                                };
                                Current_Object = null;
                            }
                            else if (Level > 2)
                            {
                                if (Infos[Level].In_Array)
                                {
                                    Infos[Level].In_Array = false;
                                }
                                else
                                {
                                    Cursor.Step_Out();
                                };
                            };
                            Level--;
                            break;
                        default:
                            break;
                    };

                };

                //Close the reader.
                Reader.Close();
                return true;

            }
            catch
            {
                return false;
            };

            #endregion
        }

        #region Internals
        #region Declarations
        internal static Jmd_Type[] Root_Types = new Jmd_Type[Jmd_Root.Size];
        internal static Named_List Public_Types = new Named_List();
        internal static Named_List Public_Objects = new Named_List();
        #endregion

        private class Type_Builder
        {
            #region body
            int Nature;
            private string Description;
            private int Index = 0;
            private int Tail;

            public Type_Builder()
            {
            }

            private string Next_Type()
            {
                int Start = Index;
                int Level = 0;

                for (int I = Index; I <= Tail; I++)
                {
                    switch (Description[I])
                    {
                        case ' ':
                            if (Level == 0)
                            {
                                Index = I + 1;
                                return Description.Substring(Start, I - Start);
                            };
                            break;
                        case '[':
                            Level++;
                            break;
                        case ']':
                            Level--;
                            break;
                        default:
                            break;
                    }
                }
                Index = Tail + 1;
                return Description.Substring(Start, Tail - Start + 1);
            }

            private string Next_Word()
            {
                int Start = Index;
                for (int I = Index; I <= Tail; I++)
                {
                    if (Description[I] == ' ')
                    {
                        Index = I + 1;
                        return Description.Substring(Start, I - Start);
                    };
                }
                Index = Tail + 1;
                return Description.Substring(Start, Tail - Start + 1);
            }

            private string Next_Colon()
            {
                int Start = Index;
                for (int I = Index; I <= Tail; I++)
                {
                    if (Description[I] == ':')
                    {
                        Index = I + 1;
                        return Description.Substring(Start, I - Start);
                    };
                }
                Index = Tail + 1;
                return "";
            }

            // Number of unread character remaining
            private int Rest()
            {
                return (Tail - Index) + 1;
            }

            private Jmd_Type Typ = null;

            private void Add_Enum(int Enum_Row)
            {
                if (Rest() == 0)
                {
                    Typ = new Jmd_Type(Nature, Enum_Row);
                }
                else
                {
                    string Enum = this.Next_Word();
                    this.Add_Enum(Enum_Row + 1);
                    Typ.Enums[Enum_Row] = Enum;
                };
            }

            private bool Add_Component(int Comp_Row)
            {
                if (Rest() == 0)
                {
                    Typ = new Jmd_Type(Nature, Comp_Row);
                    return true; // Success
                };
                string Comp_Name = this.Next_Colon().ToUpper();
                string Comp_Type = this.Next_Type().ToUpper();
                Jmd_Type Sub_Type;

                if (!this.Add_Component(Comp_Row + 1)) return false; // Error
                if (Comp_Name == "" || Comp_Type == "") return false; // Error
                Sub_Type = Get_Internal_Type(Comp_Type);
                if (Sub_Type == null) return false; // Error
                Typ.Components[Comp_Row].Name = Comp_Name;
                Typ.Components[Comp_Row].Sub_Type = Sub_Type;
                return true; // Success
            }

            public Jmd_Type Get_Type(string Type_Description)
            {
                Description = Type_Description.ToUpper();
                Tail = Description.Length - 1;
                if (Description.Length == 0) return null;
                switch (Description[Index])
                {
                    case 'I': Nature = Jmd_Root.Integer; break;
                    case 'F': Nature = Jmd_Root.Float; break;
                    case 'T': Nature = Jmd_Root.Time; break;
                    case 'B': Nature = Jmd_Root.Boolean; break;
                    case 'E': Nature = Jmd_Root.Enumeration; break;
                    case 'S': Nature = Jmd_Root.String; break;
                    case 'R': Nature = Jmd_Root.Record; break;
                    case 'O': Nature = Jmd_Root.Object; break;
                    case 'V': Nature = Jmd_Root.Value; break;
                    case 'A': Nature = Jmd_Root.Array; break;
                    default: return null;
                };
                Index = Index + 2; // Skip Nature + one space

                switch (Nature)
                {
                    case Jmd_Root.Enumeration:
                        if (Rest() < 1) return null;
                        this.Add_Enum(0);
                        return this.Typ;

                    case Jmd_Root.Record:
                        if (Rest() < 1) return null;
                        Jmd_Type Parent_Type = null;
                        int Parent_Size = 0;

                        // detect if is an extension of an existing record
                        if (Description[Index] == '+')
                        {
                            Index++;
                            string Parent_Type_Name = this.Next_Type().ToUpper();

                            Parent_Type = Get_Type(Parent_Type_Name);
                            if (Parent_Type == null || Parent_Type.Root_Type != Jmd_Root.Record)
                            {
                                return null;
                            }
                            else
                            {
                                Parent_Size = Parent_Type.Size;
                            };
                        }

                        if (this.Add_Component(Parent_Size))
                        {
                            // copy parent components
                            if (Parent_Type != null)
                            {
                                Typ.Sub_Type = Parent_Type;
                                Parent_Type.Components.CopyTo(Typ.Components, 0);
                            }
                        }

                        return this.Typ;

                    case Jmd_Root.Array:
                        if (Rest() < 1) return null;

                        string Subtype_Name = this.Next_Type();
                        Jmd_Type Sub_Type;
                        if (Rest() > 0) return null;
                        Sub_Type = Get_Internal_Type(Subtype_Name);
                        if (Sub_Type == null) return null;
                        Typ = new Jmd_Type(Nature, 0);
                        Typ.Sub_Type = Sub_Type;
                        return this.Typ;

                    default:
                        if (Rest() > 0) return null;
                        return new Jmd_Type(Nature, 0);

                };

            }
            #endregion
        }

        private struct Level_Info
        {
            public bool In_Array;
            public int Next_Row;
        };

        internal static Jmd_Type Get_Internal_Type(string Name)
        {
            Jmd_Type Typ;
            if (Name.Length > 2 && Name[0] == '[' && Name[Name.Length - 1] == ']')
            {
                Typ = Jmd.New_Type("", Name.Substring(1, Name.Length - 2));
                if (Typ != null) Typ.Public = false;
            }
            else if (Name.IndexOf(' ') > 0)
            {
                Typ = Jmd.New_Type("", Name);
                if (Typ != null) Typ.Public = false;
            }
            else
            {
                Typ = (Jmd_Type)(Public_Types.Item(Name));
            };
            return Typ;
        }

        private static bool Save(object[] Objects, XmlTextWriter Writer)
        {
            #region body
            Writer.WriteStartDocument();
            Writer.WriteStartElement("JMD");
            Writer.WriteAttributeString("V", "1");

            foreach (object Object in Objects)
            {
                if (Object.GetType() == typeof(Jmd_Type))
                {
                    ((Jmd_Type)Object).Save(Writer);
                };
            };

            foreach (object Object in Objects)
            {
                if (Object.GetType() == typeof(Jmd_Object))
                {
                    ((Jmd_Object)Object).Save(Writer);
                };
            };

            Writer.WriteEndElement();
            Writer.WriteEndDocument();
            Writer.Close();
            return true;

            #endregion
        }
        public static Jmd_Type Meta_Type = null;
        private const string Meta_Definition =
        "R NAME:S DEFINITION:S DOC:S TYPE:[E ? I B F T E S R V O A] COMPONENTS:[A [R NAME:S DOC:S TYPE:V]]";

        private static bool Initialization()
        {
            // Perform initializations
            // Create default values
            // Create root types

            //Console.Write("Initialisation ... ");
            Root_Types[Jmd_Root.Integer] = New_Type("I", "I");
            Root_Types[Jmd_Root.Boolean] = New_Type("B", "B");
            Root_Types[Jmd_Root.Float] = New_Type("F", "F");
            Root_Types[Jmd_Root.Time] = New_Type("T", "T");
            Root_Types[Jmd_Root.String] = New_Type("S", "S");
            Root_Types[Jmd_Root.Value] = New_Type("V", "V");
            Root_Types[Jmd_Root.Object] = New_Type("O", "O");
            Meta_Type = New_Type("M", Meta_Definition);

            //Console.Write("Initialisation [Ok]");

            return true;
        }

        private static bool Initialized = Jmd.Initialization();
        #endregion

    } // end Jmd
    #endregion Jmd

    public class Jmd_Type : I_Named
    #region body
    {

        public Jmd_Object Describe(string Result_Name)
        // Returns a new meta model object describing the type
        {
            #region body
            return null;
            #endregion
        }

        public string External_Name()
        {
            #region body
            if (Public) return Name;
            return Definition;
            #endregion
        }

        #region Internals

        internal struct Ty_Component
        {
            public string Name;
            //public string Documentation;
            public Jmd_Type Sub_Type;
        }

        internal int Root_Type;
        //internal int Nature;
        internal int Size;
        internal string Documentation = "";
        internal string Definition = "";
        internal bool Public = false;
        internal Jmd_Type Sub_Type = null;
        internal string[] Enums;
        internal Ty_Component[] Components;
        internal string Nom;

        public string Name
        {
            get { return this.Nom; }
            set { this.Nom = value; }
        }

        internal Jmd_Type(int Nature, int Size)
        {
            this.Root_Type = Nature;
            this.Size = Size;
            if (Nature == Jmd_Root.Enumeration)
            {
                Enums = new string[Size];
            }
            else if (Nature == Jmd_Root.Record)
            {
                Components = new Ty_Component[Size];
            }
        }

        internal void Save(XmlTextWriter Writer)
        {
            #region body

            Writer.WriteStartElement("TYPE");
            Writer.WriteAttributeString("NAME", this.External_Name());
            Writer.WriteString(this.Definition);
            Writer.WriteEndElement();

            #endregion
        }

        #endregion

    } // end Jmd_Type
    #endregion Jmd_Type

    public class Jmd_Object : I_Named
    #region body
    {

        public string Public_Name()
        // return the public name of the object
        {
            #region body
            return Nom_Public;
            #endregion
        }

        public string Physical_Name()
        // return the physical name of the object
        {
            #region body
            return Nom;
            #endregion
        }

        public Jmd_Cursor Cursor(bool Read_Only)
        // return a cursor associated with this object
        {
            #region body
            Jmd_Cursor Curs = new Jmd_Cursor();
            Curs.Point(this, Read_Only);
            return Curs;
            #endregion
        }

        public Jmd_Cursor Cursor()
        // return a read only cursor associated with this object
        {
            #region body
            return this.Cursor(true);
            #endregion
        }

        public bool Save(string File)
        // Save an object to a file
        {
            #region body
            return Jmd.Save(File, this);
            #endregion
        }

        public string To_Xml()
        // Save an object to a XML string
        {
            #region body
            return Jmd.To_Xml(new Jmd_Object[] { this });
            #endregion
        }

        public void Reset()
        {
            #region body
            this.Value.Value = null;
            #endregion
        }

        #region Internals
        internal string Nom;
        internal string Nom_Public;
        internal bool Public;
        internal Jmd_Value Value = null;
        public Jmd_Type Nature;

        public string Name
        {
            get { return this.Nom; }
            set { this.Nom = value; }
        }

        public Jmd_Object(string Name, Jmd_Type Nature)
        {
            this.Nom_Public = Name;
            this.Nature = Nature;
            this.Public = (Name != "");
            this.Value = new Jmd_Value(Nature);
        }

        internal void Save(XmlTextWriter Writer)
        {
            #region body

            Writer.WriteStartElement("OBJECT");
            Writer.WriteAttributeString("NAME", this.Public_Name());
            Writer.WriteAttributeString("TYPE", this.Nature.External_Name());
            if (this.Value.Value != null) this.Value.Save(Writer);
            Writer.WriteEndElement();

            #endregion
        }

        #endregion

    } // end Jmd_Object
    #endregion

    public class Jmd_Cursor
    #region body
    {

        public void Step_Out()
        {
            #region body
            if (this.Level > 1)
            {
                this.Level--;
                this.In_Error = false;
            }
            else
            {
                this.In_Error = true;
            };
            #endregion
        }

        public void Step_Into(string Tag)
        {
            #region body
            if (Level == 0 || Tag == "") { In_Error = true; return; };
            // Localize the tag
            Jmd_Value Value = Values[Level];
            int Row = 0;
            if (Value != null)
            {
                if (Value.Root_Type == Jmd_Root.Record)
                {
                    int I = 0;
                    foreach (Jmd_Type.Ty_Component Component in Value.Nature.Components)
                    {
                        I++;
                        if (Strings.Like(Tag, Component.Name))
                        {
                            Row = I;
                            break;
                        }
                    };
                }
                else if (Value.Root_Type == Jmd_Root.Array)
                {
                    Row = int.Parse(Tag);
                };
            };

            Level++;
            Path[Level] = Row;
            Values[Level] = null;
            Check();
            #endregion
        }

        public void Step_Into(int Raw)
        {
            #region body
            if (Level == 0 || Raw == 0) { In_Error = true; return; };
            Level++;
            Values[Level] = null;
            Path[Level] = Raw;
            Check();
            #endregion
        }

        public void Step_Into()
        {
            #region body
            if (Level == 0) { In_Error = true; return; };
            Level++;
            Path[Level] = 1;
            Values[Level] = null;
            this.Check();
            #endregion
        }

        public void Step()
        {
            #region body
            if (Level == 0) { In_Error = true; return; };
            Path[Level]++;
            this.Check();
            #endregion
        }

        public void Jump_To(int Row)
        {
            #region body
            if (Level == 0) { In_Error = true; return; };
            Path[Level] = Row;
            this.Check();
            #endregion
        }

        public void Jump_To(string Tag)
        {
            #region body
            In_Error = true;
            if (Level == 0) return;
            Jmd_Value Parent_Value = Values[Level - 1];
            Path[Level] = 0;
            if (Parent_Value != null && Parent_Value.Root_Type == Jmd_Root.Record)
            {
                int Row = 0;
                foreach (Jmd_Type.Ty_Component Component in Parent_Value.Nature.Components)
                {
                    Row++;
                    if (Strings.Like(Tag, Component.Name))
                    {
                        Path[Level] = Row;
                        this.Check();
                        break;
                    }
                };
            };
            #endregion
        }

        public Jmd_Object Top_Object => Object;

        public char Root_Type
        // returns the nature of the value pointed by the cursor
        //   when 'I'    => Type_Integer
        //   when 'V'    => Type_Value
        //   when 'F'    => Type_Float
        //	 when 'B'    => Type_Boolean
        //   when 'E'    => Type_Enumeration
        //	 when 'S'    => Type_String
        //   when 'R'    => Type_Record
        //   when 'O'    => Type_Object
        //   when 'A'    => Type_Array
        //   when 'T'    => Type_Time
        //   when '?'    => Error, not a valid value
        {
            #region body
            get
            {
                const string Letter = "IBFTESRVOA";
                if (Values[Level] == null) return '?';
                return Letter[Values[Level].Root_Type];
            }
            #endregion
        }

        public Jmd_Type Base_Type
        // returns the internal type of the value pointed by the cursor
        {
            #region body
            get
            {
                if (Values[Level] == null) return null;
                return Values[Level].Nature;
            }
            #endregion
        }

        public string Base_Name
        // Returns external name of the type of the value pointed by the cursor
        {
            #region body
            get
            {
                if (Values[Level] == null) return "";
                return Values[Level].Nature.External_Name();
            }
            #endregion
        }


        internal string Name
        // Returns the name of the component pointed by the cursor
        {
            #region body
            get
            {
                return Component_Name(this.Level);
            }
            #endregion
        }

        public string Full_Name
        // Returns the full path name of the value pointed by the cursor
        //  ie : Object.Component.Component...Component
        {
            #region body
            get
            {
                string Name = Component_Name(1);
                for (int I = 2; I <= Level; I++)
                {
                    Name += ("." + Component_Name(I));
                };
                return Name;
            }
            #endregion
        }

        public bool Error
        // Indicates an error in previous cursor operation
        {
            #region body
            get
            {
                return this.In_Error;
            }
            #endregion
        }

        public bool End
        // Tells if the cursor points a valid value
        {
            #region body
            get
            {
                return Values[Level] == null;
            }
            #endregion
        }

        public int Size
        //   Gives the number of sub items pointed by the cursor
        //  ==> number of components if a record is pointed
        //  ==> Size of table if an array is pointed
        {
            #region body
            get
            {
                Jmd_Value V = Values[Level];
                if (V == null) return 0;
                if (V.Value == null) return 0;
                switch (V.Root_Type)
                {
                    case Jmd_Root.Record:
                        return V.Nature.Size;
                    case Jmd_Root.Array:
                        return ((ArrayList)(V.Value)).Count;
                    default:
                        return 1;
                };
            }
            #endregion
        }


        // 
        //Returns the value pointed by the cursor, completed by Raw (if /= 0) and sub_expression (if /= "")
        // ==> equivalent to Cursor[(RAW)][.Sub_Expression]
        // The first param is the subexpression
        // The second is the raw

        public bool Undefined_Value(params object[] Expression_Row)
        {
            #region body
            Jmd_Value V = Zoom(Expression_Row);
            if (V == null) return true;
            return (V.Value == null);
            #endregion
        }

        object Default_Value(int Root_Type)
        {
            switch (Root_Type)
            {
                case Jmd_Root.Integer: return 0;
                case Jmd_Root.Boolean: return false;
                case Jmd_Root.Float: return 0.0;
                case Jmd_Root.Time: return DateTime.Now;
                case Jmd_Root.Enumeration: return 0;
                case Jmd_Root.String: return "";
            };
            return System.DBNull.Value;
        }


        System.Type DataType(int Root_Type)
        {
            switch (Root_Type)
            {
                case Jmd_Root.Integer: return typeof(int);
                case Jmd_Root.Boolean: return typeof(bool);
                case Jmd_Root.Float: return typeof(float);
                case Jmd_Root.Time: return typeof(DateTime);
                case Jmd_Root.Enumeration: return typeof(int);
                case Jmd_Root.String: return typeof(string);
                case Jmd_Root.Record: return typeof(int);
                case Jmd_Root.Value: return typeof(object);
                case Jmd_Root.Object: return typeof(object);
                case Jmd_Root.Array: return typeof(int);
            };
            return typeof(object);
        }

        // Loop an array
        public void Loop(Action<int> On_Row, bool Step_In_Row = true)
        {
            for (int I = 1; I <= Size; I++)
            {
                if (Step_In_Row) Step_Into(I);
                On_Row(I);
                if (Step_In_Row) Step_Out();
            }
        }

        // Fill a datatable from object
        public void Read(DataTable Table, params object[] Expression_Row)
        {
            #region body
            Table.Clear();
            Jmd_Value V = Zoom(Expression_Row);
            if (V == null) return;
            if (V.Value == null) return;
            if (V.Root_Type != Jmd_Root.Array) return;
            if (V.Nature.Sub_Type.Root_Type != Jmd_Root.Record) return;

            // Build the schema if has no column yet
            if (Table.Columns.Count == 0)
            {
                foreach (Jmd_Type.Ty_Component Comp in V.Nature.Sub_Type.Components)
                {
                    Table.Columns.Add(new DataColumn(Comp.Name, DataType(Comp.Sub_Type.Root_Type)));
                };
            };

            // Add values
            foreach (Jmd_Value Row in (ArrayList)V.Value)
            {
                ArrayList Record_Val = (ArrayList)Row.Value;
                DataRow DR = Table.NewRow();
                for (int I = 0; I < V.Nature.Sub_Type.Size; I++)
                {
                    Jmd_Value Val = (Jmd_Value)Record_Val[I];
                    if (Val.Value == null)
                    {
                        DR[I] = Default_Value(Val.Root_Type);
                    }
                    else
                    {
                        DR[I] = Val.Value;
                    };
                };
                Table.Rows.Add(DR);
            };
            #endregion
        }

        public bool B_Value(params object[] Expression_Row)
        {
            #region body
            Jmd_Value V = Zoom(Expression_Row);
            if (V == null) return false;
            if (V.Value == null) return false;
            switch (V.Root_Type)
            {
                case Jmd_Root.Integer: return Convert.To_Boolean((int)(V.Value));
                case Jmd_Root.Boolean: return (bool)V.Value;
                case Jmd_Root.Float: return Convert.To_Boolean((double)(V.Value));
                case Jmd_Root.Time: return Convert.To_Boolean((double)(V.Value));
                case Jmd_Root.Enumeration: return Convert.To_Boolean((int)(V.Value));
                case Jmd_Root.String: return Convert.To_Boolean((string)(V.Value));
                case Jmd_Root.Record: return false;
                case Jmd_Root.Value: return (V.Value != null);
                case Jmd_Root.Object: return (V.Value != null);
                case Jmd_Root.Array: return (((ArrayList)V.Value).Count > 0);
            };
            return false;
            #endregion
        }

        public int I_Value(params object[] Expression_Row)
        {
            #region body
            Jmd_Value V = Zoom(Expression_Row);
            if (V == null) return 0;
            if (V.Value == null) return 0;
            switch (V.Root_Type)
            {
                case Jmd_Root.Integer: return (int)(V.Value);
                case Jmd_Root.Boolean: return (int)Convert.To_Integer((bool)V.Value);
                case Jmd_Root.Float: return (int)Convert.To_Integer((double)(V.Value));
                case Jmd_Root.Time: return (int)Convert.To_Integer((double)(V.Value));
                case Jmd_Root.Enumeration: return (int)(V.Value);
                case Jmd_Root.String: return (int)Convert.To_Integer((string)(V.Value));
                case Jmd_Root.Record: return 0;
                case Jmd_Root.Value: return 0;
                case Jmd_Root.Object: return 0;
                case Jmd_Root.Array: return ((ArrayList)(V.Value)).Count;
            };
            return 0;
            #endregion
        }
        public double F_Value(params object[] Expression_Row)
        {
            #region body
            Jmd_Value V = Zoom(Expression_Row);
            if (V == null) return 0.0;
            if (V.Value == null) return 0.0;
            switch (V.Root_Type)
            {
                case Jmd_Root.Integer: return Convert.To_Float((int)(V.Value));
                case Jmd_Root.Boolean: return Convert.To_Float((bool)V.Value);
                case Jmd_Root.Float: return Convert.To_Float((double)(V.Value));
                case Jmd_Root.Time: return Convert.To_Float((double)(V.Value));
                case Jmd_Root.Enumeration: return Convert.To_Float((int)(V.Value));
                case Jmd_Root.String: return Convert.To_Float((string)(V.Value));
                case Jmd_Root.Record: return 0.0;
                case Jmd_Root.Value: return 0.0;
                case Jmd_Root.Object: return 0.0;
                case Jmd_Root.Array: return Convert.To_Float(((ArrayList)V.Value).Count);
            };
            return 0.0;
            #endregion
        }
        public string S_Value(params object[] Expression_Row)
        {
            #region body

            Jmd_Value V = Zoom(Expression_Row);
            if (V == null) return "";
            if (V.Value == null) return "";
            switch (V.Root_Type)
            {
                case Jmd_Root.Integer: return Convert.To_String((int)(V.Value));
                case Jmd_Root.Boolean: return Convert.To_String((bool)V.Value);
                case Jmd_Root.Float: return Convert.To_String((double)(V.Value));
                case Jmd_Root.Time: return Convert.To_String((DateTime)(V.Value));
                case Jmd_Root.Enumeration: return V.Nature.Enums[(int)(V.Value)];
                case Jmd_Root.String: return (string)(V.Value);
                case Jmd_Root.Record: return "";
                case Jmd_Root.Value: return "";
                case Jmd_Root.Object: return ((Jmd_Object)V.Value).Physical_Name();
                case Jmd_Root.Array: return Convert.To_String(((ArrayList)V.Value).Count);
            };
            return "";
            #endregion
        }
        public object Value(params object[] Expression_Row)
        {
            #region body
            Jmd_Value V = Zoom(Expression_Row);
            if (V == null) return null;
            return V.Value;
            #endregion

        }

        public object Any_Value(params object[] Expression_Row)
        {
            return S_Value(Expression_Row).Decoded();
        }

        public void Read(ref object Value, params object[] Expression_Row)
        // Value is not changed if the cursor points to an undefined value
        {
            #region body
            if (Undefined_Value(Expression_Row)) return;
            Value = this.Value(Expression_Row);
            #endregion
        }

        public void Read(ref string Value, params object[] Expression_Row)
        // Value is not changed if the cursor points to an undefined value
        {
            #region body
            if (Undefined_Value(Expression_Row)) return;
            Value = this.S_Value(Expression_Row);
            #endregion
        }


        public void Write_Text(string Value, params object[] Expression_Row)
        // Write to value pointed by the cursor
        // The cursor must be open in write mode (ie : not Read_Only)
        {
            #region body
            if (Read_Only) { In_Error = true; return; };
            Jmd_Value V = Zoom(Expression_Row);
            if (V == null) return;
            switch (V.Root_Type)
            {
                case Jmd_Root.Integer: V.Value = Convert.To_Integer(Value); break;
                case Jmd_Root.Boolean: V.Value = Convert.To_Boolean(Value); break;
                case Jmd_Root.Float: V.Value = Convert.To_Float(Value); break;
                case Jmd_Root.Time: V.Value = Convert.To_Time(Value); break;
                case Jmd_Root.Enumeration:
                    {
                        V.Value = 0;
                        int Row = 0;
                        foreach (string Enum in V.Nature.Enums)
                        {
                            if (Strings.Like(Value, Enum))
                            {
                                V.Value = Row;
                                return;
                            };
                            Row++;
                        };
                        break;
                    };
                case Jmd_Root.String: V.Value = Value; break;
                case Jmd_Root.Object: V.Value = Jmd.Get_Object(Value); break;
            };

            #endregion
        }


        static int To_Enum(object Value, Jmd_Type Nature)
        {
            try
            {
                if (Value is int) return (int)Value;
                if (Value is bool) return Convert.To_Integer(Value);
                if (Value is double) return Convert.To_Integer(Value);
                if (Value is string)
                {
                    string Text = (string)Value;
                    for (int I = 0; I < Nature.Enums.Length; I++)
                    {
                        if (JR.Strings.Like(Nature.Enums[I], Text)) return I;
                    };
                };
            }
            catch { };

            return 0;
        }

        public void Write_Any(object Value, params object[] Expression_Row)
        // Write to value pointed by the cursor
        // The cursor must be open in write mode (ie : not Read_Only)
        {
            Write(Value.Encoded(), Expression_Row);
        }

        public void Write(object Value, params object[] Expression_Row)
        // Write to value pointed by the cursor
        // The cursor must be open in write mode (ie : not Read_Only)
        {
            #region body
            if (Read_Only) { In_Error = true; return; };
            Jmd_Value V = Zoom(Expression_Row);
            if (V == null) return;
            if (Value == null || Value == System.DBNull.Value)
            {
                V.Value = null;
                return;
            };

            switch (V.Root_Type)
            {
                case Jmd_Root.Integer:
                    {
                        V.Value = Convert.To_Integer(Value);
                        if (_Reset_If_Default && (int)V.Value == 0) V.Value = null;
                        return;
                    }
                case Jmd_Root.Boolean:
                    {
                        V.Value = Convert.To_Boolean(Value);
                        if (_Reset_If_Default && (bool)V.Value == false) V.Value = null;
                        return;
                    }
                case Jmd_Root.Float:
                    {
                        V.Value = Convert.To_Float(Value);
                        if (_Reset_If_Default && (double)V.Value == 0.0) V.Value = null;
                        return;
                    }
                case Jmd_Root.Time:
                    {
                        V.Value = Convert.To_Time(Value);
                        if (_Reset_If_Default && (DateTime)V.Value == System.DateTime.MinValue) V.Value = null;
                        return;
                    }
                case Jmd_Root.Enumeration:
                    {
                        V.Value = To_Enum(Value, V.Nature);
                        if (_Reset_If_Default && (int)V.Value == 0) V.Value = null;
                        return;
                    }
                case Jmd_Root.String:
                    {
                        V.Value = Convert.To_String(Value);
                        if (_Reset_If_Default && (string)V.Value == "") V.Value = null;
                        return;
                    }
                case Jmd_Root.Value:
                    {
                        if (Value == null) { V.Value = null; return; };
                        V.Value = (Jmd_Value)Value;
                        return;
                    }
            };
            V.Value = Value;
            #endregion
        }

        public void Reset(params object[] Expression_Row)
        // Completly clear a sub expression
        {
            #region body
            if (Read_Only) { In_Error = true; return; };
            Jmd_Value V = Zoom(Expression_Row);
            if (V == null) return;
            V.Value = null;
            return;
            #endregion
        }

        public void Define(Jmd_Type Nature, params object[] Expression_Row)
        // Force the type of a 'V'(value) kind of value
        {
            #region body
            if (Read_Only) { In_Error = true; return; };
            Jmd_Value V = Zoom(Expression_Row);
            if (V == null) return;

            switch (V.Root_Type)
            {
                case Jmd_Root.Value:
                    {
                        V.Value = null;
                        V.Value = new Jmd_Value(Nature);
                        return;
                    }
            };
            #endregion
        }

        public void Copy_To(Jmd_Cursor Destination)
        // Copy of the value pointed by Source to the value pointed by Destination
        // Source & Destination must be compatible types ie: same root type
        // except when Destination points a "Value" type
        {
            if (Destination.Read_Only) { In_Error = true; return; };
            Jmd_Value Src_V = Values[Level];
            Jmd_Value Dst_V = Destination.Values[Level];
            if (Src_V == null||Dst_V == null) return;

            void Dup(Jmd_Value Src, Jmd_Value Dst)
            {
                Dst.Nature = Src.Nature;
                Dst.Root_Type = Src.Root_Type;
                if (Src.Value == null)
                {
                    Dst.Value = null;
                    return;
                }
                switch (Src.Root_Type)
                {
                    case Jmd_Root.Integer:
                    case Jmd_Root.Boolean:
                    case Jmd_Root.Float:
                    case Jmd_Root.Time:
                    case Jmd_Root.Enumeration:
                    case Jmd_Root.Object:
                    case Jmd_Root.String:
                        Dst.Value = Src.Value;
                        break;
                    case Jmd_Root.Record:
                    case Jmd_Root.Array:
                        {
                            var S_List = (ArrayList)Src.Value;
                            var D_List = S_List.Clone() as ArrayList;
                            Dst.Value = D_List;
                            int Row = 0;
                            foreach (Jmd_Value R_Item in S_List)
                            {
                                var DV = new Jmd_Value(R_Item.Nature);
                                D_List[Row] = DV;
                                Dup(R_Item, DV);
                                Row++;
                            };
                        };
                        break;

                    case Jmd_Root.Value:
                        Jmd_Value S_Val = (Jmd_Value)Src.Value;
                        Jmd_Value D_Val = new Jmd_Value(S_Val.Nature);
                        Dst.Value = D_Val;
                        Dup(S_Val, D_Val);
                        break;
                };
            }
            Dup(Src_V, Dst_V);
        }

        public void Move_To(Jmd_Cursor Destination)
        // Same as copy, except that source value is cleared at the end
        {
        }

        public void Insert(params int[] Row_Count)
        //Insert Count items at a specific position
        //   existing items at position Raw and above are scrolled from Count position
        //The size of the array is incremented by Count
        // if Raw = 0, items are added at the last position
        {
            #region body
            if (Read_Only) { In_Error = true; return; };
            int Row = 0;
            Jmd_Value V = Values[Level];
            if (V.Root_Type != Jmd_Root.Array) return;
            if (Row_Count.Length > 0) Row = Row_Count[0];
            int Count = 1;
            if (Row_Count.Length > 1) Count = Row_Count[1];

            if (V.Value == null)
            {
                V.Value = new ArrayList();
            };
            ArrayList List = (ArrayList)V.Value;

            for (int I = 1; I <= Count; I++)
            {
                if (Row > 0)
                {
                    List.Insert(Row - 1, new Jmd_Value(V.Nature.Sub_Type));
                }
                else
                {
                    List.Add(new Jmd_Value(V.Nature.Sub_Type));
                };
            };
            #endregion
        }


        public void Remove(params int[] Row_Count)
        //Removes Count items at raw position
        //   The size of the array is reduced by Count
        //   existing items at position Raw+Count and above are scrolled from Count position
        // if Raw = 0, items are removed from the last position
        {
            #region body
            if (Read_Only) { In_Error = true; return; };
            int Row = 0;
            Jmd_Value V = Values[Level];
            if (V.Root_Type != Jmd_Root.Array) return;
            if (Row_Count.Length > 0) Row = Row_Count[0];
            int Count = 1;
            if (Row_Count.Length > 1) Count = Row_Count[1];

            if (V.Value == null) return;
            ArrayList List = (ArrayList)V.Value;

            if (Row > 0)
            {
                List.RemoveRange(Row - 1, Count);
            }
            else
            {
                List.RemoveRange(List.Count - Count, Count);
            };
            #endregion
        }

        public Jmd_Cursor Cursor(bool Read_Only)
        // return a cursor pointing the same value
        {
            #region body
            Jmd_Cursor Curs = new Jmd_Cursor();
            Curs.Point(this, Read_Only);
            return Curs;
            #endregion
        }

        public bool Reset_If_Default
        // If set, writing default (0, false, "", 0.0 value in a field resets the field
        {
            get { return _Reset_If_Default; }
            set { _Reset_If_Default = value; }
        }

        #region Internals
        #region declarations
        internal bool Read_Only = true;
        internal Jmd_Object Object = null;
        internal int[] Path = new int[50];
        internal Jmd_Value[] Values = new Jmd_Value[50];
        internal int Level = 0;
        internal bool In_Error = false;
        internal bool _Reset_If_Default = false;

        public Jmd_Cursor()
        {
        }
        #endregion

        public void Point(Jmd_Cursor Cursor, bool Read_Only)
        // Points the cursor to the value of another cursor
        {
            #region body
            this.Read_Only = Read_Only;
            this.In_Error = false;
            this.Object = Cursor.Object;
            if (this.Object == null)
            {
                this.Level = 0;
            }
            else
            {
                this.Level = Cursor.Level;
                Cursor.Values.CopyTo(this.Values, 0);
                Cursor.Path.CopyTo(this.Path, 0);
            };
            #endregion
        }

        public void Point(Jmd_Object Object, bool Read_Only)
        // Points the cursor to the value of an object
        {
            #region body
            this.Read_Only = Read_Only;
            this.In_Error = false;
            this.Object = Object;
            if (Object == null)
            {
                this.Level = 0;
            }
            else
            {
                this.Level = 1;
                this.Path[1] = 1;
                this.Values[1] = Object.Value;
            };
            #endregion
        }

        internal void Point(Jmd_Value Value, bool Read_Only)
        // Points the cursor to the value of an object
        {
            #region body
            this.Read_Only = Read_Only;
            this.In_Error = false;
            this.Object = null;
            this.Level = 1;
            this.Path[1] = 1;
            this.Values[1] = Value;
            #endregion
        }

        internal void Point(string Expression, bool Read_Only)
        // Points the cursor to an expression
        //  general syntax is Object.Component.Component...Component
        {
            #region body
            Strings.Cursor S = new Strings.Cursor(Expression);

            // Points the object
            this.Point(Jmd.Get_Object(S.Next(".")), Read_Only);
            while (!S.End)
            {
                this.Step_Into(S.Next("."));
            };
            #endregion
        }




        private void Check()
        // Verify and completes the cursor informations
        {
            Jmd_Value Parent_Value = Values[Level - 1];
            // Computes current value
            if (Parent_Value != null)
            {
                switch (Parent_Value.Root_Type)
                {
                    case Jmd_Root.Record:
                        int Record = Path[Level];
                        if (!Numbers.In_Range(Record, 1, Parent_Value.Nature.Size)) { In_Error = true; return; };
                        if (Parent_Value.Value == null)
                        {
                            if (Read_Only) { In_Error = true; return; };
                            ArrayList List = new ArrayList(Parent_Value.Nature.Size);
                            for (int I = 0; I < Parent_Value.Nature.Size; I++)
                            {
                                List.Add(new Jmd_Value(Parent_Value.Nature.Components[I].Sub_Type));
                            };
                            Parent_Value.Value = List;
                        };
                        Values[Level] = (Jmd_Value)((ArrayList)Parent_Value.Value)[Record - 1];

                        break;

                    case Jmd_Root.Value:
                        if (Path[Level] == 1)
                        {
                            Values[Level] = (Jmd_Value)Parent_Value.Value;
                        };
                        break;

                    case Jmd_Root.Object:
                        if (Path[Level] == 1 && Parent_Value.Value != null)
                        {
                            Values[Level] = (Jmd_Value)Parent_Value.Value;
                        };
                        break;

                    case Jmd_Root.Array:
                        int Index = Path[Level];

                        if (Parent_Value.Value == null)
                        {
                            if (Read_Only) { In_Error = true; return; };
                            Parent_Value.Value = new ArrayList(2 * Index);
                        };

                        // 
                        ArrayList Array = ((ArrayList)Parent_Value.Value);
                        if (!Numbers.In_Range(Index, 1, Array.Count))
                        {
                            if (Read_Only) { In_Error = true; return; };
                            for (int I = Array.Count + 1; I <= Index; I++)
                            {
                                Array.Add(new Jmd_Value(Parent_Value.Nature.Sub_Type));
                            }
                        };

                        Values[Level] = (Jmd_Value)Array[Index - 1];
                        break;

                    default:

                        break;

                };

            };
            In_Error = (Values[Level] == null);
        }

        internal string Component_Name(int Level)
        // Returns the name of the component pointed by the cursor
        {
            Jmd_Type T = null;
            if (Level <= 0) return "";
            if (Values[Level] == null) return "";
            if (Level == 1) return Object.Name;
            T = Values[Level].Nature;
            switch (T.Root_Type)
            {
                case Jmd_Root.Record:
                    if (Path[Level] > 0 && Path[Level] <= T.Size) return T.Components[Path[Level] - 1].Name;
                    return "";
                case Jmd_Root.Array:
                    return Path[Level].ToString();
                default:
                    return "?";  // Grave probleme
            };

        }

        private Jmd_Value Zoom(params object[] Expression_Row)
        {

            int Inside = 0;
            Jmd_Value V = Values[Level];
            if (V == null) return null;
            if (Expression_Row.Length == 0) return V;


            foreach (object Param in Expression_Row)
            {
                if (Param.GetType() == typeof(int))
                {
                    Step_Into((int)Param);
                    Inside++;
                }
                else
                {
                    // Allocate a new cursor
                    Strings.Cursor S = new Strings.Cursor((string)Param);
                    while (!S.End)
                    {
                        Step_Into(S.Next("."));
                        Inside++;
                    };
                };

            };

            V = Values[Level];
            while (Inside > 0)
            {
                Step_Out();
                Inside--;
            };
            if (V == null) return null;
            return V;
        }

        internal void Presize(int Size)
        // Pre size an array directly to the good size
        {
            if (Read_Only) { In_Error = true; return; };
            Jmd_Value V = Values[Level];
            if (V.Root_Type != Jmd_Root.Array) return;
            if (V.Value != null) return;
            ArrayList List = new ArrayList(Size);
            V.Value = List;
            for (int I = 1; I <= Size; I++)
            {
                List.Add(new Jmd_Value(V.Nature.Sub_Type));
            };
        }
        #endregion

    } // end Jmd_Cursor
    #endregion

    public class Jmd_Value
    {
        #region Declarations
        public Object Value;
        public int Root_Type;
        public Jmd_Type Nature;

        public Jmd_Value(Jmd_Type Nature)
        {
            this.Root_Type = Nature.Root_Type;
            this.Nature = Nature;
            switch (Root_Type)
            {
                case Jmd_Root.Enumeration:
                    this.Value = 0;
                    break;
            }
        }
        #endregion

        private bool Is_Empty()
        {
            return (this.Value == null);
        }

        public Jmd_Cursor Cursor(bool Read_Only)
        // return a cursor associated with this value
        {
            #region body
            Jmd_Cursor Curs = new Jmd_Cursor();
            Curs.Point(this, Read_Only);
            return Curs;
            #endregion
        }

        internal void Save(XmlTextWriter Writer)
        {
            #region body
            if (this.Is_Empty()) return;

            switch (this.Root_Type)
            {
                case Jmd_Root.Integer:
                    Writer.WriteString(Convert.To_String(Value)); break;
                case Jmd_Root.Boolean:
                    Writer.WriteString(Convert.To_String(Value)); break;
                case Jmd_Root.Float:
                    Writer.WriteString(Convert.To_String(Value)); break;
                case Jmd_Root.Time:
                    Writer.WriteString(Convert.To_String(Value)); break;
                case Jmd_Root.Enumeration:
                    Writer.WriteString(this.Nature.Enums[(int)Value]); break;
                case Jmd_Root.String:
                    Writer.WriteString((string)Value); break;
                case Jmd_Root.Record:
                    {
                        ArrayList List = (ArrayList)Value;
                        int Row = 0;
                        foreach (Jmd_Value R_Item in List)
                        {
                            if (!R_Item.Is_Empty())
                            {
                                Writer.WriteStartElement(this.Nature.Components[Row].Name);
                                R_Item.Save(Writer);
                                Writer.WriteEndElement();
                            };
                            Row++;
                        };
                    };
                    break;
                case Jmd_Root.Value:
                    Jmd_Value Val = (Jmd_Value)Value;
                    Writer.WriteStartElement("VALUE");
                    Writer.WriteAttributeString("TYPE", Val.Nature.External_Name());
                    Val.Save(Writer);
                    Writer.WriteEndElement();
                    break;

                case Jmd_Root.Object:
                    Writer.WriteString(((Jmd_Object)Value).Physical_Name()); break;

                case Jmd_Root.Array:
                    {
                        ArrayList List = (ArrayList)Value;
                        Writer.WriteStartElement("ARRAY");
                        Writer.WriteAttributeString("SIZE", List.Count.ToString());
                        int Last_Saved = 0;
                        int Row = 1;
                        foreach (Jmd_Value Item in List)
                        {
                            if (!Item.Is_Empty())
                            {
                                Writer.WriteStartElement("I");
                                if (Last_Saved != Row - 1)
                                {
                                    Writer.WriteAttributeString("ROW", Row.ToString());
                                };
                                Item.Save(Writer);
                                Writer.WriteEndElement();
                                Last_Saved = Row;
                            };
                            Row++;
                        };
                        Writer.WriteEndElement();
                    };
                    break;
            };

            #endregion
        }

    }

    #region Global Internals

    internal class Jmd_Root
    {
        internal const int Integer = 0;
        internal const int Boolean = 1;
        internal const int Float = 2;
        internal const int Time = 3;
        internal const int Enumeration = 4;
        internal const int String = 5;
        internal const int Record = 6;
        internal const int Value = 7;
        internal const int Object = 8;
        internal const int Array = 9;
        internal const int Size = 10;
    }
    #endregion

} // end JR
