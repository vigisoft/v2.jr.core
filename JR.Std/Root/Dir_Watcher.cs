using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace JR
{
    // Classe qui surveille un répertoire en faisant un scan sur time out en nb de sec
    public class Dir_Watcher
    {
        string _Directory;
        string _Filter;
        double _Period;
        DateTime Next_Check = DateTime.UtcNow;
        public Dir_Watcher(string Directory, string Filter, double Period)
        {
            _Directory = Directory;
            _Filter = Filter;
            _Period = Period;
            Check();
        }

        bool Check()
        {
            if (Next_Check > DateTime.UtcNow) return false;
            bool Change = false;
            Dictionary<string, DateTime> New_Files = new Dictionary<string, DateTime>();
            string[] Files = Directory.GetFiles(_Directory, _Filter);
            if (Files.Length != Last_Files.Count) Change = true;
            foreach (string File in Files)
            {
                FileInfo FI = new FileInfo(File);
                if (!Change && !Last_Files.ContainsKey(File)) Change = true;
                if (!Change && Last_Files[File] != FI.LastWriteTimeUtc) Change = true;
                New_Files.Add(File, FI.LastWriteTimeUtc);
            }
            Last_Files.Clear();
            Last_Files = New_Files;
            Next_Check = DateTime.UtcNow.AddSeconds(_Period);
            return Change;
        }

        Dictionary<string, DateTime> Last_Files = new Dictionary<string, DateTime>();
        public bool Changed
        {
            get
            {
                return Check();
            }
        }
    }
}
