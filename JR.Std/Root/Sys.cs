using System;
using Microsoft.Win32;
using System.Reflection;

namespace JR
{
	/// <summary>
	/// Description r�sum�e de SYS
	/// </summary>
	public static class Sys
	{
        public static object Create_Instance (string Type_Name)
        {
            System.Runtime.Remoting.ObjectHandle obj = Activator.CreateInstance(Assembly.GetCallingAssembly().FullName, Type_Name);
            return obj.Unwrap();
        }

	}
}

