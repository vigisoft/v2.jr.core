using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace JR.Root
{
    public class Cache<T> where T: class
    {
        class Cached_Object
        {
            public T Object;
            public DateTime Expiration;
            public TimeSpan Time_Out;
            public string Linked_Ressource;
            public DateTime Ressource_Time = DateTime.MinValue;
        }

        Dictionary<string, Cached_Object> _Cache = new Dictionary<string, Cached_Object>();
        public Cache()
        {
        }
        public void Add(T Object, string Key, string Linked_Ressource, TimeSpan Expire)
        {
            Cached_Object CO = new Cached_Object();
            CO.Object = Object;
            CO.Expiration = DateTime.UtcNow.Add (Expire);
            CO.Time_Out = Expire;
            CO.Linked_Ressource = Linked_Ressource;
            if (Linked_Ressource != "")
            {
                try
                {
                    FileInfo FI = new FileInfo(Linked_Ressource);
                    CO.Ressource_Time = FI.LastWriteTimeUtc;
                }
                catch { }

            }
            _Cache[Key] = CO;
        }

        public void Remove (string Key)
        {
            _Cache.Remove (Key);
        }

        public void Clear()
        {
            _Cache.Clear();
        }

        public T Get(string Key)
        {
            if (!_Cache.ContainsKey(Key)) return null;
            Cached_Object CO = _Cache[Key];
            if (CO.Expiration < DateTime.UtcNow)
            {
                if (CO.Linked_Ressource != "")
                {
                    try
                    {
                        FileInfo FI = new FileInfo(CO.Linked_Ressource);
                        if (CO.Ressource_Time != FI.LastWriteTimeUtc)
                        {
                            _Cache.Remove(Key);
                            return null;
                        };
                        CO.Ressource_Time = FI.LastWriteTimeUtc;
                        CO.Expiration = DateTime.UtcNow.Add(CO.Time_Out);
                    }
                    catch { }
                }
                else
                {
                    _Cache.Remove(Key);
                    return null;
                }
            }
            return CO.Object;
        }
    }
}
