﻿using System;
using System.Collections.Generic;

namespace JR
{

    public class Args_Reader
    {
        Dictionary<string, string> Args = new();

        public Args_Reader(string[] args, bool With_Verb = true)
        {
            if (args.Length == 0) return;
            int I = 0;
            foreach (var a in args)
            {
                string Key = "";
                string Value = "";
                if (I==0 && With_Verb)
                {
                    Key = "verb";
                    Value = a.Trim();
                }
                else
                {
                    var V = a.Split(['=']);
                    Key = V[0].ToLower().Trim();
                    Value = V.Length == 0 ? "":V[1];
                }
                Args[Key] = Value;
                Args[I.ToString()] = a;
                I++;
            }
        }

        public int Count => Args.Count;
        public string Arg(string Key, string Default = null)
        {
            foreach (var K in Key.Split('|'))
            {
                if (Args.TryGetValue(K.ToLower().Trim(), out var V)) return V;
            }
            return Default;
        }

        public string Verb => Arg("verb", "");

        public string Arg(int Row, string Default = null) => Arg(Row.ToString(), Default);

    }

    public class Arguments : Dictionary<string, string>
    {
        public List<Tuple<string, string>> Queue;
        public string Verb = "";
        public Arguments(string Def, bool With_Verb = true, bool Keep_Order = false)
        {
            if (Keep_Order) Queue = new List<Tuple<string, string>>();
            var C = Def.Scan();
            bool Check_Verb = With_Verb;
            while (!C.End)
            {
                if (Check_Verb)
                {
                    Verb = C.Next("/ ").Trim();
                    Check_Verb = false;
                    continue;
                }
                var Key = C.Next("=/ ").Trim().ToLower();
                if (Key == "") continue;
                if (C.Separator == '=')
                {
                    var Next_Char = C.Next_Char();
                    switch (Next_Char)
                    {
                        case '\'':
                            Add_Arg(Key, C.Next("'"));
                            continue;
                        case '"':
                            Add_Arg(Key, C.Next("\""));
                            continue;
                        case '[':
                            Add_Arg(Key, C.Next("]"));
                            continue;
                    }
                    C.Skip(-1);
                    Add_Arg(Key, C.Next("/ ").Trim());
                }
                else
                {
                    Add_Arg(Key, "1");
                }
            }
        }

        public void Add_Arg(string Key, string Value)
        {
            if (TryGetValue(Key, out string Val))
            {
                this[Key] = Val + "|" + Value;
            }
            else
            {
                this[Key] = Value;
            }
            if (Queue != null) Queue.Add(new Tuple<string, string>(Key, Value));
        }

        public bool Exists(params string[] Keys)
        {
            foreach (var K in Keys)
            {
                if (ContainsKey(K.ToLower())) return true;
            }
            return false;
        }

        public bool Read(out string Value, params string[] Keys)
        {
            foreach (var K in Keys)
            {
                if (TryGetValue(K.ToLower(), out Value)) return true;
            }
            Value = null;
            return false;
        }

        public string Value(params string[] Keys)
        {
            foreach (var K in Keys)
            {
                if (TryGetValue(K.ToLower(), out string V)) return V;
            }
            return "";
        }

        public new string[] Values(params string[] Keys)
        {
            foreach (var K in Keys)
            {
                if (TryGetValue(K.ToLower(), out string V)) return V.Split('|');
            }
            return new string[] { };
        }

        public bool Read_Next(out string Key, out string Value)
        {
            if (Queue.Count == 0)
            {
                Key = null;
                Value = null;
                return false;
            }
            Key = Queue[0].Item1;
            Value = Queue[0].Item2;
            Queue.RemoveAt(0);
            return true;
        }

        public string Read_Next(string Key)
        {
            for (int I = 0; I < Queue.Count; I++)
            {
                if (Queue[I].Item1 == Key)
                {
                    var V = Queue[I].Item2;
                    Queue.RemoveAt(I);
                    return V;
                }
            }
            return "";
        }

    }

}
