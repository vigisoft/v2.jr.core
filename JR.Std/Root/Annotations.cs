﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JR
{
    public class Annotations
    {

        public string this[string Index]
        {
            get
            {
                return Value_Of(Index);
            }

            set
            {
                Remove_Annotation(Index);
                Append_Annotation(Index, value);
            }
        }

        private void Append_Annotation(string Index, string Annotation)
        {
            if (Annotation == null || Annotation == "") return;
            Value += "[" + Index + ":" + Annotation + "]";
        }

        private void Remove_Annotation(string Index)
        {
            string Key = JR.Strings.Official(Index);
            int Start = Value.IndexOf("[" + Key + ":", 0);
            if (Start < 0) return;
            int Stop = Value.IndexOf("]", Start);
            if (Stop < 0) return;
            Value = Value.Remove (Start, Stop - Start +1);
        }

        private string Value_Of(string Index)
        {
            string Key = JR.Strings.Official(Index);
            int Start = Value.IndexOf("[" + Key + ":", 0);
            if (Start < 0) return "";
            Start = Start + 2 + Key.Length;
            int Stop = Value.IndexOf("]", Start);
            if (Stop < 0) return "";
            return Value.Substring (Start, Stop-Start);
        }
        public string Value {get;set;}
        public Annotations ()
        {
            Value = "";
        }
    }
}
