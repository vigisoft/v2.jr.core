using System;
using System.Collections;

namespace JR
{

    public class Pair<T> where T:class
    {
        public string Key = "";
        public T Value = default(T);
        public Pair()
        {
        }
        public Pair (string Key, T Value)
        {
            this.Key = Key;
            this.Value = Value;
        }
    }

	/// <summary>
	/// 
	/// </summary>
	public interface I_Named
	{
		 string Name {get; set;}
	}

	public class Named_List : Hashtable
	{
		public Named_List():base()
		{
		}

		public object Item (string Name)
		{
			return base[Name.ToUpper()];
		}

		public void Add (string Name, I_Named Object)
		{
			Object.Name = Name;
			base.Add (Name.ToUpper(), Object);
		}

		public void Remove (I_Named Object)
		{
			base.Remove (Object.Name.ToUpper());
		}
	}
}
