using System;
using System.Globalization;
using System.Text;

namespace JR
{
	/// <summary>
	/// 
	/// </summary>
	public static class Time
	{
		public readonly static DateTime Null_Time = DateTime.MinValue;
        public static DateTime Parse(string Value, string Format)
        {
            foreach (string F in Format.Split(';'))
            {
                try
                {
                    if (Value == "") return Null_Time;
                    DateTime D;
                    if (DateTime.TryParseExact(Value, F, CultureInfo.CurrentCulture, DateTimeStyles.None, out D)) return D;
                }
                catch
                {
                }
            }
            return Null_Time;
        }

        // yyyyww
        public static DateTime Parse_Year_Week(string Value)
        {
            if (Value == "") return Null_Time;
            if (Value.Length != 6) return Null_Time;
            int Year = JR.Convert.To_Integer(Value.Substring(0, 4));
            int Week = JR.Convert.To_Integer(Value.Substring(4, 2));
            DateTime D = new DateTime(Year, 1, 1);
            var C = CultureInfo.CurrentCulture.Calendar;
            D = C.AddWeeks(D, Week - C.GetWeekOfYear(D, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday));
            return D;
        }

        public static string Year_Week(DateTime Date)
        {
            return Year_Week(Date, DayOfWeek.Monday);
        }

        public static int Week(DateTime Date)
        {
            return Week(Date, DayOfWeek.Monday);
        }

        public static string Year_Week (DateTime Date, DayOfWeek First_Day)
        {
            int Week = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(Date, CalendarWeekRule.FirstFourDayWeek, First_Day);
            int Year = Date.Year;
            if (Week > 10 * Date.Month) Year--;
            return $"{Year:0000}{Week:00}";
        }

        public static int Week(DateTime Date, DayOfWeek First_Day)
        {
            return CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(Date, CalendarWeekRule.FirstFourDayWeek, First_Day);
        }

        // Retourne un time stamp plus court
        public static string Stamp(this DateTime Date)
        {
            int Ticks = (int)((long)((Date - DateTime.MinValue).TotalSeconds) % 100000000);
            var Clist = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            var SB = new StringBuilder();
            do
            {
                SB.Append(Clist[Ticks % Clist.Length]);
                Ticks /= Clist.Length;
            } while (Ticks != 0);
            return SB.ToString();
        }

        // Retourne un time stamp plus court
        public static string Format (this DateTime Date, string Format_String = "yyyyMMddHHmmss", string Default = "")
        {
            if (Date == Null_Time) return Default;
            return Date.ToString(Format_String);
        }

        public static DateTime To_Local(this DateTime D)
        {
            if (D == Null_Time) return Null_Time;
            return D.ToLocalTime();
        }

        public static DateTime To_UTC(this DateTime D)
        {
            if (D == Null_Time) return Null_Time;
            return D.ToUniversalTime();
        }
    }
}
