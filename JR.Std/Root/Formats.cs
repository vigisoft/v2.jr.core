using System;
using System.Text;
using System.IO;
using System.Collections;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace JR
{
    /// <summary>
    /// 
    /// </summary>
    public static class Formats
    {

        // Retourne le texte instanci�
        //  le solver retourne null pour un parametre qu'il ne connait pas
        // Retourne une chaine vide en cas d'erreur
        // OU si Empty_If_Default et qu'un des parametres est une valeur par default
        // Si la chaine format commence par [�Culture,alignement!default_empty] il s'agit d'options globales de formatage
        // Si la chaine d'option format commence par [�fr-fr] ou autre, utiliser la culture indiqu�e
        //        CultureInfo Culture = new CultureInfo(Page_State.Session.French?"fr-FR":"en-US");
        // ,alignement => comme dans le format standard
        // !xxxx => Indique que la chaine globale sera remplac�e par xxxx si une des variables � une valeur par d�faut
        //return string.Format (Culture,  "{0:" + Format + "}" ,Value);
        // Pour ins�rer un { lit�ral, le doubler. Il n'y a pas de restriction sur les }
        // Format conditionnel dans une variable {val?valeur=text?valeur=text?valeur_d�faut}
        //      doubler les == et ?? pour ins�rer lit�ralement. ?seul signifie retourner la chaine vide
        // 

        // Il faudrait ajouter la possibilit� de passer en min/maj/cap
        // et Possibilit� de padder l'alignement avec un caract�re autre que espace

        public class Dynamic_Text
        {
            public string Format = "";
            public CultureInfo Global_Culture = CultureInfo.InvariantCulture;
            public string Global_Default = "";
            public Alignement Global_Align = null;

            public class Alignement
            {
                public int Width;
                public char Mode;
                public char Pad = ' ';
                public static Alignement Extract(string Def)
                {
                    if (Def == "") return null;
                    switch (Def[0])
                    {
                        case '+':
                        case '-':
                        case '*':
                            return new Alignement { Mode = Def[0], Width = Def.Substring(1).To_Integer() };
                    }
                    return new Alignement { Mode = '+', Width = Def.To_Integer() };
                }
            }

            public List<object> Steps = null;

            public class Param
            {
                public Func<object> Solver;
                public Alignement Align = null;
                public string Format = null;
                public Dictionary<string, string> Cases = null;
                public string Case_Default = null;

                public void Add_Case(string Def)
                {
                    var I_Sep = Def.IndexOf('=');
                    if (I_Sep < 0)
                    {
                        Case_Default = Def;
                    }
                    else
                    {
                        if (Cases == null) Cases = new Dictionary<string, string>();
                        Cases[Def.Substring(0, I_Sep)] = Def.Substring(I_Sep + 1);
                    }
                }
            }

            //public bool Empty_If_Default = false;
            //public List<Func<object>> Solvers = new List<Func<object>>();
            //public object[] Params = null;

            string Aligned(Alignement Align, string V)
            {
                var Delta = Align.Width - V.Length;
                if (Delta > 0)
                {
                    switch (Align.Mode)
                    {
                        case '+':
                            return V.PadLeft(Align.Width, Align.Pad);
                        case '-':
                            return V.PadRight(Align.Width);
                        case '*':
                            return V.PadLeft(Align.Width / 2, Align.Pad).PadRight(Align.Width - Align.Width / 2);
                    }
                }
                else if (Delta < 0)
                {
                    switch (Align.Mode)
                    {
                        case '+':
                            return V.Substring(-Delta, Align.Width);
                        case '-':
                            return V.Substring(0, Align.Width);
                        case '*':
                            return V.Substring(-Delta / 2, Align.Width);
                    }
                }
                return V;
            }

            public override string ToString()
            {
                if (Steps == null) return Format;
                var SB = new StringBuilder();
                foreach (var S in Steps)
                {
                    if (S is string)
                    {
                        SB.Append(S);
                        continue;
                    }
                    var P = S as Param;
                    var V = P.Solver();
                    if (Global_Default != null && JR.Convert.Is_Default(V)) return Global_Default;

                    string SV = null;
                    if (V == null)
                    {
                        SV = "";
                    }
                    else if (P.Format != null)
                    {
                        try
                        {
                            bool Can_Convert = true;
                            if (V is string)
                            {
                                var FV = JR.Convert.To_Float (V, out bool Err);
                                if (Err)
                                {
                                    Can_Convert = false;
                                }
                                else
                                {
                                    V = FV;
                                }
                            }
                            if (Can_Convert)
                            {
                                if (Global_Culture == null)
                                {
                                    SV = string.Format("{0:" + P.Format + "}", V);
                                }
                                else
                                {
                                    SV = string.Format(Global_Culture, "{0:" + P.Format + "}", V);
                                }
                            }
                        }
                        catch
                        {
                            SV = "?";
                        }
                    }
                    else
                    {
                        if (Global_Culture == null)
                        {
                            SV = V.ToString();
                        }
                        else
                        {
                            SV = string.Format(Global_Culture, "{0}", V);
                        }
                    }

                    if (P.Cases != null)
                    {
                        if (P.Cases.TryGetValue(SV, out string New_SV))
                        {
                            SV = New_SV;
                        }
                        else
                        {
                            if (P.Case_Default != null) SV = P.Case_Default;
                        }
                    }

                    if (P.Align != null)
                    {
                        SB.Append(Aligned(P.Align, SV));
                    }
                    else
                    {
                        SB.Append(SV);
                    }
                }
                if (Global_Align != null)
                {
                    return Aligned(Global_Align, SB.ToString());
                }
                return SB.ToString();
            }


            public Dynamic_Text(string Expression, Func<string, Func<object>> Solver, string Global_Default_Value = "")
            {
                Global_Default = Global_Default_Value;
                Format = Expression;
                if (Solver == null || Format.IndexOf('{') < 0) return;
                var F = Format.Scan();
                Steps = new List<object>();

                // G�rer les paramatres globaux
                if (Format.StartsWith("[�"))
                {
                    F.Goto(2);
                    string Cult = F.Next("],!");
                    if (Cult != "")
                    {
                        try
                        {
                            Global_Culture = new CultureInfo(Cult);
                        }
                        catch { };
                    }
                    while (!(F.Separator == ']' || F.End))
                    {
                        switch (F.Separator)
                        {
                            case ',':
                                Global_Align = Alignement.Extract(F.Next("],!"));
                                break;
                            case '!':
                                Global_Default = F.Next("],!");
                                break;
                        }
                    }
                }

                while (!F.End)
                {
                    var Prefix = F.Next("{");
                    if (Prefix != "") Steps.Add(Prefix);
                    if (F.End) break;
                    F.Mark();
                    if (F.Next_Char() == '{')
                    {
                        Steps.Add("{");
                        continue;
                    }
                    F.Rewind();
                    var Val = F.Next(":,?}");
                    var Solv = Solver(Val);
                    if (Solv == null)
                    {
                        Steps.Add(Val);
                        continue;
                    }
                    var Param = new Dynamic_Text.Param { Solver = Solv };
                    Steps.Add(Param);
                    while (!(F.Separator == '}' || F.End))
                    {
                        switch (F.Separator)
                        {
                            case ',':
                                Param.Align = Alignement.Extract(F.Next(",:?}"));
                                break;
                            case ':':
                                Param.Format = F.Next(",?}");
                                break;
                            case '?':
                                Param.Add_Case(F.Next(",:?}"));
                                break;
                        }
                    }
                }
                if (Global_Culture == null) Global_Culture = CultureInfo.InvariantCulture;
            }
        }
    }
}

