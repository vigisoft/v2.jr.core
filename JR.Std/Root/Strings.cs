using System;
using System.Text;
using System.IO;
using System.Collections;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Net.Http.Headers;
using System.IO.MemoryMappedFiles;

namespace JR
{
    /// <summary>
    /// 
    /// </summary>
    public static class Strings
    {
        public static string Capitalized(this string Source)
        {
            if (Source == null) return Source;
            char[] Dest = Source.ToCharArray();
            for (int I = 0; I < Dest.Length; I++)
            {
                if (I == 0 || Dest[I - 1] == ' ' || Dest[I - 1] == '_' || Dest[I - 1] == '/')
                {
                    Dest[I] = char.ToUpper(Dest[I]);
                }
                else
                {
                    Dest[I] = char.ToLower(Dest[I]);
                }
            }
            return new string(Dest);
        }

        public static string Official(this string C1)
        {
            if (C1 == null) return "";
            return (C1.ToUpper()).Trim();
        }

        public static bool Like(this string C1, string C2)
        {
            return ((C1.ToUpper()).Trim() == (C2.ToUpper()).Trim());
        }

        public static string Range(this string C, int First, int Last)
        {
            if (First > Last) return "";
            if (Last < C.Length) return C.Substring(First, Last - First + 1);
            if (First < C.Length) return C.Substring(First);
            return "";
        }

        public static string Treat(this string Value, string Options)
        {
            foreach (var C in Options)
            {
                switch (C)
                {
                    case 'U':
                    case 'u':
                        Value = Value.ToUpper(); break;
                    case 'L':
                    case 'l':
                        Value = Value.ToLower(); break;
                    case 'C':
                    case 'c':
                        Value = Capitalized(Value); break;
                    case 'O':
                    case 'o':
                        Value = Official(Value); break;
                    case 'T':
                    case 't':
                        Value = Value.Trim(); break;
                }
            }
            return Value;
        }

        static Encoding Get_Encoding(string Name)
        {
            try
            {
                return Encoding.GetEncoding(Name);
            }
            catch
            {
                return Encoding.Default;
            }

        }
        public static Encoding ANSI_Encoding = Get_Encoding("windows-1252");
        public static Encoding ISO_Encoding = Get_Encoding("iso-8859-1");

        public static StreamReader Ascii_Reader(string File_Name)
        {
            return new StreamReader(File_Name, ANSI_Encoding);
        }

        public static StreamWriter Ascii_Writer(string File_Name)
        {
            return new StreamWriter(File_Name, false, ANSI_Encoding);
        }

        public static string Resize(this string C, int Length)
        {
            return Resize(C, Length, ' ');
        }

        public static string Resize(this string C, int Length, char Pad_Char)
        {
            if (C.Length >= Length) return C.Substring(0, Length);
            return C + new string(Pad_Char, Length - C.Length);
        }

        public static string New_Guid()
        {
            Guid Uid = Guid.NewGuid();
            return Uid.ToString();
        }

        public static string Limited_To(this string Source, int Length, bool One_Line)
        {
            if (Source == null || Source.Length == 0) return "";
            //int Idx = Source.IndexOfAny("\r\n".ToCharArray());
            if (One_Line) Source = Source.Replace('\r', ' ').Replace('\n', ' ');
            if (Source.Length > Length) return Source.Substring(0, Length - 2) + "..";
            return Source;
        }

        public static string Compress_To(this string Source, int Length)
        {
            if (Source == null || Source.Length == 0) return "";
            if (Source.Length <= Length) return Source;
            var S = Source.To_Letters();
            if (S.Length <= Length) return S;
            var Cp = new char[Length];
            var Coeff = (float)S.Length / (float)Length;
            float Pos = 0;
            for (int I = 0; I < Length; I++)
            {
                var Row = (int)Pos;
                if (Row >= S.Length) Row = S.Length - 1;
                Cp[I] = S[Row];
                Pos += Coeff;
            }
            return new string(Cp);
        }

        // Pattern matching simple avec * et ?
        public static bool Match(this string Value, string Pattern, bool Is_Regex = false)
        {
            if (Pattern == "") return true;
            if (!Is_Regex)
            {
                Pattern = "^" + Pattern.Replace(".", @"\.").Replace("*", ".*").Replace("?", ".{1}").Replace("$", @"\$");
            }
            return System.Text.RegularExpressions.Regex.IsMatch(Value, Pattern);
        }

        // Pattern matching simple avec * et ?
        public static bool Contains_Text(this string Value, string Pattern, bool Accent_Sensitive = true)
        {
            if (Value == "") return false;
            if (Pattern == "") return true;
            var Pat = new StringBuilder(Pattern);
            Pat.Replace("-", @"\-");
            Pat.Replace(".", @"\.");
            Pat.Replace("(", @"\(");
            Pat.Replace(")", @"\)");
            Pat.Replace(".", @"\.");
            Pat.Replace("*", ".*");
            Pat.Replace("?", ".{1}");
            if (!Accent_Sensitive)
            {
                Value = To_Letters(Value);
            }
            foreach (string P in Pat.ToString().Split(' '))
            {
                var PT = P.Trim();
                if (PT == "") continue;
                if (!System.Text.RegularExpressions.Regex.IsMatch(Value, PT, RegexOptions.IgnoreCase | RegexOptions.CultureInvariant)) return false;
            }
            return true;
        }

        public class Text_Searcher
        {
            string[] Patterns;
            public Text_Searcher(string Expression)
            {
                var Pat = Expression.Trim().Normalize(' ', true);
                Patterns = Pat.ToString().Split(' ');
            }
            public bool Is_Match(string Value)
            {
                if (Patterns.Length == 0) return true;
                Value = Value.To_Letters();
                foreach (string P in Patterns)
                {
                    if (!Value.Contains(P)) return false;
                }
                return true;
            }
        }

        public static Text_Searcher Search(this string Expression) => new Text_Searcher(Expression);


        public static bool Is_Included_In(this string Value, string In)
        {
            int I = 0;
            foreach (var C in Value)
            {
                bool Found = false;
                while (I < In.Length)
                {
                    Found = (C == In[I]);
                    I++;
                    if (Found) break;
                }
                if (!Found) return false;
            }
            return true;
        }

        public static bool Is_In(this string Value, params string[] In)
        {
            foreach (var C in In)
            {
                if (Value == C) return true;
            }
            return false;
        }

        // Pattern matching simple avec * et ?
        public static bool Contains_Letters(this string Value, string Pattern)
        {
            Value = Value.To_Letters().Trim();
            if (Value == "") return false;
            Pattern = Pattern.Trim();
            if (Pattern == "") return true;
            foreach (string P in Pattern.Split(' '))
            {
                var PT = P.To_Letters().Trim();
                if (PT == "") continue;
                if (!Value.Contains(PT)) return false;
            }
            return true;
        }

        public static bool Is_Integer(this string Value)
        {
            foreach (char C in Value)
            {
                if (!char.IsDigit(C)) return false;
            }
            return true;
        }

        public static bool Is_Valid_ID(this string Value)
        {
            if (Value.Length == 0) return false;
            foreach (char C in Value)
            {
                if (!(char.IsLetterOrDigit(C) || C == '_')) return false;
            }
            return true;
        }

        // Splitter intelligent
        // Le premier caract�re est le s�parateur sauf si c'est un alphabetique
        // ou un @ auquel cas on cherche le premier s�parateur standard de la chaine (,;|~)
        static char[] Standard_Seps = ",;|~".ToCharArray();
        static string Exclude_Seps = "@";
        static char Separator(ref string Expression)
        {
            char Sep = Expression[0];
            if (char.IsLetterOrDigit(Sep) || Exclude_Seps.IndexOf(Sep) >= 0)
            {
                int Sep_Index = Expression.IndexOfAny(Standard_Seps);
                if (Sep_Index < 0)
                {
                    Sep = '~';
                }
                else
                {
                    Sep = Expression[Sep_Index];
                }
                Expression = Sep + Expression;
            }
            return Sep;
        }
        public static string[] Split(string Expression)
        {
            if (Expression == null || Expression == "") return new string[0];
            string Express = Expression;
            char Sep = Separator(ref Express);
            string[] Exp = Express.Split(Sep);
            string[] New_Exp = new string[Exp.Length - 1];
            for (int I = 0; I < New_Exp.Length; I++)
            {
                New_Exp[I] = Exp[I + 1];
            }
            return New_Exp;
        }

        // Avec un nombre fixe de cases
        public static string[] Split(string Expression, int Count)
        {
            if (Expression == null || Expression == "") return new string[0];
            string Express = Expression;
            char Sep = Separator(ref Express);
            string[] Exp = Express.Split(Sep);
            string[] New_Exp = new string[Count];
            for (int I = 0; I < Count; I++)
            {
                New_Exp[I] = (I < Exp.Length - 1) ? Exp[I + 1] : "";
            }
            return New_Exp;
        }

        // Constitue une chaine splitable avec un s�parateur
        //   ascii peu commun ~
        public static string UnSplit(params string[] Strings)
        {
            if (Strings == null || Strings.Length == 0) return "";
            string Result = "~";
            foreach (string S in Strings)
            {
                Result += S + "~";
            }
            return Result;
        }

        public static IList<string> Split_List(this string Value, char Separator = ',', bool No_Empty = true, bool Trim = true)
        {
            var L = new List<string>();
            if (Value == null || Value.Length == 0) return L;
            foreach (var S in Value.Split(Separator))
            {
                var SS = Trim ? S.Trim() : S;
                if (No_Empty && SS.Length == 0) continue;
                L.Add(S);
            }
            return L;
        }

        public static void Disassemble(this string Value, string Separator, Action<string> On_Next, bool No_Empty = false)
        {
            var CC = Value.Scan();
            while (!CC.End)
            {
                var V = CC.Next(Separator);
                if (No_Empty && V.Trim() == "") continue;
                On_Next(V);
            }
        }

        public static string Stacked(this string Prefix, string Separator, params object[] Objects)
        {
            if (Objects == null || Objects.Length == 0) return "";
            StringBuilder SB = new StringBuilder();
            bool First = true;
            foreach (var S in Objects)
            {
                if (S == null || S as string == "") continue;
                SB.Append(First ? Prefix : Separator);
                SB.Append(S);
                First = false;
            }
            if (First) return "";
            return SB.ToString();
        }

        public static string Assembled(char Separator, params string[] Objects)
        {
            if (Objects == null || Objects.Length == 0) return "";
            StringBuilder SB = new StringBuilder();
            bool First = true;
            foreach (var S in Objects)
            {
                if (S == null) continue;
                if (First) First = false; else SB.Append(Separator);
                SB.Append(S);
            }
            return SB.ToString();
        }

        public static string Assembled<T>(char Separator, IEnumerable<T> Objects)
        {
            if (Objects == null) return "";
            StringBuilder SB = new StringBuilder();
            bool First = true;
            foreach (var S in Objects)
            {
                if (S == null) continue;
                if (First) First = false; else SB.Append(Separator);
                SB.Append(S);
            }
            return SB.ToString();
        }

        public static string Assembled<T>(string Separator, IEnumerable<T> Objects)
        {
            if (Objects == null) return "";
            StringBuilder SB = new StringBuilder();
            bool First = true;
            foreach (var S in Objects)
            {
                if (S == null) continue;
                if (First) First = false; else SB.Append(Separator);
                SB.Append(S);
            }
            return SB.ToString();
        }

        public static string Assembled_With<T>(this IEnumerable<T> Objects, string Separator)
        {
            if (Objects == null) return "";
            StringBuilder SB = new StringBuilder();
            bool First = true;
            foreach (var S in Objects)
            {
                if (S == null) continue;
                if (First) First = false; else SB.Append(Separator);
                SB.Append(S);
            }
            return SB.ToString();
        }

        public static string Replace(this string original,
                    string pattern, string replacement)
        {
            int count, position0, position1;
            count = position0 = position1 = 0;
            string upperString = original.ToUpper();
            string upperPattern = pattern.ToUpper();
            int inc = (original.Length / pattern.Length) *
                      (replacement.Length - pattern.Length);
            char[] chars = new char[original.Length + Math.Max(0, inc)];
            while ((position1 = upperString.IndexOf(upperPattern,
                                              position0)) != -1)
            {
                for (int i = position0; i < position1; ++i)
                    chars[count++] = original[i];
                for (int i = 0; i < replacement.Length; ++i)
                    chars[count++] = replacement[i];
                position0 = position1 + pattern.Length;
            }
            if (position0 == 0) return original;
            for (int i = position0; i < original.Length; ++i)
                chars[count++] = original[i];
            return new string(chars, 0, count);
        }

        // Remplace les \n et \t par leurs vrais �quivalents
        public static string Slash_Replaced(this string Text)
        {
            return Text.Replace("\\n", "\r\n").Replace("\\t", "\t");
        }

        // Parsing a la recherche de []
        public class Dic
        {
            public Cursor C;
            public string Same = "";
            public string Last_Key = "";

            public Dic(string Item)
            {
                C = new Cursor(Item);
            }

            public bool Is_Empty
            {
                get { return C.Max_Length == 0; }
            }

            public bool End
            {
                get { return C.End; }
            }

            string Next()
            {
                Same = C.Next("[");
                if (C.Found) C.Index--;
                while (Same.EndsWith(Environment.NewLine))
                {
                    Same = Same.Remove(Same.Length - Environment.NewLine.Length);
                }
                return Same;
            }
            public string this[string Key]
            {
                get
                {
                    switch (Key)
                    {
                        case ".":
                            return Same;
                        case "K":
                        case "k":
                            return Last_Key;
                        case "+":
                            C.Next("[");
                            Last_Key = C.Next("]");
                            return Next();
                        case "0":
                            C.Index = 0;
                            C.Next("[");
                            Last_Key = C.Next("]");
                            return Next();
                        case "$":
                            C.Next("[");
                            Last_Key = C.Next("]");
                            Same = C.Tail();
                            return Same;
                    }
                    C.Index = 0;
                    C.Next_Word("[" + Key + "]");
                    Last_Key = Key;
                    return Next();
                }
            }
        }

        public static Cursor Scan(this string Text)
        {
            return new Cursor(Text);
        }

        public class Cursor
        {
            public int Index;
            public int Found_Index;
            string Item;

            void Reset_Found()
            {
                _Found = false;
                Found_Index = -1;
            }

            void Set_Found(int At)
            {
                _Found = true;
                Found_Index = At;
            }

            public int Mark_Index = 0;

            public Cursor(string Item)
            {
                this.Item = Item == null ? "" : Item;
                this.Index = 0;
                Reset_Found();
            }

            public int Max_Length
            {
                get { return Item.Length; }
            }

            public void Mark()
            {
                Mark_Index = Index;
            }

            public void Rewind()
            {
                Index = Mark_Index;
            }

            bool _Found = false;
            public bool Found
            {
                get { return _Found; }
            }

            public void Goto(int New_Index)
            {
                Index = New_Index;
                if (Index >= Item.Length) Index = Item.Length;
            }

            public void Skip(int Count = 1)
            {
                Index += Count;
                if (Index >= Item.Length) Index = Item.Length;
            }

            public char Next_Char()
            {
                Reset_Found();
                if (Index >= Item.Length) return ' ';
                Index++;
                Set_Found(Index - 1);
                return Item[Found_Index];
            }

            public string Next_Line()
            {
                Reset_Found();
                int New_Index = this.Item.IndexOfAny("\n\r".ToCharArray(), this.Index);
                if (New_Index >= 0)
                {
                    string Result = Item.Substring(Index, New_Index - Index);
                    Index = New_Index + 1;
                    if (Index < Item.Length && Item.Substring(New_Index, 2) == "\r\n")
                    {
                        Index++;
                    }
                    Set_Found(New_Index);
                    return Result;
                }
                return this.Tail();
            }

            public string Next(string Pattern = "")
            {
                Reset_Found();
                int New_Index = this.Item.IndexOfAny(Pattern.ToCharArray(), this.Index);
                if (New_Index >= 0)
                {
                    string Result = Item.Substring(Index, New_Index - Index);
                    Index = New_Index + 1;
                    Set_Found(New_Index);
                    return Result;
                }
                return this.Tail();
            }

            // Recherche d'un terme sans ponctuation ni caract�re sp�ciaux
            public string Next_Term()
            {
                Reset_Found();
                int Start = Index;
                for (int I = Start; I < Item.Length; I++)
                {
                    if (char.IsLetterOrDigit(Item, I)) continue;
                    string Result = Item.Substring(Index, I - Index);
                    Index = I + 1;
                    Set_Found(Start);
                    return Result;
                }
                return this.Tail();
            }

            public T Next<T>(string Pattern = "")
            {
                return JR.Convert.To_Type<T>(Next(Pattern));
            }

            public string Next_Word(string Word)
            {
                Reset_Found();
                int New_Index = this.Item.IndexOf(Word, this.Index, StringComparison.InvariantCultureIgnoreCase);
                if (New_Index >= 0)
                {
                    string Result = Item.Substring(Index, New_Index - Index);
                    Index = New_Index + Word.Length;
                    Set_Found(New_Index);
                    return Result;
                }
                return this.Tail();
            }

            public string Next_Number()
            {
                Reset_Found();
                int Start = Index;
                bool Digit_Found = false;
                for (int I = Start; I < Item.Length; I++)
                {
                    if (char.IsDigit(Item, I))
                    {
                        if (!Digit_Found)
                        {
                            Start = I;
                            Digit_Found = true;
                        }
                        continue;
                    }
                    else if (!Digit_Found)
                    {
                        continue;
                    }
                    string Result = Item.Substring(Start, I - Start);
                    Index = I + 1;
                    Set_Found(Start);
                    return Result;
                }
                return this.Tail();
            }

            public string Between_Words(string Word1, string Word2)
            {
                Next_Word(Word1);
                return Next_Word(Word2);
            }

            public char Separator
            {
                get
                {
                    if (!Found) return '$';
                    return Item[Index - 1];
                }
            }

            public string Tail()
            {
                string Result = Item.Substring(Index, Item.Length - Index);
                Index = Item.Length;
                return Result;
            }

            public bool End
            {
                get
                {
                    return (Index >= Item.Length);
                }
            }

            public int Remaining
            {
                get { return Item.Length - Index; }
            }

            public string Range(int Deb, int End)
            {
                if (Deb > End) return "";
                if (Deb > Item.Length) return "";
                if (End >= Item.Length) return Item.Substring(Deb);
                return Item.Substring(Deb, End - Deb + 1);
            }

            public void Trim()
            {
                Item = Item.Trim();
            }
        }

        // Retourne la chaine trim�e ou non et default_value si "" ou null
        public static string If_Empty(this string Text, string Default_Value = "", bool Trim = true)
        {
            if (Text == null) return Default_Value;
            var T = Text;
            if (Trim) T = T.Trim();
            if (T == "") return Default_Value;
            return T;
        }

        // Retourne le texte instanci�
        //  Params est une s�rie Cle, Valeur, Cl�, Valeur
        public static string Generic_Text(this string Text, params object[] Params)
        {
            if (Params.Length == 0) return Text;
            if (Text == "") return "";
            string New_Text = Text;
            for (int I = 0; I < Params.Length; I += 2)
            {
                New_Text = New_Text.Replace("{" + (string)Params[I], "{" + (I + 1));
            }
            return String.Format(New_Text, Params);
        }

        // Retourne le texte instanci�
        //  le solver retourne null pour un parametre qu'il ne connait pas
        public static string Generic_Text(this string Text, Func<string, string> Solver, char Open_Sep = '{', char Close_Sep = '}')
        {
            if (Text == "") return "";
            if (Text.IndexOf(Open_Sep) < 0) return Text;
            StringBuilder S = new StringBuilder(1000);
            StringBuilder P = new StringBuilder(100);
            bool Inside = false;
            foreach (char C in Text)
            {
                if (C == Open_Sep)
                {
                    if (Inside)  // restituer le morceau qu'on avait pris pour un parametre
                    {
                        S.Append(Open_Sep);
                        S.Append(P);
                        P.Clear();
                    }
                    Inside = true;
                }
                else if (C == Close_Sep)
                {
                    if (!Inside)
                    {
                        S.Append(Close_Sep);
                    }
                    else
                    {
                        if (P.Length == 0)
                        {
                            S.Append(Open_Sep);
                            S.Append(Close_Sep);
                        }
                        else
                        {
                            string Val = Solver(P.ToString().Trim());
                            if (Val != null)
                            {
                                S.Append(Val);
                            }
                            else
                            {
                                // restituer le morceau du parametre refus�
                                S.Append(Open_Sep);
                                S.Append(P);
                                S.Append(Close_Sep);
                            }
                        }
                    }
                    P.Clear();
                    Inside = false;
                }
                else
                {
                    if (Inside) P.Append(C); else S.Append(C);
                }
            }
            return S.ToString();
        }

        // Retourne le texte instanci�
        //  le solver retourne null pour un parametre qu'il ne connait pas
        // Retourne une chaine vide en cas d'erreur
        // OU si Empty_If_Default et qu'un des parametres est une valeur par default
        // Si la chaine format commence par [�fr-fr] ou autre, utiliser la culture indiqu�e
        //        CultureInfo Culture = new CultureInfo(Page_State.Session.French?"fr-FR":"en-US");
        //return string.Format (Culture,  "{0:" + Format + "}" ,Value);

        public static string Generic_Format(this string Format, Func<string, object> Solver, bool Empty_If_default = false)
        {
            if (Format == "") return "";
            if (Format.IndexOf('{') < 0) return Format;
            CultureInfo Culture = null; ;
            if (Format.StartsWith("[�"))
            {
                var C = Format.Scan();
                C.Goto(2);
                string Cult = C.Next("]");
                try
                {
                    Culture = new CultureInfo(Cult);
                }
                catch { };
                Format = C.Tail();
            }
            StringBuilder S = new StringBuilder(1000);
            StringBuilder P = new StringBuilder(100);
            List<object> Params = new List<object>();
            bool Inside = false;
            foreach (char C in Format)
            {

                switch (C)
                {
                    case '{':
                        {
                            if (Inside)  // restituer le morceau qu'on avait pris pour un parametre
                            {
                                S.Append(C);
                                S.Append(P);
                                P.Clear();
                            }
                            Inside = true;
                        }
                        break;
                    case '}':
                    case ':':
                        {
                            if (!Inside)
                            {
                                S.Append(C);
                            }
                            else
                            {
                                object Val = Solver(P.ToString());
                                if (Val != null)
                                {
                                    if (Empty_If_default && JR.Convert.Is_Default(Val)) return "";
                                    S.Append('{');
                                    S.Append(Params.Count.ToString());
                                    S.Append(C);
                                    Params.Add(Val);
                                }
                                else
                                {
                                    if (Empty_If_default) return "";
                                    // restituer le morceau du parametre refus�
                                    S.Append('{');
                                    S.Append(P);
                                    S.Append(C);
                                }
                            }
                            P.Clear();
                            Inside = false;
                        }
                        break;
                    default:
                        {
                            if (Inside) P.Append(C); else S.Append(C);
                        }
                        break;
                }
            }
            if (Params.Count == 0) return Format;
            try
            {
                if (Culture != null)
                {
                    return string.Format(Culture, S.ToString(), Params.ToArray());
                }
                return string.Format(S.ToString(), Params.ToArray());
            }
            catch { return ""; }
        }

        // Encodage hexa d'une chaine
        public static string Encode(this string Input)
        {
            return To_Hexa(Input);
            //UnicodeEncoding encoder = new UnicodeEncoding();
            //// Convert the byte array to a base 64 encoded string.
            //return System.Convert.ToBase64String(encoder.GetBytes(Input));
        }

        // Decodage hexa d'une chaine
        public static string Decode(this string Input)
        {
            return From_Hexa(Input);
            //UnicodeEncoding encoder = new UnicodeEncoding();
            //byte[] bytes = System.Convert.FromBase64String(Input);

            //// Convert byte array back to the original string.
            //return encoder.GetString(bytes, 0, bytes.Length); 
        }

        // Hash an input string and return the hash as
        // a 32 character hexadecimal string.
        public static string Get_MD5(this string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5 md5Hasher = MD5.Create();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public static string Get_SHA1(this string input)
        {
            SHA1 shaHasher = SHA1.Create();
            // Convert the input string to a byte array and compute the hash.
            byte[] data = shaHasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public static string To_Hexa(this string input)
        {
            StringBuilder SB = new StringBuilder();
            foreach (byte B in Encoding.Default.GetBytes(input))
            {
                SB.Append(B.ToString("x2"));
            }

            // Return the hexadecimal string.
            return SB.ToString();
        }

        public static string From_Hexa(this string input)
        {
            try
            {
                byte[] Bytes = new byte[input.Length / 2];
                for (int I = 0; I < Bytes.Length; I++)
                {
                    Bytes[I] = byte.Parse(input.Substring(2 * I, 2), System.Globalization.NumberStyles.HexNumber);
                }
                return Encoding.Default.GetString(Bytes);
            }
            catch
            {
                return "";
            }
        }

        //const string ASCII_32_126 =  " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
        //      const string ASCII_160_255 = " ������������-�����������������������������������������������������������������������������������";
        const string ASCII_144_255 = "i`'^~-^.^..o,',' !cLoY|S^ca<_-R-o+23'u!.,10>423?AAAAAAACEEEEIIIIDNOOOOOxOUUUUYDBaaaaaaaceeeeiiiionooooo/ouuuuypy";
        public static string To_ASCII(this string Text)
        {
            if (Text.Length == 0) return "";
            var C = new StringBuilder();
            foreach (var Car in Text)
            {
                var Code = (int)Car;
                if (Code < 127) { C.Append(Car); continue; }
                if (Code < 144) { C.Append('.'); continue; }
                switch (Car)  // Cas sp�ciaux
                {
                    case '�': C.Append('\''); continue;
                    case '�': C.Append("AE"); continue;
                    case '�': C.Append("ae"); continue;
                    case '�': C.Append('E'); continue;
                    case '�': C.Append("oe"); continue;
                    case '�': C.Append("OE"); continue;
                    case '�': C.Append("1/4"); continue;
                    case '�': C.Append("1/2"); continue;
                    case '�': C.Append("3/4"); continue;
                }
                if (Code <= 255) { C.Append(ASCII_144_255[Code - 144]); continue; };
                C.Append('.');
            }
            return C.ToString();
        }

        const string Accent = "���������������������������";
        const string Lower = @"aaaaaaooooooeeeeiiiiuuuuync";
        public static string To_Letters(this string Text, bool Keep_Specials)
        {
            char[] C = new char[Text.Length];
            int I = 0;
            foreach (char CC in Text)
            {
                if (!char.IsLetterOrDigit(CC))
                {
                    if (!Keep_Specials) continue;
                    C[I] = CC;
                }
                else
                {
                    C[I] = char.ToLower(CC);
                    if ((int)C[I] > 127)
                    {
                        int R = Accent.IndexOf(C[I]);
                        if (R >= 0)
                        {
                            C[I] = Lower[R];
                        }
                    }
                }
                I++;
            }
            return new string(C, 0, I);
        }
        public static string To_Letters(this string Text)
        {
            return To_Letters(Text, false);
        }

        public static IList<string> Normalizes_Words(this string Text)
        {
            var Words = new List<string>();
            char[] C = new char[Text.Length];
            int I = 0;
            Action Push = () =>
            {
                if (I == 0) return;
                Words.Add(new string(C, 0, I));
                I = 0;
            };
            foreach (char CC in Text)
            {
                if (!char.IsLetterOrDigit(CC))
                {
                    Push();
                }
                else
                {
                    C[I] = char.ToLower(CC);
                    if ((int)C[I] > 127)
                    {
                        int R = Accent.IndexOf(C[I]);
                        if (R >= 0)
                        {
                            C[I] = Lower[R];
                        }
                    }
                    I++;
                }
            }
            Push();
            return Words;
        }

        public static string Normalize(this string Text, char Separator = '_', bool Compact_Separators = false)
        {
            char[] C = new char[Text.Length];
            int I = 0;
            bool Prev_Is_Separator = false;
            foreach (char CC in Text)
            {
                if (!char.IsLetterOrDigit(CC))
                {
                    if (Compact_Separators && Prev_Is_Separator) continue;
                    C[I] = Separator;
                    Prev_Is_Separator = true;
                }
                else
                {
                    Prev_Is_Separator = false;
                    C[I] = char.ToLower(CC);
                    if ((int)C[I] > 127)
                    {
                        int R = Accent.IndexOf(C[I]);
                        if (R >= 0)
                        {
                            C[I] = Lower[R];
                        }
                    }
                }
                I++;
            }
            return new string(C, 0, I);
        }

        // Compare deux chaines par les majuscules uniquement
        public static bool Same_Letters(this string Text1, string Text2)
        {
            return To_Letters(Text1) == To_Letters(Text2);
        }

        // Compare deux chaines par les majuscules uniquement et retourne le nombre de lettres identiques
        // Retourne nb lettres ok si comparaison OK
        // REtuorne - nb lettres sinon
        public static int Compare_Letters(this string Text1, string Text2)
        {
            if (Text1.Length == 0) return 0;
            if (Text2.Length == 0) return 0;
            int Ok_Count = 0;
            int IT1 = 0;
            int IT2 = 0;

            while (IT1 < Text1.Length)
            {
                char C1 = Text1[IT1];
                IT1++;
                if (!char.IsLetterOrDigit(C1)) continue;
                bool Ok = false;
                while (IT2 < Text2.Length)
                {
                    char C2 = Text2[IT2];
                    IT2++;
                    if (!char.IsLetterOrDigit(C2)) continue;
                    if (C1 == C2)
                    {
                        Ok = true;
                        break;
                    }
                    C1 = char.ToLower(C1);
                    C2 = char.ToLower(C2);
                    if ((int)C1 > 127)
                    {
                        int R = Accent.IndexOf(C1);
                        if (R >= 0)
                        {
                            C1 = Lower[R];
                        }
                    }
                    if ((int)C2 > 127)
                    {
                        int R = Accent.IndexOf(C2);
                        if (R >= 0)
                        {
                            C2 = Lower[R];
                        }
                    }
                    if (C1 == C2)
                    {
                        Ok = true;
                    }
                    break;
                }
                if (!Ok) return -Ok_Count;
                Ok_Count++;
            }
            if (IT2 < Text2.Length) return -Ok_Count;
            return Ok_Count;
        }

        public static IEnumerable<string> Extract_Words(this string Text, bool Keep_Specials = false)
        {
            var Words = new List<string>();
            char[] C = new char[Text.Length];
            int Count = 0;
            Action Flush = () =>
                {
                    if (Count == 0) return;
                    Words.Add(new string(C, 0, Count));
                    Count = 0;
                };
            foreach (char CC in Text)
            {
                if (char.IsWhiteSpace(CC) || char.IsPunctuation(CC))
                {
                    Flush();
                    continue;
                }
                else if (!char.IsLetterOrDigit(CC))
                {
                    if (!Keep_Specials)
                    {
                        Flush();
                        continue;
                    }
                    C[Count] = CC;
                }
                else
                {
                    C[Count] = char.ToLower(CC);
                    if ((int)C[Count] > 127)
                    {
                        int R = Accent.IndexOf(C[Count]);
                        if (R >= 0)
                        {
                            C[Count] = Lower[R];
                        }
                    }
                }
                Count++;
            }
            Flush();
            return Words;
        }

        public static string Remove_All(this string From, params string[] Strings)
        {
            string S = From;
            foreach (string R in Strings)
            {
                if (R == "") continue;
                int I = 0;
                while (I >= 0)
                {
                    I = S.IndexOf(R, I, StringComparison.InvariantCultureIgnoreCase);
                    if (I >= 0) S = S.Remove(I, R.Length);
                }
            }
            return S;
        }


        public static string Keep_Digits(this string Value)
        {
            StringBuilder SB = new StringBuilder();
            foreach (char C in Value)
            {
                if (char.IsDigit(C)) SB.Append(C);
            }
            return SB.ToString();
        }

        // Supprime les caract�res non imprimables qui perturbent l'affichage
        public static string Printable(this string Value)
        {
            char[] C = new char[Value.Length];
            int I = 0;
            foreach (char CC in Value)
            {
                int Code = (int)CC;
                if (Code <= 0x08)
                {
                    continue;
                }
                if (Code <= 0x1B && Code >= 0x0E)
                {
                    continue;
                }
                C[I] = CC;
                I++;
            }
            return new string(C, 0, I);
        }

        public static IList<string> Read_Lines(this string Source, bool Remove_Empty = false)
        {
            List<string> L = new List<string>();
            if (Source == null) return L;
            using (StringReader SR = new StringReader(Source))
            {
                while (true)
                {
                    string S = SR.ReadLine();
                    if (S == null) break;
                    if (Remove_Empty && S == "") continue;
                    L.Add(S);
                }
            }
            return L;
        }

        public static string Lang_Format(this string Lang, string Format, params object[] Args)
        {
            CultureInfo CI;
            switch (Lang.ToUpper())
            {
                case "FR":
                    CI = CultureInfo.GetCultureInfo("fr-FR");
                    break;
                case "ES":
                    CI = CultureInfo.GetCultureInfo("es-ES");
                    break;
                case "IT":
                    CI = CultureInfo.GetCultureInfo("it-IT");
                    break;
                case "DE":
                    CI = CultureInfo.GetCultureInfo("de-DE");
                    break;
                case "BE":
                    CI = CultureInfo.GetCultureInfo("nl-BE");
                    break;
                default:
                    CI = CultureInfo.GetCultureInfo("en-US");
                    break;
            }
            return string.Format(CI, Format, Args);
        }


        public static string To_Plain_Text(this string htmlString)
        {
            string htmlTagPattern = "<.*?>";
            var regexCss = new Regex("(\\<script(.+?)\\</script\\>)|(\\<style(.+?)\\</style\\>)", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            htmlString = regexCss.Replace(htmlString, string.Empty);
            htmlString = Regex.Replace(htmlString, htmlTagPattern, string.Empty);
            htmlString = Regex.Replace(htmlString, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
            htmlString = htmlString.Replace("&nbsp;", string.Empty);

            return htmlString;
        }

        public static string To_Js(this string Value)
        {
            return Value.Replace("'", @"\'").Replace(@"""", @"\""");
        }

        public static string Encoded(this object Value)
        {
            if (Value == null) return "N";
            if (Value is int || Value is sbyte || Value is short) return "I" + Value.ToString();
            if (Value is byte || Value is ushort || Value is uint || Value is ulong) return "U" + Value.ToString();
            if (Value is long) return "L" + Value.ToString();
            if (Value is double) return "F" + ((double)Value).ToString("G15", CultureInfo.InvariantCulture);    // La pr�cision G17 est trop importante et g�n�re des chiffres non significatifs
            if (Value is decimal) return "D" + Value.ToString();
            if (Value is bool) return "B" + ((bool)Value ? "1" : "0");
            if (Value is string) return "S" + Value;
            if (Value is Guid) return "G" + ((Guid)Value).ToString("N");
            if (Value is DateTime) return "T" + ((DateTime)Value).ToString("O", CultureInfo.InvariantCulture);
            return "O" + Value;
        }

        public static object Decoded(this string Text)
        {
            if (Text == null || Text.Length < 2) return null;
            var V = Text.Substring(1);
            switch (Text[0])
            {
                case 'I': return int.Parse(V);
                case 'L': return long.Parse(V);
                case 'U': return ulong.Parse(V);
                case 'F': return double.Parse(V, CultureInfo.InvariantCulture);
                case 'D': return decimal.Parse(V);
                case 'B': return V == "1";
                case 'S': return V;
                case 'G': return Guid.Parse(V);
                case 'T': return DateTime.ParseExact(V, "O", CultureInfo.InvariantCulture);
            }
            return V;
        }


        public static string Spaced_Number(this string Number, string Space = " ")
        {
            if (Number.Length < 4) return Number;
            // Localiser le point ou la virgule
            char Sign = ' ';
            char Dot = ' ';
            StringBuilder Left = new();
            StringBuilder Right = new();
            int Right_Count = 0;
            void Push(char C)
            {
                if (Dot == ' ')
                {
                    Left.Append(C);
                }
                else
                {
                    if (Right_Count > 0 && Right_Count % 3 == 0)
                    {
                        Right.Append(Space);
                    }
                    Right.Append(C);
                    Right_Count++;
                }

            }
            foreach (var C in Number)
            {
                switch (C)
                {
                    case '-':
                    case '+':
                        if (Sign == ' ') Sign = C;
                        break;
                    case '.':
                    case ',':
                        Dot = '.';
                        break;
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        Push(C);
                        break;
                }
            }

            StringBuilder SB = new();
            if (Sign != ' ') SB.Append(Sign);
            if (Left.Length < 4)
            {
                SB.Append(Left);
            }
            else
            {
                int I = 3 - (Left.Length % 3) ;
                bool One_Left = false;
                foreach (var C in Left.ToString())
                {
                    if (One_Left && I % 3 == 0) SB.Append(Space);
                    SB.Append(C);
                    I++;
                    One_Left = true;
                }
            }
            if (Dot != ' ') SB.Append(Dot);
            SB.Append(Right);
            return SB.ToString();
        }
    }

}

