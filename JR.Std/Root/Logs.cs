using System;
using System.Diagnostics;
using System.IO;

namespace JR
{
	/// <summary>
	/// Description r�sum�e de Logs.
	/// </summary>
	public class Logs
	{
        // Different levels of message
        public enum Severity { Trace, Message, Warning, Error, Fatal, Forced }


        private Logs()
		{
		}

		static string _Log_File;
		public static void Set_Default_Log (string File)
		{
			try
			{
				JR.Files.Safe_Copy (File + ".log" , File + ".old_log");
			} 
			catch {};
			_Log_File = File +".log";
		}

		public static void Post (string Message, Severity Severity, string Module)
		{
			string Mess = "[" + Severity.ToString() + "] " + Message + " (" + Module + ")" ;
			Trace.WriteLine (Mess);
			if (_Log_File != null && _Log_File != "")
			{
				System.IO.StreamWriter W = System.IO.File.AppendText (_Log_File);
				W.WriteLine (Mess);
				W.Flush ();
				W.Close ();
			};
		}

		public static void Tip_Top (string Format, params object [] Params)
		{
            string Mess = DateTime.Now.ToLongTimeString () + " - " + string.Format (Format, Params);
			System.IO.StreamWriter W = System.IO.File.AppendText ("c:\\TOP.TXT");
			W.WriteLine (Mess);
            W.Flush();
            W.Close  ();
		}
	}

}
