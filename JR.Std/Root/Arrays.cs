using System;
using System.Collections;

namespace JR
{
    public class Circular_Array<T>
    {
        T[] Items;
        int End_Index = -1;
        public int Size = 0;
        public int Max_Size;

        public void Push(T Item)
        {
            if (Size < Max_Size) Size++;
            End_Index++;
            if (End_Index == Max_Size) End_Index = 0;
            Items[End_Index] = Item;
        }

        public T Push_Reuse(Func<T> New_Item)
        {
            if (Size < Max_Size) Size++;
            End_Index++;
            if (End_Index == Max_Size) End_Index = 0;
            if (Items[End_Index] == null) Items[End_Index] = New_Item();
            return Items[End_Index];
        }

        public Circular_Array(int Max_Size)
        {
            this.Max_Size = Max_Size;
            Items = new T[Max_Size];
        }

        public T Current
        {
            get { return Size > 0 ? Items[End_Index] : default(T); }
        }

        public T Item(int Pos)
        {
            if (Size == 0) return default(T);
            var Real_Index = End_Index - Pos;
            return Real_Index < 0 ? Items[Real_Index + Size] : Items[Real_Index];
        }

        public void Clear()
        {
            Size = 0;
            Items = new T[Max_Size];
        }
    }

}
