using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Threading;

namespace JR.Mail
{
    public interface I_Mail
    {
        string Subject { get; }
        string Body { get; }
        string To { get; }
        string From { get; }
        string Sender { get; }
        IEnumerable<string> Attachments { get; }
    }
}
