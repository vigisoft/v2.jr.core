﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JR
{

    // Timer précis qui ne revient jamais en arrière
    public class Stamp_Clock
    {

        public bool Auto_Sync;
        long Current_Stamp;
        DateTime Start;
        long Start_Stamp;
        double Freq;

        public long Sync()
        {
            Current_Stamp = Stopwatch.GetTimestamp();
            return Current_Stamp;
        }

        public long Get_Stamp(TimeSpan Duration)
        {
            return Get_Stamp (Duration.TotalSeconds);
        }

        public long Get_Stamp(double Seconds = 0)
        {
            if (Auto_Sync) Sync();
            return Current_Stamp + (long)(Freq * Seconds);
        }

        public long Add_Stamp(long Stamp, double Seconds)
        {
            return Stamp + (long)(Freq * Seconds);
        }

        public long Add_Stamp(long Stamp, TimeSpan Duration)
        {
            return Add_Stamp (Stamp, Duration.TotalSeconds);
        }

        public bool Elapsed(long Stamp)
        {
            if (Stamp == 0) return false;
            if (Auto_Sync) Sync();
            return Stamp < Current_Stamp;
        }

        public bool Null_Or_Elapsed(long Stamp)
        {
            if (Stamp == 0) return true;
            if (Auto_Sync) Sync();
            return Stamp < Current_Stamp;
        }

        public DateTime Now
        {
            get
            {
                if (Auto_Sync) Sync();
                return Start.AddSeconds( ((double)(Current_Stamp - Start_Stamp))/Freq);
            }
        }

        public Stamp_Clock(bool Auto_Sync = false)
        {
            this.Auto_Sync = Auto_Sync;
            Current_Stamp = Stopwatch.GetTimestamp();
            Start = DateTime.Now;
            Start_Stamp = Current_Stamp;
            Freq = Stopwatch.Frequency;
        }
    }
}
