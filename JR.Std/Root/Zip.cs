using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Compression;
using System.IO;

namespace JR
{
    public class Zip
    {
        public static Boolean Create_Zip(IEnumerable<string> Files, string Zip_File, bool No_Compression = false)
        {
            if (System.IO.File.Exists(Zip_File)) return false;
            try
            {
                using (var zip = ZipFile.Open(Zip_File, ZipArchiveMode.Create))
                {
                    foreach (String filename in Files)
                    {
                        zip.CreateEntryFromFile(filename, Path.GetFileName(filename), No_Compression ? CompressionLevel.NoCompression : CompressionLevel.Fastest);
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static byte [] Zipped(byte [] Source)
        {
            using (MemoryStream MS = new System.IO.MemoryStream())
            {
                using (Stream CS = new System.IO.Compression.DeflateStream(MS, System.IO.Compression.CompressionMode.Compress, true))
                {
                    CS.Write(Source, 0, Source.Length);
                }
                return MS.ToArray();
            }
        }

        public static byte [] Unzipped(byte [] Source)
        {
            using (MemoryStream MS = new System.IO.MemoryStream(Source))
            {
                using (Stream CS = new System.IO.Compression.DeflateStream(MS, System.IO.Compression.CompressionMode.Decompress))
                {
                    using (MemoryStream MD = new System.IO.MemoryStream())
                    {
                        CS.CopyTo(MD);
                        return MD.ToArray();
                    }
                }
            }
        }


    }
}
