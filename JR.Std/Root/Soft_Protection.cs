﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;
using System.Security.Cryptography;
using System.Net;
using System.IO;
using JR.Saf;
using System.Web;

namespace JR
{
    public class Soft_Protection
    {

        public static string Get_Machine_Id
        {
            get
            {
                // Le machine Id est composé des ID du disque C, de la premiere MAC Address, du processeur, le tout haché
                string Disk_Serial = JR.Strings.Get_MD5 (GetVolumeCSerial).Substring (0,16);
                string MAC_Serial = JR.Strings.Get_MD5(GetMACAddress).Substring(0, 16);
                string CPU_Serial = JR.Strings.Get_MD5(GetCPUId).Substring(0, 16);
                return string.Format ("{0}-{1}-{2}", Disk_Serial, MAC_Serial, CPU_Serial);
            }
        }

        public static string GetVolumeCSerial
        {
            get
            {
                using (ManagementObject disk = new ManagementObject(@"win32_logicaldisk.deviceid=""C:"""))
                {
                    disk.Get();
                    return disk["VolumeSerialNumber"].ToString();
                }
            }
        }

        public static string GetMACAddress
        {
            get
            {
                using (ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration"))
                {
                    using (ManagementObjectCollection moc = mc.GetInstances())
                    {
                        foreach (ManagementObject mo in moc)
                        {
                            if ((bool)mo["IPEnabled"]) return mo["MacAddress"].ToString().Replace(":", "");
                        }
                    }
                }
                return "";
            }
        }

        public static string GetCPUId
        {
            get
            {
                using (ManagementClass mc = new ManagementClass("Win32_Processor"))
                {
                    using (ManagementObjectCollection moc = mc.GetInstances())
                    {
                        foreach (ManagementObject mo in moc)
                        {
                            object Val = mo.Properties["ProcessorId"].Value;
                            if (Val == null) return "none";
                            return Val.ToString();
                        }
                    }
                }
                return "";
            }
        }

        public string Gen_License(string Machine_Id)
        {
            string[] Ids = Machine_Id.Split('-');
            MD5 md5Hasher = MD5.Create();
            StringBuilder License = new StringBuilder();
            foreach (string S in Machine_Id.Split('-'))
            {
                License.Append(License_Part(S));
            }
            return License.ToString();
        }

        public string License_Part(string Id)
        {
            return License_Part(Id, "");
        }

        public string License_Part(string Id, string Mode)
        {
            MD5 md5Hasher = MD5.Create();
            return System.Convert.ToBase64String(md5Hasher.ComputeHash(Encoding.Default.GetBytes(_HKey + Mode + Id)));
        }

        public string Current_Machine_Id { get; set; }
        public string Current_License_Status_Title {get;set;}
        public bool Current_License_Is_Valid {get;set;}
        public string Current_License_Key { get; set; }

        public bool Register_License_Key (string Key)
        {
            Current_License_Key = Key;
            int Matches = 0;
            foreach (string S in Current_Machine_Id.Split('-'))
            {
                if (Key.Contains (License_Part(S))) Matches++;
            }
            Current_License_Is_Valid = Matches > 1;
            Current_License_Status_Title = Key == ""? "Logiciel non activé":Matches > 1?"Logiciel activé":"Activation incorrecte";

            // Essayer une activation via le net
            if (!Current_License_Is_Valid)
            {
                Matches = 0;
                foreach (string S in Current_Machine_Id.Split('-'))
                {
                    if (Key.Contains(License_Part(S, Net_Activation))) Matches++;
                }
                if (Matches < 2) return false;

                // Appel du server de licence qui doit renvoyer la licence encryptée avec 
                string Response = Get_License_Server_Key(Key);
                if (Response == JR.Strings.Get_MD5("ViGiSoFt" + Key))
                {
                    Current_License_Is_Valid = true;
                    Current_License_Status_Title = "Logiciel activé par internet";
                }
            }
            
            return Current_License_Is_Valid;
        }

        private string Get_License_Server_Key(string Key)
        {
            string Url = string.Format(Main.Parameter("Licenser_URL"), HttpUtility.UrlEncode(Key));
            try
            {
                Saf.Logs.Log("Sending license request : {0}", Url);
                HttpWebRequest Request = (HttpWebRequest)WebRequest.Create(Url);
                using (HttpWebResponse Response = (HttpWebResponse)Request.GetResponse())
                {
                    using (StreamReader SR = new StreamReader(Response.GetResponseStream(), Encoding.Default))
                    {
                        string License = SR.ReadToEnd();
                        JR.Saf.Logs.Log("License received : {0}", License);
                        return License;
                    }
                }
            }
            catch
            {
                Saf.Logs.Log("license request failure");
            }
            return "";
        }


        public string [] Get_Update_Server_Release
        {
            get
            {
                string Url = string.Format(Main.Parameter("Updater_URL"), HttpUtility.UrlEncode(Current_License_Key));
                try
                {
                    Saf.Logs.Log("Sending release request : {0}", Url);
                    HttpWebRequest Request = (HttpWebRequest)WebRequest.Create(Url);
                    using (HttpWebResponse Response = (HttpWebResponse)Request.GetResponse())
                    {
                        using (StreamReader SR = new StreamReader(Response.GetResponseStream(), Encoding.Default))
                        {
                            string License = SR.ReadToEnd();
                            JR.Saf.Logs.Log("Release received : {0}", License);
                            return JR.Strings.Split(License); // Retourne la version et l'URL de download
                        }
                    }
                }
                catch
                {
                    Saf.Logs.Log("release request failure");
                }
                return new string[] { "", "" };
            }
        }

        string _HKey = "";
        public Soft_Protection(string HKey)
        {
            _HKey = HKey;
            Current_Machine_Id = Get_Machine_Id;
        }

        public const string Net_Activation = "$N";
    }
}


