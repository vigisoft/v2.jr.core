﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JR
{
    public class Bag:Dictionary<string, object>
    {
        //Dictionary<string, object> _Depot = new Dictionary<string, object>();
        public new object this[string Index]
        {
            get
            {
                object Value = null;
                TryGetValue(Index, out Value);
                return Value;
            }

            set
            {
                if (value == null)
                {
                    Remove(Index);
                    return;
                }
                base[Index] = value;
            }
        }

        public string String(string Index)
        {
            object O = this[Index];
            return O == null ? "" : O.ToString();
        }

        public bool Bool(string Index)
        {
            object O = this[Index];
            return O == null ? false : JR.Convert.To_Boolean(O);
        }

        public int Int(string Index)
        {
            object O = this[Index];
            return O == null ? 0 : JR.Convert.To_Integer(O);
        }

        public Guid Guid(string Index)
        {
            object O = this[Index];
            return O == null ? System.Guid.Empty : JR.Convert.To_Guid(O);
        }

        public static Bag From_Json (string Json)
        {
            var B = new Bag();
            JR.Json.Populate(B, Json);
            return B;
        }
    }
}
