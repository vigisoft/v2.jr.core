﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Web;

namespace JR
{
    public static class Xml_Extensions
    {
        public static string A_String(this XmlNode Node, string Attribute)
        {
            var V = Node.Attributes[Attribute];
            return V == null ? "" : HttpUtility.HtmlDecode(V.Value);
        }
        public static string A_Value(this XmlNode Node, string Attribute)
        {
            var V = Node.Attributes[Attribute];
            return V == null ? "" : V.Value;
        }
        public static int A_Integer(this XmlNode Node, string Attribute)
        {
            var V = Node.Attributes[Attribute];
            return V == null ? 0 : V.Value.To_Integer();
        }
        public static long A_Long(this XmlNode Node, string Attribute)
        {
            var V = Node.Attributes[Attribute];
            return V == null ? 0 : V.Value.To_Long();
        }
        public static bool A_Boolean(this XmlNode Node, string Attribute)
        {
            var V = Node.Attributes[Attribute];
            return V == null ? false : V.Value.To_Boolean();
        }
        public static double A_Float(this XmlNode Node, string Attribute)
        {
            var V = Node.Attributes[Attribute];
            return V == null ? 0.0 : V.Value.To_Float();
        }
        public static DateTime A_Time (this XmlNode Node, string Attribute, params string [] Formats)
        {
            var V = Node.Attributes[Attribute];
            if (V == null) return DateTime.MinValue;
            return V.Value.To_Format_Date (Formats);
        }
        public static string N_String(this XmlNode Node, string Attribute = "")
        {
            var V = Attribute == "" ? Node : Node[Attribute];
            return V == null ? "" : HttpUtility.HtmlDecode(V.InnerText);
        }
        public static string N_Value(this XmlNode Node, string Attribute = "")
        {
            var V = Attribute == "" ? Node : Node[Attribute];
            return V == null ? "" : V.InnerText;
        }
        public static int N_Integer(this XmlNode Node, string Attribute = "")
        {
            var V = Attribute == "" ? Node : Node[Attribute];
            return V == null ? 0 : V.InnerXml.To_Integer();
        }
        public static long N_Long(this XmlNode Node, string Attribute = "")
        {
            var V = Attribute == "" ? Node : Node[Attribute];
            return V == null ? 0 : V.InnerXml.To_Long();
        }
        public static double N_Float(this XmlNode Node, string Attribute = "")
        {
            var V = Attribute == "" ? Node : Node[Attribute];
            return V == null ? 0 : V.InnerXml.To_Float();
        }
        public static DateTime N_Time(this XmlNode Node, string Attribute, params string[] Formats)
        {
            var V = Attribute == "" ? Node : Node[Attribute];
            return V == null ? DateTime.MinValue : V.InnerXml.To_Format_Date(Formats);
        }
        public static bool N_Boolean(this XmlNode Node, string Attribute)
        {
            var V = Attribute == "" ? Node : Node[Attribute];
            return V == null ? false : V.InnerXml.To_Boolean();
        }
    }

}
