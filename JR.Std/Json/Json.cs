using Newtonsoft.Json;

namespace JR
{
    // CLasse de serialisation
    public static class Json
    {
        static JsonSerializerSettings JSon_Settings = new JsonSerializerSettings
        {
            DefaultValueHandling = DefaultValueHandling.Ignore,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            Formatting = Formatting.None
        };
        static JsonSerializerSettings JSon_Settings_Indented = new JsonSerializerSettings
        {
            DefaultValueHandling = DefaultValueHandling.Ignore,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            Formatting = Formatting.Indented
        };
        public static T From_Json<T>(this string Json)
        {
            T Result = default(T);
            if ((Json ?? "") == "") return Result;

            // Deserialiser
            try
            {
                Result = JsonConvert.DeserializeObject<T>(Json);
            }
            catch
            {
            }
            return Result;
        }

        // Overload pour anonymous types
        public static T From_Json<T>(this string Json, T Definition)
        {
            T Result = Definition;
            if ((Json ?? "") == "") return Result;

            // Deserialiser
            try
            {
                Result = JsonConvert.DeserializeAnonymousType(Json, Definition);
            }
            catch
            {
            }
            return Result;
        }

        public static bool Populate(this object Object, string Json)
        {
            if (Object == null) return false;
            if ((Json ?? "") == "") return false;

            // Deserialiser
            try
            {
                JsonConvert.PopulateObject(Json, Object);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static string To_Json(this object Object, bool Indented = false, bool Include_Defaults = false)
        {
            if (Object == null) return "";
            try
            {
                var Settings = new JsonSerializerSettings
                {
                    DefaultValueHandling = Include_Defaults ? DefaultValueHandling.Include : DefaultValueHandling.Ignore,
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    Formatting = Indented ? Formatting.Indented : Formatting.None
                };
                return JsonConvert.SerializeObject(Object, Settings);
            }
            catch
            {
            }
            return "";
        }
    }
}
