using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace JR
{

    public interface I_Empty
    {
        bool Is_Empty { get; }
    }

    // CLasse de serialisation
    public class Serial<T>
    {
        public static T From_Xml(string Xml)
        {
            T Result = default(T);
            if (Xml == "") return Result;

            // Deserialiser
            using (MemoryStream MS = new MemoryStream (Encoding.Default.GetBytes (Xml), false))
            try
            {
                XmlSerializer S = new XmlSerializer(typeof(T));
                Result = (T)S.Deserialize(MS);
            }
            catch
            {
            }
            finally
            {
                MS.Close();
            }
            return Result;
        }

        public static string To_Xml (T Object)
        {
            if (Object == null) return "";
            using (MemoryStream MS = new MemoryStream())
                try
                {
                    XmlSerializer S = new XmlSerializer(typeof(T));
                    S.Serialize(MS, Object);
                    string Xml = Encoding.Default.GetString (MS.ToArray());
                    MS.Close();
                    return Xml;
                }
                catch
                {
                    MS.Close();
                }
            return "";
        }

        static JsonSerializerSettings JSon_Settings = new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore, ReferenceLoopHandling = ReferenceLoopHandling.Ignore };
        public static T From_Json(string Json)
        {
            T Result = default(T);
            if ((Json ?? "") == "") return Result;

            // Deserialiser
            try
            {
                Result = JsonConvert.DeserializeObject<T>(Json);
            }
            catch
            {
            }
            return Result;
        }

        public static bool Populate_Json(T Object, string Json)
        {
            if (Object == null) return false;
            if (Json == null) return false;
            if (Json == "") return false;

            // Deserialiser
            try
            {
                JsonConvert.PopulateObject (Json, Object);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static string To_Json(T Object)
        {
            if (Object == null) return "";
            try
            {
                return JsonConvert.SerializeObject(Object, JSon_Settings);
            }
            catch
            {
            }
            return "";
        }

        public static bool Is_Empty (T Object)
        {
            if (Object == null) return true;
            if (!(Object is I_Empty)) return false;
            return ((I_Empty)Object).Is_Empty;
        }
    }
}
