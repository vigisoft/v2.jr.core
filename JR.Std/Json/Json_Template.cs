using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using JR;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace JR
{
    public class Json_Template
    {
        public string Open_String = "<!--";
        public string Close_String = "-->";

        public Json_Template (Json_Template Parent, string Key, JToken T)
        {
            if (Parent != null)
            {
                Open_String = Parent.Open_String;
                Close_String = Parent.Close_String;
            }
            this.Scope = Json_Template.New_Scope(Parent == null? null:Parent.Scope, Key, T);
        }

        public Json_Template (JToken T):this(null, "", T)
        {

        }

        public Json_Template Child_Template (string Key, JToken T)
        {
            return new Json_Template(this, Key, T);
        }

        public class Code
        {
            public string Open_String;
            public string Close_String;

            public List<Code> Sub_Code = new List<Code>();
            public string Rule;

            void Push_Static(string Text)
            {
                Sub_Code.Add(new Static_Code { Text = Text });
            }

            void Copy_From (Code C)
            {
                Open_String = C.Open_String;
                Close_String = C.Close_String;
            }

            Code Push(Code Code)
            {
                Sub_Code.Add(Code);
                Code.Copy_From(this);
                return Code;
            }

            string Read_Next_Tag(JR.Strings.Cursor C, out string Rule, out string Key, out JR.Strings.Cursor Params)
            {
                var From = C.Index;
                Rule = null;
                Key = null;
                Params = null;
                var Before = C.Next_Word(Open_String);
                if (!C.Found) return Before;
                var Cl = C.Next_Word(Close_String);
                if (!C.Found) return C.Range(From, C.Index - 1);
                var SR = Cl.Trim().Scan();
                Rule = Cl;
                Key = SR.Next(" ").Trim().ToLower();
                Params = SR.Tail().Scan();
                return Before;
            }

            public string Parse_To(JR.Strings.Cursor SC, params string[] Ends)
            {
                while (!SC.End)
                {
                    string Rule = null;
                    string Key = null;
                    Strings.Cursor Params = null;
                    var From = SC.Index;
                    var Before = Read_Next_Tag(SC, out Rule, out Key, out Params);
                    Push_Static(Before);
                    if (Rule != null)
                    {
                        switch (Key)
                        {
                            case "for":
                                {
                                    var C = Push(new For_Code { Rule = Rule }) as For_Code;
                                    C.Loop_Var = Params.Next_Word(" in").Trim();
                                    C.Array = Params.Tail().Trim();
                                    C.Parse_To(SC, "end");
                                }
                                continue;
                            case "if":
                                {
                                    var C = Push(new If_Code { Rule = Rule }) as If_Code;
                                    C.Test_Var = Params.Next(" ").Trim();
                                    C.Operator = Params.Next_Term().Trim().ToLower();
                                    C.Value = Params.Tail().Trim();
                                    if (C.Parse_To(SC, "end", "else") == "else")
                                    {
                                        C.Else = new Code();
                                        C.Else.Copy_From(this);
                                        C.Else.Parse_To(SC, "end");
                                    }
                                }
                                continue;
                            case "let":
                                {
                                    var C = Push(new Let_Code { Rule = Rule }) as Let_Code;
                                    C.Let_Var = Params.Next_Word(" be").Trim();
                                    C.Object = Params.Tail().Trim();
                                    C.Parse_To(SC, "end");
                                }
                                continue;
                            default:
                                foreach (var End in Ends)
                                {
                                    if (Key != End) continue;
                                    return End;
                                }
                                Push_Static(Open_String + Rule + Close_String);
                                continue;
                        }
                    }
                }
                return "";
            }

            public void Parse(string Text)
            {
                var SC = Text.Scan();
                Parse_To(SC);
            }
            public class Static_Code : Code
            {
                public string Text;
            }

            public class For_Code : Code
            {
                public string Loop_Var;
                public string Array;
            }
            public class If_Code : Code
            {
                public string Test_Var;
                public string Operator;
                public string Value;
                public Code Else = null;
            }
            public class Let_Code : Code
            {
                public string Let_Var;
                public string Object;
            }

        }

        public class Scope_List : List<Pair<JToken>>
        {
        }

        public Scope_List Scope = null;
        public StringBuilder SB = new StringBuilder();

        static JToken Field_Token(string Field, JToken T)
        {
            JToken Val = Field == "" ? T : (T.SelectToken(Field));
            return Val;
        }

        public string Solver(string Key)
        {
            var Key_Format = Key.Split(':');
            var T = Solver_Token(Key_Format[0]);
            if (T == null) return string.Concat("{", Key, "}");
            if (Key_Format.Length == 1) return T.ToString();
            try
            {
                return string.Format("{0:" + Key_Format[1] + "}", T.Value<object>());
            }
            catch { return T.ToString(); }
        }

        JToken Solver_Token(string Key)
        {
            if (Scope.Count > 1)
            {
                var S = Key.Scan();
                var K = S.Next(".");
                for (int I = Scope.Count - 1; I > 0; I--)
                {
                    if (K == "") break;
                    if (Scope[I].Key == K)
                    {
                        var Field = S.Tail();
                        return Field_Token(Field, Scope[I].Value);
                    }
                }
            }
            return Field_Token(Key, Scope[0].Value);
        }

        public string Output(string Text)
        {

            // Construire l'arbre d'appel
            var Code = new Code {Open_String = Open_String, Close_String = Close_String };
            Code.Parse(Text);


            // G�n�rer le code
            Expand(Code);


            return SB.ToString();
        }

        bool Expand(Code Code)
        {
            foreach (var SC in Code.Sub_Code)
            {
                {
                    var C = SC as Code.Static_Code;
                    if (C != null)
                    {
                        Expand(C.Text);
                        continue;
                    }
                }
                {
                    var C = SC as Code.For_Code;
                    if (C != null)
                    {
                        var T = Solver_Token(C.Array);
                        if (T == null)
                        {
                            Store("!!!Can't solve : " + C.Rule);
                            return false;
                        }

                        foreach (var Item in T.Children())
                        {
                            var Exp = Child_Template (C.Loop_Var, Item);
                            var Success = Exp.Expand(C);
                            Store(Exp.SB.ToString());
                            if (!Success) return false;
                        }
                        continue;
                    }
                }
                {
                    var C = SC as Code.If_Code;
                    if (C != null)
                    {
                        var T = Solver_Token(C.Test_Var);
                        if (T == null)
                        {
                            Store("!!!Can't solve : " + C.Rule);
                            return false;
                        }

                        var Value = T.Value<object>();
                        bool Ok = false;
                        switch (C.Operator)
                        {
                            case "is":
                                Ok = Value.ToString() == C.Value;
                                break;
                            case "not":
                                Ok = Value.ToString() != C.Value;
                                break;
                            case "like":
                                Ok = Value.ToString().Like(C.Value);
                                break;
                            case "":
                                Ok = Value.To_Boolean();
                                break;
                        }

                        var Sub_Code = Ok ? C : C.Else;
                        if (Sub_Code != null)
                        {
                            var Exp = Child_Template(null, null);
                            var Success = Exp.Expand(Sub_Code);
                            Store(Exp.SB.ToString());
                            if (!Success) return false;
                        }
                        continue;
                    }
                }
                {
                    var C = SC as Code.Let_Code;
                    if (C != null)
                    {
                        var T = Solver_Token(C.Object);
                        if (T == null)
                        {
                            Store("!!!Can't solve : " + C.Rule);
                            return false;
                        }
                        var Exp = Child_Template (C.Let_Var, T);
                        var Success = Exp.Expand(C);
                        Store(Exp.SB.ToString());
                        if (!Success) return false;
                        continue;
                    }
                }
            }
            return true;
        }

        void Expand(string Text)
        {
            if (Text == "") return;
            SB.Append(Text.Generic_Text(Solver, '{', '}'));
        }
        void Store(string Text)
        {
            if (Text == "") return;
            SB.Append(Text);
        }

        public static Scope_List New_Scope(Scope_List Scope, string Key, JToken T)
        {
            var NS = new Scope_List();
            if (Scope != null)
            {
                NS.AddRange(Scope);
            }
            if (Key != null)
            {
                NS.Add(new Pair<JToken>(Key, T));
            }
            return NS;
        }

    }

}