﻿using JR;
using JR.Saf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace Mustache
{
    public class Mail_Generator : JR.Mail.I_Mail
    {
        // Body peut commencer par @ pour indiquer un soustemplate
        // File_Template peut être null
        public string Body { get; set; } = "";
        public string Subject { get; set; } = null;
        public string From { get; set; } = null;
        public string Sender { get; set; } = null;
        public string To { get; set; } = null;

        public IEnumerable<string> Attachments { get; } = new List<string>();

        static FormatCompiler compiler = new FormatCompiler();

        public void Generate(string File_Template, object Model)
        {
            string Extract(string Source, string Tag)
            {
                string Value = null;
                void Treat_Tag(string Attrib, string Content) => Value = Content;
                JR.Web.Html_Parser.Parse_Tags(Source, Tag, Treat_Tag);
                return Value;
            }
            string Template_Content(string Path) => Main.Universal_Cached_File(Path, out bool Is_New_Code);

            if (Body.StartsWith("@")) Body = Template_Content(Body.Substring(1));
            if (File_Template != null)
            {
                var Replace_Body = Body.Length > 0;
                Body = Template_Content(File_Template);
                if (Replace_Body) Body = Body.Replace("{body}", Body);
            }
            var Tpl = compiler.Compile(Body).Render(Model);
            if (Tpl.StartsWith("<MAIL>"))
            {
                var End = Tpl.IndexOf("</MAIL>");
                if (End > 0)
                {
                    var Mail_Def = Tpl.Substring(6, End-6);
                    if (From == null) From = Extract(Mail_Def, "from");
                    if (Subject == null) Subject = Extract(Mail_Def, "subject");
                    if (To == null) To = Extract(Mail_Def, "to");
                    if (Sender == null) Sender = Extract(Mail_Def, "sender");
                    Body = Tpl.Substring(End + 7);
                    return;
                }
            }
            Body = Tpl;
        }

        public MailMessage Mail
        {
            get
            {
                var M = new MailMessage();
                if (From != "") M.From = new MailAddress(From);
                if (Sender != "") M.Sender = new MailAddress(Sender);
                if (To != "") M.To.Add(To);
                M.Subject = Subject;
                M.Body = Body;
                M.IsBodyHtml = true;
                M.BodyEncoding = System.Text.Encoding.Default;
                return M;
            }
        }
    }
}