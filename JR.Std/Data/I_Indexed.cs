﻿using System;
namespace JR.DB
{
    public interface I_Indexed: I_Debug
    {
        object this[string Key] { get; set; }
    }
    public interface I_Debug
    {
        string HTML_Debug_Info { get; }
    }
}
