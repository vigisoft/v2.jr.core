using ExcelDataReader;
using JR.DB;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

namespace JR
{

    public static class Excel
    {

        static bool Is_Init = false;
        static DataSet Data_Set(object Stream_Or_File_Name, bool With_Header)
        {
            DataSet DS = null;
            if (!Is_Init)
            {
                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                Is_Init = true;
            }
            if (Stream_Or_File_Name is string)
            {
                var File_Name = Stream_Or_File_Name as string;
                try
                {
                    if (!File.Exists(File_Name)) return null;
                    using (FileStream stream = File.Open(File_Name, FileMode.Open, FileAccess.Read))
                    {
                        return Data_Set(stream, With_Header);
                    }
                }
                catch { }

            }
            else if (Stream_Or_File_Name is Stream)
            {
                try
                {
                    var Stream = Stream_Or_File_Name as Stream;
                    using (var excelReader = ExcelReaderFactory.CreateReader(Stream))
                    {
                        var Config = new ExcelDataSetConfiguration
                        {
                            ConfigureDataTable = (x) => new ExcelDataTableConfiguration { UseHeaderRow = With_Header }
                        };
                        DS = excelReader.AsDataSet(Config);
                    }
                }
                catch { }

            }
            return DS;
        }

        public static DataSet Read_All(object Stream_Or_File_Name, bool With_Header = false)
        {
            return Data_Set(Stream_Or_File_Name, With_Header);
        }

        public static DataTable Read_Sheet(object Stream_Or_File_Name, string Sheet_Name = "", bool With_Header = false, bool Remove_Comments_And_Empty = false)
        {
            using (DataSet DS = Data_Set(Stream_Or_File_Name, With_Header))
            {
                if (DS == null) return null;
                foreach (DataTable DT in DS.Tables)
                {
                    if (Sheet_Name == "" || JR.Strings.Like(DT.TableName, Sheet_Name))
                    {
                        if (Remove_Comments_And_Empty)
                        {
                            Remove_Comments(DT);
                        }
                        return DT;
                    }
                }
            }
            return null;
        }

        public static void Remove_Comments(DataTable DT, char Comment_Char = '!', bool Remove_Empty = true)
        {
            if (DT == null) return;
            if (DT.Columns.Count < 1) return;
            var LDR = new List<DataRow>();
            foreach (DataRow DR in DT.Rows)
            {
                string Head = DR.String(0);
                if (Head.Length == 0)
                {
                    if (Remove_Empty)
                    {
                        LDR.Add(DR);
                    }
                }
                else
                if (Comment_Char != ' ' && Head[0] == Comment_Char)
                {
                    LDR.Add(DR);
                }
            }
            foreach (DataRow DR in LDR)
            {
                DT.Rows.Remove(DR);
            }
        }

        //G�n�re un fichier HTML qui peut �tre lu directement par EXCEL pour une source quelconque
        public static bool Gen_HTML_Xls(string File_Name, DataTable Table)
        {
            if (!JR.Files.Check_Directory(File_Name)) return false;
            try
            {

                var SB = new StringBuilder();
                SB.Append("<table cellspacing='0' rules='all' border='1' style='border-collapse:collapse; '>");
                SB.Append("<tr style='font-weight:bold;'>");
                foreach (DataColumn Col in Table.Columns)
                {
                    SB.AppendFormat("<td>{0}</td>", Col.ColumnName.To_Html());
                }
                SB.Append("</tr>");
                foreach (DataRow Row in Table.Rows)
                {
                    SB.Append("<tr>");
                    foreach (DataColumn Col in Table.Columns)
                    {
                        SB.AppendFormat("<td>{0}</td>", (Row[Col] ?? "").ToString().To_Html());
                    }
                    SB.Append("</tr>");
                }
                SB.Append("</table>");

                return JR.Files.Safe_Write(File_Name, SB.ToString());
            }
            catch { return false; }
        }


        /// <summary>
        /// Produces Excel file without using Excel
        /// </summary>
        public class ExcelWriter
        {
            private Stream stream;
            private BinaryWriter writer;

            private ushort[] clBegin = { 0x0809, 8, 0, 0x10, 0, 0 };
            private ushort[] clEnd = { 0x0A, 00 };


            private void WriteUshortArray(ushort[] value)
            {
                for (int i = 0; i < value.Length; i++)
                    writer.Write(value[i]);
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="ExcelWriter"/> class.
            /// </summary>
            /// <param name="stream">The stream.</param>
            public ExcelWriter(Stream stream)
            {
                this.stream = stream;
                writer = new BinaryWriter(stream);
            }

            /// <summary>
            /// Writes the text cell value.
            /// </summary>
            /// <param name="row">The row.</param>
            /// <param name="col">The col.</param>
            /// <param name="value">The string value.</param>
            public void WriteCell(int row, int col, string value)
            {
                ushort[] clData = { 0x0204, 0, 0, 0, 0, 0 };
                int iLen = value.Length;
                byte[] plainText = Encoding.ASCII.GetBytes(value);
                clData[1] = (ushort)(8 + iLen);
                clData[2] = (ushort)row;
                clData[3] = (ushort)col;
                clData[5] = (ushort)iLen;
                WriteUshortArray(clData);
                writer.Write(plainText);
            }

            /// <summary>
            /// Writes the integer cell value.
            /// </summary>
            /// <param name="row">The row number.</param>
            /// <param name="col">The column number.</param>
            /// <param name="value">The value.</param>
            public void WriteCell(int row, int col, int value)
            {
                ushort[] clData = { 0x027E, 10, 0, 0, 0 };
                clData[2] = (ushort)row;
                clData[3] = (ushort)col;
                WriteUshortArray(clData);
                int iValue = (value << 2) | 2;
                writer.Write(iValue);
            }

            /// <summary>
            /// Writes the double cell value.
            /// </summary>
            /// <param name="row">The row number.</param>
            /// <param name="col">The column number.</param>
            /// <param name="value">The value.</param>
            public void WriteCell(int row, int col, double value)
            {
                ushort[] clData = { 0x0203, 14, 0, 0, 0 };
                clData[2] = (ushort)row;
                clData[3] = (ushort)col;
                WriteUshortArray(clData);
                writer.Write(value);
            }

            /// <summary>
            /// Writes the empty cell.
            /// </summary>
            /// <param name="row">The row number.</param>
            /// <param name="col">The column number.</param>
            public void WriteCell(int row, int col)
            {
                ushort[] clData = { 0x0201, 6, 0, 0, 0x17 };
                clData[2] = (ushort)row;
                clData[3] = (ushort)col;
                WriteUshortArray(clData);
            }

            /// <summary>
            /// Must be called once for creating XLS file header
            /// </summary>
            public void BeginWrite()
            {
                WriteUshortArray(clBegin);
            }

            /// <summary>
            /// Ends the writing operation, but do not close the stream
            /// </summary>
            public void EndWrite()
            {
                WriteUshortArray(clEnd);
                writer.Flush();
            }
        }

        public class CSV_Writer : IDisposable
        {
            string _File_Name = "";
            List<string> Lines = new List<string>();
            public CSV_Writer(string File_Name)
            {
                _File_Name = File_Name;
            }
            char[] Spec_Char = "\";\r\n".ToCharArray();
            string Convert(string Text)
            {
                if (Text.IndexOfAny(Spec_Char) < 0) return Text;
                return $"\"{Text.Replace("\"", "\"\"").Replace("\r", "")}\"";
            }
            StringBuilder SB = new StringBuilder();
            public void Add(params string[] Text)
            {
                SB.Clear();
                foreach (string S in Text)
                {
                    SB.Append(Convert(S));
                    SB.Append(';');
                }
                Lines.Add(SB.ToString());
            }
            public void Add(DataColumnCollection Columns)
            {
                SB.Clear();
                foreach (DataColumn C in Columns)
                {
                    SB.Append(Convert(C.ColumnName));
                    SB.Append(';');
                }
                Lines.Add(SB.ToString());
            }
            public void Add(DataRow Row)
            {
                SB.Clear();
                foreach (var V in Row.ItemArray)
                {
                    if (V == null)
                    {
                        SB.Append(';');
                        continue;
                    }
                    var T = V.GetType();
                    if (T == typeof(int)) SB.Append(V);
                    else
                    if (T == typeof(long)) SB.Append(V);
                    else
                    if (T == typeof(double)) SB.Append(V.To_String());
                    else
                    if (T == typeof(decimal)) SB.Append(V.To_String());
                    else
                    if (T == typeof(DateTime)) SB.Append(((DateTime)V).ToString("yyyy-MM-dd HH:mm:ss"));
                    else
                    if (T == typeof(bool)) SB.Append(((bool)V ? "1" : "0"));
                    else
                    if (T == typeof(string)) SB.Append(Convert(V.To_String()));
                    else
                    if (T == typeof(Guid)) SB.Append(Convert(V.To_String()));
                    SB.Append(';');
                }
                Lines.Add(SB.ToString());
            }
            public void Close()
            {
                JR.Files.Safe_Write(_File_Name, Lines.ToArray());
            }

            public void Dispose()
            {
                Close();
            }
        }


    }

}
