using System.Data;
using System.Text;

namespace JR
{
	/// <summary>
	/// Description r�sum�e de Excel.
	/// </summary>
	public static class CSV
    {

        public static bool Gen_CSV(string File_Name, DataTable Table, StringBuilder SB = null, char Column_Separator = ';')
        {
            char[] Spec_Char = $"\"\r\n{Column_Separator}".ToCharArray();
            string To_Csv(string Text)
            {
                if (Text.IndexOfAny(Spec_Char) < 0) return Text;
                return '"' + Text.Replace("\"", "\"\"").Replace("\r", "") + '"';
            }
            if (File_Name != null && !JR.Files.Check_Directory(File_Name)) return false;
            try
            {

                if (SB == null) SB = new StringBuilder();
                foreach (DataColumn Col in Table.Columns)
                {
                    SB.Append(To_Csv(Col.ColumnName));SB.Append(Column_Separator);
                }
                SB.Append("\r\n");
                foreach (DataRow Row in Table.Rows)
                {
                    foreach (var V in Row.ItemArray)
                    {
                        if (V != null) SB.Append(To_Csv(V.ToString())); SB.Append(Column_Separator);
                    }
                    SB.Append("\r\n");
                }
                if (File_Name != null) return JR.Files.Safe_Write(File_Name, SB.ToString(), Encoding.UTF8);
                return true;
            }
            catch { return false; }
        }
    }
}
