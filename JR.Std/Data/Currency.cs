﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JR.Saf;
using System.Net;
using System.Xml;
using System.Xml.XPath;
using System.IO;

namespace JR
{
    public static class Currency
    {

        static object Request_Lock = new object();

        public static string Get_Symbol(string Currency_Code)
        {
            if (Currency_Code == null) return "€";
            var Curr = Currency_Code.ToUpper();
            switch (Curr)
            {
                case "":
                case "EUR":
                    return "€";
                case "USD":
                    return "$";
                case "GBP":
                    return "£";
            }
            return Curr;
        }

        public static string Get_Key(string Currency_Code)
        {
            if (Currency_Code == null) return null;
            var Curr = Currency_Code.ToUpper();
            switch (Curr)
            {
                case "":
                case "€":
                case "EUR":
                    return null;
                case "$":
                    return "USD";
                case "£":
                    return "GBP";
            }
            return Curr;
        }

        public static string Get_Code(string Currency_Code)
        {
            if (Currency_Code == null) return "EUR";
            var Curr = Currency_Code.ToUpper();
            switch (Curr)
            {
                case "":
                case "€":
                    return "EUR";
                case "$":
                    return "USD";
                case "£":
                    return "GBP";
            }
            return Curr;
        }

        public static double Get_Rate(string Currency_Code)
        {
            if (Currency_Code == null) return 1;
            var Curr = Currency_Code.ToUpper();
            switch (Curr)
            {
                case "€":
                case "EUR":
                    return 1;
                case "$":
                    Curr = "USD";
                    break;
                case "£":
                    Curr = "GBP";
                    break;
            }
            Check_Rates();
            double Rate;
            if (Rates.TryGetValue(Curr, out Rate)) return Rate;
            return 1;
        }

        public static double Convert_To(string Currency_Code, double Value)
        {
            var Rate = Get_Rate(Currency_Code);
            if (Rate == 1) return Value;
            return Value * Rate;
        }
        public static double Convert_From(string Currency_Code, double Value)
        {
            var Rate = Get_Rate(Currency_Code);
            if (Rate == 1) return Value;
            return Value / Rate;
        }
        public static IEnumerable<double> Convert_To(string Currency_Code, IEnumerable<double> Values, out double Rate)
        {
            Rate = Get_Rate(Currency_Code);
            if (Rate == 1) return Values;
            var L = new List<double>();
            foreach (var V in Values)
            {
                L.Add(V * Rate);
            }
            return L;
        }
        public static IEnumerable<double> Convert_From(string Currency_Code, IEnumerable<double> Values, out double Rate)
        {
            Rate = Get_Rate(Currency_Code);
            if (Rate == 1) return Values;
            var L = new List<double>();
            foreach (var V in Values)
            {
                L.Add(V / Rate);
            }
            return L;
        }

        public static bool Needs_Convert(string Currency_Code)
        {
            return Get_Rate(Currency_Code) != 1;
        }
        public static bool Can_Convert(string Currency_Code)
        {
            if (Currency_Code == null) return true;
            var Curr = Currency_Code.ToUpper();
            switch (Curr)
            {
                case "€":
                case "EUR":
                case "$":
                case "£":
                    return true;
            }
            Check_Rates();
            return Rates.ContainsKey (Curr);
        }

        static DateTime Next_Read = DateTime.MinValue;
        static Dictionary<string, double> Rates = new Dictionary<string, double>();

        static void Check_Rates()
        {
            // Le fichier de la bce est mis a jour vers 16h, on le recharge à 17h
            DateTime Next_Time(DateTime T) => T.AddHours(-17).Date.AddDays(1).AddHours(17);

            var Now = DateTime.UtcNow;
            if (Next_Read > Now) return;
            lock (Request_Lock) // Une seule génération à la fois
            {
                if (Next_Read > Now) return;    // Une lecture a eu lieu pendant le lock

                var Local_Currency_File = Main.File_Parameter("CURRENCY.LOCAL_FILE");
                if (Local_Currency_File == "") Local_Currency_File = Main.To_Real_File_Name("#LOCAL:CURRENCY.XML");
                var Local_Rates_File = Local_Currency_File.ToLower().Replace(".xml", ".json");

                // première lecture charger le fichier JSON, qui regroupe l'ensemble de devises connues
                //  nécessaire, car parfois le service ne remonte pas toutes les devises
                if (Next_Read == DateTime.MinValue)
                {
                    JR.Json.Populate(Rates, JR.Files.Safe_Read_All(Local_Rates_File));

                    // Se baser sur la date du fichier
                    Next_Read = Next_Time(JR.Files.Safe_Date(Local_Currency_File).To_UTC());
                }

                if (Next_Read <= Now)   // Update du fichier
                {
                    var Service_URL = Main.Parameter("CURRENCY.REMOTE_FILE");
                    if (Service_URL == "") Service_URL = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
                    Service_URL += ("?" + Guid.NewGuid());
                    using (WebClient Request = new WebClient())
                    {
                        try
                        {
                            var Tmp_Currency_File = Local_Currency_File + "tmp";
                            Request.DownloadFile(Service_URL, Tmp_Currency_File);
                            JR.Files.Safe_Move(Tmp_Currency_File, Local_Currency_File);
                        }
                        catch { };
                    }
                    Next_Read = Next_Time(Now);
                }

                try
                {
                    //XPathDocument Doc = new XPathDocument(Local_Currency_File);
                    //XPathNavigator Nav = Doc.CreateNavigator();
                    //XmlNamespaceManager manager = new XmlNamespaceManager(Nav.NameTable);
                    //manager.AddNamespace("gm", "http://www.ecb.int/vocabulary/2002-08-01/eurofxref");
                    ////XPathNodeIterator Nodes = Nav.Select(string.Format("//gm:Cube/gm:Cube/gm:Cube[@currency='{0}']/@rate", Code_Currency), manager);
                    //XPathNodeIterator Nodes = Nav.Select("//gm:Cube[@rate]", manager);
                    //while (Nodes.MoveNext())
                    //{
                    //    Rates.Add(N_Rate.Attributes["currency"].Value.ToUpper(), N_Rate.Attributes["rate"].Value.To_Decimal());
                    //    return Nodes.Current.Value;
                    //}

                    var R = new XmlDocument();
                    R.Load(Local_Currency_File);
                    XmlNamespaceManager manager = new XmlNamespaceManager(R.NameTable);
                    manager.AddNamespace("gm", "http://www.ecb.int/vocabulary/2002-08-01/eurofxref");
                    var N_All = R.SelectNodes("//gm:Cube[@rate]", manager);
                    foreach (XmlNode N_Rate in N_All)
                    {
                        Rates[N_Rate.Attributes["currency"].Value.ToUpper()] = N_Rate.Attributes["rate"].Value.To_Float();
                    }
                    JR.Files.Safe_Write(Local_Rates_File, Rates.To_Json(true));
                }
                catch { }

            }

        }

        //        <?xml version = "1.0" encoding="UTF-8"?>
        //<gesmes:Envelope xmlns:gesmes="http://www.gesmes.org/xml/2002-08-01" xmlns="http://www.ecb.int/vocabulary/2002-08-01/eurofxref">
        //	<gesmes:subject>Reference rates</gesmes:subject>
        //	<gesmes:Sender>
        //		<gesmes:name>European Central Bank</gesmes:name>
        //	</gesmes:Sender>
        //	<Cube>
        //		<Cube time = '2016-11-18' >

        //            < Cube currency= 'USD' rate= '1.0629' />

        //            < Cube currency= 'JPY' rate= '116.95' />

        //            < Cube currency= 'BGN' rate= '1.9558' />

        //            < Cube currency= 'CZK' rate= '27.035' />

        //            < Cube currency= 'DKK' rate= '7.4399' />

        //            < Cube currency= 'GBP' rate= '0.86218' />

        //            < Cube currency= 'HUF' rate= '309.49' />

        //            < Cube currency= 'PLN' rate= '4.4429' />

        //            < Cube currency= 'RON' rate= '4.5150' />

        //            < Cube currency= 'SEK' rate= '9.8243' />

        //            < Cube currency= 'CHF' rate= '1.0711' />

        //            < Cube currency= 'NOK' rate= '9.1038' />

        //            < Cube currency= 'HRK' rate= '7.5320' />

        //            < Cube currency= 'RUB' rate= '68.7941' />

        //            < Cube currency= 'TRY' rate= '3.5798' />

        //            < Cube currency= 'AUD' rate= '1.4376' />

        //            < Cube currency= 'BRL' rate= '3.6049' />

        //            < Cube currency= 'CAD' rate= '1.4365' />

        //            < Cube currency= 'CNY' rate= '7.3156' />

        //            < Cube currency= 'HKD' rate= '8.2450' />

        //            < Cube currency= 'IDR' rate= '14272.09' />

        //            < Cube currency= 'ILS' rate= '4.1163' />

        //            < Cube currency= 'INR' rate= '72.2170' />

        //            < Cube currency= 'KRW' rate= '1250.14' />

        //            < Cube currency= 'MXN' rate= '21.6968' />

        //            < Cube currency= 'MYR' rate= '4.6810' />

        //            < Cube currency= 'NZD' rate= '1.5073' />

        //            < Cube currency= 'PHP' rate= '52.687' />

        //            < Cube currency= 'SGD' rate= '1.5107' />

        //            < Cube currency= 'THB' rate= '37.744' />

        //            < Cube currency= 'ZAR' rate= '15.2790' />

        //        </ Cube >

        //    </ Cube >
        //</ gesmes:Envelope>

    }
}