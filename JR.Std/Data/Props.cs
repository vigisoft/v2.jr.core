using System;
using System.Collections.Generic;
using System.Text;

namespace JR
{
    // Class de gestion de propriétés
    public delegate void Load_Props();
    public class Props
    {
        public interface Indexer
        {
            int Row(string Key);
            string Key(int Row);
            string Name(int Row);
            string Table_Name(int Row);
        }

        public List<IProp> L = new List<IProp>();
        public Dictionary<string, IProp> D;
        public Load_Props On_Missing_Prop;
        public Indexer Idx = null;
        public Props(Indexer Indexer = null)
        {
            Idx = Indexer;
            if (Idx == null)
            {
                D = new Dictionary<string, IProp>();
            }
        }

        public bool Props_Loaded = false;
        public void Load_Props()
        {
            if (Props_Loaded) return;
            if (On_Missing_Prop == null) return;
            Props_Loaded = true;
            On_Missing_Prop();
        }

        public bool Preview
        {
            get
            {
                foreach (IProp P in L)
                {
                    if (P.Preview) return true;
                }
                return false;
            }
            set
            {
                foreach (IProp P in L)
                {
                    P.Preview = value;
                }
            }
        }

        public bool Is_Modified
        {
            get
            {
                foreach (IProp P in L)
                {
                    if (P.Is_Modified) return true;
                }
                return false;
            }
            set
            {
                foreach (IProp P in L)
                {
                    P.Is_Modified = value;
                }
            }
        }

        public void Cancel_Modif()
        {
            foreach (IProp P in L)
            {
                P.Cancel_Modif();
            }
        }

        public void Add(IProp P)
        {
            L.Add(P);
            if (D != null) D[P.Key] = P;
        }

        public void Copy_From(Props P)
        {
            int I = 0;
            foreach (IProp PP in L)
            {
                PP.Copy_From(P.L[I]);
                I++;
            }
        }

        public object this[string Key]
        {
            get
            {
                Key = Key.Official();
                if (Idx == null)
                {
                    D.TryGetValue(Key, out JR.IProp P);
                    return P?.Get_Object_Value;
                }
                else
                {
                    var Row = Idx.Row(Key);
                    if (Row >= 0) return L[Row].Get_Object_Value;
                    return null;
                }
            }
            set
            {
                Key = Key.Official();
                if (Idx == null)
                {
                    if (D.TryGetValue(Key, out JR.IProp P)) P.Set_Object_Value(value);
                }
                else
                {
                    var Row = Idx.Row(Key);
                    if (Row >= 0) L[Row].Set_Object_Value(value);
                }
            }
        }

        public object this[int Row]
        {
            get
            {
                return L[Row].Get_Object_Value;
            }
            set
            {
                L[Row].Set_Object_Value(value);
            }
        }

        public IProp Find_Prop(string Key)
        {
            Key = Key.Official();
            if (Idx == null)
            {
                D.TryGetValue(Key, out JR.IProp P);
                return P;
            }
            else
            {
                var Row = Idx.Row(Key);
                if (Row >= 0) return L[Row];
                return null;
            }

        }

        public IProp Find_First_Prop(string Key)
        {
            return Find_Prop(JR.Strings.Split(Key)[0]);
        }

    }

    public abstract class IProp
    {
        public IProp(Props Parent, string Name, object Default_Value)
        {
            _Parent = Parent;
            Is_Active = false;
            string[] N = Name.Split('.');
            _Name = N[N.Length - 1];
            _Table_Name = N.Length > 1 ? N[0] : "";
            _Key = JR.Strings.Official(_Name);
            _Default_Value = Default_Value;
            _Current_Value = Default_Value;
            _New_Value = Default_Value;
            Parent.Add(this);
        }

        public IProp(Props Parent, int Row, object Default_Value)
        {
            _Parent = Parent;
            Is_Active = false;
            _Name = Parent.Idx.Name(Row);
            _Table_Name = Parent.Idx.Table_Name(Row);
            _Key = Parent.Idx.Key(Row);
            _Default_Value = Default_Value;
            _Current_Value = Default_Value;
            _New_Value = Default_Value;
            Parent.Add(this);
        }

        object _Default_Value = null;

        object _Current_Value;
        object _New_Value;
        bool _Modified = false;
        bool _Exists = false;
        bool _Preview = false;
        Props _Parent = null;
        public Props Parent
        {
            get { return _Parent; }
        }

        void Check_Missing()
        {
            if (_Exists) return;
            Parent.Load_Props();
            _Exists = true;
        }

        bool _Locked = false;
        public bool Is_Locked
        {
            get { return _Locked; }
            set { _Locked = value; }
        }
        public void Load_Object_Value(object V)
        {
            this.Object_Value = V;
        }

        public object Get_Object_Value
        {
            get
            {
                return Preview ? _New_Value : _Current_Value;
            }
        }
        public void Set_Object_Value(object V)
        {
            if (Is_Locked) return;
            this.New_Object_Value = V;
        }

        public object Object_Value
        {
            get
            {
                Check_Missing();
                return Preview ? _New_Value : _Current_Value;
            }
            set
            {
                _Current_Value = (value == null) ? _Default_Value : value;
                _New_Value = _Current_Value;
                _Modified = false;
                _Exists = true;
                _Preview = false;
            }
        }
        public object New_Object_Value
        {
            get
            {
                Check_Missing();
                return _New_Value;
            }
            set
            {
                Check_Missing();
                _Preview = false;
                object V = _Default_Value;
                if (value != null) V = value;
                if (_Current_Value == null)
                {
                    if (V == null) return;
                }
                else if (_Current_Value.Equals(V)) return;
                _Modified = true;
                _New_Value = V;
            }
        }
        public bool Exists
        {
            get { return _Exists; }
        }
        public bool Is_Modified
        {
            get { return _Modified; }
            set
            {
                if (_Modified == value) return;
                if (!value)
                {
                    _Current_Value = _New_Value;
                }
                _Modified = value;
                _Preview = false;
            }
        }
        public void Cancel_Modif()
        {
            _New_Value = _Current_Value;
            _Modified = false;
            _Preview = false;
        }
        public bool Preview
        {
            get { return _Preview; }
            set
            {
                _Preview = value;
            }
        }
        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        string _Table_Name;
        public string Table_Name
        {
            get { return _Table_Name; }
            set { _Table_Name = value; }
        }
        string _Key;
        public string Key
        {
            get { return _Key; }
            set { _Key = value; }
        }

        public void Copy_From(IProp P)
        {
            Load_Object_Value(P.Object_Value);
        }
        public bool Is_Active { get; set; }
        public bool Is_Default
        {
            get
            {
                return _New_Value.Equals(_Default_Value);
            }
        }
    }

    // Prop de type string
    public class SProp : IProp
    {
        public SProp(Props Parent, int Row)
            : base(Parent, Row, "")
        {
        }

        public SProp(Props Parent, string Name)
            : base(Parent, Name, "")
        {
        }

        public SProp(Props Parent, string Name, string Default_Value)
            : base(Parent, Name, Default_Value)
        {
        }

        public SProp(Props Parent, int Row, string Default_Value)
            : base(Parent, Row, Default_Value)
        {
        }

        public void Load_Value(string V)
        {
            this.Object_Value = V;
        }


        public void Set_Value(string V)
        {
            this.New_Object_Value = V;
        }

        public string Value
        {
            get
            {
                return Object_Value as string;
            }
            set
            {
                Object_Value = value;
            }
        }
        public string New_Value
        {
            get
            {
                return New_Object_Value as string;
            }
            set
            {
                New_Object_Value = value;
            }
        }

    }

    // Prop generique
    public class Prop<T> : IProp
    {
        public Prop(Props Parent, string Name)
            : base(Parent, Name, JR.Convert.Default<T>())
        {
        }

        public Prop(Props Parent, string Name, object Default_Value)
            : base(Parent, Name, Default_Value)
        {
        }

        public Prop(Props Parent, int Row)
            : base(Parent, Row, JR.Convert.Default<T>())
        {
        }

        public Prop(Props Parent, int Row, object Default_Value)
            : base(Parent, Row, Default_Value)
        {
        }

        public void Load_Value(T V)
        {
            this.Object_Value = V;
        }


        public void Set_Value(T V)
        {
            this.New_Object_Value = V;
        }

        public T Value
        {
            get
            {
                return JR.Convert.To_Type<T>(Object_Value);
            }
            set
            {
                Object_Value = value;
            }
        }
        public T New_Value
        {
            get
            {
                return JR.Convert.To_Type<T>(New_Object_Value);
            }
            set
            {
                New_Object_Value = value;
            }
        }

    }
}
