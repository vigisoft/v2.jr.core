﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JR
{
    public class Enumeration<T> where T:Enum, new ()
    {
        public Enumeration(params string[] Descriptions)
        {
            List = new List<T>();
            int R = 0;
            foreach (string S in Descriptions)
            {
                JR.Strings.Cursor C = new JR.Strings.Cursor(S);
                string Id = C.Next ("|");
                string Lib_Fr = C.Next("|");
                string Lib_En = C.Next("|");
                string Flags = C.Tail();
                T E = Enum.Create_From<T>(Id, Lib_Fr, Lib_En, Flags, R);
                List.Add(E);
                R++;
            }
        }

        public List<T> List{get;set;}

        public int Index_Of_Id(string Id)
        {
            return List.IndexOf (Get_By_Id(Id));
        }

        public T Get_By_Id(string Id, string Default_Id = null)
        {
            if (Id == null) return null;
            string Oid = JR.Strings.Official(Id);
            string First_Id = null;
            foreach (T P in List)
            {
                if (First_Id == null) First_Id = P.Id;
                if (P.Id == Oid) return P;
            }
            foreach (T P in List)
            {
                if (JR.Strings.Like(P.Lib_Fr, Oid)) return P;
            }
            foreach (T P in List)
            {
                if (JR.Strings.Like(P.Lib_En, Oid)) return P;
            }
            return Get_By_Id(Default_Id, First_Id);
        }

    }

    public class Enum:IComparable
    {
        string _Id = "";

        public static T Create_From <T>(string Id, string Lib_Fr, string Lib_En, string Flags, int Row) where T : Enum, new()
        {
            T E = new T();
            E.Id = JR.Strings.Official(Id);
            E.Lib_Fr = Lib_Fr;
            E.Lib_En = Lib_En;
            E.Flags = Flags;
            E.Row = Row;
            E.After_Load();
            return E;
        }

        public virtual void After_Load()
        {
        }

        public virtual void Before_Save ()
        {
        }

        public string Id {get;set;}
        public string Lib_Fr {get;set;}
        public string Lib_En {get;set;}
        public string Flags {get;set;}
        public bool Is_Private
        {
            get { return _Id.EndsWith("!"); }
        }

        public int Row {get;set;}
        #region IComparable Membres

        public int CompareTo(object obj)
        {
            Enum Other = obj as Enum;
            return this.Row.CompareTo(Other.Row);
        }

        #endregion
    }

    public class Enums : Enumeration<Enum>
    {
        public Enums(params string[] Descriptions):base(Descriptions)
        {
        }
    }
}
