using System;
using System.Data;
//using System.Data.OleDb;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using JR.DB;
using System.Data.Common;

namespace JR
{
	/// <summary>
	/// Description r�sum�e de Excel.
	/// </summary>
	public static class Jet_Excel
	{

        public static string Connection_String(string File_Name)
        {
            if (File_Name.ToLower().Contains (".xlsx") || Environment.Is64BitProcess)
            {
                return string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml", File_Name);
            }
            else
            {
                return string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0", File_Name);
            }
        }
        public class Sql_Writer : IDisposable
		{
			string _File_Name;
			string Command_Buffer = "";
			string _Template = "";
            bool _Auto_Template = false; // Template construite � reception de la premi�re ligne
			DbConnection BD;
            SQL_Db Config;

            public Sql_Writer(string File_Name):this(File_Name, false)
            {
            }

            public Sql_Writer(string File_Name, bool Create_New)
            {
                if (Create_New)
                {
                    JR.Files.Safe_Delete(File_Name);
                }
                _File_Name = File_Name;
                string Connection = Connection_String(File_Name) + ";HDR=NO;MAXSCANROWS=1;\"";
                Config = new SQL_Db { Connection_String = Connection };
                // Le parametre MAXSCANROWS indique a excel de ne se baser que sur le contenu de la
                // Premi�re ligne (!!! contenu et non pas format)
                // Pour forcer le format num�rique il faut donc ins�rer en premi�re ligne un nombre
                // et masquer la ligne
                BD = Config.New_Connection;
                BD.Open();
            }

            public void Create_Sheet (string Sheet_Name, params string [] Col_Names)
            {
                var SB = new StringBuilder("CREATE TABLE [");
                SB.Append(Sheet_Name);
                SB.Append("] (");
                bool Virg = false;
                foreach (var Col in Col_Names)
                {
                    if (Virg) SB.Append(','); else Virg = true;
                    SB.AppendFormat("[{0}] string", Col);
                }
                SB.Append(")");
                using (var cmd = BD.CreateCommand())
                {
                    cmd.CommandText = SB.ToString();
                    cmd.ExecuteNonQuery();
                }

            }
            public string Template
			{
				get{return _Template;}
                set
                {
                    Post(); _Template = value; _Auto_Template = false;
                }
			}

            public void Set_Insert_Template(string Sheet, int Col_Count)
            {
                _Template = string.Format("insert into [{0}$] values ('{{0}}'", Sheet);
                for (int I = 1; I < Col_Count; I++)
                {
                    _Template += string.Format(",'{{{0}}}'", I);
                };
                _Template += ")";
                _Auto_Template = false;
            }

            public void Set_Auto_Template (string Sheet)
            {
                _Template = string.Format("insert into [{0}$] values ", Sheet);
                _Auto_Template = true;
            }

            object[] Check_Quotes(params object[] Params)
            {
                object[] Pars = new object[Params.Length];
                for (int I = 0; I < Params.Length; I++)
                {
                    object Param = Params[I];
                    if (Param is string)
                    {
                        Pars[I] = ((string)Param).Replace("'", "''");
                    }
                    else
                    {
                        Pars[I] = Params[I];
                    };
                }

                return Pars;
            }

            string Check_Quote (string Param)
            {
                return Param.Replace("'", "''");
            }

            void Complete_Template(params object[] Params)
            {
                _Auto_Template = false;
                for (int I = 0; I < Params.Length; I++)
                {
                    _Template += (I == 0)?"(":",";
                    object Param = Params[I];
                    if (Param is short || Param is int || Param is long || Param is double || Param is float)
                    {
                        _Template += string.Format("{{{0}}}", I);
                    }
                    else
                    {
                        _Template += string.Format("'{{{0}}}'", I);
                    };
                }
                _Template += ")";

            }
            
			public void Add (params object [] Params)
			{
				if (Command_Buffer != "") Command_Buffer += ";";
                if (_Auto_Template) Complete_Template(Params);
				Command_Buffer += string.Format (_Template, Check_Quotes(Params));
				Post();
			}

            public void Change_Cell(string Sheet, string Cell, int Col, string Value)
            {
                string Command = string.Format("update [{0}${1}:{1}] set F{2}='{3}'", Sheet, Cell, Col, Check_Quote (Value));
                Add_Command(Command);
            }

            public void Change_Num_Cell(string Sheet, string Cell, int Col, string Value)
            {
                string Command = string.Format("update [{0}${1}:{1}] set F{2}={3}", Sheet, Cell, Col, Check_Quote(Value));
                Add_Command(Command);
            }

            public void Add_Command(string Command)
			{
				Command_Buffer = Command;
				Post();
			}

			public void Post ()
			{
				if (Command_Buffer == "") return;
				try
				{
					DbCommand Command = Config.New_Command (Command_Buffer, BD);
					Command.ExecuteNonQuery();
				} 
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine (e.Message);
				}
				Command_Buffer = "";
			}

			public void Close ()
			{
				Post();
				if (BD.State != ConnectionState.Closed)
				{
					BD.Close ();
				};
			}

			public void Test ()
			{
			}


            public void Dispose()
            {
                Close();
            }
        }

        // Attention, pour lire des chaines de + de 256, il faut en mettre au moins une dans les 8 premieres lignes
		public class Sql_Reader
		{
			string _File_Name;
            DbConnection BD;
            SQL_Db Config;
            bool _Remove_Comments = false;
			string [] _Sheets = new string [0];

            public Sql_Reader(string File_Name):this(File_Name, false, false)
            {
            }

			public Sql_Reader (string File_Name, bool With_Header, bool Remove_Comments)
			{
                _Remove_Comments = Remove_Comments;
                string[] File_Parts = (File_Name + "@").Split('@');
				_File_Name = File_Parts[0];
                string Sheet_Name = JR.Strings.Official (File_Parts[1]);
                if (Sheet_Name != "") Sheet_Name += "$";

                if (!File.Exists(_File_Name)) return;
                string Connection = Connection_String(_File_Name) + ";HDR=" + (With_Header ? "yes" : "no") + "\"";
                Config = new SQL_Db { Connection_String = Connection };
                try
                {
                    BD = Config.New_Connection;
                    BD.Open();
                }
                catch (Exception e)
                {
                    JR.Saf.Logs.Log("Error opening Excel Reader {0}", Connection);
                    JR.Saf.Logs.Log(e.Message);
                    return; 
                }

				// Lire la liste des feuilles
				DataTable DT = BD.GetSchema ("Tables");
 
				if (DT == null)	return;

                Dictionary<string, string> _Sheets_Dic = new Dictionary<string, string>();

				foreach(DataRow Row in DT.Rows)
				{
					string _Sheet = (string)Row["TABLE_NAME"];
                    if (_Sheet.EndsWith("$"))
                    {
                        if (Sheet_Name != "")
                        {
                            if (Sheet_Name == JR.Strings.Official (_Sheet))
                            {
                                _Sheets_Dic[_Sheet] = _Sheet;
                                break;
                            }
                        }
                        else
                        {
                            _Sheets_Dic[_Sheet] = _Sheet;
                        }
                    }
				}
				_Sheets = new String [_Sheets_Dic.Count];
                _Sheets_Dic.Keys.CopyTo(_Sheets, 0);
			}

			// Retourne le nom des feuilles
			public string [] Sheets
			{
				get
				{
					return _Sheets;
				}

			}

            public DataTable Sheet(int Row)
            {
                DataSet DS = new DataSet();
                DbDataAdapter DA = Config.New_Adapter ("SELECT * FROM [" + _Sheets[Row] + "]", BD);
                try
                {
                    DA.Fill(DS);
                }
                catch { return null; }
                if (DS.Tables.Count == 0) return null;
                List<DataRow> LDR = new List<DataRow>();
                if (_Remove_Comments && DS.Tables[0].Columns.Count > 0)
                {
                    foreach (DataRow DR in DS.Tables[0].Rows)
                    {
                        if (DR[0].ToString().StartsWith("!"))
                        {
                            LDR.Add(DR);
                        }
                    }
                }
                foreach (DataRow DR in LDR)
                {
                    DS.Tables[0].Rows.Remove(DR);
                }
                return DS.Tables[0];
            }

            public DataSet SheetDS(int Row)
            {
                string Sheet_Name = _Sheets[Row].TrimEnd('$');
                DataSet DS = new DataSet(Sheet_Name + "S");
                DbDataAdapter DA = Config.New_Adapter("SELECT * FROM [" + _Sheets[Row] + "]", BD);
                try
                {
                    DA.Fill(DS, Sheet_Name);
                }
                catch {}
                return DS;
            }

            public void Close()
			{
                if (BD != null) BD.Close ();
			}

		}



	}

}
