using System;
using System.Collections.Generic;
using System.Text;
using JR.DB;
using System.Data;
using System.Linq;

namespace JR
{

    //Transforme une liste en tableau paginable
    public class Tablizer <T>
    {
        IList<T> L;
        public Tablizer(IList<T> List, int Col_Count, int Page_Size)
        {
            Rows = new List<List<T>>();
            this.Col_Count = Col_Count;
            this.Page_Size = Page_Size;
            int Col = 0;
            L = List;
            List<T> Row = null;
            foreach (var I in L)
            {
                if (Col % Col_Count == 0)
                {
                    Row = new List<T>();
                    Rows.Add(Row);
                }
                Row.Add(I);
                Col++;
            }
        }

        public List<List<T>> Rows { get; set; }
        public int Col_Count { get; set; }
        public int Page_Size { get; set; }
        public int Count
        {
            get { return L.Count; }
        }

        public int Row_Count
        {
            get { return Rows.Count; }
        }

        public bool Has_Value(int Row, int Col)
        {
            if (Row >= 0 && Row < Row_Count)
            {
                return Col >= 0 && Col < Rows[Row].Count;
            }
            return false;
        }

        public T Value(int Row, int Col)
        {
            if (Has_Value(Row, Col))
            {
                return Rows[Row][Col];
            }
            return default(T);
        }
    }
}
