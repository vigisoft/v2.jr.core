﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JR.DB
{
    public class DB_Where
    {
        public StringBuilder Where = new StringBuilder();
        public List<object> Args = new List<object>();
        public bool Append_Full_Text_Search(string Expression, params string [] Fields)
        {
            if (Fields.Length == 0) return false;
            string Exp = Expression.Trim().Replace('?', '_').Replace('*', '%');
            if (Exp.Length == 0) return false;
            string Where_String = "";
            foreach (string W in Fields)
            {
                if (Where_String != "") Where_String += " or";
                Where_String += string.Format(" {0} like @A{{0}}", W);
            }
            Where_String = "(" + Where_String + ")";

            StringBuilder SB = new StringBuilder();
            int I = Args.Count;
            string[] Texts = Exp.Split(' ');
            bool Mode_Sauf = false;
            foreach (string T in Texts)
            {
                string TT = T.Trim();
                if (TT.Length == 0) continue;
                if (TT == "<>")
                {
                    Mode_Sauf = true;
                    continue;
                }
                Args.Add(Db.Like_String(TT));
                if (SB.Length != 0) SB.Append(" and");
                if (Mode_Sauf) SB.Append(" not");
                SB.Append(string.Format(Where_String, I));
                I++;
            }
            Append_Where_Clause(true, SB.ToString());
            return true;
        }

        public void Append_Where_Clause(bool And, params string[] Clauses)
        {
            foreach (string Clause in Clauses)
            {
                if (Clause.Length == 0) continue;
                if (Where.Length > 0) Where.Append(And ? " and" : " or");
                Where.Append("(");
                Where.Append(Clause);
                Where.Append(")");
            }
        }

        public void Append_Test(string Test, params object [] Params)
        {
            var Args_Offset = Args.Count;
            if (Args_Offset > 0)
            {
                for (int I = Params.Length; I > 0; I--)
                {
                    Test = Test.Replace("@" + I, "@" + I + Args_Offset);
                }
            }
            Append_Where_Clause(true, Test);
            Args.AddRange(Params);
        }

        public void Append_Range_Clause(string Field, IEnumerable<object> Range)
        {
            StringBuilder SB = new StringBuilder();
            SB.AppendFormat("{0} in (", Field);
            bool First = true;
            foreach (var S in Range)
            {
                if (First) First = false; else SB.Append(',');
                if (S is string)
                {
                    SB.Append(Dbase.To_Arg(S as string));
                }
                else
                {
                    SB.Append(S);
                }
            }
            if (First) return;
            SB.Append(")");
            Append_Where_Clause(true, SB.ToString());
        }
        public string Where_Clause
        {
            get { return Where.ToString(); }
        }
        public string Full_Where_Clause
        {
            get { return Where.Length == 0 ? "":("where " + Where.ToString()); }
        }
        public object[] Params
        {
            get { return Args.ToArray(); }
        }
    }

}
