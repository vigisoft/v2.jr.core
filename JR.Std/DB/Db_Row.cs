using System;
using System.Collections.Generic;
using System.Text;
using JR.DB;
using System.Data;
using System.Linq;

namespace JR.DB
{

    // JRO_Row est un objet simplifi� bas� sur un datarow
    public class Db_Row
    {

        public Db_Row(DataRow Data_Row)
        {
            Row = Data_Row;
        }

        public Db_Row()
        {
        }

        public DataRow Row { get; set; }
        public bool Is_Null
        {
            get { return Row == null; }
        }

        public bool Null(string Field)
        {
            if (Row == null) return true;
            return Row.Is_Null(Field);
        }

        public T Value<T>(string Field)
        {
            try
            {
                if (Row.Table.Columns.Contains(Field))
                {
                    DataColumn DC = Row.Table.Columns[Field];
                    if (Row.IsNull(DC)) return (T)JR.Convert.Default<T>();
                    return JR.Convert.To_Type<T>(Row[DC]);
                }
                if (Field.StartsWith("S#"))
                {
                    return Spec_Value<T>(Field);
                }
            }
            catch
            {
            }
            return (T)JR.Convert.Default<T>();
        }

        public virtual T Spec_Value<T>(string Field)
        {
            return (T)JR.Convert.Default<T>();
        }

        public void Set<T>(string Field, T Value)
        {
            int Col = Row.Table.Columns.IndexOf(Field);
            if (Col >=0)
            {
                T Val;
                if (Row.IsNull(Col))
                {
                    Val = (T)JR.Convert.Default<T>();
                } else
                {
                   Val = JR.Convert.To_Type<T>(Row[Col]);
                };

                if (Val == null)
                {
                    if (Value == null) return;
                } else if (Val.Equals(Value)) return;

                // Si date nulle, mettre dbnull
                if (Value != null && Value.Equals(DateTime.MinValue))
                {
                    Row[Col] = DBNull.Value;
                }
                else
                {
                    Row.SetField<T>(Col, Value);
                }
            }
        }

        public string String(string Field)
        {
            return Value<string>(Field);
        }

        public string Format(string Format, Boolean Empty_If_Default = false)
        {
            Func<string, object> Solver = (Param) =>
            {
                // Un parametre est de la forme {Param[:format]}
                if (Param == "") return null;
                return Value<object>(Param)??Param;
            };

            return JR.Strings.Generic_Format (Format, Solver, Empty_If_Default);
        }

        public int Int(string Field)
        {
            return Value<int>(Field);
        }

        public long Long(string Field)
        {
            return Value<long>(Field);
        }

        public DateTime Date(string Field)
        {
            return Value<DateTime>(Field);
        }

        public bool Bool(string Field)
        {
            return Value<bool>(Field);
        }

        public Guid Guid(string Field)
        {
            return Value<Guid>(Field);
        }
        public decimal Decimal(string Field)
        {
            return Value<decimal>(Field);
        }
        public double Float(string Field)
        {
            return Value<double>(Field);
        }

        public bool Is_New
        {
            get { return Row.RowState == DataRowState.Detached || Row.RowState == DataRowState.Added; }
        }

        public virtual bool Removable { get { return true; } }
        public virtual void Remove(string Table_Name, string Key_Name)
        {
            if (Is_New) return;
            Gen_Db<object> D = Row.Table.ExtendedProperties["Gen_Db"] as Gen_Db<object>;
            if (D != null)
            {
                DataColumn Key = Row.Table.Columns[Key_Name];
                if (Key != null) D.Exec($"delete from {Table_Name} where {Key_Name}=@0", Row[Key]);
            };
            Cleanup();
            Row.Table.Rows.Remove(Row);
        }

        public virtual void Cleanup()
        {
        }

        public virtual bool Save(string Table_Name, string Key_Name, bool Auto_Key = true)
        {
            bool Saved = false;
            Dbase D = Row.Table.ExtendedProperties["Gen_Db"] as Dbase;
            if (D != null)
            {
                bool Is_New = Row.RowState == DataRowState.Detached;
                DataColumn Key = Row.Table.Columns[Key_Name];
                if (Key == null) return false; // non support�

                List<string> Fields = new List<string>();
                List<string> Keys = new List<string>();
                List<object> Values = new List<object>();

                Action<DataColumn, object> Save_Field = (DC, Value) =>
                    {
                        Fields.Add(DC.ColumnName);
                        if (Is_New)
                        {
                            Keys.Add($"@{Values.Count}");
                        }
                        else
                        {
                            Keys.Add($"{DC.ColumnName}=@{Values.Count}");
                        }
                        Values.Add(Value);

                    };

                // Id toujours devant
                Values.Add(Row[Key]);
                foreach (DataColumn DC in Row.Table.Columns)
                {
                    object New = Row[DC];
                    if (Is_New)
                    {
                        if (DBNull.Value.Equals(New)) continue;
                    }
                    else if (!DC.ColumnName.StartsWith("#"))
                    {
                        object Original = Row[DC, DataRowVersion.Original];
                        if (New.Equals(Original)) continue;
                    }
                    Save_Field(DC, New);
                }

                // Sauver l'objet �tendu
                string Req;
                bool Has_Values = Values.Count > 1;
                if (Is_New)
                {
                    if (Auto_Key)
                    {
                        if (Has_Values)
                        {
                            Req = "insert into {0} ({1}) values ({2})";
                        }
                        else
                        {
                            Req = "insert into {0} values (@0)";
                        }
                    }
                    else
                    {
                        if (Has_Values)
                        {
                            Req = "insert into {0} (" + Key_Name + ",{1}) values (@0,{2})";
                        }
                        else
                        {
                            Req = "insert into {0} (" + Key_Name + ") values (@0)";
                        }
                    }
                }
                else
                {
                    Req = "update {0} set {2} where " + Key_Name + "=@0";
                }
                Req = string.Format(Req, Table_Name, JR.Strings.Assembled(',', Fields.ToArray()), JR.Strings.Assembled(',', Keys.ToArray()));
                if (Is_New || Has_Values)
                {
                    Saved = true;
                    if (Is_New)
                    {
                        decimal Id = D.Insert_Request(Req, Values.ToArray());
                        if (Auto_Key)
                        {
                            Row[Key] = System.Convert.ChangeType(Id, Key.DataType);
                        }
                    }
                    else
                    {
                        D.Exec(Req, Values.ToArray());
                    }
                }

                if (Is_New)
                {
                    Row.Table.Rows.Add(Row);
                }
                Row.AcceptChanges();
            }
            return Saved;
        }

        // Modifier un paquet de colonnes
        public static void Set_Row(DataRow R, params object[] Properties)
        {
            for (int I = 0; I + 1 < Properties.Length; I += 2)
            {
                R[Properties[I].ToString()] = Properties[I + 1];
            }
        }

        // Creer une instance selon le schema d'un table
        public static DataRow New_Row(DataTable T)
        {
            DataRow R = T.NewRow();
            return R;
        }

        // Creer une instance selon le schema d'un table
        public static DataRow New_Row(Dbase Db, string Table_Name)
        {
            DataTable T = Db.Exec_Table("select * from {0} where (0=1)", Table_Name);
            return New_Row(T);
        }

        // Creer une instance selon le schema d'un table
        public static DataRow Get_Row(Dbase Db, string Table_Name, string Key_Name, object Key)
        {
            return Db.Exec_Row ("select * from {0} where {1}=@2", Table_Name, Key_Name, Key);
        }

        public static void Copy(DataRow Source, DataRow Dest)
        {
            foreach (DataColumn C in Source.Table.Columns)
            {
                foreach (DataColumn K in Source.Table.PrimaryKey)
                {
                    if (K == C) continue;
                }
                if (C.ColumnName.Length > 2 && C.ColumnName[1] == '#') continue;
                Dest[C.Ordinal] = Source[C.Ordinal];
            }
        }

        public string HTML_Debug_Info
        {
            get 
            {
                return Row.HTML_Debug_Info();
            }

        }

    }

    public static class DB_Row_Extensions
    {
        public static bool Is_Null(this DataRow Row, string Field)
        {
            if (Row == null) return true;
            if (Row.Table.Columns.Contains(Field))
            {
                DataColumn DC = Row.Table.Columns[Field];
                return Row.IsNull(DC);
            }
            return true;
        }

        public static bool Is_Null(this DataRow Row, int Field)
        {
            if (Row == null) return true;
            if (Row.Table.Columns.Count > Field)
            {
                DataColumn DC = Row.Table.Columns[Field];
                return Row.IsNull(DC);
            }
            return true;
        }

        public static T Value<T>(this DataRow Row, int Field)
        {
            try
            {
                if (Row.Table.Columns.Count > Field)
                {
                    DataColumn DC = Row.Table.Columns[Field];
                    if (Row.IsNull(DC)) return (T)JR.Convert.Default<T>();
                    return JR.Convert.To_Type<T>(Row[DC]);
                }
            }
            catch
            {
            }
            return (T)JR.Convert.Default<T>();
        }

        public static T Value<T>(this DataRow Row, string Field)
        {
            try
            {
                if (Row.Table.Columns.Contains(Field))
                {
                    DataColumn DC = Row.Table.Columns[Field];
                    if (Row.IsNull(DC)) return (T)JR.Convert.Default<T>();
                    return JR.Convert.To_Type<T>(Row[DC]);
                }
            }
            catch
            {
            }
            return (T)JR.Convert.Default<T>();
        }

        public static void Set<T>(this DataRow Row, string Field, T Value)
        {
            Set<T> (Row, Row.Table.Columns.IndexOf(Field), Value);
        }

        public static void Set<T>(this DataRow Row, int Field, T Value)
        {
            if (Row.Table.Columns.Count <= Field) return;
            T Val;
            if (Row.IsNull(Field))
            {
                Val = (T)JR.Convert.Default<T>();
            }
            else
            {
                Val = JR.Convert.To_Type<T>(Row[Field]);
            };

            if (Val == null)
            {
                if (Value == null) return;
            }
            else if (Val.Equals(Value)) return;

            // Si date nulle, mettre dbnull
            if (Value != null && Value.Equals(DateTime.MinValue))
            {
                Row[Field] = DBNull.Value;
            }
            else
            {
                Row.SetField<T>(Field, Value);
            }
        }

        public static string String(this DataRow Row, string Field)
        {
            return Row.Value<string>(Field);
        }

        public static string String(this DataRow Row, int Field)
        {
            return Row.Value<string>(Field);
        }

        public static string Lang_String(this DataRow Row, string Field, string Lang, string Default_Lang = "FR")
        {
            if (Lang != Default_Lang)
            {
                string Val = Row.Value<string>(Field + Lang);
                if (Val != "") return Val;
            }
            return Row.Value<string>(Field + Default_Lang);
        }

        public static string Format(this DataRow Row, string Format, Boolean Empty_If_Default = false)
        {
            Func<string, object> Solver = (Param) =>
            {
                // Un parametre est de la forme {Param[:format]}
                if (Param == "") return null;
                if (char.IsDigit(Param[0]))
                {
                    return Row.Value<object>(JR.Convert.To_Integer(Param));
                }
                return Row.Value<object>(Param);
            };

            return JR.Strings.Generic_Format(Format, Solver, Empty_If_Default);
        }

        public static int Int(this DataRow Row, string Field)
        {
            return Row.Value<int>(Field);
        }

        public static int Int(this DataRow Row, int Field)
        {
            return Row.Value<int>(Field);
        }

        public static long Long(this DataRow Row, string Field)
        {
            return Row.Value<long>(Field);
        }

        public static long Long(this DataRow Row, int Field)
        {
            return Row.Value<long>(Field);
        }

        public static DateTime Date(this DataRow Row, string Field)
        {
            return Row.Value<DateTime>(Field);
        }
        public static DateTime Date(this DataRow Row, int Field)
        {
            return Row.Value<DateTime>(Field);
        }

        public static bool Bool(this DataRow Row, string Field)
        {
            return Row.Value<bool>(Field);
        }
        public static bool Bool(this DataRow Row, int Field)
        {
            return Row.Value<bool>(Field);
        }

        public static Guid Guid(this DataRow Row, string Field)
        {
            return Row.Value<Guid>(Field);
        }
        public static Guid Guid(this DataRow Row, int Field)
        {
            return Row.Value<Guid>(Field);
        }
        public static decimal Decimal(this DataRow Row, string Field)
        {
            return Row.Value<decimal>(Field);
        }
        public static decimal Decimal(this DataRow Row, int Field)
        {
            return Row.Value<decimal>(Field);
        }
        public static double Float(this DataRow Row, string Field)
        {
            return Row.Value<double>(Field);
        }
        public static double Float(this DataRow Row, int Field)
        {
            return Row.Value<double>(Field);
        }

        public static bool Is_New (this DataRow Row)
        {
            return Row.RowState == DataRowState.Detached || Row.RowState == DataRowState.Added;
        }

        public static string HTML_Debug_Info(this DataRow Row)
        {
            using (Db_Reader DR = new Db_Reader(Row))
            {
                return DR.Full_HTML_Debug_Info;
            }
        }
        public static string XML_Debug_Info(this DataRow Row)
        {
            using (Db_Reader DR = new Db_Reader(Row))
            {
                return DR.XML_Debug_Info;
            }
        }

        // Modifier un paquet de colonnes
        public static void Set_Row(DataRow R, params object[] Properties)
        {
            for (int I = 0; I + 1 < Properties.Length; I += 2)
            {
                R[Properties[I].ToString()] = Properties[I + 1];
            }
        }

        public static void Copy_To (this DataRow Source, DataRow Dest)
        {
            foreach (DataColumn C in Source.Table.Columns)
            {
                foreach (DataColumn K in Source.Table.PrimaryKey)
                {
                    if (K == C) continue;
                }
                //if (C.ColumnName.Length > 2 && C.ColumnName[1] == '#') continue;
                Dest[C.Ordinal] = Source[C.Ordinal];
            }
        }

    }


}
