using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Text;

namespace JR.DB
{
	/// <summary>
	/// Description r�sum�e de Session.
	/// </summary>
	//public class Base:Dbase
	//{
 //       public Base(string Connection)
 //       {
 //           this.Connexion = Connection;
 //       }

 //       public string Connexion { get; set; }

 //       //public bool Sql_Mode
 //       //{
 //       //    get { return _Sql_Mode; }
 //       //}

 //       public static string Param(string Source)
 //       {
 //           return Source.Replace("'", "''");
 //       }

 //       //public static SqlParameter To_Sql(OleDbParameter P)
 //       //{
 //       //    SqlParameter SP = new SqlParameter(P.ParameterName, P.Value);
 //       //    return SP;
 //       //}

 //       //OleDbConnection New_Ole_Connection
 //       //{
 //       //    get
 //       //    {
 //       //        OleDbConnection BD = new OleDbConnection(_Connexion);
 //       //        BD.Open();
 //       //        return BD;
 //       //    }
 //       //}


 //       //SqlConnection New_Sql_Connection
 //       //{
 //       //    get
 //       //    {
 //       //        SqlConnection BD = new SqlConnection(_Connexion);
 //       //        BD.Open();
 //       //        return BD;
 //       //    }
 //       //}


 //       // Lecture des resultats d'une requete
	//	public DataSet Lire (string Requete, params DbParameter [] Parameters)
	//	{
 //           DataSet Ds = new DataSet();
 //           try
 //           {
 //                   using (SqlConnection BD = New_Sql_Connection)
 //                   {
 //                       using (SqlDataAdapter Command = new SqlDataAdapter (Requete, BD))
 //                       {
 //                           if (Parameters != null && Parameters.Length > 0)
 //                           {
 //                               Command.SelectCommand.CommandType = Get_Type(Requete);
 //                               foreach (OleDbParameter P in Parameters)
 //                               {
 //                                   Command.SelectCommand.Parameters.Add(To_Sql(P));
 //                               };
 //                           };
 //                           Command.Fill(Ds);
 //                       }
 //                   }
 //           }
 //           catch (Exception e)
 //           {
 //               JR.Saf.Logs.Err(e.Message);
 //               JR.Saf.Logs.Err(Connexion);
 //               JR.Saf.Logs.Err(Requete);
 //               JR.Saf.Logs.Except(e);
 //           };
 //           return Ds;
 //       }

 //       // Lecture des resultats d'une requete
 //       public DataTable Get_Table(string Requete, params object [] Parameters)
 //       {
 //           DbParameter[] Params = new DbParameter[Parameters.Length/2];
 //           for (int I = 0; I < Params.Length; I++)
 //           {
 //               Params[I] = new OleDbParameter(Parameters[2*I] as string, Parameters[2*I+1]);
 //           }
 //           using (DataSet Ds = Lire(Requete, Params))
 //           {
 //               if (Ds.Tables.Count == 0) return null;
 //               return Ds.Tables[0];
 //           }
 //       }

	//	// Lecture des resultats d'une requete
	//	public DataTable Lire_Table (string Requete, params DbParameter [] Parameters)
	//	{
	//		using (DataSet Ds = Lire (Requete, Parameters))
 //           {
 //               if (Ds.Tables.Count == 0) return null;
 //               return Ds.Tables[0];
 //           }
	//	}

	//	// Lecture des resultats d'une requete
	//	public DataRowCollection Lire_Lignes (string Requete, params DbParameter [] Parameters)
	//	{
	//		using (DataSet Ds = Lire (Requete, Parameters))
 //           {
 //               if (Ds.Tables.Count == 0) return null;
 //               return Ds.Tables[0].Rows;
 //           }
	//	}

 //       // Lecture de la premi�re Ligne des resultats d'une requete
 //       public DataRow Lire_Ligne(string Requete, params DbParameter[] Parameters)
 //       {
 //           using (DataTable Dt = Lire_Table(Requete, Parameters))
 //           {
 //               if (Dt == null) return null;
 //               if (Dt.Rows.Count == 0) return null;
 //               return Dt.Rows[0];
 //           }
 //       }

 //       // Lecture de la premi�re Ligne des resultats d'une requete
 //       public DataRow Get_Row(string Requete, params object[] Parameters)
 //       {
 //           using (DataTable Dt = Get_Table(Requete, Parameters))
 //           {
 //               if (Dt == null) return null;
 //               if (Dt.Rows.Count == 0) return null;
 //               return Dt.Rows[0];
 //           }
 //       }

 //       // Lecture d'une fonction
	//	public object Function (string Requete, params DbParameter[] Parameters)
	//	{
	//		using (DataTable Dt = Lire_Table(Requete, Parameters))
 //           { 
	//		    if (Dt == null) return null;
	//		    if (Dt.Rows.Count == 0) return null;
	//		    if (Dt.Rows[0].ItemArray.GetLength(0) == 0) return null;
	//		    return Dt.Rows[0][0];
 //           }
 //       }

 //       public void Execute (string Requete, params object[] Parameters)
 //       {
 //           DbParameter[] Params = new DbParameter[Parameters.Length / 2];
 //           for (int I = 0; I < Params.Length; I++)
 //           {
 //               Params[I] = new OleDbParameter(Parameters[2 * I] as string, Parameters[2 * I + 1]);
 //           }
 //           Executer(Requete, Params);
 //       }

 //       CommandType Get_Type(string Request)
 //       {
 //           if (Request.ToLower().StartsWith("select ")) return CommandType.Text;
 //           if (Request.ToLower().StartsWith("update ")) return CommandType.Text;
 //           if (Request.ToLower().StartsWith("delete ")) return CommandType.Text;
 //           if (Request.ToLower().StartsWith("exec ")) return CommandType.Text;
 //           if (Request.ToLower().StartsWith("execute ")) return CommandType.Text;
 //           return CommandType.StoredProcedure;
 //       }

	//	public void Executer(string Requete, params DbParameter[] Parameters)
	//		// Execution d'une requete instantan�e
	//	{
 //               try
 //               {
 //                   if (_Sql_Mode)
 //                   {
 //                       using (SqlConnection BD = New_Sql_Connection)
 //                       {
 //                           using (SqlCommand Command = new SqlCommand(Requete, BD))
 //                           {
 //                               if (Parameters != null && Parameters.Length > 0)
 //                               {
 //                                   Command.CommandType = Get_Type(Requete);
 //                                   foreach (OleDbParameter P in Parameters)
 //                                   {
 //                                       Command.Parameters.Add(To_Sql(P));
 //                                   };
 //                               };

 //                               Command.ExecuteNonQuery();
 //                           }
 //                       }
 //                   }
 //                   else
 //                   {
 //                       using (OleDbConnection BD = New_Ole_Connection)
 //                       {
 //                           using (OleDbCommand Command = new OleDbCommand(Requete, BD))
 //                           {
 //                               if (Parameters != null && Parameters.Length > 0)
 //                               {
 //                                   Command.CommandType = Get_Type(Requete);
 //                                   foreach (OleDbParameter P in Parameters)
 //                                   {
 //                                       Command.Parameters.Add(P);
 //                                   };
 //                               };

 //                               Command.ExecuteNonQuery();
 //                           }

 //                       }
 //                   }
 //               }
 //               catch (Exception e)
 //               {
 //                   JR.Saf.Logs.Err(e.Message);
 //                   JR.Saf.Logs.Err(Connexion);
 //                   JR.Saf.Logs.Err(Requete);
 //                   JR.Saf.Logs.Except(e);
 //               };
	//	}

	//	public static string Get_String (DataRow Ligne, string Key)
	//	{
	//		try
	//		{
	//			object Val = Ligne[Key];
	//			if (Val == null||Val == DBNull.Value) return "";
	//			//return (string)Val;
	//			return Val.ToString();
	//		} 
	//		catch
	//		{
	//			return "";
	//		};
	//	}

	//	public static string Get_Guid (DataRow Ligne, string Key)
	//	{
	//		try
	//		{
	//			object Val = Ligne[Key];
	//			if (Val == null||Val == DBNull.Value) return "";
	//			return Val.ToString();
	//		} 
	//		catch
	//		{
	//			return "";
	//		};
	//	}

	//	public static string Get_Time (DataRow Ligne, string Key)
	//	{
	//		try
	//		{
	//			object Val = Ligne[Key];
	//			if (Val==null||Val == DBNull.Value) return "";
	//			return string.Format("{0:F}", (DateTime)Val);
	//		} 
	//		catch
	//		{
	//			return "";
	//		};
	//	}

	//	public static string Get_Float (DataRow Ligne, string Key)
	//	{
	//		try
	//		{
	//			object Val = Ligne[Key];
	//			if (Val==null||Val == DBNull.Value) return "";
	//			return string.Format("{0:F}", (double)Val);
	//		} 
	//		catch
	//		{
	//			return "";
	//		};
	//	}

 //       public static string Get_Int(DataRow Ligne, string Key)
 //       {
 //           try
 //           {
 //               object Val = Ligne[Key];
 //               if (Val == null || Val == DBNull.Value) return "";
 //               return ((int)Val).ToString();
 //           }
 //           catch
 //           {
 //               return "";
 //           };
 //       }

 //       public static bool Get_Bool(DataRow Ligne, string Key)
	//	{
	//		try
	//		{
	//			object Val = Ligne[Key];
	//			if (Val==null||Val == DBNull.Value) return false;
	//			return (bool)Val;
	//		} 
	//		catch
	//		{
	//			return false;
	//		};
	//	}

 //       public class Transaction
 //       {
 //           OleDbConnection OBD;
 //           OleDbTransaction OTR;
 //           SqlConnection SBD;
 //           SqlTransaction STR;
 //           bool _Sql_Mode;


 //           public Transaction (Base Base)
 //           {
 //               _Sql_Mode = Base.Sql_Mode;
 //               if (_Sql_Mode)
 //               {
 //                   SBD = new SqlConnection(Base.Connexion);
 //                   SBD.Open();
 //                   STR = SBD.BeginTransaction();
 //               }
 //               else
 //               {
 //                   OBD = new OleDbConnection(Base.Connexion);
 //                   OBD.Open();
 //                   OTR = OBD.BeginTransaction();
 //               }
 //           }

 //           public void Executer(string Requete, params object [] Parameters)
 //           // Execution d'une requete instantan�e
 //           {
 //               DbParameter[] Params = new DbParameter[Parameters.Length / 2];
 //               for (int I = 0; I < Params.Length; I++)
 //               {
 //                   Params[I] = new OleDbParameter(Parameters[2 * I] as string, Parameters[2 * I + 1]);
 //               }

 //               try
 //               {
 //                   if (_Sql_Mode)
 //                   {
 //                       SqlCommand Command = new SqlCommand(Requete, SBD);
 //                       Command.Transaction = STR;

 //                       if (Parameters != null && Parameters.Length > 0)
 //                       {
 //                           Command.CommandType = CommandType.StoredProcedure;
 //                           foreach (OleDbParameter P in Params)
 //                           {
 //                               Command.Parameters.Add(To_Sql(P));
 //                           };
 //                       };
 //                       Command.ExecuteNonQuery();
 //                   }
 //                   else
 //                   {
 //                       OleDbCommand Command = new OleDbCommand(Requete, OBD);
 //                       Command.Transaction = OTR;

 //                       if (Parameters != null && Parameters.Length > 0)
 //                       {
 //                           Command.CommandType = CommandType.StoredProcedure;
 //                           foreach (OleDbParameter P in Params)
 //                           {
 //                               Command.Parameters.Add(P);
 //                           };
 //                       };
 //                       Command.ExecuteNonQuery();
 //                   }

 //               }
 //               catch (Exception e)
 //               {
 //                   Debug.WriteLine(e.Message);
 //               };
 //           }

 //           public void Commit()
 //           {
 //               if (_Sql_Mode)
 //               {
 //                   STR.Commit();
 //                   SBD.Close();
 //               }
 //               else
 //               {
 //                   OTR.Commit();
 //                   OBD.Close();
 //               }
 //           }
 //       }
 //       public static int Row_Count(DataTable DT)
 //       {
 //           if (DT == null) return 0;
 //           if (DT.Rows == null) return 0;
 //           return DT.Rows.Count;
 //       }

 //       public static bool Is_Null(DataRow Ligne, string Key)
 //       {
 //           try
 //           {
 //               object Val = Ligne[Key];
 //               if (Val == null || Val == DBNull.Value) return true;
 //           }
 //           catch
 //           {
 //           };
 //           return false;
 //       }
 //   }

    public class DB_Row
    {
        public DataRow R;
        public DB_Row(DataRow DataRow)
        {
            UTF8 = false;
            R = DataRow;
        }

        public bool UTF8 { get; set; }
        public bool Null
        {
            get { return R == null; }
        }

        public bool Is_Null (string Key)
        {
            object Val = R[Key];
            return (Val == null || Val == DBNull.Value);
        }

        public object Value (string Key)
        {
            try
            {
                object Val;
                if (char.IsDigit(Key, 0))
                {
                    Val = R[JR.Convert.To_Integer(Key)];
                }
                else
                {
                    Val = R[Key];
                }
                if (Val == null || Val == DBNull.Value) return "";
                return Val;
            }
            catch
            {
                return "";
            };
        }

        string From_UTF8(object Val)
        {
            if (!(Val is string)) return Val.ToString();
            return Encoding.UTF8.GetString (Encoding.Unicode.GetBytes(Val.ToString()));
        }

        public string String(string Key)
        {
            try
            {
                object Val = R[Key];
                if (Val == null || Val == DBNull.Value) return "";
                if (UTF8) return From_UTF8(Val);
                return Val.ToString();
            }
            catch
            {
                return "";
            };
        }

        public string String(int Row)
        {
            try
            {
                object Val = R[Row];
                if (Val == null || Val == DBNull.Value) return "";
                if (UTF8) return From_UTF8(Val);
                return Val.ToString();
            }
            catch
            {
                return "";
            };
        }

        public Guid Guid(string Key)
        {
            try
            {
                object Val = R[Key];
                if (Val == null || Val == DBNull.Value) return System.Guid.Empty;
                return (Guid)Val;
            }
            catch
            {
                return System.Guid.Empty;
            };
        }

        public Guid Guid(int Row)
        {
            try
            {
                object Val = R[Row];
                if (Val == null || Val == DBNull.Value) return System.Guid.Empty;
                return (Guid)Val;
            }
            catch
            {
                return System.Guid.Empty;
            };
        }

        public string this[string Key]
        {
            get 
            {
                if (Key == "") return "";
                if (char.IsDigit(Key, 0))
                {
                    return this.String(JR.Convert.To_Integer(Key));
                }
                return this.String(Key); 
            }
        }

        public string this[int Row]
        {
            get { return this.String(Row); }
        }

        public int Int(string Key)
        {
            try
            {
                object Val = R[Key];
                if (Val == null || Val == DBNull.Value) return 0;
                return JR.Convert.To_Integer(Val);
            }
            catch
            {
                return 0;
            };
        }

        public int Int(int Key)
        {
            try
            {
                object Val = R[Key];
                if (Val == null || Val == DBNull.Value) return 0;
                return JR.Convert.To_Integer(Val);
            }
            catch
            {
                return 0;
            };
        }

        public long Long(string Key)
        {
            try
            {
                object Val = R[Key];
                if (Val == null || Val == DBNull.Value) return 0;
                return JR.Convert.To_Long(Val);
            }
            catch
            {
                return 0;
            };
        }

        public double Double(string Key)
        {
            try
            {
                object Val = R[Key];
                if (Val == null || Val == DBNull.Value) return 0.0;
                return JR.Convert.To_Float(Val);
            }
            catch
            {
                return 0.0;
            };
        }

        public double Double(int Key)
        {
            try
            {
                object Val = R[Key];
                if (Val == null || Val == DBNull.Value) return 0.0;
                return JR.Convert.To_Float(Val);
            }
            catch
            {
                return 0.0;
            };
        }

        public DateTime Time(string Key)
        {
            try
            {
                object Val = R[Key];
                if (Val == null || Val == DBNull.Value) return DateTime.MinValue;
                return JR.Convert.To_Time(Val);
            }
            catch
            {
                return DateTime.MinValue;
            };
        }

        public bool Bool(string Key)
        {
            try
            {
                object Val = R[Key];
                if (Val == null || Val == DBNull.Value) return false;
                return JR.Convert.To_Boolean(Val);
            }
            catch
            {
                return false;
            };
        }

        public bool Bool(int Key)
        {
            try
            {
                object Val = R[Key];
                if (Val == null || Val == DBNull.Value) return false;
                return JR.Convert.To_Boolean(Val);
            }
            catch
            {
                return false;
            };
        }

        public int Col_Count
        {
            get
            {
                if (R == null) return 0;
                return R.Table.Columns.Count;
            }
        }

    }

}
