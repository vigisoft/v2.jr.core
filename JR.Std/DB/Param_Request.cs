﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JR.DB
{
    // Interpretation et extraction d'une requete paramétrée
    public class Param_Request
    {
        public Param_Request(string Name, string Request)
        {
            _Name = Name;
            Analyze(Request);
        }

        string _Name = "";
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        void Analyze(string Req)
        {
            StringBuilder RQ = new StringBuilder();
            int S1 = 0;
            while(S1< Req.Length)
            {
                int P1 = Req.IndexOf ('[',S1);
                if (P1 < 0)
                {
                    RQ.Append (Req.Substring (S1));
                    break;
                }
                int P2 = Req.IndexOf (']',P1);
                if (P2 < 0)
                {
                    RQ.Append (Req.Substring (S1));
                    break;
                }
                Param P = Analyse_Param (Req.Substring (P1+1, P2-P1-1));
                if (P != null)
                {
                    RQ.Append (string.Format ("@",_Params.IndexOf (P)));
                }
                else
                {
                    RQ.Append(Req.Substring(S1, P2-S1+1));
                }
                S1 = P2 + 1;
            }
            _Request = RQ.ToString();
        }

        Param Analyse_Param(string Definition)
        {
            string [] Defs = Definition.Split ('|');
            if (Defs.Length == 1)
            {
                foreach (Param P in _Params)
                {
                    if (P.Id == Defs[0]) return P;
                }
            }
            else if (Defs.Length >= 3)
            {
                Param P = new Param(Defs);
                _Params.Add(P);
                return P;
            }
            return null;

        }
        List<Param> _Params = new List<Param>();
        public List<Param> Params
        {
            get
            {
                return _Params;
            }
        }

        public object[] Values
        {
            get
            {
                object[] V = new object[Params.Count];
                int I = 0;
                foreach (object O in Params)
                {
                    V[I] = O;
                    I++;
                }
                return V;
            }
        }

        string _Request;
        public string Request
        {
            get { return _Request; }
        }
        
        public class Param
        {
            string _Id;
            public string Id
            {
                get {return _Id;} set {_Id=value;}
            }
            string _Name;
            public string Name
            {
                get {return _Name;} set {_Name=value;}
            }
            Base_Type _Base_Type;
            public Base_Type Base_Type
            {
                get { return _Base_Type; }
            }
            //object _Value;
            public object Value
            {
                get { return _Base_Type.Value; }
                set { _Base_Type.Value = value; }
            }
            public Param(string [] Definition)
            {
                _Id = Definition[0];
                _Base_Type = Base_Type.New_Type((Definition[1]).Split(':'));
                _Name = Definition[2];
            }
        }
        public class Base_Type
        {
            internal static Base_Type New_Type(string[] Defs)
            {
                if (Defs.Length == 0) return new Invalid_Type();
                string Def_Value = Defs.Length < 2 ? "" : Defs[1];
                switch (Defs[0])
                {
                    case "S":
                        {
                            String_Type T = new String_Type();
                            T.Value = Def_Value;
                            return T;
                        }
                    case "I":
                        {
                            Int_Type T = new Int_Type();
                            T.Value = JR.Convert.To_Integer(Def_Value);
                            return T;
                        }
                    case "T":
                        {
                            Time_Type T = new Time_Type();
                            T.Value = To_Time(Def_Value);
                            return T;
                        }
                    case "F":
                        {
                            Float_Type T = new Float_Type();
                            T.Value = JR.Convert.To_Float(Def_Value);
                            return T;
                        }
                    case "B":
                        {
                            Bool_Type T = new Bool_Type();
                            T.Value = JR.Convert.To_Boolean(Def_Value);
                            return T;
                        }
                    case "E":
                        {
                            Enum_Type T = new Enum_Type();
                            T.Value = Def_Value;
                            for (int I = 2;I<Defs.Length;I+=2)
                            {
                                T.List.Add(new string[]{Defs[I],Defs[I+1]});
                            }
                            return T;
                        }
                }
                return new Invalid_Type();
            }

            private static object To_Time(string Def_Value)
            {
                string V = JR.Strings.Resize (Def_Value, 10);
                DateTime D = DateTime.Now;
                switch (V[0])
                {
                    case 'N': D = DateTime.Now; break;
                    case 'U': D = DateTime.UtcNow; break;
                    case 'D': D = Db.Db_Now; break;
                }
                switch (V[1])
                {
                    case 'D': D = D.Date; break;
                    case 'M': D = new DateTime(D.Year, D.Month, 1); break;
                    case 'Y': D = new DateTime(D.Year, 1, 1); break;
                }
                int Delta = JR.Convert.To_Integer(V.Substring(3));
                if (Delta > 0)
                {
                    switch (V[2])
                    {
                        case 'D': D = D.AddDays (Delta); break;
                        case 'M': D = D.AddMonths (Delta); break;
                        case 'Y': D = D.AddYears (Delta); break;
                    }
                }
                return D;
            }
            object _Value;
            public virtual object Value
            {
                get { return _Value; }
                set { _Value = value; }
            }
        }
        public class Invalid_Type : Base_Type
        {
        }
        public class Int_Type : Base_Type
        {
        }
        public class String_Type : Base_Type
        {
        }
        public class Float_Type : Base_Type
        {
        }
        public class Enum_Type : Base_Type
        {
            List<string[]> _List = new List<string[]>();
            public List<string[]> List
            {
                get { return _List; }
            }
        }
        public class Bool_Type : Base_Type
        {
        }
        public class Time_Type : Base_Type
        {
        }
    }
}
