using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace JR.DB
{

    public class Db_Config : SQL_Db
    {

        public string Logger_Prefix = "Sql";

        struct Audit
        {
            public string Audit_String;
            public string Filter;
            public int Max_Len;
        }
        static Audit No_Audit = new Audit { Filter = null, Max_Len = 0, Audit_String = "" };
        Audit _Audit = No_Audit;

        public string Audit_String
        {
            get => _Audit.Audit_String;
            set
            {

                _Audit = No_Audit;
                if (value == "") return;
                var C = value.Scan();
                var Filter = C.Next(";").Trim();
                if (Filter == "") return;
                if (Filter == "\\w*") Filter = "*";
                var Max_Len = C.Next(";").To_Integer();
                _Audit = new Audit { Audit_String = value, Filter = Filter, Max_Len = Max_Len };
            }
        }

        public delegate string Log(string Message, params object[] Params);
        public string Default_Logger(string Message, params object[] Params) => JR.Saf.Logs.File_Log(Logger_Prefix, Message, Params);
        public string Default_Error_Logger(string Message, params object[] Params) => JR.Saf.Logs.File_Log(Logger_Prefix + "_err", Message, Params);

        bool Init = true;
        bool Use_Default_Logger = true;
        Log _Trace_Logger = null;
        Log _Error_Logger = null;

        void Check_Logger()
        {
            if (!Init) return;
            Init = false;
            if (Use_Default_Logger)
            {
                _Trace_Logger = Default_Logger;
                _Error_Logger = Default_Error_Logger;
            }
            if (_Trace_Logger != null)
            {
                _Trace_Logger("Connection string = " + Connection_String);
            }
        }
        public Log Error_Logger
        {
            get
            {
                Check_Logger();
                return _Error_Logger;
            }
            set
            {
                Use_Default_Logger = false;
                _Error_Logger = value;
            }
        }

        public Log Trace_Logger
        {
            get
            {
                Check_Logger();
                return _Trace_Logger;
            }
            set
            {
                Use_Default_Logger = false;
                _Trace_Logger = value;
            }
        }

        public bool No_Write = false;
        Audit Suspended_Audit = No_Audit;
        public void Suspend_Audit()
        {
            Suspended_Audit = _Audit;
            _Audit = No_Audit;
        }
        public void Resume_Audit()
        {
            _Audit = Suspended_Audit;
        }

        internal long Track_Id = 0;
        public Tracker New_Tracker(string Request = null)
        {
            if (_Audit.Filter == null) return null;
            if (Trace_Logger == null) return null;
            if (Request != null && Request.StartsWith("  ")) return null;
            return new Tracker(this);
        }

        internal void Trace(string Message, params object[] Params)
        {
            Trace(0, Message, Params);
        }
        internal void Trace_Exception(Exception E, string Request, params object[] Params)
        {
            Trace(-1, Request + "\r\n" + E.Message, Params);
        }

        internal void Trace(long Id, string Message, params object[] Params)
        {
            Log Logger = Id < 0 ? Error_Logger : Trace_Logger;
            if (Logger == null) return;
            var SB = new StringBuilder();
            if (Id > 0)
            {
                SB.Append(Id);
                SB.Append("/");
            }
            SB.Append(Message);
            for (int P = 0; P < Params.Length; P++)
            {
                SB.AppendLine();
                SB.Append($"  - @");
                SB.Append(P);
                SB.Append("=");
                object Param = Params[P];
                if (Param == null)
                {
                    SB.Append("<null>");
                }
                else
                {
                    var Mess = Param.ToString();
                    if (_Audit.Max_Len > 0 && Mess.Length > _Audit.Max_Len)
                    {
                        SB.Append($" [{Param.GetType()} - Len={Mess.Length}]\r\n" + Mess.Substring(0, _Audit.Max_Len));
                    }
                    else
                    {
                        SB.Append($"{Mess} [{Param.GetType()}]");
                    }
                }
            }
            Logger(SB.ToString());
        }
        string Audit_Filter => _Audit.Filter;
        public class Tracker
        {
            internal Db_Config Config { get; set; }
            long _Id = 0;
            Stopwatch W = new Stopwatch();
            public void Track(string Request, params object[] Params)
            {
                if (Config.Audit_Filter != "*" && !Regex.IsMatch(Request, Config.Audit_Filter)) return;
                Config.Track_Id++;
                _Id = Config.Track_Id;
                Config.Trace(_Id, Request, Params);
                W.Start();
            }
            public void End_Track(object Result)
            {
                if (_Id == 0) return;
                object S_Result = Result;
                if (Result is DbDataReader)
                {
                    S_Result = To_String(Result as DbDataReader);
                }
                Config.Trace(_Id, $"[Ok in {W.Elapsed}s] R={S_Result}");
                W.Stop();
            }
            static string To_String(DbDataReader DR)
            {
                if (!DR.HasRows) return "Empty";
                var SB = new StringBuilder();
                for (int I = 0; I < DR.FieldCount; I++)
                {
                    if (I > 0) SB.Append(",");
                    SB.Append(DR.GetName(I));
                    SB.Append(":");
                    SB.Append(DR.GetDataTypeName(I));
                }
                return SB.ToString();
            }

            public Tracker(Db_Config Config)
            {
                this.Config = Config;
            }
        }

    }

    // On passe un type g�n�rique diff�rent pour forcer la cr�ation de plusieurs classes
    //  Statiques bien distinctes
    public class Gen_Db<T> : Dbase
    {
        public static Gen_Db<T> Def_Db { get; set; }
        // C'est le fait de mettre une chaiune de connexion par d�faut qui pr�pare la base par d�faut
        public static Db_Config Default_Config
        {
            get { return Def_Db.Config; }
            set { Def_Db = new Gen_Db<T> { Config = value }; }
        }
        public static Exception Check_Connection()
        {
            return Def_Db.Check();
        }
        public static int Execute_Int(string Request, params object[] Params)
        {
            return Def_Db.Exec_Int(Request, Params);
        }
        public static long Execute_Long(string Request, params object[] Params)
        {
            return Def_Db.Exec_Long(Request, Params);
        }
        public static DateTime Execute_Date(string Request, params object[] Params)
        {
            return Def_Db.Exec_Date(Request, Params);
        }
        public static string Execute_String(string Request, params object[] Params)
        {
            return Def_Db.Exec_String(Request, Params);
        }
        public static double Execute_Float(string Request, params object[] Params)
        {
            return Def_Db.Exec_Float(Request, Params);
        }
        public static DataTable Execute_Table(string Request, params object[] Params)
        {
            return Def_Db.Exec_Table(Request, Params);
        }
        public static DataRow Execute_Row(string Request, params object[] Params)
        {
            return Def_Db.Exec_Row(Request, Params);
        }
        public static Db_Row Execute_Db_Row(string Request, params object[] Params)
        {
            return Def_Db.Exec_Db_Row(Request, Params);
        }
        public static DataTable Execute_Request(string Request, params object[] Params)
        {
            return Def_Db.Exec_Request(Request, Params);
        }
        public static decimal Insert(string Request, params object[] Params)
        {
            return Def_Db.Insert_Request(Request, Params);
        }
        public static bool Create_Table(string Table_Name, params string[] Create_Commands)
        {
            return Def_Db.Check_Table(Table_Name, Create_Commands);
        }

        public static bool Execute(string Request, params object[] Params)
        {
            return Def_Db.Exec(Request, Params);
        }
        public static DTransaction New_Transaction()
        {
            return Def_Db.New_Trans;
        }
        public static DateTime Db_Now
        {
            get
            {
                return Def_Db.Now;
            }
        }
    }
    public class Dbase
    {
        public Dbase() { }
        public Dbase(string Connection_String)
        {
            Config = new Db_Config { Connection_String = Connection_String };
        }
        public Db_Config Config { get; set; }

        public Exception Check()
        {
            using (var _DB = Config.New_Connection)
            {
                try
                {
                    _DB.Open();
                }
                catch (Exception e)
                {
                    return e;
                }
            }
            return null;
        }

        // Retourne la chaine avec les quotes
        public static string To_Arg(string Value)
        {
            if (Value == null) return "''";
            return "'" + Value.Replace("'", "''") + "'";
        }

        // Retourne la chaine sans les quotes
        public static string To_Arg_No_Quote(string Value)
        {
            if (Value == null) return "";
            return Value.Replace("'", "''");
        }

        public static string To_Arg_No_Quote_No_Generic(string Value)
        {
            if (Value == null) return "";
            return Value.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]");
        }

        // Retourne la date avec les quotes
        public static string To_Arg(DateTime Value)
        {
            return "'" + Value.ToString("dd/MM/yyyy HH:mm:ss") + "'";
        }

        // Retourne une liste de chaines
        public static string To_Arg(IEnumerable<string> Value)
        {
            StringBuilder SB = new StringBuilder("(");
            bool First = true;
            foreach (string S in Value)
            {
                if (!First) SB.Append(',');
                SB.Append(To_Arg(S.Trim()));
                First = false;
            }
            SB.Append(')');
            return SB.ToString();
        }

        // Lecture d'un entier
        public const int Error_Int = -99999;
        public int Exec_Int(string Request, params object[] Params)
        {
            if (Config.No_Write) return Error_Int;
            object Result = Exec_Object(Request, Params);
            if (Result is int) return (int)Result;
            if (Result is short) return (int)((short)Result);
            if (Result is bool) return (bool)Result ? 1 : 0;
            if (Result is decimal) return (int)((decimal)Result);
            if (Result == DBNull.Value) return 0;
            return Error_Int;
        }

        public const long Error_Long = -99999;
        public long Exec_Long(string Request, params object[] Params)
        {
            if (Config.No_Write) return Error_Long;
            object Result = Exec_Object(Request, Params);
            if (Result is int) return (long)((int)Result);
            if (Result is short) return (long)((short)Result);
            if (Result is decimal) return (long)((decimal)Result);
            if (Result is bool) return (bool)Result ? 1 : 0;
            if (Result is long) return (long)Result;
            if (Result == DBNull.Value) return 0;
            return Error_Long;
        }

        public static DateTime Error_Date = DateTime.MinValue;
        public DateTime Exec_Date(string Request, params object[] Params)
        {
            if (Config.No_Write) return Error_Date;
            object Result = Exec_Object(Request, Params);
            if (Result is DateTime) return (DateTime)Result;
            if (Result == DBNull.Value) return DateTime.MinValue;
            return Error_Date;
        }

        public string Exec_String(string Request, params object[] Params)
        {
            if (Config.No_Write) return "";
            object Result = Exec_Object(Request, Params);
            if (Result == null) return "";
            if (Result is string) return (string)Result;
            if (Result == DBNull.Value) return "";
            return Result.ToString();
        }

        public const double Error_Float = Double.NaN;
        public double Exec_Float(string Request, params object[] Params)
        {
            if (Config.No_Write) return Error_Float;
            object Result = Exec_Object(Request, Params);
            if (Result is double) return (double)Result;
            if (Result == DBNull.Value) return 0.0;
            return Error_Float;
        }

        //static void Audit(string Request, params object[] Params)
        //{
        //    if (_Log == null) return;
        //    if (Regex.IsMatch (string.Format(Request, Params),_Audit)) Trace (Request, Params);
        //}

        static object To_Date(object Value)
        {
            if (Value.Equals(DateTime.MinValue)) return (object)DBNull.Value;
            return Value;
        }

        static DbParameter Associate_Return_Value(DbCommand Com)
        {
            // Ce parametre est en out car en return value ca ne fonctionne pas (en tout cas pas avec moi ..)
            // La syntaxe � respecter est donc exec @Return=proc p1,P2, ...
            DbParameter SP = Com.CreateParameter();
            SP.ParameterName = "@Return";
            SP.DbType = DbType.Int32;
            SP.Direction = ParameterDirection.Output;
            Com.Parameters.Add(SP);
            return SP;
        }

        public static void Associate_Db_Params(DbCommand Com, params object[] Params)
        {
            Com.CommandTimeout = 100;
            bool First = true;
            foreach (DbParameter Param in Params)
            {
                Com.CommandText += $"{(First ? " " : ",")}{Param.ParameterName}={Param.ParameterName}";
                Com.Parameters.Add(Param);
                First = false;
            }
        }

        // Le @A sert � g�n�rer un varchar ASCII (et non un nvarchar Unicode), ce qui p�nalise moins les recherches
        public static void Associate_Params(DbCommand Com, params object[] Params)
        {
            Com.CommandTimeout = 100;
            if (!Com.CommandText.Contains("@")) return;
            bool Ascii_Strings = Com.CommandText.Contains("@A");
            int P = 0;
            foreach (object Param in Params)
            {
                DbParameter SP = Com.CreateParameter();
                SP.ParameterName = "@" + P;
                Action<object> PushA = (Value) =>
                {
                    Com.Parameters.Add(SP);
                    SP = Com.CreateParameter();
                    SP.ParameterName = "@A" + P;
                    SP.DbType = DbType.AnsiString;
                    SP.Value = Value;
                };
                if (Param == null)
                {
                    SP.Value = DBNull.Value;
                    if (Ascii_Strings)
                    {
                        PushA(DBNull.Value);
                    }

                }
                else
                    if (Param is bool)
                {
                    SP.Value = (bool)Param ? 1 : 0;
                }
                else
                        if (Param is DateTime)
                {
                    SP.Value = To_Date(Param);
                }
                else
                {
                    SP.Value = Param;
                    if (Ascii_Strings && Param is string)
                    {
                        PushA(Param);
                    }
                }
                Com.Parameters.Add(SP);
                P++;
            }

            // Transformation des param�tres @[n-m] en @n,...@m ou @[*] en @0, ... @Params.Length-1
            var SC = Com.CommandText.Scan();
            while (!SC.End)
            {
                SC.Next_Word("@[");
                if (!SC.Found) break;
                int Deb = SC.Found_Index;
                int R0 = 0;
                int R1 = Params.Length - 1;
                string S0 = SC.Next("-]");
                if (S0 != "*") R0 = JR.Convert.To_Integer(S0);
                if (SC.Separator == '-')
                {
                    R1 = JR.Convert.To_Integer(SC.Next("]"));
                }
                int End = SC.Found_Index;
                if (R0 > R1) continue;
                var SB = new StringBuilder();
                for (int I = R0; I <= R1; I++)
                {
                    if (I != R0) SB.Append(",");
                    SB.Append("@" + I.ToString());
                }
                Com.CommandText = Com.CommandText.Replace(SC.Range(Deb, End), SB.ToString());
            }
        }

        static void Associate_Command(DbCommand Command, string Request, params object[] Params)
        {
            bool Db_Params = false;
            foreach (var P in Params)
            {
                Db_Params = P is DbParameter;
                break;
            }

            bool With_Params = (!Db_Params) && Params.Length > 0;
            if (With_Params)
            {
                Request = string.Format(Request, Params);
            }
            if (Request.IndexOf(' ') < 0)
            {
                Request = "exec " + Request;
                if (With_Params)
                {
                    Request = Request + " @[*]";
                }
            }
            Command.CommandText = Request;
            if (With_Params)
            {
                Associate_Params(Command, Params);
            }
            else if (Db_Params)
            {
                Associate_Db_Params(Command, Params);
            }

        }
        public static DbCommand Build_Command(Db_Config Config, string Request, DbConnection Connection, params object[] Params)
        {
            var Command = Connection.CreateCommand();
            Associate_Command(Command, Request, Params);
            return Command;
        }

        public static DbDataAdapter Build_Adapter(Db_Config Config, string Request, DbConnection Connection, params object[] Params)
        {
            var P = Config.New_Adapter(Request, Connection);
            var Command = P.SelectCommand;
            Associate_Command(Command, Request, Params);
            return P;
        }

        public DTransaction New_Trans
        {
            get { return new DTransaction(this); }
        }

        object Exec_Object(string Request, params object[] Params)
        {
            object Result = null;
            using (var _DB = Config.New_Connection)
            {
                Db_Config.Tracker T = Config.New_Tracker(Request);
                try
                {
                    _DB.Open();
                    if (T != null) T.Track(Request, Params);
                    using (DbCommand Com = Build_Command(Config, Request, _DB, Params))
                    {
                        Result = Com.ExecuteScalar();
                        Com.Cancel();
                    }
                    if (T != null) T.End_Track(Result);
                    _DB.Close();
                    _DB.Dispose();
                }
                catch (Exception E)
                {
                    Config.Trace_Exception(E, Request, Params);
                }
            }
            return Result;
        }

        public DataTable Exec_Table(string Request, params object[] Params)
        {
            using DataSet Ds = new DataSet();
            using (var _DB = Config.New_Connection)
            {
                _DB.Open();
                Db_Config.Tracker T = Config.New_Tracker(Request);
                try
                {
                    if (T != null) T.Track(Request, Params);
                    using (DbDataAdapter SA = Dbase.Build_Adapter(Config, Request, _DB, Params))
                    {
                        int Count = SA.Fill(Ds);
                        if (T != null) T.End_Track($"{Count} lines ");
                    }
                }
                catch (Exception E)
                {
                    Config.Trace_Exception(E, Request, Params);
                }
            }
            if (Ds.Tables == null) return null;
            if (Ds.Tables.Count == 0) return null;
            DataTable DT = Ds.Tables[0];
            DT.ExtendedProperties["Gen_Db"] = this;
            return DT;
        }
        public DataSet Exec_DataSet(string Request, params object[] Params)
        {
            DataSet Ds = new DataSet();
            using (var _DB = Config.New_Connection)
            {
                _DB.Open();
                Db_Config.Tracker T = Config.New_Tracker(Request);
                try
                {
                    if (T != null) T.Track(Request, Params);
                    using (DbDataAdapter SA = Dbase.Build_Adapter(Config, Request, _DB, Params))
                    {
                        int Count = SA.Fill(Ds);
                        if (T != null) T.End_Track($"{Count} lines ");
                    }
                }
                catch (Exception E)
                {
                    Config.Trace_Exception(E, Request, Params);
                }
            }
            return Ds;
        }

        public DataRow Exec_Row(string Request, params object[] Params)
        {
            using var DT = Exec_Table(Request, Params);
            if (DT == null) return null;
            if (DT.Rows.Count == 0) return null;
            return DT.Rows[0];
        }

        public DataRowCollection Exec_Rows(string Request, params object[] Params)
        {
            using (DataTable DT = Exec_Table(Request, Params))
            {
                if (DT == null) return null;
                if (DT.Rows.Count == 0) return null;
                return DT.Rows;
            }
        }

        public Db_Row Exec_Db_Row(string Request, params object[] Params)
        {
            return new Db_Row(Exec_Row(Request, Params));
        }

        public DataTable Exec_Request(string Request, params object[] Params)
        {
            DataSet Ds = new DataSet();
            using (var _DB = Config.New_Connection)
            {
                _DB.Open();
                Db_Config.Tracker T = Config.New_Tracker(Request);
                try
                {
                    if (T != null) T.Track(Request, Params);
                    using (DbDataAdapter SA = Dbase.Build_Adapter(Config, Request, _DB, Params))
                    {
                        int Count = SA.Fill(Ds);
                        if (T != null) T.End_Track($"{Count} lines ");
                    }
                }
                catch (Exception E)
                {
                    DataTable DT = new DataTable();
                    DT.Columns.Add("Erreur");
                    DataRow DR = DT.NewRow();
                    DR[0] = E.Message;
                    DT.Rows.Add(DR);
                    Config.Trace_Exception(E, Request, Params);
                    return DT;
                }
            }
            if (Ds.Tables == null) return null;
            if (Ds.Tables.Count == 0) return null;
            return Ds.Tables[0];
        }

        public decimal Insert_Request(string Request, params object[] Params)
        {
            if (Config.No_Write) return 9999999;
            object Result = DBNull.Value;
            using (var _DB = Config.New_Connection)
            {
                _DB.Open();
                Db_Config.Tracker T = Config.New_Tracker(Request);
                try
                {
                    if (T != null) T.Track(Request, Params);
                    using (DbCommand Com = Build_Command(Config, Request, _DB, Params))
                    {
                        if (Com.ExecuteNonQuery() > 0)
                        {
                            Com.Dispose();
                            using (DbCommand Com2 = Config.New_Command(Config.MySql_Mode ? "select last_insert_id()" : "select @@identity", _DB))
                            {
                                Result = Com2.ExecuteScalar();
                            }
                        };
                    }
                    if (T != null) T.End_Track(Result);
                }
                catch (Exception E)
                {
                    Config.Trace_Exception(E, Request, Params);
                }
            }
            if (Result == DBNull.Value)
            {
                return 0;
            }
            return System.Convert.ToDecimal(Result);
        }

        public bool Check_Table(string Table_Name, params string[] Create_Commands)
        {
            // Pour tester la pr�sence de la table on compte ses lignes
            if (Exec($"select count(*) from [{Table_Name}]")) return true;

            if (Config.No_Write) return false;

            using (var _DB = Config.New_Connection)
            {
                try
                {
                    _DB.Open();
                    foreach (string Req in Create_Commands)
                    {
                        using (DbCommand Com = Config.New_Command(Req, _DB))
                        {
                            int Count = Com.ExecuteNonQuery();
                        }
                    }
                }
                catch
                {
                    return false;
                }
            }
            return true;

        }

        public bool Exec(string Request, params object[] Params)
        {
            if (Config.No_Write) return true;

            bool Success = false;
            using (var _DB = Config.New_Connection)
            {
                _DB.Open();
                Db_Config.Tracker T = Config.New_Tracker(Request);
                try
                {
                    if (T != null) T.Track(Request, Params);
                    using (DbCommand Com = Build_Command(Config, Request, _DB, Params))
                    {
                        int Count = Com.ExecuteNonQuery();
                        Success = true;
                        if (T != null) T.End_Track($"{Count} lines");
                    }
                }
                catch (Exception E)
                {
                    Config.Trace_Exception(E, Request, Params);
                }
            }
            return Success;
        }

        DateTime _Db_Now = DateTime.MinValue;
        DateTime _Date_Db_Now = DateTime.MinValue;

        public DateTime Now
        {
            get
            {
                if (_Date_Db_Now < DateTime.UtcNow.AddHours(-1.0))
                {
                    _Db_Now = Exec_Date("  select getdate() as D");
                    _Date_Db_Now = DateTime.UtcNow;
                }
                return _Db_Now.Add(DateTime.UtcNow - _Date_Db_Now);

            }
        }

        public class Reader : IDisposable
        {

            string _Request = null;
            object[] _Params = null;
            IDataReader DR = null;
            DbConnection DB = null;
            DbCommand Com = null;
            bool First_Read = true;
            bool Terminated = false;
            string _Last_Error = "";
            int Cur_Row = 0;
            DataTable DT = null;
            DbParameter Return_Param = null;
            public Db_Config Config { get; set; }

            public bool Can_Cancel = false;

            CancellationTokenSource CTS = null;
            public void Cancel()
            {
                CTS?.Cancel(true);
            }

            public Reader(string Request, params object[] Params)
            {
                _Request = Request;
                _Params = Params;
            }

            public Reader(DataTable Table)
            {
                DT = Table;
            }

            public Reader(DataRow Data_Row)
            {
                DT = Data_Row.Table.Clone();
                DT.ImportRow(Data_Row);
            }

            void Terminate()
            {
                Terminated = true;
                if (Com != null)
                {
                    Com.Cancel();
                    Com.Dispose();
                    Com = null;
                }
                if (!First_Read)
                {
                    if (DR != null) DR.Close();
                    if (DB != null) DB.Close();
                }
                if (CTS != null)
                {
                    CTS.Dispose();
                    CTS = null;
                }
            }

            // Charge toutes les donn�es dans une table
            public bool Read_Atomic()
            {
                return Read(true);
            }

            public bool Read(bool Atomic = false)
            {
                if (Atomic && First_Read && DT == null)
                {
                    var D = new Gen_Db<Reader> { Config = Config };
                    DT = D.Exec_Table(_Request, _Params);
                }
                Cur_Row = 0;
                if (Terminated) return false;
                if (First_Read)
                {
                    First_Read = false;
                    if (DT == null)
                    {
                        DB = Config.New_Connection;
                        DB.Open();
                        Db_Config.Tracker T = Config.New_Tracker(_Request);
                        if (T != null) T.Track(_Request, _Params);
                        //using (DbCommand Com = Config.New_Command(string.Format(_Request, _Params), DB))
                        Com = Dbase.Build_Command(Config, _Request, DB, _Params);
                        {
                            Return_Param = Associate_Return_Value(Com);
                            //Associate_Params(Com, _Params);
                            try
                            {
                                if (Can_Cancel)
                                {
                                    CTS = new CancellationTokenSource();
                                    DR = Com.ExecuteReaderAsync(CTS.Token).Result;
                                    if (CTS.IsCancellationRequested) return false;
                                }
                                else
                                {
                                    DR = Com.ExecuteReader();
                                }
                                if (T != null) T.End_Track(DR);
                            }
                            catch (Exception E)
                            {
                                _Last_Error = E.Message;
                                Config.Trace_Exception(E, _Request, _Params);
                                Terminate();
                                return false;
                            };
                        }
                    }
                    else
                    {
                        try
                        {
                            DR = new DataTableReader(DT);
                        }
                        catch (Exception E)
                        {
                            _Last_Error = E.Message;
                            Config.Trace_Exception(E, _Request, _Params);
                            Terminate();
                            return false;
                        };
                    }
                }

                if (!DR.Read())
                {
                    // Terminate(); plus vraiment besoin car un impose toujours soit un close, soit un using
                    return false;
                }
                return true;
            }

            public bool Next_Results()
            {
                if (Terminated) return false;
                return !DR.NextResult();
            }


            // !!! Les valeurs en out ou return ne peuvent �tre lues qu'apres avoir parcouru tout le DR
            public int Return_Value
            {
                get
                {
                    if (Return_Param == null) return -1;
                    return JR.Convert.To_Integer(Return_Param.Value);
                }
            }

            public void Close()
            {
                Terminate();
            }

            public IDataReader Data_Reader
            {
                get { return DR; }
            }

            public void Skip_Next_Row()
            {
                Cur_Row += 1;
            }

            public int Current_Row
            {
                get
                {
                    return Cur_Row;
                }
                set
                {
                    Cur_Row = value;
                }
            }

            public bool Goto_Row(string Key)
            {
                try
                {
                    Cur_Row = DR.GetOrdinal(Key);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            bool Null_Row(int Row)
            {
                return Row < 0 || DR.FieldCount <= Row || DR.IsDBNull(Row);
            }

            int Get_Row(string Row)
            {
                if (Goto_Row(Row)) return Cur_Row;
                return -1;
            }

            public bool Is_Null(string Row)
            {
                return Is_Null(Get_Row(Row));
            }

            public bool Is_Null(int Row)
            {
                Cur_Row = Row;
                return Null_Row(Row);
            }

            public bool Next__Null
            {
                get { return Is_Null(Cur_Row); }
            }

            public object Get_Value(string Row)
            {
                return Get_Value(Get_Row(Row));
            }

            public object Get_Value(int Row)
            {
                Cur_Row = Row + 1;
                return Null_Row(Row) ? null : DR.GetValue(Row);
            }
            public object Next_Value
            {
                get { return Get_Value(Cur_Row); }
            }

            public string Get_String(string Row)
            {
                return Get_String(Get_Row(Row));
            }

            public string Get_String(int Row)
            {
                Cur_Row = Row + 1;
                return Null_Row(Row) ? "" : DR.GetValue(Row).ToString();
            }
            public string Get_String_Null(int Row)
            {
                Cur_Row = Row + 1;
                return Null_Row(Row) ? null : DR.GetValue(Row).ToString();
            }
            public T Get_Json<T>(string Row)
            {
                return Get_Json<T>(Get_Row(Row));
            }

            public T Get_Json<T>(int Row)
            {
                Cur_Row = Row + 1;
                return Null_Row(Row) ? default(T) : JR.Serial<T>.From_Json(DR.GetValue(Row).ToString());
            }
            public string Next_String
            {
                get { return Get_String(Cur_Row); }
            }
            public string Next_String_Null
            {
                get { return Get_String_Null(Cur_Row); }
            }
            public T Next_Json<T>()
            {
                return Get_Json<T>(Cur_Row);
            }

            public Guid Next_Guid
            {
                get { return Get_Guid(Cur_Row); }
            }
            public Guid Get_Guid(string Row)
            {
                return Get_Guid(Get_Row(Row));
            }
            public Guid Get_Guid(int Row)
            {
                Cur_Row = Row + 1;
                if (Null_Row(Row)) return Guid.Empty;
                return DR.GetGuid(Row);
            }

            public byte[] Next_Bytes
            {
                get { return Get_Bytes(Cur_Row); }
            }
            public byte[] Get_Bytes(string Row)
            {
                return Get_Bytes(Get_Row(Row));
            }
            public byte[] Get_Bytes(int Row)
            {
                Cur_Row = Row + 1;
                if (Null_Row(Row)) return new byte[] { };
                long Count = DR.GetBytes(Row, 0, null, 0, 0);
                byte[] Value = new byte[Count];
                DR.GetBytes(Row, 0, Value, 0, Value.Length);
                return Value;
            }

            public char Next_Char
            {
                get { return Get_Char(Cur_Row); }
            }
            public char Get_Char(string Row)
            {
                return Get_Char(Get_Row(Row));
            }
            public char Get_Char(int Row)
            {
                Cur_Row = Row + 1;
                return Null_Row(Row) ? ' ' : (DR.GetValue(Row).ToString() + " ")[0];
            }

            public string Get_String(string Row, string Format)
            {
                return Get_String(Get_Row(Row), Format);
            }
            public string Get_String(int Row, string Format)
            {
                Cur_Row = Row + 1;
                return Null_Row(Row) ? "" : string.Format(Format, DR.GetValue(Row));
            }
            public string Get_Next_String(string Format)
            {
                return Get_String(Cur_Row, Format);
            }

            // Traite aussi bien le nom que le rang
            string Row_Index(string Key)
            {
                foreach (char C in Key)
                {
                    if (!char.IsDigit(C))
                    {
                        return Get_Row(Key).ToString();
                    }
                }
                return Key;
            }

            // Remplace les {0} par leurs valeurs et les {Row_name} pareillement
            public string Format(string Format)
            {
                StringBuilder SB = new StringBuilder();
                JR.Strings.Cursor C = new Strings.Cursor(Format);
                while (!C.End)
                {
                    string S = C.Next("{");
                    SB.Append(S);
                    if (C.Found)
                    {
                        SB.Append(C.Separator);
                        string Key = C.Next(":}");
                        if (C.Found)
                        {
                            SB.Append(Row_Index(Key));
                            SB.Append(C.Separator);
                        }
                        else
                        {
                            SB.Append(Key);  // Format non valide
                        }
                    }
                }
                object[] Values = new object[DR.FieldCount];
                DR.GetValues(Values);
                return string.Format(SB.ToString(), Values);
            }

            public int Get_Int(string Row)
            {
                return Get_Int(Get_Row(Row));
            }
            public int Get_Int(int Row)
            {
                Cur_Row = Row + 1;
                if (Null_Row(Row)) return 0;
                var T = DR.GetFieldType(Row);
                if (T == typeof(string)) return JR.Convert.To_Integer(DR.GetValue(Row).ToString());
                if (T == typeof(Decimal)) return (int)DR.GetDecimal(Row);
                if (T == typeof(Double)) return (int)DR.GetDouble(Row);
                if (T == typeof(float)) return (int)DR.GetFloat(Row);
                if (T == typeof(Byte)) return (int)DR.GetByte(Row);
                if (T == typeof(System.Int64)) return (int)DR.GetInt64(Row);
                if (T == typeof(System.Int16)) return (int)DR.GetInt16(Row);
                if (T == typeof(System.Boolean)) return DR.GetBoolean(Row) ? 1 : 0;
                return DR.GetInt32(Row);
            }
            public int? Get_Int_Null(int Row)
            {
                Cur_Row = Row + 1;
                if (Null_Row(Row)) return null;
                var T = DR.GetFieldType(Row);
                if (T == typeof(string)) return JR.Convert.To_Integer(DR.GetValue(Row).ToString());
                if (T == typeof(Decimal)) return (int)DR.GetDecimal(Row);
                if (T == typeof(Double)) return (int)DR.GetDouble(Row);
                if (T == typeof(float)) return (int)DR.GetFloat(Row);
                if (T == typeof(Byte)) return (int)DR.GetByte(Row);
                if (T == typeof(System.Int64)) return (int)DR.GetInt64(Row);
                if (T == typeof(System.Int16)) return (int)DR.GetInt16(Row);
                if (T == typeof(System.Boolean)) return DR.GetBoolean(Row) ? 1 : 0;
                return DR.GetInt32(Row);
            }
            public int Next_Int
            {
                get { return Get_Int(Cur_Row); }
            }
            public int? Next_Int_Null
            {
                get { return Get_Int_Null(Cur_Row); }
            }

            public bool Next_Bool
            {
                get { return Get_Bool(Cur_Row); }
            }
            public bool Get_Bool(string Row)
            {
                return Get_Bool(Get_Row(Row));
            }
            public bool Get_Bool(int Row)
            {
                Cur_Row = Row + 1;
                if (Null_Row(Row)) return false;
                if (DR.GetFieldType(Row) == typeof(int)) return DR.GetInt32(Row) != 0;
                return DR.GetBoolean(Row);
            }

            public long Next_Long
            {
                get { return Get_Long(Cur_Row); }
            }

            public long Get_Long(string Row)
            {
                return Get_Long(Get_Row(Row));
            }
            public long Get_Long(int Row)
            {
                Cur_Row = Row + 1;
                if (Null_Row(Row)) return 0;
                var T = DR.GetFieldType(Row);
                if (T == typeof(Decimal)) return (long)DR.GetDecimal(Row);
                if (T == typeof(Double)) return (long)DR.GetDouble(Row);
                if (T == typeof(Byte)) return (long)DR.GetByte(Row);
                if (T == typeof(System.Int16)) return (long)DR.GetInt16(Row);
                if (T == typeof(System.Int32)) return (long)DR.GetInt32(Row);
                return DR.GetInt64(Row);
            }

            public double Next_Float
            {
                get { return Get_Float(Cur_Row); }
            }
            public double Next_Round_Float(int Precision = 4)
            {
                return Get_Float(Cur_Row, Precision);
            }

            public double? Next_Float_Null
            {
                get { return Get_Float_Null(Cur_Row); }
            }
            public double? Next_Round_Float_Null(int Precision = 4)
            {
                return Get_Float_Null(Cur_Row, Precision);
            }

            public double Get_Float(string Row)
            {
                return Get_Float(Get_Row(Row));
            }

            public double Get_Float(int Row, int Precision)
            {
                return Math.Round(Get_Float(Row), Precision);
            }

            public double Get_Float(int Row)
            {
                Cur_Row = Row + 1;
                if (Null_Row(Row)) return 0.0;
                var T = DR.GetFieldType(Row);
                if (T == typeof(Decimal)) return (double)DR.GetDecimal(Row);
                if (T == typeof(System.Int64)) return (double)DR.GetInt64(Row);
                if (T == typeof(System.Int32)) return (long)DR.GetInt32(Row);
                if (T == typeof(System.Int16)) return (double)DR.GetInt16(Row);
                if (T == typeof(System.Single)) return (double)DR.GetFloat(Row);
                return DR.GetDouble(Row);
            }

            public double? Get_Float_Null(int Row, int Precision)
            {
                var Val = Get_Float_Null(Row);
                if (Val == null) return null;
                return Math.Round((double)Val, Precision);
            }

            public double? Get_Float_Null(int Row)
            {
                Cur_Row = Row + 1;
                if (Null_Row(Row)) return null;
                var T = DR.GetFieldType(Row);
                if (T == typeof(Decimal)) return (double)DR.GetDecimal(Row);
                if (T == typeof(System.Int64)) return (double)DR.GetInt64(Row);
                if (T == typeof(System.Int32)) return (long)DR.GetInt32(Row);
                if (T == typeof(System.Int16)) return (double)DR.GetInt16(Row);
                if (T == typeof(System.Single)) return (double)DR.GetFloat(Row);
                return DR.GetDouble(Row);
            }

            public decimal Next_Decimal
            {
                get { return Get_Decimal(Cur_Row); }
            }

            public decimal? Next_Decimal_Null
            {
                get { return Get_Decimal_Null(Cur_Row); }
            }

            public decimal Get_Decimal(string Row)
            {
                return Get_Decimal(Get_Row(Row));
            }

            public decimal Get_Decimal(int Row)
            {
                Cur_Row = Row + 1;
                if (Null_Row(Row)) return 0;
                var T = DR.GetFieldType(Row);
                if (T == typeof(Double)) return (decimal)DR.GetDouble(Row);
                if (T == typeof(System.Int64)) return (decimal)DR.GetInt64(Row);
                if (T == typeof(System.Int32)) return (long)DR.GetInt32(Row);
                if (T == typeof(System.Int16)) return (decimal)DR.GetInt16(Row);
                if (T == typeof(System.Single)) return (decimal)DR.GetFloat(Row);
                return DR.GetDecimal(Row);
            }

            public decimal? Get_Decimal_Null(int Row)
            {
                Cur_Row = Row + 1;
                if (Null_Row(Row)) return null;
                var T = DR.GetFieldType(Row);
                if (T == typeof(Double)) return (decimal)DR.GetDouble(Row);
                if (T == typeof(System.Int64)) return (decimal)DR.GetInt64(Row);
                if (T == typeof(System.Int32)) return (long)DR.GetInt32(Row);
                if (T == typeof(System.Int16)) return (decimal)DR.GetInt16(Row);
                if (T == typeof(System.Single)) return (decimal)DR.GetFloat(Row);
                return DR.GetDecimal(Row);
            }


            public DateTime Get_Date(string Row)
            {
                return Get_Date(Get_Row(Row));
            }
            public DateTime Get_Date(int Row)
            {
                Cur_Row = Row + 1;
                return Null_Row(Row) ? DateTime.MinValue : DR.GetDateTime(Row);
            }
            public DateTime Next_Date
            {
                get { return Get_Date(Cur_Row); }
            }

            public DateTime? Get_Date_Null(int Row)
            {
                Cur_Row = Row + 1;
                return Null_Row(Row) ? null : (DateTime?)DR.GetDateTime(Row);
            }
            public DateTime? Next_Date_Null
            {
                get { return Get_Date_Null(Cur_Row); }
            }

            public string XML_Debug_Info
            {
                get
                {
                    StringBuilder S = new StringBuilder("<row>");
                    while (this.Read(true))
                    {
                        for (int I = 0; I < DR.FieldCount; I++)
                        {
                            S.AppendFormat("<{0}>{1}</{0}>", DR.GetName(I), DR.IsDBNull(I) ? "null" : DR.GetValue(I).ToString());
                        }
                        break;
                    }
                    S.Append("</row>");
                    return S.ToString();
                }
            }

            public string HTML_Debug_Info
            {
                get
                {
                    StringBuilder S = new StringBuilder("<TABLE>");
                    for (int I = 0; I < DR.FieldCount; I++)
                    {
                        S.AppendFormat("<TR class='row{2}'><TD class='title'>{0}</TD><TD class='value'>{1}</TD></TR>", DR.GetName(I), DR.IsDBNull(I) ? "<i>null<i>" : DR.GetValue(I).ToString(), I % 2);
                    }
                    S.Append("</TABLE><br/>");
                    return S.ToString();
                }
            }

            public string Full_HTML_Debug_Info
            {
                get
                {
                    StringBuilder S = new StringBuilder();
                    const string Header =
@"<html><head><style type='text/css'>
body,table,td,tr {
    font-family: Tahoma;
	font-size: 8.25pt;
}
td{
   vertical-align:top;
}
.row1 {
	background-color:#FFFFFF;
}
.row0 {
	background-color:#EEEEFF;
}
.title{
   font-weight:bold;
}
.value{
   padding-left:5px;
}
</style></head><body>";
                    S.AppendLine(Header);
                    while (this.Read(true))
                    {
                        S.Append(this.HTML_Debug_Info);
                    }
                    S.AppendLine("</body></html>");
                    return S.ToString();
                }
            }

            public string Last_Error
            {
                get { return _Last_Error; }
            }

            #region IDisposable Membres

            public void Dispose()
            {
                Terminate();
            }

            #endregion
        }

        public static string Like_String(string Text, bool Exact_First = false, bool Exact_Last = false)
        {
            if (Text == null) return "";
            if (Text == "") return "";
            StringBuilder SB = new StringBuilder();
            if (!Exact_First) SB.Append('%');
            SB.Append(JR.Strings.To_Letters(Text.Trim(), true).Replace("e", "[e����]").Replace("i", "[i����]").Replace("a", "[a������]").Replace("o", "[o������]").Replace("u", "[u����]").Replace("y", "[�y]").Replace("n", "[n�]").Replace("c", "[c�]"));
            if (!Exact_Last) SB.Append('%');
            return SB.ToString();
        }

        public static bool Append_Full_Text_Search(StringBuilder Where_Clause, IList<string> Fields, string Expression)
        {
            if (Fields.Count == 0) return false;
            string Exp = Expression.Trim().Replace('?', '_').Replace('*', '%');
            if (Exp.Length == 0) return false;
            var Where_String = new StringBuilder("(");
            foreach (string W in Fields)
            {
                if (Where_String.Length > 1) Where_String.Append(" or ");
                if (W.StartsWith("="))
                {
                    Where_String.Append(W.Substring(1));
                    Where_String.Append(" like {1}");
                }
                else
                {
                    Where_String.Append(W);
                    Where_String.Append(" like {0}");
                }
            }
            Where_String.Append(")");

            StringBuilder SB = new StringBuilder();
            string[] Texts = Exp.Split(' ');
            bool Mode_Sauf = false;
            foreach (string T in Texts)
            {
                string TT = T.Trim();
                if (TT.Length == 0) continue;
                if (TT == "<>")
                {
                    Mode_Sauf = true;
                    continue;
                }
                if (SB.Length != 0) SB.Append(" and");
                if (Mode_Sauf) SB.Append(" not");
                SB.Append(string.Format(Where_String.ToString(), To_Arg(Db.Like_String(TT)), To_Arg(TT)));
            }
            Append_Where_Clause(Where_Clause, SB.ToString());
            return true;
        }


        public static bool Append_Full_Text_Search(StringBuilder Where_Clause, IList<string> Fields, string Expression, IList<object> Out_Params)
        {
            if (Fields.Count == 0) return false;
            string Exp = Expression.Trim().Replace('?', '_').Replace('*', '%');
            if (Exp.Length == 0) return false;
            string Where_String = "";
            foreach (string W in Fields)
            {
                if (Where_String != "") Where_String += " or";
                Where_String += $" {W} like @A{{0}}";
            }
            Where_String = "(" + Where_String + ")";

            StringBuilder SB = new StringBuilder();
            int I = Out_Params.Count;
            string[] Texts = Exp.Split(' ');
            bool Mode_Sauf = false;
            foreach (string T in Texts)
            {
                string TT = T.Trim();
                if (TT.Length == 0) continue;
                if (TT == "<>")
                {
                    Mode_Sauf = true;
                    continue;
                }
                Out_Params.Add(Db.Like_String(TT));
                if (SB.Length != 0) SB.Append(" and");
                if (Mode_Sauf) SB.Append(" not");
                SB.Append(string.Format(Where_String, I));
                I++;
            }
            Append_Where_Clause(Where_Clause, SB.ToString());
            return true;
        }

        public static void Append_Where_Clause(StringBuilder Where_Clause, params string[] Clauses)
        {
            Append_Where_Clause(Where_Clause, true, Clauses);
        }

        // Clause De la forme toto in (Range)
        // Ne pas ajouter si Range est vide
        public static void Append_Range_Clause(StringBuilder Where_Clause, string Clause, string Range)
        {
            if (Range.Trim() == "") return;
            Append_Where_Clause(Where_Clause, string.Format(Clause, Range));
        }

        // Clause est de la forme toto in 
        public static void Append_Range_Clause(StringBuilder Where_Clause, string Field, IList<string> Range, bool String_Range = false)
        {
            if (Range.Count == 0) return;
            StringBuilder SB = new StringBuilder();
            SB.AppendFormat("{0} in (", Field);
            bool First = true;
            foreach (string S in Range)
            {
                if (First) First = false; else SB.Append(',');
                SB.Append(String_Range ? To_Arg(S) : S);
            }
            SB.Append(")");
            Append_Where_Clause(Where_Clause, true, SB.ToString());
        }

        public static void Append_Where_Clause(StringBuilder Where_Clause, bool And, params string[] Clauses)
        {
            foreach (string Clause in Clauses)
            {
                if (Clause.Length == 0) continue;
                if (Where_Clause.Length > 0) Where_Clause.Append(And ? " and" : " or");
                Where_Clause.Append("(");
                Where_Clause.Append(Clause);
                Where_Clause.Append(")");
            }
        }

        // Construit une requete select
        // Exemple Select ("* from factures", "order by date", "top 100 where date=@0 order by code");
        public static string Select(string Table, string Default_Order_By, string Where)
        {
            string Wh = Where.ToUpper();
            int Idx_Where = Wh.IndexOf("WHERE");
            int Idx_Order = Wh.IndexOf("ORDER BY");
            string Top_Clause = Idx_Where > 0 ? Where.Substring(0, Idx_Where) : "";
            string Where_Clause = Idx_Where >= 0 ? Where.Substring(Idx_Where) : "";
            string Order_Clause = Idx_Order > 0 ? "" : Default_Order_By;
            return "select " + Top_Clause + " " + Table + " " + Where_Clause + " " + Order_Clause;
        }

        // Construit une clause CONTAINS par analyse de l'expression
        // G�re les parenth�ses, les or,ou,and,et,not,sauf
        // <> signale le d�but d'une s�rie de not
        // "" signale une expression exacte
        // Par d�faut les pr�fixes sont g�r�s en ajoutant une �toile � la fin du mot

        public static string Full_Text_Clause(string Expression, bool Remove_Ponctuation = false)
        {
            StringBuilder SB = new StringBuilder();
            JR.Strings.Cursor C = new Strings.Cursor(Expression.Trim());
            string Default_Operator = "&";
            string Next_Operator = "";

            void Force_Operator(string Exp)
            {
                if (SB.Length == 0) return; // Jamais d'op�rateur en premier
                Next_Operator = Exp;
                Default_Operator = "&";
            };

            void Force_Permanent_Operator(string Exp)
            {
                if (SB.Length == 0) return; // Jamais d'op�rateur en premier
                Next_Operator = Exp;
                Default_Operator = Exp;
            };

            void Push_Term(string Exp)
            {
                if (Exp.Length == 0) return;
                if (!Remove_Ponctuation)
                {
                    switch (Exp.ToLower())
                    {
                        case "or":
                        case "ou":
                            Force_Operator("|");
                            return;
                        case "and":
                        case "et":
                            Force_Operator("&");
                            return;
                        case "sauf":
                        case "not":
                            Force_Operator("&!");
                            return;
                        case "<>":
                            Force_Permanent_Operator("&!");
                            return;
                    }
                }
                SB.Append(Next_Operator);

                // Fin * : entourer de guillemnts
                if (Exp.EndsWith("*"))
                {
                    SB.AppendFormat("\"{0}\"", Exp);
                }
                // Guillemets, laisser tel quel
                else if (Exp.StartsWith("\""))
                {
                    SB.Append(Exp);
                }
                else if (Exp.Length > 1)
                {
                    SB.AppendFormat("FORMSOF (INFLECTIONAL,{0})", Exp);
                }
                else
                {
                    SB.Append(Exp); // Une seule lettre
                    //return;
                }
                Next_Operator = Default_Operator;
            };

            while (!C.End)
            {
                if (Remove_Ponctuation)
                {
                    string Key = C.Next_Term();
                    switch (C.Separator)
                    {
                        case '*':
                            Push_Term(Key + '*');
                            break;
                        case '|':
                        case '&':
                            Push_Term(Key);
                            Force_Operator(C.Separator.ToString());
                            break;
                        default:
                            Push_Term(Key);
                            break;
                    }
                }
                else
                {
                    string Key = C.Next(" ()\"-+");

                    switch (C.Separator)
                    {
                        // Quote : AJouter jusqu'a prochaine quote 
                        case '\"':
                            Push_Term('"' + C.Next("\"") + '"');
                            continue;
                        // Nouveau mot ou fin de l'expression
                        case '-':
                        case '+':
                        case ' ':
                        case '$':
                            Push_Term(Key);
                            continue;
                        case '(':
                            SB.Append(C.Separator);
                            Push_Term(Key);
                            continue;
                        case ')':
                            Push_Term(Key);
                            SB.Append(C.Separator);
                            break;
                    }
                }
            }
            return SB.ToString();
        }

        public Reader Exec_Reader(string Request, params object[] Params)
        {
            return new Dbase.Reader(Request, Params) { Config = Config };
        }
    }

    public class Db : Gen_Db<string>
    {
    }

    // Classes qui simplifient l'acces en lecture � la BD
    public class Db_Reader : Db.Reader
    {
        public Db_Reader(string Request, params object[] Params)
            : base(Request, Params)
        {
            Config = Db.Default_Config;
        }
        public Db_Reader(DataTable DT)
            : base(DT)
        {
        }
        public Db_Reader(DataRow DR)
            : base(DR)
        {
        }
    }


    public class DTransaction : IDisposable
    {
        DbConnection DB;
        DbTransaction TR;
        Db_Config Config;
        Dbase Base;
        Db_Config.Tracker T;

        void Terminate()
        {
            if (TR != null) TR.Dispose();
            if (DB != null) DB.Close();
            if (T != null) T.End_Track("End transaction");
        }

        public DTransaction(Dbase Base)
        {
            this.Base = Base;
            Config = Base.Config;
            DB = Config.New_Connection;
            DB.Open();
            TR = Config.New_Transaction(DB);
            T = Config.New_Tracker();
            if (T != null) T.Track("Begin transaction");
        }

        public decimal Insert_Request(string Request, params object[] Params)
        {
            object Result = DBNull.Value;
            try
            {
                if (T != null) T.Track(Request, Params);
                using (DbCommand Com = Dbase.Build_Command(Config, Request, DB, Params))
                {
                    Com.Transaction = TR;
                    if (Com.ExecuteNonQuery() > 0)
                    {
                        Com.Dispose();
                        using (DbCommand Com2 = Config.New_Command(Config.MySql_Mode ? "select last_insert_id()" : "select @@identity", DB))
                        {
                            Com2.Transaction = TR;
                            Result = Com2.ExecuteScalar();
                        }
                    };
                }
            }
            catch (Exception E)
            {
                Config.Trace_Exception(E, Request, Params);
            }
            if (Result == DBNull.Value)
            {
                return 0;
            }
            return System.Convert.ToDecimal(Result);
        }

        public bool Exec(string Request, params object[] Params)
        {
            if (Config.No_Write) return true;
            bool Success = false;
            try
            {
                if (T != null) T.Track(Request, Params);
                using (DbCommand Com = Dbase.Build_Command(Config, Request, DB, Params))
                {
                    Com.Transaction = TR;
                    int Count = Com.ExecuteNonQuery();
                    Success = true;
                }
            }
            catch (Exception E)
            {
                Config.Trace_Exception(E, Request, Params);
            }
            return Success;
        }

        public bool Commit()
        {
            try
            {
                if (T != null) T.Track("Begin Commit");
                TR.Commit();
                if (T != null) T.End_Track("End Commit");
                return true;
            }
            catch (Exception E)
            {
                Config.Trace_Exception(E, "commit");
                Config.Trace("Commit error");
                return false;
            }
        }

        public bool RollBack()
        {
            try
            {
                if (T != null) T.Track("Begin Rollback");
                TR.Rollback();
                if (T != null) T.End_Track("End RollBack");
                return true;
            }
            catch (Exception E)
            {
                Config.Trace_Exception(E, "rollback");
                Config.Trace("RollBack error");
                return false;
            }
        }

        public void Dispose()
        {
            Terminate();
        }
    }


}