using System.Data.Common;
using System.Collections.Generic;

namespace JR.DB
{
	/// <summary>
	/// Description r�sum�e de Session.
	/// </summary>
	public class SQL_Db
	{
		string _Connection;			// Chaine de connexion
        public bool MySql_Mode { get; internal set; }
        public bool MSSql_Mode { get; internal set; }


        public I_Sql_Provider Provider;
        public string Connection_String
        {
            get { return _Connection; }
            set 
            {
                _Connection = value;
                foreach (var P in Providers)
                {
                    if (!P.Provides(value, out string Mode)) continue;
                    Provider = P;
                    MySql_Mode = Mode == "MYSQL";
                    MSSql_Mode = Mode == "MSSQL";
                    break;
                }
            }
        }

        public DbConnection New_Connection
        {
            get
            {
                if (Provider == null)
                {
                    JR.Saf.Logs.Log("!! null provider for : " + Connection_String);
                    return null;
                }
                return Provider.New_Connection (Connection_String);
            }
        }

        public DbCommand New_Command(string Request, DbConnection Connection)
        {
            var Command = Connection.CreateCommand();
            Command.CommandText = Request;
            return Command;
        }

        public DbTransaction New_Transaction(DbConnection Connection)
        {
            return Connection.BeginTransaction();
        }

        public DbDataAdapter New_Adapter(string Request, DbConnection Connection)
        {
            return Provider.New_Adapter (Request, Connection);
        }

        public static void Register (I_Sql_Provider Provider)
        {
            Providers.Add(Provider);
        }
        public static List<I_Sql_Provider> Providers = new List<I_Sql_Provider>();
        //class OleDb_Prov : I_Sql_Provider
        //{
        //    public DbDataAdapter New_Adapter(string Request, DbConnection Connection)
        //    {
        //        return new System.Data.OleDb.OleDbDataAdapter(Request, Connection as System.Data.OleDb.OleDbConnection);
        //    }
        //    public DbConnection New_Connection(string Connection_String)
        //    {
        //        return new System.Data.OleDb.OleDbConnection(Connection_String);
        //    }
        //}
    }

    public interface I_Sql_Provider
    {
        DbDataAdapter New_Adapter(string Request, DbConnection Connection);
        DbConnection New_Connection(string Connection_String);
        bool Provides (string Connection_String, out string Mode);
    }
}
