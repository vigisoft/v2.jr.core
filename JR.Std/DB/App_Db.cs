using System;
using System.Data;
using System.Collections.Generic;
using System.Text;

namespace JR.DB
{

    public class App_Db:Gen_Db<int>
    {
    }

    public class App_Db_Reader : Db.Reader
    {
        public App_Db_Reader(string Request, params object[] Params):base(Request, Params)
        {
            Config = App_Db.Default_Config;
        }
    }
}