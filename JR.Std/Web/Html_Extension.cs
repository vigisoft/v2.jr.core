﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Text;


namespace JR
{
    public static class Html_Extension
    {
        public static string To_Url(this string Text)
        {
            return HttpUtility.UrlEncode(Text);
        }
        public static string To_Html(this string Text)
        {
            return HttpUtility.HtmlEncode(Text);
        }
        public static string From_Url(this string Text)
        {
            return HttpUtility.UrlDecode(Text);
        }
        public static string From_Html(this string Text)
        {
            return HttpUtility.HtmlDecode(Text);
        }
        public static string To_Html_Auto(this string Text)
        {
            // Si contient du html retourner sans modif
            int Tag = Text.IndexOf('<');
            if (Tag >= 0 && Text.IndexOf('>') > Tag) return Text;

            // Sinon, splitter par ligne et si plus d'une ligne entourer de <P>
            string[] Lines = Text.Replace("\r", "").Split('\n');
            if (Lines.Length < 2) return HttpUtility.HtmlEncode(Text);
            StringBuilder SB = new StringBuilder();
            foreach (string P in Lines)
            {
                //SB.Append("<p>");
                SB.Append(HttpUtility.HtmlEncode(P));
                //SB.Append("</p>");
                SB.Append("<br/>");
            }
            return SB.ToString();
        }


        /// <summary>
        /// Converts HTML to plain text.
        /// </summary>
        class HtmlToText
        {
            // Static data tables
            protected static Dictionary<string, string> _tags;
            protected static HashSet<string> _ignoreTags;

            // Instance variables
            protected TextBuilder _text;
            protected string _html;
            protected int _pos;

            // Static constructor (one time only)
            static HtmlToText()
            {
                _tags = new Dictionary<string, string>();
                _tags.Add("address", "\n");
                _tags.Add("blockquote", "\n");
                _tags.Add("div", "\n");
                _tags.Add("dl", "\n");
                _tags.Add("fieldset", "\n");
                _tags.Add("form", "\n");
                _tags.Add("h1", "\n");
                _tags.Add("/h1", "\n");
                _tags.Add("h2", "\n");
                _tags.Add("/h2", "\n");
                _tags.Add("h3", "\n");
                _tags.Add("/h3", "\n");
                _tags.Add("h4", "\n");
                _tags.Add("/h4", "\n");
                _tags.Add("h5", "\n");
                _tags.Add("/h5", "\n");
                _tags.Add("h6", "\n");
                _tags.Add("/h6", "\n");
                _tags.Add("p", "\n");
                _tags.Add("/p", "\n");
                _tags.Add("table", "\n");
                _tags.Add("/table", "\n");
                _tags.Add("ul", "\n");
                _tags.Add("/ul", "\n");
                _tags.Add("ol", "\n");
                _tags.Add("/ol", "\n");
                _tags.Add("/li", "\n");
                _tags.Add("br", "\n");
                _tags.Add("/td", "\t");
                _tags.Add("/tr", "\n");
                _tags.Add("/pre", "\n");

                _ignoreTags = new HashSet<string>();
                _ignoreTags.Add("script");
                _ignoreTags.Add("noscript");
                _ignoreTags.Add("style");
                _ignoreTags.Add("object");
            }

            /// <summary>
            /// Converts the given HTML to plain text and returns the result.
            /// </summary>
            /// <param name="html">HTML to be converted</param>
            /// <returns>Resulting plain text</returns>
            public string Convert(string html, bool To_Html = false)
            {
                // Initialize state variables
                _text = new TextBuilder();
                _html = html;
                _pos = 0;

                // Process input
                while (!EndOfText)
                {
                    if (Peek() == '<')
                    {
                        // HTML tag
                        bool selfClosing;
                        string tag = ParseTag(out selfClosing);

                        // Handle special tag cases
                        if (tag == "body")
                        {
                            // Discard content before <body>
                            _text.Clear();
                        }
                        else if (tag == "/body")
                        {
                            // Discard content after </body>
                            _pos = _html.Length;
                        }
                        else if (tag == "pre")
                        {
                            // Enter preformatted mode
                            _text.Preformatted = true;
                            EatWhitespaceToNextLine();
                        }
                        else if (tag == "/pre")
                        {
                            // Exit preformatted mode
                            _text.Preformatted = false;
                        }

                        if (_tags.TryGetValue(tag, out string value))
                        {
                            _text.Write(To_Html ? "<br/>":value);
                        }

                        if (_ignoreTags.Contains(tag))
                            EatInnerContent(tag);
                    }
                    else if (Char.IsWhiteSpace(Peek()))
                    {
                        // Whitespace (treat all as space)
                        _text.Write(_text.Preformatted ? Peek() : ' ');
                        MoveAhead();
                    }
                    else
                    {
                        // Other text
                        _text.Write(Peek());
                        MoveAhead();
                    }
                }
                // Return result
                if (To_Html) return _text.ToString();
                return HttpUtility.HtmlDecode(_text.ToString());
            }

            // Eats all characters that are part of the current tag
            // and returns information about that tag
            protected string ParseTag(out bool selfClosing)
            {
                string tag = String.Empty;
                selfClosing = false;

                if (Peek() == '<')
                {
                    MoveAhead();

                    // Parse tag name
                    EatWhitespace();
                    int start = _pos;
                    if (Peek() == '/')
                        MoveAhead();
                    while (!EndOfText && !Char.IsWhiteSpace(Peek()) &&
                        Peek() != '/' && Peek() != '>')
                        MoveAhead();
                    tag = _html.Substring(start, _pos - start).ToLower();

                    // Parse rest of tag
                    while (!EndOfText && Peek() != '>')
                    {
                        if (Peek() == '"' || Peek() == '\'')
                            EatQuotedValue();
                        else
                        {
                            if (Peek() == '/')
                                selfClosing = true;
                            MoveAhead();
                        }
                    }
                    MoveAhead();
                }
                return tag;
            }

            // Consumes inner content from the current tag
            protected void EatInnerContent(string tag)
            {
                string endTag = "/" + tag;

                while (!EndOfText)
                {
                    if (Peek() == '<')
                    {
                        // Consume a tag
                        bool selfClosing;
                        if (ParseTag(out selfClosing) == endTag)
                            return;
                        // Use recursion to consume nested tags
                        if (!selfClosing && !tag.StartsWith("/"))
                            EatInnerContent(tag);
                    }
                    else MoveAhead();
                }
            }

            // Returns true if the current position is at the end of
            // the string
            protected bool EndOfText
            {
                get { return (_pos >= _html.Length); }
            }

            // Safely returns the character at the current position
            protected char Peek()
            {
                return (_pos < _html.Length) ? _html[_pos] : (char)0;
            }

            // Safely advances to current position to the next character
            protected void MoveAhead()
            {
                _pos = Math.Min(_pos + 1, _html.Length);
            }

            // Moves the current position to the next non-whitespace
            // character.
            protected void EatWhitespace()
            {
                while (Char.IsWhiteSpace(Peek()))
                    MoveAhead();
            }

            // Moves the current position to the next non-whitespace
            // character or the start of the next line, whichever
            // comes first
            protected void EatWhitespaceToNextLine()
            {
                while (Char.IsWhiteSpace(Peek()))
                {
                    char c = Peek();
                    MoveAhead();
                    if (c == '\n')
                        break;
                }
            }

            // Moves the current position past a quoted value
            protected void EatQuotedValue()
            {
                char c = Peek();
                if (c == '"' || c == '\'')
                {
                    // Opening quote
                    MoveAhead();
                    // Find end of value
                    int start = _pos;
                    _pos = _html.IndexOfAny(new char[] { c, '\r', '\n' }, _pos);
                    if (_pos < 0)
                        _pos = _html.Length;
                    else
                        MoveAhead();    // Closing quote
                }
            }

            /// <summary>
            /// A StringBuilder class that helps eliminate excess whitespace.
            /// </summary>
            protected class TextBuilder
            {
                private StringBuilder _text;
                private StringBuilder _currLine;
                private int _emptyLines;
                private bool _preformatted;

                // Construction
                public TextBuilder()
                {
                    _text = new StringBuilder();
                    _currLine = new StringBuilder();
                    _emptyLines = 0;
                    _preformatted = false;
                }

                /// <summary>
                /// Normally, extra whitespace characters are discarded.
                /// If this property is set to true, they are passed
                /// through unchanged.
                /// </summary>
                public bool Preformatted
                {
                    get
                    {
                        return _preformatted;
                    }
                    set
                    {
                        if (value)
                        {
                            // Clear line buffer if changing to
                            // preformatted mode
                            if (_currLine.Length > 0)
                                FlushCurrLine();
                            _emptyLines = 0;
                        }
                        _preformatted = value;
                    }
                }

                /// <summary>
                /// Clears all current text.
                /// </summary>
                public void Clear()
                {
                    _text.Length = 0;
                    _currLine.Length = 0;
                    _emptyLines = 0;
                }

                /// <summary>
                /// Writes the given string to the output buffer.
                /// </summary>
                /// <param name="s"></param>
                public void Write(string s)
                {
                    foreach (char c in s)
                        Write(c);
                }

                /// <summary>
                /// Writes the given character to the output buffer.
                /// </summary>
                /// <param name="c">Character to write</param>
                public void Write(char c)
                {
                    if (_preformatted)
                    {
                        // Write preformatted character
                        _text.Append(c);
                    }
                    else
                    {
                        if (c == '\r')
                        {
                            // Ignore carriage returns. We'll process
                            // '\n' if it comes next
                        }
                        else if (c == '\n')
                        {
                            // Flush current line
                            FlushCurrLine();
                        }
                        else if (Char.IsWhiteSpace(c))
                        {
                            // Write single space character
                            int len = _currLine.Length;
                            if (len == 0 || !Char.IsWhiteSpace(_currLine[len - 1]))
                                _currLine.Append(' ');
                        }
                        else
                        {
                            // Add character to current line
                            _currLine.Append(c);
                        }
                    }
                }

                // Appends the current line to output buffer
                protected void FlushCurrLine()
                {
                    // Get current line
                    string line = _currLine.ToString().Trim();

                    // Determine if line contains non-space characters
                    string tmp = line.Replace("&nbsp;", String.Empty);
                    if (tmp.Length == 0)
                    {
                        // An empty line
                        _emptyLines++;
                        if (_emptyLines < 2 && _text.Length > 0)
                            _text.AppendLine(line);
                    }
                    else
                    {
                        // A non-empty line
                        _emptyLines = 0;
                        _text.AppendLine(line);
                    }

                    // Reset current line
                    _currLine.Length = 0;
                }

                /// <summary>
                /// Returns the current output as a string.
                /// </summary>
                public override string ToString()
                {
                    if (_currLine.Length > 0)
                        FlushCurrLine();
                    return _text.ToString();
                }
            }
        }

        public static bool Is_Html(this string Text)
        {
            if (Text.Length < 3) return false;
            var I_Open = Text.IndexOf('<', 0, 3);
            if (I_Open < 0) return false;
            if (Text.LastIndexOf('>', Text.Length - 3) < I_Open) return false;
            return true;
        }

        public static string To_Readable_Text(this string Text, bool To_Html = false)
        {
            if (!Text.Is_Html()) return Text;
            var C = new HtmlToText();
            return C.Convert(Text, To_Html);
        }


        // Retourne le texte converti en HTML sauf si c'en est deja, limité au premier paragraphe
        public static string To_Html_Auto_Header(this string Text)
        {
            // Si contient du html retourner sans modif
            int Tag = Text.IndexOf('<');
            if (Tag >= 0 && Text.IndexOf('>') > Tag)
            {
                var C = new JR.Strings.Cursor(Text);
                return C.Next_Word("<br/>");
            }

            Tag = Text.IndexOf("\r\n");
            if (Tag < 0) return HttpUtility.HtmlEncode(Text);
            return HttpUtility.HtmlEncode(Text.Substring(0, Tag));
        }

        // Retourne le texte en minuscules et dans lequel les séparateurs sont remplacés par -
        // La taille est limitées à 128 caractères
        public static string To_Url_Segment(this string Text)
        {
            var SB = new StringBuilder();
            foreach (var S in Text.Normalizes_Words())
            {
                if (SB.Length > 128) break;
                if (SB.Length > 0) SB.Append('-');
                SB.Append(S);
            }
            return SB.ToString();
        }

        // Chemin physique en url
        public static string Path_To_Url(this string Path)
        {
            return Path.Replace("\\", "/");
        }

        public static string Doc_Domain_Url(this string Absolute_Doc, bool Include_Slash = true)
        {
            var C = Absolute_Doc.Scan();
            C.Next("/");
            C.Next("/");
            C.Next("/");
            if (!C.Found) return "";
            return C.Range(0, C.Found_Index - (Include_Slash ? 0 : 1));
        }

        public static string Doc_Base_Url(this string Absolute_Doc, bool Include_Slash = true)
        {
            var Index = Absolute_Doc.LastIndexOf("/");
            if (Index >= 0)
            {
                return Absolute_Doc.Substring(0, Index + (Include_Slash ? 1 : 0));
            }
            return "";
        }


        public static string Combine_Url(this string Absolute_Doc, string Url)
        {
            // Si l'URL contient : ou commence par // c'est déjà une URL absolue
            if (Url.Contains(":") || Url.StartsWith("//")) return Url;

            // Si l'URL commence par /, il faut repartir de la racine du site, qui est le troisième /
            // ex : http:14362542//toto.com/ ou //tutu.com
            if (Url.StartsWith("/"))
            {
                return Absolute_Doc.Doc_Domain_Url(false) + Url;
            }

            // Chemin relatif au document
            // Détecter le dernier /
            return Absolute_Doc.Doc_Base_Url(true) + Url;
        }


        static Dictionary<string, string> Readable_Equiv = null;
        public static string To_Readable_Html(this string Src)
        {
            if (Readable_Equiv == null)
            {
                var Equiv = new Dictionary<string, string>();
                void Add(string C, string S) => Equiv[S] = C;
                Add("'", "#039");
                Add("\x0022", "quot");
                Add("\x0026", "amp");
                //Add("\x003c","lt");
                //Add("\x003e","gt");
                Add("\x00a0", "nbsp");
                Add("\x00a1", "iexcl");
                Add("\x00a2", "cent");
                Add("\x00a3", "pound");
                Add("\x00a4", "curren");
                Add("\x00a5", "yen");
                Add("\x00a6", "brvbar");
                Add("\x00a7", "sect");
                Add("\x00a8", "uml");
                Add("\x00a9", "copy");
                Add("\x00aa", "ordf");
                Add("\x00ab", "laquo");
                Add("\x00ac", "not");
                Add("\x00ad", "shy");
                Add("\x00ae", "reg");
                Add("\x00af", "macr");
                Add("\x00b0", "deg");
                Add("\x00b1", "plusmn");
                Add("\x00b2", "sup2");
                Add("\x00b3", "sup3");
                Add("\x00b4", "acute");
                Add("\x00b5", "micro");
                Add("\x00b6", "para");
                Add("\x00b7", "middot");
                Add("\x00b8", "cedil");
                Add("\x00b9", "sup1");
                Add("\x00ba", "ordm");
                Add("\x00bb", "raquo");
                Add("\x00bc", "frac14");
                Add("\x00bd", "frac12");
                Add("\x00be", "frac34");
                Add("\x00bf", "iquest");
                Add("\x00c0", "Agrave");
                Add("\x00c1", "Aacute");
                Add("\x00c2", "Acirc");
                Add("\x00c3", "Atilde");
                Add("\x00c4", "Auml");
                Add("\x00c5", "Aring");
                Add("\x00c6", "AElig");
                Add("\x00c7", "Ccedil");
                Add("\x00c8", "Egrave");
                Add("\x00c9", "Eacute");
                Add("\x00ca", "Ecirc");
                Add("\x00cb", "Euml");
                Add("\x00cc", "Igrave");
                Add("\x00cd", "Iacute");
                Add("\x00ce", "Icirc");
                Add("\x00cf", "Iuml");
                Add("\x00d0", "ETH");
                Add("\x00d1", "Ntilde");
                Add("\x00d2", "Ograve");
                Add("\x00d3", "Oacute");
                Add("\x00d4", "Ocirc");
                Add("\x00d5", "Otilde");
                Add("\x00d6", "Ouml");
                Add("\x00d7", "times");
                Add("\x00d8", "Oslash");
                Add("\x00d9", "Ugrave");
                Add("\x00da", "Uacute");
                Add("\x00db", "Ucirc");
                Add("\x00dc", "Uuml");
                Add("\x00dd", "Yacute");
                Add("\x00de", "THORN");
                Add("\x00df", "szlig");
                Add("\x00e0", "agrave");
                Add("\x00e1", "aacute");
                Add("\x00e2", "acirc");
                Add("\x00e3", "atilde");
                Add("\x00e4", "auml");
                Add("\x00e5", "aring");
                Add("\x00e6", "aelig");
                Add("\x00e7", "ccedil");
                Add("\x00e8", "egrave");
                Add("\x00e9", "eacute");
                Add("\x00ea", "ecirc");
                Add("\x00eb", "euml");
                Add("\x00ec", "igrave");
                Add("\x00ed", "iacute");
                Add("\x00ee", "icirc");
                Add("\x00ef", "iuml");
                Add("\x00f0", "eth");
                Add("\x00f1", "ntilde");
                Add("\x00f2", "ograve");
                Add("\x00f3", "oacute");
                Add("\x00f4", "ocirc");
                Add("\x00f5", "otilde");
                Add("\x00f6", "ouml");
                Add("\x00f7", "divide");
                Add("\x00f8", "oslash");
                Add("\x00f9", "ugrave");
                Add("\x00fa", "uacute");
                Add("\x00fb", "ucirc");
                Add("\x00fc", "uuml");
                Add("\x00fd", "yacute");
                Add("\x00fe", "thorn");
                Add("\x00ff", "yuml");
                Add("\x0152", "OElig");
                Add("\x0153", "oelig");
                Add("\x0160", "Scaron");
                Add("\x0161", "scaron");
                Add("\x0178", "Yuml");
                Add("\x0192", "fnof");
                Add("\x02c6", "circ");
                Add("\x02dc", "tilde");
                Add("\x0391", "Alpha");
                Add("\x0392", "Beta");
                Add("\x0393", "Gamma");
                Add("\x0394", "Delta");
                Add("\x0395", "Epsilon");
                Add("\x0396", "Zeta");
                Add("\x0397", "Eta");
                Add("\x0398", "Theta");
                Add("\x0399", "Iota");
                Add("\x039a", "Kappa");
                Add("\x039b", "Lambda");
                Add("\x039c", "Mu");
                Add("\x039d", "Nu");
                Add("\x039e", "Xi");
                Add("\x039f", "Omicron");
                Add("\x03a0", "Pi");
                Add("\x03a1", "Rho");
                Add("\x03a3", "Sigma");
                Add("\x03a4", "Tau");
                Add("\x03a5", "Upsilon");
                Add("\x03a6", "Phi");
                Add("\x03a7", "Chi");
                Add("\x03a8", "Psi");
                Add("\x03a9", "Omega");
                Add("\x03b1", "alpha");
                Add("\x03b2", "beta");
                Add("\x03b3", "gamma");
                Add("\x03b4", "delta");
                Add("\x03b5", "epsilon");
                Add("\x03b6", "zeta");
                Add("\x03b7", "eta");
                Add("\x03b8", "theta");
                Add("\x03b9", "iota");
                Add("\x03ba", "kappa");
                Add("\x03bb", "lambda");
                Add("\x03bc", "mu");
                Add("\x03bd", "nu");
                Add("\x03be", "xi");
                Add("\x03bf", "omicron");
                Add("\x03c0", "pi");
                Add("\x03c1", "rho");
                Add("\x03c2", "sigmaf");
                Add("\x03c3", "sigma");
                Add("\x03c4", "tau");
                Add("\x03c5", "upsilon");
                Add("\x03c6", "phi");
                Add("\x03c7", "chi");
                Add("\x03c8", "psi");
                Add("\x03c9", "omega");
                Add("\x03d1", "thetasym");
                Add("\x03d2", "upsih");
                Add("\x03d6", "piv");
                Add("\x2002", "ensp");
                Add("\x2003", "emsp");
                Add("\x2009", "thinsp");
                Add("\x200c", "zwnj");
                Add("\x200d", "zwj");
                Add("\x200e", "lrm");
                Add("\x200f", "rlm");
                Add("\x2013", "ndash");
                Add("\x2014", "mdash");
                Add("\x2018", "lsquo");
                Add("\x2019", "rsquo");
                Add("\x201a", "sbquo");
                Add("\x201c", "ldquo");
                Add("\x201d", "rdquo");
                Add("\x201e", "bdquo");
                Add("\x2020", "dagger");
                Add("\x2021", "Dagger");
                Add("\x2022", "bull");
                Add("\x2026", "hellip");
                Add("\x2030", "permil");
                Add("\x2032", "prime");
                Add("\x2033", "Prime");
                Add("\x2039", "lsaquo");
                Add("\x203a", "rsaquo");
                Add("\x203e", "oline");
                Add("\x2044", "frasl");
                Add("\x20ac", "euro");
                Add("\x2111", "image");
                Add("\x2118", "weierp");
                Add("\x211c", "real");
                Add("\x2122", "trade");
                Add("\x2135", "alefsym");
                Add("\x2190", "larr");
                Add("\x2191", "uarr");
                Add("\x2192", "rarr");
                Add("\x2193", "darr");
                Add("\x2194", "harr");
                Add("\x21b5", "crarr");
                Add("\x21d0", "lArr");
                Add("\x21d1", "uArr");
                Add("\x21d2", "rArr");
                Add("\x21d3", "dArr");
                Add("\x21d4", "hArr");
                Add("\x2200", "forall");
                Add("\x2202", "part");
                Add("\x2203", "exist");
                Add("\x2205", "empty");
                Add("\x2207", "nabla");
                Add("\x2208", "isin");
                Add("\x2209", "notin");
                Add("\x220b", "ni");
                Add("\x220f", "prod");
                Add("\x2211", "sum");
                Add("\x2212", "minus");
                Add("\x2217", "lowast");
                Add("\x221a", "radic");
                Add("\x221d", "prop");
                Add("\x221e", "infin");
                Add("\x2220", "ang");
                Add("\x2227", "and");
                Add("\x2228", "or");
                Add("\x2229", "cap");
                Add("\x222a", "cup");
                Add("\x222b", "int");
                Add("\x2234", "there4");
                Add("\x223c", "sim");
                Add("\x2245", "cong");
                Add("\x2248", "asymp");
                Add("\x2260", "ne");
                Add("\x2261", "equiv");
                Add("\x2264", "le");
                Add("\x2265", "ge");
                Add("\x2282", "sub");
                Add("\x2283", "sup");
                Add("\x2284", "nsub");
                Add("\x2286", "sube");
                Add("\x2287", "supe");
                Add("\x2295", "oplus");
                Add("\x2297", "otimes");
                Add("\x22a5", "perp");
                Add("\x22c5", "sdot");
                Add("\x2308", "lceil");
                Add("\x2309", "rceil");
                Add("\x230a", "lfloor");
                Add("\x230b", "rfloor");
                Add("\x2329", "lang");
                Add("\x232a", "rang");
                Add("\x25ca", "loz");
                Add("\x2660", "spades");
                Add("\x2663", "clubs");
                Add("\x2665", "hearts");
                Add("\x2666", "diams");
                Readable_Equiv = Equiv;
            }
            string Solve(string Key)
            {
                if (Readable_Equiv.TryGetValue(Key, out string V)) return V;
                return Key;
            }

            return Src.Generic_Text(Solve, '&', ';');

        }

    }
}
