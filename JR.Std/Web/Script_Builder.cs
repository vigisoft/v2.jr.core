﻿using System;
using System.Text;
using System.Collections.Generic;
using JR;
namespace JR.Web
{
    public class Script_Builder
    {
        public StringBuilder SB = new StringBuilder();

        public override string ToString() => SB.ToString();
        public void Exec(string Code)
        {
            SB.Append(Code);
            if (!Code.EndsWith(";")) SB.AppendLine(";");
        }

        public string Code(bool Include_Tag)
        {
            if (SB.Length == 0) return "";
            var C = Include_Tag ? "<script>" + SB.ToString() + "</script>" : SB.ToString();
            SB.Clear();
            return C;
        }

        string JS(object V)
        {
            if (V is string) return "'" + (V as string).To_Js().Replace("\n", @"\n") + "'";
            if (V is bool) return V.ToString().ToLower();
            return V.ToString();
        }

        public void Call(string Function, params object[] Args)
        {
            SB.Append(Function);
            SB.Append("(");
            int Count = 0;
            foreach (var A in Args)
            {
                if (Count > 0) SB.Append(",");
                SB.Append(JS(A));
                Count++;
            }
            SB.Append(")");
            SB.AppendLine(";");
        }

        public void Navigate_To(string URL) => SB.AppendLine($"window.location.href = '{URL}';");

        string Selector (string C)
        {
            if (C.Length < 3) return C;
            switch (C[0])
            {
                case '.':
                case '#':
                    return $"$('{C}')";
            }
            return $"$('#{C}')";
        }
        public void Set_Visible(string C, bool Visible) => SB.AppendLine($"{Selector(C)}.{(Visible ? "remove" : "add")}Class('hidden');");

        public void Set_Class(string C, string Class, bool On) => SB.AppendLine($"{Selector(C)}.{(On ? "add" : "remove")}Class('{Class}');");

        public void Set_Enabled(string C, bool Enabled) => SB.AppendLine($"{Selector(C)}.prop('disabled', {(Enabled ? "false" : "true")});");
        public void Set_Text(string C, string Text) => SB.AppendLine($"{Selector(C)}.text({JS(Text)});");
        public void Set_Value(string C, object Value)
        {
            if (Value == null)
            {
                SB.AppendLine($"{Selector(C)}.empty();");
            }
            else
            {
                SB.AppendLine($"{Selector(C)}.val({JS(Value)});");
            }
        }

        public void Reload() => SB.AppendLine($"window.location.reload();");

        public void Show_Modal(string Element) => SB.AppendLine($"{Selector(Element)}.modal('show');");
        public void Hide_Modal(string Element) => SB.AppendLine($"{Selector(Element)}.modal('hide');");
        public void Toastr_Ok(string Reason, int Time_Out = 10) => SB.AppendLine($"toastr.success({JS(Reason)},'',{{ preventDuplicates: true, timeOut: {Time_Out}000 }});");
        public void Toastr_Warning(string Reason, int Time_Out = 10) => SB.AppendLine($"toastr.warning({JS(Reason)},'',{{ preventDuplicates: true, timeOut: {Time_Out}000 }});");
        public void Toastr_Error(string Reason, int Time_Out = 10) => SB.AppendLine($"toastr.error({JS(Reason)},'',{{ preventDuplicates: true, timeOut: {Time_Out}000 }});");
        public void Force_Download(string Url) => SB.AppendLine($"window.open({JS(Url)}, '_self');");
    }

}

