﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace JR.Web
{
    public class Html_Parser
    {

        // L'action reçoit le nom du tag (sans prefixe), la chaine attributs et la chaine contenu
        // Recherche sur un seul niveau
        public static void Parse_Prefix(string Source, string Prefix, Action<string, string, string> Treat_Tag, Action<string> Treat_Inner_Text)
        {
            string Deb = "<" + Prefix;
            Strings.Cursor C = new Strings.Cursor(Source);
            bool Inner_Treated = false;
            while (!C.End)
            {
                var Inner = C.Next_Word(Deb);
                if (!Inner_Treated)
                {
                    Inner_Treated = true;
                    if (Inner.Match("[a-zA-Z]", true)) Treat_Inner_Text(Inner);
                }
                if (!C.Found) return;
                string Tag = C.Next(" />");
                if (!C.Found) return;
                string Attribs = "";
                switch (C.Separator)
                {
                    case ' ':
                        Attribs = C.Next(">").Trim();
                        if (Attribs.EndsWith("/"))
                        {
                            Attribs = Attribs.Substring(0, Attribs.Length - 1).Trim();
                            Treat_Tag(Tag, Attribs, "");
                            continue;
                        }
                        break;
                    case '/':
                        C.Next(">");
                        Treat_Tag(Tag, "", "");
                        continue;
                    case '>':
                        break;
                    default:
                        continue;
                }
                string Content = C.Next_Word("</" + Prefix + Tag + ">");
                if (!C.Found) return;
                Treat_Tag(Tag, Attribs, Content);
            }
        }

        // L'action reçoit la chaine attributs et la chaine contenu
        // Recherche très précisément un tag particulier avec marque de début et de fin
        public static void Parse_Tags(string Source, string Tag_Name, Action<string, string> Treat_Tag)
        {
            string Deb = "<" + Tag_Name;
            string Fin = "</" + Tag_Name + ">";
            Strings.Cursor C = new Strings.Cursor(Source);
            while (!C.End)
            {
                C.Next_Word(Deb);
                if (!C.Found) return;
                string Attribs = "";
                switch (C.Next_Char())
                {
                    case ' ':
                        Attribs = C.Next(">").Trim();
                        if (Attribs.EndsWith("/"))
                        {
                            Attribs = Attribs.Substring(0, Attribs.Length - 1).Trim();
                            Treat_Tag(Attribs, "");
                            continue;
                        }
                        break;
                    case '/':
                        C.Next(">");
                        Treat_Tag("", "");
                        continue;
                    case '>':
                        break;
                    default:
                        continue;
                }
                string Content = C.Next_Word(Fin);
                if (!C.Found) return;
                Treat_Tag(Attribs, Content);
            }
        }

        // L'action reçoit le nom de l'attribut et sa valeur
        public static Dictionary<string, string> Parse_Attributes (string Source)
        {
            Dictionary<string, string> Dic = new Dictionary<string, string>();
            Strings.Cursor C = new Strings.Cursor(Source);
            while (!C.End)
            {
                string Name = C.Next("=").Trim();
                if (!C.Found) break;
                string Value = "";
                if (C.Next_Char() != '"')
                {
                    C.Skip(-1);
                    Value = C.Next(" ");
                }
                else
                {
                    Value = C.Next("\"");
                }
                Dic[Name] = Value;
            }
            return Dic;
        }

        public static string Extract_Title(string Doc)
        {
            JR.Strings.Cursor C = new JR.Strings.Cursor(Doc);
            C.Next_Word("title>");
            return C.Next_Word("<").Trim();
        }
        public static string Extract_Text(string Doc, string Tag)
        {
            JR.Strings.Cursor C = new JR.Strings.Cursor(Doc);
            C.Next_Word("<" + Tag); C.Next_Word (">");
            return C.Next_Word("<").Trim();
        }

        public static string Extract_Option(string Doc, string Option_Name)
        {
            // <!--import_options=nocid-->
            var C = Doc.Scan();
            C.Next_Word("<!--" + Option_Name + "=");
            return C.Next_Word("-->").Trim();
        }

        public static IList<string> Extract_Src(string Doc)
        {
            List<string> L = new List<string>();
            JR.Strings.Cursor C = new JR.Strings.Cursor(Doc);
            while (!C.End)
            {
                C.Next_Word("src=\"");
                L.Add(C.Next("\"").Trim());
            }
            return L;
        }

    }
}
