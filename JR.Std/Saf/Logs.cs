using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;

namespace JR.Saf
{
    /// <summary>
    /// Production de traces dans les fichiers de log
    /// </summary>
    public class Logs
    {

        static string _Log_File = "";
        public static void Set_Default_Log(string File)
        {
            _Log_File = File;
        }

        static object Lock = new object();

        static bool Is_Init = false;

        static string Trace(string File, string Message, params object[] Params)
        {
            lock (Lock)
            {
                if (!Is_Init)
                {
                    void Init()
                    {
                        if (_Log_File == "") return;
                        string Dir_Path = JR.Files.Get_Directory_Name(_Log_File);
                        if (Dir_Path == "" || !Directory.Exists(Dir_Path)) return;
                        if (Purge_Max != 0)
                        {
                            if (Reorg_Days != 0)
                            {
                                var Dirs = Directory.GetDirectories(Dir_Path, "????_??").OrderByDescending(D => D).ToArray();
                                for (int I = Purge_Max; I < Dirs.Length; I++)
                                {
                                    JR.Files.Safe_Remove_Dir(Dirs[I]);
                                }
                            }
                            else
                            {
                                var DI = new DirectoryInfo(Dir_Path);
                                var Files = DI.GetFiles("*" + _Log_File.Get_Extension()).OrderByDescending(F => F.LastWriteTime).ToArray();
                                for (int I = Purge_Max; I < Files.Length; I++)
                                {
                                    JR.Files.Safe_Delete(Files[I].FullName);
                                }
                            }
                        }
                        if (Reorg_Days != 0)
                        {
                            var Limit = DateTime.Now.Date.AddDays(-Reorg_Days);
                            var DI = new DirectoryInfo(Dir_Path);
                            foreach (var F in DI.GetFiles("*" + _Log_File.Get_Extension()).Where(F => F.LastWriteTime < Limit))
                            {
                                JR.Files.Safe_Move(F.FullName, Path.Combine(DI.FullName,F.LastWriteTime.ToString("yyyy_MM"),F.Name));
                            }
                        }
                    }
                    try
                    {
                        Init();
                    }
                    catch { }
                    Is_Init = true;
                }

                string Mess = DateTime.Now.ToString("HH:mm:ss") + " : " + ((Params.Length == 0) ? Message : string.Format(Message, Params));
                if (!Quiet_Mode) Console.WriteLine(Mess);
                if (File != "")
                {
                    // Premier essai
                    try
                    {
                        System.IO.File.AppendAllText(File, Mess + "\r\n");

                    }
                    catch (DirectoryNotFoundException E)
                    {
                        // Deuxieme essai
                        // Creer le r�pertoire de log si il n'existe pas
                        try
                        {
                            string Dir_Path = JR.Files.Get_Directory_Name(File);
                            if (Dir_Path != "" && !Directory.Exists(Dir_Path)) Directory.CreateDirectory(Dir_Path);
                            System.IO.File.AppendAllText(File, Mess + "\r\n");
                        }
                        catch { }
                    }
                    catch { }
                }
                return Mess;
            }
        }

        // G�n�ration de traces dans un fichier index� sur le jour
        public static string Log(string Message, params object[] Params)
        {
            string Log_File = string.Format(_Log_File, DateTime.Now, "Log");
            return Trace(Log_File, Message, Params);
        }

        // G�n�ration d'erreurs dans un fichier index� sur le jour
        public static string Err(string Message, params object[] Params)
        {
            string Err_File = string.Format(_Log_File, DateTime.Now, "Err");
            return Trace(Err_File, Message, Params);
        }

        // Si prefix == "" ==> a l'�cran
        public static string File_Log(string Prefix, string Message, params object[] Params)
        {
            string Log_File = "";
            if (Prefix != "")
            {
                Log_File = string.Format(_Log_File, DateTime.Now, Prefix);
            }
            return Trace(Log_File, Message, Params);
        }


        // G�n�ration d'erreurs dans un fichier index� sur le jour
        public static void Except(Exception e)
        {
            Err($"{e.Message}\r\n{e.Source}\r\n{e.StackTrace}");
            //foreach (DictionaryEntry D in e.Data)
            //{
            //    Err("\t{0}={2}", D.Key, D.Value);
            //}
        }

        // Pas de trace sur la console
        public static bool Quiet_Mode { get; set; } = false;
        public static int Level { get; set; } = 1;

        // Publier toutes les Buffer secondes
        static int Buffer = 0;

        // Reorganiser les logs trop vieux
        static int Reorg_Days = 0;

        // Purger les logs ou les r�pertoires trop nombreux
        static int Purge_Max  = 0;

        public static void Set_Config(int Buff, int Reorg, int Purge)
        {
            Buffer = Buff;
            Reorg_Days = Reorg;
            Purge_Max = Purge;
            Is_Init = false;
        }
    }
}
