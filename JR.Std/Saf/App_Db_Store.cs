using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using JR.DB;
using System.Xml.Serialization;

namespace JR.Saf
{
    public static class App_Db_Store<T> where T : class
    {
        const string Req_Load = "select [Id] as K, Value as V, [Date] as D from [{0}] where [Id] like @1";
        const string Req_Get = "select Value as V from [{0}] where [Id] = @1";
        const string Req_Del = "delete from [{0}] where [Id] like @1";
        const string Req_Add = "delete from [{0}] where [Id]=@1; insert into [{0}] values (@1, @2, getutcdate())";
        const string Req_List = "select [Id] as K from [{0}]";

        public class Entry
        {
            public string Key_Name = "";
            public DateTime Date;
            public T State = null;
        }

        public static string [] List_Items(string Module_Name)
        {
            List<string> L = new List<string>();
            using (App_Db_Reader DR = new App_Db_Reader(Req_List, Module_Name))
            {
                while (DR.Read())
                {
                    L.Add(DR.Get_String(0));
                }
            }
            return L.ToArray();
        }

        public static List<Entry> Load_Items(string Module_Name, string Key_Pattern)
        {
            List<Entry> L = new List<Entry>();
            using (App_Db_Reader DR = new App_Db_Reader(Req_Load, Module_Name, Key_Pattern.Replace('*', '%')))
            {
                while (DR.Read())
                {
                    Entry E = new Entry();
                    E.Key_Name = DR.Get_String(0);
                    string Xml = DR.Get_String(1);
                    E.State = (Xml == "") ? default(T) : JR.Serial<T>.From_Xml(Xml);
                    E.Date = DR.Get_Date(2);
                    L.Add(E);
                }
            }
            return L;
        }

        public static T Load_Item(string Module_Name, string Key_Name) 
        {
            // Recup�rer les donnees sous forme de chaine
            string Xml = App_Db.Execute_String (Req_Get, Module_Name, Key_Name);
            if (Xml == "") return default (T);
            return JR.Serial<T>.From_Xml(Xml);
        }

        public static bool Save_Item(string Module_Name, string Key_Name, T State)
        {
            if (State == null)
            {
                App_Db.Execute(Req_Del, Module_Name, Key_Name);
                return true;
            }
            string Value = JR.Serial<T>.To_Xml(State);
            return App_Db.Execute(Req_Add, Module_Name, Key_Name, Value);
        }

        public static void Remove_Items(string Module_Name, string Key_Pattern)
        {
            App_Db.Execute (Req_Del, Module_Name, Key_Pattern.Replace('*','%'));
        }

    }
}
