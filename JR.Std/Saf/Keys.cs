using System;
using System.Collections.Generic;
using System.Text;

namespace JR.Saf
{
    public class Keys
    {
        // Les cl�s ont un status : ' ' = absente, '*' = pr�sente, 'H' = cach�e, 'R' = readonly
        // Deux jeux de cl�s sont fournis :
        // Les cl�s exceptionnelles qui sont cherch�es en premier
        // Les cl�s standards sans caract�res g�n�riques qui viennent ensuite modifier la premi�re recherche

        Dictionary<string, char> _Keys = new Dictionary<string, char>();
        List<string> _Keys_Exceptions = new List<string>();
        public void Append_Key(string Key, char Status)
        {
            string K = JR.Strings.Official(Key);
            if (K.Length == 0) return;
            switch (K[0])
            {
                case '-':
                case '+':
                    {
                        bool Found = false;
                        for (int I = 0; I < _Keys_Exceptions.Count; I++)
                        {
                            if (_Keys_Exceptions[I].StartsWith(K))
                            {
                                _Keys_Exceptions[I] = K + ":" + Status;
                                Found = true;
                                break;
                            }
                        }
                        if (!Found) _Keys_Exceptions.Add(K + ":" + Status);
                    }
                    break;
                default:
                    _Keys[K] = Status;
                    break;
            }
        }

        public void Init_Keys()
        {
            _Keys.Clear();
            _Keys_Exceptions.Clear();
        }

        public void Remove_Key(string Key)
        {
            string K = JR.Strings.Official(Key);
            if (K.Length == 0) return;
            switch (K[0])
            {
                case '-':
                case '+':
                    for (int I = 0; I < _Keys_Exceptions.Count; I++)
                    {
                        if (_Keys_Exceptions[I].StartsWith(K))
                        {
                            _Keys_Exceptions.RemoveAt(I);
                            break;
                        }
                    }
                    break;
                default:
                    _Keys.Remove(K);
                    break;
            }
        }

        // Cherche si il existe une cl� avec le status demand�
        // si la cl� avec le status '*' existe on consid�re que c'est ok
        // On traite en premier les exceptions
        // Une cle demand�e peut �tre de la forme CLE1+CLE2+CLE3 (et des 3 cl�s)
        // ou bien CLE1|CLE2|CLE3 (ou des trois cles)
        // ou bien CLE1+CLE2|CLE3+CLE4 (on fait d'abord les et puis le ou)
        // Auquel cas les trois cl�s sont test�es
        bool Has_Single_Key(string K, char Status)
        {
            char Stat = ' ';
            foreach (string Ex in _Keys_Exceptions)
            {
                if (Ex.StartsWith(K))
                {
                    Stat = Ex[Ex.Length - 1];
                    break;
                }
            }
            if (Stat != ' ') return (Status == Stat);
            if (!_Keys.ContainsKey(K)) return false;
            Stat = _Keys[K];
            if (Stat == '*') return true;
            return (Stat == Status);
        }

        public bool Has_Key(string Key, char Status)
        {
            string K = JR.Strings.Official(Key);
            string[] Ors = K.Split('|');
            bool Or_Result = false;
            foreach (string Or in Ors)
            {
                string[] Ands = Or.Split('+');
                bool And_Result = true;
                foreach (string And in Ands)
                {
                    And_Result = And_Result && Has_Single_Key(And, Status);
                }
                Or_Result = Or_Result || And_Result;
            }
            return Or_Result;
        }

    }
}
