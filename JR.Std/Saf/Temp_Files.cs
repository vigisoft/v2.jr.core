using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
//using System.IO.IsolatedStorage;

namespace JR.Saf
{
    public static class Temp_Files
    {
        static string _Dir_Name = "";
        static void Check_Dir()
        {
            if (_Dir_Name != "") return;
            _Dir_Name = Main.Real_File_Name ("Temp", Main.Local_Space);
            if (Directory.Exists (_Dir_Name)) return;
            Directory.CreateDirectory (_Dir_Name);
        }

        static string Full_Name(string Key)
        {
            return _Dir_Name + "\\" + Key;
        }

        static void Purge_Old_Files()
        {
            Check_Dir();
            foreach (string F in Directory.GetFiles (_Dir_Name))
            {
                FileInfo FI = new FileInfo (F);
                if (FI.LastWriteTimeUtc < DateTime.UtcNow.AddDays (-2)) FI.Delete();
            }
        }

        static int Temp_Count = 0;
        public static string Get_Full_Name(string File_Name)
        {
            Purge_Old_Files();
            Temp_Count++;
            return Full_Name(string.Format (File_Name,Temp_Count));
        }
    }
}
