﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;


namespace JR.Saf
{

    public class Lang
    {
        public string Key;
        public string LKey;
        public string Name;
        public int Id;
        public CultureInfo Culture;
        public string ISO_Lang;
        public string ISO_Code;
        public static List<Lang> List = new List<Lang>();
        public static Lang FR = new Lang("FR", "Français", "fr-FR");
        public static Lang EN = new Lang("EN", "English", "en-US");
        public static Lang ES = new Lang("ES", "Español", "es-ES");
        public static Lang CN = new Lang("CN", "简体中文", "zh-CN");
        public static Lang DE = new Lang("DE", "Deutch", "de-DE");
        public static Lang RU = new Lang("RU", "Русский", "ru-RU");
        public static Lang AR = new Lang("AR", "العراق", "ar-EG");
        public Lang(string K, string N, string C)
        {
            Key = K;
            LKey = K.ToLower();
            ISO_Code = C;
            Culture = CultureInfo.GetCultureInfo(C);
            ISO_Lang = Culture.TwoLetterISOLanguageName;
            Name = N;
        }

        public static Lang By_Key(string Key)
        {
            switch (Key.Trim().ToUpper())
            {
                case "F":
                case "FR": return FR;
                case "E":
                case "EN": return EN;
                case "S":
                case "ES": return ES;
                case "C":
                case "CN": return CN;
                case "D":
                case "DE": return DE;
                case "R":
                case "RU": return RU;
                case "AR": return AR;
            }
            return EN;
        }
        public static void Init()
        {
            foreach (var L in Main.Language_List)
            {
                var La = By_Key(L);
                La.Id = List.Count;
                List.Add(La);
            }
        }
    }

    public class Ref_Parameter
    {
        string K = "";
        string V = null;
        bool Solved = false;

        public Ref_Parameter(string Key, string Default = "")
        {
            K = Key;
            V = Default;
        }
        public string Value
        {
            get
            {
                if (!Solved)
                {
                    if (Main.Try_Get_String(out string VV, "P", K)) V = VV;
                    Solved = true;
                }
                return V;
            }
        }
    }

    public class Main_Base<T>
    {

        public static string Host_Name = System.Environment.MachineName;
        public static string Software_Release;
        //public static string Vendor_Name;
        public static string Root_Path;
        public static string App_Name = "APP";
        public static string Full_Version;
        public static string Vendor;
        public static bool Is_Web_Site = false;


        // Espaces de nommage
        public const string System_Space = "#SYSTEM";
        public const string Local_Space = "#LOCAL";
        public const string Log_Space = "#LOG";
        public const string Temp_Space = "#TEMP";
        public const string Root_Space = "#ROOT";
        public const string App_Space = "#APP";
        public const string First_Space = "#1";

        static Dictionary<string, string> Spaces = new Dictionary<string, string>();
        public static List<string> Space_Names = new List<string>();

        public static void Add_Space(string Name, string File, bool First = false)
        {
            string Key_Name = JR.Strings.Official(Name);
            if (Spaces.ContainsKey(Key_Name))
            {
                Spaces[Key_Name] = File;
                return;
            }
            Spaces.Add(Key_Name, File);
            if (Spaces.Count > 1)
            {
                if (First)
                {
                    Space_Names.Insert(0, Key_Name);
                }
                else
                {
                    Space_Names.Add(Key_Name);
                }
            }
        }

        public static string Space_Path(string Space_Name)
        {
            Spaces.TryGetValue(JR.Strings.Official(Space_Name), out var Value);
            return Value;
        }

        // Gestion d'un dictionnaire
        public static Generic_Dictionnary Dico = new Generic_Dictionnary(true, "P.");

        public static string Try_Get_String(params string[] Keys) => Dico.Try_Get_String(Keys);

        public static bool Try_Get_String(out string Value, params string[] Keys) => Dico.Try_Get_String(out Value, Keys);

        // Les chaines applicatives sont stockées 
        public static string Get_String(params string[] Keys) => Dico.Get_String(Keys);

        // Insère une chaine dans les dicos
        // La chaine est dans un premier temps marquée comme non traduite
        //   par deux ~~
        public static void Set_String(string Value, params string[] Keys) => Dico.Set_String(Value, Keys);


        // Lecture d'un parametre
        // Les paramètres commencent par "P." dans les dictionnaires
        public static string Parameter(string Key)
        {
            int Start = Key.IndexOf('[');
            if (Start >= 0)
            {
                int End = Key.IndexOf(']', Start + 1);
                if (End > Start)
                {
                    string V = Parameter(Key.Substring(0, Start) + Key.Substring(End + 1));
                    if (V != "") return V;
                    return Parameter(Key.Remove(End, 1).Remove(Start, 1));
                }
            }
            string Value = Try_Get_String("P", Key);
            if (Value.StartsWith("<[]"))    // Indique une UNIVERSAL STRING a translater une seule fois
            {
                Value = To_Universal_String(Value.Substring(3));
                Set_String(Value, "P", Key);
            }
            return Value;
        }

        public static string Generic_Parameter(string Key, params string[] Params) => Strings.Generic_Text(Parameter(Key), Params);

        // Lecture d'un parametre de type fichier
        public static string File_Parameter(string Key) => To_Real_File_Name(Parameter(Key));

        public static string Generic_File_Parameter(string Key, params string[] Params) => Strings.Generic_Text(File_Parameter(Key), Params);


        public static bool Bool_Parameter(string Key) => Convert.To_Boolean(Parameter(Key));

        public static int Int_Parameter(string Key) => Convert.To_Integer(Parameter(Key));

        public static double Float_Parameter(string Key) => Convert.To_Float(Parameter(Key));

        // Check parameter presence
        public static bool Parameter_Exist(string Key) => Parameter(Key) != "";

        // Write parameter value
        public static void Write_Parameter(string Key, string Value) => Set_String(Value, "P", Key);

        public static string Real_File_Name(string File) => To_Real_File_Name(File);

        public static P JSon_Parameter<P>(string Key) => Serial<P>.From_Json(Parameter(Key));

        public static Func<string, string> To_Real_File_Name = (Value) => Value;
        public static Func<string, string> To_Universal_String = (Value) => Value;

        public static IList<string> Language_List = new string[] { "FR", "EN" };

        public static string Env_Mode = null;
        public static bool Prod_Mode = false;
        public static bool Dev_Mode = false;
        public static bool Test_Mode = false;

        public static void Set_Env_Mode(string Mode)
        {
            Env_Mode = Mode.Official();
            Prod_Mode = Env_Mode == "PROD";
            Dev_Mode = Env_Mode == "DEV";
            Test_Mode = Env_Mode == "TEST";
        }

    }

    /// <summary>
    /// Version allégée et générique du Framework pour fonctionnement standalone 
    /// Supprime des notions incompatibles, comme la langue
    ///  ou le nom d'application unique
    /// </summary>
    public class Main : Main_Base<Main>
    {

        //public static bool Check_Private_Storage = false;
        public static event EventHandler Before_Load_Config;
        public static event EventHandler After_Load_Config;

        // Confirmation de chargement
        public static Func<string, bool> Confirm_Load_Config = null;


        public static string Stamp { get; set; }

        // Charge les espaces et les chemins
        // Charge les fichiers de configuration
        static bool Register(string Specifiq_Root)
        {
            To_Real_File_Name = Real_FileName;
            To_Universal_String = Universal_String;
            var Initial_Directory = System.IO.Directory.GetCurrentDirectory();
            Root_Path = (Specifiq_Root != "") ? Specifiq_Root : Initial_Directory;

            // Ne pas déplacer le répertoire de base pour une appli web
            var Web_Config = Path.Combine(Initial_Directory, "web.config");
            Is_Web_Site = File.Exists(Web_Config);
            if (!Is_Web_Site && Specifiq_Root != "")
            {
                System.IO.Directory.SetCurrentDirectory(Specifiq_Root);
                Web_Config = Path.Combine(Specifiq_Root, "web.config");
                Is_Web_Site = File.Exists(Web_Config);
            }

            // Generer un id unique basé sur la date de l'exe ou du web.config
            // pour forcer le rechargement de fichiers web par exemple (&V=xxxx)
            string App_Ref_File = Is_Web_Site ? Web_Config : JR.OS.Current_Program_File_Name;
            Stamp = new FileInfo(App_Ref_File).LastWriteTime.Stamp().ToLower();
            Write_Parameter("APP.STAMP", Stamp);

            // Ajout de paramètres système
            Write_Parameter("#HOME", Root_Path);
            Write_Parameter("#FOLDER", Path.GetFileName(Root_Path));
            Write_Parameter("#APP", App_Name);
            Write_Parameter("#HOST", System.Net.Dns.GetHostName());
            Write_Parameter("#MACHINE", System.Environment.MachineName);
            Write_Parameter("#APP_FOLDER", App_Folder);

            // Chargement de la liste des espaces
            Add_Space("#ROOT", Path.Combine(Root_Path, "{F}"));
            Add_Space("#SYSTEM", Path.Combine(Root_Path, "{F}"));
            Add_Space("#TEMP", Path.Combine(Path.GetTempPath(), "{F}"));
            Add_Space("#LOCAL", Path.Combine(Root_Path, "Local", "{F}"));
            Add_Space("#LOG", Path.Combine(Root_Path, "Log", "{F}"));

            var Lines = JR.Files.Read_File(Path.Combine(Root_Path, "..", "..", "spaces.ini")).ToList();
            Lines.AddRange(JR.Files.Read_File(Path.Combine(Root_Path, "..", "spaces.ini")));
            Lines.AddRange(JR.Files.Read_File(Path.Combine(Root_Path, "Config", "spaces.ini")));
            Lines.AddRange(JR.Files.Read_File(Path.Combine(Root_Path, "local", "spaces.ini")));
            foreach (string Line in Lines)
            {
                var C = Line.Trim().Scan();
                var New_Space = C.Next("=").Trim();
                if (New_Space.Length < 2) continue;
                if (!char.IsLetterOrDigit(New_Space[0])) continue;
                var Value = C.Tail().Trim();
                if (New_Space == "ENV_MODE")
                {
                    Set_Env_Mode(Value);
                    continue;
                }
                Add_Space("#" + New_Space, Dico.Translated(Value));
            }

            // Repasser local en tete, app en queue
            if (Space_Names.Remove("#LOCAL")) Space_Names.Add("#LOCAL");
            if (Space_Names.Remove("#APP")) Space_Names.Insert(0, "#APP");
            string Loc_Path = Real_File_Name("", "#LOCAL");
            Loc_Path = Loc_Path.Remove(Loc_Path.Length - 1);
            Write_Parameter("#LOCAL", Loc_Path);

            Logs.Set_Default_Log(Real_File_Name(App_Name + @"{0:yyMMdd}_{1}.log", Log_Space));

            if (Before_Load_Config != null) Before_Load_Config(null, EventArgs.Empty);
            Load_Configurations();
            if (After_Load_Config != null) After_Load_Config(null, EventArgs.Empty);
            if (Log_Config)
            {
                Saf.Logs.Log($"Detected local dir : {Loc_Path}");
                Saf.Logs.Log($"Detected env mode : {Env_Mode ?? "null"}");
            }
            if (Parameter_Exist("run_initial_command")) OS.Execute(File_Parameter("run_initial_command"));
            if (Parameter_Exist("Languages")) Language_List = Parameter("Languages").Split(',');
            Lang.Init();
            if (Parameter_Exist("Log_File")) Logs.Set_Default_Log(Parameter("Log_File"));
            if (Parameter_Exist("Log_Level")) Logs.Level = Int_Parameter("Log_Level");
            if (Parameter_Exist("Log_Quiet")) Logs.Quiet_Mode = Bool_Parameter("Log_Quiet");
            Logs.Set_Config(Int_Parameter("Log_Buffer"), Int_Parameter("Log_Reorg"), Int_Parameter("Log_Purge"));
            if (Parameter_Exist("DB_Connection"))
            {
                JR.DB.App_Db.Default_Config = new DB.Db_Config
                {
                    Connection_String = Parameter("DB_Connection"),
                    Audit_String = Parameter("DB_Audit"),
                    Trace_Logger = Logs.Log,
                    Error_Logger = Logs.Err,
                    No_Write = Bool_Parameter("Db_No_Write")
                };
                JR.DB.Db.Default_Config = new DB.Db_Config
                {
                    Connection_String = Parameter("DB_Connection"),
                    Audit_String = Parameter("DB_Audit"),
                    Trace_Logger = Logs.Log,
                    Error_Logger = Logs.Err,
                    No_Write = Bool_Parameter("Db_No_Write")
                };
            }
            if (Bool_Parameter("Use_DB_State")) Db_State<string>.Init();  // Initialisation de la table predefinie des etats
            if (Parameter_Exist("APP_DB_Connection")) JR.DB.App_Db.Default_Config.Connection_String = Parameter("App_DB_Connection");

            // 
            // CHargement des enumérations
            Enums.Load();
            return true;
        }

        // return success of registration
        public static bool Register_Software
           (string Software_Release,
            string Vendor_Name,
            string Specifiq_Root,
            string Trigram)
        {
            Full_Version = Software_Release;
            Vendor = Vendor_Name;
            App_Name = Trigram;
            return Register(Specifiq_Root);
        }


        private static void Load_Dictionaries(string Space)
        {
            Dico.Confirm_Load = Confirm_Load_Config;
            List<string> Files = new List<string>();
            List<string> Defered_Files = new List<string>();
            string Config_Dir = Real_File_Name("Config", Space);

            bool Defer_File(bool Defer, string File)
            {
                if (Defer) Defered_Files.Add(File);
                return false;
            }

            // Rejeter les fichiers des autres environnements (config_xxx)
            // Charger le fichier de l'environnement à la fin
            bool Is_Allowed(string File)
            {
                if (Env_Mode == null) return true;
                var File_Name = Path.GetFileNameWithoutExtension(File);
                if (File_Name.StartsWith("config_"))
                {
                    return Defer_File(File_Name == "config_" + Env_Mode.ToLower(), File);
                }
                return true;
            }

            if (Directory.Exists(Config_Dir))
            {
                void Add_Files(string Pattern) => Files.AddRange(System.IO.Directory.GetFiles(Config_Dir, Pattern).Select(F => F.ToLower()).Where(F => Is_Allowed(F)));
                Add_Files("*.txt");
                Add_Files("*.xml");
                Add_Files("*.xlsx");
                Add_Files("*.xls");
            }
            // Charger les fichiers par ordre alpha
            Files.Sort();
            Files.AddRange(Defered_Files);
            foreach (string File in Files)
            {
                Dico.Add_Source(File);
            }
        }

        public static string App_Folder =>
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData, Environment.SpecialFolderOption.DoNotVerify), Vendor, App_Name);

        // Indique si le chargement des fichiers de config doit être effectué
        public static bool Log_Config => Bool_Parameter("Log_Config");
        // Charge tous les dictionnaires de configuration
        static void Load_Configurations()
        {
            foreach (string Space in Space_Names)
            {
                Load_Dictionaries(Space);
            }
            Load_Db_Dictionnary();

            // Charger les dictionnaires complémentaires
            // Limitation : on ne peut pas chainer les dictionnaires complémentaires
            foreach (string Dic in Dico.Keys_Like("DIC.*"))
            {
                Dico.Add_Source(Real_File_Name(Try_Get_String(Dic)));
            }
        }

        // Chargement des données de config dans SQLServer
        static void Load_Db_Dictionnary()
        {
            if (!Parameter_Exist("DB_Config_Connection")) return;
            if (!Parameter_Exist("DB_Config_Request")) return;
            string Connection = Parameter("DB_Config_Connection");
            string Config_Request = Parameter("DB_Config_Request");
            JR.DB.SQL_Db Config = new DB.SQL_Db { Connection_String = Connection };
            try
            {
                using (var DB = Config.New_Connection)
                {
                    DB.Open();
                    using (DbCommand Cmd = Config.New_Command(Config_Request, DB))
                    {
                        using (DbDataReader SR = Cmd.ExecuteReader())
                        {
                            if (SR.HasRows)
                            {
                                while (SR.Read())
                                {
                                    Set_String(SR.GetString(1), SR.GetString(0).ToUpper());
                                }
                            }
                        }
                    }
                    DB.Close();
                }
            }
            catch (Exception e)
            {
                Logs.Err($"Error reading DB_Config : {Connection} / {Config_Request}");
                Logs.Except(e);
                return;
            };
        }

        public static string Stamp_Url(string URL, string Key = "")
        {
            var SB = new StringBuilder(URL);
            SB.Append(URL.Contains("?") ? "&" : "?");
            if (Key != "")
            {
                SB.Append(Key);
                SB.Append("=");
            }
            SB.Append(Stamp);
            return SB.ToString();
        }

        // Return file name associated with a module in a specifiq directory
        // The directory is taken from the space 
        static string Real_FileName(string File)
        {
            // Space au debut du nom
            string[] Files = File.Split(new char[] { ':' }, 2);
            if (Files.Length == 1)
            {
                if (File.Length < 2) return File;
                if (Is_Web_Site && char.IsLetterOrDigit(File[0])) return Root_Path + @"\" + File;
                return File;
            }
            return Real_File_Name(Files[1], Files[0].ToUpper());
        }

        public static string Real_File_Name(string File, string Logical_Space)
        {
            string Trans_File = File;
            if (Logical_Space == First_Space)
            {
                for (int I = Main.Space_Names.Count - 1; I >= 0; I--)
                {
                    string Real_File = Real_File_Name(Trans_File, Main.Space_Names[I]);
                    if (System.IO.File.Exists(Real_File))
                    {
                        return Real_File;
                    }
                }
                return Trans_File;
            }
            else if (Logical_Space.StartsWith("#"))
            {
                string Path = Space_Path(Logical_Space);
                if (Path != null)
                {
                    return Path.Replace("{F}", Trans_File);
                }
            }
            return Logical_Space + @":" + Trans_File;
            // Cas particulier des a:,c:
            //if (Logical_Space.Length < 3)
            //{
            //    return Logical_Space + @":" + Trans_File;
            //}
            //else
            //{
            //    return Logical_Space + @"\" + Trans_File;
            //}
        }

        // Chemin racine du site
        public static string Base_Path(string File_Name) => Path.Combine(Root_Path, File_Name);

        // Enregistrement d'un etat courant par serialization
        public static bool Save_State<T>(T Context)
        {
            Stream Stream = new FileStream(Real_File_Name(@"#LOCAL:State.xml"), System.IO.FileMode.Create);
            XmlSerializer S = new XmlSerializer(typeof(T));
            S.Serialize(Stream, Context);
            Stream.Close();
            return true;
        }

        // Lecture d'un etat courant par serialization
        public static T Load_State<T>() where T : new()
        {
            Stream Stream = new FileStream(Real_File_Name(@"#LOCAL:State.xml"), System.IO.FileMode.OpenOrCreate);
            T C;
            try
            {
                XmlSerializer S = new XmlSerializer(typeof(T));
                C = (T)S.Deserialize(Stream);
            }
            catch
            {
                C = new T();
            }
            finally
            {
                Stream.Close();
            };
            return C;
        }

        public delegate string Ressource_Solver(string Key, string Param);
        public static Ressource_Solver Solver = null;

        // Alternance de parametre / date
        static string If_Date(string[] Params)
        {
            string Param = "";
            bool In_Date = false;
            foreach (string P in Params)
            {
                if (In_Date)
                {
                    DateTime D = JR.Convert.To_Format_Date(P, "yyyyMMdd", "yyyyMMdd:HH:mm");
                    if (D > DateTime.MinValue && D < DateTime.Now) return Param;
                }
                else
                {
                    Param = P;
                }
                In_Date = !In_Date;
            }
            return Param;
        }

        // *Valeur à tester*resultat1*valeur1*resultat2*valeur2*...*resultat par défaut
        static string If_Cond(bool Match_Method, string[] Params)
        {

            // L'appel a universal_string se fait le moins possible
            string Param = "";
            string Comp = "";
            bool In_Comp = true;
            bool In_Test = true;
            foreach (string Par in Params)
            {
                if (In_Comp)
                {
                    Comp = Universal_String(Par);

                }
                else if (In_Test)
                {
                    if (Match_Method)
                    {
                        if (JR.Strings.Match(Comp, Universal_String(Par))) return Param;
                    }
                    else
                    {
                        if (JR.Strings.Like(Comp, Universal_String(Par))) return Param;
                    }
                }
                else
                {
                    Param = Par;
                }
                In_Test = !In_Test;
                In_Comp = false;
            }
            return Universal_String(Param);
        }

        // Lecture d'une cellule datatable
        static string Get_Cell(string[] Params)
        {
            if (Params.Length < 3) return "";
            string Col = Params[0];
            string Row = Params[1];
            DataTable DT = Universal_Table(Params[2]);
            if (DT.Rows.Count == 0) return "";
            if (DT.Columns.Count == 0) return "";
            int C = -1;
            if (JR.Strings.Is_Integer(Col))
            {
                C = JR.Convert.To_Integer(Col);
            }
            else
            {
                C = DT.Columns.IndexOf(Col);
                if (C == -1)
                {
                    for (int I = 0; I < DT.Columns.Count; I++)
                    {
                        if (JR.Strings.Like(Col, DT.Rows[0][I].ToString()))
                        {
                            C = I;
                            break;
                        }
                    }
                }
            }
            if (C < 0 || C >= DT.Columns.Count) return "";


            int R = -1;
            if (JR.Strings.Is_Integer(Row))
            {
                R = JR.Convert.To_Integer(Row);
            }
            else
            {
                for (int I = 0; I < DT.Rows.Count; I++)
                {
                    if (JR.Strings.Like(Row, DT.Rows[I][0].ToString()))
                    {
                        R = I;
                        break;
                    }
                }
            }
            if (R < 0 || R >= DT.Rows.Count) return "";

            return DT.Rows[R][C].ToString();
        }

        // Retourne le contenu d'un local file, si le fichier se termine par @XXX, on ne remonte que la partie [XXX], mis en cache
        // CLient permet de gérer le flag Is_New par module client
        static char[] UCF_Seps = { '@', '$' };
        public static string Universal_Cached_File(string Param, out bool Is_New, string Client = "")
        {
            Is_New = false;
            string Phy_File_Name = Real_File_Name(Param);
            if (Phy_File_Name == "") return "";
            string Content = Db_Cache<string>.Get_Cache(Phy_File_Name, "U_CLF" + Client);
            if (Content != null) return Content;

            string File_Name;
            string Split = "";
            int I_Split = Phy_File_Name.LastIndexOfAny(UCF_Seps);
            if (I_Split > 0)
            {
                File_Name = Phy_File_Name.Substring(0, I_Split);
                Split = Phy_File_Name.Substring(I_Split + 1);
            }
            else
            {
                File_Name = Phy_File_Name;
            }


            if (!File.Exists(File_Name)) return "";
            try
            {
                Content = File.ReadAllText(File_Name, System.Text.Encoding.Default);
                if (I_Split > 0)
                {
                    var C = Content.Scan();
                    Content = "";
                    while (!C.End)
                    {
                        C.Next_Word("[");
                        if (!C.Found) break;
                        string Key = C.Next_Word("]");
                        string Block = C.Next_Word("[/" + Key + "]");
                        if (Key == Split)
                        {
                            Content += Block;
                            break;
                        }
                        if (Key == "*")
                        {
                            Content += Block;
                        }
                    }
                }
            }
            catch { }
            Db_Cache<string>.Add_Cache(Phy_File_Name, "U_CLF" + Client, Content, 100 * 24); // expiration 100 jours
            Is_New = true;
            return Content;
        }

        public static string Universal_Ressource(string Key, string Param)
        {
            switch (JR.Strings.Official(Key))
            {
                case "GBC":  // Retourne le contenu du cache global
                    {
                        bool Is_New;
                        return Universal_Cached_File(Param, out Is_New);
                    }
                case "CLF":  // Retourne le contenu d'un local file, si le fichier se termine par @XXX, on ne remonte que la partie [XXX], mis en cache
                    {
                        bool Is_New;
                        return Universal_Cached_File(Param, out Is_New);
                    }


                case "SLF":  // Retourne le contenu d'un local file, si le fichier se termine par @XXX, on ne remonte que la partie [XXX]
                    {
                        string File_Name;
                        string Split = "";
                        int I_Split = Param.LastIndexOf('@');
                        if (I_Split > 0)
                        {
                            File_Name = Real_File_Name(Param.Substring(0, I_Split));
                            Split = Param.Substring(I_Split + 1);
                        }
                        else
                        {
                            File_Name = Real_File_Name(Param);
                        }
                        if (!File.Exists(File_Name)) return "";
                        try
                        {
                            string File_Content = File.ReadAllText(File_Name, System.Text.Encoding.Default);
                            if (I_Split > 0)
                            {
                                JR.Strings.Cursor C = new Strings.Cursor(File_Content);
                                C.Next_Word("[" + Split + "]");
                                return C.Next_Word("[/" + Split + "]");
                            }
                            else
                            {
                                return File_Content;
                            }
                        }
                        catch { }
                        return "";
                    }
                case "SPF":  // Parameter file
                    {
                        string File_Name = Real_File_Name(Parameter(Param));
                        if (!File.Exists(File_Name)) return "";
                        try
                        {
                            return File.ReadAllText(File_Name, System.Text.Encoding.Default);
                        }
                        catch { }
                        return "";
                    }
                case "SLP":  // Retourne le contenu d'un parametre local
                    {
                        return Parameter(Param);
                    }
                case "SDB":  // Retourne le contenu d'une execution base de données
                    {
                        return DB.Db.Execute_String(Param);
                    }
                case "SID":  // Retourne le contenu selon la date
                    {
                        return If_Date(JR.Strings.Split(Param));
                    }
                case "SIL":  // Retourne le contenu selon une condition like
                    {
                        return If_Cond(false, JR.Strings.Split(Param));
                    }
                case "SIM":  // Retourne le contenu selon une condition match
                    {
                        return If_Cond(true, JR.Strings.Split(Param));
                    }
                case "SDL":  // Retourne la date locale
                    {
                        return DateTime.Now.ToString(Param);
                    }
                case "SDU":  // Retourne la date UTC
                    {
                        return DateTime.UtcNow.ToString(Param);
                    }
                case "STAMP":  // Retourne le stamp courant
                    {
                        return Stamp;
                    }
                case "CEL":  // Retourne le contenu d'une cellule
                    {
                        return Get_Cell(JR.Strings.Split(Param));
                    }
                case "FILENAME":  // Retourne la nom du fichier
                    {
                        return JR.Files.Get_File_Name(Param);
                    }
                case "DIRNAME":  // Retourne le chemin du fichier
                    {
                        return JR.Files.Get_Directory_Name(Param);
                    }
                case "EXTENSION":  // Retourne le chemin du fichier
                    {
                        return JR.Files.Get_Extension(Param);
                    }
                case "SYS_RESTART":  // Redémarre l'application
                    {
                        Stop_Application();
                        return "Done";
                    }
            }
            if (Solver != null)
            {
                foreach (Ressource_Solver Solv in Solver.GetInvocationList())
                {
                    string Solved = Solv(Key, Param);
                    if (Solved != null) return Solved;
                }
            }
            return null;
        }

        // Interprète la chaine et localise et remplace les indicateurs de UR de la forme [Key:Param]
        // Les [] dans les parametres sont decomptés pour accepter l'imbrication
        public static string Universal_String(string Source)
        {
            if (Source.Length < 5) return Source;
            int Start = 0, Middle = 0, End = 0;
            while (Source.Length - Start > 4)
            {
                // Rechercher un [
                Start = Source.IndexOf('[', Start);
                if (Start < 0) return Source;

                // Rechercher un :  , la clé ne doit pas excéder 5 car
                Middle = -1;
                if (Source.Length - Start > 4) Middle = Source.IndexOf(':', Start + 3, 3);

                //Si pas trouvé, recommencer car fausse alerte
                if (Middle < 0)
                {
                    Start = Start + 1;
                    continue;
                }

                End = Middle;
                int Level = 1;
                while (End < Source.Length - 1)
                {
                    End = Source.IndexOfAny(new char[] { ']', '[' }, End + 1);
                    if (End < 0) return Source;
                    switch (Source[End])
                    {
                        case '[': Level++; break;
                        case ']': Level--; break;
                    }
                    if (Level == 0) break;
                }
                if (End < 0) return Source;
                string Key = Source.Substring(Start + 1, Middle - Start - 1);
                string Param = Source.Substring(Middle + 1, End - Middle - 1);
                string Ressource = Universal_Ressource(Key, Universal_String(Param));
                if (Ressource == null)
                {
                    Start = End + 1;
                    Ressource = "";
                }
                else
                {
                    Ressource = Universal_String(Ressource);
                }
                if (Start > 0) Ressource = Source.Substring(0, Start) + Ressource;
                if (Source.Length > End + 1) Ressource += Universal_String(Source.Substring(End + 1));
                return Ressource;
            }
            return Source;
        }

        // Retourne une table selon la data source passée en paramètres
        public static DataTable Universal_Table(string Source)
        {
            string Src = Universal_String(Source).Trim();
            if (Src.Length < 5) return null;

            DataTable Dt = null;
            string Key = JR.Strings.Official(Src.Substring(0, 3));
            switch (Key)
            {
                case "XL:":  // Source fichier Excel
                case "XH:":  // Source fichier Excel
                    {
                        string[] Root_File = Src.Substring(3).Split('@');
                        string File_Name = Real_File_Name(Root_File[0]);
                        string Sheet_Name = "";
                        string Cache_Name = File_Name;
                        if (Root_File.Length > 1)
                        {
                            Sheet_Name += Root_File[1];
                            Cache_Name += "@" + Sheet_Name;
                        }
                        // Chercher dans le cache
                        Dt = Db_Cache<DataTable>.Get_Cache(Cache_Name, "U_TABLE");
                        if (Dt != null) break;
                        Dt = Excel.Read_Sheet(File_Name, Sheet_Name, Key.EndsWith("H:"), true);
                        if (Dt == null) Dt = new DataTable();
                        Db_Cache<DataTable>.Add_Cache(Cache_Name, "U_TABLE", Dt, 100 * 24); // expiration 100 jours
                    }
                    break;
                case "TX:":  // Source fichier TExt
                    {
                        string File_Name = Real_File_Name(Src.Substring(3));
                        // Chercher dans le cache
                        Dt = Db_Cache<DataTable>.Get_Cache(File_Name, "U_TABLE");
                        if (Dt != null) break;
                        Dt = new DataTable();
                        if (File.Exists(File_Name))
                        {
                            Dt = JR.Serial<DataTable>.From_Xml(File.ReadAllText(File_Name));
                            if (Dt == null) Dt = new DataTable();
                        }
                        Db_Cache<DataTable>.Add_Cache(File_Name, "U_TABLE", Dt, 100 * 24); // expiration 100 jours
                    }
                    break;
                case "DS:": // Source Db_State
                    {
                        string[] Names = JR.Strings.Split(Src.Substring(3));
                        if (Names.Length > 1)
                        {
                            return Db_State<DataTable>.Load_State(Names[0], Names[1]);
                        }
                    }
                    break;
                case "DB:":  // Source execution base de données
                    return JR.DB.App_Db.Execute_Table(Src.Substring(3));
            }
            return Dt;
        }

        // Retourne une table selon la data source passée en paramètres
        public static XmlDocument Universal_Doc(string Source)
        {
            string Src = Universal_String(Source).Trim();
            if (Src.Length < 5) return null;

            XmlDocument Dt = null;
            string Key = JR.Strings.Official(Src.Substring(0, 3));
            switch (Key)
            {
                case "XL:":  // Source fichier Excel
                case "XH:":  // Source fichier Excel
                    {
                        string[] Root_File = Src.Substring(3).Split('@');
                        string File_Name = Real_File_Name(Root_File[0]);
                        string Sheet_Name = "";
                        string Cache_Name = File_Name;
                        if (Root_File.Length > 1)
                        {
                            Sheet_Name += Root_File[1];
                            Cache_Name += "@" + Sheet_Name;
                        }
                        // Chercher dans le cache
                        Dt = Db_Cache<XmlDocument>.Get_Cache(File_Name, "U_SET");
                        if (Dt != null) break;
                        DataTable DT = Excel.Read_Sheet(File_Name, Sheet_Name, Key.EndsWith("H:"), true);
                        if (DT == null)
                        {
                            Dt = new XmlDocument();
                        }
                        else
                        {
                            using (DataSet DS = new DataSet())
                            {
                                DS.Tables.Add(DT);
                                Dt.LoadXml(DS.GetXml());
                            }
                        }
                        Db_Cache<XmlDocument>.Add_Cache(Cache_Name, "U_SET", Dt, 100 * 24); // expiration 100 jours
                    }
                    break;
            }
            return Dt;

        }

        // Retourne une table selon la data source passée en paramètres
        public static XElement Universal_Xml(string Source)
        {
            string Src = Universal_String(Source).Trim();
            if (Src.Length < 5) return null;

            XElement Dt = null;
            string Key = JR.Strings.Official(Src.Substring(0, 3));
            switch (Key)
            {
                case "XL:":  // Source fichier Excel
                case "XH:":  // Source fichier Excel
                    {
                        string[] Root_File = Src.Substring(3).Split('@');
                        string File_Name = Real_File_Name(Root_File[0]);
                        string Sheet_Name = "";
                        string Cache_Name = File_Name;
                        if (Root_File.Length > 1)
                        {
                            Sheet_Name += Root_File[1];
                            Cache_Name += "@" + Sheet_Name;
                        }
                        // Chercher dans le cache
                        Dt = Db_Cache<XElement>.Get_Cache(File_Name, "X_SET");
                        if (Dt != null) break;
                        Dt = XElement.Parse("");
                        DataTable DT = Excel.Read_Sheet(File_Name, Sheet_Name, Key.EndsWith("H:"), true);
                        if (DT == null)
                        {
                            Dt = XElement.Parse("");
                        }
                        else
                        {
                            using (DataSet DS = new DataSet())
                            {
                                DS.Tables.Add(DT);
                                Dt = XElement.Parse(DS.GetXml(), LoadOptions.PreserveWhitespace);
                            }
                        }
                        Db_Cache<XElement>.Add_Cache(File_Name, "X_SET", Dt, 100 * 24); // expiration 100 jours
                    }
                    break;
            }
            return Dt;

        }

        public static void Stop_Application()
        {
            if (Is_Web_Site)
            {
                string Web_Config = Root_Path + @"\web.config";
                try
                {
                    File.SetLastWriteTime(Web_Config, DateTime.Now);
                }
                catch { };
            }
            else
            {
                OS.Kill_Program();
            }
        }
    }
}

