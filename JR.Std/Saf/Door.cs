using System;
using System.Collections.Generic;
using System.Text;

namespace JR.Saf
{
    // Une porte sert a filtrer
    public class Door
    {
        // Liste de termes s�par�s par le premier car
        public Door(string Rules)
        {
            _Rules = JR.Strings.Split (Rules);
        }

        // Si il y a interdiction on refuse
        // Si il n'y a aucun filtre d'autorisation on accepte
        // Sinon il faut au moins une des autorisations
        public bool Is_Allowed(Keys Keys)
        {
            bool Allows = false;
            foreach (string Rule in _Rules)
            {
                if (Rule.Length == 0)
                {
                } else if (Rule[0] == '-')
                {
                    if (Keys.Has_Key(Rule.Substring(1), '*')) return false;
                }
                else if (Rule[0] == '+')
                {
                    if (Keys.Has_Key(Rule.Substring(1), '*')) return true;
                }
                else
                {
                    if (Keys.Has_Key(Rule, '*')) return true;
                    Allows = true;
                }
            }
            return (!Allows);
        }

        string [] _Rules;  // Liste de cl�s pr�c�d�es de+ ou  de - pour signifier allow ou deny
    }
}
