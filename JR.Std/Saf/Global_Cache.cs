using System;
using System.Collections.Generic;
using System.Text;
using JR.DB;
using System.IO;


namespace JR.Saf
{
    // Cache global, qui ne se vide que si on clear
    public class Global_Cache
    {
        static Memory_Cache Mem_Cache = new Memory_Cache(0);

        public static T Get<T>(string Key, Func<T> Creator) where T : class
        {
            return Mem_Cache.Get_Cached<T>(Key, Creator);
        }

        public static void Clear (string Key)
        {
            Mem_Cache.Remove_From_Cache(Key);
        }

    }
}
