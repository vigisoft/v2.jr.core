using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Xml;
using System.Linq;

namespace JR
{
    public class Generic_Dictionnary
    {
        bool _Log_Missing = false;
        public Generic_Dictionnary()
            : this(false)
        {
        }
        public Generic_Dictionnary(bool Log_Missing)
        {
            _Log_Missing = Log_Missing;
        }

        string _Sub_Prefix = "";
        public Generic_Dictionnary(bool Log_Missing, string Substitution_Prefix)
        {
            _Sub_Prefix = Substitution_Prefix;
            _Log_Missing = Log_Missing;
        }

        class Entry
        {
            public string Value;
            public bool Translated;
        }

        // Gestion d'un dictionnaire
        Dictionary<string, Entry> _Dico = new Dictionary<string, Entry>();
        Dictionary<string, Entry> _Gen_Dico = new Dictionary<string, Entry>();

        public string Try_Get_String(params string[] Keys)
        {
            return Get_Dictionary_String(true, Keys);
        }

        public bool Try_Get_String(out string Value, params string[] Keys)
        {
            return Try_Get_Dictionary_String(out Value, Keys);
        }

        public string Get_String(params string[] Keys)
        {
            return Get_Dictionary_String(false, Keys);
        }

        public IEnumerable<string> Get_All_Keys()
        {
            return _Dico.Keys.Union(_Gen_Dico.Keys);
        }

        // Traduction d'une chaine avec des param�tres {{...}}
        //  Qui sont remplac�s par la valeur du parametre correspondant
        public string Translated(string Source)
        {
            int Start, End = 0;
            Start = Source.IndexOf("{{", End);
            if (Start < 0) return Source;
            End = Source.IndexOf("}}", Start + 3);
            if (End < 0) return Source;
            string P_Name = Source.Substring(Start + 2, End - Start - 2);
            string Value = "";
            string[] Reg_Keys = P_Name.Split(':');
            if (Reg_Keys.Length == 3)
            {
                switch (Reg_Keys[1].ToUpper())
                {
                    case "LM":
                        Value = OS.Get_Value("KLM", Reg_Keys[1], Reg_Keys[2]);
                        break;
                    case "CU":
                        Value = OS.Get_Value("KCU", Reg_Keys[1], Reg_Keys[2]);
                        break;
                }

            }
            else
            {
                if (!Try_Get_String(out Value, _Sub_Prefix + P_Name)) return Source;
            }
            return Translated(Source.Replace("{{" + P_Name + "}}", Value));
        }

        // Traduction d'une chaine avec des param�tres {{...}}
        // La substitution ne s'op�re qu'avec les P.XXXX
        //  Qui sont remplac�s par la valeur du parametre correspondant
        // On ajoute le pr�fixe au mot cl� et si on ne trouve pas on essaie sans le prefixe
        public string Prefix_Translated(string Source, string Prefix)
        {
            int Start, End = 0;
            Start = Source.IndexOf("{{", End);
            if (Start < 0) return Source;
            End = Source.IndexOf("}}", Start + 3);
            if (End < 0) return Source;
            string P_Name = Source.Substring(Start + 2, End - Start - 2);
            string Value = "";
            if (!Try_Get_String(out Value, _Sub_Prefix + Prefix, P_Name))
            {
                if (!Try_Get_String(out Value, _Sub_Prefix + P_Name)) return Source;
            }
            return Translated(Source.Replace("{{" + P_Name + "}}", Value));
        }

        // Recherche une chaine dans les dicos
        string Get_Dictionary_String(bool Quiet, params string[] Keys)
        {
            string Value = "";
            if (Try_Get_Dictionary_String(out Value, Keys)) return Value;
            if (Quiet) return "";
            if (_Log_Missing) Saf.Logs.Err("Unknown dictionary key : " + Value);
            return Value;
        }

        // Recherche une chaine dans les dicos
        bool Try_Get_Dictionary_String(out string Result, params string[] Keys)
        {
            string _Key = "";
            foreach (string i in Keys)
            {
                if (i == null) continue;
                _Key += (_Key == "") ? i : "." + i;

            };
            _Key = _Key.ToUpper();
            Entry E = null;
            bool Found = _Dico.TryGetValue(_Key, out E);
            string Get_Value()
            {
                if (!E.Translated)
                {
                    E.Value = Translated(E.Value);
                    E.Translated = true;
                }
                return E.Value;
            }
            if (Found)
            {
                Result = Get_Value();
                return true;
            }
            foreach (var Gen_Key in _Gen_Dico)
            {
                if (JR.Strings.Match(_Key, Gen_Key.Key))
                {
                    E = Gen_Key.Value;
                    Result = Get_Value();
                    return true;
                }
            }
            Result = _Key;
            return false;
        }

        // Ins�re une chaine dans les dicos
        // La chaine est dans un premier temps marqu�e comme non traduite
        //   par deux ~~
        public void Set_String(string Value, params string[] Keys)
        {
            string _Key = "";
            foreach (string i in Keys)
            {
                _Key += (_Key == "") ? i : "." + i;
            };
            _Key = _Key.ToUpper();
            if (_Key.Contains("*") || _Key.Contains("?"))
            {
                _Gen_Dico[_Key] = new Entry { Value = Value, Translated = false };
            }
            else
            {
                _Dico[_Key] = new Entry { Value = Value, Translated = false };
            }
        }

        void Append_Line(Dictionary<string, Entry> Dic, string Key, string Value)
        {
            var S = Dic[Key];
            if (S != null && S.Value.Length > 0)
            {
                Dic[Key].Value = string.Concat(S.Value, "\r\n", Value);
            }
            else
            {
                Dic[Key] = new Entry { Value = Value, Translated = false };
            }
        }
        void Append_String(string Value, string Keys)
        {
            string _Key = Keys.ToUpper();
            if (_Key.Contains("*") || _Key.Contains("?"))
            {
                Append_Line(_Gen_Dico, _Key, Value);
            }
            else
            {
                Append_Line(_Dico, _Key, Value);
            }
        }

        public bool Get_Bool(params string[] Keys)
        {
            return JR.Convert.To_Boolean(Try_Get_String(Keys));
        }

        public int Get_Int(params string[] Keys)
        {
            return JR.Convert.To_Integer(Try_Get_String(Keys));
        }

        public double Get_Float(params string[] Keys)
        {
            return JR.Convert.To_Float(Try_Get_String(Keys));
        }

        // Check parameter presence
        public bool Exist(params string[] Keys)
        {
            return (Try_Get_String(Keys) != "");
        }

        // List of pattern parameters
        public string[] Keys_Like(string Pattern)
        {
            List<String> Keys = new List<string>();
            foreach (string K in _Dico.Keys)
            {
                if (JR.Strings.Match(K, Pattern))
                {
                    Keys.Add(K);
                }
            }
            return Keys.ToArray();
        }

        List<string> _Sources = new List<string>();

        // Partie gestion des fichiers source
        public void Add_Source(string File_Name)
        {
            Load_File(File_Name);
        }

        static string No_Sheet(string Name)
        {
            if (Name.IndexOf('@') < 0) return Name;
            return Name.Split('@')[0];
        }

        public Func<string, bool> Confirm_Load = null;

        void Load_File(string File_Name)
        {
            if (No_Sheet(File_Name.ToLower()).EndsWith(".xls") || No_Sheet(File_Name.ToLower()).EndsWith(".xlsx"))
            {
                Load_Xls_Dictionary(File_Name);
            }
            else if (File_Name.ToLower().EndsWith(".xml"))
            {
                Load_Xml_Dictionary(File_Name);
            }
            else if (File_Name.ToLower().EndsWith(".txt"))
            {
                Load_Txt_Dictionary(File_Name);
            }
            _Sources.Add(File_Name);
            if (Saf.Main.Log_Config) Saf.Logs.Log($"Loaded dictionnary {File_Name}");
        }

        public void Load_Xls_Dictionary(string File)
        {
            using (DataSet DS = JR.Excel.Read_All(File))
            {
                if (DS == null) return;

                foreach (DataTable Sheet in DS.Tables)
                {
                    string Sheet_Name = Sheet.TableName.Split('!')[0];    // Terminer par !xxx permet d'avoir des feuilles de nom diff�rent qui pointent le m�me pr�fixe

                    // Le point est remplac� par un # par Excel et le $ est ins�r�
                    //string Sheet_Title = Sheet_Name.Remove(Sheet_Name.Length - 1).Replace("#", ".");

                    // LE ! est un commentaire
                    if (Sheet_Name == "") continue;
                    List<string>[] Col_Key = null;
                    bool?[] Col_Prefix = null;
                    int Max_Col = 0;
                    string Sheet_Prefix = "";
                    if (Sheet_Name.EndsWith(".")) Sheet_Prefix = Sheet_Name;
                    void Loop_Keys(int Col, string Key, Action<string> On_Key)
                    {
                        bool? Prefix = Col_Prefix[Col];
                        foreach (var K in Col_Key[Col])
                        {
                            switch (Prefix)
                            {
                                case true:
                                    On_Key(K + Sheet_Prefix + Key);
                                    break;
                                case false:
                                    On_Key(Sheet_Prefix + Key + K);
                                    break;
                            }
                        }
                    }
                    foreach (DataRow Row in Sheet.Rows)
                    {
                        // Premi�re ligne
                        if (Col_Key == null)
                        {
                            Max_Col = Sheet.Columns.Count;
                            Col_Key = new List<string>[Max_Col];
                            Col_Prefix = new bool?[Max_Col];
                            for (int Col = 1; Col < Sheet.Columns.Count; Col++)
                            {
                                string Head = Row[Col].ToString();
                                var K = new List<string>();
                                Col_Key[Col] = K;
                                bool? Prefix = null;
                                if (Head.StartsWith("."))
                                {
                                    Prefix = false;
                                }
                                else if (Head.EndsWith("."))
                                {
                                    Prefix = true;
                                }
                                else
                                {
                                    // Si c'est une colonne sans point final ou initial
                                    //   c'est la derni�re sauf si il y a deja 
                                    if (Col == 1)
                                    {
                                        Max_Col = 2;
                                    }
                                    else
                                    {
                                        Max_Col = Col;
                                    }
                                    break;
                                }
                                Col_Prefix[Col] = Prefix;
                                // Expand [,,,]
                                var H = Head.Scan();
                                var Start = H.Next("[");
                                if (!H.Found)
                                {
                                    K.Add(Head);
                                }
                                else
                                {
                                    var Middle = H.Next("]");
                                    if (!H.Found)
                                    {
                                        K.Add(Head);
                                    }
                                    else
                                    {
                                        var End = H.Tail();
                                        foreach (var KK in Middle.Split(','))
                                        {
                                            K.Add($"{Start}{KK}{End}");
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            string Key = Row[0]?.ToString();
                            if (Key == null || Key == "" || Key.StartsWith("!")) continue;
                            bool _No_Head = Key[0] == '@'; // Les cl�s en @ sont trait�es directement
                            if (_No_Head)
                            {
                                Key = Key.Substring(1);
                            }

                            // la premi�re valeur est la valeur par d�faut pour les autres
                            string Val1 = "";
                            for (int Col = 1; Col < Max_Col; Col++)
                            {
                                string Val = Row[Col].ToString();
                                if (Col == 1)
                                {
                                    Val1 = Val;
                                }
                                else
                                {
                                    if (Val == "") Val = Val1;
                                }
                                if (_No_Head)
                                {
                                    Set_String(Val, Key.ToUpper());
                                }
                                else
                                {
                                    Loop_Keys(Col, Key, Full_Key => Set_String(Val, Full_Key.ToUpper()));
                                }
                                if (_No_Head) break;
                            }
                        }
                    }
                }
            }
        }


        void Load_Xml_Dictionary(string File)
        {
            XmlTextReader Reader;
            try
            {
                Reader = new XmlTextReader(File);
            }
            catch
            {
                Saf.Logs.Err("Bad XML configuration file : " + File);
                return;
            }

            string Section = "";
            string Key = "";
            while (Reader.Read())
            {
                switch (Reader.NodeType)
                {
                    case XmlNodeType.Element:
                        switch (Reader.Name)
                        {
                            case "Section":
                                Section = Reader[0];  // Le premier attribut est le nom
                                break;
                            case "Value":
                                Key = Reader[0];  // Le premier attribut est le nom de la cl�
                                Set_String(Reader[1], (Section + "." + Key).ToUpper());
                                break;
                        }
                        break;
                }
            }
            Reader.Close();
        }
        bool Load_Txt_Dictionary(string File)
        {
            string[] Lines;
            try
            {
                Lines = System.IO.File.ReadAllLines(File, Encoding.Default);
            }
            catch
            {
                Saf.Logs.Err("Bad TXT configuration file : " + File);
                return false;
            }

            string Section = "";
            string Last_Key = "";
            bool In_Block = false;
            foreach (string Line in Lines)
            {
                if (Line.StartsWith("[") && Line.EndsWith("]"))
                {
                    In_Block = false;
                    Section = JR.Strings.Official(Line.Substring(1, Line.Length - 2)) + ".";
                    if (Confirm_Load != null && Section.StartsWith("?"))
                    {
                        string Info = Section + File;
                        if (!Confirm_Load(Info))
                        {
                            Saf.Logs.Err("User skipped configuration file : " + Info);
                            return true;
                        }
                    }
                }
                else
                {
                    if (In_Block)
                    {
                        if (Line == "�")
                        {
                            In_Block = false;
                        }
                        else
                        {
                            Append_String(Line, Last_Key);
                        }
                    }
                    else
                    {
                        string[] L = Line.Split("=".ToCharArray(), 2);
                        if (L.Length == 2)
                        {
                            In_Block = false;
                            if (L[0] == "+")
                            {
                                Append_String(L[1], Last_Key);
                            }
                            else
                            {
                                Last_Key = Section + JR.Strings.Official(L[0]);
                                if (L[1] == "�")
                                {
                                    Set_String("", Last_Key);
                                    In_Block = true;
                                }
                                else
                                {
                                    Set_String(L[1], Last_Key);
                                }

                            }
                        }
                    }
                }
            }
            return true;
        }

    }
}
