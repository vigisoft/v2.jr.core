﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace JR
{
    public class Perf
    {
        Stopwatch W = new Stopwatch();
        public Perf()
        {
            W.Start();
        }
        public void Tick(string Comment)
        {
            JR.Saf.Logs.Log("{0} - {1}", W.Elapsed, Comment);
        }

        public void Reset ()
        {
            W.Restart();
        }

        public TimeSpan Elapsed
        {
            get
            {
                return W.Elapsed;
            }
        }
    }
}
