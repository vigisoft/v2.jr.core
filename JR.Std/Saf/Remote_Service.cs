using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using JR.DB;
using JR.Saf;
using System.Net;

namespace JR.Web
{
    public class Remote_Service
    {
        public static T Call<T>(string Site, string Service, params string [] Params)
        {
            return JR.Serial<T>.From_Json(Call(Site, Service, Params));
        }

        public static string Call(string Site, string Service, params string [] Params)
        {
            string Remote_URL = JR.Strings.Generic_Text(Main.Parameter(Site + ".REMOTE_SERVICE"), "S", Service, "P", Encode_Params (Params));
            if (Remote_URL == "") return "";
            using (WebClient WC = new WebClient {Encoding = Encoding.UTF8})
            {
                try
                {
                    return WC.DownloadString(Remote_URL);
                }
                catch
                {
                    return "";
                }
            }
        }

        // Encodage de parametres deux a deux
        public static string Encode_Params(string[] Params)
        {
            return JR.Strings.Encode(Serial<string []>.To_Xml (Params));
        }

        // Decodage de parametres deux a deux
        public static Bag Decode_Params(string Params)
        {
            Bag Bag = new Bag();
            try
            {
                string Key = null;
                foreach (string S in Serial<string[]>.From_Xml(JR.Strings.Decode(Params)))
                {
                    if (Key == null)
                    {
                        Key = S;
                    }
                    else
                    {
                        Bag[Key] = S;
                        Key = null;
                    }
                }
            }
            catch { }
            return Bag;
        }
    }
}
