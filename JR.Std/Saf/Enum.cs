using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Xml;
using System.Data;
using System.Xml.Serialization;

namespace JR.Saf
{

    /// <summary>
    /// Version all�g�e et g�n�rique du Framework pour fonctionnement standalone 
    /// Supprime des notions incompatibles, comme la langue
    ///  ou le nom d'application unique
    /// </summary>
    public class Enums
    {

        List<JR.Pair<string>> _Items = new List<Pair<string>>();
        Dictionary<string, JR.Pair<string>> _Dic = new Dictionary<string, Pair<string>>();

        void Add(string Key, string Value)
        {
            Pair<string> P = new Pair<string>(Key, Value);
            _Dic[Key] = P;
            _Items.Add(P);
        }

        public List<JR.Pair<string>> Items
        {
            get { return _Items; }
        }

        public string this[string Index]
        {
            get
            {
                Pair<string> P;
                if (_Dic.TryGetValue(Index.ToUpper(), out P)) return P.Value;
                return "";
            }
        }

        public string this[int Index]
        {
            get
            {
                Pair<string> P;
                if (_Dic.TryGetValue(Index.ToString(), out P)) return P.Value;
                return "";
            }
        }

        static Dictionary<string, Enums> _Enums = new Dictionary<string, Enums>();

        public static Enums Get_Enum(string Language, string Key)
        {
            Enums E;
            if (_Enums.TryGetValue(Key + "." + Language, out E)) return E;
            if (_Enums.TryGetValue(Key + ".FR", out E)) return E;
            if (_Enums.TryGetValue(Key, out E)) return E;
            return new Enums();
        }

        private static void Load_Enums(string Space)
        {
            try
            {
                string Dir_Name = Main.Real_File_Name("Enum", Space);
                if (!Directory.Exists(Dir_Name)) return;
                string[] Files = System.IO.Directory.GetFiles(Dir_Name, "*.xls");
                foreach (string File in Files)
                {
                    Load_Xls_Enum(File);
                }
            }
            catch (Exception e)
            {
                Logs.Err("Error listing xls enum space : {0}", Space);
                Logs.Except(e);
            }
        }


        public static void Load()
        {
            foreach (string Space in Main.Space_Names)
            {
                Load_Enums(Space);
            }
        }


        // Charge tous les dictionnaires de l'application courante
        // Les dictionnaires sont des classeurs EXCEL dans le r�pertoire ENUM
        // L'entete de chaque colonne puis le nom de chaque feuille est pr�fix� si il se termine par "."
        // Si il n'y a pas d'entete se terminant par un point, on ne charge que la premi�re colonne
        //  Ce qui permet de mettre des comms dans les autres
        // Une cl� qui commence par -- est un commentaire
        static void Load_Xls_Enum(string File)
        {
            using (DataSet DS = JR.Excel.Read_All(File))
            {
                if (DS == null) return;

                foreach (DataTable Sheet in DS.Tables)
                {
                    string Sheet_Name = Sheet.TableName;

                    // LE ! est un commentaire
                    if (Sheet_Name.StartsWith("!")) continue;


                    // Le titre est le nom de l'�num�ration
                    // La premi�re colonne est la cl�
                    // Les autres les valeurs selon la langue
                    //  Effacer la pr�c�dente liste
                    Enums[] Heads = null;
                    int Max_Col = 0;
                    foreach (DataRow Row in Sheet.Rows)
                    {
                        // Premiere ligne qui permet de d�terminer les noms de colonne
                        if (Heads == null)
                        {
                            Max_Col = Sheet.Columns.Count;
                            Heads = new Enums[Max_Col];
                            for (int Col = 1; Col < Sheet.Columns.Count; Col++)
                            {
                                string Enum_Name = Sheet_Name + "." + Row[Col].ToString();
                                _Enums.Remove(Enum_Name);
                                Heads[Col] = new Enums();
                                _Enums.Add(Enum_Name, Heads[Col]);
                            }
                        }
                        else
                        {
                            string Key = Row[0] as string;
                            if (Key == null || Key == "" || Key.StartsWith("!")) continue;

                            // la premi�re valeur est la valeur par d�faut pour les autres
                            string Val1 = "";
                            for (int Col = 1; Col < Max_Col; Col++)
                            {
                                string Val = Row[Col].ToString();
                                if (Col == 1)
                                {
                                    Val1 = Val;
                                }
                                else
                                {
                                    if (Val == "") Val = Val1;
                                }
                                Heads[Col].Add(Key.ToUpper(), Val);
                            }
                        }
                    }
                }
            }
        }
    }
}
