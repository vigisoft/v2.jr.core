using System;
using System.Collections.Generic;
using System.Text;
using JR.DB;
using System.IO;


namespace JR.Saf
{
    public class Db_Cache<T> where T:class
    {
        static Memory_Cache Mem_Cache = new Memory_Cache(1);

        static DateTime Write_Time(string File_Name)
        {
            return File.GetLastWriteTimeUtc(File_Name.Split('@')[0]);
        }

        public static T Get_Cache(string File_Name, string Module)
        {
            T Obj = Mem_Cache.Get_Cached<T>(File_Name + Module, Write_Time(File_Name));
            return Obj;
        }

        public static void Add_Cache(string File_Name, string Module, T Object, int Expiration)
        {
            Mem_Cache.Put_In_Cache(File_Name + Module, Object, Write_Time(File_Name));
        }

    }
}
