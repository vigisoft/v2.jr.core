using System;
using System.Collections.Generic;
using System.IO;

namespace JR.Saf
{
    public static class File_State<T> where T : class
    {
        static string _Dir_Name = "";
        static string Storage_Name = "";
        public static string Get_Storage_Name => Path.Combine(JR.Saf.Main.App_Folder, "States");

        static string Storage_File(string File_Name) => Path.Combine(Storage_Name, File_Name);

        static void Check_Dir()
        {
            // Si d�j� check� ne rien faire
            if (Storage_Name != "") return;
            _Dir_Name = "states";

            // Le r�pertoire de stockage est dans local application
            Storage_Name = Get_Storage_Name;
            bool Created = false;
            JR.Files.Check_Directory(Storage_File(@"*"), out Created);
        }

        public class Entry
        {
            public string Key = "";
            public DateTime Date;
            public T State = null;
        }

        public static List<Entry> Load_States(string Key_Pattern)
        {
            Check_Dir();
            List<Entry> L = new List<Entry>();
            foreach (var F in JR.Files.Safe_Dir(Storage_File(Key_Pattern)))
            {
                Entry E = new Entry();
                E.Key = F;
                E.Date = DateTime.Now;
                string Xml = JR.Files.Safe_Read_All(F);
                E.State = (Xml == "") ? default(T) : (Xml.StartsWith("<") ? JR.Serial<T>.From_Xml(Xml) : JR.Serial<T>.From_Json(Xml));
                L.Add(E);
            }
            return L;
        }

        public static T Load_State(string Key)
        {
            List<Entry> L = Load_States(Key);
            foreach (Entry E in L)
            {
                return E.State;
            }
            return default(T);
        }

        public static bool Save_State(string Key, T State)
        {
            if (State == null)
            {
                Remove_States(Key);
                return true;
            }
            Check_Dir();

            try
            {
                string Content = JR.Serial<T>.To_Json(State);
                JR.Files.Safe_Write(Storage_File(Key), Content);
            }
            catch { return false; }
            return true;
        }

        public static void Remove_States(string Key_Pattern)
        {
            Check_Dir();
            foreach (string F in JR.Files.Safe_Dir(Storage_File(Key_Pattern)))
            {
                JR.Files.Safe_Delete(F);
            }
        }

        public static Stream Get_Stream(string Key, bool Read)
        {
            Check_Dir();
            try
            {
                if (Read)
                {
                    if (!File.Exists(Storage_File(Key))) return null;
                    return new FileStream(Storage_File(Key), FileMode.Open, FileAccess.Read);
                }
                else
                {
                    return new FileStream(Storage_File(Key), FileMode.Create, FileAccess.Write);
                }
            }
            catch { return null; }
        }

    }
}
