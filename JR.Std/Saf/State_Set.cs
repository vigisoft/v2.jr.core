using System;
using System.Collections.Generic;
using System.Text;

namespace JR.Saf
{
    public class Key_Value
    {
        public string Key;
        public string Value;
        public Key_Value(string Key, string Value)
        {
            this.Key = Key; this.Value = Value;
        }
        public Key_Value() { }
    }
    public class State_Set
    {

        public Dictionary<string, string> Values;
        string _Module_Name;
        string _Key_Name;

        public State_Set(string Module_Name, string Key_Name):this(Module_Name, Key_Name, Db_State<List<Key_Value>>.Load_State(Module_Name, Key_Name))
        {
        }

        public State_Set(string Module_Name, string Key_Name, List<Key_Value> Vals)
        {
            if (Module_Name == "") return;
            if (Vals != null)
            {
                Values = new Dictionary<string, string>();
                foreach (Key_Value KV in Vals)
                {
                    Values[KV.Key] = KV.Value;
                }
            }
            _Module_Name = Module_Name;
            _Key_Name = Key_Name;
            Changed = false;
        }

        public string Get(string Key, string Default_Value)
        {
            if (Values == null) return Default_Value;
            string K = Key.ToUpper();
            if (!Values.ContainsKey(K)) return Default_Value;
            return Values[K];
        }

        public Key_Value[] Get_Subset(string Key)
        {
            if (Values == null) return new Key_Value[0];
            string K = Key.ToUpper();
            List<Key_Value> Vals = new List<Key_Value>();
            foreach (KeyValuePair<string, string> V in Values)
            {
                if (V.Key.StartsWith(K))
                {
                    Vals.Add(new Key_Value (V.Key, V.Value));
                }
            }
            return Vals.ToArray();
        }

        public bool Is_Empty
        {
            get { return Values == null; }
        }
        public void Reset()
        {
            Changed = Changed ||Values != null;
            Values = null;
        }

        public bool Changed { get; set; }
        public void Set (string Key, string Value)
        {
            string K = Key.ToUpper();
            Changed = Changed || Values == null;
            if (Values == null) Values = new Dictionary<string, string>();
            Changed = Changed || Get(K, "") != Value;
            Values[K] = Value;
        }

        public bool Save()
        {
            Changed = false;
            List<Key_Value> Pairs = null;
            if (Values != null)
            {
                Pairs = new List<Key_Value>();
                foreach (KeyValuePair<string, string> V in Values)
                {
                    Pairs.Add(new Key_Value (V.Key, V.Value));
                }
            }
            return Db_State<List<Key_Value>>.Save_State(_Module_Name, _Key_Name, Pairs);
        }

        public List<Key_Value> Vals
        {
            get{
                List<Key_Value> Pairs = null;
                if (Values != null)
                {
                    Pairs = new List<Key_Value>();
                    foreach (KeyValuePair<string, string> V in Values)
                    {
                        Pairs.Add(new Key_Value(V.Key, V.Value));
                    }
                }
                return Pairs;
            }
        }

    }
}
