using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using JR.DB;
using System.Xml.Serialization;

namespace JR.Saf
{
    public static class Db_State<T> where T : class
    {
        const string Req_Load = "select Module as M, [Key] as K, Value as V, [Date] as D from [JR$State] where module like @0 and [Key] like @1";
        const string Req_Get = "select Value as V from [JR$State] where module = @0 and [Key] = @1";
        const string Req_Del = "delete from [JR$State] where module = @0 and [Key] = @1";
        const string Req_Add = "delete from [JR$State] where module=@0 and [key]=@1; insert into [JR$State] values (@0,@1,@2, getutcdate())";
        const string Req_List = "select [Key] as K from [JR$State] where module = @0";

        public class Entry
        {
            public string Module_Name = "";
            public string Key_Name = "";
            public DateTime Date;
            public T State = null;
        }

        public static string [] List_States(string Module_Name)
        {
            List<string> L = new List<string>();
            using (Db_Reader DR = new Db_Reader(Req_List, Module_Name))
            {
                while (DR.Read())
                {
                    L.Add(DR.Get_String(0));
                }
            }
            return L.ToArray();
        }

        public static List<Entry> Load_States(string Module_Pattern, string Key_Pattern)
        {
            List<Entry> L = new List<Entry>();
            using (Db_Reader DR = new Db_Reader(Req_Load, Module_Pattern.Replace('*', '%'), Key_Pattern.Replace('*', '%')))
            {
                while (DR.Read())
                {
                    Entry E = new Entry();
                    E.Module_Name = DR.Get_String(0);
                    E.Key_Name = DR.Get_String(1);
                    E.Date = DR.Get_Date(3);
                    string Xml = DR.Get_String(2);
                    E.State = (Xml == "") ? default(T) : JR.Serial<T>.From_Xml(Xml);
                    L.Add(E);
                }
            }
            return L;
        }

        public static T Load_State(string Module_Name, string Key_Name) 
        {
            if (Module_Name == "") return default(T);
            // Recup�rer les donnees sous forme de chaine
            string Xml = Db.Execute_String (Req_Get, Module_Name, Key_Name);
            if (Xml == "") return default (T);
            return JR.Serial<T>.From_Xml(Xml);
        }

        public static bool Save_State(string Module_Name, string Key_Name, T State)
        {
            if (State == null)
            {
                Db.Execute(Req_Del, Module_Name, Key_Name);
                return true;
            }
            string Value = JR.Serial<T>.To_Xml(State);
            return Db.Execute(Req_Add, Module_Name, Key_Name, Value);
        }

        public static void Remove_States(string Module_Pattern, string Key_Pattern)
        {
            Db.Execute (Req_Del, Module_Pattern.Replace('*','%'), Key_Pattern.Replace('*','%'));
        }

        // V�rifie la pr�sence de la table et la cr�e si elle n'existe pas
        public static bool Init()
        {
            return Db.Create_Table("JR$State", Req_Create);
        }

        const string Req_Create = @"
        CREATE TABLE [dbo].[JR$State](
	[Module] [varchar](10) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[Date] [datetime] NOT NULL,
         CONSTRAINT [PK_JR$State] PRIMARY KEY CLUSTERED 
        (
	        [Module] ASC,
        	[Key] ASC
        )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
        ) ON [PRIMARY]";
    }
}
