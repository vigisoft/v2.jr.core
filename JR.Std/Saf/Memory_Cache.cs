using System;
using System.Collections.Generic;
using System.Text;

namespace JR.Saf
{
    public class Memory_Cache
    {
        double _Hours = 1.0;
        Dictionary<string, Cache_Entry> _Cache = new Dictionary<string, Cache_Entry>();
        public Memory_Cache(int Hours)
        {
            _Hours = (double)Hours;
        }

        public Memory_Cache()
        {
            _Hours = 0;
        }

        public void Remove_From_Cache(string Key)
        {
            _Cache.Remove(Key);
        }

        public T Get_Cached<T>(string Key, Func<T> Create) where T : class
        {
            Cache_Entry C;
            if (!_Cache.TryGetValue(Key, out C))
            {
                C = new Cache_Entry(Create());
                _Cache[Key] = C;
            }
            return C.Object as T;
        }

        public void Put_In_Cache(string Key, object O, DateTime Signature)
        {
            Cache_Entry C = new Cache_Entry(O);
            C.Purge = DateTime.UtcNow.AddHours(_Hours);
            C.Signature = Signature;
            _Cache[Key] = C;
        }

        public T Get_Cached<T>(string Key, DateTime Signature) where T : class
        {
            Cache_Entry C;
            if (!_Cache.TryGetValue(Key, out C)) return null;
            if (C.Signature != Signature)
            {
                _Cache.Remove(Key);
                return null;
            }
            C.Purge = DateTime.UtcNow.AddHours(_Hours);
            Purge_Cache();
            return C.Object as T;
        }

        class Cache_Entry
        {
            public object Object = null;
            public DateTime Signature;
            public DateTime Purge;
            public Cache_Entry(object O)
            {
                Object = O;
            }
        }

        DateTime _Time_To_Purge = DateTime.UtcNow.AddMinutes (10.0);
        void Purge_Cache()
        {
            if (_Hours == 0) return;
            if (_Time_To_Purge > DateTime.UtcNow) return;
            DateTime _Limit = DateTime.UtcNow;
            List<string> _Removed = new List<string>();
            foreach (KeyValuePair<string, Cache_Entry> KP in _Cache)
            {
                if (KP.Value.Purge < _Limit)
                {
                    _Removed.Add(KP.Key);
                }
            }
            foreach (string Key in _Removed)
            {
                _Cache.Remove(Key);
            }
            _Time_To_Purge = DateTime.UtcNow.AddMinutes(10.0);
        }

    }
}
