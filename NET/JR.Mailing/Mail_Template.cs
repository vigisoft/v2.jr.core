﻿using BlazorTemplater;
using JR.Saf;
using Microsoft.AspNetCore.Components;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace JR.Mailing
{
    public class Mail_Template : MailMessage
    {
        public Bag Bag = new Bag();
        public string Smtp_Server = JR.Saf.Main.Parameter("SMTP_Server");

        Dictionary<string, LinkedResource> Cids = new Dictionary<string, LinkedResource>();
        string Cid_Suffix = $"@{Guid.NewGuid():N}";

        public string Html_Body = "";
        public void Generate<TComponent>(object Model = null) where TComponent : IComponent
        {
            IsBodyHtml = true;
            BodyEncoding = System.Text.Encoding.UTF8;

            Templater Tp = new();
            Dictionary<string, object> Params = new();
            Params["Mail"] = this;
            if (Model != null) Params["Model"] = Model;
            Html_Body = Tp.RenderComponent<TComponent>(Params);

            // Ajouter deux alternate views
            var Text_Alt = AlternateView.CreateAlternateViewFromString(Html_Body.To_Plain_Text(), System.Text.Encoding.ASCII, "text/plain");
            AlternateViews.Add(Text_Alt);
            var Html_Alt = AlternateView.CreateAlternateViewFromString(Html_Body, System.Text.Encoding.Default, "text/html");
            foreach (var LR in Cids)
            {
                Html_Alt.LinkedResources.Add(LR.Value);
            }
            AlternateViews.Add(Html_Alt);

        }

        public bool Send()
        {
            bool Success = false;
            try
            {
                var C = Smtp_Server.Scan();
                using SmtpClient Client = new(C.Next("|"));
                if (!C.End) Client.Port = C.Next("|").To_Integer();
                if (!C.End)
                {
                    Client.Credentials = new NetworkCredential(C.Next("|"), C.Next("|"));
                    Client.EnableSsl = true;
                }
                Client.Send(this);
                Success = true;
            }
            catch
            {
            }
            Attachments?.Dispose();
            return Success;
        }

        public static string To_Html(string Source)
        {
            var Lines = Source.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            if (Lines.Length < 2) return HttpUtility.HtmlEncode(Source);
            var SB = new StringBuilder();
            foreach (string P in Lines)
            {
                if (SB.Length > 0) SB.Append("<br/>");
                SB.Append(HttpUtility.HtmlEncode(P));
            }
            return SB.ToString();
        }

        public static string To_Base64(string File_Name)
        {
            var File_Content = JR.Files.Full_File(File_Name);
            return "data:" + JR.Files.Mime_String(File_Name) + ";base64," + System.Convert.ToBase64String(File_Content);
        }

        public string To_Cid(string File_Name, string URL)
        {
            var Temp_File = System.IO.Path.GetTempFileName();
            var Paths = JR.Files.Paths(File_Name);
            var Content_Id = (Paths.File + Cid_Suffix).ToLower();
            LinkedResource LR = null;
            if (!Cids.TryGetValue(Content_Id, out LR))
            {
                try
                {
                    Net_File.Download(URL, Temp_File);
                    LR = new LinkedResource(Temp_File, JR.Files.Mime_String(File_Name)) { ContentId = Content_Id, TransferEncoding = System.Net.Mime.TransferEncoding.Base64 };
                    Cids.Add(Content_Id, LR);
                }
                catch
                {
                }
            }
            JR.Files.Safe_Delete(Temp_File);
            return "cid:" + Content_Id;
        }

        public string To_Cid(string File_Name)
        {

            var Paths = JR.Files.Paths(File_Name);
            var Content_Id = (Paths.File + Cid_Suffix).ToLower();
            LinkedResource LR = null;
            if (!Cids.TryGetValue(Content_Id, out LR))
            {
                try
                {
                    LR = new LinkedResource(File_Name, JR.Files.Mime_String(File_Name)) { ContentId = Content_Id, TransferEncoding = System.Net.Mime.TransferEncoding.Base64 };
                    Cids.Add(Content_Id, LR);
                }
                catch
                {
                }
            }
            return "cid:" + Content_Id;
        }

        public int Schedule(JR.DB.Dbase Db, Guid Owner, List<string> EM_Dest, string EM_Control)
        {
            // Supprimer la session precedente
            Db.Exec("delete from JR$Mailing where source=@0", Owner);
            // Créer une nouvelle session
            var S_Id = Db.Insert_Request("insert into JR$MAILING (source, stream,status, d_create, EM_Wait, EM_Control) values (@0,@1,'S', getdate(), @2,@3)",
                Owner, JR.Mail.SMailMessage.Serialize_From_Mail(this), EM_Dest.To_Json(), EM_Control);
            return (int)S_Id;
        }

        public bool Send_Preview(string Address)
        {
            bool Success = false;
            var Stream = JR.Mail.SMailMessage.Serialize_From_Mail(this);
            var MM = JR.Mail.SMailMessage.Create_From(Stream);
            MM.To.Clear();
            MM.To.Add(Address);
            try
            {
                using SmtpClient Client = new(Smtp_Server);
                Client.Send(MM);
                Success = true;
            }
            catch
            {
            }
            Attachments?.Dispose();
            return Success;
        }

    }
    public class Mail_Template<T> : Mail_Template
    {
        public T Model;
        public Mail_Template(T M)
        {
            Model = M;
        }
    }
}