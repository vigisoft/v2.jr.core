﻿# JR.Mailing

La génération de mail est basée sur le package BlazorTemplater
[https://github.com/conficient/BlazorTemplater](https://github.com/conficient/BlazorTemplater)

Elle fonctionne sour .Net9+, supporte les templates et les layouts en `razor components`

Un exemple simple est dans Example

Pour éditer facilement un template razor dans un projet quelconque :
* s'assurer que chaque fichier razor est bien de type contenu
* le type de projet dans le fichier `.csproj` doit être `<Project Sdk="Microsoft.NET.Sdk.Razor">` ou bien 
`<Project Sdk="Microsoft.NET.Sdk.Web">`
* la class du composant est dans le namespace de son répertoire
```html
@inherits LayoutComponentBase
<html>
<head>
    <title>Layout Example</title>
</head>
<body>
    <div class="content">
        @Body
    </div>
</body>
</html>
```
