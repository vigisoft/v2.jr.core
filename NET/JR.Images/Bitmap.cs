﻿using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Jpeg;
using SixLabors.ImageSharp.Metadata;
using SixLabors.ImageSharp.Processing;
using SI = SixLabors.ImageSharp;
using SixLabors.ImageSharp.Metadata.Profiles.Iptc;
using JR;

namespace JR.Images
{

    public class Bitmap : IDisposable
    {

        SI.Image Img = null;

        string Img_File_Name = null;
        public class Watermark
        {
            public string Path;
            public int H_Spacing = 0;
            public int V_Spacing = 0;
            public double Transparency = 0.0;
        }


        public void Load_Bmp(string File_Name)
        {
            Img?.Dispose();
            Img_File_Name = File_Name;
            Img = SI.Image.Load(File_Name);
            Width = Img.Width;
            Height = Img.Height;
        }
        public void Load_Bmp(Stream File_Stream)
        {
            Img?.Dispose();
            Img_File_Name = null;
            Img = SI.Image.Load(File_Stream);
            Width = Img.Width;
            Height = Img.Height;
        }
        public void Dispose()
        {
            Img?.Dispose();
        }

        public int Width = 0;
        public int Height = 0;

        ImageMetadata Get_Mata_Data() => Img?.Metadata;

        public Iptc_Reader Get_Metadata()
        {
            if (Img_File_Name != null) return new Iptc_Reader(Img_File_Name);
            var File_Name = Path.GetTempFileName();
            Img.Save(File_Name);
            var R = new Iptc_Reader(File_Name);
            JR.Files.Safe_Delete(File_Name);
            return R;
        }

        public void Save(string File, int Width, int Height, Watermark Watermark, int Dpi)
        {
            JR.Files.Check_Directory(File);
            int Nwidth = Img.Width;
            int Nheight = Img.Height;
            if (Width != 0 && Nwidth * Height > Nheight * Width)
            {
                Nheight = (Nheight * Width) / Nwidth;
                Nwidth = Width;
            }
            else
            {
                Nwidth = (Nwidth * Height) / Nheight;
                Nheight = Height;
            }

            var RO = new ResizeOptions { Size = new Size(Nwidth, Nheight), Sampler = KnownResamplers.Bicubic };
            if (Width * Height < 10000 * 10000)
            {
                RO.Sampler = KnownResamplers.MitchellNetravali;
            }

            using SI.Image Dest = Img.Clone(x => x.Resize(RO));
            Dest.Metadata.HorizontalResolution = Dpi;
            Dest.Metadata.VerticalResolution = Dpi;

            if (Watermark != null)
            {
                using (var Water = SI.Image.Load(Watermark.Path))
                {
                    Water.Mutate(x => x.Opacity((float)Watermark.Transparency));
                    int Y = 0;
                    while (Y < Nheight)
                    {
                        int X = 0;
                        while (X < Nwidth)
                        {
                            Point P = new(X, Y);
                            Dest.Mutate(x => x.DrawImage(Water, P, 1f));
                            X += Watermark.H_Spacing;
                        }
                        Y += Watermark.V_Spacing;
                    }
                }
            }

            System.IO.File.Delete(File);
            var Encoder = new JpegEncoder { Quality = 90 };
            Dest.Save(File, Encoder);
        }

        public void Save(string File_Name)
        {
            Img.Save(File_Name);

        }
        public static Bitmap Load(string File_Name)
        {
            var B = new Bitmap();
            B.Load_Bmp(File_Name);
            return B;
        }

        public static bool Read_Size(string File_Name, out int Width, out int height)
        {
            Width = height = 0;
            try
            {
                var Inf = SI.Image.Identify(File_Name);
                Width = Inf.Width;
                height = Inf.Height;
                return true;
            }
            catch { }
            return false;
        }

        public static Iptc_Reader Read_Metadata(string File_Name) => new Iptc_Reader(File_Name);
        public void Erase_All_Meta()
        {
            Img.Metadata.ExifProfile = null;
            Img.Metadata.IptcProfile = null;
            Img.Metadata.XmpProfile = null;
            Img.Metadata.IccProfile = null;
            Img.Metadata.CicpProfile = null;
        }


        string Get_Iptc(IptcTag T)
        {
            return Get_Mata_Data().IptcProfile?.GetValues(T)?.FirstOrDefault()?.Value;
        }
        void Set_Iptc(IptcTag T, string V)
        {
            if (Get_Mata_Data().IptcProfile == null) Get_Mata_Data().IptcProfile = new();
            if (V?.Length > 0)
            {
                Get_Mata_Data().IptcProfile.SetValue(T, V);
            }
            else
            {
                Get_Mata_Data().IptcProfile.RemoveValue(T);
            }
        }
        public string FFO_Credit
        {
            get => Get_Iptc(IptcTag.Credit);
            set => Set_Iptc(IptcTag.Credit, value);
        }
        public string FFO_Byline
        {
            get => Get_Iptc(IptcTag.Byline);
            set => Set_Iptc(IptcTag.Byline, value);
        }
        public string FFO_Source
        {
            get => Get_Iptc(IptcTag.Source);
            set => Set_Iptc(IptcTag.Source, value);
        }
        public string FFO_OTR
        {
            get => Get_Iptc(IptcTag.OriginalTransmissionReference);
            set => Set_Iptc(IptcTag.OriginalTransmissionReference, value);
        }
        public string FFO_ObjectName
        {
            get => Get_Iptc(IptcTag.Name);
            set => Set_Iptc(IptcTag.Name, value);
        }
        public string FFO_EditStatus
        {
            get => Get_Iptc(IptcTag.EditStatus);
            set => Set_Iptc(IptcTag.EditStatus, value);
        }
        public string FFO_Headline
        {
            get => Get_Iptc(IptcTag.Headline);
            set => Set_Iptc(IptcTag.Headline, value);
        }
        public string FFO_Caption
        {
            get => Get_Iptc(IptcTag.Caption);
            set => Set_Iptc(IptcTag.Caption, value);
        }

        public string FFO_FixtureId
        {
            get => Get_Iptc(IptcTag.FixtureIdentifier);
            set => Set_Iptc(IptcTag.FixtureIdentifier, value);
        }
        public string FFO_City
        {
            get => Get_Iptc(IptcTag.City);
            set => Set_Iptc(IptcTag.City, value);
        }
        public string FFO_CountryName
        {
            get => Get_Iptc(IptcTag.Country);
            set => Set_Iptc(IptcTag.Country, value);
        }
        public string FFO_SpecialInstructions
        {
            get => Get_Iptc(IptcTag.SpecialInstructions);
            set => Set_Iptc(IptcTag.SpecialInstructions, value);
        }
        public string FFO_CopyrightNotice
        {
            get => Get_Iptc(IptcTag.CopyrightNotice);
            set => Set_Iptc(IptcTag.CopyrightNotice, value);
        }

        public string FFO_Category
        {
            get => Get_Iptc(IptcTag.Category);
            set => Set_Iptc(IptcTag.Category, value);
        }

        public string DateCreated
        {
            get => Get_Iptc(IptcTag.CreatedDate);
            set => Set_Iptc(IptcTag.CreatedDate, value);
        }

        public string ReleaseDate
        {
            get => Get_Iptc(IptcTag.ReleaseDate);
            set => Set_Iptc(IptcTag.ReleaseDate, value);
        }

        public string[] FFO_Categories
        {
            get => Get_Iptc(IptcTag.SupplementalCategories).Split('/');
            set => Set_Iptc(IptcTag.SupplementalCategories, value.Assembled_With("/"));
        }

        public string[] FFO_Keywords
        {
            get => Get_Iptc(IptcTag.Keywords).Split('/');
            set => Set_Iptc(IptcTag.Keywords, value.Assembled_With("/"));
        }

        static DateTime From_String(string V)
        {
            char[] Date = JR.Time.Null_Time.ToString("yyyyMMdd").ToCharArray();
            int Idx = 0;
            foreach (char C in V)
            {
                if (char.IsDigit(C))
                {
                    Date[Idx] = C;
                    Idx++;
                }
                if (Idx >= Date.Length) break;
            }
            try
            {
                return DateTime.ParseExact(new string(Date), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
            }
            catch
            {
            }
            return JR.Time.Null_Time;
        }
        static string To_String(DateTime D)
        {
            if (D == JR.Time.Null_Time) return null;
            return D.ToString("yyyy-MM-dd");
        }
        public DateTime FFO_ReleaseDate
        {
            get => From_String(ReleaseDate);
            set
            {
                ReleaseDate = To_String(value);
            }
        }


        public DateTime FFO_DateCreated
        {
            get => From_String(DateCreated);
            set
            {
                DateCreated = To_String(value);
            }
        }

        public string All_Keywords(char Separator)
        {
            return Get_Iptc(IptcTag.Keywords).Replace('/', Separator);
        }

    }
}
