using JR.Saf;
using System.Diagnostics;
using System.Text;

namespace JR.Images
{

    public class Iptc_Manager : IDisposable
    {
        string Pic_File = "";
        bool Temp_File = false;
        public Iptc_Manager(string File)
        {
            if (!System.IO.File.Exists(File)) return;
            Pic_File = File;
        }

        public Iptc_Manager(byte[] Buffer, int Size)
        {
            if (Buffer == null || Buffer.Length < 10) return;
            try
            {
                var File_Name = Path.GetTempFileName();
                File.WriteAllBytes(File_Name, Buffer);
                Temp_File = true;
                Pic_File = File_Name;
            }
            catch { }
        }

        public bool Valid => Pic_File.Length > 0;

        static Ref_Parameter Exiv2_Path = new Ref_Parameter("exiv2.path", @"c:\tools\progs\exiv2.exe");
        string Run_Exiv2(string Args, Action<Process> Prepare = null, Action<string, string> Complete = null)
        {
            try
            {
                using (var P = new Process())
                {
                    P.StartInfo.UseShellExecute = false;
                    P.StartInfo.FileName = Exiv2_Path.Value;
                    P.StartInfo.CreateNoWindow = true;
                    P.StartInfo.Arguments = Args;
                    if (Prepare != null) Prepare(P);
                    P.StartInfo.RedirectStandardOutput = true;
                    P.StartInfo.RedirectStandardError = true;
                    P.StartInfo.StandardOutputEncoding = Encoding.Latin1;
                    P.Start();
                    var Out = P.StandardOutput.ReadToEnd();
                    var Err = P.StandardError.ReadToEnd();
                    P.WaitForExit(10000);
                    if (Complete != null) Complete(Out, Err);
                    return Out;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        void Apply_Actions()
        {
            if (Clear_All)
            {
                Run_Exiv2("-da " + Pic_File);
            }
            {
                var Action_File = Path.GetTempFileName();
                JR.Files.Safe_Write(Action_File, Actions.ToArray());
                Run_Exiv2("-m " + Action_File + " " + Pic_File);
                JR.Files.Safe_Delete(Action_File);
            }
        }

        IEnumerable<string> List_Tags()
        {
            var Lines = Run_Exiv2("-PXIkycv " + Pic_File);
            return Lines.Split(new[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);
        }


        List<string> Actions = new List<string>();
        bool Clear_All = false;
        public void Save(string Dest = "")
        {
            if (!Valid) return;
            if (Dest != "")
            {
                if (!JR.Files.Safe_Copy(Pic_File, Dest)) return;
                Pic_File = Dest;
            }
            Apply_Actions();
        }

        public byte[] Read_Data()
        {
            if (!Valid) return null;
            return File.ReadAllBytes(Pic_File);
        }

        public int Remove_Meta(string Key)
        {
            Actions.Add($"del {Key}");
            return 1;
        }

        // Retourner false pour interrompre l'enumeration
        bool Enum_Data(string Key, string Value)
        {
            //Console.WriteLine("{0}={1}", Key, Value);
            if (Value == null || Value == "" || Value.Trim() == "") return true;
            string Val = Value;
            string K = Key.ToUpper();
            if (_Dic.ContainsKey(K))
            {
                if (_Dic[K] == Val) return true; // false;
                _Dic[K] += ("/" + Val);
            }
            else
            {
                _Dic[K] = Val;
            }
            return true;
        }
        Dictionary<string, string> _Dic;
        //static Encoding E_Iso = Encoding.GetEncoding("iso-8859-1");
        static Encoding E_Utf = Encoding.UTF8;
        public Dictionary<string, string> Dic
        {
            get
            {
                if (_Dic == null)
                {
                    _Dic = new Dictionary<string, string>();
                    {
                        var SB = new StringBuilder();
                        string Key = "";
                        bool Is_Xmp = false;
                        void Push()
                        {
                            if (Key != "")
                            {
                                Enum_Data(Key, SB.ToString());
                                Key = "";
                            }
                            SB.Clear();
                            Is_Xmp = false;
                        }
                        void Append_Iptc(string S)
                        {
                            Is_Xmp = false;
                            Key = S.Substring(0, 45).Trim();
                            var Type = S.Substring(45, 10).Trim().ToLower();
                            var Val = S.Substring(60);
                            switch (Type)
                            {
                                case "date":
                                    Val = Val.To_Letters();
                                    break;
                            }
                            SB.Append(Val);

                        }
                        void Append_Xmp_String(string S) =>
                            SB.Append(Encoding.UTF8.GetString(Encoding.Latin1.GetBytes(S)));
                        void Append_Xmp(string S)
                        {
                            Is_Xmp = true;
                            Key = S.Substring(0, 45).Trim();
                            var Type = S.Substring(45, 10).Trim().ToLower();
                            var Val = S.Substring(60);
                            switch (Type)
                            {
                                case "langalt":
                                    {
                                        var C = Val.Scan();
                                        C.Next("\"");
                                        C.Next("\"");
                                        C.Next(" ");
                                        Val = C.Next_Word("lang=");
                                    }
                                    break;
                                case "xmpbag":
                                    Val = Val.Replace(", ", "/");
                                    break;
                            }
                            Append_Xmp_String(Val);
                        }
                        foreach (var S in List_Tags())
                        {
                            if (S.StartsWith("Iptc."))
                            {
                                Push();
                                if (S.Length < 60) continue;
                                Append_Iptc(S);
                            }
                            else if (S.StartsWith("Xmp.dc."))
                            {
                                Push();
                                if (S.Length < 60) continue;
                                Append_Xmp(S);

                            }
                            else if (Key != "" && !S.StartsWith("Xmp."))
                            {
                                SB.AppendLine();
                                if (Is_Xmp)
                                {
                                    Append_Xmp_String(S);
                                }
                                else
                                {
                                    SB.Append(S);
                                }
                            }
                        }
                        Push();
                    }

                    if (Auto_Translate)
                    {
                        string CharacterSet = "";
                        if (Dic.TryGetValue("IPTC.ENVELOPE.CHARACTERSET", out CharacterSet))
                        {
                            Func<string, string> Translator = null;
                            if (CharacterSet.StartsWith("27 37"))
                            {
                                Translator = (Src) =>
                                {
                                    if (Src == null || Src == "") return Src;
                                    byte[] B = Encoding.Default.GetBytes(Src);
                                    return E_Utf.GetString(B);
                                };
                            }
                            if (Translator != null)
                            {
                                var Keys = _Dic.Keys.Where((K) => K.StartsWith("IPTC")).ToArray();
                                foreach (var Key in Keys)
                                {
                                    _Dic[Key] = Translator(_Dic[Key]);
                                }
                                Translated = true;
                            }

                        }
                    }
                }
                return _Dic;
            }
        }

        public string this[string Key]
        {
            get
            {
                string Val = "";
                if (!Dic.TryGetValue(Key.ToUpper(), out Val)) return "";
                return Val;
            }
            set
            {
                Remove_Meta(Key);
                if (value != "") Modify_Meta(Key, value);
            }
        }

        //public int Read_Meta(string Key, string Buff, int BuffSize)
        //{
        //    if (!Valid) return 0;
        //    return Exif.ReadMeta(Id, Key, Buff, BuffSize);
        //}

        public int Add_Meta(string Key, string Val)
        {
            Actions.Add($"add {Key} {Val}");
            return 1;
        }

        public int Modify_Meta(string Key, string Val)
        {
            Actions.Add($"set {Key} {Val}");
            return 1;
        }

        public int Erase_All_Meta()
        {
            Clear_All = true;
            return 1;
        }

        #region IDisposable Membres

        public void Dispose()
        {
            if (!Valid) return;
            if (Temp_File) JR.Files.Safe_Delete(Pic_File);
            Pic_File = "";
        }

        #endregion

        public string FFO_Credit
        {
            get { return this["Iptc.Application2.Credit"]; }
            set { this["Iptc.Application2.Credit"] = value; }
        }
        public string FFO_Author
        {
            get { return this["Iptc.Application2.Author"]; }
        }
        public string FFO_Byline
        {
            get { return this["Iptc.Application2.Byline"]; }
            set { this["Iptc.Application2.Byline"] = value; }
        }
        public string FFO_Source
        {
            get { return this["Iptc.Application2.Source"]; }
            set { this["Iptc.Application2.Source"] = value; }
        }
        public string FFO_OTR
        {
            get { return this["Iptc.Application2.TransmissionReference"]; }
            set { this["Iptc.Application2.TransmissionReference"] = value; }
        }
        public string FFO_ObjectName
        {
            get { return this["Iptc.Application2.ObjectName"]; }
            set { this["Iptc.Application2.ObjectName"] = value; }
        }
        public string FFO_EditStatus
        {
            get { return this["Iptc.Application2.EditStatus"]; }
            set { this["Iptc.Application2.EditStatus"] = value; }
        }
        public string FFO_Headline
        {
            get { return this["Iptc.Application2.Headline"]; }
            set { this["Iptc.Application2.Headline"] = value; }
        }
        public string FFO_Caption
        {
            get { return this["Iptc.Application2.Caption"]; }
            set { this["Iptc.Application2.Caption"] = value; }
        }

        public string FFO_FixtureId
        {
            get { return this["Iptc.Application2.FixtureId"]; }
            set { this["Iptc.Application2.FixtureId"] = value; }
        }
        public string FFO_City
        {
            get { return this["Iptc.Application2.City"]; }
            set { this["Iptc.Application2.City"] = value; }
        }
        public string FFO_CountryName
        {
            get { return this["Iptc.Application2.CountryName"]; }
            set { this["Iptc.Application2.CountryName"] = value; }
        }
        public string FFO_SpecialInstructions
        {
            get { return this["Iptc.Application2.SpecialInstructions"]; }
            set { this["Iptc.Application2.SpecialInstructions"] = value; }
        }
        public string FFO_CopyrightNotice
        {
            get { return this["Iptc.Application2.Copyright"]; }
            set { this["Iptc.Application2.Copyright"] = value; }
        }

        public string FFO_Category
        {
            get { return this["Iptc.Application2.Category"]; }
            set { this["Iptc.Application2.Category"] = value; }
        }

        public string DateCreated
        {
            get { return this["Iptc.Application2.DateCreated"]; }
            set { this["Iptc.Application2.DateCreated"] = value; }
        }

        public string ReleaseDate
        {
            get { return this["Iptc.Application2.ReleaseDate"]; }
            set { this["Iptc.Application2.ReleaseDate"] = value; }
        }

        public string[] FFO_Categories
        {
            get { return this["Iptc.Application2.SuppCategory"].Split('/'); }
            set
            {
                Remove_Meta("Iptc.Application2.SuppCategory");
                foreach (string Val in value)
                {
                    Add_Meta("Iptc.Application2.SuppCategory", Val);
                }
            }
        }

        public string[] FFO_Keywords
        {
            get { return this["Iptc.Application2.Keywords"].Split('/'); }
            set
            {
                if (!Valid) return;
                Remove_Meta("Iptc.Application2.Keywords");
                foreach (string Val in value)
                {
                    string V = Val.Trim();
                    if (V.Length == 0) continue;
                    Add_Meta("Iptc.Application2.Keywords", V);
                }
            }
        }

        public DateTime FFO_ReleaseDate
        {
            get
            {
                char[] Date = JR.Time.Null_Time.ToString("yyyyMMdd").ToCharArray();
                int Idx = 0;
                foreach (char C in this["Iptc.Application2.ReleaseDate"])
                {
                    if (char.IsDigit(C))
                    {
                        Date[Idx] = C;
                        Idx++;
                    }
                    if (Idx >= Date.Length) break;
                }
                try
                {
                    return DateTime.ParseExact(new string(Date), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                }
                catch
                {
                }
                return JR.Time.Null_Time;
            }
            set
            {
                if (!Valid) return;
                Remove_Meta("Iptc.Application2.ReleaseDate");
                if (value == JR.Time.Null_Time) return;
                Modify_Meta("Iptc.Application2.ReleaseDate", value.ToString("yyyy-MM-dd"));
            }
        }


        public DateTime FFO_DateCreated
        {
            get
            {
                char[] Date = JR.Time.Null_Time.ToString("yyyyMMdd").ToCharArray();
                int Idx = 0;
                foreach (char C in this["Iptc.Application2.DateCreated"])
                {
                    if (char.IsDigit(C))
                    {
                        Date[Idx] = C;
                        Idx++;
                    }
                    if (Idx >= Date.Length) break;
                }
                try
                {
                    return DateTime.ParseExact(new string(Date), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                }
                catch
                {
                }
                return JR.Time.Null_Time;
            }
            set
            {
                if (!Valid) return;
                Remove_Meta("Iptc.Application2.DateCreated");
                if (value == JR.Time.Null_Time) return;
                Modify_Meta("Iptc.Application2.DateCreated", value.ToString("yyyy-MM-dd"));
            }
        }

        public string All_Keywords(char Separator)
        {
            return this["Iptc.Application2.Keywords"].Replace('/', Separator);
        }

        //public int Width
        //{
        //    get
        //    {
        //        if (!Valid) return 0;
        //        return Exif.ImageWidth(Id);
        //    }
        //}
        //public int Height
        //{
        //    get
        //    {
        //        if (!Valid) return 0;
        //        return Exif.ImageHeight(Id);
        //    }
        //}

        public bool Auto_Translate { get; set; } = false;
        public bool Translated { get; set; } = false;
    }

}