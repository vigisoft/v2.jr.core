using WicNet;

namespace JR.Images
{
    public class Iptc_Reader
    {
        public enum Mode { None = 0, Classic = 1, All = 2 };
        public Iptc_Reader(string File, Mode Mode = Mode.Classic)
        {
            Valid = false;
            try
            {
                using var S = new FileStream(File, FileMode.Open, FileAccess.Read);  // Passer par un stream pour forcer la cloture du fichier sans attendre le GC
                using var Decoder = WicBitmapDecoder.Load(S);
                using var Frame = Decoder.GetFrame(0);
                Width = Frame.Width;
                Height = Frame.Height;
                Dic["file"] = JR.Files.Get_File_Name(File);
                Dic["width"] = Width.ToString();
                Dic["height"] = Height.ToString();
                Valid = true;
                if (Mode == Mode.None) return;
                using var M = Frame.GetMetadataReader();
                if (Mode != Mode.None && M != null)
                {
                    bool Classic_Mode = Mode == Mode.Classic;
                    void Extract(WicMetadataQueryReader Metadata, string Query)
                    {
                        foreach (var relativeQuery in Metadata.Enumerate())
                        {
                            string fullQuery = Query + relativeQuery.Key.Key;
                            using var Reader = relativeQuery.Value as WicMetadataQueryReader;
                            if (Reader != null)
                            {
                                Extract(Reader, fullQuery);
                            }
                            else
                            {
                                var Key = fullQuery;
                                void Set_Value(string Value)
                                {
                                    Value = Value.Trim();
                                    if (Value.Length == 0) return;
                                    Dic[Key] = Value;
                                }
                                if (fullQuery.StartsWith("/app13/"))
                                {
                                    if (Classic_Mode)
                                    {
                                        var C = relativeQuery.Key.Key.Scan();
                                        C.Next_Word("str=");
                                        if (C.End) continue;
                                        Key = C.Next("}").Replace(" ", "").ToUpper();
                                        // Normaliser les noms
                                        switch (Key)
                                        {
                                            case "ORIGINATINGPROGRAM":
                                                Key = "PROGRAM";
                                                break;
                                            case "ORIGINALTRANSMISSIONREFERENCE":
                                                Key = "TRANSMISSIONREFERENCE";
                                                break;
                                            case "SUPPLEMENTALCATEGORY":
                                                Key = "SUPPCATEGORY";
                                                break;
                                            case "COUNTRY/PRIMARYLOCATIONNAME":
                                                Key = "COUNTRYNAME";
                                                break;
                                            case "COPYRIGHTNOTICE":
                                                Key = "COPYRIGHT";
                                                break;
                                            case "BY-LINE":
                                                Key = "BYLINE";
                                                break;
                                            case "BY-LINETITLE":
                                                Key = "BYLINETITLE";
                                                break;
                                            case "WRITER/EDITOR":
                                                Key = "WRITER";
                                                break;
                                            case "CODEDCHARACTERSET":
                                                Key = "CHARACTERSET";
                                                break;
                                        }
                                    }
                                    if (relativeQuery.Value is byte[])
                                    {
                                        Set_Value(JR.Strings.Assembled<byte>('/', (byte[]?)relativeQuery.Value));
                                    }
                                    else
                                        if (relativeQuery.Value is string[])
                                    {
                                        Set_Value(JR.Strings.Assembled<string>('/', (string[])relativeQuery.Value));
                                    }
                                    else if (relativeQuery.Value != null)
                                        Set_Value(relativeQuery.Value.ToString());
                                }
                                else if (fullQuery.StartsWith("/xmp/dc:") && relativeQuery.Value is string)
                                {
                                    if (Classic_Mode)
                                    {
                                        Key = ("xmp." + Query.Replace("/xmp/dc:", "")).ToUpper();
                                    }
                                    var T = relativeQuery.Value.ToString().Trim();
                                    if (Dic.TryGetValue(Key, out var V))
                                    {
                                        Set_Value(V + "/" + T);
                                    }
                                    else
                                    {
                                        Set_Value(T);
                                    }
                                }
                            }
                        }
                    };
                    Extract(M, "");
                }
            }
            catch
            {
            }

        }

        public bool Valid { get; set; }

        public Dictionary<string, string> Dic { get; set; } = new();

        public string this[string Key]
        {
            get
            {
                if (!Dic.TryGetValue(Key.ToUpper(), out var Val)) return "";
                return Val;
            }
        }

        public string Get_Dic_Value(params string[] Keys)
        {
            foreach (var K in Keys)
            {
                if (Dic.TryGetValue(K.ToUpper(), out var Val)) return Val;
            }
            return "";
        }
        public string Get_Filtered_Dic_Value(Func<string, string, bool> Is_Ok, params string[] Keys)
        {
            foreach (var K in Keys)
            {
                if (Dic.TryGetValue(K.ToUpper(), out var Val) && Is_Ok(K, Val)) return Val;
            }
            return "";
        }

        public string FFO_Credit => this["CREDIT"];

        public string FFO_Byline => this["BYLINE"];
        public string FFO_Source => this["SOURCE"];
        public string FFO_Program => this["PROGRAM"];
        public string FFO_Contact => this["CONTACT"];
        public string FFO_OTR => this["TRANSMISSIONREFERENCE"];
        public string FFO_ObjectName => this["OBJECTNAME"];
        public string FFO_Headline => this["HEADLINE"];
        public string FFO_Caption => this["CAPTION"];

        public string FFO_FixtureId => this["FIXTUREID"];
        public string FFO_City => this["CITY"];
        public string FFO_CountryName => this["COUNTRYNAME"];
        public string FFO_SpecialInstructions => this["SPECIALINSTRUCTIONS"];
        public string FFO_CopyrightNotice => this["COPYRIGHT"];

        public string FFO_Category => this["CATEGORY"];

        public string DateCreated => this["DATECREATED"];

        public string[] FFO_Categories => this["SUPPLEMENTALCATEGORY"].Split('/');

        public string[] FFO_Keywords => this["KEYWORDS"].Split('/');

        public DateTime FFO_DateCreated
        {
            get
            {
                char[] Date = JR.Time.Null_Time.ToString("yyyyMMdd").ToCharArray();
                int Idx = 0;
                foreach (char C in DateCreated)
                {
                    if (char.IsDigit(C))
                    {
                        Date[Idx] = C;
                        Idx++;
                    }
                    if (Idx >= Date.Length) break;
                }
                try
                {
                    return DateTime.ParseExact(new string(Date), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                }
                catch
                {
                }
                return JR.Time.Null_Time;
            }
        }

        public string All_Keywords(char Separator) => this["KEYWORDS"].Replace('/', Separator);

        public int Width { get; private set; } = 0;
        public int Height { get; private set; } = 0;
    }
}