﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Net;
using System.IO.Compression;
using MaxMind.GeoIP2;
using JR.Saf;
using JR.DB;
using System.Text;

namespace JR.GEO
{
    /// <summary>
    /// Classe de gestion de la geolocalisation utilise la base de MaxMind (GEOLITE2)
    /// </summary>

    public class Session_Geo
    {

        //public static void Update_Geo_Db()
        //{
        //    string Db_File = Main.File_Parameter("GEOLITE2.DB");

        //    // Si la date de la dernière base est de + d'une semaine la retélécharger
        //    FileInfo FI = new FileInfo(Db_File);
        //    if (FI.Exists)
        //    {
        //        if (FI.LastWriteTimeUtc > DateTime.UtcNow.AddDays(-7)) return;
        //    }

        //    var Db = new WebClient().DownloadData(Main.Parameter("GEOLITE2.URL"));
        //    //http://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.mmdb.gz
        //    using (MemoryStream MS = new MemoryStream(Db))
        //    {
        //        using (GZipStream Decompress = new GZipStream(MS, CompressionMode.Decompress))
        //        {
        //            using (FileStream Out_File = File.Create(Db_File))
        //            {
        //                Decompress.CopyTo(Out_File);
        //            }
        //        }
        //    }
        //}


        public static void Update_Geo_Db()
        {
            string Db_File = Main.File_Parameter("GEOLITE2.DB");

            // Si la date de la dernière base est de + d'une semaine la retélécharger
            FileInfo FI = new FileInfo(Db_File);
            if (FI.Exists)
            {
                if (FI.LastWriteTimeUtc > DateTime.UtcNow.AddDays(-7)) return;
            }

            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var Db = new WebClient().DownloadData(Main.Parameter("GEOLITE2.URL"));
                // https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-Country&license_key=n13D9YkzSZieMgUm&suffix=tar.gz
                // Depuis dec 2019, geolite fournit un tar.gz avec 3 fichiers + impose un login + une license
                //  avant : http://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.mmdb.gz
                using (MemoryStream MS = new MemoryStream(Db))
                {
                    using (GZipStream Decompress = new GZipStream(MS, CompressionMode.Decompress))
                    {
                        using (MemoryStream stream = new MemoryStream())
                        {
                            Decompress.CopyTo(stream);
                            stream.Position = 0;

                            // Algo de décompression tar simple et portable !
                            var buffer = new byte[100];
                            while (true)
                            {
                                stream.Read(buffer, 0, 100);
                                var name = Encoding.ASCII.GetString(buffer).Trim('\0');
                                if (String.IsNullOrWhiteSpace(name)) break;
                                stream.Seek(24, SeekOrigin.Current);
                                stream.Read(buffer, 0, 12);
                                var size = System.Convert.ToInt64(Encoding.UTF8.GetString(buffer, 0, 12).Trim('\0').Trim(), 8);
                                stream.Seek(376L, SeekOrigin.Current);
                                if (!name.EndsWith("/"))
                                {
                                    var buf = new byte[size];
                                    stream.Read(buf, 0, buf.Length);
                                    if (name.EndsWith(".mmdb"))
                                    {
                                        JR.Files.Check_Directory(Db_File);
                                        System.IO.File.WriteAllBytes(Db_File, buf);
                                        break;
                                    }
                                }
                                var pos = stream.Position;
                                var offset = 512 - (pos % 512);
                                if (offset == 512) offset = 0;
                                stream.Seek(offset, SeekOrigin.Current);
                            }
                        }
                    }
                }
            }
            catch (Exception E)
            {
                if (FI.Exists) FI.LastWriteTimeUtc = DateTime.UtcNow;
                JR.Saf.Logs.Except(E);
            }
        }

        static void Update_Geo_Infos(string Filter)
        {
            string Db_File = Main.File_Parameter("GEOLITE2.DB");
            using (var Geo_Db = new DatabaseReader(Db_File))
            {
                using (var DT = App_Db.Execute_Table("select distinct ip from Log_Session where country is null" + Filter))
                {
                    foreach (DataRow DR in DT.Rows)
                    {
                        string IP = DR.Value<string>(0);
                        string Country_Code = "?";
                        string Continent_Code = "?";
                        try
                        {
                            var Country = Geo_Db.Country(IP);
                            if (Country.Country.IsoCode != null)
                            {
                                Country_Code = Country.Country.IsoCode;
                                Continent_Code = Country.Continent.Code;
                            }
                        }
                        catch
                        {
                        }
                        App_Db.Execute("update Log_Session set Country=@1, Continent=@2 where IP=@0 and Country is null", IP, Country_Code, Continent_Code);
                    }
                }
            }
        }

        static bool Is_Local_IP(string IP)
        {
            switch (IP)
            {
                case "::1":
                case "127.0.0.1":
                    return true;
            }
            return false;
        }

        public static void Update_Geo_Infos(string IP, long Session_Id)
        {
            string Db_File = Main.File_Parameter("GEOLITE2.DB");
            using (var Geo_Db = new DatabaseReader(Db_File))
            {
                string Country_Code = "?";
                string Continent_Code = "?";
                if (!Is_Local_IP(IP))
                {
                    try
                    {
                        var Country = Geo_Db.Country(IP);
                        if (Country.Country.IsoCode != null)
                        {
                            Country_Code = Country.Country.IsoCode;
                            Continent_Code = Country.Continent.Code;
                        }
                    }
                    catch
                    {
                    }
                }
                App_Db.Execute("update Log_Session set Country=@1, Continent=@2 where id=@0", Session_Id, Country_Code, Continent_Code);
            }
        }

        public static bool Read_Country(string IP, out string Name, out string Iso_Code)
        {
            if (Is_Local_IP(IP))
            {
                Name = "locahost";
                Iso_Code = "FR";
                return true;
            }
            try
            {
                string Db_File = Main.File_Parameter("GEOLITE2.DB");
                using (var Geo_Db = new DatabaseReader(Db_File))
                {
                    var Country = Geo_Db.Country(IP);
                    if (Country.Country.IsoCode != null)
                    {
                        Iso_Code = Country.Country.IsoCode;
                        Name = Country.Country.Name;
                        return true;
                    }
                }
            }
            catch
            {
            }
            Name = "?";
            Iso_Code = "?";
            return false;
        }

        public static void Update(DateTime? From = null, string Part = "Y")
        {

            // Compter les infos à mettre à jour
            var SB = new StringBuilder();
            if (From != null)
            {
                SB.AppendFormat(" and Year(Date)={0}", ((DateTime)From).Year);
                switch (Part)
                {
                    case "M":
                        SB.AppendFormat(" and Month(Date)={0}", ((DateTime)From).Month);
                        break;
                    case "D":
                        SB.AppendFormat(" and Month(Date)={0} and Day(Date)={1}", ((DateTime)From).Month, ((DateTime)From).Day);
                        break;
                }
            }
            int Count = App_Db.Execute_Int("select count(*) from Log_Session where Country is null" + SB.ToString());
            if (Count == 0) return;

            // Mettre à jour la base
            Update_Geo_Db();

            // Mettre à jour les enregistrements
            Update_Geo_Infos(SB.ToString());
        }


        const string Stat_Req_By_Prod = @"
with Page_List(page, Country, Continent, Ip, Agent) as
(
    SELECT Page, Country, Continent, S.IP, S.Agent
    FROM LOG_Page P
     inner join Log_Session S on P.Id_Session=S.Id 
    where page like @1
     and Year(S.Date) = Year(@0) and Month(S.Date) = Month(@0)
     group by page,S.Continent, S.Country, S.IP, S.AGENT
) 
SELECT Page, count(*), Country, Continent, IP
FROM Page_List
 group by page,Continent,Country,IP
  order by 4,3
";

        const string Stat_Visit = @"
SELECT Page, 1, Country, Continent, S.IP
FROM LOG_Page P
 inner join Log_Session S on P.Id_Session=S.Id 
where page like @1
 and Year(S.Date) = Year(@0) and Month(S.Date) = Month(@0)
 group by page,S.Continent, S.Country, S.IP
 order by 4,3,5;
";


        public static List<Page_Stat> Freq(DateTime Month, string Page_Filter = "%/product%prod=%")
        {
            var L = new List<Page_Stat>();
            using (App_Db_Reader DR = new App_Db_Reader(Stat_Req_By_Prod, Month, Page_Filter))
            {
                while (DR.Read())
                {
                    L.Add(new Page_Stat { Page = DR.Get_String(0), Count = DR.Get_Int(1), Country = DR.Get_String(2), Continent = DR.Get_String(3), Ip = DR.Get_String(4).ToLower() });
                }
            }
            return L;
        }

        public class Page_Stat
        {
            public string Page;
            public int Count;
            public string Country;
            public string Continent;
            public string Ip;
        }

        public static string Continent_Name(string Code)
        {
            switch (Code.ToUpper())
            {
                case "AF": return "Afrique";
                case "AN": return "Antarctique";
                case "AS": return "Asie";
                case "NA": return "Amérique du nord";
                case "SA": return "Amérique du sud";
                case "EU": return "Europe";
                case "OC": return "Océanie";
            }
            return "?";
        }

        public static string Country_Name(string Code)
        {
            try
            {
                return new RegionInfo(Code).DisplayName;
            }
            catch { }
            return "?";
        }
    }

}
