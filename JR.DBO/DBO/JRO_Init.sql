﻿USE [JRO_DB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Retourne la version du schéma de base de données
CREATE FUNCTION [O$Software_Version] ()
RETURNS int
AS
begin
return 1;
end
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [OBJ_Cleaner](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Id_Property] [varchar](20) NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_OBJ_Cleaner] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [OBJ](
	[Id] [uniqueidentifier] NOT NULL,
	[Alias] [varchar](40) NULL,
	[Name] [varchar](max) NULL,
	[Id_Parent] [uniqueidentifier] NULL,
	[Id_Class] [uniqueidentifier] NULL,
	[Tick] [bigint] NULL,
	[Build] [bigint] NULL,
	[Layer] [int] NULL,
	[Protection] [char](1) NULL,
	[State] [varchar](4) NULL,
	[Flags] [varchar](50) NULL,
 CONSTRAINT [PK_OBJ] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_OBJECT] ON [OBJ] 
(
	[Alias] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CMD](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Command] [varchar](max) NULL,
	[Issuer] [varchar](40) NULL,
	[Status] [char](1) NULL,
	[Response] [varchar](max) NULL,
 CONSTRAINT [PK_Command_Queue] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [APP](
	[Id] [int] NOT NULL,
	[Build] [bigint] NULL,
	[Cycle_Tick] [bigint] NULL,
	[Date] [datetime] NULL,
	[Ready] [bit] NULL,
	[Started] [bit] NULL,
	[Locked] [bit] NULL,
	[Next_Journal_Id] [bigint] NULL,
	[Cycle_Time] [datetime] NULL,
	[Version] [int] NULL,
 CONSTRAINT [PK_APP] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [O$Flag]
	(
	@Flags varchar(50),
	@Id_Flag varchar(10)
	)
RETURNS varchar(50)
AS
begin
	if (@Flags is null) return '';
	DECLARE @Fl varchar(51);
	set @FL = @Flags+';';
	DECLARE @Start int;
	DECLARE @Len int;
	set @Start = CHARINDEX (';' + @Id_Flag + '=', @Fl);
	if (@Start <1 ) return '';
	set @Start = 1+ CHARINDEX ('=', @Fl, @Start+1);
	set @Len = CHARINDEX (';', @Fl, @Start) - @Start;
	return SUBSTRING (@Fl, @Start, @Len);
end
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Retourne la version de configuration courante
CREATE FUNCTION [O$App_Tick] ()
RETURNS bigint
AS
	BEGIN
	DECLARE @Retour bigint;
	SET @Retour = 0;
	SELECT @Retour=Cycle_Tick FROM APP
	RETURN @Retour
	END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Retourne la version de configuration courante
CREATE FUNCTION [O$App_Build] ()
RETURNS bigint
AS
	BEGIN
	DECLARE @Retour bigint;
	SET @Retour = 0;
	SELECT @Retour=Build FROM APP
	RETURN @Retour
	END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [O$Alias_Id]
	(
		@Alias varchar(50)
	)
RETURNS uniqueidentifier
AS
	BEGIN
	DECLARE @Retour uniqueidentifier
	SET @Retour = null;
	SELECT @Retour=Id FROM OBJ WHERE (Alias = @Alias)
	RETURN @Retour/* value */
	END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [OBJ_Resource](
	[Id_Object] [uniqueidentifier] NOT NULL,
	[Id_Property] [varchar](20) NOT NULL,
	[Value] [varbinary](max) NULL,
	[Dim1] [int] NULL,
	[Dim2] [int] NULL,
	[Source] [nvarchar](max) NULL,
	[Info] [nvarchar](max) NULL,
	[Version] [datetime] NULL,
 CONSTRAINT [PK_OBJ_Resource] PRIMARY KEY CLUSTERED 
(
	[Id_Object] ASC,
	[Id_Property] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [OBJ_Relation](
	[Id_Object] [uniqueidentifier] NOT NULL,
	[Id_Property] [varchar](20) NOT NULL,
	[Value] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_OBJ_Relation] PRIMARY KEY CLUSTERED 
(
	[Id_Object] ASC,
	[Id_Property] ASC,
	[Value] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [OBJ_Property](
	[Id_Object] [uniqueidentifier] NOT NULL,
	[Id_Property] [varchar](20) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_OBJ_Property_1] PRIMARY KEY CLUSTERED 
(
	[Id_Object] ASC,
	[Id_Property] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [O$Parent]
	(
	@Id uniqueidentifier
	)
RETURNS uniqueidentifier
AS
	BEGIN
	DECLARE @Retour uniqueidentifier
	SELECT @Retour=Id_Parent FROM OBJ WHERE (Id=@Id)
	RETURN @Retour/* value */
	END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [O$Name]
	(
	@Id uniqueidentifier
	)
RETURNS varchar(MAX)
AS
	BEGIN
	DECLARE @Name varchar(MAX);
	Set @Name = '';
	  if (@Id is not null)
	  begin
		SELECT @Name=Name FROM OBJ WHERE (Id=@Id)
	  end
	RETURN @Name
	END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [O$Layer]
	(
		@Id uniqueidentifier
	)
RETURNS int
AS
	BEGIN
	DECLARE @Retour int
	SET @Retour = 0;
	SELECT @Retour=Layer FROM OBJ WHERE (Id = @Id)
	RETURN @Retour/* value */
	END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [O$Is_Child]
	(
	@Id_Object uniqueidentifier,
	@Id_Parent uniqueidentifier
	)
RETURNS int
AS
begin
	if (@Id_Parent is null) return 1;
	if (@Id_Parent = '') return 1;
	if (@Id_Object is null) return 0;

	declare @Parent uniqueidentifier;
	set @Parent = null;
	select @Parent=Id_Parent from OBJ where  Id=@Id_Object

	while ((@Parent Is not Null) and (@Parent <> @Id_Parent))
	begin
		select @Parent=Id_Parent from OBJ where  Id=@Parent
	end;

    if (@Parent = @Id_Parent) return 1;
    return 0;

end
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [O$TH_App]
AS
  update APP set Cycle_Tick=Cycle_Tick+1, Cycle_Time=GETUTCDATE();
  select Cycle_Tick as Tick from APP;
RETURN
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [O$State]
	(
	@Id uniqueidentifier
	)
RETURNS varchar(4)
AS
	BEGIN
	DECLARE @State varchar(4) = '';
	 if (@Id is not null)
	  begin
		SELECT @State=[State] FROM OBJ WHERE (Id=@Id)
	  end
	RETURN @State
	END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [O$Has_Flag]
	(
	@Id_Object uniqueidentifier,
	@Id_Flag varchar (10)
	)
RETURNS int
AS
begin
	if (@Id_Object is null) return 0;
	DECLARE @Flags varchar(50);
	Set @Flags = null;
	SELECT @Flags=Flags FROM OBJ WHERE (Id=@Id_Object)
	if (@Flags is null) return 0;
	return CHARINDEX (';' + @Id_Flag + '=', @Flags);
end
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [O$Full_Name]
	(
		@Id uniqueidentifier,
		@Id_Folder uniqueidentifier
	)

RETURNS nvarchar(MAX)
BEGIN

	declare @Full_Name varchar (MAX);
	declare @Id_Parent uniqueidentifier;
	set @Full_Name = '';
	set @Id_Parent = null;
	select @Full_Name = [Name], @Id_Parent=Id_Parent from OBJ where  Id=@Id

	while ((@Id_Parent Is not Null) and (@Id_Parent <> @Id_Folder))
	begin
		select @Full_Name = [Name] + '.' + @Full_Name, @Id_Parent=Id_Parent from OBJ where  Id=@Id_Parent

	end;

    return @Full_Name

END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [O$WR_App_Tick]
	(
		@Tick bigint
	)
AS
  update APP set Cycle_Tick=@Tick, Cycle_Time=GETUTCDATE();

RETURN
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [O$Tick]
	(
	@Id uniqueidentifier
	)
RETURNS bigint
AS
	BEGIN
	DECLARE @Tick bigint;
	SELECT @Tick=Tick FROM OBJ WHERE (Id=@Id)
	RETURN @Tick;
	END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [On_Modif]
   ON  [OBJ_Property]
   AFTER DELETE,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    insert into Obj_Cleaner select Id_Property, Value from deleted where LEFT(Id_Property,1) = '!' and Value <> '';

END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [O$TH_Object]
	(
		@Id uniqueidentifier,
		@Touch_App bit = 0
	)
AS


  -- !! Conflit potentiel si concurrence d'accès
  declare @Tick bigint = dbo.O$App_Tick() +1;
  update OBJ set Tick=@Tick where Id=@Id;
  if @Touch_App=1
	  begin
		  update APP set Cycle_Tick=Cycle_Tick+1, Cycle_Time=GETUTCDATE();
		  select Cycle_Tick as Tick from APP;
	  end
  else
	  begin
		  select @Tick as Tick;
	  end

RETURN
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [O$RZ_Properties]
	(
		@Id_Object uniqueidentifier,
		@Id_Property nvarchar (20)
	)
AS
	delete from OBJ_Relation where Id_Object=@Id_Object and Id_Property like @Id_Property + '%';
	delete from OBJ_Property where Id_Object=@Id_Object and Id_Property like @Id_Property + '%';
RETURN
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Destruction d'un objet
--  L'appelant se charge de supprimer au préalable les enfants

CREATE PROCEDURE [O$RM_Object]
(
	@Id uniqueidentifier
)
 AS

    DECLARE CC CURSOR LOCAL FOR SELECT id from OBJ where Id_Parent=@Id
	OPEN CC

	declare @Code uniqueidentifier;
	FETCH next from CC into @Code

	WHILE @@FETCH_STATUS = 0
	BEGIN
		execute O$RM_Object @Code
		FETCH next from CC into @Code
	END

	CLOSE CC
	DEALLOCATE CC

	delete from OBJ_Property where Id_Object = @Id;
	delete from OBJ_Resource where Id_Object = @Id;
	delete from OBJ_Relation where Value = @Id;
	delete from OBJ where Id=@Id
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [O$Relation]
	(
	@Id_Object uniqueidentifier,
	@Id_Property varchar (20)
	)
RETURNS uniqueidentifier
AS
begin
	DECLARE @Value uniqueidentifier;
	Set @Value = null;
	  if (@Id_Object is not null)
	  begin
		SELECT @Value=Value FROM OBJ_Relation WHERE (Id_Object=@Id_Object and Id_Property=@Id_Property)
	  end
	RETURN @Value
end
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [O$Property]
	(
	@Id_Object uniqueidentifier,
	@Id_Property varchar (20)
	)
RETURNS nvarchar(MAX)
AS
begin
	DECLARE @Value nvarchar(Max);
	Set @Value = '';
	  if (@Id_Object is not null)
	  begin
		SELECT @Value=Value FROM OBJ_Property WHERE (Id_Object=@Id_Object and Id_Property=@Id_Property)
	  end
	RETURN @Value
end
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Reinitialise la base de donnée

CREATE PROCEDURE [O$Init]
AS

-- delete  everything


		delete  CMD
		delete  APP
		delete  OBJ_Property
		delete  OBJ_Relation
		delete  OBJ

        -- Creer l'objet application
        insert into APP values (0,0, 0, GETUTCDATE(), 1,1,0,1,GETUTCDATE(), 1);
        -- Creer l'objet null   
   		insert into OBJ values (0x00000000000000000000000000000000, '<Null>','<Null>',null,null,0,0,0,'S', '','')

RETURN
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [O$WR_Resource]
	(
		@Id_Object uniqueidentifier,
		@Id_Property varchar (40),
		@Value varbinary (MAX),
		@Dim1 int,
		@Dim2 int,
		@Source nvarchar (MAX),
		@Info nvarchar (MAX)
	)
AS
	if @Value is null
	begin
	  delete from OBJ_Resource where Id_Object=@Id_Object and Id_Property = @Id_Property;
	  return;
	end
	else if exists (select * from OBJ_resource where Id_Object=@Id_Object and Id_Property = @Id_Property)
	begin
		update OBJ_Resource set Value=@Value,Dim1=@Dim1,Dim2=@Dim2,Source=@Source,Info=@Info,Version=GETDATE() where Id_Object=@Id_Object and Id_Property = @Id_Property
	end
	else
	begin
		insert into OBJ_Resource values (@Id_Object, @Id_Property, @Value,@Dim1,@Dim2,@Source,@Info,GETDATE())
	end


RETURN
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [O$WR_Property]
	(
		@Id_Object uniqueidentifier,
		@Id_Property varchar (40),
		@Value nvarchar (MAX)
	)
AS
	if @Value is null
	begin
	  delete from OBJ_Property where Id_Object=@Id_Object and Id_Property = @Id_Property;
	  return;
	end
	else if exists (select * from OBJ_Property where Id_Object=@Id_Object and Id_Property = @Id_Property)
	begin
		update OBJ_Property set Value=@Value where Id_Object=@Id_Object and Id_Property = @Id_Property
	end
	else
	begin
		insert into OBJ_Property values (@Id_Object, @Id_Property, @Value)
	end
	
	-- Si propriété unique surveillée, supprimer de OBJ_Cleaner et supprimer préventivement partout ailleurs dans l'objet
	--  Afin d'éviter une suppression auto ultérieure
	if (LEFT(@Id_Property,1) = '!')
	begin
	  delete from OBJ_Cleaner where Value=@Value;
	  delete from OBJ_Property where Id_Object=@Id_Object and Id_Property<>@Id_Property and Value=@Value;
	end

RETURN
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Creation / Modification d'un objet

CREATE PROCEDURE [O$WR_Object]
	(
		@Id uniqueidentifier,
		@Alias varchar(40),
		@Id_Class uniqueidentifier,
		@Name nvarchar(MAX),
		@Id_Parent uniqueidentifier,
		@Protection char(1) = ' '
	)
AS
begin

	if ((select count (*) FROM OBJ WHERE (Id = @Id)) = 0)
	
	-- Create a new object
	begin
		declare @Layer int
		if (LEFT (@Alias, 3) = 'CL$')
		begin
			set @Layer = 1;     -- Forcer les classes à se charger en premier
		end
		else
		begin
			set @Layer = dbo.O$Layer (@Id_Parent) +1;
		end
	
		INSERT into OBJ values
			(@Id, @Alias, @Name, @Id_Parent, @Id_Class , dbo.O$App_Tick() +1,
			 dbo.O$App_Build (), @Layer, @Protection, '', '')
	

	end
	else


	-- Reset existing object
	begin
	update OBJ
	set 
		Alias = @Alias,
	   [Name] = LTrim(Rtrim(@Name)),
		Id_Parent=@Id_Parent,
		Id_Class=@Id_Class,
		Build=dbo.O$App_Build (),
		Tick=dbo.O$App_Tick() +1

	 where Id = @Id

	delete from OBJ_Property where Id_Object = @Id; -- n'effacer que les infos de config
	delete from OBJ_Relation where Id_Object = @Id
	-- douteux ... delete from Object_Relation where Value = @Id and [Level] in ('C','c')
	end
	
end
	
RETURN
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [O$CK_Layer]
(
	@Layer int
)
AS

	declare @Id uniqueidentifier, @Parent_Layer int;
	declare Obj cursor for select Id, dbo.O$Layer (Id_Parent) from OBJ where Layer = @Layer;
    open Obj

	fetch next from Obj into @Id, @Parent_Layer

	while @@FETCH_STATUS = 0
	begin
	    if (@Layer <= @Parent_Layer)
	    begin
			update OBJ set Layer = @Parent_Layer+1 where Id=@Id
	    end
      
		fetch next from Obj into @Id, @Parent_Layer

	end
	close Obj
	deallocate Obj
	
RETURN
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [O$CK_Layers]
AS
  declare @Max_Layer int;
  declare @Layer int;
  set @Layer = 2;  -- Commencer a deux pour ne pas impacter les classes quie sont forcées à 1
  select @Max_Layer = max (Layer) from OBJ

  while @Layer <= @Max_Layer
  begin
    execute O$CK_Layer @Layer;
    set @Layer = @Layer +1;
  end
  select GETDATE()

RETURN
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE O$CR_Table
(
	@Table_Name varchar (MAX)
)
AS
BEGIN

  sET NOCOUNT ON;

   declare @Request1 varchar(Max) = replace ('
CREATE TABLE [{TABLE_NAME}](
	[Id] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_{TABLE_NAME}] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]','{TABLE_NAME}',@Table_Name);

   declare @Request2 varchar(Max) = replace ('
CREATE VIEW [V${TABLE_NAME}] AS
SELECT     dbo.{TABLE_NAME}.*,
  dbo.OBJ.Alias, dbo.OBJ.Name, dbo.OBJ.Id_Parent, dbo.OBJ.Id_Class, dbo.OBJ.Tick, dbo.OBJ.Build,dbo.OBJ.Layer, dbo.OBJ.Protection, dbo.OBJ.State, dbo.OBJ.Flags
FROM  dbo.{TABLE_NAME} INNER JOIN dbo.OBJ ON dbo.{TABLE_NAME}.Id = dbo.OBJ.Id','{TABLE_NAME}',@Table_Name);

   declare @Request3 varchar(Max) = replace ('ALTER TABLE [{TABLE_NAME}]  WITH CHECK ADD  CONSTRAINT [FK_{TABLE_NAME}_OBJ] FOREIGN KEY([Id]) REFERENCES [OBJ] ([Id]) ON DELETE CASCADE','{TABLE_NAME}',@Table_Name);
   declare @Request4 varchar(Max) = replace ('ALTER TABLE [{TABLE_NAME}] CHECK CONSTRAINT [FK_{TABLE_NAME}_OBJ] ','{TABLE_NAME}',@Table_Name);


  exec (@Request1);
  exec (@Request2);
  exec (@Request3);
  exec (@Request4);

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[O$CK_Table]
(
	@Table_Name varchar (MAX)
)
AS
BEGIN

  sET NOCOUNT ON;

   declare @Request2 varchar(Max) = replace ('
ALTER VIEW [V${TABLE_NAME}] AS
SELECT     dbo.{TABLE_NAME}.*,
  dbo.OBJ.Alias, dbo.OBJ.Name, dbo.OBJ.Id_Parent, dbo.OBJ.Id_Class, dbo.OBJ.Tick, dbo.OBJ.Build,dbo.OBJ.Layer, dbo.OBJ.Protection, dbo.OBJ.State, dbo.OBJ.Flags
FROM  dbo.{TABLE_NAME} INNER JOIN dbo.OBJ ON dbo.{TABLE_NAME}.Id = dbo.OBJ.Id','{TABLE_NAME}',@Table_Name);

  exec (@Request2);

END
GO

ALTER TABLE [OBJ]  WITH CHECK ADD  CONSTRAINT [FK_Class] FOREIGN KEY([Id_Class])
REFERENCES [OBJ] ([Id])
GO
ALTER TABLE [OBJ] CHECK CONSTRAINT [FK_Class]
GO
ALTER TABLE [OBJ]  WITH CHECK ADD  CONSTRAINT [FK_Parent] FOREIGN KEY([Id_Parent])
REFERENCES [OBJ] ([Id])
GO
ALTER TABLE [OBJ] CHECK CONSTRAINT [FK_Parent]
GO
ALTER TABLE [OBJ_Property]  WITH CHECK ADD  CONSTRAINT [FK_Property] FOREIGN KEY([Id_Object])
REFERENCES [OBJ] ([Id])
GO
ALTER TABLE [OBJ_Property] CHECK CONSTRAINT [FK_Property]
GO
ALTER TABLE [OBJ_Relation]  WITH CHECK ADD  CONSTRAINT [FK_Related] FOREIGN KEY([Value])
REFERENCES [OBJ] ([Id])
GO
ALTER TABLE [OBJ_Relation] CHECK CONSTRAINT [FK_Related]
GO
ALTER TABLE [OBJ_Relation]  WITH CHECK ADD  CONSTRAINT [FK_Relation] FOREIGN KEY([Id_Object])
REFERENCES [OBJ] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [OBJ_Relation] CHECK CONSTRAINT [FK_Relation]
GO
ALTER TABLE [OBJ_Resource]  WITH CHECK ADD  CONSTRAINT [FK_Resource] FOREIGN KEY([Id_Object])
REFERENCES [OBJ] ([Id])
GO
ALTER TABLE [OBJ_Resource] CHECK CONSTRAINT [FK_Resource]
GO
