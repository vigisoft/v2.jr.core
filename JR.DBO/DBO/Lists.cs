using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using JR.DB;


namespace JR.DBO
{
    public class JRO_List
    {
        public JRO_List (JRO_Object Parent)
        {
            if (Parent == null) return;
            _List = new Dictionary<Guid, JRO_Object>();
            foreach (JRO_Object Child in Parent.Children)
            {
                _List[Child.Id] = Child;
            };
        }

        Dictionary <Guid, JRO_Object> _List;
        public JRO_Object Get_Item_By_Id(Guid Id)
        {
            if (_List == null) return null;
            if (!_List.ContainsKey(Id)) return null;
            return _List[Id];
        }

        public JRO_Object Get_Item_By_Key(string Key)
        {
            if (_List == null) return null;
            foreach (JRO_Object Child in _List.Values)
            {
                if (JR.Strings.Like (Key, Child.Name))
                {
                    return Child;
                }
            }
            return null;
        }

        public Dictionary<Guid, JRO_Object> Get_List
        {
            get {return _List; }
        }

    }
}
