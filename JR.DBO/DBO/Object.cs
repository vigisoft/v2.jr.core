using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using JR.DB;

namespace JR.DBO
{
    public class JRO_Object:I_Debug
    {

        Guid _Id;
        string _Alias;
        string _Class_Id = "";
        int _Touched = 0;
        DB_Row _Info;
        Int64 _Tick = 0;
        List<JRO_Object> _Children = null;
        string _Name = "";
        string _Protection = "";
        Guid _Parent_Id = Guid.Empty;
        string _Parent_Name = "";
        JRO_Class _Class;
        JRO_Object _Parent;

        Dictionary<string, JRO_Property> _Properties;

        public JRO_Model M { get; set; }
        public bool Is_New { get; set; }

        public JRO_Object()
        {
            M = null; // Volontaire car doit �tre forc� ensuite JRO_Model.Current;
            Is_New = false;
        }

        public void Prepare(JRO_Model Model, Guid Oid, string Alias)
        {
            M = Model;
            _Alias = Alias;

            // Si null a ete cr�� et pas de Guid, chercher par alias sinon creer un nouvel objet
            if (Oid == Guid.Empty && M.Null != null)
            {
                if (Alias != "")
                {
                    JRO_Object Existing = M.Get_Cached_Object(Alias);
                    // Si un alias existe, le cloner et le remplacer
                    if (Existing != null)
                    {
                        Clone(Existing);
                        M.Cache_Object(this, true);
                        return;
                    }
                }
                _Id = M.NewGuid();
                Is_New = true;
            }
            else
            {
                _Id = (Guid)Oid;
            }
        }

        public JRO_Object(JRO_Model Model, Guid Oid, string Alias)
        {
            Is_New = false;
            Prepare(Model, Oid, Alias);
        }

        void Clone(JRO_Object Source)
        {
            _Id = Source.Id;
            _Alias = Source.Alias;
            _Info = Source._Info;
            _Class_Id = Source.Class_Id;
            _Children = Source._Children;
            _Name = Source._Name;
            _Protection = Source._Protection;
            _Parent_Id = Source._Parent_Id;
            _Parent_Name = Source._Parent_Name;
            _Class = Source._Class;
            _Parent = Source._Parent;
            _Properties = Source._Properties;
            _Touched = Source._Touched;
            _Tick = Source._Tick;
        }

        public JRO_Object(JRO_Model Model, Guid Id)
            : this(Model, Id, "")
        {
        }

        public JRO_Object(JRO_Model Model, string Alias)
            : this(Model, Guid.Empty, Alias)
        {
        }

        public static JRO_Object Get_Object(Guid Id, JRO_Model Model)
        {
            if (Model.DB == JRO_Db.Current)
            {
                JRO_Object O = Model.Get_Cached_Object(Id);
                if (O != null) return O;
            }
            return new JRO_Object(Model, Id, "");
        }

        // Index est toujours en majuscule sans blanc
        public virtual object Extended_Property(string Index)
        {
            return null;
        }

        // Indexation multiple d'un objet
        object Member (object Source, string Index)
        {
            if (Source == null) return null;
            if (Source is string)
            {
                if ((string)Source == "") return "";

                // Instruction de formatage
                if (Index.Length < 2) return Source;
                switch (Index.ToUpper()[0])
                {
                    case 'D':
                        {
                            DateTime Val = JR.Convert.To_Time(Source);
                            if (Val == Time.Null_Time) return "";
                            return Val.ToString(Index.Substring(1));
                        }
                    case 'I':
                        {
                            int Val = JR.Convert.To_Integer(Source);
                            if (Val == 0) return "";
                            return Val.ToString(Index.Substring(1));
                        }
                    case 'R':
                        {
                            double Val = JR.Convert.To_Float(Source);
                            if (Val == 0.0) return "";
                            return Val.ToString(Index.Substring(1));
                        }
                    case 'F':
                        {
                            string [] Params = JR.Strings.Split ((string)Source,5);
                            switch (Index[1])
                            {
                                case 'S': return Params[0]; // Server file
                                case 'C': return Params[1]; // Client File
                                case 'F': return Params[2]; // File type
                                case 'T': return Params[3]; // Title
                                case 'D': return Params[4]; // Date
                            }
                            return "";
                        }
                    case 'S':
                        {
                            switch (Index.ToUpper()[1])
                            {
                                case 'T':  // Tronc
                                    {
                                        string Val = Source as string;
                                        int Max = JR.Convert.To_Integer(Index.Substring(2));
                                        if (Val.Length > Max) return Val.Substring(0, Max - 3) + "...";
                                        return Source;
                                    }
                                case 'S':  // Substring
                                    {
                                        string Val = Source as string;
                                        int Max = JR.Convert.To_Integer(Index.Substring(2));
                                        if (Val.Length > Max) return Val.Substring(Max);
                                        return "";
                                    }
                            }
                            return Source;
                        }
                }
                return Source;
            }
            if (Source is JRO_Object)
            {
                JRO_Object O = Source as JRO_Object;
                switch (Index.ToUpper())
                {
                    case "N": return O.Name;
                    case "ID": return O.Id;
                    case "A": return O.Alias;
                    case "C": return O.Class;
                    case "CH": return O.Children;
                    case "PA": return O.Parent;
                    default: return O.Properties;
                }
            }
            if (Source is JRO_Object [])
            {
                JRO_Object [] O = Source as JRO_Object [];
                int I = JR.Convert.To_Integer (Index);
                if (I >= 0 && I < O.Length) return O[I];
                return null;
            }
            if (Source is Dictionary <string, JRO_Property> )
            {
                Dictionary <string, JRO_Property> O = Source as Dictionary <string, JRO_Property> ;
                if (!O.ContainsKey(Index.ToUpper()))
                {
                    if (O == this.Properties)    // tester si on est toujours dans le m�me objet
                    {
                        return Extended_Property(Index);    // Chercher une propriete calcul�e
                    }
                    else
                    {
                        return null;
                    }
                }
                JRO_Property P = O[Index];
                switch (P.Mode)
                {
                    case JRO_Property_Mode.Relation: return this.Check_Links (P);
                    case JRO_Property_Mode.Volatile: return P.Volatile;
                }
                return P.Value;
            }
            return null;

        }

        // Indique un objet dont l'ID n'est pas connu dans la base
        public bool Is_Unknown 
        {
            get
            {
                R();
                return (!Is_New) && _Info.R == null;
            }
        }

        // �value et remplace les {}
        public string Format(string Expression)
        {
            string New_Exp = "";
            string Rest = Expression;
            while (Rest != "")
            {
                int From = Rest.IndexOf ('{');
                if (From == -1) break;
                int To = Rest.IndexOf ('}', From+1);
                if (To == -1) break;
                if (From > 0) New_Exp += Rest.Substring(0, From);
                New_Exp += this[Rest.Substring (From+1, To-From-1)];
                Rest = Expression.Substring(To + 1);
            }
            return New_Exp + Rest;
        }

        public JRO_Object Get_Enum(string Index)
        {
            P();
            object Source = this;
            char Separator = '#';
            if (Index.IndexOf('|') >= 0) Separator = '|';
            string[] Indexes = JR.Strings.Official(Index).Split(Separator);
            foreach (string I in Indexes)
            {
                Source = Member(Source, I);
                if (Source == null) return null;
                if (Source is JRO_Object)
                {
                    JRO_Object O = (JRO_Object)Source;
                    if (O.Is_A ("EN")) return O;
                }
            }
            if (Source is JRO_Object) return (JRO_Object)Source;
            return null;
        }

        public virtual string Get_Extended_Value(string Key)
        {
            return "";
        }

        public virtual void Set_Extended_Value(string Key, string Value)
        {
        }

        public string this[string Index]
        {
            get
            {
                P();
                object Source = this;
                char Separator = '#';
                if (Index.IndexOf('|') >= 0) Separator = '|';
                string[] Indexes = Index.Trim().Split(Separator);
                foreach (string I in Indexes)
                {
                    Source = Member (Source, I);
                    if (Source == null) return Get_Extended_Value(I);
                }
                return Source.ToString ();
            }

            set
            {
                object Source = this;
                char Separator = '#';
                if (Index.IndexOf('|') >= 0) Separator = '|';
                string[] Indexes = JR.Strings.Official(Index).Split(Separator);
                if (Indexes.Length < 1) return;
                string Prefix = Indexes[0];

                // Gestion de l'autosave (commence par *)
                bool _Autosave = false;
                if (Prefix.Length > 0 && Prefix[0] == '*')
                {
                    Prefix = Prefix.Substring(1);
                    _Autosave = true;
                }
                switch (Prefix)
                {
                    case "N": this.Name = value; break;
                    case "C": this.Class.Alias = value; break;
                    case "PA": this.Parent_Id = JR.Convert.To_Guid (value); break;
                    default:
                        if (Indexes.Length < 2) return;
                        string Key = Indexes[1];
                        switch (Prefix)
                        {
                            case "P": Add_Property(Key, value); break;
                            case "R": Add_Relations(Key, value); break;
                            case "V": Add_Volatile(Key, value); break;
                        }
                        break;
                }
                if (_Autosave) this.Save ();

            }
        }

        // Indique un changement qui autorise la sauvegarde
        // 0 : a jour
        // 1 : modif profonde (on poste tout l'objet)
        // 2 : modif des attributs (on ne poste que les attributs touch�s)

        public void Touch (int Level)
        {
            if (_Touched == 1 && Level > 0) return;
            _Touched = Level;
        }

        public bool Touched
        {
            get { return _Touched > 0; }
        }

        public Guid Id
        {
            get { return _Id; }
        }

        public string Alias
        {
            get { return _Alias; }
            set { if (_Alias == value) return; Touch(1); _Alias = value; }
        }


        public JRO_Model_Db Db
        {
            get { return M.DB; }
        }

        public bool Is_Null
        {
            get { return (_Id == Guid.Empty); }
        }

        public bool Is_Empty
        {
            get { return (_Info == null||_Info.R == null); }
        }

        //public string Key
        //{
        //   get
        //   {
        //       if (Is_Null) return "";
        //       int Pos = Class.Id.Length -5;
        //       if (Id.Length < Pos) return Id;
        //       return Id.Substring (Pos);
        //   }
        //}

        // Effectue un rafraichissement simple
        public bool Refresh()
        {
            _Info = new DB_Row(null);
            if (Is_New) return false;
            Int64 Old_Tick = _Tick;
            _Info = Db.Get_Object(_Id);
            if (_Info.R == null) return true;
            _Tick = (Int64)_Info.Long("Tick");
            if (Tick == Old_Tick) return false;
            Class = M.Objects[_Info.Guid("Id_Class")] as JRO_Class;
            Name = _Info.String("Name");
            Parent_Name = _Info.String("Parent_Name");
            Protection = _Info.String("Protection");
            Parent_Id = _Info.Guid("Id_Parent");
            _Properties = null;
            Load_Extended();
            _Touched = 0;
            return true;
        }

        // Effectue un rafraichissement conditionnel au plus toutes les Period secondes
        DateTime Next_Refresh = DateTime.MinValue;
        public bool Refresh(double Period)
        {
            if (Next_Refresh == DateTime.MinValue) Next_Refresh = DateTime.UtcNow.AddSeconds(Period);
            if (Next_Refresh > DateTime.UtcNow) return false;
            Next_Refresh = DateTime.UtcNow.AddSeconds(Period);
            if (Db.Get_Object_Tick(Id) == _Tick) return false;
            return Refresh();
        }

        // Remont�e des infos de premier niveau
		void R ()
		{
			if (_Info == null) Refresh ();
		}

		public bool Dead {get {R();return (_Info.R == null);}}
        public int Layer { get { R(); return _Info.Int ("Layer"); } }
        public long Build { get { R(); return _Info.Long ("Build"); } }
        public long Tick { get { R(); return _Tick; } }
		public bool Is_A(string Class_Suffix)
		{
			return (Class_Id.StartsWith ("@@@_CL_" + Class_Suffix));
		}

        public string Get_Full_Name (string From)
		{
			if (_Id == Guid.Empty) return "";
			return Db.Get_Full_Name (_Id, From);
		}

        // Retourne la propri�t� d'un objet
        public string Get_Property
            (string Id_Property)
        {
            JRO_Property Prop = Get_P(Id_Property.ToUpper());
            if (Prop == null) return "";
            if (Prop.Mode == JRO_Property_Mode.Volatile) return Prop.Volatile.ToString();
            return Prop.Text;
        }

        public string Get_Date_Property
            (string Id_Property, string Format)
        {
            DateTime Date = Date_Property(Id_Property);
            if (Date == Time.Null_Time) return "";
            return Date.ToString(Format);
        }

        public void Set_Date_Property
            (string Id_Property, string Value, string Format)
        {
            DateTime Date = JR.Time.Parse(Value, Format);
            Add_Property(Id_Property, JR.Convert.To_String(Date));
        }

        // Retourne la propri�t� fichier d'un objet
        //public JRO_File_Property Get_File_Property
        //    (string Id_Property)
        //{
        //    return new JRO_File_Property(this, Get_P(Id_Property));
        //}

        //// Positionne la propri�t� fichier d'un objet
        //public void Set_File_Property(
        //    string Id_Property,
        //    string Server_File,
        //    string Client_File,
        //    string File_Type,
        //    string Title,
        //    DateTime Date)
        //{
        //    string Prop = "*" + Server_File + "*" + Client_File + "*" + File_Type + "*" + Title + "*" + JR.Convert.To_String(Date) + "*";
        //    Add_Property(Id_Property, Prop);
        //}

        // Retourne la propri�t� d'un objet
        JRO_Property Get_P
            (string Id_Property)
        {
            P();
            if (!_Properties.ContainsKey(Id_Property)) return null;
            return _Properties[Id_Property];
        }

        // Retourne la propri�t� d'un objet
        public string [] Get_Properties
            (string Id_Property)
        {
            string Value = Get_Property(Id_Property);
            if (Value == null) return new string[0];
            return Value.Split (new string[] {"\r\n"},StringSplitOptions.RemoveEmptyEntries );
        }

		// Retourne la relation d'un objet
		public string Get_Relation
			(string Id_Property)
		{
            return Get_Property(Id_Property);
		}

        // Retourne les relations d'un objet
        public string[] Get_Relations
            (string Id_Property)
        {
            string Rels = Get_Property (Id_Property);
            if (Rels == "") return new string[0];
            return Rels.Split(';');
        }

        JRO_Object [] Check_Links (JRO_Property P)
        {
           string[] Rels = P.Value.Split (';');
           P.Links = new JRO_Object[Rels.Length];
           for (int I = 0; I < Rels.Length; I++)
           {
              P.Links[I] = JRO_Object.Get_Object (JR.Convert.To_Guid (Rels[I]), this.M);
           }
           return P.Links;
        }
        // Retourne les relations d'un objet
        public JRO_Object [] Get_Links
            (string Id_Property)
        {
            JRO_Property P = Get_P (Id_Property);
            if (P == null) return new JRO_Object [0];
            if (P.Links == null)
            {
                Check_Links(P);
            }
            return P.Links;
        }

        public object Get_Volatile
            (string Id_Property)
        {
            JRO_Property P = Get_P(Id_Property);
            if (P == null) return null;
            return P.Volatile;
        }

        // Retourne l'object relation
		public JRO_Object Get_Related
			(string Id_Property)
		{
            JRO_Object[] Links = Get_Links (Id_Property);
            if (Links.Length == 0) return null;
            return Links[0];
		}

        public string Text_Property (string Prop)
        {
            return Get_Property (Prop);
        }

        public string Key_Property (string Prop)
        {
            return JR.Strings.Official(Get_Property (Prop));
        }

        public int Int_Property (string Prop)
        {
            return JR.Convert.To_Integer (Get_Property (Prop));
        }

        public bool Bool_Property (string Prop)
        {
            return JR.Convert.To_Boolean (Get_Property(Prop));
        }

        public double Float_Property (string Prop)
        {
            return JR.Convert.To_Float(Get_Property(Prop));
        }

        public DateTime Date_Property (string Prop)
        {
            return JR.Convert.To_Time(Get_Property(Prop));
        }

        internal void P(bool Preserve_Touch = false)
        {
            R();
            if (_Properties != null) return;
            _Properties = new Dictionary<string, JRO_Property>();
            Load_Properties();
            if (!Preserve_Touch) _Touched = 0;
        }

        // Chargement d'une liste selon critere
        public static IList<JRO_Object> Get_List (string Where_Clause)
        {
            List<JRO_Object> L = new List<JRO_Object>();
            foreach (Guid Id in JRO_Model.Current.DB.Select_Ids(Where_Clause))
            {
                L.Add(new JRO_Object(JRO_Model.Current, Id));
            }
            return L;
        }

		void Load_Children ()
		{
            IList<Guid> _Ids = Db.Select_Ids(string.Format("where Id_Parent = '{0}'", _Id));
            _Children = new List<JRO_Object>();
			foreach (Guid Row in _Ids)
			{
				_Children.Add (JRO_Object.Get_Object (Row, this.M));
			}
		}

		void C ()
		{
			if (_Children != null) return;
			Load_Children();
		}

		public IList<JRO_Object> Children
		{
			get {C(); return _Children;}
		}

        public JRO_Object Get_Child(string Key)
        {
            foreach (var O in Children)
            {
                if (JR.Strings.Like(O.Name, Key)) return O;
            }
            return null;
        }

		void Load_Properties ()
		{
            // Lecture des propri�t�s
            DataRowCollection Props = Db.Get_Properties(_Id);
            DataRowCollection Rels = Db.Get_Relations(_Id);
            DataRowCollection Back_Rels = Db.Get_Back_Relations (_Id);

            // Ajouter les propri�t�s
            if (Props != null)
            {
                foreach (DataRow P in Props)
                {
                    Add_Property(P["Id_Property"].ToString(), P["Value"].ToString());
                };
            };

            if (Rels != null)
			{
                JRO_Property Re = null;
				foreach (DataRow R in Rels)
				{
                    string[] Prop_Name = ((string)R["Id_Property"]).Split('!');
                    if (Prop_Name.Length == 1)
                    {
                        Re = Add_Relation (Prop_Name [0], R["Value"].ToString());
                    }
                    else
                    {
                        if (Re != null)
                        {
                            Re.Value += ";" + R["Value"].ToString();
                        }
                    }
				};
			};

			if (Back_Rels != null)
			{
                foreach (DataRow R in Back_Rels)
                {
                    string Re_Id = "<" + (R["Id_Property"]).ToString();
                    string Value = R["Id_Object"].ToString();
                    if (!_Properties.ContainsKey (Re_Id))
                    {
                        Add_Relation(Re_Id, Value);
                    }
                    else
                    {
                        JRO_Property Re = _Properties[Re_Id];
                        Re.Value += ";" + Value;
                    }
                };
			};
		}

        // Nom de l'objet
        public string Name
        {
            get
            {
                if (_Name == "" && _Info == null) R();
                return _Name;
            }

            set { if (_Name == value) return; Touch(1); _Name = value; }
        }

        // Protection de l'objet
        public string Protection
        {
            get { return _Protection; }
            set { if (_Protection == value) return; Touch(1); _Protection = value; }
        }


        // Id du parent
        public Guid Parent_Id
        {
            get
            {
                if (_Parent_Id == Guid.Empty && _Info == null) R();
                return _Parent_Id;
            }

            set { if (_Parent_Id == value) return; Touch(1); _Parent_Id = value; }
        }

        // Nom du parent
        public string Parent_Name
        {
            get
            {
                if (_Parent_Name == "" && _Info == null) R();
                return _Parent_Name;
            }

            set { if (_Parent_Name == value) return; Touch(1); _Parent_Name = value; }
        }

        // Id de la classe
        public string Class_Id
        {
            get
            {
                if (_Class_Id == "" && _Info == null) R();
                return _Class_Id;
            }

            set { if (_Class_Id == value) return; Touch(1); _Class_Id = value; }
        }

        // Classe
        public JRO_Class Class
        {
            get
            {
                if (_Class == null) R();
                return _Class;
            }

            set { if (_Class == value) return; Touch(1); _Class = value; Class_Id = _Class.Alias; }

        }

        void Solve_Property(JRO_Property P, string Id_Property)
        {
            if (Class.Members.ContainsKey (Id_Property))
            {
                P.Member = Class.Members[Id_Property];
            }
        }

        // Parent
        public JRO_Object Parent
        {
            set { if (_Parent == value) return; Touch(1); _Parent = value; Parent_Id = _Parent.Id; _Parent_Name = _Parent.Name; }
            get
            {
                if (_Parent == null) _Parent = JRO_Object.Get_Object (Parent_Id, this.M);
                return _Parent; 
            }
        }

        public void Clear_Properties()
        {
            P(true);
            _Properties.Clear();
            Touch(2);
        }

        public JRO_Property Add_Property(string Id_Property, string Value)
        {
            P(true);
            string Id_Prop = JR.Strings.Official(Id_Property);
            if (_Properties.ContainsKey(Id_Prop) && _Properties[Id_Prop].Value == Value) return _Properties[Id_Property];
            JRO_Property Pr = new JRO_Property();
            Solve_Property(Pr, Id_Prop);
            Pr.Value = Value;
            _Properties[Id_Prop] = Pr;
            Touch(2);
            return Pr;
        }

        public JRO_Property Add_Volatile (string Id_Property, object Value)
        {
            int _Old_Touched = _Touched;
            JRO_Property P = Add_Property(Id_Property, "");
            P.Mode = JRO_Property_Mode.Volatile;
            P.Volatile = Value;
            _Touched = _Old_Touched;
            return P;
        }

        public JRO_Property Add_Related(string Id_Property, JRO_Object Value)
        {
            return Add_Relations(Id_Property, Value.Id.ToString());
        }

        public JRO_Property Add_Relation(string Id_Property, string Value)
        {
            return Add_Relations(Id_Property, Value);
        }

        public JRO_Property Add_Relations(string Id_Property, string Values)
        {
            JRO_Property P = Add_Property (Id_Property, Values);
            P.Mode = JRO_Property_Mode.Relation;
            return P;
        }

        public Dictionary<string, JRO_Property> Properties
        {
            get { P(); return _Properties; }
        }

        public virtual void Clear()
        {
        }

        // Destruction de l'objet
        public void Remove()
        {
            Clear();
            Db.Del_Object(this.Id);
        }

        // Calculs interm�diaires avant sauvegarde
        public virtual void Compute()
        {
            return;
        }

        public void Low_Level_Save()
        {
            if (this == M.Null) return;
            if (this is JRO_Class)
            {
                // C'est une classe, mais ne pas sauver la Meta classe
                Db.Set_Object(this.Id, this.Alias, M.Null.Id, this.Name, this.Parent_Id, this.Protection);
            }
            else
            {
                Db.Set_Object(this.Id, this.Alias, this.Class.Id, this.Name, this.Parent_Id, this.Protection);
                if (Is_New && Class.Extended_Tables != null)
                {
                    foreach (var DT in Class.Extended_Tables)
                    {
                        Db.Execute("insert into {0} (id) values (@1)", DT, Id);
                    }
                }
            }
        }

        public virtual void Save_Extended()
        {
        }

        public virtual void Load_Extended()
        {
        }

        public void Low_Level_Save_Properties()
        {
            Save_Extended();
            if (Properties == null)
            {
                _Touched = 0;
                return;
            }
            foreach (KeyValuePair<string, JRO_Property> P in _Properties)
            {
                if (Is_New||_Touched == 1 || P.Value.Touched)
                {
                    switch (P.Value.Mode)
                    {
                        case JRO_Property_Mode.Property:
                            {
                                Db.Set_Property(this.Id, P.Key, P.Value.Value);
                                break;
                            }
                        case JRO_Property_Mode.Relation:
                            {
                                int Row = 0;
                                string Id_Property = P.Key;
                                if (!Id_Property.StartsWith("<"))
                                {
                                    if (_Touched > 1)
                                    {
                                        Db.Reset_Relation(this.Id, Id_Property);
                                    }
                                    foreach (string R in P.Value.Value.Split(';'))
                                    {
                                        Row++;
                                        Db.Set_Relation(this.Id, Id_Property, JR.Convert.To_Guid(R), Row);
                                    }
                                }
                                break;
                            }
                    }
                    P.Value.Touched = false;
                }
            }
            _Touched = 0;
        }

        // Enregistrement des modifications
        public bool Save(bool Touch_App = true)
        {
            Compute();
            if (_Touched == 0) return false;
            if (Is_New || _Touched == 1)
            {
                Low_Level_Save();
            }
            Low_Level_Save_Properties();
            Is_New = false;
            _Tick = Db.Touch_Object(Id, Touch_App);
            if (Touch_App) M.Tick = _Tick;
            return true;
        }

        //public void Gen_Event(string Id_From, string Id_To, string Id_Category, string Message, int Priority)
        //{
        //    JRO_Event Ev = new JRO_Event();
        //    Ev.Id_Subject = this.Id.ToString();
        //    Ev.Id_From = Id_From;
        //    Ev.Id_To = Id_To;
        //    Ev.Id_Category = Id_Category;
        //    Ev.Message = Message;
        //    Ev.Priority = Priority;
        //    Ev.Send();
        //}

        // Charge une liste d'�critures � effectuer
        //public void Load_Script (string Def_File)
        //{
        //    string[] Lines = JR.Files.Read_File(Saf.Main.Program_Module.Real_File_Name(Def_File));
        //    foreach (string Line in Lines)
        //    {
        //        string[] Fields = JR.Strings.Split(Line);
        //        if (Fields.Length < 2) continue;
        //        this[Fields[0]] = Fields[1];
        //    }
        //}



        // Met l'objet en cache si il n'y est pas et 
        //public void Check_Cache()
        //{
        //    throw new NotImplementedException();
        //}

        public string HTML_Debug_Info
        {
            get 
            {
                P();
                if (_Info == null) return "";
                using (DB.Db_Reader DR = new Db_Reader(_Info.R))
                {
                    return DR.Full_HTML_Debug_Info;
                }
            }
        }
    }

    public enum JRO_Property_Mode {Property, Relation, Volatile}

    public class JRO_Property
    {
        public bool Touched = true;
        public JRO_Property()
        {
            this.Mode = JRO_Property_Mode.Property;
        }
        public JRO_Property(JRO_Property_Mode Mode)
        {
            this.Mode = Mode;
        }
        public JRO_Member Member;
        public string Value;
        public object Volatile;
        public JRO_Object[] Links;
        public string Text
        {
            get { return Value; }
        }
        public JRO_Property_Mode Mode;
    }

 
}
