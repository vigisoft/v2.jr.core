using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using JR.DB;

namespace JR.DBO
{

    // Un modele est un ensemble de classes et d'objets
    //  h�berg� dans une base de donn�es
    public class JRO_Model
    {

        public static Dictionary<string, JRO_Model> Models = new Dictionary<string, JRO_Model>();
//        public Dictionary<string, JRO_Event_Category> Event_Categories = new Dictionary<string, JRO_Event_Category>();
        public Dictionary<string, JRO_Object> Aliases = new Dictionary<string, JRO_Object>();
        public Dictionary<Guid, JRO_Object> Objects = new Dictionary<Guid, JRO_Object>();
        public List<JRO_Class> Classes = new List<JRO_Class>();

        JRO_Db _DB;
        //JRO_Event_Db _Event_DB;

        public JRO_Model(string Key)
        {
            _DB = new JRO_Db(Key);
            //_Event_DB = new JRO_Event_Db(Key);
            _Current_Model = this;
            Models[Key] = this;
            Tick = 0;
        }

        public static T Get_By_Key<T>(string Key) where T:JRO_Model, new ()
        {
            JRO_Model M;
            if (!Models.TryGetValue(Key, out M))
            {
                M = new T().Load();
            }
            return M as T;
        }

        // Dernier mod�le cr�� (en g�n�ral on en cr�e un seul)
        static JRO_Model _Current_Model = null;
        public static JRO_Model Current
        {
            get { return _Current_Model; }
        }

        // Retourne un modele par key
        public static JRO_Model Get (string Key)
        {
            JRO_Model M = Current;
            Models.TryGetValue(Key, out M);
            return M;
        }

        public long Tick { get; set; }


        void Save_Basics()
        {
            bool Touched = false;
            //foreach (JRO_Event_Category E in Event_Categories.Values)
            //{
            //    _Event_DB.Set_Event_Category (E.Alias,E.Name);
            //}

            // Sauver les nouvelles classes
            foreach (JRO_Class C in Classes)
            {
                if (!C.Is_New) continue;
                if (C == Null) continue;
                C.Low_Level_Save();
                Touched = true;
            }

            // Sauver les nouveaux objets sauf les classes
            foreach (JRO_Object O in Objects.Values)
            {
                if (O == Null) continue;
                if (!O.Touched) continue;
                O.Low_Level_Save();
                Touched = true;
            }
            foreach (JRO_Object O in Objects.Values)
            {
                if (!O.Touched) continue;
                O.Low_Level_Save_Properties();
            }
            if (Touched) _DB.Touch_App ();
        }

        // Recherche d'un objet dans le cache
        public JRO_Object Get_Cached_Object(Guid Id)
        {
            JRO_Object O = null;
            Objects.TryGetValue(Id, out O);
            return O;
        }

        // Recherche d'un objet dans le cache
        public JRO_Object Get_Cached_Object(string Alias)
        {
            JRO_Object O = null;
            Aliases.TryGetValue(Alias, out O);
            return O;
        }

        // Ajout d'un objet dans le cache
        public void Cache_Object(JRO_Object Object, bool Force = false)
        {
            if (!(_Cache_Active||Force)) return;
            Objects[Object.Id] = Object;
            if (Object.Alias != null) Aliases[Object.Alias] = Object;
        }

        bool _Cache_Active;

        // Chargement des classes de base
        void Build_Basics()
        {
            Load_Basics();
            Build();
            Save_Basics();
        }

        bool Loaded = false;
        public JRO_Model Load (bool Can_Init = false)
        {
            if (!Loaded)
            {
                Loaded = true;
                _Cache_Active = true;
                if (Can_Init && !_DB.App_Exists) _DB.Init();
                Build_Basics();
                Tick = _DB.App_Tick;
                _Cache_Active = false;
            }
            return this;
        }

        public void Touch()
        {
            Tick = _DB.Touch_App();
        }

        public void Rebuild ()
        {
            _Cache_Active = true;
            Build_Basics();
            _Cache_Active = false;
        }

        public virtual void Build()
        {
        }

        public JRO_Class Def_Class(string Key, string Name, string Content = "")
        {
            JRO_Class C;

            // Rechercher la classe dans les classes existantes
            C = Get_Cached_Object("CL$" + Key) as JRO_Class;

            // Si elle existe la completer
            if (C != null)
            {
                C.Content = Content;
            }
            // Sinon la cr�er
            else
            {
                C = new JRO_Class(this, Guid.Empty, Key, Content);
                Classes.Add(C);
                C.Name = Name;
                C.Protection = "M";
                Cache_Object(C);
            }

            return C;
        }

        public void Def_Extended_Table (string Table_Name, string Parameters)
        {
            if (Parameters != "") Parameters += ",";
            string Com1 = @"
CREATE TABLE [dbo].[{0}](
	[Id] [uniqueidentifier] NOT NULL,{1}
 CONSTRAINT [PK_{0}] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";

            string Com2 = @"
ALTER TABLE [dbo].[{0}]  WITH CHECK ADD  CONSTRAINT [FK_{0}_OBJ] FOREIGN KEY([Id])
REFERENCES [dbo].[OBJ] ([Id])
ON DELETE CASCADE";

            string Com3 = @"
ALTER TABLE [dbo].[{0}] CHECK CONSTRAINT [FK_{0}_OBJ]";

            string Com4 = @"
CREATE VIEW [dbo].[V${0}]
AS
SELECT     dbo.{0}.*, dbo.OBJ.Alias, dbo.OBJ.Name, dbo.OBJ.Id_Parent, dbo.OBJ.Id_Class, dbo.OBJ.Tick, dbo.OBJ.Build, dbo.OBJ.Protection, dbo.OBJ.State, 
                      dbo.OBJ.Flags, dbo.OBJ.Layer
FROM         dbo.{0} INNER JOIN
                      dbo.OBJ ON dbo.{0}.Id = dbo.OBJ.Id";

            DB.DB.Check_Table(Table_Name, string.Format(Com1, Table_Name, Parameters), string.Format(Com2, Table_Name), string.Format(Com3, Table_Name));
            DB.DB.Check_Table("V$" + Table_Name, string.Format(Com4, Table_Name));
        }

        public JRO_Class Def_Extended_Class(string Key, string Name, string Extended_Table, string Content = "")
        {
            JRO_Class C = Def_Class(Key, Name, Content);
            C.Extended_Tables = Extended_Table.Split(',');
            return C;
        }

        //public void Def_Event_Category(string Key, string Name)
        //{
        //    JRO_Event_Category EV = new JRO_Event_Category(Key);
        //    EV.Name = Name;
        //    Event_Categories[Key] = EV;
        //}

        public JRO_Db DB
        {
            get { return _DB; }
        }

        //public JRO_Event_Db Event_Db
        //{
        //    get { return _Event_DB; }
        //}

        //public long Add_Event
        //    (string Id_Category, DateTime Date, string Id_Subject, string Message,
        //    int Priority, string Id_From, string Id_To, int Duration, long Id_Operation,
        //    string Data)
        //{
        //    return _Event_DB.Add_Event(Id_Category, Date, Id_Subject, Message, Priority,
        //       Id_From, Id_To , Duration, Id_Operation, Data);
        //}

        //public long Add_Event
        //    (string Id_Category, string Id_Subject, string Message,
        //     string Id_From, string Id_To)
        //{
        //    return Add_Event(Id_Category, DateTime.UtcNow, Id_Subject, Message, 1, Id_From,Id_To,
        //        0, 0, null);
        //}


        // Retourne une data table apr�s transformation de la requ�te
        //
        public DataTable Read_Table(string Request, params object[] Params)
        {
            return _DB.Execute(Request, Params);
        }

        public DataRowCollection Read_Rows(string Request, params object[] Params)
        {
            using (var DT = _DB.Execute(Request, Params))
            {
                if (DT == null) return null;
                return DT.Rows;
            }
        }

        public DataRow Read_Row(string Request, params object[] Params)
        {
            using (var DT = _DB.Execute(Request, Params))
            {
                if (DT == null) return null;
                if (DT.Rows.Count == 0) return null;
                return DT.Rows[0];
            }
        }


        //public JRO_Class Load_Enum(string Key, string Name, string Def_File)
        //{
        //    JRO_Class Cl_Fo = Def_Class("FO_EN_" + Key, Name, "EN_" + Key);
        //    JRO_Object Fo_En = Cl_Fo.Def_Object("EN_" + Key, "@FO_EN", Name, "", "S");
        //    JRO_Class _Class = Def_Class("EN_" + Key, Name, "");

        //    string[] Lines = JR.Files.Read_File (Jaf.Main.Program_Module.Real_File_Name (Def_File));
        //    string Values = "";
        //    foreach (string Line in Lines)
        //    {
        //        string[] Fields = JR.Strings.Split(Line);
        //        if (Fields.Length < 3) continue;
        //        string [] Params = new string [Fields.Length -3];
        //        for (int I = 3; I< Fields.Length;I++)
        //        {
        //            Params [I-3] = Fields[I];
        //        }
        //        JRO_Object O = _Class.Def_Object(Key + "_" + Fields[0], "@FO_EN_" + Key, Fields[1], "S", Params);
        //        Values += ";" + Fields[0]; 
        //    }
        //    if (Values.Length > 0)
        //    {
        //        Fo_En.Add_Property("ORDER", Values.Substring(1));
        //    }
        //    return _Class;
        //}


        // Charge tous les objets globaux
        void Load_Aliases()
        {
            foreach (DataRow DR in _DB.List_Aliases())
            {
                Guid Id = (Guid)DR[0];
                string Alias = (string)DR[1];
                JRO_Object Ob;

                // Cas particulier de l'objet null
                if (Null == null && Id == Guid.Empty)
                {
                    Null = new JRO_Class(this, Id, "", "");
                    Ob = Null;
                    Classes.Add(Null);
                }
                // Cas d'une classe
                else if (Alias.StartsWith ("CL$"))
                {
                    JRO_Class OC = new JRO_Class(this, Id, Alias.Substring(3), "");
                    Ob = OC;
                    Classes.Add(OC);
                }
                else
                {
                    Ob = new JRO_Object(this, Id, Alias);
                }
                Ob.Is_New = false;
                Objects[Id] = Ob;
                Aliases[Alias] = Ob;
                Ob.Touch(0);
            }
        }

        // Classe mere et objet null
        public JRO_Class Null;

        // Objets
        public JRO_Class Cl_Ob;

        // Folder
        public JRO_Class Cl_Fo;

        // Folder system
        public JRO_Object Fo_Sys;

        // Folder application
        public JRO_Object Fo_App;

        // Charger les metas objets
        void Load_Basics()
        {

            // Chargement des objets alias�s
            Load_Aliases();

            // Event categories
            //Def_Event_Category("SYPRER", "Project Error");
            //Def_Event_Category("SYPRSP", "Project Stop");
            //Def_Event_Category("SYPRST", "Project Start");
            //Def_Event_Category("SYWSER", "Web Server Error");
            //Def_Event_Category("SYWSSP", "Web Server Stop");
            //Def_Event_Category("SYWSST", "Web Server Start");
            //Def_Event_Category("SYUSLF", "User login Failure");
            //Def_Event_Category("SYUSLI", "User login");
            //Def_Event_Category("SYUSLO", "User logout");
            //Def_Event_Category("SYTRAC", "Trace");
            //Def_Event_Category("SYSSST", "System start");
            //Def_Event_Category("SYSSSP", "System stop");

            // Classes
            Cl_Ob = Def_Class("OB", "Object");
            Cl_Fo = Def_Class("FO", "Folder");
            Fo_App = Cl_Fo.Def_Object("APP", Null, "Application", "S");
            Fo_Sys = Cl_Fo.Def_Object("SYS", Null, "System", "S");

        }



        internal Guid NewGuid()
        {
            return _DB.NewGuid();
        }

        // Permet de suivre globalement les op�rations de modifications sur le model
        public class Tracker
        {
            public JRO_Model Model {get;set;}
            long Save_Count = 0;
            public Tracker(JRO_Model Model)
            {
                this.Model = Model;
                Save_Count = Model.Save_Count;
            }
            public bool Changed
            {
                get { return Save_Count != Model.Save_Count; }
            }
        }
        public long Save_Count { get; set; }
        public void Notify_Save()
        {
            Save_Count++;
        }
        public Tracker Track
        {
            get { return new Tracker (this); }
        }
    }

}
