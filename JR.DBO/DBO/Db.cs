using System;
using System.Collections.Generic;
using System.Text;
using JR.DB;
using System.Data;
using System.Text.RegularExpressions;

namespace JR.DBO
{


    // Interfaces base de donn�es standard unifi�e
    public class JRO_Model_Db
    {

        // Classes qui simplifient l'acces en lecture � la BD
        public Db.Reader New_Reader (string Request, params object[] Params)
        {
            return new Db.Reader(Request, Params) {Config= DB.Config};
        }


        //internal Base _DB;
        public Gen_Db<JRO_Object> DB { get; set; }
        public JRO_Model_Db (Db_Config Config)
        {
            //_DB = new Base(Config.Connection_String);
            DB = new Gen_Db<JRO_Object>{Config=Config};
        }

        //public Base Base
        //{
        //    get { return _DB; }
        //}

        //internal static OleDbParameter P(string Name, Object Value)
        //{
        //    return new OleDbParameter(Name, Value == null ? DBNull.Value : Value);
        //}

        public DB_Row Get_Object(Guid Id)
        {
            return new DB_Row(DB.Exec_Row("Select OBJ.*, dbo.O$Name (Id_Parent) as Parent_Name from OBJ where OBJ.Id = @0", Id));
        }

        public long Get_Object_Tick (Guid Id)
        {
            return DB.Exec_Long("select Tick from Obj where Id = @0", Id);
        }

        public string Get_Full_Name(Guid Id, string Folder_Id)
        {
            return DB.Exec_String("select O$Full_Name( @0, @1)", Id, Folder_Id);
        }

        public DB_Row Get_Property(Guid Id, string Id_Property)
        {
            return new DB_Row(DB.Exec_Row("O$Property @0, @A1", Id, Id_Property));
        }

        public void Del_Object(Guid Id)
        {
            DB.Exec ("O$RM_Object @0", Id);
        }

        public void Rename_Object(Guid Id, string Name)
        {
            DB.Exec("update OBJ set [Name]=@1 where Id=@0", Id, Name);
        }

        public DataRowCollection Get_Back_Relations(Guid Id)
        {
            return DB.Exec_Table("select * from OBJ_Relation where Value = @0", Id).Rows;
        }

        public DataRowCollection Get_Properties(Guid Id)
        {
            return DB.Exec_Table("select * from OBJ_Property where Id_Object = @0", Id).Rows;
        }

        public DB_Row Get_Relation(Guid Id, string Id_Property)
        {
            return new DB_Row(DB.Exec_Row("O$Relation @0, @A1", Id, Id_Property));
        }

        public DataRowCollection Get_Relations(Guid Id)
        {
            return DB.Exec_Table("Select * from OBJ_Relation where Id_Object = @0 order by Id_Property asc", Id).Rows;
        }

        public void Init()
        {
            DB.Exec ("O$Init");
        }

        //public DataTable List_Children(Guid Id_Parent)
        //{
        //    return DB.Exec_Table("select * from OBJ where (Id_Parent = @0 and Protection <> 'H') order by Id_Class, OBJ.Name", Id_Parent);
        //}

        public DataRowCollection List_Aliases()
        {
            return DB.Exec_Table("select id, alias from OBJ where alias is not null order by layer, Alias").Rows;
        }

        public void Set_Object(Guid Id, string Alias, Guid Id_Class, string Name, Guid Id_Parent, string Protection)
        {
            DB.Exec("O$WR_Object @0, @A1, @2, @A3, @4, @5", Id, Alias, Id_Class, Name, Id_Parent, Protection);
        }

        public void Move_Object(Guid Id, string Name, Guid Id_Parent)
        {
            DB.Exec("update OBJ set [Name]=@1,Id_Parent=@2 where Id=@0", Id, Name, Id_Parent);
        }

        public void Set_App_Tick(long Tick)
        {
            DB.Exec ("O$WR_App_Tick", Tick);
        }

        public void Set_Property(Guid Id_Object, string Id_Property, string Value)
        {
            DB.Exec("O$WR_Property @0,@A1,@2", Id_Object, Id_Property, Value);
        }

        public void Set_State(Guid Id_Object, string Value)
        {
            DB.Exec("update OBJ set [State]=@A1 where Id=@0", Id_Object, Value);
        }

        public void Set_Flags (Guid Id_Object, string Value)
        {
            DB.Exec("update OBJ set Flags=@A1 where Id=@0", Id_Object, Value);
        }

        public void Reset_Relation(Guid Id_Object, string Id_Property, bool Exact = false)
        {
            if (Exact)
            {
                DB.Exec("delete from OBJ_Relation where Id_Object=@0 and Id_Property = @1", Id_Object, Id_Property);
            }
            else
            {
                DB.Exec("delete from OBJ_Relation where Id_Object=@0 and Id_Property like @1 + '%'", Id_Object, Id_Property);
            }
        }

        public void Reset_Property (Guid Id_Object, string Id_Property)
        {
            DB.Exec("delete from OBJ_Property where Id_Object=@0 and Id_Property = @1", Id_Object, Id_Property);
        }

        public void Reset_Properties(Guid Id_Object, string Id_Property = "")
        {
            DB.Exec("O$RZ_Properties @0, @A1", Id_Object, Id_Property);
        }

        public void Set_Relation(Guid Id_Object, string Id_Property, Guid? Value, int Row=-1)
        {
            if (Value == null) return;
            if (Row > 1) Id_Property = string.Format("{0}!{1:d3}", Id_Property, Row);
            DB.Exec("insert into OBJ_Relation values (@0, @A1, @2)", Id_Object, Id_Property, Value.Value);
        }

        public long Touch_Object(Guid Id, bool Touch_App)
        {
            return DB.Exec_Long("O$TH_Object @0, @1", Id, Touch_App?1:0);
        }

        public long Touch_App()
        {
            return DB.Exec_Long("O$TH_App");
        }

        public long App_Tick
        {
            get
            {
                return DB.Exec_Long("select dbo.O$App_Tick() as Tick");
            }
        }


        public void Write_Resource (Guid Id, string Id_Property, byte[] Value, string Source="", string Info="", int Dim1=0, int Dim2=0)
        {
            if (Value == null)
            {
                Delete_Resource(Id, Id_Property);
                return;
            }
            DB.Exec("O$WR_Resource @0,@1,@2,@3,@4,@5,@6", Id, Id_Property, Value, Dim1, Dim2, Source,Info);
        }

        public void Delete_Resource(Guid Id, string Id_Property)
        {
            DB.Exec("delete from Obj_Resource where id_Object=@0 and Id_Property=@1", Id, Id_Property);
        }

        public void Read_Resource(Guid Id, string Id_Property, out byte[] Value, out int Dim1, out int Dim2, out string Source, out string Info, out DateTime Version, bool Read_Value = true)
        {
            Source = Info = "";
            Dim1= Dim2 = 0;
            Value = null;
            Version = DateTime.MinValue;
            using (var DR = New_Reader("select " + (Read_Value?"Value":"null") + ",Dim1,Dim2,Source,Info,Version from Obj_Resource where id_Object=@0 and Id_Property=@1", Id, Id_Property))
            {
                Value = DR.Next_Value as byte[];
                Dim1 = DR.Next_Int;
                Dim2 = DR.Next_Int;
                Source = DR.Next_String;
                Info = DR.Next_String;
                Version = DR.Next_Date;
            }
        }

        // Effectue des remplacements sur la requ�te et execute
        public DataTable Execute(string Request, params object [] Params)
        {
            StringBuilder Req = new StringBuilder(Request);
            Req.Replace("$P*", "dbo.O$Properties(id,'|') as PX#,dbo.O$Relations(id) as RX#");
            Req.Replace("$PX(", "dbo.O$Properties(");
            Req.Replace("$RX(", "dbo.O$Relations(");
            Req.Replace("$P(", "dbo.O$Property(");
            Req.Replace("$PR(", "dbo.O$Parent(");
            Req.Replace("$L(", "dbo.O$Layer(");
            Req.Replace("$A(", "dbo.O$Alias_Id(");
            Req.Replace("$F(", "dbo.O$Flag(");
            Req.Replace("$S(", "dbo.O$State(");
            Req.Replace("$N(", "dbo.O$Name(");
            Req.Replace("$FN(", "dbo.O$Full_Name(");
            Req.Replace("$T(", "dbo.O$Tick(");
            Req.Replace("$R(", "dbo.O$Relation(");
            Req.Replace("$C(", "dbo.O$Child(");
            Req.Replace("$HF(", "dbo.O$Has_Flag(");
            Req.Replace("$RL(", "dbo.O$Related(");

            //Les tables qui sont pr�fix�es de $O: sont automatiquement jointes
            //Regex R = new Regex(@"(\$O\:)(?<table>\w+)");
            Regex R = new Regex(@"(\$O\:)(?<table>\S+)");  // Car on peut avoir des $ ou des points
            Func<Match, string> Eval = (M) =>
            {
                return string.Format (" {0} inner join OBJ as O on O.id={0}.id ", M.Groups["table"].Value);
            };

            string New_Req = R.Replace(Req.ToString(), new MatchEvaluator (Eval));
            return DB.Exec_Table (New_Req, Params);
        }

        public IList<Guid> Select_Ids(string Where_Clause)
        {
            List<Guid> L = new List<Guid>();
            using (var DR = New_Reader (string.Format("select Id from OBJ {0}", Where_Clause)))
            {
                while (DR.Read())
                {
                    L.Add (DR.Next_Guid);
                }
            }
            return L;
        }

        public virtual Guid NewGuid()
        {
            return Guid.NewGuid();
        }
    }

    //public class JRO_Event_Db
    //{
    //    OleDbParameter P(string Name, Object Value)
    //    {
    //        Object Val = Value;
    //        if (Value == null || (Name.StartsWith("Id") && Value.ToString() == ""))
    //        {
    //            Val = DBNull.Value;
    //        }
    //        return new OleDbParameter("@" + Name, Val);
    //    }
    //    static Base New_Db(string Key)
    //    {
    //        return new Base(Saf.Main.Parameter(Key + ".Event_Db_Connection"));
    //    }
    //    public JRO_Event_Db(string Key)
    //    {
    //        _DB = New_Db(Key);
    //        _Current = this;
    //    }
    //    Base _DB;
    //    public Base DB
    //    {
    //        get { return _DB; }
    //    }

    //    public long Add_Event(string Id_Category, DateTime UTCDate, string Id_Subject,
    //    string Message, int Priority, string Id_From, string Id_To, int Duration, long Id_Operation, string Data)
    //    {
    //        return (long)(decimal)(_DB.Lire_Ligne("Add_Event", P("Id_Category", Id_Category), P("Date", UTCDate), P("Id_Subject", Id_Subject),
    //            P("Message", Message), P("Priority", Priority), P("Id_From", Id_From), P("Id_To", Id_To), P("Duration", Duration),
    //            P("Id_Operation", Id_Operation), P("Data", Data))[0]);
    //    }

    //    public void Set_Event_Category(string Id, string Name)
    //    {
    //        //_DB.Executer("Set_Event_Category", P("Id", Id), P("Name", Name));
    //    }

    //    public DB_Row Get_Event(long Id)
    //    {
    //        return new DB_Row(_DB.Lire_Ligne("Get_Event", P("Id", Id)));
    //    }

    //    //public JRO_Event_Table Filter_Event
    //    //    (string Where_Clause, DateTime From_Date, DateTime To_Date, int Max_Count)
    //    //{
    //    //    DataTable _DT = _DB.Lire_Table("Filter_Event", P("Filter", Where_Clause),
    //    //        P("From", From_Date == DateTime.MinValue ? null : (object)From_Date), P("To", To_Date == DateTime.MaxValue ? null : (object)To_Date), P("Max_Event", Max_Count));
    //    //    JRO_Event_Table _JT = new JRO_Event_Table();
    //    //    foreach (DataRow DR in _DT.Rows)
    //    //    {
    //    //        JRO_Event E = new JRO_Event(new DB_Row(DR));
    //    //        _JT.Add(E);
    //    //    }
    //    //    return _JT;
    //    //}

    //    //public JRO_Event_Table Filter_Event
    //    //    (string Where_Clause, int Max_Count)
    //    //{
    //    //    return Filter_Event(Where_Clause, DateTime.MinValue, DateTime.MaxValue, Max_Count);
    //    //}

    //    //static JRO_Event_Db _Current;
    //    //public static JRO_Event_Db Current
    //    //{
    //    //    get { return _Current; }
    //    //}

    //    public DataTable Execute(string Request)
    //    {
    //        return _DB.Lire_Table(Request);
    //    }


    //}

    public class JRO_Db : JRO_Model_Db
    {
        public JRO_Db(string Key)
            : base(new Db_Config { Connection_String = Saf.Main.Parameter(Key + ".Db_Connection"),
                Audit_String = Saf.Main.Parameter(Key + ".Db_Audit"),
                No_Write = JR.Saf.Main.Bool_Parameter(Key + ".Db_No_Write")
            })
        {
            _Current = this;
          }
        public long Add_Command(string Command, string Issuer)
        {
            return (long)DB.Insert_Request ("insert into CMD (Command, Issuer, Status) values (@0, @1, 'W')", Command, Issuer);
        }

        public DB_Row Get_Command (long Id)
        {
            return new DB_Row(DB.Exec_Row("select * from CMD where Id = @0", Id));
        }

        public bool App_Exists
        {
            get
            {
                //return false; // pour Debug
                return DB.Exec_Int("select count(*) from APP") != 0;
            }
        }

        public DataRowCollection List_Commands (long Last_Id)
        {
            return DB.Exec_Table("select * from CMD where Id > @0 and Status = 'W' order by Id asc", Last_Id).Rows;
        }

        public void Purge_Commands(long Id)
        {
            DB.Exec("delete from CMD where Id <= @0", Id);
        }

        static JRO_Db _Current;
        public static JRO_Db Current
        {
            get { return _Current; }
        }


        public override Guid NewGuid()
        {
            return Guid.NewGuid();
        }
    }

}
