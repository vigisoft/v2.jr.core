using System;
using System.Collections.Generic;
using System.Text;
using JR.DB;
using System.Data;

namespace JR.DBO
{
    // D�finition d'une classe
    public sealed class JRO_Class : JRO_Object
    {
        public string Suffix { get; set; }
        //JRO_Model M;
        public JRO_Class(JRO_Model Model, Guid Id, string Key, string Content)
            : base(Model, Id, Key == "" ? "<Null>" : "CL$" + Key)
        {
            Cached_Bag = new Bag();
            M = Model;
            this.Content = Content;
            Extended_Tables = null;
            // Classe nulle
            if (Key == "")
            {
                Parent = null;
                this.Class = null;
            }
            else
            {

                int I_Mother = Alias.LastIndexOf('_');
                if (I_Mother < 0)
                {
                    _Mother = M.Null;
                }
                else
                {
                    _Mother = M.Aliases[Alias.Substring(0, I_Mother)] as JRO_Class;
                    foreach (JRO_Member Member in _Mother.Members.Values)
                    {
                        Members[Member.Key] = Member;
                    }
                }
                Parent = _Mother;
                this.Class = M.Null;
            }

            Suffix = Key.ToUpper();

        }

        public IList<string> Extended_Tables { get; set; }
        public JRO_Object Default_Parent { get; set; }
        public Dictionary<string, JRO_Member> Members = new Dictionary<string, JRO_Member>();
        public JRO_Member Def_Member(string Key, string Name, JRO_Member_Type Type, int Min_Cardinality, int Max_Cardinality, string Constraint, string Display)
        {
            JRO_Member M = new JRO_Member { Key = Key, Class = this, Constraint = Constraint, Display = Display, Max_Cardinality = Max_Cardinality, Min_Cardinality = Min_Cardinality, Name = Name, Type = Type };
            Members[Key] = M;
            return M;
        }

        JRO_Class _Mother;
        public JRO_Class Mother
        {
            get { return _Mother; }
        }

        long _Next_Id = 0;
        public long Next_Id
        {
            get { _Next_Id++; return _Next_Id; }
        }

        public JRO_Class Def_Class(string Key, string Name, string Content)
        {
            return M.Def_Class(Suffix + "_" + Key, Name, Content);
        }

        public JRO_Object Def_Object(string Alias, JRO_Object Parent, string Name, string Protection = "")
        {
            Guid Id_Parent = Parent == null ? Guid.Empty : Parent.Id;
            return Def_Object(Alias, Id_Parent, Name, Protection);
        }

        public JRO_Object Create_Object(JRO_Object Parent, string Name, string Protection = "")
        {
            return Def_Object("", Parent.Id, Name, Protection);
        }

        public T Create_As<T>(Guid Parent, string Name, string Protection = "") where T : JRO_Object, new()
        {
            return Def_As<T>("", Id, Name, Protection);
        }

        public JRO_Object Def_Object(string Alias, Guid Parent, string Name, string Protection = "")
        {
            return Def_As<JRO_Object>(Alias, Parent, Name, Protection);
        }

        List<DataTable> _Extended_DT = null;
        List<DataTable> Extended_DT
        {
            get
            {
                if (Extended_Tables == null) return null;
                if (_Extended_DT == null)
                {
                    _Extended_DT = new List<DataTable>();
                    foreach (var S in Extended_Tables)
                    {
                        DataTable DT = M.Read_Table(string.Format("select * from {0} where 1=0", S));
                        DT.TableName = S;
                        _Extended_DT.Add(DT);
                    }
                }
                return _Extended_DT;
            }
        }

        // Les tables �tendues xxxx doivent avoir une view V$xxxx associ�e
        // xxxxx est le nom de la premi�re table de la liste (cas des tables multiples)
        public string Full_Table_Name
        {
            get
            {
                if (Extended_Tables == null)
                {
                    return "OBJ as O";
                }
                return "V$" + Extended_Tables[0] + " as O";
            }
        }

        public T Def_As<T>(string Alias, Guid Parent, string Name, string Protection) where T : JRO_Object, new()
        {
            T O = new T();
            O.Prepare(M, Guid.Empty, Alias);
            O.Parent_Id = Parent;
            O.Class = this;
            O.Name = Name;
            O.Protection = Protection;
            M.Cache_Object(O, Protection != "");
            return O;
        }

        public T Load_As<T>(Guid Id) where T : JRO_Object, new()
        {
            T O = new T();
            O.Prepare(M, Id, "");
            O.Refresh();
            if (O.Is_Empty) return null;
            return O;
        }

        List<JRO_Class> _Children;  // Classes enfant qui peuvent �tre ajout�es en mode admin
        List<string> _Contents;  // Liste de suffixes de classes qui peuvent �tre contenues

        // Indique si une classe d�rive d'une classe racine
        public bool Is_Root(string Root_Id)
        {
            return Suffix.StartsWith(Root_Id);
        }

        public string Id_Root
        {
            get { return Suffix.Substring(0, 2); }
        }

        public string Content { get; set; }

        public List<string> Contents
        {
            get
            {
                if (_Contents == null)
                {
                    _Contents = new List<string>();
                    _Children = new List<JRO_Class>();

                    bool Child_Part = true;
                    foreach (string Sub_Cl in Content.Split(','))
                    {
                        if (Sub_Cl == "")
                        {
                            continue;
                        }
                        else if (Sub_Cl == "C")
                        {
                            Child_Part = false;
                        }
                        else
                        {
                            string Prefix = (Sub_Cl == ".") ? Suffix : Sub_Cl;
                            _Contents.Add(Prefix);
                            if (Child_Part)
                            {
                                foreach (JRO_Class Cla in M.Classes)
                                {
                                    if (Cla.Is_Root(Prefix)) _Children.Add(Cla);
                                }
                            }
                        };
                    }
                }
                return _Contents;
            }
        }

        // Scan les classes et cherche toutes celles qui peuvent contenir
        // directement celle la
        string[] _Parent_Roots = null;

        public string[] Parent_Roots
        {
            get
            {
                if (_Parent_Roots == null)
                {
                    Dictionary<string, string> _Roots = new Dictionary<string, string>();
                    foreach (JRO_Class Cla in M.Classes)
                    {
                        foreach (JRO_Class Cla_Child in Cla.Children)
                        {
                            if (Cla_Child == this)
                            {
                                _Roots[Cla.Id_Root] = Cla.Id_Root;
                                break;
                            }
                        }
                    }
                    _Parent_Roots = new string[_Roots.Count];
                    _Roots.Keys.CopyTo(_Parent_Roots, 0);
                }
                return _Parent_Roots;

            }
        }

        // Liste des classes qui peuvent �tre ajout�es
        public new List<JRO_Class> Children
        {
            get
            {
                if (_Children == null)
                {
                    _Children = new List<JRO_Class>();
                }
                return _Children;
            }
        }

        // Indique si les instances de cette classe peuvent faire des enfants
        public bool Can_Create_Children
        {
            get { return Children.Count > 0; }
        }

        // Indique si les instances de cette classe peuvent avoir des enfants
        public bool Can_Have_Children
        {
            get { return Contents.Count > 0; }
        }

        public static JRO_Class Get_Class(Guid Id)
        {
            if (!JRO_Model.Current.Objects.ContainsKey(Id)) return null;
            return JRO_Model.Current.Objects[Id] as JRO_Class;
        }

        public static JRO_Class Get_Class(string Alias)
        {
            if (!JRO_Model.Current.Aliases.ContainsKey(Alias)) return null;
            return JRO_Model.Current.Aliases[Alias] as JRO_Class;
        }

        // Supprime un objet � partir d'un DataRow
        public bool Remove_Object(DataRow Row)
        {
            Guid Oid = Row.Field<Guid>("Id");
            M.DB.Del_Object(Oid);
            M.Notify_Save();
            Cached_Bag.Clear();
            return true;
        }

        // Sauvegarde un objet � partir d'un DataRow
        public bool Save_Object(DataRow Row)
        {
            bool Saved = false;
            bool Is_New = (Row.RowState == DataRowState.Detached || Row.RowState == DataRowState.Added);
            Guid Oid = Row.Field<Guid>("Id");

            bool Save_Standard = false;
            bool Save_State = false;
            bool Save_Flags = false;

            List<List<string>> Fields = new List<List<string>>();
            List<List<string>> Keys = new List<List<string>>();
            List<List<object>> Values = new List<List<object>>();

            Dictionary<string, object> Properties = null;
            Dictionary<string, object> Old_Properties = null;
            bool Extended = Extended_DT != null;

            int T_Count = Extended ? Extended_DT.Count : 0;
            for (int T = 0; T < T_Count; T++)
            {
                Fields.Add(new List<string>());
                Keys.Add(new List<string>());
                Values.Add(new List<object>());
            }

            // Id toujours devant
            foreach (var V in Values)
            {
                V.Add(Oid);
            }

            void Save_Field(DataColumn DC, object Value)
            {
                switch (DC.ColumnName)
                {
                    case "Id":
                        return;
                    case "Name":
                        Save_Standard = true;
                        return;
                    case "Id_Parent":
                        Save_Standard = true;
                        return;
                    case "State":
                        Save_State = true;
                        return;
                    case "Flags":
                        Save_Flags = true;
                        return;
                    case "#Properties":
                        Properties = Value as Dictionary<string, object>;
                        return;
                    case "#Old_Properties":
                        Old_Properties = Value as Dictionary<string, object>;
                        return;
                }

                object Db_Value(DataTable DT)
                {
                    if (!(Value is string)) return Value;
                    if (DC.ColumnName.Length < 4) return Value;
                    if (DC.ColumnName[DC.ColumnName.Length - 3] != '_') return Value;
                    if (DC.ColumnName.EndsWith("FR")) return Value;
                    string Col_Ref = DC.ColumnName.Substring(0, DC.ColumnName.Length - 2) + "FR";
                    int Col_Row = DT.Columns.IndexOf(Col_Ref);
                    if (Col_Row >= 0)
                    {
                        object Ref = Row[Col_Row];
                        if (Ref.Equals(Value)) return DBNull.Value;
                    }
                    return Value;
                };

                for (int T = 0; T < T_Count; T++)
                {
                    var DT = Extended_DT[T];
                    var F = Fields[T];
                    var K = Keys[T];
                    var V = Values[T];
                    foreach (DataColumn C in DT.Columns)
                    {
                        if (C.ColumnName == DC.ColumnName)
                        {
                            F.Add(DC.ColumnName);
                            if (Is_New)
                            {
                                K.Add($"@{V.Count}");
                            }
                            else
                            {
                                K.Add($"{DC.ColumnName}=@{V.Count}");
                            }
                            V.Add(Db_Value(Row.Table ?? DT));
                            return;
                        }
                    }
                }
            };

            foreach (DataColumn DC in Row.Table.Columns)
            {
                object New = Row[DC.Ordinal];
                if (Is_New)
                {
                    if (DBNull.Value.Equals(New)) continue;
                }
                else if (!DC.ColumnName.StartsWith("#"))
                {
                    object Original = Row[DC.Ordinal, DataRowVersion.Original];
                    if (New.Equals(Original)) continue;
                }
                Save_Field(DC, New);
            }

            // Sauver l'objet standard
            if (Is_New)
            {
                Db.Set_Object(Oid, "", this.Id, Row.Field<string>("Name"), Row.Field<Guid>("Id_Parent"), "");
                Saved = true;
            }
            else if (Save_Standard)
            {
                Db.Move_Object(Oid, Row.Field<string>("Name"), Row.Field<Guid>("Id_Parent"));
                Saved = true;
            }

            if (Save_Flags)
            {
                Db.Set_Flags(Oid, Row.Field<string>("Flags"));
                Saved = true;
            }

            if (Save_State)
            {
                Db.Set_State(Oid, Row.Field<string>("State"));
                Saved = true;
            }

            if (Properties != null && Properties.ContainsKey("TOUCH"))
            {
                Properties.Remove("TOUCH");
                // Sauver le differentiel par rapport � old
                // Suppressions
                foreach (var P in Old_Properties)
                {
                    if (Properties.ContainsKey(P.Key)) continue;
                    if (P.Key.StartsWith("P#"))
                    {
                        Db.Reset_Property(Oid, P.Key.Substring(2));
                    }
                    else if (P.Key.StartsWith("R#"))
                    {
                        Db.Reset_Relation(Oid, P.Key.Substring(2), true);
                    }
                    Saved = true;
                }

                // Ajout / Modifications
                foreach (var P in Properties)
                {
                    bool Is_Old = Old_Properties.ContainsKey(P.Key);
                    if (Is_Old && Old_Properties[P.Key].Equals(P.Value)) continue;
                    if (P.Key.StartsWith("P#"))
                    {
                        Db.Set_Property(Oid, P.Key.Substring(2), JR.Convert.To_String(P.Value));
                        Saved = true;
                    }
                    else
                        if (P.Key.StartsWith("R#"))
                    {
                        if (Is_Old) Db.Reset_Relation(Oid, P.Key.Substring(2), true);
                        Guid G = JR.Convert.To_Guid(P.Value);
                        if (G == Guid.Empty) continue;
                        Db.Set_Relation(Oid, P.Key.Substring(2), G);
                        Saved = true;
                    }
                }

            }

            // Sauver l'objet �tendu
            for (int T = 0; T < T_Count; T++)
            {
                var DT = Extended_DT[T].TableName;
                var F = Fields[T];
                var K = Keys[T];
                var V = Values[T];
                string Req;
                bool Has_Values = V.Count > 1;
                if (Is_New)
                {
                    if (Has_Values)
                    {
                        Req = "insert into {0} (id,{1}) values (@0,{2})";
                    }
                    else
                    {
                        Req = "insert into {0} (id) values (@0)";
                    }
                }
                else
                {
                    Req = "update {0} set {2} where id=@0";
                }
                Req = string.Format(Req, DT, JR.Strings.Assembled(',', F.ToArray()), JR.Strings.Assembled(',', K.ToArray()));
                if (Is_New || Has_Values)
                {
                    Saved = true;
                    Db.DB.Exec(Req, V.ToArray());
                }
            }

            // Toucher l'objet
            if (Saved && !Is_New) Db.Touch_Object(Oid, true);
            if (Saved)
            {
                M.Notify_Save();
                Cached_Bag.Clear();
            }
            return Saved;
        }

        public JR.Bag Cached_Bag { get; set; }

        public class Class_Cache<T>
        {
            public JRO_Class Class { get; set; }
            public string Key { get; set; }
            public T Value
            {
                get
                {
                    if (Key == "") return default(T);
                    return (T)Class.Cached_Bag[Key];
                }
                set
                {
                    if (Key == "") return;
                    Class.Cached_Bag[Key] = value;
                }
            }
        }

        public Class_Cache<T> Cache<T>(string Key)
        {
            Class_Cache<T> C = new Class_Cache<T> { Class = this, Key = Key };
            return C;
        }

        // Creer une table d'instances de cette classe
        public DataTable Get_Table(string Request, params object[] Params)
        {
            // Traiter les parties g�n�riques
            // remplacer [FROM] par table as 


            DataTable DT = M.Read_Table(Request, Params);
            DT.Columns.Add("#Properties", typeof(Dictionary<string, object>));
            DT.Columns.Add("#Old_Properties", typeof(Dictionary<string, object>));
            DT.ExtendedProperties["Class"] = this;
            return DT;
        }

        DataTable _Default_Table = null;
        public DataTable Default_Table
        {
            get
            {
                if (_Default_Table == null)
                {
                    _Default_Table = Get_Table(string.Format("select * from {0} where 1=0", Full_Table_Name));
                }
                return _Default_Table;
            }
        }

        // Creer une instance isol�e non attach�e � une table pour insertion ult�rieure
        public DataRow New_Row(string Name, Guid? Parent = null)
        {
            return JRO_Row.New_Row(Default_Table, Parent == null ? Default_Parent.Id : Parent.Value, Name);
        }

        public JRO_Row Create_Row(string Name, Guid? Parent = null)
        {
            DataRow DR = JRO_Row.New_Row(Default_Table, Parent == null ? Default_Parent.Id : Parent.Value, Name);
            return new JRO_Row(DR);
        }

        public JRO_Row Load_Row(Guid Id)
        {
            using (DataTable T = Get_Table(string.Format("select * from {0} where Id=@0", Full_Table_Name), Id))
            {
                if (T.Rows.Count == 0) return null;
                return new JRO_Row(T.Rows[0]);
            }
        }

        public JRO_Row Get_Row(string Request, params object[] Params)
        {
            using (DataTable T = Get_Table(Request, Params))
            {
                if (T.Rows.Count == 0) return null;
                return new JRO_Row(T.Rows[0]);
            }
        }

    }

    // Diff�rents types de membres dans une classe
    public enum JRO_Member_Type { Text, Link, File, URL, Back_Link, Enum, Bit, Special }

    // D�finition d'un membre d'une classe
    public class JRO_Member
    {
        public JRO_Class Class { get; set; }
        public JRO_Member_Type Type { get; set; }
        public int Min_Cardinality { get; set; }
        public int Max_Cardinality { get; set; }
        public string Constraint { get; set; }
        public string Display { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
    }

}
