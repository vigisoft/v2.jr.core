using System;
using System.Data;
using System.Collections.Generic;
using JR.DB;
using System.Globalization;
using System.Net;
using JR.Saf;
using JR;
using System.Text;

namespace JRO.DBO
{
    public class JRO_Search
    {

        public class Option
        {
            public string Field = "";
            public string Op = "";
            public bool Neg = false;
            public List<object> Values = new List<object>();
            public string Text_Field = "";
            public string Text_Value = "";
        }

        public List<Option> Options = new List<Option>();

        public bool Use_Db_Lang = false;

        // Chaine de caract�re qui repr�sente la recherche en cours
        public string Current_Search_Text
        {
            get
            {
                StringBuilder SB = new StringBuilder();
                foreach (Option O in Options)
                {
                    string Val = JR.Strings.Limited_To(O.Text_Value, 100, true);
                    if (Val.Length + O.Text_Field.Length == 0) continue;
                    SB.Append(" [");
                    SB.Append(O.Text_Field);
                    SB.Append(" ");
                    if (O.Neg) SB.Append('!');
                    SB.AppendFormat ("{0} ",O.Op);
                    if (Val.Length != 0) SB.Append(Val);
                    SB.Append("]");
                }
                return SB.ToString();
            }
        }


        // crit=valeur
        Option Opt = null;  // Derni�re option

        public void Add_Option(string Field, string Op, bool Neg, object Value, string Text_Field, string Text_Value)
        {
            if (Opt == null || Opt.Field != Field)
            {
                Opt = new Option { Field = Field, Text_Field = Text_Field, Op = Op, Neg = Neg };
                Options.Add(Opt);
            }
            Add_Option(Value, Text_Value);
        }
        public void Add_Option(object Value, string Text_Value)
        {
            if (Text_Value != "")
            {
                if (Opt.Text_Value.Length > 0) Opt.Text_Value += ",";
                Opt.Text_Value += Text_Value;
            }
            Opt.Values.Add(Value);
        }

        internal string _Langue = "fr";
        internal string _Db_Langue = "FR";
        internal string _LCID = "1036";
        public bool French
        {
            get { return _Langue == "fr"; }
            set
            {
                _Langue = value ? "fr" : "gb";
                _Db_Langue = value ? "FR" : "EN";
                if (Use_Db_Lang) _Langue = _Db_Langue;
                _LCID = value ? "1036" : "1033";
            }
        }

        static string Arg (object V)
        {
            if (V == null) return " null";
            var T = V.GetType();
            if (T == typeof(DateTime)) return Db.To_Arg ((DateTime)V);
            if (T == typeof(string)) return Db.To_Arg (V.ToString());
            if (T == typeof(bool)) return (bool)V?"1":"0";
            if (T == typeof(Guid)) return Db.To_Arg (V.ToString());
            return JR.Convert.To_String (V);
        }

        static string String_Arg(string Format, string Field, List<object> Values)
        {
            string V = Values[0] as string;
            if (V == null) return "";
            if (V == "") return "";
            return String.Format (Format, Field, Db.To_Arg_No_Quote_No_Generic (V).Replace('*', '%').Replace('?', '_'));
        }

        static string First_Arg (List<object> Values)
        {
            foreach (var V in Values)
            {
                return Arg(V);
            }
            return "1";
        }

        static string All_Arg(List<object> Values)
        {
            StringBuilder SB = new StringBuilder();
            foreach (var O in Values)
            {
                if (O == null) continue;
                if (SB.Length > 0) SB.Append(',');
                SB.Append(Arg(O));
            }
            return SB.ToString();
        }

        string Full_Text(string Field, List<object> Values)
        {
            string Text = Values[0].ToString().Trim();
            if (Text == "") return "";
            List<string> Patterns = new List<string>();
            StringBuilder Where_Clause = new StringBuilder();
            StringBuilder Where_String = new StringBuilder("(");
            int I = 0;
            string[] Texts = Text.Split(' ');
            foreach (string W in string.Format(Field, _Langue).Split(','))
            {
                if (Where_String.Length > 1) Where_String.Append(" or");
                Where_String.AppendFormat(" isnull({0},'') like @A{{0}}", W);
            }
            Where_String.Append(")");
            foreach (string T in Texts)
            {
                string TT = T.Trim();
                if (TT.Length == 0) continue;
                Patterns.Add(Db.Like_String(TT));
                if (Where_Clause.Length > 0) Where_Clause.Append(" and");
                Where_Clause.AppendFormat(Where_String.ToString(), I);
                I++;
            }

            int N = 0;
            foreach (string A in Patterns)
            {
                Where_Clause.Replace(string.Format("@A{0}", N), Db.To_Arg(A));
                N++;
            }
            return Where_Clause.ToString();
        }

        public string Computed_Where_Clause ()
        {
            StringBuilder Where_Clause = new StringBuilder();

            // gestion des autres options
            foreach (var P in Options)
            {
                string Key = string.Format(P.Field, _Langue);
                Func<string, string> Null_Key = (Default_String_Value) =>
                    {
                        foreach (var V in P.Values)
                        {
                            if (V == null) return Key;
                            var T = V.GetType();
                            if (T == typeof(DateTime)) return Key;
                            if (T == typeof(string)) return String.Format ("isnull({0},'{1}')", Key, Default_String_Value);
                            if (T == typeof(Guid)) return String.Format("isnull({0},'{1}')", Key, Guid.Empty);
                            break;
                        }
                        return String.Format("isnull({0},0)", Key);
                    };
                bool Neg = P.Neg;
                StringBuilder Clause = new StringBuilder();
                switch (P.Op)
                {
                    case "I":   // in
                        {
                            // La premi�re valeur nulle indique que la pr�c�dente est la valeur par d�faut
                            object Defaut_Value = "";
                            int R = 0;
                            foreach (var V in P.Values)
                            {
                                if (R >0 && V == null)
                                {
                                    Defaut_Value = P.Values[R - 1];
                                    break;
                                }
                                R++;
                            }
                            // La premi�re valeur qui finit par ~D est le defaut
                            Clause.AppendFormat("({0} in ({1}))", Null_Key(Defaut_Value.ToString()), All_Arg(P.Values));
                        }
                        break;
                    case "T":   // Full Text
                        Clause.Append (Full_Text (Key, P.Values));
                        break;
                    case "M":   // Mid
                        Clause.Append(String_Arg("(isnull({0},'') like '%{1}%')", Key, P.Values));
                        break;
                    case "L":   // Like
                        Clause.Append(String_Arg("(isnull({0},'') like '{1}')", Key, P.Values));
                        break;
                    case "S":   // Start
                        Clause.Append(String_Arg("(isnull({0},'') like '{1}%')", Key, P.Values));
                        break;
                    case "E":   // End
                        Clause.Append(String_Arg("(isnull({0},'') like '%{1}')", Key, P.Values));
                        break;
                    case "B":   // Between
                        Clause.AppendFormat("({0} between {1} and {2})", Null_Key(""), Arg(P.Values[0]), Arg(P.Values[1]));
                        break;
                    case "@":   // literal
                        Clause.AppendFormat("({0})", P.Values[0]);
                        break;
                    default:    // =
                        Clause.AppendFormat("({0}{1}{2})", Null_Key(""), P.Op, First_Arg(P.Values));
                        break;
                }
                if (Clause.Length == 0) continue;
                if (Where_Clause.Length > 0) Where_Clause.Append(" and");
                Where_Clause.Append("(");
                if (Neg) Where_Clause.Append(" not ");
                Where_Clause.Append(Clause);
                Where_Clause.Append(")");
            }
            if (Where_Clause.Length == 0) return "(1=1)";
            return Where_Clause.ToString();

        }

    }
}
