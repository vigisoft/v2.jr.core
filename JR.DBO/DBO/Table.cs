using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace JR.DBO
{
    public class JRO_Table
    {
        List<JRO_Object> _List;
        List<JRO_Object> _Filtered_List;
        public JRO_Table()
            : base()
        {
            _List = new List<JRO_Object>();
            _Filtered_List = new List<JRO_Object>();
        }

        public List<JRO_Object> List
        {
            get { return _List; }
        }

        struct Term
        {
            public string Text;
            public bool Required;
            public bool Excluded;
            public string Col;
        }
        public virtual bool Good_Row (JRO_Object O)
        {
            return true;
        }

        static bool Match (string Value, Term T)
        {
            return (Value.ToUpper().Contains(T.Text));
        }

        void Filter()
        {
            if (!_Need_Filter) return;

            _Filtered_List = new List<JRO_Object>();

            string[] Texts = JR.Strings.Official (_Filter_String).Split(' ');
            Term[] Terms = new Term[Texts.Length];

            // Si le terme commence par + il est obligatoire
            // Si le terme commence par [col]
            //   on ne cherche que dans la colonne correspondante
            for (int I = 0; I < Texts.Length; I++)
            {
                Terms[I].Text = Texts[I];
                Terms[I].Required = false;
                Terms[I].Excluded = false;
                Terms[I].Col = "";
                string Text = Terms[I].Text;
                if (Text.StartsWith("+"))
                {
                    Text = Text.Substring(1);
                    Terms[I].Text = Text;
                    Terms[I].Required = true;
                } else
                if (Text.StartsWith("-"))
                {
                    Text = Text.Substring(1);
                    Terms[I].Text = Text;
                    Terms[I].Excluded = true;
                }
                if (Text.StartsWith("["))
                {
                    int C_Par = Text.IndexOf(']');
                    if (C_Par > 0)
                    {
                        Terms[I].Text = Text.Substring(C_Par+1);
                        Terms[I].Col = Text.Substring(1, C_Par - 1);
                    }
                }
            }

            foreach (JRO_Object O in _List)
            {

                bool Hidden = !Good_Row(O);

                if (!Hidden && _Filter_String != "")
                {
                    Hidden = true;
                    foreach (Term T in Terms)
                    {
                        if (T.Required) Hidden = true;
                        if (Hidden)
                        {
                            if (T.Col == "")
                            {
                                foreach (JRO_Property P in O.Properties.Values)
                                {
                                    if (Match(P.Value, T)) {Hidden = false;goto Ok;}
                                }
                                if (Match(O.Name, T)) { Hidden = false; goto Ok; }
                                if (Match(O.Id.ToString(), T)) { Hidden = false; goto Ok; }
                            }
                            else
                            {
                                if (Match(O["#" + T.Col], T))
                                {
                                    Hidden = false;
                                    goto Ok;
                                }
                            }
                        }
                    Ok:

                        if (T.Required && Hidden) goto Found;
                    }

                }
            Found:
                if (!Hidden) _Filtered_List.Add(O);
            }

        }

        public List<JRO_Object> Filtered_List
        {
            get
            {
                Filter();
                return _Filtered_List; 
            }
        }

        string _Filter_String = "";
        bool _Need_Filter = true;

        public string Filter_String
        {
            get { return _Filter_String; }
            set { if (value == _Filter_String) return; _Need_Filter = true; _Filter_String = value; }
        }

        public int Add(JRO_Object Object)
        {
            _Need_Filter = true;
            _List.Add(Object);
            return _List.Count-1;
        }

        public JRO_Object Find_Id(Guid Id)
        {
            foreach (JRO_Object O in _List)
            {
                if (O.Id == Id) return O;
            }
            return null;
        }

        public JRO_Object Find_Row (int Row)
        {
            if (Row >= _List.Count) return null;
            return _List[Row];
        }

    }

}
