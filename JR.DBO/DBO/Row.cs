using System;
using System.Collections.Generic;
using System.Text;
using JR.DB;
using System.Data;
using System.Linq;

namespace JR.DBO
{

    // JRO_Row est un objet simplifi� bas� sur un datarow
    public class JRO_Row:I_Debug
    {

        public JRO_Row(DataRow Data_Row):this()
        {
            Row = Data_Row;
            foreach (var JB in JSON_Bags)
            {
                Deserialize_Bag(JB);
            }
            After_Load();
        }

        public JRO_Row()
        {
            In_Memory = false;
            Is_Dirty = false;
            Save_With_Parent = false;
        }

        DataRow _Row = null;
        public DataRow Row
        {
            get { return _Row; }
            set
            {
                _Row = value;
                Properties = Row["#Properties"] as Dictionary<string, object>;
                Old_Properties = Row["#Old_Properties"] as Dictionary<string, object>;
                Class = Row.Table.ExtendedProperties["Class"] as JRO_Class;
            }
        }

        public T Value<T>(string Field)
        {
            try
            {
                int Col = Row.Table.Columns.IndexOf(Field);
                if (Col >= 0)
                {
                    if (Row.IsNull(Col))
                    {
                        if (typeof (T) == typeof (object))
                        {
                            return (T)JR.Convert.Default (Row.Table.Columns[Col].DataType);
                        }
                        return (T)JR.Convert.Default<T>();
                    }
                    return JR.Convert.To_Type<T>(Row[Col]);
                } else
                if (Field.StartsWith("P#"))
                {
                    Load_Properties();
                    object V;
                    if (Properties.TryGetValue(Field, out V)) return JR.Convert.To_Type<T>(V);
                } else
                if (Field.StartsWith("R#"))
                {
                    Load_Properties();
                    object V;
                    if (Properties.TryGetValue(Field, out V)) return JR.Convert.To_Type<T>(V);
                } else
                if (Field.StartsWith("S#"))
                {
                    return Spec_Value<T>(Field);
                }
                else
                {
                    if (Bag != null)
                    {
                        object V;
                        if (Bag.TryGetValue(Field, out V)) return JR.Convert.To_Type<T>(V);
                    }
                }
            }
            catch
            {
            }
            return (T)JR.Convert.Default<T>();
        }

        public virtual T Spec_Value<T>(string Field)
        {
            return (T)JR.Convert.Default<T>();
        }


        Dictionary<string, object> Old_Properties;
        Dictionary<string, object> Properties;

        void Load_Properties()
        {
            if (Old_Properties != null) return;
            Properties = new Dictionary<string, object>();
            Old_Properties = new Dictionary<string, object>();

            Action<bool> Load_Props = (Relations) =>
                {
                    int Col = Row.Table.Columns.IndexOf(Relations ? "RX#" : "PX#");
                    if (Col >= 0)
                    {
                        if (Row.IsNull(Col)) return;
                        JR.Strings.Cursor C = new Strings.Cursor((string)Row[Col]);
                        string Sep = new string(C.Next_Char(), 1);
                        while (!C.End)
                        {
                            string Id_Property = C.Next(Sep);
                            string Value = C.Next(Sep);
                            if (Relations)
                            {
                                Properties["R#" + Id_Property] = JR.Convert.To_Guid(Value);
                            }
                            else
                            {
                                Properties["P#" + Id_Property] = Value;
                            }

                        }
                        return;
                    }
                    DataRowCollection Props = Relations ? Class.Db.Get_Relations(Id) : Class.Db.Get_Properties(Id);
                    if (Props != null)
                    {
                        string Prev_Prop = "";
                        foreach (DataRow P in Props)
                        {
                            string Prop = P["Id_Property"].ToString();
                            if (Prev_Prop == Prop)
                            {
                                // Logique � d�velopper ...
                                Properties[(Relations ? "R" : "P") + "#" + Prop] = P["Value"];
                            }
                            else
                            {
                                Properties[(Relations ? "R" : "P") + "#" + Prop] = P["Value"];
                            }
                            Prev_Prop = Prop;
                        };
                    };
                };
            Load_Props(false);
            Load_Props(true);
            Sync_Properties();

        }

        void Sync_Properties()
        {
            if (Properties == null) return;
            Old_Properties.Clear();
            foreach (var P in Properties)
            {
                Old_Properties.Add(P.Key, P.Value);
            }
        }

        public void Reset_Property(string Pattern)
        {
            foreach (string Key in List_Properties(Pattern))
            {
                Properties.Remove(Key);
                Properties["TOUCH"] = true;
            }
        }

        public void Set_Property(string Pattern, params object[] Values)
        {
            Load_Properties();
            string Digits = new string('0', (Values.Length - 1).ToString().Length);
            for (int R = 0; R < Values.Length; R++)
            {
                object V = Values[R];
                string K = string.Format(Pattern, R.ToString(Digits));
                if (V == null) Properties.Remove(K); else Properties[K] = V;
                Properties["TOUCH"] = true;
            }
        }

        public void Set_Properties <T> ( string Pattern, IEnumerable<T> Values)
        {
            Load_Properties();
            List<T> L = (from X in Values select X).ToList<T>();
            string Digits = new string('0', (L.Count - 1).ToString().Length);
            for (int R = 0; R < L.Count; R++)
            {
                object V = L[R];
                string K = string.Format(Pattern, R.ToString(Digits));
                if (V == null) Properties.Remove(K); else Properties[K] = V;
                Properties["TOUCH"] = true;
            }
        }

        public IList<string> List_Properties(string Pattern)
        {
            Load_Properties();
            List<string> Keys = new List<string>();
            foreach (string Key in Properties.Keys)
            {
                if (JR.Strings.Match(Key, Pattern))
                {
                    Keys.Add(Key);
                }
            }
            Keys.Sort();
            return Keys;
        }

        public void Set_Null(string Field)
        {
            int Col = Row.Table.Columns.IndexOf(Field);
            if (Col >= 0)
            {
                Row[Col] = DBNull.Value;
            }
        }

        public void Set_Relations(string Field, IList<Guid> Ids)
        {
            Reset_Property("R#" + Field + "!*");
            int Row = 1;
            foreach (Guid Id in Ids)
            {
                Set<Guid>(string.Format("R#{0}!{1:d4}", Field, Row), Id);
                Row++;
            }
        }

        public IList<Guid> Get_Relations(string Field)
        {
            List<Guid> L = new List<Guid>();
            foreach (string F in List_Properties("R#" + Field + "!*"))
            {
                L.Add(Guid(F));
            }
            return L;
        }

        private class MultiLink
        {
            public MultiLink (JRO_Row O, string Field)
            {
                this.Field = Field;
                this.O = O;
                foreach (string F in O.List_Properties(Prefix("*")))
                {
                    L.Add(O.Guid(F));
                }
            }

            // Bourrin mais efficace
            int Free_Row
            {
                get 
                {
                    int FRow = 0;
                    foreach (string F in O.List_Properties(Prefix("*")))
                    {
                        int Row = JR.Convert.To_Integer(F.Split('!')[1]);
                        if (Row == FRow) FRow = Row + 1;
                    }
                    return FRow;
                }
            }

            string Prefix (string Suffix = "") {return "R#" + Field + "!" + Suffix;}
            public string Field { get; set; }
            public JRO_Row O { get; set; }
            List<Guid> L = new List<Guid>();
            public void Link(Guid Value)
            {
                if (Is_Linked(Value)) return;
                O.Set<Guid>(string.Format("R#{0}!{1:d5}", Field, Free_Row), Value);
                L.Add(Value);
            }

            public void UnLink(Guid Value)
            {
                if (!Is_Linked(Value)) return;
                foreach (string F in O.List_Properties(Prefix("*")))
                {
                    if (O.Guid(F) == Value)
                    {
                        O.Reset_Property(F);
                        break;
                    }
                }
                L.Remove(Value);
            }

            public void UnLinkAll()
            {
                O.Reset_Property(Prefix("*"));
                L.Clear();
            }

            public bool Is_Linked(Guid Value)
            {
                return L.Contains (Value);
            }

            public IList<Guid> Links
            {
                get { return L; }
            }

            public int Link_Count
            {
                get { return L.Count; }
            }

        }

        Dictionary<string, MultiLink> MultiLinks = new Dictionary<string, MultiLink>();
        MultiLink Get_Multi_Link(string Field)
        {
            MultiLink L = null;
            if (!MultiLinks.TryGetValue (Field, out L))
            {
                L= new MultiLink (this, Field);
                MultiLinks.Add(Field, L);
            }
            return L;
        }

        public void Link(string Field, Guid Object)
        {
            Get_Multi_Link(Field).Link(Object);
        }

        public void UnLink(string Field, Guid Object)
        {
            Get_Multi_Link(Field).UnLink(Object);
        }

        public void UnLinkAll(string Field)
        {
            Get_Multi_Link(Field).UnLinkAll();
        }

        public bool Is_Linked(string Field, Guid Object)
        {
            return Get_Multi_Link(Field).Is_Linked(Object); ;
        }

        public IList<Guid> Links(string Field)
        {
            return Get_Multi_Link(Field).Links;
        }

        public int Link_Count (string Field)
        {
            return Get_Multi_Link(Field).Link_Count;
        }

        HashSet<string> Json_Bags = new HashSet<string>();
        Bag Bag { get; set; }

        public void Set<T>(string Field, T Value)
        {
            int Col = Row.Table.Columns.IndexOf(Field);
            if (Col >=0)
            {
                T Val;
                if (Row.IsNull(Col))
                {
                    Val = (T)JR.Convert.Default<T>();
                } else
                {
                   Val = JR.Convert.To_Type<T>(Row[Col]);
                };

                if (Val == null)
                {
                    if (Value == null) return;
                } else if (Val.Equals(Value)) return;

                // Si date nulle, mettre dbnull
                if (Value != null && Value.Equals(DateTime.MinValue))
                {
                    Row[Col] = DBNull.Value;
                }
                else
                {
                    Row.SetField<T>(Col, Value);
                }
            }
            else if (Field.StartsWith("P#"))
            {
                Set_Property(Field, JR.Convert.To_String(Value));
            }
            else if (Field.StartsWith("R#"))
            {
                Set_Property(Field, JR.Convert.To_Guid(Value));
            }
            else
            {
                if (Bag == null) Bag = new Bag();
                Bag[Field] = Value;
            }
        }

        // D�tecte un changement de valeur avant sauvegarde par exemple
        // Ne traite pas encore les propri�t�s
        public bool Is_Changed(string Field)
        {
            if (Is_New) return true;
            int Col = Row.Table.Columns.IndexOf(Field);
            if (Col >= 0)
            {
                object New = Row[Col];
                object Original = Row[Col, DataRowVersion.Original];
                return !New.Equals(Original);
            }
            else if (Field.StartsWith("P#"))
            {
                if (Old_Properties == null) return false;
                if (Properties.ContainsKey(Field) != Old_Properties.ContainsKey(Field)) return true;
                if (!Properties.ContainsKey(Field)) return false;
                return !Old_Properties[Field].Equals(Properties[Field]);
            }
            return false;
        }

        public string String(string Field)
        {
            return Value<string>(Field);
        }

        public string Lang_String(string Field, string Lang, string Default_Lang = "FR")
        {
            if (Lang != Default_Lang)
            {
                string Val = Value<string>(Field + Lang);
                if (Val != "") return Val;
            }
            return Value<string>(Field + Default_Lang);
        }

        public string Format(string Format, Boolean Empty_If_Default = false)
        {
            Func<string, object> Solver = (Param) =>
            {
                // Un parametre est de la forme {Param[:format]}
                if (Param == "") return null;
                return Value<object>(Param);
            };

            return JR.Strings.Generic_Format (Format, Solver, Empty_If_Default);
        }

        public int Int(string Field)
        {
            return Value<int>(Field);
        }

        public double Float(string Field)
        {
            return Value<double>(Field);
        }

        public decimal Decimal(string Field)
        {
            return (decimal)Value<double>(Field);
        }

        public long Long(string Field)
        {
            return Value<long>(Field);
        }

        public DateTime Date(string Field)
        {
            return Value<DateTime>(Field);
        }

        public bool Bool(string Field)
        {
            return Value<bool>(Field);
        }

        public Guid Guid(string Field)
        {
            return Value<Guid>(Field);
        }

        public bool Is_New
        {
            get { return Row.RowState == DataRowState.Detached || Row.RowState == DataRowState.Added; }
        }

        public List<JRO_Row> Children { get; set; }
        public void Register_Child(JRO_Row Child)
        {
            if (Children == null) Children = new List<JRO_Row>();
            if (Children.Contains(Child)) return;
            Children.Add(Child);
        }

        public void Unregister_Child(JRO_Row Child)
        {
            if (Children == null) return;
            Children.Remove (Child);
        }

        public string Id_Or_Name
        {
            get
            {
                var N = Name;
                if (N.Length > 0) return N;
                return Id.ToString();
            }
        }

        public Guid Id { get { return Guid("Id"); } }
        public JRO_Row Parent { get; set; }
        public Guid Parent_Id 
        {
            get { return Guid("Id_Parent"); } 
            set { Set<Guid> ("Id_Parent", value); }
        }
        public string Name 
        { 
            get { return String("Name"); }
            set { Set<string>("Name", value); }
        }
        public virtual bool Removable { get { return String("Protection") == ""; } }
        public virtual void Remove()
        {
            if (Is_New) return;
            Class.Remove_Object(Row);
            Cleanup();
            Row.Table.Rows.Remove(Row);
        }

        internal JRO_Class Class { get; set; }

        public virtual void Cleanup()
        {
        }

        public bool In_Memory { get; set; }
        public bool Is_Dirty { get; set; }
        public bool Save_With_Parent { get; set; }

        // Indique si la sauvegarde doit �tre poursuivie
        public virtual bool Before_Save()
        {
            return true;
        }

        public virtual void After_Save()
        {
        }

        public virtual void After_Load()
        {
        }

        public virtual string [] JSON_Bags
        {
            get
            {
                return new string[] { };
            }
        }

        void Deserialize_Bag(string Prefix)
        {
            var B = new Bag();
            if (JR.Json.Populate(B, this.String(Prefix)))
            {
                Bag = new Bag();
                foreach (var NB in B)
                {
                    Bag.Add(Prefix + "_" + NB.Key, NB.Value);
                }
            }
        }

        void Serialize_Bag (string Prefix)
        {
            var B = new Bag();
            // Serialiser le bag
            foreach (var NB in Bag)
            {
                if (NB.Key.StartsWith(Prefix + "_") && !JR.Convert.Is_Default(NB.Value))
                {
                    B.Add(NB.Key.Substring(Prefix.Length + 1), NB.Value);
                }
            }
            if (B.Count > 0)
            {
                Set<string>(Prefix, JR.Json.To_Json(B));
            }
            else
            {
                Set_Null(Prefix);
            }
        }

        public virtual bool Save(string User = "")
        {
            if (!Before_Save()) return false;
            if (Bag != null)
            {
                foreach (var B in JSON_Bags)
                {
                    Serialize_Bag(B);
                }
            }

            // Sauvegarde diff�r�e
            if (Parent != null && Parent.Is_New)
            {
                if (Is_New && !Save_With_Parent)
                {
                    Row.Table.Rows.Add(Row);
                }
                Parent.Register_Child(this);
                Save_With_Parent = true;
                Is_Dirty = true;
                return true;
            }
            bool Saved = false;
            if (Class != null)
            {
                bool Do_Cleanup = !Is_New;
                Row["#Properties"] = Properties;
                Row["#Old_Properties"] = Old_Properties;

                // G�n�ration d'un nom auto juste avant enregistrement
                if (Is_New && Name.EndsWith("++"))
                {
                    JR.Strings.Cursor C = new Strings.Cursor(Name);
                    string Prefix = C.Next("+");
                    C.Skip(-1);
                    string Nums = C.Tail();
                    string Request = string.Format("select max(Name) from {0} where id_Parent=@0 and Name like @1", Class.Full_Table_Name);
                    string Last_Name = Class.Db.DB.Exec_String(Request, Parent_Id, Name.Replace("+", "[0-9]"));
                    int Max_Key = Last_Name == "" ? 0 : JR.Convert.To_Integer(Last_Name.Substring(Name.Length - Nums.Length, Nums.Length));
                    Name = Prefix + (Max_Key + 1).ToString(Nums.Replace("+", "0"));
                }
                if (!In_Memory) Saved = Class.Save_Object(Row);
                Sync_Properties();
                if (Saved && Do_Cleanup) Cleanup();
                if (Saved) After_Save();
            }
            if (Is_New && !Save_With_Parent)
            {
                Row.Table.Rows.Add (Row);
            }
            Row.AcceptChanges();
            if (Saved) Is_Dirty = true;
            if (Save_With_Parent)
            {
                //Parent.Unregister_Child(this);
                Save_With_Parent = false;
            }
            // Sauvegarde diff�r�e des enfants
            if (Children != null)
            {
                foreach (var C in Children)
                {
                    C.Save(User);
                }
                Children = null;
            }
            return Saved;
        }


        // Creer une instance selon le schema d'un table
        public static DataRow New_Row(DataTable T, Guid Parent, string Name, params object[] Properties)
        {
            JRO_Class Class = T.ExtendedProperties["Class"] as JRO_Class;
            if (Parent == System.Guid.Empty) Parent = Class.M.Fo_App.Id;
            // Name = xxxxx_????? : num�rotation auto selon le parent
            if (Name.EndsWith("??"))
            {
                JR.Strings.Cursor C = new Strings.Cursor(Name);
                string Prefix = C.Next("?");
                C.Skip(-1);
                string Nums = C.Tail();
                string Request = string.Format("select max(Name) from {0} where id_Parent=@0 and Name like @1", Class.Full_Table_Name);
                string Last_Name = Class.Db.DB.Exec_String(Request, Parent, Name.Replace("?", "[0-9]"));
                int Max_Key = Last_Name == ""?0:JR.Convert.To_Integer(Last_Name.Substring(Name.Length -Nums.Length, Nums.Length));
                Name = Prefix + (Max_Key + 1).ToString (Nums.Replace("?", "0"));
            }
            DataRow R = T.NewRow();
            R["Id"] = System.Guid.NewGuid();
            R["Id_Parent"] = Parent;
            R["Name"] = Name;
            for (int I = 0; I + 1 < Properties.Length; I += 2)
            {
                R[Properties[I].ToString()] = Properties[I + 1];
            }
            return R;
        }

        public static void Copy (DataRow Source, DataRow Dest)
        {
            foreach (DataColumn C in Source.Table.Columns)
            {
                switch (C.ColumnName)
                {
                    case "Id":
                    case "Id_Parent":
                    case "Name":
                        break;
                    default:
                        if (C.ColumnName.Length > 2 && C.ColumnName[1] == '#') break;
                        Dest[C.Ordinal] = Source[C.Ordinal];
                        break;
                }
            }
        }



        // Copie les attributs et les propri�t�s � des fins de clipboard par exemple
        public void Copy_From(JRO_Row Source, string New_Name = "")
        {
            Row = New_Row(Source.Row.Table, Source.Parent_Id, New_Name);
            Copy(Source.Row, Row);

            // Cloner les properties
            Source.Load_Properties();
            Properties = new Dictionary<string, object>(Source.Properties);
            Old_Properties = new Dictionary<string, object>(Source.Old_Properties);
        }

        // Copie les attributs et les propri�t�s � des fins de clipboard par exemple
        public void Copy_To(JRO_Row Dest, IList<string> Props)
        {
            foreach (string P in Props)
            {
                object V = Value<object>(P);
                Dest.Set<object>(P, V);
            }
        }

        public string HTML_Debug_Info
        {
            get 
            {
                using (Db_Reader DR = new Db_Reader(Row))
                {
                    return DR.Full_HTML_Debug_Info;
                }
            }

        }

        public static string Gen_Name (string Prefix)
        {
            return string.Format("{0}_{1:yyMMddHHmmss}", Prefix, DateTime.Now);
        }

        // La syntaxe est Field:Flag
        public void Set_Flag(bool Value, string Field_Flag, bool Case_Sensitive = false)
        {
            JR.Strings.Cursor C = new Strings.Cursor(Field_Flag);
            string Field = C.Next(":");
            string Flag = C.Tail();
            if (Flag == "") return;
            string Val = String(Field);
            if (!Case_Sensitive)
            {
                Val = Val.ToUpper();
                Flag = Flag.ToUpper();
            }
            Val = Val.Replace(Flag, "");
            Set<string>(Field, Value ? (Val + Flag) : Val);
        }

        // La syntaxe est Field:Flag
        public bool Has_Flag(string Field_Flag, bool Case_Sensitive = false)
        {
            JR.Strings.Cursor C = new Strings.Cursor(Field_Flag);
            string Field = C.Next(":");
            string Flag = C.Tail();
            if (Flag == "") return false;
            string Val = String(Field);
            if (!Case_Sensitive)
            {
                Val = Val.ToUpper();
                Flag = Flag.ToUpper();
            }
            return Val.IndexOf(Flag) >= 0;
        }

        public bool Is_Unique (string Field_Expression, string Value)
        {
            return Class.Db.DB.Exec_Int ("select count(*) from {0} where (O.Id <> @1) and ({2} = @3)", Class.Full_Table_Name, Id, Field_Expression, Value) == 0;
        }
    }

    public class JRO_List<T> : List<T> where T : JRO_Row, new ()
    {
        Dictionary<Guid, T> Dic = null;

        // Liste parente dont sont issus les objets
        // Les objets supprim�s ou ajout�s dans la liste fille sont ajout�s ou supprim�s dans la liste m�re
        public JRO_List<T> Parent_List = null;

        public T Get_By_Id (Guid Id)
        {
            if (Dic == null)
            {
                Dic = new Dictionary<Guid, T>();
                foreach (var O in this)
                {
                    Dic[O.Id] = O;
                }
            }
            T Result = null;
            Dic.TryGetValue(Id, out Result);
            return Result;
        }

        void On_Change() { Dic = null; }

        public void Remove(T Item, bool Notify)
        {
            Remove(Item);
            if (!Notify) return;
            On_Change();
            if (Parent_List != null) Parent_List.Remove(Item, true);
        }
        public void Add (T Item, bool Notify)
        {
            Add(Item);
            if (!Notify) return;
            On_Change();
            if (Parent_List != null) Parent_List.Add(Item, true);
        }
        DataTable DT = null;
        public DataTable Source
        {
            get { return DT; }
            set
            {
                DT = value;
                this.Clear();
                foreach (DataRow DR in DT.Rows)
                {
                    Add(new T { Row = DR });
                }
                DT.ExtendedProperties["JRO_List"] = this;
            }
        }

        // Prefixe automatiquement les colonnes lorsque plusieurs tables sont li�es et qu'il y a des conflits
        public void Auto_Prefix(params string [] Prefix)
        {
            int R = -1;
            foreach (DataColumn C in Source.Columns)
            {
                string CN = C.ColumnName;
                if (CN.Length == 3 && CN.StartsWith("Id")) R++;
                if (R >= 0 && CN[0] != '#')
                {
                    // Supprimer l'�ventuel num�ro de fin, li� aux doublons de colonnes
                    if (char.IsDigit(CN[CN.Length - 1])) CN = CN.Remove(CN.Length - 1);
                    // AJouter le prefixe
                    C.ColumnName = Prefix[R] + "_" + CN;
                }
            }
        }

        public JRO_Row Parent
        {
            set
            {
                foreach (var R in this)
                {
                    R.Parent = value;
                }
            }
        }

        public static JRO_List<T> List(DataTable Source)
        {
            return Source.ExtendedProperties["JRO_List"] as JRO_List<T>;
        }

        public T Get_By_Row(DataRow Row)
        {
            if (Row == null) return null;
            foreach (var R in this)
            {
                if (R.Row == Row) return R;
            }
            return null;
        }

        //public T Get_By_Id(Guid Id)
        //{
        //    foreach (var R in this)
        //    {
        //        if (R.Id == Id) return R;
        //    }
        //    return null;
        //}

        public int Index(T Item)
        {
            return IndexOf(Item);
        }

        public JRO_List<T> Child_List (Func<T,bool> Is_Ok)
        {
            var L = new JRO_List<T>();
            L.Parent_List = this;
            foreach (var I in this)
            {
                if (Is_Ok(I)) L.Add(I);
            }
            return L;
        }
    }
}
