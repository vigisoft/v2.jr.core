﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Reporting.WinForms;

namespace JR.Report
{
    public class Report
    {
        LocalReport LR = new LocalReport();
        List<ReportParameter> P = new List<ReportParameter>();

        public Report (string Path)
        {
            LR.ReportPath = Path;
        }

        public void Add_Source (string Key, object Datas)
        {
            LR.DataSources.Add(new ReportDataSource(Key, Datas));
        }

        public void Add_Param (string Key, string Format, params object [] Values)
        {
            P.Add(new ReportParameter(Key, string.Format(Format, Values)));
        }

        // Formats dispo : Excel/EXCELOPENXML/IMAGE/PDF/Word/WORDOPENXML
        public byte [] Render (string Format = "Excel")
        {
            LR.SetParameters(P);
            //foreach (var Ext in LR.ListRenderingExtensions())
            //{
            //    System.Diagnostics.Debug.WriteLine("Supported Report Extension : {0} / {1}", Ext.Name, Ext.Name);
            //}
            return LR.Render(Format);
        }

            //LocalReport LR = new LocalReport ();
            ////var RE = LR.ListRenderingExtensions();
            //LR.ReportPath = JR.Saf.Main.Real_File_Name("Reports\\Membres.rdlc", "#SYSTEM");
            //LR.DataSources.Add(new ReportDataSource("Membres", List));
            //List<ReportParameter> P = new List<ReportParameter>();
            ////P.Add(new ReportParameter("Titre", string.Format("Collection {0} pour {1} [{3} {2}] - Rétribution de {4}", _R.Code_Collection, _R.Societe, _R.Nom, _R.Prenom, _R.Periode)));
            ////P.Add(new ReportParameter("Coord_P12", Main.Parameter("P12.COORD")));
            ////P.Add(new ReportParameter("NBI", _R.Nb_Images.ToString()));
            ////P.Add(new ReportParameter("MTV", string.Format("{0:#########.00} €", _R.Montant)));
            ////P.Add(new ReportParameter("TXR", string.Format("{0:##.0}%", _R.Taux)));
            ////P.Add(new ReportParameter("MTR", string.Format("{0:#########.00} €", _R.Retrib)));
            //LR.SetParameters(P);
            //byte[] Content = LR.Render("Excel");  // "Excel/EXCELOPENXML/IMAGE/PDF/Word/WORDOPENXML";
            //Helper.Desktop.Download_Bytes(Content, "application/vnd.ms-excel", "Membres_SMMOF.xls");
        
    }
}
