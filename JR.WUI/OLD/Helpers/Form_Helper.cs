using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Interfaces;
using Gizmox.WebGUI.Forms.Hosts;
using System.Web;
using System.Collections;
using Gizmox.WebGUI.Common.Gateways;
using System.IO;

namespace JR.WUI
{

    public class Form_Helper
    {

        Form D;
        TreeView Nav_Tree;
        Control Page;
        Label L_Title;
        Panel P_Page;
        public Form_Helper(Form Desktop)
        {
            D = Desktop;
            foreach (var C in D.Controls.Find("NAV", true))
            {
                Nav_Tree = C as TreeView;
                break;
            }

            foreach (var C in D.Controls.Find("TITLE", true))
            {
                L_Title = C as Label;
                break;
            }

            foreach (var C in D.Controls.Find("P_PAGE", true))
            {
                P_Page = C as Panel;
                break;
            }

        }

        // Préparation finale du desktop
        public void Finalize_Desktop()
        {
            if (Nav_Tree != null)
            {
                Nav_Tree.AfterSelect += new TreeViewEventHandler(Nav_Tree_AfterSelect);
                Nav_Tree.BeforeSelect += new TreeViewCancelEventHandler(Nav_Tree_BeforeSelect);
                Nav_Tree.ExpandAll();
            }
            Display_Selected_Page();
        }

        TreeNode PrevNode;
        void Nav_Tree_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            PrevNode = Nav_Tree.SelectedNode;
            I_Page IP = Page as I_Page;
            if (IP != null && !IP.Helper.Can_Quit) e.Cancel = true;
        }

        void Nav_Tree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode N = e.Node;
            while (N.Nodes.Count > 0 && N.Tag == null) N = N.Nodes[0];
            if (N.Tag == null) return;
            if (N != e.Node)
            {
                Nav_Tree.SelectedNode = N;
            }
            else
            {
                Display_Selected_Page();
            }
        }

        public TreeNode Add_Nav_Node(TreeNode Parent, string Title, Type Page_Type, object Context = null)
        {
            TreeNode TP = new TreeNode(Title);
            if (Page_Type != null)
            {
                TP.Tag = new Page_Ref {Title = Title, Page_Type = Page_Type, Context = Context };
            }
            if (Parent == null) Nav_Tree.Nodes.Add(TP);
            else Parent.Nodes.Add(TP);
            return TP;
        }

        void Set_Title(string Value)
        {
            if (L_Title != null) L_Title.Text = Value;
        }

        public bool Display_Selected_Page()
        {
            I_Page IP = Page as I_Page;
            if (IP != null)
            {
                if (!IP.Helper.Can_Quit) return false;
                IP.Cleanup();
                //// Suppression des status et des outils sécifiques
                //foreach (var SB in D.Chapter.Status_Tools)
                //{
                //    D.Status_Bar.Items.Remove(SB);
                //}
                //foreach (var TB in D.Chapter.Bar_Tools)
                //{
                //    D.Tool_Strip.Items.Remove(TB);
                //}
            }
            Set_Title ("");
            foreach (Control C in P_Page.Controls)
            {
                C.Dispose();
            }
            P_Page.Controls.Clear();
            Page = null;


            Page_Ref PR = Nav_Tree.SelectedNode == null ? null : Nav_Tree.SelectedNode.Tag as Page_Ref;
            if (PR == null) return false;
            Page = PR.New_Page;
            if (Page == null) return false;
            Page.Dock = DockStyle.Fill;
            P_Page.Controls.Add (Page);

            IP = Page as I_Page;

            //// Ajout des status et outils spécifiques
            //foreach (var SB in D.Chapter.Status_Tools)
            //{
            //    D.Status_Bar.Items.Add(SB);
            //}
            //foreach (var TB in D.Chapter.Bar_Tools)
            //{
            //    D.Tool_Strip.Items.Add(TB);
            //}
            //D.Status_Bar.Update();
            //D.Tool_Strip.Update();
            if (IP != null)
            {
                IP.First_Display();
            }
            Set_Title (PR.Title);
            return true;
        }


        public List<ToolBarButton> Tools = new List<ToolBarButton>();
        public List<StatusBarPanel> Status = new List<StatusBarPanel>();

        #region Gestion du téléchargement
        byte[] _D_Content = null;
        string _D_Content_Type = "";
        string _D_Dest_Name = "";

        public byte[] Download_Content
        {
            get { return _D_Content; }
        }

        public string Download_Content_Type
        {
            get { return _D_Content_Type; }
        }

        public string Download_Dest_Name
        {
            get { return _D_Dest_Name; }
        }

        // Téléchargement d'un fichier / d'un contenu
        public bool Download_Bytes(byte[] Content, string Content_Type, string Dest_Name)
        {
            if (Content.Length == 0) return false;
            _D_Content = Content;
            _D_Content_Type = Content_Type;
            _D_Dest_Name = Dest_Name;

            // Le principe est de se rappeler soi même sous forme de gateway !!!
            LinkParameters Lp = new LinkParameters();
            Lp.Target = "_self";
            Link.Open(new GatewayReference(D, "DOWNLOAD"), Lp);
            return true;
        }


        // Téléchargement d'un fichier / d'un contenu
        public bool Download_File(string Source_Name, string Content_Type, string Dest_Name)
        {
            if (!File.Exists(Source_Name)) return false;
            return Download_Bytes(File.ReadAllBytes(Source_Name), Content_Type, Dest_Name);
        }

        #endregion


        public void ProcessGatewayRequest(IContext objContext, string strAction)
        {
            switch (strAction)
            {
                case "DOWNLOAD":
                    {
                        HttpResponse Response = objContext.HttpContext.Response;
                        Response.ClearHeaders();
                        Response.ContentType = Download_Content_Type;
                        Response.AppendHeader("Content-disposition", "attachment;filename=\"" + Download_Dest_Name + "\"");
                        Response.OutputStream.Write(Download_Content, 0, Download_Content.Length);
                        Response.End();
                        break;
                    }
            }
        }

        public Form Desktop
        {
            get
            {
                return D;
            }
        }

        public class Page_Ref
        {
            public string Title { get; set; }
            public Type Page_Type { get; set; }
            public object Context { get; set; }

            public Page_Ref()
            {
            }

            public Control New_Page
            {
                get
                {
                    if (Context == null)
                    {
                        return Activator.CreateInstance(Page_Type) as Control;
                    }
                    else
                    {
                        return Activator.CreateInstance(Page_Type, Context) as Control;
                    }
                }
            }
        }

    }
}


