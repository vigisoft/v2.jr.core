using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Interfaces;
using Gizmox.WebGUI.Forms.Hosts;
using System.Web;
using System.Collections;
using Gizmox.WebGUI.Common.Gateways;
using System.IO;

namespace JR.WUI
{

    public class Desktop_Helper
    {
        I_Desktop D;
        public Desktop_Helper(I_Desktop Desktop)
        {
            D = Desktop;
            Session_State.Desktop = D;
        }

        // Préparation finale du desktop
        public void Finalize_Desktop()
        {
            // Ajout des status et outils prédéfinis
            D.Status_Bar.Panels.Clear();
            foreach (StatusBarPanel SB in D.Status)
            {
                D.Status_Bar.Panels.Add(SB);
            }
            D.Tool_Bar.Buttons.Clear();
            foreach (ToolBarButton TB in D.Tools)
            {
                D.Tool_Bar.Buttons.Add(TB);
            }

            Display_Selected_Tab();
            D.Nav_Bar.SelectedIndexChanged += new EventHandler(Nav_Bar_SelectedIndexChanged);
        }

        void Nav_Bar_SelectedIndexChanged(object sender, EventArgs e)
        {
            Display_Selected_Tab();
        }

        public void Add_Nav_Tab (string Title, Type Chapter_Type)
        {
            TabPage TP = new TabPage(Title);
            TP.Tag = Chapter_Type;
            D.Nav_Bar.TabPages.Add(TP);
        }

        public void Display_Selected_Tab()
        {
            if (D.Chapter != null)
            {
                // Suppression des status et des outils sécifiques
                foreach (StatusBarPanel SB in D.Chapter.Status)
                {
                    D.Status_Bar.Panels.Remove(SB);
                }
                foreach (ToolBarButton TB in D.Chapter.Tools)
                {
                    D.Tool_Bar.Buttons.Remove(TB);
                }
                D.Chapter.Cleanup();
            }
            D.Panel.Controls.Clear();


            Type Chapter_Type = D.Nav_Bar.SelectedItem.Tag as Type;
            if (Chapter_Type == null)
            {
                D.Status_Bar.Update();
                D.Tool_Bar.Update();
                D.Chapter = null;
                return;
            }
            I_Chapter Chap = Activator.CreateInstance (Chapter_Type) as I_Chapter;
            D.Chapter = Chap;
            Chap.Desktop = D;
            Chap.View.Dock = DockStyle.Fill;
            D.Panel.Controls.Add(Chap.View);
            // Ajout des status et outils spécifiques
            foreach (StatusBarPanel SB in D.Chapter.Status)
            {
                D.Status_Bar.Panels.Add(SB);
            }
            foreach (ToolBarButton TB in D.Chapter.Tools)
            {
                D.Tool_Bar.Buttons.Add(TB);
            }
            D.Status_Bar.Update();
            D.Tool_Bar.Update();
            Chap.First_Display();
        }

        public List<ToolBarButton> Tools = new List<ToolBarButton>();
        public List<StatusBarPanel> Status = new List<StatusBarPanel>();

        #region Gestion du téléchargement
        byte[] _D_Content = null;
        string _D_Content_Type = "";
        string _D_Dest_Name = "";

        public byte[] Download_Content
        {
            get { return _D_Content; }
        }

        public string Download_Content_Type
        {
            get { return _D_Content_Type; }
        }

        public string Download_Dest_Name
        {
            get { return _D_Dest_Name; }
        }

        // Téléchargement d'un fichier / d'un contenu
        public bool Download_Bytes(byte[] Content, string Content_Type, string Dest_Name)
        {
            if (Content.Length == 0) return false;
            _D_Content = Content;
            _D_Content_Type = Content_Type;
            _D_Dest_Name = Dest_Name;

            // Le principe est de se rappeler soi même sous forme de gateway !!!
            LinkParameters Lp = new LinkParameters();
            Lp.Target = "_self";
            Link.Open(new GatewayReference(D, "DOWNLOAD"), Lp);
            return true;
        }


        // Téléchargement d'un fichier / d'un contenu
        public bool Download_File(string Source_Name, string Content_Type, string Dest_Name)
        {
            if (!File.Exists(Source_Name)) return false;
            return Download_Bytes(File.ReadAllBytes(Source_Name), Content_Type, Dest_Name);
        }

        #endregion

        public I_Desktop Desktop
        {
            get
            {
                return D;
            }
        }

        public static Desktop_Helper Current
        {
            get
            {
                return Session_State.Desktop.Helper;
            }
        }

    }
}


