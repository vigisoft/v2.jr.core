#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace Admin
{
    public partial class ConfirmBox : Form
    {
        public ConfirmBox()
        {
            InitializeComponent();
        }

        public ConfirmBox (string Title, string Message, object Context, EventHandler On_Yes)
        {
            InitializeComponent();
            if (Session_State.Current.Language != "FR")
            {
                B_Ok.Text = "Yes";
                B_Cancel.Text = "No";
            }
            this.AcceptButton = B_Ok;
            this.Text = Title;
            C_Message.Text = Message;
            _Context = Context;
            this.On_Yes += On_Yes;
        }

        public ConfirmBox(string Message, object Context, EventHandler On_Yes)
        {
            InitializeComponent();
            this.FormBorderStyle = Gizmox.WebGUI.Forms.FormBorderStyle.FixedSingle;
            if (Session_State.Current.Language != "FR")
            {
                B_Ok.Text = "Yes";
                B_Cancel.Text = "No";
            }
            this.AcceptButton = B_Ok;
            C_Message.Text = Message;
            _Context = Context;
            this.On_Yes += On_Yes;
        }

        public string Message
        {
            get { return C_Message.Text; }
            set { C_Message.Text = value; }
        }

        public string Title
        {
            get { return Text; }
            set { Text = value; }
        }

        object _Context = null;
        public object Context
        {
            get { return _Context; }
            set { _Context = value; }
        }

        public event EventHandler On_Yes;

        private void B_Ok_Click(object sender, EventArgs e)
        {
            Close();
            if (On_Yes != null) On_Yes(_Context, EventArgs.Empty);
        }

        private void B_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ConfirmBox_Enter(object sender, EventArgs e)
        {
            B_Ok_Click(sender, e);
        }

        public static void Ask(string Message, object Context, EventHandler On_Yes)
        {
            new ConfirmBox(Message, Context, On_Yes).Show();
        }

    }
}