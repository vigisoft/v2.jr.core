namespace Admin
{
    partial class ConfirmBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.B_Cancel = new Gizmox.WebGUI.Forms.Button();
            this.B_Ok = new Gizmox.WebGUI.Forms.Button();
            this.C_Message = new Gizmox.WebGUI.Forms.Label();
            this.SuspendLayout();
            // 
            // B_Cancel
            // 
            this.B_Cancel.DialogResult = Gizmox.WebGUI.Forms.DialogResult.Cancel;
            this.B_Cancel.Location = new System.Drawing.Point(332, 80);
            this.B_Cancel.Name = "B_Cancel";
            this.B_Cancel.Size = new System.Drawing.Size(75, 23);
            this.B_Cancel.TabIndex = 0;
            this.B_Cancel.Text = "Non";
            this.B_Cancel.Click += new System.EventHandler(this.B_Cancel_Click);
            // 
            // B_Ok
            // 
            this.B_Ok.Location = new System.Drawing.Point(251, 80);
            this.B_Ok.Name = "B_Ok";
            this.B_Ok.Size = new System.Drawing.Size(75, 23);
            this.B_Ok.TabIndex = 1;
            this.B_Ok.Text = "Oui";
            this.B_Ok.Click += new System.EventHandler(this.B_Ok_Click);
            // 
            // C_Message
            // 
            this.C_Message.Location = new System.Drawing.Point(13, 13);
            this.C_Message.Name = "C_Message";
            this.C_Message.Size = new System.Drawing.Size(394, 51);
            this.C_Message.TabIndex = 2;
            this.C_Message.Text = "label1";
            this.C_Message.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ConfirmBox
            // 
            this.CancelButton = this.B_Cancel;
            this.Controls.Add(this.C_Message);
            this.Controls.Add(this.B_Ok);
            this.Controls.Add(this.B_Cancel);
            this.FormBorderStyle = Gizmox.WebGUI.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Size = new System.Drawing.Size(419, 115);
            this.Text = "Confirmation";
            this.Enter += new System.EventHandler(this.ConfirmBox_Enter);
            this.ResumeLayout(false);

        }

        #endregion

        private Gizmox.WebGUI.Forms.Button B_Cancel;
        private Gizmox.WebGUI.Forms.Button B_Ok;
        private Gizmox.WebGUI.Forms.Label C_Message;


    }
}