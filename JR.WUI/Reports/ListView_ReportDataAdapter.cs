﻿using System;
using System.Collections.Generic;
using ReportExporters.Common.Adapters;
using Gizmox.WebGUI.Forms;
using ReportExporters.Common.Model.Style;
using ReportExporters.Common.Model.DataColumns;
using ReportExporters.Common.Model;
using ReportExporters.Common.Model.Images;
using ReportExporters.Common.Model.TableGroups;

namespace JR.WUI
{
    using W_Unit = System.Web.UI.WebControls.Unit;
    using W_BorderStyle = System.Web.UI.WebControls.BorderStyle;
    using W_UnitType = System.Web.UI.WebControls.UnitType;
    public class ListView_ReportDataAdapter : IReportDataAdapter
    {
        public ListView Grid;
		private W_Unit DefaultColumnWidth = new W_Unit(1.25, W_UnitType.Inch);
		private W_Unit DefaultRowHeight = new W_Unit(0.25, W_UnitType.Inch);
		private Border DefaultBorder = new Border(System.Drawing.Color.LightGray, W_BorderStyle.Solid, new W_Unit(1, W_UnitType.Point));


        //List<ListView> Rows = new List<DataRow>();
        List<ColumnHeader> Cols = new List<ColumnHeader>();

        public bool Append_Id = false;
        public bool All_Strings = false;
        public ListView_ReportDataAdapter(ListView G, string Tag_Title = "", bool String_Types = true)
        {
            All_Strings = String_Types;
            Grid = G;
            Append_Id = Tag_Title != "";

            // Construire la liste des colonnes telles qu'elles sont affichées
            ColumnHeader[] Columns = new ColumnHeader[G.Columns.Count];
            foreach (ColumnHeader C in Grid.Columns)
            {
                if (!C.Visible) continue;
                Columns[C.DisplayIndex] = C;
            }
            if (Append_Id)
            {
                Cols.Add(new ColumnHeader("Tag_Column", Tag_Title, 50, ListViewColumnType.Text));
            }
            foreach (var C in Columns)
            {
                if (C != null) Cols.Add(C);
            }
    
        }

        Type Sense_Type(ColumnHeader CH)
        {
            var Detected_Type = typeof(string);
            if (All_Strings) return Detected_Type;
            foreach (ListViewItem LVI in Grid.Items)
            {
                var LSI = LVI.SubItems[CH.Index];
                if (LSI.Text == "") continue;
                var LSV = LSI as ListViewSub_Value;
                if (LSV != null) return LSV.Value.GetType();
                return typeof(string);
            }
            return Detected_Type;
        }

        #region IReportDataAdapter Members

		public virtual ReportColumnCollection GetColumns()
		{
			ReportColumnCollection toRet = new ReportColumnCollection();

            int IC = 0;
            foreach (ColumnHeader CH in Cols)
			{
				ReportDataColumn rdColumn = toRet.AddNewReportDataColumn("C" +IC);

                rdColumn.DefaultCellStyle.Width = new Size(new W_Unit(CH.Width, W_UnitType.Pixel));
                rdColumn.HeaderStyle.BackgroundColor = System.Drawing.Color.Orange;
                rdColumn.HeaderStyle.Font.FontWeight = FontWeightType.Semi_Bold;
                string Col_Title = CH.Text;
                if (Col_Title == "")
                {
                    if (CH.Tag is string)
                    {
                        Col_Title = CH.Tag.ToString();
                    }
                    else if (CH.Tag is JR.I_Public_Ident)
                    {
                        Col_Title = ((JR.I_Public_Ident)CH.Tag).Public_Name;
                    }
                }
                rdColumn.HeaderText = Col_Title;
                if (Append_Id && IC == 0)
                {
                    rdColumn.ValueType = typeof(string);
                }
                else
                {
                    rdColumn.ValueType = Sense_Type(CH);
                }
                //if (dataColumn.Format != "") rdColumn.TemplateFormat = dataColumn.Format;
                //if (dataColumn.Format != "") rdColumn.DefaultCellStyle.Format = dataColumn.Format;

                if (rdColumn.ValueType == typeof(System.Drawing.Image))
                {
                    DefaultRowHeight = new W_Unit(75, W_UnitType.Pixel);
                    rdColumn.DataCellViewType = CellViewImage.CreateDatabaseImage(ImageMIMEType.Jpeg);
                    rdColumn.DefaultCellStyle.Width = new Size(new W_Unit(85, W_UnitType.Pixel));
                }
                else if (rdColumn.ValueType == typeof(System.Drawing.Bitmap))
                {
                    DefaultRowHeight = new W_Unit(75, W_UnitType.Pixel);
                    rdColumn.DataCellViewType = CellViewImage.CreateDatabaseImage(ImageMIMEType.Bmp);
                    rdColumn.DefaultCellStyle.Width = new Size(new W_Unit(85, W_UnitType.Pixel));
                }

				rdColumn.HeaderStyle.Border = 
					rdColumn.DefaultCellStyle.Border = DefaultBorder;
					
				rdColumn.DefaultCellStyle.Wrap = true;
                IC++;
			}

			return toRet;
		}

		public virtual object GetCellItemValue(ReportColumn rdColumn, int rowIndex)
		{
            if (rowIndex >= Grid.Items.Count) return null;
            var LVI = Grid.Items[rowIndex];
            if (Append_Id && rdColumn.Index == 0)
            {
                if (LVI.Tag != null)
                {
                    if (LVI.Tag is JR.I_Public_Ident)
                    {
                        return ((JR.I_Public_Ident)LVI.Tag).Public_Id;
                    }
                    else
                    {
                        return LVI.Tag.ToString();
                    }
                }
            }
            var LSI = LVI.SubItems[Cols[rdColumn.Index].Index];
            if (All_Strings) return LSI.ToString();
            var LSV = LSI as ListViewSub_Value;
            return JR.Convert.To_Type(rdColumn.ValueType, LSV != null ? LSV.Value : LSI.Text);
        }

		public virtual int GetRowCount()
		{
            return Grid.Items.Count;
		}

		public virtual ReportTableGroupList GetTableGroups(ReportColumnCollection columns)
		{
			// No grouping by default
			return null;
		}

		public virtual Size GetRowHeight()
		{
			return DefaultRowHeight;
		}

		public virtual IEnumerable<object> GetReportParameters()
		{
			// No report parameters by default
			return null;
		}

		#endregion

        #region IReportDataAdapter Membres


        ReportExporters.Common.Model.ControlItems.CITextBox ReportExporters.Common.Adapters.IReportDataAdapter.GetTitle()
        {
            return null;
        }

        ReportExporters.Common.Model.ControlItems.CITextBox ReportExporters.Common.Adapters.IReportDataAdapter.GetHeader()
        {
            return null;
        }

        ReportExporters.Common.Model.ControlItems.CITextBox ReportExporters.Common.Adapters.IReportDataAdapter.GetFooter()
        {
            return null;
        }

        #endregion
    }
}

