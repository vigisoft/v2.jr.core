﻿function Notify(Client_Id, Action, Data, JS) {
    var objForm = vwgContext.provider.getComponentByClientId(Client_Id);
    var objAtt = {};
    objAtt.Action = Action;
    objAtt.Data = Data;
    vwgContext.events.performClientEvent(objForm.id(), 'Notify', objAtt, true);
    vwgContext.events.raiseEvents();
}

function JNotify(Client_Id, Action, Data) {
    var objForm = vwgContext.provider.getComponentByClientId(Client_Id);
    var objAtt = {};
    objAtt.Action = Action;
    objAtt.Data = JSON.stringify(Data);
    vwgContext.events.performClientEvent(objForm.id(), 'JNotify', objAtt, true);
    vwgContext.events.raiseEvents();
}

var CN_Loop = 0;
var CN_Status = 0;
var CN_Data = '';
var CN_Id = '';
var CN_Period = 10;
var CN_Timer = null;
var CN_URL = '/VWG/CN_async.ashx';
var CN_JQXHR = null;

function CN_Init(Id, Period, URL) {
    CN_Id = Id;
    if (typeof Period !== "undefined") CN_Period = Period;
    if (typeof URL !== "undefined") CN_URL = URL;
}

function CN_Start() {
    if (CN_Timer == null)
    {
        CN_Timer = setInterval(CN_Check, CN_Period);
    }
    CN_Status = 1;
    CN_Loop++;
    CN_JQXHR = $.get(CN_URL + '?l=' + CN_Loop, { id: CN_Id })
    .done(function (data) {
        if (CN_Timer != null)
        {
            CN_Data = data;
            CN_Status = 2;
            CN_JQXHR = null;
        }
    });
}
function CN_Stop() {
    CN_Status = -1;
    if (CN_Timer != null) clearInterval(CN_Timer);
    if (CN_JQXHR != null) CN_JQXHR.abort();
    CN_Timer = null;
    CN_JQXHR = null;
}

function CN_Check()
{
    if (CN_Status == 0)
    {
        CN_Start();
    } else if (CN_Status == 2)
    {
        if (CN_Data == '!Not registered' || CN_Data == '!Disposed' || CN_Data == '!Inactive')
        {
            CN_Stop();
            return;
        }
        var objForm = vwgContext.provider.getComponentByClientId(CN_Id);
        if (CN_Data.indexOf('!') == -1)
        {
            var objAtt = {};
            objAtt.Data=CN_Data;
            vwgContext.events.performClientEvent(objForm.id(), 'Notify', objAtt, true);
            vwgContext.events.raiseEvents();
            CN_Status = 3;
        } else
        {
            CN_Status = 0;
        }
    }
}

