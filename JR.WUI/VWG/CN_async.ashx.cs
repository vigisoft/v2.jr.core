﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JR.WUI
{
    /// <summary>
    /// Description résumée de async
    /// </summary>
    public class CN_async : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string Status = Form_Notifier.Wait_Messages(context.Request["id"]);
            context.Response.Write(Status);
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}