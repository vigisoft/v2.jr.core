﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JR.WUI
{
    /// <summary>
    /// Description résumée de APISearch
    /// </summary>
    public class Temp_Page : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            string Temp_Key = context.Request.QueryString["K"]??"";
            JR.WUI.Temporary_Page.Treat_Temp_Page(context.Response, Temp_Key);
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}