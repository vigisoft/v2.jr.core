﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace JR.WUI
{
    public class Global : System.Web.HttpApplication
    {

        public static JR.WUI.Application_State A;

        protected void On_Application_Start()
        {
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Application.Lock();
            A.Create_Session(Session);
            Application.UnLock();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Application.Lock();
            A.Error(Context);
            Application.UnLock();
        }

        protected void Session_End(object sender, EventArgs e)
        {
            Application.Lock();
            A.Release_Session(Session);
            Application.UnLock();
        }

        protected void Application_End(object sender, EventArgs e)
        {
            A = A.End();
        }

    }
}