﻿using Gizmox.WebGUI.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JR.WUI
{
    public static class Extension
    {

        public static void Resize_Max(this Form F, bool Show_Title = false)
        {
            F.WindowState = FormWindowState.Maximized;
            F.TopLevel = true;
            F.FormBorderStyle = Show_Title ? FormBorderStyle.FixedToolWindow : FormBorderStyle.None;
            if (F.CancelButton == null)
            {
                var B = new Button { Width = 0, Height = 0, Top = -10 };
                B.Click += (sender, e) => { F.Close(); };
                F.CancelButton = B;
            }
        }

        public static DialogResult Show_Max(this Form F, bool Show_Title = false)
        {
            F.WindowState = FormWindowState.Maximized;
            F.TopLevel = true;
            F.FormBorderStyle = Show_Title ? FormBorderStyle.FixedToolWindow : FormBorderStyle.None;
            if (F.CancelButton == null)
            {
                var B = new Button { Width = 0, Height = 0, Top = -10 };
                B.Click += (sender, e) => { F.Close(); };
                F.CancelButton = B;
            }
            return F.ShowDialog();
        }

        private static void F_FormClosed(object sender, FormClosedEventArgs e)
        {
            throw new NotImplementedException();
        }

        public static DialogResult Show_Max(this Control C)
        {
            Form F = new Form();
            F.Controls.Add(C);
            C.Dock = DockStyle.Fill;
            if (C is I_Closable)
            {
                F.FormClosed += (s, e) => { ((I_Closable)C).Before_Close(); };
            }
            return F.Show_Max();
        }

    }
    public interface I_Closable
    {
        void Before_Close();
    }
}
