using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Forms;

namespace JR.WUI.WAF
{
	public partial class Icon_Button : Label
	{
        public Icon_Button()
        {
            InitializeComponent();
            AutoSize = false;
            Width = 25;
            Height = 25;
        }
        bool _Enabled = true;
        public override bool Enabled
        {
            get
            {
                return base.Enabled;
            }
            set
            {
                base.Enabled = value;
                _Enabled = value;
                Draw_Button();
            }
        }

        public string Frame_Format = "{0}";

        void Draw_Button ()
        {
            this.Tag ("��" + Frame_Format, string.Format("<span class='{0} {2}' style='padding:0px;'><i class='fa {1}'></i></span>", Global_Class, _Icon, _Enabled ? "" : "disabled"));
        }

        public string Global_Class = "btn btn-link";
        string _Icon = "";
        public string Icon
        {
            get
            {
                return _Icon;
            }
            set
            {
                _Icon = value;
                Draw_Button();
            }
        }

        public string Role
        {
            set 
            { 
                switch (value)
                {
                    case "ADD": Name = "B_Add"; Icon = "fa-plus"; break;
                    case "REMOVE": Name = "B_Remove"; Icon = "fa-minus"; break;
                    case "EDIT": Name = "B_Edit"; Icon = "fa-pencil"; break;
                    case "COLS": Name = "B_Cols"; Icon = "fa-cog"; break;
                    case "REFRESH": Name = "B_Refresh"; Icon = "fa-refresh"; break;
                    case "EXTRACT": Name = "B_Extract"; Icon = "fa-upload"; break;
                }
            }
        }

        public Action On_Click
        {
            set
            {
                Click += (o,e) => value();
            }
        }
    }
}
