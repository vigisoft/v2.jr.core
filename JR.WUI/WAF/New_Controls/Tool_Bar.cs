#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace JR.WUI.WAF
{
    public partial class Tool_Bar : UserControl
    {
        public Tool_Bar()
        {
            InitializeComponent();
            Padding = new Padding (5,2,5,0);
        }
        public static Tool_Bar Add_Tool(Control C)
        {
            var TB = new Tool_Bar { };
            C.Controls.Add(TB);
            TB.Dock = DockStyle.Top;
            return TB;
        }
        public void Append (Control C, bool Left = true)
        {
            if (Left)
            {
                Controls.Insert(0, C);
                C.Dock = DockStyle.Left;
            } else
            {
                Controls.Add(C);
                C.Dock = DockStyle.Right;
            }
        }
        public void Enable (string Name, bool State)
        {
            foreach (Control C in Controls)
            {
                if (C.Name != Name) continue;
                var I = C as Icon_Button;
                if (I != null)
                {
                    I.Enabled = State;
                    break;
                }
                C.Enabled = State;
                break;
            }
        }
    }

}