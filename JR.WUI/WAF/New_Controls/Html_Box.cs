using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Forms;

namespace JR.WUI.WAF
{
	public partial class Html_Box : HtmlBox
	{
		public Html_Box()
		{
			InitializeComponent();
            IsWindowless = true;
		}		
	}
}
