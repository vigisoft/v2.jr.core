using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Interfaces;
using Gizmox.WebGUI.Forms.Hosts;
using System.Collections;
using System.Drawing;

namespace JR.WUI.WAF
{

    public class Error_Helper
    {
        Color Original_Color;
        Control Checked = null;
        Control Message = null;
        public Error_Helper (Control Checked_Control, Control Message_Control = null)
        {
            Message = Message_Control;
            Checked = Checked_Control;
            Original_Color = Checked.BorderColor;
            if (Message != null)
            {
                Message.Text = "";
            }
        }

        public void Reset ()
        {
            Checked.BorderColor = Original_Color;
            if (Message != null) Message.Text = "";
        }

        public bool Err (string Error_Message)
        {
            Checked.BorderColor = Color.Red;
            if (Message != null) Message.Text = Error_Message;
            return false;
        }
    }

}
