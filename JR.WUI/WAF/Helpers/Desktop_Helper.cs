using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using Gizmox.WebGUI.Forms;
using System.Collections.Generic;
using Gizmox.WebGUI.Common.Gateways;
using System.IO;

namespace JR.WUI.WAF
{
    public class Desktop_Helper
    {
        I_Desktop D;
        public Desktop_Helper(I_Desktop Desktop)
        {
            D = Desktop;
        }

        public bool Auto_Activate = true;

        // Préparation finale du desktop
        public void Finalize_Desktop()
        {
            // Ajout des status et outils prédéfinis
            //D.Status_Bar.Items.Clear();
            foreach (var SB in D.Status_Items)
            {
                D.Status_Bar.Items.Add(SB);
            }

            foreach (var TB in D.Tool_Items)
            {
                D.Tool_Strip.Items.Add(TB);
            }

            Display_Selected_Tab();
            D.Nav_Tree.AfterSelect += new TreeViewEventHandler(Nav_Tree_AfterSelect);
            D.Nav_Tree.ExpandAll();
        }

        void Nav_Tree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode N = e.Node;

            // Autoactivation
            // Si c'est un node sans action (.Tag = null)
            // Un seul niveau supporté
            while (N.Nodes.Count > 0 && N.Tag == null)
            {
                // Préselectionner la première vue (chapter) du sous menu
                foreach (TreeNode NC in N.Nodes)
                {
                    if (NC.Tag as Chapter_Helper != null)
                    {
                        if (Auto_Activate || NC.Text == "")
                        {
                            N = NC;
                            break;
                        }
                    }
                }
                break;
            }

            if (N.Tag == null) return;

            // Si changement de node le resélectionner
            if (N != e.Node)
            {
                D.Nav_Tree.SelectedNode = N;
                return;
            }

            // Effectuer les actions selon le type de node
            Form_Helper F = N.Tag as Form_Helper;
            if (F != null)
            {
                F.Display();
                D.Nav_Tree.SelectedNode = null;
                return;
            }
            Chapter_Helper Ch = N.Tag as Chapter_Helper;
            if (Ch != null)
            {
                Display_Selected_Tab();
                return;
            }
            Action Ac = N.Tag as Action;
            if (Ac != null)
            {
                Ac();
                D.Nav_Tree.SelectedNode = null;
                return;
            }
        }

        public TreeNode Add_Form_Node(TreeNode Parent, string Title, Func<I_Form> Create)
        {
            TreeNode TP = new TreeNode(Title);
            Form_Helper CH = new Form_Helper { Title = Title, Create = Create, Desktop = D };
            TP.Tag = CH;
            if (Parent == null) D.Nav_Tree.Nodes.Add(TP);
            else Parent.Nodes.Add(TP);
            return TP;
        }

        public TreeNode Add_Command_Node(TreeNode Parent, string Title, Action Command)
        {
            TreeNode TP = new TreeNode(Title);
            TP.Tag = Command;
            if (Parent == null) D.Nav_Tree.Nodes.Add(TP);
            else Parent.Nodes.Add(TP);
            return TP;
        }

        public TreeNode Add_View_Node(TreeNode Parent, string Title, Type Chapter_Type, object Context)
        {
            return Add_Nav_Node(Parent, Title, Chapter_Type, Context);
        }

        public TreeNode Add_Nav_Node(TreeNode Parent, string Title, Type Chapter_Type, object Context)
        {
            TreeNode TP = new TreeNode(Title);
            if (Chapter_Type != null)
            {
                Chapter_Helper CH = new Chapter_Helper(Title, Chapter_Type, Context);
                TP.Tag = CH;
            }
            if (Parent == null) D.Nav_Tree.Nodes.Add(TP);
            else Parent.Nodes.Add(TP);
            return TP;
        }

        public void Display_Selected_Tab()
        {
            Chapter_Helper Ch = D.Nav_Tree.SelectedNode == null ? null : D.Nav_Tree.SelectedNode.Tag as Chapter_Helper;
            if (Ch == D.Chapter) return;
            if (D.Chapter != null)
            {
                // Suppression des status et des outils sécifiques
                foreach (var SB in D.Chapter.Status_Tools)
                {
                    D.Status_Bar.Items.Remove(SB);
                }
                foreach (var TB in D.Chapter.Bar_Tools)
                {
                    D.Tool_Strip.Items.Remove(TB);
                }
                D.Chapter.Chapter.Cleanup();
            }


            string Title = Ch == null ? "" : Ch.Title;

            if (Ch == null)
            {
                if (D.Status_Bar != null) D.Status_Bar.Update();
                if (D.Tool_Strip != null) D.Tool_Strip.Update();
                D.Chapter = null;
                return;
            }
            D.Chapter = Ch;
            Ch.Desktop = D;
            I_Chapter Chap = Ch.Create_Page() as I_Chapter;
            Ch.View.Dock = DockStyle.Fill;
            D.Set_Current_Panel (Ch.View);
            // Ajout des status et outils spécifiques
            foreach (var SB in D.Chapter.Status_Tools)
            {
                D.Status_Bar.Items.Add(SB);
            }
            foreach (var TB in D.Chapter.Bar_Tools)
            {
                D.Tool_Strip.Items.Add(TB);
            }
            if (D.Status_Bar != null) D.Status_Bar.Update();
            if (D.Tool_Strip != null) D.Tool_Strip.Update();
            Chap.First_Display();
            D.Title = Title;
        }

        public List<ToolStripItem> Tool_Items = new List<ToolStripItem>();
        public List<ToolStripItem> Status_Items = new List<ToolStripItem>();

        #region Gestion du téléchargement
        byte[] _D_Content = null;
        string _D_Content_Type = "";
        string _D_Dest_Name = "";

        public byte[] Download_Content
        {
            get { return _D_Content; }
        }

        public string Download_Content_Type
        {
            get { return _D_Content_Type; }
        }

        public string Download_Dest_Name
        {
            get { return _D_Dest_Name; }
        }

        // Téléchargement d'un fichier / d'un contenu
        public bool Download_Bytes(byte[] Content, string Content_Type, string Dest_Name)
        {
            if (Content.Length == 0) return false;
            _D_Content = Content;
            _D_Content_Type = Content_Type == "" ? "application/octet-stream" : Content_Type;
            _D_Dest_Name = Dest_Name;

            // Le principe est de se rappeler soi même sous forme de gateway !!!
            LinkParameters Lp = new LinkParameters();
            Lp.Target = "_self";
            Link.Open(new GatewayReference(D, "DOWNLOAD"), Lp);
            return true;
        }


        // Téléchargement d'un fichier / d'un contenu
        public bool Download_File(string Source_Name, string Content_Type, string Dest_Name)
        {
            if (!File.Exists(Source_Name)) return false;
            return Download_Bytes(File.ReadAllBytes(Source_Name), Content_Type, Dest_Name);
        }

        #endregion


    }
}
