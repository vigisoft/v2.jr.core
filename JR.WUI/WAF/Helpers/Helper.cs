using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using Gizmox.WebGUI.Forms;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Common.Resources;

namespace WAF.UI
{
    public static class Helper
    {
        public static int Full_Width_Column(this ListView LV, int Min_Width)
        {
            var W = LV.Width;
            if (LV.CheckBoxes) W -= 24;
            foreach (ColumnHeader LC in LV.Columns)
            {
                W -= LC.Width + 2;
            }
            return Math.Max(Min_Width, W);
        }

        //public static void Set_Text(Control Control, string Text)
        //{
        //    if (Control is Label || Control is Button)
        //    {
        //        Control.Text = Text.Replace("&", "&&");
        //    }
        //    else Control.Text = Text;
        //}
    }
}
