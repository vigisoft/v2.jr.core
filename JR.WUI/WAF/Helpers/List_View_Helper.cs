using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using Gizmox.WebGUI.Forms;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Drawing;

namespace JR.WUI.WAF
{


    public class List_View_Helper<T>:I_List_Selection<T>
    {
        ListView _LV;
        public List_View_Helper(ListView List)
        {
            _LV = List;
            if (_LV == null) return;
            _LV.GridLines = true;
            _LV.UseInternalPaging = true;
        }

        public void Insert_Total (int Count = -1)
        {
            ListViewItem Tit = new ListViewItem();
            Tit.BackColor = Color.Lavender;
            Tit.Tag = "";
            Tit.SubItems.Add("TOTAL").BackColor = Color.Lavender;
            Tit.SubItems.Add(new ListViewSub_Value(Count == -1?_LV.Items.Count:Count)).BackColor = Color.Lavender;
            _LV.Items.Insert(0, Tit);
        }

        public void Prepare(bool Autosize, bool Sort)
        {
            Prepare(Autosize, Sort, 0);
        }
        public void Prepare (bool Autosize, bool Sort, int Page_Size)
        {
            if (_LV == null) return;
            if (Sort)
            {
                _LV.ListViewItemSorter = new ListView_Sorter(_LV);
            }
            if (Autosize && (_LV.Items.Count > 0))
            {
                for (int I = 1; I < _LV.Columns.Count; I++)
                {
                    if (_LV.Columns[I].Type == ListViewColumnType.Text)
                    {
                        int Init_Size = _LV.Columns[I].Width;
                        _LV.AutoResizeColumn(I, ColumnHeaderAutoResizeStyle.ColumnContent);
                        int New_Size = _LV.Columns[I].Width;
                        if (New_Size < Init_Size) _LV.Columns[I].Width = Init_Size;
                    }
                }
            }
            if (Page_Size == 0)
            {
                if (_LV.Items.Count > 1000)
                {
                    _LV.ItemsPerPage = 30;      // Pages de petite taille pour �viter d'avoir ascenseur + pagination
                }
                else
                {
                    _LV.ItemsPerPage = 1000;      // Pas de pagination, donc seulement ascenceur
                }
            }
            else
            {
               _LV.ItemsPerPage = Page_Size;      // Pages de petite taille pour bien voir qu'il manque des �l�ments
            }
        }

        public Gizmox.WebGUI.Forms.Control List
        {
            get { return _LV; }
        }

        public bool Select_Next()
        {
            if (_LV == null) return false;
            if (!Has_Next) return false;
            _LV.SelectedIndex++;
            //_LV.EnsureVisible(_LV.SelectedIndex);
            if (After_Selected_Change != null) After_Selected_Change();
            return true;
        }

        public bool Select_Previous()
        {
            if (_LV == null) return false;
            if (!Has_Previous) return false;
            _LV.SelectedIndex--;
            if (After_Selected_Change != null) After_Selected_Change();
            //_LV.EnsureVisible(_LV.SelectedIndex);
            return true;
        }

        public bool Has_Previous
        {
            get { return (_LV != null) && _LV.SelectedIndex > 0; }
        }

        public bool Has_Next
        {
            get { return (_LV != null) && _LV.SelectedIndex < _LV.Items.Count - 1; }
        }


        #region I_List_Selection<T> Membres

        public delegate T Get_Selection();
        public Get_Selection Current_Selection = null;


        public delegate void Selection_Change();
        public Selection_Change After_Selected_Change = null;

        public delegate void Selected_Refresh ();
        public Selected_Refresh On_Selected_Refresh = null;

        public T Selected
        {
            get { return (Current_Selection == null)?default(T):Current_Selection (); }
        }

        public void Refresh_Selected()
        {
            if (On_Selected_Refresh != null) On_Selected_Refresh();
        }

        #endregion


        public static void Export_To_Excel(ListView List, string File_Name)
        {
            Export_To_Excel(List, File_Name, "");
        }

        // Outil d'exportation Excel
        public static void Export_To_Excel(ListView List, string File_Name, string Tag_Title)
        {
            StringBuilder SB = new StringBuilder();
            bool Append_Id = false;
            if (Tag_Title != "")
            {
                Append_Id = true;
                SB.Append(Tag_Title);
                SB.Append('\t');
            }
            foreach (ColumnHeader CH in List.Columns)
            {
                if (!CH.Visible) continue;
                string Title = CH.Text;
                if (Title == "")
                {
                    if (CH.Tag is string)
                    {
                        Title = CH.Tag.ToString();
                    }
                    else if (CH.Tag is JR.I_Public_Ident)
                    {
                        Title = ((JR.I_Public_Ident)CH.Tag).Public_Name;
                    }
                }
                SB.Append(Title);
                SB.Append('\t');
            }
            SB.AppendLine();
            ListViewGroup Group = null;
            foreach (ListViewItem LI in List.Items)
            {
                if (LI.Group != Group)
                {
                    Group = LI.Group;
                    if (Group != null)
                    {
                        SB.AppendLine();
                        SB.Append(Group.Header);
                        SB.Append('\t');
                        SB.AppendLine();
                    }
                }
                if (Append_Id)
                {
                    if (LI.Tag != null)
                    {
                        if (LI.Tag is JR.I_Public_Ident)
                        {
                            SB.Append(((JR.I_Public_Ident)LI.Tag).Public_Id);
                        }
                        else
                        {
                            SB.Append(LI.Tag.ToString());
                        }
                    }
                    SB.Append('\t');
                }
                int C = 0;
                foreach (ListViewItem.ListViewSubItem SI in LI.SubItems)
                {
                    ColumnHeader CH = List.Columns[C];
                    if (CH.Visible)
                    {
                        SB.Append(SI.Text.Replace("\r\n", ".").Replace("\t", " "));
                        SB.Append('\t');
                    }
                    C++;
                }
                SB.AppendLine();
            }
            Session_State.Current_Desktop.Download_Bytes(Encoding.Default.GetBytes(SB.ToString()), "application/vnd.ms-excel", File_Name);

        }

        public static void Export_To_Excel(ListView List, string File_Name, string Tag_Title, int Mode)
        {
            StringBuilder SB = new StringBuilder();
            bool Append_Id = false;
            if (Tag_Title != "")
            {
                Append_Id = true;
                SB.Append(Tag_Title);
                SB.Append('\t');
            }
            foreach (ColumnHeader CH in List.Columns)
            {
                string Title = CH.Text;
                if (Title == "")
                {
                    if (CH.Tag is string)
                    {
                        Title = CH.Tag.ToString();
                    }
                    else if (CH.Tag is JR.I_Public_Ident)
                    {
                        Title = ((JR.I_Public_Ident)CH.Tag).Public_Name;
                    }
                }
                SB.Append(Title);
                SB.Append('\t');
            }
            SB.AppendLine();
            foreach (ListViewItem LI in List.Items)
            {
                if (Append_Id)
                {
                    if (LI.Tag != null)
                    {
                        if (LI.Tag is JR.I_Public_Ident)
                        {
                            SB.Append(((JR.I_Public_Ident)LI.Tag).Public_Id);
                        }
                        else
                        {
                            SB.Append(LI.Tag.ToString());
                        }
                    }
                    SB.Append('\t');
                }
                foreach (ListViewItem.ListViewSubItem SI in LI.SubItems)
                {
                    SB.Append(SI.Text.Replace("\r\n", ".").Replace("\t", " "));
                    SB.Append('\t');
                }
                SB.AppendLine();
            }
            Session_State.Current_Desktop.Download_Bytes(Encoding.Default.GetBytes(SB.ToString()), "application/vnd.ms-excel", File_Name);

        }

    }
}
