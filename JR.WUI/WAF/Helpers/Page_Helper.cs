using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Interfaces;
using Gizmox.WebGUI.Forms.Hosts;
using System.Web;
using System.Collections;

namespace JR.WUI.WAF
{

    public class Page_Helper
    {
        public I_Page Control { get; set; }
        public Page_Helper()
        {
            Can_Quit = true;
        }

        public void Finalize_Page()
        {
        }

        public List<ToolBarButton> Tools = new List<ToolBarButton>();
        public List<StatusBarPanel> Status = new List<StatusBarPanel>();

        public bool Can_Quit { get; set; }

    }

}
