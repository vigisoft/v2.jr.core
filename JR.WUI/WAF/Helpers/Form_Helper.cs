using System;
using System.Data;
using System.Web;
using Gizmox.WebGUI.Forms;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Common.Resources;

namespace JR.WUI.WAF
{
    public class Form_Helper
    {
        public I_Desktop Desktop;
        public I_Form Form;
        public string Title;
        public object Context = null;

        public Func<I_Form> Create;

        public void Display()
        {
            Form = Create();
            Form.Helper = this;
            var F = Form as Form;
            F.ShowDialog();
        }

        public void Refresh_Main ()
        {
            if (Desktop.Chapter.Chapter != null) Desktop.Chapter.Chapter.First_Display();
        }
    }
}
