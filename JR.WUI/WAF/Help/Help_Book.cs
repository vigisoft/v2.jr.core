#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace JR.WUI
{
    public partial class Help_Book : Form
    {
        public Help_Book()
        {
            InitializeComponent();
        }

        public Help_Repository HR;
        public string Initial_Key = "";

        public static void Display_Help(Help_Repository Repository, string Initial_Key = "")
        {
            var D = new Help_Book { HR = Repository, Initial_Key = Initial_Key};
            D.Show_Max();
        }

        Help_Item Initial_Item = null;
        void Add_Entry (TreeNodeCollection Nodes, Help_Item Item)
        {
            foreach (var SI in Item.Sub_Items)
            {
                var TN = new TreeNode(SI.Title){Tag = SI};
                Nodes.Add(TN);
                Add_Entry(TN.Nodes, SI);
                if (SI == Initial_Item) Tree.SelectedNode = TN;
            }
        }

        private void Help_Book_Load(object sender, EventArgs e)
        {
            Tree.Nodes.Clear();
            if (HR.Dic.Count > 0)
            {
                if (!HR.Dic.TryGetValue (Initial_Key, out Initial_Item))
                {
                    Initial_Item = HR.Main_Entry.Sub_Items[0];
                }
            }
            Add_Entry(Tree.Nodes, HR.Main_Entry);
            Tree.AfterSelect += Tree_AfterSelect;
        }

        void Show_Item (Help_Item It)
        {
            Frame.Url = It.URL;
        }

        private void Tree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            var It = e.Node.Tag as Help_Item;
            if (It == null) return;
            Show_Item (It);
        }
    }
}