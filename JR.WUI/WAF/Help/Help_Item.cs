﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JR.WUI
{
    public class Help_Item
    {
        public string Key = "";
        public string Title = "";
        public string URL = "";
        public List<Help_Item> Sub_Items = null;
    }

    public class Help_Repository
    {
        public Dictionary<string, Help_Item> Dic = new Dictionary<string, Help_Item>();
        public Help_Item Main_Entry = null;
        public static Help_Repository Create (string Json)
        {
            Action<Help_Item> Index = null;
            var H = JR.Serial<Help_Item>.From_Json(Json);
            var R = new Help_Repository { Main_Entry = H };
            Index = (I) =>
                {
                    R.Dic[I.Key] = I;
                    foreach (var SI in I.Sub_Items)
                    {
                        Index(SI);
                    }
                };
            return R;
        }
    }
}
