using System;
using System.Collections.Generic;
using System.Text;

namespace JR.WUI.WAF
{
    // Interface d'un bureau
    public interface I_View:I_Notified
    {

        // Appel� sur changement de filtre
        void Apply_Filter();

        // Appel� au chargement
        void First_Display();

        // Appel� au rafraichissement
        void Refresh_Display();

        // Appel� � l'effacement
        void Cleanup();

        // Exporte le contenu
        void Export_Content();

    }
}
