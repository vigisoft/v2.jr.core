using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

namespace JR.WUI.WAF
{
    // Interface d'une liste
    public interface I_List_Selection<T>
    {
        // Vue principale
        Control List{get;}

        T Selected { get;}
        // Selection objet suivant
        bool Select_Next ();

        // Selection objet précédent
        bool Select_Previous();

        // precedent possible
        bool Has_Previous { get;}

        // suivant possible
        bool Has_Next { get;}

        // Rafraichissement de l'item selectionné
        void Refresh_Selected();
    }
}
