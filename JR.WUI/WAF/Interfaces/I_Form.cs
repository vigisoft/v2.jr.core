using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Interfaces;

namespace JR.WUI.WAF
{
    // Interface d'un dialogue principal
    public interface I_Form:IForm
    {
        // Helper
        Form_Helper Helper { set; get; }
    }
}
