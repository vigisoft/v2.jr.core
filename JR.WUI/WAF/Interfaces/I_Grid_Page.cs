using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Interfaces;
using System.Data;

namespace JR.WUI.WAF
{
    // Interface d'un bureau
    public interface I_Grid_Page:I_Page
    {
        DataGridView Grid { get; }
        Grid_Extender Extender { get; set; }
    }
}
