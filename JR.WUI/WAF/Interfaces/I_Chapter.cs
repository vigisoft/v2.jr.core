using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

namespace JR.WUI.WAF
{
    // Interface d'un chapitre
    public interface I_Chapter
    {
        // Appel� � l'effacement
        void Cleanup();

        // PRemier affichage
        void First_Display();

        // Helper
        Chapter_Helper Helper { set; get; }

    }
}
