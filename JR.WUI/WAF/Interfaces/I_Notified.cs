using System;
using System.Collections.Generic;
using System.Text;

namespace JR.WUI.WAF
{
    // Interface d'un bureau
    public interface I_Notified
    {
        // Appel� suite � changement de contexte global
        void After_Context_Change(string Event, params object[] Params);

    }
}
