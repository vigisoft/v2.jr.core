using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common;

namespace JR.WUI.WAF
{
    partial class Db_Dump
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visual WebGui Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.C_Html = new Gizmox.WebGUI.Forms.HtmlBox();
            this.SuspendLayout();
            // 
            // C_Html
            // 
            this.C_Html.ContentType = "text/html";
            this.C_Html.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.C_Html.Expires = -1;
            this.C_Html.Html = "<HTML>No content.</HTML>";
            this.C_Html.Location = new System.Drawing.Point(0, 0);
            this.C_Html.Name = "C_Html";
            this.C_Html.Size = new System.Drawing.Size(430, 523);
            this.C_Html.TabIndex = 0;
            // 
            // Db_Dump
            // 
            this.Controls.Add(this.C_Html);
            this.FormBorderStyle = Gizmox.WebGUI.Forms.FormBorderStyle.SizableToolWindow;
            this.Size = new System.Drawing.Size(430, 523);
            this.Text = "Html";
            this.ResumeLayout(false);

        }

        #endregion

        private HtmlBox C_Html;


    }
}