#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace JR.WUI.WAF
{
    public partial class Line_Editor : Form
    {
        public Line_Editor()
        {
            InitializeComponent();
        }

        public Action<string> On_Valid = null;
        public string Initial_Text = "";
        public string Ok_Button_Text = null;
        public bool Read_Only = false;

        private void B_Ok_Click(object sender, EventArgs e)
        {
            Close();
            if (Read_Only) return;
            if (On_Valid != null) On_Valid(C_Text.Text);
        }

        private void B_Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void Text_Editor_Load(object sender, EventArgs e)
        {
            C_Text.Text = Initial_Text;
            if (Ok_Button_Text != null) B_Ok.Text = Ok_Button_Text;
            if (Read_Only)
            {
                B_Cancel.Visible = false;
                C_Text.ReadOnly = true;
            }
        }

        public static void Edit(Action<string> On_Valid, string Initial_Text = "", string Help_Text = "")
        {
            var L = new Line_Editor { Initial_Text = Initial_Text, On_Valid = On_Valid , Text = Help_Text};
            L.MinimizeBox = L.MaximizeBox = false;
            L.ShowDialog();
        }
    }
}