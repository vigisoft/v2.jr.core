#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace JR.WUI.WAF
{
    public partial class Editor : Form
    {
        public Editor()
        {
            InitializeComponent();
        }

        bool Wait_Blur = false;
        private void B_Ok_Click(object sender, EventArgs e)
        {
            if (!Has_Focus)
            {
                Quit();
                return;
            }

            // Attendre le signalement de perte de focus qui remonte �galement la valeur
            Wait_Blur = true;
            B_Ok.Enabled = false;
        }

        private void B_Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        string Original_Text = "";
        public string Html
        {
            get { return C_Editor.Text.Trim ('\0'); }
            set { C_Editor.Text = value; Original_Text = value; }
        }

        void Quit()
        {
            DialogResult = (Html == Original_Text) ? DialogResult.Cancel:DialogResult.OK;
            Close();
        }

        private void C_Editor_TextChanged(object sender, EventArgs e)
        {
            if (!Wait_Blur) return;
            Quit();
        }

        // CKGotFocus et CKLoastFocus doivent absoluement �tre wrapp�s pour que la d�tection de texte fonctionne
        bool Has_Focus = false;
        private void C_Editor_GotFocus(object sender, EventArgs e)
        {
            Has_Focus = true;
        }

        private void C_Editor_LostFocus(object sender, EventArgs e)
        {
            Has_Focus = false;
            if (!Wait_Blur) return;
            Quit();
        }

    }
}