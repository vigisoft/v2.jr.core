using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common;

namespace JR.WUI.WAF
{
    partial class Editor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visual WebGui Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Editor));
            this.C_Editor = new JR.WUI.CKEditor();
            this.B_Cancel = new Gizmox.WebGUI.Forms.Button();
            this.B_Ok = new Gizmox.WebGUI.Forms.Button();
            this.panel1 = new Gizmox.WebGUI.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // C_Editor
            // 
            this.C_Editor.CKBasePath = "CKeditor";
            this.C_Editor.CKContentsCss = "CKeditor/contents.css\n";
            this.C_Editor.CKIndentClasses = new string[0];
            this.C_Editor.CKProtectedSource = new string[0];
            this.C_Editor.CKTemplatesFiles = "CKeditor/plugins/templates/templates/default.js";
            this.C_Editor.CKToolbarFull = resources.GetString("C_Editor.CKToolbarFull");
            this.C_Editor.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.C_Editor.Expires = -1;
            this.C_Editor.Location = new System.Drawing.Point(0, 0);
            this.C_Editor.Name = "C_Editor";
            this.C_Editor.Size = new System.Drawing.Size(742, 528);
            this.C_Editor.TabIndex = 0;
            this.C_Editor.Text = "";
            this.C_Editor.CKGotFocus += new System.EventHandler(this.C_Editor_GotFocus);
            this.C_Editor.CKLostFocus += new System.EventHandler(this.C_Editor_LostFocus);
            this.C_Editor.TextChanged += new System.EventHandler(this.C_Editor_TextChanged);
            // 
            // B_Cancel
            // 
            this.B_Cancel.Dock = Gizmox.WebGUI.Forms.DockStyle.Right;
            this.B_Cancel.Location = new System.Drawing.Point(654, 0);
            this.B_Cancel.Name = "B_Cancel";
            this.B_Cancel.Size = new System.Drawing.Size(88, 27);
            this.B_Cancel.TabIndex = 1;
            this.B_Cancel.Text = "Annuler";
            this.B_Cancel.Click += new System.EventHandler(this.B_Cancel_Click);
            // 
            // B_Ok
            // 
            this.B_Ok.Dock = Gizmox.WebGUI.Forms.DockStyle.Right;
            this.B_Ok.Location = new System.Drawing.Point(557, 0);
            this.B_Ok.Name = "B_Ok";
            this.B_Ok.Size = new System.Drawing.Size(97, 27);
            this.B_Ok.TabIndex = 2;
            this.B_Ok.Text = "Ok";
            this.B_Ok.Click += new System.EventHandler(this.B_Ok_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.B_Ok);
            this.panel1.Controls.Add(this.B_Cancel);
            this.panel1.Dock = Gizmox.WebGUI.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 528);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(742, 27);
            this.panel1.TabIndex = 3;
            // 
            // Editor
            // 
            this.Controls.Add(this.C_Editor);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = Gizmox.WebGUI.Forms.FormBorderStyle.Sizable;
            this.Size = new System.Drawing.Size(742, 555);
            this.Text = "Editor";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private JR.WUI.CKEditor C_Editor;
        private Button B_Cancel;
        private Button B_Ok;
        private Panel panel1;


    }
}