#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace JR.WUI.WAF
{
    public partial class Text_Editor : Form
    {
        public Text_Editor()
        {
            InitializeComponent();
        }

        public Action<string> On_Valid = null;
        public Action<ICollection<string>> On_Valid_Lines = null;
        public string Initial_Text = "";
        public ICollection<string> Initial_Lines = null;
        public string Ok_Button_Text = null;
        public bool Read_Only = false;

        private void B_Ok_Click(object sender, EventArgs e)
        {
            Close();
            if (Read_Only) return;
            if (On_Valid != null) On_Valid(C_Text.Text);
            if (On_Valid_Lines != null) On_Valid_Lines(C_Text.Lines);
        }

        private void B_Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void Text_Editor_Load(object sender, EventArgs e)
        {
            if (Initial_Lines != null)
            {
                C_Text.Lines = Initial_Lines.ToArray();
            }
            else
            {
                C_Text.Text = Initial_Text;
            }
            if (Ok_Button_Text != null) B_Ok.Text = Ok_Button_Text;
            if (Read_Only)
            {
                B_Cancel.Visible = false;
                C_Text.ReadOnly = true;
            }
        }

        public static void Edit(Action<string> On_Valid, string Initial_Text = "")
        {
            new Text_Editor { Initial_Text = Initial_Text, On_Valid = On_Valid }.ShowDialog();
        }
        public static void Edit(Action<ICollection<string>> On_Valid_Lines, ICollection<string> Initial_Lines = null)
        {
            new Text_Editor { Initial_Lines = Initial_Lines, On_Valid_Lines = On_Valid_Lines }.ShowDialog();
        }
        public static void Show_Lines (ICollection<string> Initial_Lines)
        {
            new Text_Editor { Read_Only = true, Initial_Lines = Initial_Lines}.ShowDialog();
        }
    }
}