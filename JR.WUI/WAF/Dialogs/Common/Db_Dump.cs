#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace JR.WUI.WAF
{
    public partial class Db_Dump : Form
    {
        public Db_Dump()
        {
            InitializeComponent();
        }
        public string Content
        {
            set 
            {
                C_Html.Html = value;
            }
        }
    }
}