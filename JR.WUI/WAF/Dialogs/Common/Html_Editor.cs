#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace JR.WUI.WAF
{
    public partial class Html_Editor : Form
    {
        public Html_Editor()
        {
            InitializeComponent();
        }

        public Action<string> On_Valid = null;
        public string Initial_Text = "";

        private void B_Ok_Click(object sender, EventArgs e)
        {
            Close();
            if (On_Valid != null) On_Valid(C_Html.Value);
        }

        private void B_Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void Text_Editor_Load(object sender, EventArgs e)
        {
            C_Html.BasePath = "../../../../../../../../vwg/FCKEditor/";
            C_Html.Toolbar = new string[] {
"['Source','-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo']",
"['Bold','Italic','Underline','Strike']",
"['NumberedList','BulletedList','-','Outdent','Indent','Blockquote']",
"['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock']",
"['Link','Unlink']",
"['Image','Table','HorizontalRule']",
"'/'",
"['Styles','Format','Font','FontSize']",
"['TextColor','BGColor']"
            };
            C_Html.Value = Initial_Text;
        }

        public static void Edit(Action<string> On_Valid, string Initial_Text = "", bool Full_Screen = false)
        {
            var D = new Html_Editor { Initial_Text = Initial_Text, On_Valid = On_Valid };
            if (Full_Screen) D.Show_Max(true); else D.ShowDialog();
        }

        private void C_Html_ValueChange(object sender, EventArgs e)
        {

        }
    }
}