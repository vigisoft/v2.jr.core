using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common;

namespace JR.WUI.WAF
{
    partial class Text_Editor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visual WebGui Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.B_Cancel = new Gizmox.WebGUI.Forms.Button();
            this.B_Ok = new Gizmox.WebGUI.Forms.Button();
            this.panel1 = new Gizmox.WebGUI.Forms.Panel();
            this.C_Text = new Gizmox.WebGUI.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // B_Cancel
            // 
            this.B_Cancel.Dock = Gizmox.WebGUI.Forms.DockStyle.Right;
            this.B_Cancel.Location = new System.Drawing.Point(654, 0);
            this.B_Cancel.Name = "B_Cancel";
            this.B_Cancel.Size = new System.Drawing.Size(88, 32);
            this.B_Cancel.TabIndex = 1;
            this.B_Cancel.Text = "Annuler";
            this.B_Cancel.Click += new System.EventHandler(this.B_Cancel_Click);
            // 
            // B_Ok
            // 
            this.B_Ok.Dock = Gizmox.WebGUI.Forms.DockStyle.Right;
            this.B_Ok.Location = new System.Drawing.Point(557, 0);
            this.B_Ok.Name = "B_Ok";
            this.B_Ok.Size = new System.Drawing.Size(97, 32);
            this.B_Ok.TabIndex = 2;
            this.B_Ok.Text = "Ok";
            this.B_Ok.Click += new System.EventHandler(this.B_Ok_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.B_Ok);
            this.panel1.Controls.Add(this.B_Cancel);
            this.panel1.Dock = Gizmox.WebGUI.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 523);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(742, 32);
            this.panel1.TabIndex = 3;
            // 
            // C_Text
            // 
            this.C_Text.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.C_Text.Location = new System.Drawing.Point(0, 0);
            this.C_Text.Multiline = true;
            this.C_Text.Name = "C_Text";
            this.C_Text.ScrollBars = Gizmox.WebGUI.Forms.ScrollBars.Both;
            this.C_Text.Size = new System.Drawing.Size(742, 523);
            this.C_Text.TabIndex = 4;
            // 
            // Text_Editor
            // 
            this.Controls.Add(this.C_Text);
            this.Controls.Add(this.panel1);
            this.Size = new System.Drawing.Size(742, 555);
            this.Text = "Editor";
            this.Load += new System.EventHandler(this.Text_Editor_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Button B_Cancel;
        private Button B_Ok;
        private Panel panel1;
        private TextBox C_Text;


    }
}