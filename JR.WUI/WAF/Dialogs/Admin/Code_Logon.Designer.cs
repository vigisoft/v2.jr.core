namespace JR.WUI.WAF
{
    partial class Code_Logon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.C_Error = new Gizmox.WebGUI.Forms.Label();
            this.C_Memo = new Gizmox.WebGUI.Forms.CheckBox();
            this.button1 = new Gizmox.WebGUI.Forms.Button();
            this.panel1 = new Gizmox.WebGUI.Forms.Panel();
            this.C_Pass = new Gizmox.WebGUI.Forms.TextBox();
            this.label2 = new Gizmox.WebGUI.Forms.Label();
            this.C_Help = new Gizmox.WebGUI.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // C_Error
            // 
            this.C_Error.Dock = Gizmox.WebGUI.Forms.DockStyle.Bottom;
            this.C_Error.ForeColor = System.Drawing.Color.Red;
            this.C_Error.Location = new System.Drawing.Point(0, 62);
            this.C_Error.Name = "C_Error";
            this.C_Error.Size = new System.Drawing.Size(346, 35);
            this.C_Error.TabIndex = 6;
            this.C_Error.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // C_Memo
            // 
            this.C_Memo.Location = new System.Drawing.Point(82, 38);
            this.C_Memo.Name = "C_Memo";
            this.C_Memo.Size = new System.Drawing.Size(180, 24);
            this.C_Memo.TabIndex = 5;
            this.C_Memo.Text = "Mémoriser mon identifiant";
            // 
            // button1
            // 
            this.button1.DialogResult = Gizmox.WebGUI.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(254, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(83, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Se connecter";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.C_Pass);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.C_Memo);
            this.panel1.Controls.Add(this.C_Error);
            this.panel1.Controls.Add(this.C_Help);
            this.panel1.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(346, 125);
            this.panel1.TabIndex = 7;
            // 
            // C_Pass
            // 
            this.C_Pass.AllowDrag = false;
            this.C_Pass.Location = new System.Drawing.Point(82, 12);
            this.C_Pass.Name = "C_Pass";
            this.C_Pass.PasswordChar = 'X';
            this.C_Pass.Size = new System.Drawing.Size(169, 20);
            this.C_Pass.TabIndex = 4;
            this.C_Pass.WinFormsCompatibility.TextBoxRealTimeKeyboardEvents = Gizmox.WebGUI.Forms.WinFormsCompatibilityStates.True;
            this.C_Pass.EnterKeyDown += new Gizmox.WebGUI.Forms.KeyEventHandler(this.C_Pass_EnterKeyDown);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(13, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Identifiant :";
            // 
            // C_Help
            // 
            this.C_Help.Dock = Gizmox.WebGUI.Forms.DockStyle.Bottom;
            this.C_Help.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.C_Help.ForeColor = System.Drawing.SystemColors.InfoText;
            this.C_Help.Location = new System.Drawing.Point(0, 97);
            this.C_Help.Name = "C_Help";
            this.C_Help.Size = new System.Drawing.Size(346, 28);
            this.C_Help.TabIndex = 6;
            this.C_Help.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Code_Logon
            // 
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = Gizmox.WebGUI.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Size = new System.Drawing.Size(346, 125);
            this.StartPosition = Gizmox.WebGUI.Forms.FormStartPosition.CenterScreen;
            this.Text = "Identification";
            this.Load += new System.EventHandler(this.Logon_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Logon_Closing);
            this.Enter += new System.EventHandler(this.Logon_Enter);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Gizmox.WebGUI.Forms.CheckBox C_Memo;
        private Gizmox.WebGUI.Forms.Button button1;
        private Gizmox.WebGUI.Forms.Label C_Error;
        private Gizmox.WebGUI.Forms.Panel panel1;
        private Gizmox.WebGUI.Forms.TextBox C_Pass;
        private Gizmox.WebGUI.Forms.Label label2;
        private Gizmox.WebGUI.Forms.Label C_Help;
    }
}