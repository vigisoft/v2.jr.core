#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace JR.WUI.WAF
{
    public partial class Logon : Form
    {
        public Logon()
        {
            InitializeComponent();
        }

        public string Cookie_Prefix = "WUI.LOGON";
        public Func<string, string, bool> Identify;
        public EventHandler On_Logon;
        bool Can_Close = false;

        private void button1_Click(object sender, EventArgs e)
        {
            if (Identify(C_User.Text, C_Pass.Text))
            {
                if (C_Memo.Checked)
                {
                    VWGContext.Current.Cookies[Cookie_Prefix + ".USER"] = C_User.Text;
                    VWGContext.Current.Cookies[Cookie_Prefix + ".PASS"] = C_Pass.Text;
                }
                else
                {
                    VWGContext.Current.Cookies[Cookie_Prefix + ".USER"] = "";
                    VWGContext.Current.Cookies[Cookie_Prefix + ".PASS"] = "";
                };

                Can_Close = true;
                if (On_Logon != null)
                {
                    On_Logon(this, EventArgs.Empty);
                }
                this.Close();
            }
            else
            {
                C_Error.Text = T.Ui (".ERR");
            }
        }

        private void Logon_Closing(object sender, CancelEventArgs e)
        {
            if (!Can_Close) e.Cancel = true;
        }

        private void Logon_Enter(object sender, EventArgs e)
        {
            //button1_Click(sender, e);
        }

        private void C_Pass_Enter(object sender, EventArgs e)
        {
            //button1_Click(sender, e);
        }

        Translator T = new Translator("LOGONBOX");
        private void Logon_Load(object sender, EventArgs e)
        {
            C_Memo.Checked = (VWGContext.Current.Cookies[Cookie_Prefix + ".USER"] != "");
            if (C_Memo.Checked)
            {
                C_User.Text = VWGContext.Current.Cookies[Cookie_Prefix + ".USER"];
                C_Pass.Text = VWGContext.Current.Cookies[Cookie_Prefix + ".PASS"];
            }
            if (C_User.Text.Length > 0)
            {
                B_Connect.Focus();
            }
            else
            {
                C_User.Focus();
            }
        }

        private void C_Pass_Enter(object objSender, KeyEventArgs objArgs)
        {
            button1_Click(objSender, objArgs);
        }
    }
}