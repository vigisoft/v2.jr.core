#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace JR.WUI.WAF
{
    public partial class Logout : Form
    {
        public Logout()
        {
            InitializeComponent();
        }

        public string Cookie_Prefix = "WUI.LOGON";

        Translator T = new Translator("LOGOUTBOX");

        private void Logon_Load(object sender, EventArgs e)
        {
            VWGContext.Current.Cookies[Cookie_Prefix + ".USER"] = "";
            VWGContext.Current.Cookies[Cookie_Prefix + ".PASS"] = "";
            C_Error.Text = T.Ui(".OUT");
        }

    }
}