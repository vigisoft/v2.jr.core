namespace JR.WUI.WAF
{
    partial class Logon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.C_Error = new Gizmox.WebGUI.Forms.Label();
            this.C_Memo = new Gizmox.WebGUI.Forms.CheckBox();
            this.C_Pass = new Gizmox.WebGUI.Forms.TextBox();
            this.C_User = new Gizmox.WebGUI.Forms.TextBox();
            this.B_Connect = new Gizmox.WebGUI.Forms.Button();
            this.label2 = new Gizmox.WebGUI.Forms.Label();
            this.label1 = new Gizmox.WebGUI.Forms.Label();
            this.panel1 = new Gizmox.WebGUI.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // C_Error
            // 
            this.C_Error.ForeColor = System.Drawing.Color.Red;
            this.C_Error.Location = new System.Drawing.Point(3, 71);
            this.C_Error.Name = "C_Error";
            this.C_Error.Size = new System.Drawing.Size(338, 23);
            this.C_Error.TabIndex = 6;
            this.C_Error.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // C_Memo
            // 
            this.C_Memo.Location = new System.Drawing.Point(13, 104);
            this.C_Memo.Name = "C_Memo";
            this.C_Memo.Size = new System.Drawing.Size(180, 24);
            this.C_Memo.TabIndex = 5;
            this.C_Memo.Text = "Mémoriser les identifiants";
            // 
            // C_Pass
            // 
            this.C_Pass.AllowDrag = false;
            this.C_Pass.Location = new System.Drawing.Point(131, 45);
            this.C_Pass.Name = "C_Pass";
            this.C_Pass.PasswordChar = 'X';
            this.C_Pass.Size = new System.Drawing.Size(200, 20);
            this.C_Pass.TabIndex = 4;
            this.C_Pass.WinFormsCompatibility.TextBoxRealTimeKeyboardEvents = Gizmox.WebGUI.Forms.WinFormsCompatibilityStates.True;
            this.C_Pass.EnterKeyDown += new Gizmox.WebGUI.Forms.KeyEventHandler(this.C_Pass_Enter);
            // 
            // C_User
            // 
            this.C_User.AllowDrag = false;
            this.C_User.Location = new System.Drawing.Point(131, 13);
            this.C_User.Name = "C_User";
            this.C_User.Size = new System.Drawing.Size(200, 20);
            this.C_User.TabIndex = 3;
            this.C_User.WinFormsCompatibility.TextBoxRealTimeKeyboardEvents = Gizmox.WebGUI.Forms.WinFormsCompatibilityStates.True;
            // 
            // B_Connect
            // 
            this.B_Connect.DialogResult = Gizmox.WebGUI.Forms.DialogResult.OK;
            this.B_Connect.Location = new System.Drawing.Point(218, 104);
            this.B_Connect.Name = "B_Connect";
            this.B_Connect.Size = new System.Drawing.Size(113, 23);
            this.B_Connect.TabIndex = 2;
            this.B_Connect.Text = "Se connecter";
            this.B_Connect.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(13, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mot de passe :";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(13, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nom d\'utilisateur :";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.B_Connect);
            this.panel1.Controls.Add(this.C_Error);
            this.panel1.Controls.Add(this.C_Memo);
            this.panel1.Controls.Add(this.C_User);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.C_Pass);
            this.panel1.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(346, 140);
            this.panel1.TabIndex = 7;
            // 
            // Logon
            // 
            this.AcceptButton = this.B_Connect;
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = Gizmox.WebGUI.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Size = new System.Drawing.Size(346, 140);
            this.StartPosition = Gizmox.WebGUI.Forms.FormStartPosition.CenterScreen;
            this.Text = "Identification";
            this.Load += new System.EventHandler(this.Logon_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Logon_Closing);
            this.Enter += new System.EventHandler(this.Logon_Enter);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Gizmox.WebGUI.Forms.CheckBox C_Memo;
        private Gizmox.WebGUI.Forms.TextBox C_Pass;
        private Gizmox.WebGUI.Forms.TextBox C_User;
        private Gizmox.WebGUI.Forms.Button B_Connect;
        private Gizmox.WebGUI.Forms.Label label2;
        private Gizmox.WebGUI.Forms.Label label1;
        private Gizmox.WebGUI.Forms.Label C_Error;
        private Gizmox.WebGUI.Forms.Panel panel1;



    }
}