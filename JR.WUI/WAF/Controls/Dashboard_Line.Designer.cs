using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common;

namespace JR.WUI.WAF
{
    partial class Dashboard_Line
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visual WebGui UserControl Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.C_Title = new Gizmox.WebGUI.Forms.Label();
            this.C_Count = new Gizmox.WebGUI.Forms.Label();
            this.B_Count = new Gizmox.WebGUI.Forms.Button();
            this.SuspendLayout();
            // 
            // C_Title
            // 
            this.C_Title.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)(((Gizmox.WebGUI.Forms.AnchorStyles.Top | Gizmox.WebGUI.Forms.AnchorStyles.Left) 
            | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
            this.C_Title.Font = new System.Drawing.Font("Verdana", 10F);
            this.C_Title.Location = new System.Drawing.Point(2, 2);
            this.C_Title.Name = "C_Title";
            this.C_Title.Padding = new Gizmox.WebGUI.Forms.Padding(4, 0, 0, 0);
            this.C_Title.Size = new System.Drawing.Size(311, 19);
            this.C_Title.TabIndex = 0;
            this.C_Title.Text = "label1";
            // 
            // C_Count
            // 
            this.C_Count.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((Gizmox.WebGUI.Forms.AnchorStyles.Top | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
            this.C_Count.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.C_Count.Location = new System.Drawing.Point(330, 2);
            this.C_Count.Name = "C_Count";
            this.C_Count.Padding = new Gizmox.WebGUI.Forms.Padding(0, 0, 4, 0);
            this.C_Count.Size = new System.Drawing.Size(59, 19);
            this.C_Count.TabIndex = 0;
            this.C_Count.Text = "label1";
            this.C_Count.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // B_Count
            // 
            this.B_Count.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((Gizmox.WebGUI.Forms.AnchorStyles.Top | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
            this.B_Count.CustomStyle = "F";
            this.B_Count.FlatStyle = Gizmox.WebGUI.Forms.FlatStyle.Flat;
            this.B_Count.Font = new System.Drawing.Font("Verdana", 10F);
            this.B_Count.Location = new System.Drawing.Point(330, 2);
            this.B_Count.Name = "B_Count";
            this.B_Count.Padding = new Gizmox.WebGUI.Forms.Padding(0, 0, 4, 0);
            this.B_Count.Size = new System.Drawing.Size(59, 19);
            this.B_Count.TabIndex = 1;
            this.B_Count.Text = "button1";
            this.B_Count.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Dashboard_Line
            // 
            this.Controls.Add(this.B_Count);
            this.Controls.Add(this.C_Count);
            this.Controls.Add(this.C_Title);
            this.DockPadding.All = 2;
            this.Padding = new Gizmox.WebGUI.Forms.Padding(2);
            this.Size = new System.Drawing.Size(391, 29);
            this.Text = "Dashboard_Line";
            this.Load += new System.EventHandler(this.Dashboard_Line_Load);
            this.ResumeLayout(false);

        }

        #endregion

        internal Label C_Title;
        internal Label C_Count;
        public Button B_Count;


    }
}