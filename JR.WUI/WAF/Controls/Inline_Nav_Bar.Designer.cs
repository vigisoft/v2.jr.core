using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common;

namespace JR.WUI.WAF
{
    partial class Inline_Nav_Bar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visual WebGui UserControl Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Inline_Nav_Bar));
            this.flowLayoutPanel2 = new Gizmox.WebGUI.Forms.Panel();
            this.B_Db = new Gizmox.WebGUI.Forms.Button();
            this.toolTip1 = new Gizmox.WebGUI.Forms.ToolTip();
            this.C_Message = new Gizmox.WebGUI.Forms.Label();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.B_Db);
            this.flowLayoutPanel2.Dock = Gizmox.WebGUI.Forms.DockStyle.Left;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(34, 25);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // B_Db
            // 
            this.B_Db.CustomStyle = "F";
            this.B_Db.FlatStyle = Gizmox.WebGUI.Forms.FlatStyle.Flat;
            this.B_Db.Image = new Gizmox.WebGUI.Common.Resources.IconResourceHandle(resources.GetString("B_Db.Image"));
            this.B_Db.Location = new System.Drawing.Point(0, 0);
            this.B_Db.Name = "B_Db";
            this.B_Db.Size = new System.Drawing.Size(25, 23);
            this.B_Db.TabIndex = 0;
            this.toolTip1.SetToolTip(this.B_Db, "Contenu base de donn�es");
            // 
            // C_Message
            // 
            this.C_Message.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.C_Message.Location = new System.Drawing.Point(34, 0);
            this.C_Message.Name = "C_Message";
            this.C_Message.Size = new System.Drawing.Size(708, 25);
            this.C_Message.TabIndex = 2;
            this.C_Message.Text = "label1";
            this.C_Message.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Inline_Nav_Bar
            // 
            this.BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.FixedSingle;
            this.BorderWidth = new Gizmox.WebGUI.Forms.BorderWidth(0, 0, 0, 1);
            this.Controls.Add(this.C_Message);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Size = new System.Drawing.Size(742, 25);
            this.Text = "Nav_Bar";
            this.Load += new System.EventHandler(this.Nav_Bar_Load);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel flowLayoutPanel2;
        private ToolTip toolTip1;
        private Label C_Message;
        public Button B_Db;


    }
}