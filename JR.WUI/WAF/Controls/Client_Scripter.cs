#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;
using System.Web.SessionState;
using System.Threading;
using System.Collections.Concurrent;
using JR;
using JR.Saf;

#endregion

namespace JR.WUI
{
    public partial class Client_Scripter : UserControl
    {
        public Client_Scripter()
        {
            InitializeComponent();
        }

        private void Client_Notifier_Load(object sender, EventArgs e)
        {
            Dock = DockStyle.Bottom;
            Height = 0;
            BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.None;
        }

        StringBuilder Script_Content = new StringBuilder();
        public void Append(string Script, params object [] Params)
        {
            Script_Content.AppendLine(string.Format (Script, Params));
        }
        public void Execute(string Script = "")
        {
            if (Script != "") Append(Script);
            if (Script_Content.Length== 0) return;
            Register();
        }
        public void Register()
        {
            ClientId = Guid.NewGuid().ToString("N");
            Script_Box.Html = string.Format ("<script>// {0}\n", ClientId) + Script_Content.ToString() + "</script>";
            Script_Content.Clear();
        }

        public void UnRegister()
        {
           Script_Box.Html = "";
        }

        public static Client_Scripter Link(Control Parent, bool Is_Bootstrap = false)
        {
            var S = new Client_Scripter ();
            Parent.Controls.Add(S);
            if (Is_Bootstrap)
            {
                S.Load_Script("//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js");
                S.Load_Css("/vwg/bs.css?v=@STAMP");
            }

            return S;
        }

        public void Load_Css(string URL)
        {
            string Code = "{{var Tag= document.createElement('link');Tag.rel='stylesheet';Tag.type='text/css';Tag.href='"
                + URL.Replace("@STAMP", Main.Stamp) + "';document.getElementsByTagName('head')[0].appendChild(Tag);}}";
            Append(Code);
        }
        public void Load_Script(string URL)
        {
            string Code = "{{var Tag= document.createElement('script');Tag.rel='stylesheet';Tag.src='"
                + URL.Replace("@STAMP", Main.Stamp) + "';document.getElementsByTagName('head')[0].appendChild(Tag);}}";
            Append(Code);
        }
    }
}