using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common;

namespace JR.WUI
{
    partial class Server_Notifier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visual WebGui UserControl Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Script_Box = new Gizmox.WebGUI.Forms.HtmlBox();
            this.SuspendLayout();
            // 
            // Script_Box
            // 
            this.Script_Box.ContentType = "text/html";
            this.Script_Box.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.Script_Box.Html = "<HTML>No content.</HTML>";
            this.Script_Box.IsWindowless = true;
            this.Script_Box.Location = new System.Drawing.Point(0, 0);
            this.Script_Box.Name = "Script_Box";
            this.Script_Box.Size = new System.Drawing.Size(391, 306);
            this.Script_Box.TabIndex = 2;
            // 
            // Client_Notifier
            // 
            this.BorderColor = new Gizmox.WebGUI.Forms.BorderColor(System.Drawing.Color.Red);
            this.BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.Script_Box);
            this.Size = new System.Drawing.Size(391, 306);
            this.Text = "Client_Notifier";
            this.Load += new System.EventHandler(this.Client_Notifier_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private HtmlBox Script_Box;


    }
}