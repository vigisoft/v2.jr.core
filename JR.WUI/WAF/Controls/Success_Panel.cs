#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace JR.WUI
{
    public partial class Success_Panel : UserControl
    {
        public Success_Panel()
        {
            InitializeComponent();
        }

        private void B_Hide_Click(object sender, EventArgs e)
        {
            this.ParentForm.Close();
        }

        private void Success_Panel_Load(object sender, EventArgs e)
        {
            L_Success.Text = this.Text;
            ParentForm.CancelButton = B_Hide;
            ParentForm.AcceptButton = B_Hide;
            B_Hide.Focus();
        }

        public static void Show_Error(Form Form, string Text, Control Panel = null)
        {
            Show_Success(Form, Text, Panel, false);
        }

        public static void Show_Success (Form Form, string Text, Control Panel = null, bool Show_Error = false)
        {
            Success_Panel P = new Success_Panel { Text = Text };
            if (Show_Error)
            {
                P.BackColor = Color.LavenderBlush;
                P.L_Success.ForeColor = Color.DarkRed;
            }
            P.Dock = DockStyle.Bottom;
            foreach (Control C in Form.Controls)
            {
                if (!(C is Label)) C.Enabled = false;
            }
            if (Panel != null)
            {
                Panel.Controls.Clear();
                Panel.Controls.Add(P);
                Panel.Enabled = true;
            }
            else
            {
                Form.Controls.Add(P);
            }
        }

    }
}