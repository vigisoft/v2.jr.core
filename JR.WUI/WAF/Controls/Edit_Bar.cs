#region Using

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace JR.WUI.WAF
{
    public partial class Edit_Bar : UserControl
    {
        public Edit_Bar()
        {
            InitializeComponent();
        }

        public void Remove_Edit()
        {
            this.Controls.Remove(P_Edit);
        }

        public void Merge(Control Tool_Bar, bool Left = false)
        {
            var L = new ArrayList(Tool_Bar.Controls);
            foreach (Control C in L)
            {
                Tool_Bar.Controls.Remove(C);
                Tools.Controls.Add(C);
            }
            Tool_Bar.Visible = false;
            if (Left) Tools.FlowDirection = FlowDirection.LeftToRight;
        }
    }
}