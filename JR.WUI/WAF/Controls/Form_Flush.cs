#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace JR.WUI
{
    public partial class Form_Flush : UserControl
    {
        public Form_Flush()
        {
            InitializeComponent();
        }

        const string Gen_Script = @"
    <script>
        var Flush_Cycle = '{CYCLE}';
        var objForm = vwgContext.provider.getComponentByClientId('{ID}');
        function Run_Flush ()
        {
           vwgContext.events.performClientEvent(objForm.id(), 'Notify', '', true);
           vwgContext.events.raiseEvents();
        }
        objForm.focus();
        setTimeout(Run_Flush, 1000);
    </script>
";


        public Action<object> On_Notify = (Message) => { };

        protected override void FireEvent(Gizmox.WebGUI.Common.Interfaces.IEvent objEvent)
        {
            if (objEvent.Type == "Notify")
            {
                object Message = null;
                On_Notify(Message);
            }
            else
            {
                base.FireEvent(objEvent);
            }
        }

        public bool Attach(Form Form, Action<object> On_Notify)
        {
            Form.Controls.Add(this);
            this.On_Notify = On_Notify;
            return true;
        }

        public void Force_Flush()
        {
            Script_Box.Html = Gen_Script.Replace("{ID}", ClientId).Replace("{CYCLE}", Guid.NewGuid().ToString("N"));
        }

        private void Form_Flush_Load(object sender, EventArgs e)
        {
            Width = 0;
            Height = 0;
            Top = -10;
            ClientId = Guid.NewGuid().ToString("N");
        }

    }
}