using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common;

namespace JR.WUI
{
    partial class Error_Panel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visual WebGui UserControl Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.B_Hide = new Gizmox.WebGUI.Forms.Button();
            this.P_Mess = new Gizmox.WebGUI.Forms.FlowLayoutPanel();
            this.L_Error = new Gizmox.WebGUI.Forms.Label();
            this.P_Mess.SuspendLayout();
            this.SuspendLayout();
            // 
            // B_Hide
            // 
            this.B_Hide.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((Gizmox.WebGUI.Forms.AnchorStyles.Top | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
            this.B_Hide.CustomStyle = "F";
            this.B_Hide.FlatStyle = Gizmox.WebGUI.Forms.FlatStyle.Flat;
            this.B_Hide.Location = new System.Drawing.Point(560, 0);
            this.B_Hide.Name = "B_Hide";
            this.B_Hide.Size = new System.Drawing.Size(23, 25);
            this.B_Hide.TabIndex = 1;
            this.B_Hide.Text = "X";
            this.B_Hide.Click += new System.EventHandler(this.B_Hide_Click);
            // 
            // P_Mess
            // 
            this.P_Mess.BackColor = System.Drawing.Color.Transparent;
            this.P_Mess.Controls.Add(this.L_Error);
            this.P_Mess.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.P_Mess.Location = new System.Drawing.Point(0, 0);
            this.P_Mess.Name = "P_Mess";
            this.P_Mess.Size = new System.Drawing.Size(583, 29);
            this.P_Mess.TabIndex = 2;
            // 
            // L_Error
            // 
            this.L_Error.AutoSize = true;
            this.L_Error.BackColor = System.Drawing.Color.LavenderBlush;
            this.L_Error.BorderColor = new Gizmox.WebGUI.Forms.BorderColor(System.Drawing.Color.LightCoral);
            this.L_Error.BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.FixedSingle;
            this.L_Error.ForeColor = System.Drawing.Color.DarkRed;
            this.L_Error.Location = new System.Drawing.Point(4, 4);
            this.L_Error.Margin = new Gizmox.WebGUI.Forms.Padding(4);
            this.L_Error.Name = "L_Error";
            this.L_Error.Size = new System.Drawing.Size(37, 15);
            this.L_Error.TabIndex = 0;
            this.L_Error.Text = "label1";
            // 
            // Error_Panel
            // 
            this.BackColor = System.Drawing.Color.LavenderBlush;
            this.BorderColor = new Gizmox.WebGUI.Forms.BorderColor(System.Drawing.Color.Red);
            this.BorderWidth = new Gizmox.WebGUI.Forms.BorderWidth(0, 1, 0, 1);
            this.Controls.Add(this.B_Hide);
            this.Controls.Add(this.P_Mess);
            this.Size = new System.Drawing.Size(583, 29);
            this.Text = "Error_Panel";
            this.P_Mess.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Button B_Hide;
        private FlowLayoutPanel P_Mess;
        private Label L_Error;



    }
}