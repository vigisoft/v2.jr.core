#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace JR.WUI.WAF
{
    public partial class ConfirmBox : Form
    {
        public ConfirmBox()
        {
            InitializeComponent();
        }

        public ConfirmBox(string Title, string Message, object Context, EventHandler On_Yes)
        {
            InitializeComponent();
            this.AcceptButton = B_Ok;
            this.Text = Title;
            C_Message.Text = Message;
            _Context = Context;
            this.On_Yes += On_Yes;
        }

        public ConfirmBox(string Message, object Context, EventHandler On_Yes)
        {
            InitializeComponent();
            this.FormBorderStyle = Gizmox.WebGUI.Forms.FormBorderStyle.FixedSingle;
            this.AcceptButton = B_Ok;
            C_Message.Text = Message;
            _Context = Context;
            this.On_Yes += On_Yes;
        }

        public string Message
        {
            get { return C_Message.Text; }
            set { C_Message.Text = value; }
        }

        public string Title
        {
            get { return Text; }
            set { Text = value; }
        }

        object _Context = null;
        public object Context
        {
            get { return _Context; }
            set { _Context = value; }
        }

        public event EventHandler On_Yes;

        private void B_Ok_Click(object sender, EventArgs e)
        {
            Close();
            if (On_Yes != null) On_Yes(_Context, EventArgs.Empty);
        }

        private void B_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ConfirmBox_Enter(object sender, EventArgs e)
        {
            B_Ok_Click(sender, e);
        }

        public static void Ask(string Message, object Context, EventHandler On_Yes)
        {
            new ConfirmBox(Message, Context, On_Yes).ShowDialog();
        }
        public static void Ask(Control Field, string Message, object Context, EventHandler On_Yes)
        {
            new ConfirmBox(Message, Context, On_Yes).ShowPopup(Field, DialogAlignment.Below);
        }

        public static void Ask(string Message, Action On_Yes)
        {
            new ConfirmBox(Message, null, (sender, e) => { On_Yes(); }).ShowDialog();
        }
        public static void Ask(Control Field, string Message, Action On_Yes)
        {
            new ConfirmBox(Message, null, (sender, e) => { On_Yes(); }).ShowPopup(Field, DialogAlignment.Below);
        }

        private void ConfirmBox_Load(object sender, EventArgs e)
        {
            Translator T = new Translator("CONFIRMBOX");
            T.Label(B_Ok, B_Cancel);
        }
    }
}