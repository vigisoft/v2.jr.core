#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace JR.WUI.WAF
{
    public partial class Code_Logon : Form
    {
        public Code_Logon()
        {
            InitializeComponent();
        }

        public bool Auto_Connect = false;
        public string Cookie_Prefix = "WUI.LOGON";
        public Func<string, bool> Identify;
        public EventHandler On_Logon;
        bool Can_Close = false;

        private void button1_Click(object sender, EventArgs e)
        {
            if (Identify(C_Pass.Text))
            {
                if (C_Memo.Checked)
                {
                    VWGContext.Current.Cookies[Cookie_Prefix + ".PASS"] = C_Pass.Text;
                }
                else
                {
                    VWGContext.Current.Cookies[Cookie_Prefix + ".PASS"] = "";
                };

                Can_Close = true;
                if (On_Logon != null)
                {
                    On_Logon(this, EventArgs.Empty);
                }
                this.Close();
            }
            else if (sender != null)
            {
                C_Error.Text = T.Ui (".ERR");
            }
        }

        private void Logon_Closing(object sender, CancelEventArgs e)
        {
            if (!Can_Close) e.Cancel = true;
        }

        private void Logon_Enter(object sender, EventArgs e)
        {
            //button1_Click(sender, e);
        }

        private void C_Pass_Enter(object sender, EventArgs e)
        {
            button1_Click(sender, e);
        }

        Translator T = new Translator("LOGONBOX");
        private void Logon_Load(object sender, EventArgs e)
        {
            C_Memo.Checked = (VWGContext.Current.Cookies[Cookie_Prefix + ".PASS"] != "");
            if (C_Memo.Checked)
            {
                C_Pass.Text = VWGContext.Current.Cookies[Cookie_Prefix + ".PASS"];
                if (Auto_Connect)
                {
                    button1_Click(null, EventArgs.Empty);
                }
            }
            button1.Focus();
        }

        private void C_Pass_EnterKeyDown(object objSender, KeyEventArgs objArgs)
        {
            button1_Click(objSender, objArgs);
        }

    }
}