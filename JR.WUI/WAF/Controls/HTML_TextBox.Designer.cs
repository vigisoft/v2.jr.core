using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common;

namespace JR.WUI.WAF
{
    partial class HTML_TextBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visual WebGui UserControl Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HTML_TextBox));
            this.P_Header = new Gizmox.WebGUI.Forms.Panel();
            this.P_Left = new Gizmox.WebGUI.Forms.FlowLayoutPanel();
            this.I_Header = new Gizmox.WebGUI.Forms.PictureBox();
            this.C_Title = new Gizmox.WebGUI.Forms.Label();
            this.R_Text = new Gizmox.WebGUI.Forms.RadioButton();
            this.R_Html = new Gizmox.WebGUI.Forms.RadioButton();
            this.B_Edit = new Gizmox.WebGUI.Forms.Button();
            this.C_Text = new Gizmox.WebGUI.Forms.TextBox();
            this.C_Html = new Gizmox.WebGUI.Forms.HtmlBox();
            this.P_Header.SuspendLayout();
            this.P_Left.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.I_Header)).BeginInit();
            this.SuspendLayout();
            // 
            // P_Header
            // 
            this.P_Header.Controls.Add(this.P_Left);
            this.P_Header.Controls.Add(this.R_Text);
            this.P_Header.Controls.Add(this.R_Html);
            this.P_Header.Controls.Add(this.B_Edit);
            this.P_Header.Dock = Gizmox.WebGUI.Forms.DockStyle.Top;
            this.P_Header.Location = new System.Drawing.Point(0, 0);
            this.P_Header.Name = "P_Header";
            this.P_Header.Size = new System.Drawing.Size(243, 23);
            this.P_Header.TabIndex = 0;
            // 
            // P_Left
            // 
            this.P_Left.Controls.Add(this.I_Header);
            this.P_Left.Controls.Add(this.C_Title);
            this.P_Left.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.P_Left.DockPadding.Right = 10;
            this.P_Left.Location = new System.Drawing.Point(0, 0);
            this.P_Left.Name = "P_Left";
            this.P_Left.Padding = new Gizmox.WebGUI.Forms.Padding(0, 0, 10, 0);
            this.P_Left.Size = new System.Drawing.Size(59, 23);
            this.P_Left.TabIndex = 1;
            // 
            // I_Header
            // 
            this.I_Header.BorderWidth = new Gizmox.WebGUI.Forms.BorderWidth(0);
            this.I_Header.Location = new System.Drawing.Point(0, 0);
            this.I_Header.Name = "I_Header";
            this.I_Header.Size = new System.Drawing.Size(16, 11);
            this.I_Header.TabIndex = 7;
            this.I_Header.TabStop = false;
            // 
            // C_Title
            // 
            this.C_Title.AutoSize = true;
            this.C_Title.Location = new System.Drawing.Point(16, 0);
            this.C_Title.Name = "C_Title";
            this.C_Title.Size = new System.Drawing.Size(27, 13);
            this.C_Title.TabIndex = 0;
            this.C_Title.Text = "Title";
            // 
            // R_Text
            // 
            this.R_Text.Dock = Gizmox.WebGUI.Forms.DockStyle.Right;
            this.R_Text.Location = new System.Drawing.Point(59, 0);
            this.R_Text.Name = "R_Text";
            this.R_Text.Size = new System.Drawing.Size(98, 23);
            this.R_Text.TabIndex = 0;
            this.R_Text.Text = "Texte simple";
            this.R_Text.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            // 
            // R_Html
            // 
            this.R_Html.Dock = Gizmox.WebGUI.Forms.DockStyle.Right;
            this.R_Html.Location = new System.Drawing.Point(157, 0);
            this.R_Html.Name = "R_Html";
            this.R_Html.Size = new System.Drawing.Size(61, 23);
            this.R_Html.TabIndex = 0;
            this.R_Html.Text = "Html";
            this.R_Html.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            // 
            // B_Edit
            // 
            this.B_Edit.CustomStyle = "F";
            this.B_Edit.Dock = Gizmox.WebGUI.Forms.DockStyle.Right;
            this.B_Edit.FlatStyle = Gizmox.WebGUI.Forms.FlatStyle.Flat;
            this.B_Edit.Image = new Gizmox.WebGUI.Common.Resources.IconResourceHandle(resources.GetString("B_Edit.Image"));
            this.B_Edit.Location = new System.Drawing.Point(218, 0);
            this.B_Edit.Name = "B_Edit";
            this.B_Edit.Size = new System.Drawing.Size(25, 23);
            this.B_Edit.TabIndex = 0;
            this.B_Edit.Click += new System.EventHandler(this.B_Edit_Click);
            // 
            // C_Text
            // 
            this.C_Text.Location = new System.Drawing.Point(3, 26);
            this.C_Text.Multiline = true;
            this.C_Text.Name = "C_Text";
            this.C_Text.Size = new System.Drawing.Size(78, 54);
            this.C_Text.TabIndex = 1;
            this.C_Text.TextChanged += new System.EventHandler(this.C_Text_TextChanged);
            // 
            // C_Html
            // 
            this.C_Html.BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.FixedSingle;
            this.C_Html.ContentType = "text/html";
            this.C_Html.Html = "<HTML>No content.</HTML>";
            this.C_Html.IsWindowless = true;
            this.C_Html.Location = new System.Drawing.Point(3, 83);
            this.C_Html.Name = "C_Html";
            this.C_Html.Size = new System.Drawing.Size(92, 133);
            this.C_Html.TabIndex = 2;
            // 
            // HTML_TextBox
            // 
            this.BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.FixedSingle;
            this.BorderWidth = new Gizmox.WebGUI.Forms.BorderWidth(0, 0, 0, 1);
            this.Controls.Add(this.C_Html);
            this.Controls.Add(this.C_Text);
            this.Controls.Add(this.P_Header);
            this.Size = new System.Drawing.Size(243, 295);
            this.Text = "Nav_Bar";
            this.Load += new System.EventHandler(this.HTML_TextBox_Load);
            this.P_Header.ResumeLayout(false);
            this.P_Left.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.I_Header)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel P_Header;
        public Button B_Edit;
        private FlowLayoutPanel P_Left;
        private TextBox C_Text;
        private HtmlBox C_Html;
        private PictureBox I_Header;
        private Label C_Title;
        private RadioButton R_Text;
        private RadioButton R_Html;



    }
}