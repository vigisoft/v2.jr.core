#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace JR.WUI
{
    public partial class Error_Panel : UserControl
    {
        public Error_Panel()
        {
            InitializeComponent();
            Clear();
        }

        // Supprime toutes les notifications d'erreur
        public void Clear ()
        {
            P_Mess.Controls.Clear();
            this.Visible = false;
            Errors.Reverse();  // Pour restituer correctement les couleurs qd un champ a eu plusieurs erreurs
            foreach (var E in Errors)
            {
                E.Field.BorderColor = (BorderColor)E.Old_State;
            }
            Errors.Clear();
        }

        public class Error
        {
            public Control Field;
            public string Message;
            public object Old_State;
        }
        public List<Error> Errors = new List<Error>();
        public int Error_Count
        {
            get { return Errors.Count; }
        }

        public bool Add_Err(Control Field, string Mess)
        {
            Errors.Add(new Error { Field = Field, Message = Mess });
            return true;
        }

        public bool Show_Errors()
        {
            foreach (var E in Errors)
            {
                E.Old_State = E.Field.BorderColor;
                E.Field.BorderColor = new BorderColor(System.Drawing.Color.Red);
                if (E.Message != null)
                {
                    Label Lb = new Label(E.Message);
                    Lb.AutoSize = true;
                    Lb.Margin = L_Error.Margin;
                    Lb.BorderStyle = L_Error.BorderStyle;
                    Lb.BorderColor = L_Error.BorderColor;
                    Lb.ForeColor = L_Error.ForeColor;
                    Lb.BackColor = L_Error.BackColor;
                    P_Mess.Controls.Add(Lb);
                }
                this.Visible = true;
            }
            return Errors.Count > 0;
        }

        public bool Bad_Format(string Validation_Expression, string Message, Control Field)
        {
            string Value = Field.Text.Trim();
            if (Value == "") return false;
            string Field_Name = Field.Name.Substring(2);
            if (JR.Strings.Match(Value, Validation_Expression, true)) return false;
            return Add_Err(Field, Message);
        }

        public bool Bad_Email(string Message, Control Field)
        {
            string Value = Field.Text.Trim();
            if (Value == "") return false;
            string Field_Name = Field.Name.Substring(2);
            if (JR.Mails.Is_Valid_Address(Value)) return false;
            return Add_Err(Field, Message);
        }

        public bool Bad_Len(int Min_Len, string Message, params Control[] Controls)
        {
            bool Errs = false;
            foreach (var C in Controls)
            {
                if (C.Text.Trim().Length < Min_Len)
                {
                    Errs = true;
                    Add_Err(C, string.Format(Message, Min_Len));
                }
            }
            return Errs;
        }

        private void B_Hide_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

    }
}