#region Using

using System;
using Gizmox.WebGUI.Forms;

#endregion

namespace JR.WUI.WAF
{
    public partial class Grid_Editor : Form
    {
        public Grid_Editor()
        {
            InitializeComponent();
        }

        public static void Show (Control Grid, string Title)
        {
            Grid_Editor G = new Grid_Editor();
            G.Controls.Add(Grid);
            int Min_Width = Grid.Width + 20;
            int Min_Height = Grid.Height + 50;
            if (Min_Width > G.Width) G.Width = Min_Width;
            if (Min_Height > G.Height) G.Height = Min_Height;
            Grid.Dock = DockStyle.Fill;
            G.Text = Title;
            G.ShowDialog();
        }

        private void Grid_Editor_Load(object sender, EventArgs e)
        {

        }
    }
}