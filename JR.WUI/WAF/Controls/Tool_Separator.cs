using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Resources;

namespace JR.WUI.WAF
{
    public class Tool_Separator : ToolBarButton
    {
        public Tool_Separator()
        {
            this.Style = ToolBarButtonStyle.Separator;
        }
    }
}
