using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common;

namespace JR.WUI.WAF
{
    partial class Inline_Action_Bar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visual WebGui UserControl Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.B_Cancel = new Gizmox.WebGUI.Forms.Button();
            this.B_Save = new Gizmox.WebGUI.Forms.Button();
            this.Tools = new Gizmox.WebGUI.Forms.FlowLayoutPanel();
            this.toolTip1 = new Gizmox.WebGUI.Forms.ToolTip();
            this.panel1 = new Gizmox.WebGUI.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // B_Cancel
            // 
            this.B_Cancel.Location = new System.Drawing.Point(168, 0);
            this.B_Cancel.Name = "B_Cancel";
            this.B_Cancel.Size = new System.Drawing.Size(75, 23);
            this.B_Cancel.TabIndex = 0;
            this.B_Cancel.Text = "Annuler";
            // 
            // B_Save
            // 
            this.B_Save.Location = new System.Drawing.Point(84, 0);
            this.B_Save.Name = "B_Save";
            this.B_Save.Size = new System.Drawing.Size(75, 23);
            this.B_Save.TabIndex = 0;
            this.B_Save.Text = "Enregistrer";
            this.B_Save.GotFocus += new System.EventHandler(this.B_Save_GotFocus);
            // 
            // Tools
            // 
            this.Tools.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.Tools.Location = new System.Drawing.Point(0, 0);
            this.Tools.Name = "Tools";
            this.Tools.Size = new System.Drawing.Size(742, 25);
            this.Tools.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.B_Cancel);
            this.panel1.Controls.Add(this.B_Save);
            this.panel1.Dock = Gizmox.WebGUI.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(497, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(245, 25);
            this.panel1.TabIndex = 2;
            // 
            // Inline_Action_Bar
            // 
            this.BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.FixedSingle;
            this.BorderWidth = new Gizmox.WebGUI.Forms.BorderWidth(0, 0, 0, 1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Tools);
            this.Size = new System.Drawing.Size(742, 25);
            this.Text = "Nav_Bar";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private FlowLayoutPanel Tools;
        private ToolTip toolTip1;
        private Button B_Cancel;
        private Panel panel1;
        public Button B_Save;


    }
}