using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common;

namespace JR.WUI.WAF
{
    partial class Dashboard_Summary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visual WebGui UserControl Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.P_Head = new Gizmox.WebGUI.Forms.Panel();
            this.B_Count = new Gizmox.WebGUI.Forms.Button();
            this.C_Title = new Gizmox.WebGUI.Forms.Label();
            this.C_Count = new Gizmox.WebGUI.Forms.Label();
            this.P_Lines = new Gizmox.WebGUI.Forms.Panel();
            this.T_Bar = new Gizmox.WebGUI.Forms.TableLayoutPanel();
            this.Tip = new Gizmox.WebGUI.Forms.ToolTip();
            this.C_Scale = new Gizmox.WebGUI.Forms.Label();
            this.P_Bar = new Gizmox.WebGUI.Forms.Panel();
            this.panel2 = new Gizmox.WebGUI.Forms.Panel();
            this.tableLayoutPanel1 = new Gizmox.WebGUI.Forms.TableLayoutPanel();
            this.P_Scale = new Gizmox.WebGUI.Forms.Panel();
            this.P_Head.SuspendLayout();
            this.P_Bar.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // P_Head
            // 
            this.P_Head.BorderColor = new Gizmox.WebGUI.Forms.BorderColor(System.Drawing.Color.Silver);
            this.P_Head.BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.FixedSingle;
            this.P_Head.BorderWidth = new Gizmox.WebGUI.Forms.BorderWidth(0, 0, 0, 1);
            this.P_Head.Controls.Add(this.B_Count);
            this.P_Head.Controls.Add(this.C_Title);
            this.P_Head.Controls.Add(this.C_Count);
            this.P_Head.Dock = Gizmox.WebGUI.Forms.DockStyle.Top;
            this.P_Head.DockPadding.All = 2;
            this.P_Head.Location = new System.Drawing.Point(0, 0);
            this.P_Head.Name = "P_Head";
            this.P_Head.Padding = new Gizmox.WebGUI.Forms.Padding(2);
            this.P_Head.Size = new System.Drawing.Size(345, 56);
            this.P_Head.TabIndex = 0;
            this.P_Head.Click += new System.EventHandler(this.P_Head_Click);
            // 
            // B_Count
            // 
            this.B_Count.CustomStyle = "F";
            this.B_Count.FlatStyle = Gizmox.WebGUI.Forms.FlatStyle.Flat;
            this.B_Count.Font = new System.Drawing.Font("Verdana", 20F);
            this.B_Count.ForeColor = System.Drawing.Color.White;
            this.B_Count.Location = new System.Drawing.Point(2, 2);
            this.B_Count.Name = "B_Count";
            this.B_Count.Size = new System.Drawing.Size(113, 50);
            this.B_Count.TabIndex = 2;
            this.B_Count.Text = "button1";
            this.B_Count.Click += new System.EventHandler(this.P_Head_Click);
            // 
            // C_Title
            // 
            this.C_Title.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.C_Title.Font = new System.Drawing.Font("Verdana", 18F);
            this.C_Title.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.C_Title.Location = new System.Drawing.Point(115, 2);
            this.C_Title.Name = "C_Title";
            this.C_Title.Size = new System.Drawing.Size(226, 50);
            this.C_Title.TabIndex = 1;
            this.C_Title.Text = "Titre";
            this.C_Title.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.C_Title.Click += new System.EventHandler(this.P_Head_Click);
            // 
            // C_Count
            // 
            this.C_Count.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.C_Count.Dock = Gizmox.WebGUI.Forms.DockStyle.Left;
            this.C_Count.Font = new System.Drawing.Font("Verdana", 20F);
            this.C_Count.ForeColor = System.Drawing.Color.White;
            this.C_Count.Location = new System.Drawing.Point(2, 2);
            this.C_Count.Name = "C_Count";
            this.C_Count.Size = new System.Drawing.Size(113, 50);
            this.C_Count.TabIndex = 0;
            this.C_Count.Text = "0000";
            this.C_Count.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.C_Count.Click += new System.EventHandler(this.P_Head_Click);
            // 
            // P_Lines
            // 
            this.P_Lines.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.P_Lines.Location = new System.Drawing.Point(0, 56);
            this.P_Lines.Name = "P_Lines";
            this.P_Lines.Size = new System.Drawing.Size(345, 336);
            this.P_Lines.TabIndex = 1;
            // 
            // T_Bar
            // 
            this.T_Bar.ColumnCount = 1;
            this.T_Bar.ColumnStyles.Add(new Gizmox.WebGUI.Forms.ColumnStyle(Gizmox.WebGUI.Forms.SizeType.Absolute, 20F));
            this.T_Bar.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.T_Bar.ForeColor = System.Drawing.Color.White;
            this.T_Bar.GrowStyle = Gizmox.WebGUI.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.T_Bar.Location = new System.Drawing.Point(0, 0);
            this.T_Bar.Margin = new Gizmox.WebGUI.Forms.Padding(2);
            this.T_Bar.Name = "T_Bar";
            this.T_Bar.Padding = new Gizmox.WebGUI.Forms.Padding(2);
            this.T_Bar.RowCount = 3;
            this.T_Bar.RowStyles.Add(new Gizmox.WebGUI.Forms.RowStyle(Gizmox.WebGUI.Forms.SizeType.Absolute, 20F));
            this.T_Bar.RowStyles.Add(new Gizmox.WebGUI.Forms.RowStyle(Gizmox.WebGUI.Forms.SizeType.Percent, 100F));
            this.T_Bar.RowStyles.Add(new Gizmox.WebGUI.Forms.RowStyle(Gizmox.WebGUI.Forms.SizeType.Absolute, 20F));
            this.T_Bar.Size = new System.Drawing.Size(305, 336);
            this.T_Bar.TabIndex = 3;
            // 
            // C_Scale
            // 
            this.C_Scale.AutoSize = true;
            this.C_Scale.Location = new System.Drawing.Point(9, 15);
            this.C_Scale.Name = "C_Scale";
            this.C_Scale.Size = new System.Drawing.Size(35, 13);
            this.C_Scale.TabIndex = 0;
            this.C_Scale.Text = "100";
            // 
            // P_Bar
            // 
            this.P_Bar.Controls.Add(this.T_Bar);
            this.P_Bar.Controls.Add(this.panel2);
            this.P_Bar.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.P_Bar.Location = new System.Drawing.Point(0, 56);
            this.P_Bar.Name = "P_Bar";
            this.P_Bar.Size = new System.Drawing.Size(345, 336);
            this.P_Bar.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tableLayoutPanel1);
            this.panel2.Controls.Add(this.C_Scale);
            this.panel2.Dock = Gizmox.WebGUI.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(305, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(40, 336);
            this.panel2.TabIndex = 4;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new Gizmox.WebGUI.Forms.ColumnStyle(Gizmox.WebGUI.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.Controls.Add(this.P_Scale, 0, 1);
            this.tableLayoutPanel1.Dock = Gizmox.WebGUI.Forms.DockStyle.Left;
            this.tableLayoutPanel1.ForeColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.GrowStyle = Gizmox.WebGUI.Forms.TableLayoutPanelGrowStyle.AddColumns;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new Gizmox.WebGUI.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new Gizmox.WebGUI.Forms.Padding(2);
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new Gizmox.WebGUI.Forms.RowStyle(Gizmox.WebGUI.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new Gizmox.WebGUI.Forms.RowStyle(Gizmox.WebGUI.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new Gizmox.WebGUI.Forms.RowStyle(Gizmox.WebGUI.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(6, 336);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // P_Scale
            // 
            this.P_Scale.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)(((Gizmox.WebGUI.Forms.AnchorStyles.Top | Gizmox.WebGUI.Forms.AnchorStyles.Bottom) 
            | Gizmox.WebGUI.Forms.AnchorStyles.Left)));
            this.P_Scale.BorderColor = new Gizmox.WebGUI.Forms.BorderColor(System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64))))));
            this.P_Scale.BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.FixedSingle;
            this.P_Scale.BorderWidth = new Gizmox.WebGUI.Forms.BorderWidth(1, 1, 0, 1);
            this.P_Scale.Location = new System.Drawing.Point(2, 22);
            this.P_Scale.Name = "P_Scale";
            this.P_Scale.Size = new System.Drawing.Size(10, 292);
            this.P_Scale.TabIndex = 1;
            // 
            // Dashboard_Summary
            // 
            this.Controls.Add(this.P_Bar);
            this.Controls.Add(this.P_Lines);
            this.Controls.Add(this.P_Head);
            this.Margin = new Gizmox.WebGUI.Forms.Padding(10);
            this.Size = new System.Drawing.Size(345, 392);
            this.Text = "Dashbord_Summary";
            this.Load += new System.EventHandler(this.Dashboard_Summary_Load);
            this.P_Head.ResumeLayout(false);
            this.P_Bar.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel P_Head;
        private Label C_Title;
        private Label C_Count;
        private Panel P_Lines;
        private TableLayoutPanel T_Bar;
        private ToolTip Tip;
        private Label C_Scale;
        private Panel P_Bar;
        private Panel panel2;
        private Panel P_Scale;
        private TableLayoutPanel tableLayoutPanel1;
        private Button B_Count;


    }
}