#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Resources;
using JR;
using System.Web;

#endregion

namespace JR.WUI.WAF
{
    public partial class HTML_TextBox : UserControl
    {
        public HTML_TextBox()
        {
            InitializeComponent();
            Icon = Title = "";
        }

        private void B_Edit_Click(object sender, EventArgs e)
        {
            Html_Editor.Edit((T) => { Text = T; }, Text);
        }

        string To_Html(string Value)
        {
            string[] Lines = Value.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            if (Lines.Length < 2) return HttpUtility.HtmlEncode(Value);
            StringBuilder SB = new StringBuilder();
            foreach (string P in Lines)
            {
                SB.Append(HttpUtility.HtmlEncode(P));
                SB.Append("<br/>");
            }
            return SB.ToString();
        }

        string To_Text(string Value)
        {
            // Virer tous les tags
            var SB = new StringBuilder();
            var C = Value.Scan();
            while (!C.End)
            {
                string Before = C.Next("<");
                string Tag = C.Next(">").ToLower();
                if (Before != "")
                {
                    SB.Append(HttpUtility.HtmlDecode(Before));
                }
                if (Tag.StartsWith("br"))
                {
                    SB.AppendLine();
                }
            }
            return SB.ToString();
        }

        private void R_Text_CheckedChanged(object sender, EventArgs e)
        {
            bool Is_Html = R_Html.Checked;
            if (Is_Html)
            {
                Text = To_Html(Text);
            }
            else
            {
                Text = To_Text(Text);
            }
            Show_Text(R_Html.Checked);
        }

        public string Icon {get;set;}
        public string Title {get;set;}

        private void HTML_TextBox_Load(object sender, EventArgs e)
        {
            C_Html.Visible = C_Text.Visible = false;
            C_Text.Dock = C_Html.Dock = DockStyle.Fill;
            if (Icon == "")
            {
               I_Header.Visible = false;
            }         else
            {
                I_Header.Visible = true;
                I_Header.BackgroundImage = new IconResourceHandle(Icon);
            }
            if (Title == "")
            {
               C_Title.Visible = false;
            }         else
            {
                C_Title.Text = Title;
            }

        }

        void Show_Text(bool Is_Html)
        {
            B_Edit.Visible = C_Html.Visible = R_Html.Checked = Is_Html;
            C_Text.Visible = R_Text.Checked = !Is_Html;
            if (Is_Html)
            {
                C_Html.Html = this.Text;
            }
            else
            {
                C_Text.Text = this.Text;
            }
        }

        private void C_Text_TextChanged(object sender, EventArgs e)
        {
            this.Text = C_Text.Text;
        }

        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                if (value == base.Text) return;
                bool Is_Html = value.IndexOf('<') >= 0 && (value.IndexOf('<') < value.IndexOf('>'));
                base.Text = value;
                R_Text.CheckedChanged -= R_Text_CheckedChanged;
                C_Text.TextChanged -= C_Text_TextChanged;
                Show_Text(Is_Html);
                R_Text.CheckedChanged += R_Text_CheckedChanged;
                C_Text.TextChanged += C_Text_TextChanged;
            }
        }
    }
}