#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Common.Resources;
using Gizmox.WebGUI.Forms;

#endregion

namespace JR.WUI.WAF
{
    public partial class Nav_Bar : UserControl
    {
        public Nav_Bar()
        {
            InitializeComponent();
        }

        public void Warning(string Message)
        {
            C_Message.Text = Message;
            C_Message.ForeColor = Color.Red;
        }

        public void Message(string Message)
        {
            C_Message.Text = Message;
            C_Message.ForeColor = Color.DarkBlue;
        }

        public void Clear_Message()
        {
            C_Message.Text = "";
        }

        public Icon_Button B_Last;
        public Icon_Button B_Next;
        public Icon_Button B_Prev;
        public Icon_Button B_First;
        public Icon_Button B_Db;

        private void Nav_Bar_Load(object sender, EventArgs e)
        {
            B_Db = new Icon_Button { Icon = "fa-database" };
            B_First = new Icon_Button { Icon = "fa-step-backward" };
            B_Last = new Icon_Button { Icon = "fa-step-forward" };
            B_Prev = new Icon_Button { Icon = "fa-arrow-circle-left" };
            B_Next = new Icon_Button { Icon = "fa-arrow-circle-right" };
            Tools.Append(B_Db);
            Tools.Append(B_First, false);
            Tools.Append(B_Prev, false);
            Tools.Append(B_Next, false);
            Tools.Append(B_Last, false);
            toolTip1.SetToolTip(B_Last, "Dernier �l�ment");
            toolTip1.SetToolTip(B_Next, "Suivant");
            toolTip1.SetToolTip(B_Prev, "Pr�c�dent");
            toolTip1.SetToolTip(B_First, "Premier �l�ment");
            toolTip1.SetToolTip(B_Db, "aper�u base de donn�e");

            Clear_Message();
        }

        public void Enable(bool State, params Control[] Buttons)
        {
            Action<Control> Enable = (C) =>
            {
                Icon_Button I = C as Icon_Button;
                if (I != null)
                {
                    I.Enabled = State;
                    return;
                }
                Button B = C as Button;
                if (B == null) return;
                B.Enabled = State;
                if (B.Image == null) return;
                string File = B.Image.File;
                File = File.Replace("-Disable.png", ".png");
                if (!State) File = File.Replace(".png", "-Disable.png");
                if (B.Image.File == File) return;
                B.Image = new IconResourceHandle(File);
            };
            foreach (Control C in Buttons)
            {
                Enable(C);
            }
        }

    }
}