using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common;

namespace JR.WUI.WAF.Edit
{
    partial class Item_Editor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visual WebGui Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Tabs = new Gizmox.WebGUI.Forms.TabControl();
            this.Nav = new JR.WUI.WAF.Nav_Bar();
            this.P_Main = new Gizmox.WebGUI.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.Tabs)).BeginInit();
            this.P_Main.SuspendLayout();
            this.SuspendLayout();
            // 
            // Tabs
            // 
            this.Tabs.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.Tabs.Location = new System.Drawing.Point(0, 0);
            this.Tabs.Name = "Tabs";
            this.Tabs.Size = new System.Drawing.Size(757, 467);
            this.Tabs.TabIndex = 0;
            this.Tabs.SelectedIndexChanged += new System.EventHandler(this.Tabs_SelectedIndexChanged);
            this.Tabs.SelectedIndexChanging += new Gizmox.WebGUI.Forms.TabControlCancelEventHandler(this.Tabs_SelectedIndexChanging);
            // 
            // Nav
            // 
            this.Nav.AutoValidate = Gizmox.WebGUI.Forms.AutoValidate.EnablePreventFocusChange;
            this.Nav.BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.FixedSingle;
            this.Nav.BorderWidth = new Gizmox.WebGUI.Forms.BorderWidth(0, 0, 0, 1);
            this.Nav.Dock = Gizmox.WebGUI.Forms.DockStyle.Top;
            this.Nav.Location = new System.Drawing.Point(0, 0);
            this.Nav.Name = "Nav";
            this.Nav.Size = new System.Drawing.Size(757, 25);
            this.Nav.TabIndex = 1;
            this.Nav.Text = "Nav_Bar";
            // 
            // P_Main
            // 
            this.P_Main.Controls.Add(this.Tabs);
            this.P_Main.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.P_Main.Location = new System.Drawing.Point(0, 25);
            this.P_Main.Name = "P_Main";
            this.P_Main.Size = new System.Drawing.Size(757, 467);
            this.P_Main.TabIndex = 2;
            // 
            // Item_Editor
            // 
            this.Controls.Add(this.P_Main);
            this.Controls.Add(this.Nav);
            this.FormBorderStyle = Gizmox.WebGUI.Forms.FormBorderStyle.SizableToolWindow;
            this.Size = new System.Drawing.Size(757, 492);
            this.Text = "Item_Editor";
            this.Load += new System.EventHandler(this.D_Load);
            this.FormClosing += new Gizmox.WebGUI.Forms.Form.FormClosingEventHandler(this.Item_Editor_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.Tabs)).EndInit();
            this.P_Main.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl Tabs;
        private Nav_Bar Nav;
        private Panel P_Main;


    }
}