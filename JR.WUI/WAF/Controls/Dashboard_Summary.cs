#region Using

using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace JR.WUI.WAF
{
    public partial class Dashboard_Summary : UserControl
    {
        public Dashboard_Summary()
        {
            InitializeComponent();
        }

        public int Max_Bars = 14;
        void Check_Graph()
        {
            if (T_Bar.Controls.Count > 10) return;
            P_Scale.BorderColor = Color.DarkGray;
            T_Bar.ColumnCount = 0;
            T_Bar.ColumnStyles.Clear();
            for (int Col = 0; Col < Max_Bars; Col++)
            {
                T_Bar.ColumnStyles.Add(new Gizmox.WebGUI.Forms.ColumnStyle(Gizmox.WebGUI.Forms.SizeType.Percent, (float)(100.0/(double)Max_Bars)));
            }
            T_Bar.ColumnCount = Max_Bars;
            for (int Col = 0; Col < Max_Bars; Col++)
            {
                Panel PH = new Panel();
                PH.Dock = DockStyle.Fill;
                T_Bar.Controls.Add(PH);
                Panel P = new Panel();
                P.Dock = DockStyle.Fill;
                P.Padding = new Gizmox.WebGUI.Forms.Padding(2, 0, 2, 0);
                P.Margin = new Gizmox.WebGUI.Forms.Padding(2, 0, 2, 0);
                Panel PB = new Panel();
                PB.BackColor = Color.DarkGray;
                PB.Dock = DockStyle.Bottom;
                PB.Height = 0;
                P.Controls.Add(PB);
                T_Bar.Controls.Add(P);

                Label L = new Label("");
                L.Dock = DockStyle.Fill;
                L.TextAlign = ContentAlignment.TopCenter;
                T_Bar.Controls.Add(L);
                T_Bar.SetCellPosition(PH, new TableLayoutPanelCellPosition(Col, 0));
                T_Bar.SetCellPosition(P, new TableLayoutPanelCellPosition(Col, 1));
                T_Bar.SetCellPosition(L, new TableLayoutPanelCellPosition(Col, 2));
            }
        }

        Action Detail;
        public void Prepare(string Title, int Count = 0, Action On_Detail = null)
        {
            C_Title.Text = Title;
            P_Lines.Controls.Clear ();
            Detail = On_Detail;
            Set_Count(Count);
        }

        void Set_Count(int Count)
        {
            B_Count.Text = C_Count.Text = Count.ToString();
            if (Detail == null)
            {
                B_Count.Visible = false;
            }
            else
            {
                C_Count.Text = "";
            }
        }

        // Style :
        // 0 = Normal, 1 = A faire, 2 = Urgent
        public void Add_Line(string Title, int Count, int Priority = 0, Action On_Detail = null)
        {
            P_Lines.Visible = true;
            Dashboard_Line DL = new Dashboard_Line();
            DL.Set_Text (Title, Count);
            if (On_Detail != null)
            {
                DL.On_Click = new EventHandler((sender, e) => { On_Detail(); });
            }
            else if (Detail != null)
            {
                DL.On_Click = new EventHandler((sender, e) => { Detail(); });
            }

            if (Count > 0)
            {
                switch (Priority)
                {
                    case 1:
                        DL.Set_Colors(Color.White, Color.Orange);
                        break;
                    case 2:
                        DL.Set_Colors(Color.White, Color.Red);
                        break;
                }
            }
            P_Lines.Controls.Insert(0, DL);
        }

        private void P_Head_Click(object sender, EventArgs e)
        {
            if (Detail == null) return;
            Detail();
        }


        //class Bar
        //{
        //    public DateTime Date;
        //    public int Value;
        //}
        //List<Bar> Bars = new List<Bar>();
        //void Add_Value(DateTime Date, int Value)
        //{
        //    Bars.Add(new Bar { Date = Date, Value = Value });
        //}

        public void Display_List()
        {
            P_Lines.Visible = true;
            P_Bar.Visible = false;
        }

        public void Display_Graph(DataTable Source, DateTime Start)
        {
            DateTime Date = Start;
            P_Lines.Visible = false;
            P_Bar.Visible = true;
            Check_Graph();
            int Max = 0;
            int Height = T_Bar.Height - 40;
            int Scale = 1;
            if (Source.Rows.Count > 0)
            {
                Set_Count ((from DataRow R in Source.Rows select R[0].To_Integer()).Sum());
                Max = (from DataRow R in Source.Rows select R[0].To_Integer()).Max();
                while (Max > 0)
                {
                    if (Max < 2)
                    {
                        Scale = Scale * 2;
                    }
                    else if (Max < 5)
                    {
                        Scale = Scale * 5;
                    }
                    else
                    {
                        Scale = Scale * 10;
                    }
                    Max = Max / 10;
                }
            }
            else
            {
                Set_Count (0);
            }
            C_Scale.Text = Scale.ToString();

            Func<int, int> Bar_Size = (Value) =>
            {
                if (Scale == 1) return 0;
                return (Value * Height) / Scale;
            };

            Func<int> Date_Count = () =>
            {
                string DC = Date.ToString("yyyyMMdd");
                foreach (DataRow DR in Source.Rows)
                {
                    if (DR[1].ToString() == DC) return DR[0].To_Integer();
                }
                return 0;
            };
            for (int Col = 0; Col < Max_Bars; Col++)
            {
                int Count = Date_Count();
                Panel P = T_Bar.GetControlFromPosition(Col, 1) as Panel;
                Label L = T_Bar.GetControlFromPosition(Col, 2) as Label;
                Panel PB = P.Controls[0] as Panel;
                PB.BackColor = Color.Gainsboro;
                PB.Height = Math.Max(1, Bar_Size(Count));
                L.Text = string.Format("{0:ddd}", Date).Substring(0, 1).ToUpper();
                if (L.Text == "L")
                {
                    PB.BackColor = L.ForeColor = Color.DeepSkyBlue;
                }
                Tip.SetToolTip(P, string.Format("{0:ddd dd MMM yyyy} - [{1}]", Date, Count));
                Tip.SetToolTip(PB, string.Format("{0:ddd dd MMM yyyy} - [{1}]", Date, Count));
                Date = Date.AddDays(1.0);
            }

        }

        public class Point
        {
            public DateTime D;
            public int Y;
        }

        public enum Step {Year, Month, Week, Day, Hour};

        // Step = Y,M,W,D,H
        public void Display_Points(Step Step, IList<Point> Points)
        {
            DateTime Start = DateTime.Now;
            string Comp_Format = "";
            switch (Step)
            {
                case Step.Year:
                    Comp_Format = "yyyy";
                    Start = new DateTime(Start.Year, 1, 1).AddYears(-Max_Bars+1);
                    break;
                case Step.Month:
                    Comp_Format = "yyyyMM";
                    Start = new DateTime(Start.Year, Start.Month, 1).AddMonths(-Max_Bars+1);
                    break;
                case Step.Day:
                    Comp_Format = "yyyyMMdd";
                    Start = Start.Date.AddDays(-Max_Bars+1);
                    break;
                    // Les autres plus tard
                default:
                    Comp_Format = "yyyyMMddHH";
                    Start = Start.Date.AddHours(-Max_Bars+1);
                    break;
            }
            P_Lines.Visible = false;
            P_Bar.Visible = true;
            Check_Graph();
            int Max = 0;
            int Height = T_Bar.Height - 40;
            int Scale = 1;
            if (Points.Count > 0)
            {
                Set_Count((from Point R in Points select R.Y).Sum());
                Max = (from Point R in Points select R.Y).Max();
                while (Max > 0)
                {
                    if (Max < 2)
                    {
                        Scale = Scale * 2;
                    }
                    else if (Max < 3)
                    {
                        Scale = Scale * 3;
                    }
                    else if (Max < 5)
                    {
                        Scale = Scale * 5;
                    }
                    else
                    {
                        Scale = Scale * 10;
                    }
                    Max = Max / 10;
                }
            }
            else
            {
                Set_Count (0);
            }
            C_Scale.Text = Scale.ToString();

            Func<int, int> Bar_Size = (Value) =>
            {
                if (Scale == 1) return 0;
                return (Value * Height) / Scale;
            };

            DateTime Date = Start;
            Func<int> Date_Count = () =>
            {
                string DC = Date.ToString(Comp_Format);
                foreach (Point P in Points)
                {
                    if (P.D.ToString(Comp_Format) == DC) return P.Y;
                }
                return 0;
            };

            for (int Col = 0; Col < Max_Bars; Col++)
            {
                int Count = Date_Count();
                if (Col == Max_Bars - 1) Set_Count (Count);

                Panel P = T_Bar.GetControlFromPosition(Col, 1) as Panel;
                Label L = T_Bar.GetControlFromPosition(Col, 2) as Label;
                Panel PB = P.Controls[0] as Panel;
                PB.BackColor = Color.Gainsboro;
                PB.Height = Math.Max(1, Bar_Size(Count));
                bool First = false;
                string Tool_Tip = "";
                switch (Step)
                {
                    case Step.Year:
                        L.Text = string.Format("{0:yy}", Date);
                        Tool_Tip = string.Format("{0:yyyy} - [{1}]", Date, Count);
                        break;
                    case Step.Month:
                        L.Text = string.Format("{0:MMM}", Date).Substring(0, 1).ToUpper();
                        if (Date.Month == 1) First = true;
                        Tool_Tip = string.Format("{0:MMM yyyy} - [{1}]", Date, Count);
                        break;
                    case Step.Day:
                        L.Text = string.Format("{0:ddd}", Date).Substring(0, 1).ToUpper();
                        if (L.Text == "L") First = true;
                        Tool_Tip = string.Format("{0:ddd dd MMM yyyy} - [{1}]", Date, Count);
                        break;
                    // Les autres plus tard
                    default:
                        Comp_Format = "yyyyMMddHH";
                        Start = Start.Date.AddHours(-Max_Bars + 1);
                        break;
                }
                if (First) PB.BackColor = L.ForeColor = Color.DeepSkyBlue;
                Tip.SetToolTip(P, Tool_Tip);
                Tip.SetToolTip(PB, Tool_Tip);

                switch (Step)
                {
                    case Step.Year:
                        Date = Date.AddYears(1);
                        break;
                    case Step.Month:
                        Date = Date.AddMonths(1);
                        break;
                    case Step.Day:
                        Date = Date.AddDays(1);
                        break;
                    // Les autres plus tard
                    default:
                        Date = Date.AddDays(1);
                        break;
                }
            }

        }

        private void Dashboard_Summary_Load(object sender, EventArgs e)
        {
            B_Count.Location = C_Count.Location;
            B_Count.Size = C_Count.Size;
        }

    }
}