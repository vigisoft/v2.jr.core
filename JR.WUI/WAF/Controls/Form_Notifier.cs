#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;
using System.Web.SessionState;
using System.Threading;
using System.Collections.Concurrent;

#endregion

namespace JR.WUI
{
    public partial class Form_Notifier : UserControl
    {
        public Form_Notifier()
        {
            InitializeComponent();
        }

        // Chargement, choisir un UID et le transmettre au script
        // Ajouter � la liste statique
        private void Form_Notifier_Load(object sender, EventArgs e)
        {
            Dock = DockStyle.Bottom;
            Height = 0;
            BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.None;
            ClientId = Guid.NewGuid().ToString("N");
            VWGClientContext.Current.Invoke("CN_Init", ClientId);
            Dic[ClientId] = this;
        }

        static JR.Saf.Ref_Parameter Wait_Max = new Saf.Ref_Parameter("FORM_NOTIFIER.TIME_OUT", "5");

        public TimeSpan Max_Wait = TimeSpan.FromSeconds(Wait_Max.Value.To_Integer());

        // reception d'une notif depuis le script
        protected override void FireEvent(Gizmox.WebGUI.Common.Interfaces.IEvent objEvent)
        {
            if (objEvent.Type == "Notify")
            {
                lock (this)
                {
                    Client_Message Message = null;
                    while (Message_Queue.TryDequeue(out Message))
                    {
                        if (Message.Client == null) break;
                        if (Message.Message as string == "!Break") continue;  // break ?
                        Message.Client.Notify(Message.Message);
                    }
                    // Rearmer les notifs
                    Check_CN();
                }
            }
            else
            {
                base.FireEvent(objEvent);
            }
        }

        // Suite � probl�me de deadlock, tous les appels � Locked_... doivent �tre pr�alablement lock�s

        // For�age d'une notif depuis le dialogue principal, au cas ou une notif aurait saut�
        // FN doit �tre lock� par l'appelant
        public void Locked_Check_Events()
        {
            Client_Message Message = null;
            while (Message_Queue.TryDequeue(out Message))
            {
                if (Message.Client == null) break;
                Message.Client.Notify(Message.Message);
            }
            // Rearmer les notifs
            Check_CN();
        }


        // Armement / desarmement des notifs
        public void Check_CN()
        {
            VWGClientContext.Current.Invoke(CN_Dic.Count > 0 ? "CN_Start" : "CN_Stop");
        }

        public static Dictionary<string, Form_Notifier> Dic = new Dictionary<string, Form_Notifier>();
        public List<Client_Notifier> CN_Dic = new List<Client_Notifier>();

        class Client_Message
        {
            public Client_Notifier Client;
            public object Message;
        }
        class Break_Message : Client_Message
        {
        }
        BlockingCollection<Client_Message> Notify_Queue = new BlockingCollection<Client_Message>();
        ConcurrentQueue<Client_Message> Message_Queue = new ConcurrentQueue<Client_Message>();

        void Push_Client_Message(Client_Message M)
        {
            Message_Queue.Enqueue(M);
            Notify_Queue.Add(M);
        }

        public void Locked_Push_Message(Client_Notifier Client, object Message)
        {
            T("PM0:{0}", ClientId);
            Push_Client_Message(new Client_Message { Client = Client, Message = Message });
        }

        public void Locked_Push_Close_Message(Client_Notifier Client)
        {
            T("PCM0:{0}", ClientId);
            Push_Client_Message(new Client_Message { Client = Client, Message = "!Close" });
        }

        public void Locked_Push_Break_Message(Client_Notifier Client)
        {
            T("PBM0:{0}", ClientId);
            Push_Client_Message(new Break_Message { Client = Client, Message = "!Break" });
        }

        public static Form_Notifier Init()
        {
            var Top = VWGContext.Current.MainForm;
            var FN = new Form_Notifier();
            Top.Controls.Insert(0, FN);
            (Top as Form).FormClosed += FN.Form_Notifier_FormClosed;
            return FN;
        }
        public static Form_Notifier Current
        {
            get
            {
                Form_Notifier FN = null;
                // Localiser le form notifier, le cr�er le cas ech�ant
                var Top = VWGContext.Current.MainForm;
                foreach (var C in Top.Controls)
                {
                    FN = C as Form_Notifier;
                    if (FN != null) break;
                }
                if (FN == null) FN = Init();
                return FN;
            }
        }

        void Form_Notifier_FormClosed(object sender, FormClosedEventArgs e)
        {
            lock (this)
            {
                Dic.Remove(ClientId);
            }
        }

        public static string Wait_Messages(string Id)
        {
            Form_Notifier FN;
            if (!Dic.TryGetValue(Id, out FN)) return "!Not registered";
            if (FN.ParentForm.IsDisposed) return "!Disposed";
            Client_Message Notification = null;
            T("WM0:{0}", Id);
            if (FN.Notify_Queue.TryTake(out Notification, FN.Max_Wait))
            {
                if (Notification.Client == null) return "0";
                lock (FN)
                {
                    int Count = 1;
                    // Tout depiler
                    while ((!(Notification is Break_Message)) && FN.Notify_Queue.TryTake(out Notification))
                    {
                        if (Notification.Client == null) return Count.ToString();
                        Count++;
                    }
                    T("WM1:{0} Count={1}", Id, Count);
                    return Count.ToString();
                }
            }
            return "!Timeout";
        }


        internal void Locked_Add_Client(Client_Notifier CN)
        {
            CN_Dic.Add(CN);
            Check_CN();
        }

        internal void Locked_Remove_Client(Client_Notifier CN)
        {
            CN_Dic.Remove(CN);
            Check_CN();
        }

        static void T(string Message, params object[] Params)
        {
            System.Diagnostics.Debug.WriteLine(string.Format(Message, Params));
        }
    }
}