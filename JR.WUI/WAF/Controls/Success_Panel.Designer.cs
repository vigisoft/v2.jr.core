using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common;

namespace JR.WUI
{
    partial class Success_Panel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visual WebGui UserControl Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.B_Hide = new Gizmox.WebGUI.Forms.Button();
            this.L_Success = new Gizmox.WebGUI.Forms.Label();
            this.SuspendLayout();
            // 
            // B_Hide
            // 
            this.B_Hide.Dock = Gizmox.WebGUI.Forms.DockStyle.Right;
            this.B_Hide.Location = new System.Drawing.Point(525, 5);
            this.B_Hide.Name = "B_Hide";
            this.B_Hide.Size = new System.Drawing.Size(53, 30);
            this.B_Hide.TabIndex = 1;
            this.B_Hide.Text = "Ok";
            this.B_Hide.Click += new System.EventHandler(this.B_Hide_Click);
            // 
            // L_Success
            // 
            this.L_Success.BackColor = System.Drawing.Color.Transparent;
            this.L_Success.BorderColor = new Gizmox.WebGUI.Forms.BorderColor(System.Drawing.Color.DarkSeaGreen);
            this.L_Success.BorderWidth = new Gizmox.WebGUI.Forms.BorderWidth(0);
            this.L_Success.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.L_Success.ForeColor = System.Drawing.Color.DarkGreen;
            this.L_Success.Location = new System.Drawing.Point(5, 5);
            this.L_Success.Margin = new Gizmox.WebGUI.Forms.Padding(4);
            this.L_Success.Name = "L_Success";
            this.L_Success.Size = new System.Drawing.Size(37, 15);
            this.L_Success.TabIndex = 0;
            this.L_Success.Text = "label1";
            // 
            // Success_Panel
            // 
            this.BackColor = System.Drawing.Color.Honeydew;
            this.BorderColor = new Gizmox.WebGUI.Forms.BorderColor(System.Drawing.Color.Red);
            this.BorderWidth = new Gizmox.WebGUI.Forms.BorderWidth(0, 1, 0, 1);
            this.Controls.Add(this.L_Success);
            this.Controls.Add(this.B_Hide);
            this.DockPadding.All = 5;
            this.Padding = new Gizmox.WebGUI.Forms.Padding(5);
            this.Size = new System.Drawing.Size(583, 40);
            this.Text = "Error_Panel";
            this.Load += new System.EventHandler(this.Success_Panel_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Button B_Hide;
        private Label L_Success;



    }
}