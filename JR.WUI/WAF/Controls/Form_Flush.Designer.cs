using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common;

namespace JR.WUI
{
    partial class Form_Flush
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visual WebGui UserControl Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Script_Box = new Gizmox.WebGUI.Forms.HtmlBox();
            this.SuspendLayout();
            // 
            // Script_Box
            // 
            this.Script_Box.ContentType = "text/html";
            this.Script_Box.Html = "<HTML>No content.</HTML>";
            this.Script_Box.IsWindowless = true;
            this.Script_Box.Location = new System.Drawing.Point(18, 0);
            this.Script_Box.Name = "Script_Box";
            this.Script_Box.Size = new System.Drawing.Size(120, 54);
            this.Script_Box.TabIndex = 2;
            // 
            // Form_Flush
            // 
            this.BackColor = System.Drawing.Color.LavenderBlush;
            this.BorderColor = new Gizmox.WebGUI.Forms.BorderColor(System.Drawing.Color.Red);
            this.BorderWidth = new Gizmox.WebGUI.Forms.BorderWidth(0, 1, 0, 1);
            this.Controls.Add(this.Script_Box);
            this.Size = new System.Drawing.Size(249, 69);
            this.Text = "Error_Panel";
            this.Load += new System.EventHandler(this.Form_Flush_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private HtmlBox Script_Box;




    }
}