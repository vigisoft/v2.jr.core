#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;
using JR.DB;
using JR.DBO;

#endregion

namespace JR.WUI.WAF.Edit
{
    public partial class Item_Inline_Editor : UserControl, I_Dialog
    {
        public Item_Inline_Editor()
        {
            InitializeComponent();
        }
        public JRO_Row Item { get; set; }

        public virtual void Prepare()
        {
        }

        public void Display (JRO_Row Dest)
        {
            Item = Dest;
            this.Visible = Item != null;
            Animate_Buttons();
            Prepare();
        }

        public bool Go_To(object Dest)
        {
            JRO_Row Dest_Item = Dest as JRO_Row;
            if (Dest_Item == null) return false;
            New_Item = Dest_Item != null && Dest_Item.Is_New;
            Display(Dest_Item);
            return true;
        }

        public Dialog_Helper Helper { get; set; }

        public void Add_Page (string Name, Control Page, bool Single = false)
        {
            int Min_Width = Page.Width + 20;
            int Min_Height = Page.Height + 50;
            if (Min_Width > Width) Width = Min_Width;
            if (Min_Height > Height) Height = Min_Height;
            Helper.Pages.Add(Page as I_Tab);
            Page.Dock = DockStyle.Fill;
            if (Single)
            {
                Tabs.Visible = false;
                P_Main.Controls.Add(Page);
                this.Text = Name;
            }
            else
            {
                TabPage P = new TabPage(Name);
                P.Controls.Add(Page);
                Tabs.TabPages.Add(P);
            }
        }

        public virtual void Load_Pages()
        { 
        }

        public bool New_Item { get; set; }
        private void D_Load(object sender, EventArgs e)
        {
            Enabled = false;
            Helper = new Dialog_Helper(this);
            New_Item = Item != null && Item.Is_New;
            Load_Pages();
            Display (Item);
            Enabled = true;
            Nav.B_Db.Click += B_Db_Click;
        }

        private void B_Db_Click(object sender, EventArgs e)
        {
            if (Item == null) return;
            string Html = Item.HTML_Debug_Info;
            if (Html == "") return;
            using (Db_Dump D = new Db_Dump { Content = Html })
            {
                D.ShowDialog();
            }
        }

        public I_Indexer Indexer { get; set; }

        public bool Force_Nav = false;
        private void Tabs_SelectedIndexChanging(object sender, TabControlCancelEventArgs e)
        {
            if (Force_Nav) return;
            Action Force_Close = () =>
            {
                Force_Nav = true;
                Tabs.SelectedTab = e.TabPage;
                Force_Nav = false;
            };
            if (Helper.Can_Quit(Force_Close)) return;
            e.Cancel = true;
        }

        int Item_Count
        {
            get
            {
                return Indexer.Item_Count;
            }
        }
        int Item_Index
        {
            get
            {
                return Indexer.Item_Index(Item);
            }
        }
        object Item_At (int Index)
        {
            return Indexer.Item(Index);
        }

        void Animate_Buttons()
        {
        }


        private void Tabs_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (I_Tab P in Helper.Pages)
            {
                if (P.Helper.Control.Parent == Tabs.SelectedTab)
                {
                    P.Redisplay();
                    break;
                }
            }
        }

        public EventHandler On_Valid;
        public void Valid_Form()
        {
            On_Valid(this, EventArgs.Empty);
        }

        //private void Item_Editor_FormClosing(object sender, FormClosingEventArgs e)
        //{
        //    if (Force_Nav) return;
        //    Action Force_Close = () =>
        //        {
        //            Force_Nav = true;
        //            //Close();
        //        };
        //    if (e.CloseReason != CloseReason.UserClosing) return;
        //    if (Helper.Can_Quit(Force_Close)) return;
        //    e.Cancel = true;
        //}

        public void Display_Title()
        {
            if (New_Item)
            {
                Text = Item.Name;
            }
            else
            {
                Text = string.Format("{1}/{2} [{0}]", Item.Name, Indexer.Item_Index(Item) + 1, Indexer.Item_Count);
            }
        }
    }
}