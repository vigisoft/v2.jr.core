#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace JR.WUI.WAF
{
    public partial class Dashboard_Line : UserControl
    {
        public Dashboard_Line()
        {
            InitializeComponent();
        }

        private void Dashboard_Line_Load(object sender, EventArgs e)
        {
            Dock = DockStyle.Top;
            C_Count.Size = B_Count.Size;
            C_Count.Location = B_Count.Location;
            C_Count.Padding = B_Count.Padding;
            if (On_Click == null)
            {
                B_Count.Visible = false;
            } else
            {
              C_Count.Text = "";
              B_Count.Click += On_Click;
            }
        }

        public void Set_Text(string Title, object Value)
        {
            C_Title.Text = Title;
            B_Count.Text = C_Count.Text = Value.ToString();
        }
        public void Set_Colors(Color Text_Color, Color Back_Color)
        {
            B_Count.ForeColor = C_Count.ForeColor = Text_Color;
            C_Count.BackColor = Back_Color;
        }
        public EventHandler On_Click;
    }
}