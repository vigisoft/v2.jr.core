using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common;

namespace JR.WUI.WAF
{
    partial class Nav_Bar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visual WebGui UserControl Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolTip1 = new Gizmox.WebGUI.Forms.ToolTip();
            this.C_Message = new Gizmox.WebGUI.Forms.Label();
            this.Tools = new JR.WUI.WAF.Tool_Bar();
            this.SuspendLayout();
            // 
            // C_Message
            // 
            this.C_Message.Location = new System.Drawing.Point(120, 5);
            this.C_Message.Name = "C_Message";
            this.C_Message.Size = new System.Drawing.Size(35, 13);
            this.C_Message.TabIndex = 2;
            this.C_Message.Text = "label1";
            this.C_Message.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Tools
            // 
            this.Tools.AutoValidate = Gizmox.WebGUI.Forms.AutoValidate.EnablePreventFocusChange;
            this.Tools.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.Tools.DockPadding.Left = 5;
            this.Tools.DockPadding.Right = 5;
            this.Tools.DockPadding.Top = 2;
            this.Tools.Location = new System.Drawing.Point(0, 0);
            this.Tools.Name = "tool_Bar1";
            this.Tools.Padding = new Gizmox.WebGUI.Forms.Padding(5, 2, 5, 0);
            this.Tools.Size = new System.Drawing.Size(742, 25);
            this.Tools.TabIndex = 3;
            this.Tools.Text = "Tool_Bar";
            // 
            // Nav_Bar
            // 
            this.BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.FixedSingle;
            this.BorderWidth = new Gizmox.WebGUI.Forms.BorderWidth(0, 0, 0, 1);
            this.Controls.Add(this.C_Message);
            this.Controls.Add(this.Tools);
            this.Size = new System.Drawing.Size(742, 25);
            this.Text = "Nav_Bar";
            this.Load += new System.EventHandler(this.Nav_Bar_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ToolTip toolTip1;
        private Label C_Message;
        private Tool_Bar Tools;

    }
}