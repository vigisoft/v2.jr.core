#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;
using System.Web.SessionState;
using System.Threading;
using System.Collections.Concurrent;
using System.Security.Policy;

#endregion

namespace JR.WUI
{
    public partial class Server_Notifier : UserControl
    {
        public Server_Notifier()
        {
            InitializeComponent();
        }

        private void Client_Notifier_Load(object sender, EventArgs e)
        {
            Dock = DockStyle.Bottom;
            Height = 0;
            BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.None;
            Register();
        }

        public string Key = "XX";

        const string Gen_Script = @"
    <script>
        function {CBK}(Source, Action, Data)
        {
           var objForm = vwgContext.provider.getComponentByClientId('{ID}');
           var objAtt = {};
           objAtt.Action=Action;
           objAtt.Data=Data;
           objAtt.Source=Source;
           vwgContext.events.performClientEvent(objForm.id(), 'Notify', objAtt, true);
           vwgContext.events.raiseEvents();
        }
    </script>
";

        public Action<string, string, string> On_Notify = (Source, Action, Data) => { };

        protected override void FireEvent(Gizmox.WebGUI.Common.Interfaces.IEvent objEvent)
        {
            if (objEvent.Type == "Notify")
            {
                On_Notify(objEvent["Source"], objEvent["Action"], objEvent["Data"]);
            }
            else
            {
                base.FireEvent(objEvent);
            }
        }

        public string Callback_Name
        {
            get { return "Notif_" + Key; }
        }

        public void Include_Free_Code(string File_Name)
        {
            Script_Box.Html += JR.Files.Safe_Read_All(File_Name);
        }

        public void Add_Styles(string Styles)
        {
            if (Styles == "") return;
            Styles = Styles.Replace("\r\n","").To_Js();
            var Style_Loader = "<script>$(function(){$('head').append('<style>" + Styles + "</style>');});</script>";
            Script_Box.Html += Style_Loader;
        }
        public void Add_Style_Sheet(string URL)
        {
            var S = new Init_Scripter();
            S.Load_Css(URL);
            Script_Box.Html += "<script>" + S.Content + "</script>";
        }

        public void Include_Style_Sheet(string File_Name)
        {
            Add_Styles(JR.Files.Safe_Read_All(File_Name));
        }

        public void Add_Free_Code(string Content)
        {
            Script_Box.Html += Content;
        }

        public void Register()
        {
            ClientId = Guid.NewGuid().ToString("N");
            Script_Box.Html = Gen_Script.Replace("{ID}", ClientId).Replace("{CBK}", Callback_Name);
        }

        public void UnRegister()
        {
            Script_Box.Html = "";
        }

        // Key doit �tre unique dans l'appli
        // Le client script doit appeler [CallBack_Name](Data); pour notifier
        static long Links_Id = 0;
        public static Server_Notifier Link(Control Parent, string Key, Action<string, string, string> On_Notify)
        {
            Links_Id++;
            var S = new Server_Notifier { Key = Key + Links_Id, On_Notify = On_Notify };
            Parent.Controls.Add(S);
            return S;
        }

    }
}