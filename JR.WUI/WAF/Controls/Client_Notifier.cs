#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;
using System.Web.SessionState;
using System.Threading;
using System.Collections.Concurrent;
using System.Threading.Tasks;
//using System.Windows.Forms;

#endregion

namespace JR.WUI
{
    public class Client_Notifier
    {

        public bool Enabled = false;
        public Action<object> On_Notify = (Message) => { };

        // Il est important de toujours locker dans le m�me ordre CN , puis FN pour �viter les deadlocks
        // Donc tout lock de FN doit �tre pr�c�d� d'un lock de CN

        public void Push_Message(object Message)
        {
            lock (this)
            {
                if (FN == null) return;
                lock (FN)
                {
                    FN.Locked_Push_Message(this, Message);
                }
            }
        }
        public class End_Message
        {
        }

        BlockingCollection<object> Response_Queue = new BlockingCollection<object>();

        public void Push_Response(object Response)
        {
            Response_Queue.Add(Response);
        }

        public bool Try_Take<T>(out T Response, int Millis = -1)
        {
            Response = default(T);
            object R;
            if (!Response_Queue.TryTake(out R, Millis)) return false;
            Response = (T)R;
            return true;
        }

        public void Notify(object Message)
        {
            if (!Enabled) return;
            if (Message is End_Message)
            {
                Current_Form.Enabled = true;
                Current_Form.CloseBox = false;
                return;
            }
            On_Notify(Message);
        }

        Form_Notifier FN = null;
        Task Current_Task = null;
        Form Current_Form = null;
        public void Attach(Form Form, Action<object> On_Notify, Action Action)
        {
            lock (this)
            {
                FN = Form_Notifier.Current;
                lock (FN)
                {
                    FN.Locked_Add_Client(this);
                    Form.FormClosed += Form_FormClosed;
                    Form.CloseBox = true;
                    Form.FormClosing += new Form.FormClosingEventHandler((sender, e) =>
                    {
                        if (Form.Enabled) return;
                        FN.Locked_Check_Events();
                        e.Cancel = true;
                        Form.Enabled = false;
                    });
                    this.On_Notify = On_Notify;
                    Form.Enabled = false;
                    Current_Form = Form;
                    Enabled = true;
                    FN.Locked_Push_Break_Message(this);
                }
                if (Action == null) return;
                Current_Task = Task.Run(() =>
                {
                    System.Threading.Thread.Sleep(1); // Petit d�lai pour donner le temps d'afficher le status
                    try
                    {
                        Action();
                    }
                    catch
                    {
                    }
                    // Signaler la fin de tache
                    Push_Message(new End_Message());
                }
                );
            }

        }

        public void Force_Close()
        {
            Current_Form.Enabled = true;
            Current_Form.Close();
        }

        void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            throw new NotImplementedException();
        }

        public void Detach()
        {
            lock (this)
            {
                if (FN == null) return;
                lock (FN)
                {
                    FN.Locked_Remove_Client(this);
                    FN.Locked_Push_Close_Message(this);
                }
                Enabled = false;
                FN = null;
            }
        }

        public void Check()
        {
            lock (this)
            {
                FN = Form_Notifier.Current;
                if (FN == null) return;
                lock (FN)
                {
                    FN.Locked_Check_Events();
                }
            }
        }

        void Form_FormClosed(object sender, FormClosedEventArgs e)
        {
            Detach();
        }

    }
}