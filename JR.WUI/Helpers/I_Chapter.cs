using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Interfaces;

namespace JR.WUI
{
    // Interface d'un bureau
    public interface I_Chapter
    {
        // Vue principale
        Control View { get;}

        // Onglet de gauche
        Control Tab { get;}

        // Outils � ajouter � la barre d'outils
        List<ToolBarButton> Tools { get;}

        // Infos de status � ajouter � la barre de status
        List<StatusBarPanel> Status { get;}

        // Appel� � l'effacement
        void Cleanup();

        // Desktop courant
        I_Desktop Desktop { get;set;}

        // PRemier affichage
        void First_Display();

        // Helper
        Chapter_Helper Helper { get;}

    }
}
