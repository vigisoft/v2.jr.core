using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Interfaces;
using Gizmox.WebGUI.Forms.Hosts;
using System.Web;
using System.Collections;
using System.Data;

namespace JR.WUI
{

    public class Grid_Customizer
    {
        public Control Control;
        string Reference = "";
        Action On_Save = null;
        public Grid_Customizer(Control Grid, object Parent, Action On_Save = null)
        {
            Control = Grid;
            this.On_Save = On_Save;
            Reference = string.Format ("WUI.{2}.{1}.{0}", Grid.Name, Parent.GetType(), Main.App_Name);
            if (Is_List) ListView_Sorter.Apply(List);
            if (Is_List) Page_Size = List.ItemsPerPage;
            if (Is_Grid) Page_Size = 0;
            int P_Size = JR.Convert.To_Integer(Get("P_Size"));
            if (P_Size != 0) Page_Size = P_Size;
            Apply();
        }

        string Get(string Key)
        {
            return VWGContext.Current.Cookies[string.Format("{0}.{1}", Reference, Key)];
        }
        void Set(string Key, string Value)
        {
            VWGContext.Current.Cookies[string.Format("{0}.{1}", Reference, Key)] = Value;
        }
        bool Is_List { get { return Control is ListView; } }
        bool Is_Grid { get { return Control is DataGridView; } }

        public ListView List { get { return Control as ListView; } }
        public DataGridView Grid { get { return Control as DataGridView; } }

        void Apply()
        {
            if (Is_List)
            {
                List.ItemsPerPage = Page_Size;
            }
        }

        public void Save()
        {
            Set("P_SIZE", Page_Size.ToString());
            Apply();
            if (On_Save != null) On_Save();
        }

        public int Page_Size {get;set;}

    }

}
