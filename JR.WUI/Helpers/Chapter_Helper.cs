using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Interfaces;
using Gizmox.WebGUI.Forms.Hosts;
using System.Web;
using System.Collections;

namespace JR.WUI
{

    public class Chapter_Helper
    {
        I_Chapter _Chapter;
        public Chapter_Helper(I_Chapter Chapter)
        {
            _Chapter = Chapter;
        }

        public void Finalize_Chapter()
        {
            // Mettre � jour le tab
            TabControl Nav = _Chapter.Desktop.Nav_Bar;
            Nav.SelectedItem.Controls.Clear();
            Nav.SelectedItem.Controls.Add(_Chapter.Tab);
            Nav.Update();
        }

        public List<ToolBarButton> Tools = new List<ToolBarButton>();
        public List<StatusBarPanel> Status = new List<StatusBarPanel>();

    }

}
