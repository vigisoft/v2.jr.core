﻿using Gizmox.WebGUI.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Web;

namespace JR.WUI
{
    public static class Custom_Tag
    {
        // Retourne un texte au fromat HTML sans les pb de doubles espaces qui ajoutent des \b
        public static string H(this string Text)
        {
            return HttpUtility.HtmlEncode(Text).Replace("  ", " &nbsp;");
        }

        public static string Tag(this string Format, params object[] Params)
        {
            return string.Format(Format, Params);
        }
        public static void Tag(this Control C, string Format, params object[] Params)
        {
            C.Text = Format.Tag (Params).Replace("&","&&");
        }



        public static void Make_Tag(this Label C, string Tag, int Width = 0, int Height = 0)
        {
            if (C.AutoSize)
            {
                C.AutoSize = false;
                C.Height = 25;
            }
            if (Height != 0) C.Height = Height;
            if (Width != 0) C.Width = Width;
            C.Text = $"§§{Tag}";
        }

        public static void Make_Tag(this Label C, string Tag, Control Aligned, int Width = -1, int Height = 0)
        {
            C.AutoSize = false;
            switch (Height)
            {
                case 0:
                    C.Height = Aligned.Height;
                    break;
                case -1:
                    break;
                default:
                    C.Height = Height;
                    break;
            }
            C.Top = Aligned.Top;
            C.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            switch (Width)
            {
                case 0:
                    C.Width = Aligned.Width;
                    break;
                case -1:
                    break;
                default:
                    C.Width = Width;
                    break;
            }
            C.Text = $"§§{Tag}";
        }

        public static void Make_Icon(this Label C, string Icon, string Class = "")
        {
            if (C.AutoSize)
            {
                C.AutoSize = false;
                C.Height = 25;
            }
            C.Text = $"§§<i class='fa fa-{Icon} {Class}'></i>";
        }

        public static void Make_Icon_Button(this Label C, string Icon, Action<Label> On_Click, string Class = "", string Icon_Class = "")
        {
            if (C.AutoSize)
            {
                C.AutoSize = false;
                C.Height = 32;
            }
            C.Width = C.Height;
            C.Text = $"§§<span class='btn btn-default {Class}' style='border:none;background-image:none'><i class='fa fa-{Icon} {Icon_Class}'></i></span>";
            if (On_Click != null) C.Click += (s, e) => { On_Click(C); };
        }

        public static void Make_Icon_Link(this Label C, string Icon, Action<Label> On_Click, string Class = "", string Icon_Class = "")
        {
            if (C.AutoSize)
            {
                C.AutoSize = false;
                C.Height = 25;
            }
            C.Width = C.Height;
            C.Text = $"§§<span class='btn btn-link {Class}'><i class='fa fa-{Icon} {Icon_Class}'></i></span>";
            if (On_Click != null) C.Click += (s, e) => { On_Click(C); };
        }

        static Graphics _G = null;
        static Graphics G
        {
            get
            {
                if (_G == null)
                {
                    var B = new Bitmap(1000, 1000);
                    _G = Graphics.FromImage(B);
                };
                return _G;
            }

        }

        public static Size Text_Size(this Font Font, string Text)
        {
            return G.MeasureString(Text, Font).ToSize();
        }
        public static Size Text_Size(this Control Control, string Text)
        {
            return G.MeasureString(Text, Control.Font).ToSize();
        }
    }
}
