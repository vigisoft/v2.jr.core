using System;
using System.Data;
using Gizmox.WebGUI.Forms;
using System.Collections.Generic;
using System.Text;

namespace JR.WUI
{
    public class Translator
    {
        string Level = "UI";
        string Language = "";

        // Retourne une chaine UI localis�e
        public string Ui(string Key)
        {
            if (Key.Length < 2) return Key;
            switch (Key[0])
            {
                case '.':
                    return Main.Get_String(Language, Level, Key.Substring(1));
                case '#':
                    return Main.Get_String(Language, "UI", Key.Substring(1));
            }
            return Main.Get_String(Language, "UI", Key);
        }


        public Translator (string Default_Level = "")
        {
            if (Default_Level != "") Level = "UI." + Default_Level;
            Language = Session_State.Current.Language;
        }

        bool Translate (string Text, out string New_Text)
        {
            New_Text = null;
            string Key = Text.Trim();
            if (Key.Length < 2) return false;
            switch (Key[0])
            {
                case '.':
                    New_Text = Main.Get_String(Language, Level, Key.Substring(1));
                    return true;
                case '#':
                    New_Text = Main.Get_String(Language, "UI", Key.Substring(1));
                    return true;
            }
            return false;
        }

        public void Label(params Control[] Controls)
        {
            foreach (var C in Controls)
            {
                string New_Text;
                if (Translate(C.Text, out New_Text)) C.Text = New_Text;
            }
        }

        public void Label_Header (ListView Control, string Prefix = "")
        {
            foreach (ColumnHeader C in Control.Columns)
            {
                string New_Text;
                if (C.Text != "" && Translate(Prefix + C.Text, out New_Text))
                {
                    C.Text = New_Text;
                }
            }
        }


    }
}
