using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Common.Interfaces;
using Gizmox.WebGUI.Forms;

namespace JR.WUI
{
    // Interface d'un bureau
    public interface I_Desktop : IGatewayComponent, IRegisteredComponent
    {
        // Barre d'outils
        ToolBar Tool_Bar { get;}

        // Menu principal
        MainMenu Menu { get;}

        // Barre de status
        StatusBar Status_Bar { get;}

        // Onglets de navigation
        TabControl Nav_Bar { get;}

        // Panneau principal
        Panel Panel { get;}

        // Form principal
        Form Form { get;}

        // Outils g�n�raux
        List<ToolBarButton> Tools { get;}

        // Infos status g�n�rales
        List<StatusBarPanel> Status { get;}

        // Appel� � la fermeture de session
        void Cleanup();

        // Chapitre courant
        I_Chapter Chapter { get;set;}

        // Notification de changement de contexte
        void Change_Context(string Domain);

        // Helper associ�
        Desktop_Helper Helper {get;}

        void Activate_Tab(UserControl View);

        T Find_Tab<T>() where T : UserControl;

    }
}
