using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Interfaces;
using Gizmox.WebGUI.Forms.Hosts;
using System.Web;
using System.Collections;

namespace JR.WUI
{
    public class List_Item<T>
    {
        string _Name;
        T _Id;
        public List_Item(string Name, T Id)
        {
            _Name = Name;
            _Id = Id;
        }
        public override string ToString()
        {
            return _Name;
        }
        public T Id
        {
            get { return _Id; }
        }
        public void Rename (string New_Name)
        {
            _Name = New_Name;
        }
        public static T Get_Id(object Item)
        {
            if (Item == null) return default(T);
            if (Item is List_Item<T>) return (Item as List_Item<T>).Id;
            return default(T);
        }
        public static string Get_Name(object Item)
        {
            if (Item == null) return "";
            if (Item is List_Item<T>) return (Item as List_Item<T>)._Name;
            return "";
        }
        public static int Get_Index(IEnumerable Collection, T Id, int Default_Index = -1)
        {
            int I = 0;
            foreach (var Item in Collection)
            {
                List_Item<T> LI = Item as List_Item<T>;
                if (LI != null && (LI.Id != null && LI.Id.Equals(Id) || Id==null && LI.Id == null)) return I;
                I++;
            }
            return Default_Index;
        }
    }

    public class List_Items<T>
    {
        List<List_Item<T>> _List = new List<List_Item<T>>();
        public void Add(string Name, T Id)
        {
            _List.Add(new List_Item<T>(Name, Id));
        }

        public int Count
        {
            get { return _List.Count; }
        }
        public object[] Content
        {
            get { return _List.ToArray(); }
        }

        public int Get_Row(T Id)
        {
            int Row = 0;
            foreach (List_Item<T> I in _List)
            {
                if (I.Id.Equals(Id)) return Row;
                Row++;
            }
            return _List.Count == 0?-1:0;
        }
    }

    public class Tools
    {
        class Redirect : ILink
        {
            string _URL = "";
            public string Url
            {
                get { return _URL; }
            }
            public Redirect(string URL)
            {
                _URL = URL;
            }
        }

        public static void Open_Link(string URL)
        {
            IContext CTX = VWGContext.Current;
            Redirect Link = new Redirect(URL);
            CTX.OpenLink(Link);
        }
    }

    public static class Helper
    {
        public static void Set_Text(Control Control, string Text)
        {
            if (Control is Label || Control is Button)
            {
                Control.Text = Text.Replace("&", "&&");
            }
            else Control.Text = Text;
        }
    }
    public class ListViewSub_Value : ListViewItem.ListViewSubItem
    {
        static string Get_Text(object Value, string Format)
        {
            if (Value is DateTime && (DateTime)Value == DateTime.MinValue) return "-";
            if (Format == "") return Value.ToString();
            return string.Format(Format, Value);
        }
        public ListViewSub_Value(object Value, string Format = "")
        {
            this.Value = Value;
            this.Text = Get_Text(Value, Format);
        }
        public object Value;
        public static void Set(ListViewItem LVI, ref int Row, object Value, string Format = "")
        {
            if (Row >= LVI.SubItems.Count)
            {
                for (int I = LVI.SubItems.Count; I < Row;I++ )
                {
                    LVI.SubItems.Add(new ListViewSub_Value(""));
                }
                LVI.SubItems.Add(new ListViewSub_Value(Value, Format));
            }
            else
            {
                var SV = LVI.SubItems[Row] as ListViewSub_Value;
                SV.Value = Value;
                SV.Text = Get_Text(Value, Format);
                //LVI.Update();
            }
            Row++;
        }
    }

    public class ListView_Sorter : IComparer
    {
        ListView _LV;
        int _Col;
        bool _Desc = false;
        CaseInsensitiveComparer _Comparer = new CaseInsensitiveComparer();
        public ListView_Sorter(ListView objListView)
        {
            _LV = objListView;
            _LV.ColumnClick += new ColumnClickEventHandler(_LV_ColumnClick);
        }

        void _LV_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            _Col = e.Column;
            _Desc = _LV.Columns[_Col].SortOrder == SortOrder.Descending;
            _LV.Sort();
        }

        #region IComparer Membres

        public int Compare(object x, object y)
        {
            int Result = 0;
            ListViewItem Lx = x as ListViewItem;
            ListViewItem Ly = y as ListViewItem;
            ListViewItem.ListViewSubItem Lsx = Lx.SubItems[_Col] as ListViewItem.ListViewSubItem;
            ListViewItem.ListViewSubItem Lsy = Ly.SubItems[_Col] as ListViewItem.ListViewSubItem;
            if (Lsx is ListViewSub_Value)
            {
                ListViewSub_Value Ldsx = Lsx as ListViewSub_Value;
                ListViewSub_Value Ldsy = Lsy as ListViewSub_Value;
                Result = _Comparer.Compare(Ldsx.Value, Ldsy.Value);
            }
            else
            {
                Result = _Comparer.Compare(Lsx.Text, Lsy.Text);
            }
            return _Desc ? -Result : Result;
        }
        #endregion

        public static void Apply(ListView Grid)
        {
            Grid.ListViewItemSorter = new ListView_Sorter(Grid);
        }
    }


}


