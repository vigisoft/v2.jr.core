using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Interfaces;

namespace JR.WUI
{
    // Interface d'un bureau
    public interface I_Page
    {

        // Appel� � l'effacement
        void Cleanup();

        // PRemier affichage
        void First_Display();

        // Helper
        Page_Helper Helper { get;}

    }
}
