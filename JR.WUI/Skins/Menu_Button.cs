using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Forms;

namespace JR.WUI
{
	public partial class Menu_Button : Button
	{
		public Menu_Button()
		{
			InitializeComponent();
            this.FlatStyle = Gizmox.WebGUI.Forms.FlatStyle.Flat;
            this.Margin = new Gizmox.WebGUI.Forms.Padding(15, 2, 2, 2);
            this.Font = new Font(Font.FontFamily, 12F, Font.Style);
        }		
	}
}
