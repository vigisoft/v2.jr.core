﻿#region Using

using System;
using System.Xml;
using System.Drawing;
using System.Runtime.Serialization;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Common.Interfaces;
using Gizmox.WebGUI.Common.Gateways;
using Gizmox.WebGUI.Common.Resources;
using Gizmox.WebGUI.Common.Extensibility;
using Gizmox.WebGUI.Forms.Design;
using System.ComponentModel;
using System.Collections.Generic;
using Gizmox.WebGUI.Forms.Skins;
using Gizmox.WebGUI.Hosting;
using Gizmox.WebGUI.Forms;
using JR;
using System.Text;



#endregion

namespace JR.WUI
{
    public delegate void Notify_Handler(object sender, Notification_EventArgs e);

    [Serializable()]
    public class Notification_EventArgs : EventArgs
    {
        public string JData;
        public string Action;
        public T Data<T>(T Model = default(T))
        {
            return Json.From_Json(JData, Model);
        }
    }


    #region HtmlBox Class

    /// <summary>
    /// A html control
    /// </summary>
    [System.ComponentModel.ToolboxItem(true)]
    [ToolboxBitmapAttribute(typeof(BSBox), "HtmlBox.bmp")]
    [DesignTimeController("Gizmox.WebGUI.Forms.Design.PlaceHolderController, Gizmox.WebGUI.Forms.Design, Version=4.6.5701.0, Culture=neutral, PublicKeyToken=dd2a1fd4d120c769")]
    [ClientController("Gizmox.WebGUI.Client.Controllers.PlaceHolderController, Gizmox.WebGUI.Client, Version=4.6.5701.0, Culture=neutral, PublicKeyToken=0fb8f99bd6cd7e23")]
    [Serializable]
    [ToolboxItemCategory("Common Controls")]
    [Skin(typeof(BSBoxSkin))]
    public class BSBox : FrameControl
    {


        /// <summary>
        /// Provides a property reference to GatewayReference property.
        /// </summary>
        private static SerializableProperty GatewayReferenceProperty = SerializableProperty.Register("GatewayReference", typeof(GatewayReference), typeof(HtmlBox));


        /// <summary>
        /// Provides a property reference to Html property.
        /// </summary>
        private static SerializableProperty HtmlProperty = SerializableProperty.Register("Html", typeof(string), typeof(HtmlBox));


        #region Classes

        /// <summary>
        /// Html gateway handler
        /// </summary>

        [Serializable()]
        public class HtmlGateway : GatewayWriter
        {
            private BSBox mobjHtmlBox;

            /// <summary>
            /// Initializes a new instance of the <see cref="HtmlGateway"/> class.
            /// </summary>
            /// <param name="objHtmlBox">The obj HTML box.</param>
            public HtmlGateway(BSBox objHtmlBox)
            {
                mobjHtmlBox = objHtmlBox;
            }

            /// <summary>
            /// Processes the request.
            /// </summary>
            protected override void ProcessRequest()
            {
                if (mobjHtmlBox != null)
                {
                    Write(mobjHtmlBox.Html.Replace("{CLI_ID}", mobjHtmlBox.Client_Id()));
                }
            }
        }

        #endregion

        #region Class Members
        #endregion

        #region C'Tor/D'Tor

        /// <summary>
        /// Creates a new <see cref="HtmlBox"/> instance.
        /// </summary>
        public BSBox()
        {
            // Set the default size
            Size = new System.Drawing.Size(100, 30);
        }

        #endregion


        #region Methods

        protected override bool Focusable
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the source.
        /// </summary>
        /// <value>The source.</value>
        protected override string Source
        {
            get
            {
                return Html.Replace("{CLI_ID}", Client_Id());
            }
        }


        /// <summary>
        /// Prints this instance.
        /// </summary>
        public void Print()
        {
            VWGClientContext.Current.Invoke("FrameControl_Print", ID.ToString());
        }


        public void Call_Method(string Name, params object[] Args)
        {
            VWGClientContext.Current.Invoke(Name, Args);
        }

        /// <summary>
        /// Full updates of this instance.
        /// </summary>
        public override void Update()
        {
            base.Update();

            FireObservableItemPropertyChanged("Content");
        }


        #endregion

        #region Properties


        public StringBuilder Content = new StringBuilder();

        /// <summary>
        /// Gets or sets the HTML code of the control.
        /// </summary>
        /// <value></value>
        public virtual string Html
        {
            get
            {
                Client_Id();
                if (Content.Length > 0)
                {
                    SetValue(BSBox.HtmlProperty, Content.ToString());
                    Content.Clear();
                }
                // Get the property from the property story                
                return GetValue(BSBox.HtmlProperty, Name);
            }
            set
            {
                string strHtml = Html;
                if (strHtml != value)
                {
                    //If the new value is null or empty remove the property from the property store
                    if (string.IsNullOrEmpty(value))
                    {
                        // Remove the property from the property store
                        RemoveValue<string>(BSBox.HtmlProperty);
                    }
                    else
                    {
                        // Set the property value in the property story  
                        SetValue(BSBox.HtmlProperty, value);
                        Update();
                        //FireObservableItemPropertyChanged("Content");
                    }
                }
            }
        }


        public void Post_Content()
        {
            if (Content.Length == 0) return;
            SetValue(BSBox.HtmlProperty, Content.ToString());
            Content.Clear();
            Update();
        }

        StringBuilder Posted_Content = new StringBuilder();
        public void Post_New_Content()
        {
            if (Content.Length == 0) return;
            var New_Content = Content.ToString();
            if (Posted_Content.ToString() == New_Content)
            {
                Content.Clear();
                return;
            }
            SetValue(BSBox.HtmlProperty, New_Content);
            Posted_Content = Content;
            Content = new StringBuilder();
            Update();
        }

        /// <summary>
        /// Resets the HTML.
        /// </summary>
        private void ResetHtml()
        {
            Html = Name;
        }

        /// <summary>
        /// Indicates if the framecontrol should render source as inline html of as a url for
        /// a frame.
        /// </summary>
        /// <value></value>
        protected override bool IsInline
        {
            get
            {
                return true;
            }
        }


        /// <summary>
        /// Gets or sets the gateway reference.
        /// </summary>
        /// <value>The gateway reference.</value>
        private GatewayReference GatewayReference
        {
            get
            {
                //Get the value from the property store
                return GetValue<GatewayReference>(BSBox.GatewayReferenceProperty, null);
            }
            set
            {
                //If achange was made
                if (GatewayReference != value)
                {
                    //If the value was set to default
                    if (value == null)
                    {
                        //Remove valuefrom property store
                        RemoveValue<GatewayReference>(BSBox.GatewayReferenceProperty);
                    }
                    else
                    {
                        //Set the valu in the property store
                        SetValue(BSBox.GatewayReferenceProperty, value);
                    }
                }
            }
        }
        #endregion

        #region IGatewayControl Members

        /// <summary>
        /// Provides a way to handle gateway requests.
        /// </summary>
        /// <param name="objHostContext">The gateway request host context.</param>
        /// <param name="strAction">The gateway request action.</param>
        /// <returns>
        /// By default this method returns a instance of a class which implements the IGatewayHandler and
        /// throws a non implemented HttpException.
        /// </returns>
        /// <remarks>
        /// This method is called from the implementation of IGatewayComponent which replaces the
        /// IGatewayControl interface. The IGatewayCompoenent is implemented by default in the
        /// RegisteredComponent class which is the base class of most of the Visual WebGui
        /// components.
        /// Referencing a RegisterComponent that overrides this method is done the same way that
        /// a control implementing IGatewayControl, which is by using the GatewayReference class.
        /// </remarks>
        protected override IGatewayHandler ProcessGatewayRequest(HostContext objHostContext, string strAction)
        {
            if (strAction == "Html")
            {
                return new HtmlGateway(this);
            }
            else
            {
                return null;
            }
        }

        public event Notify_Handler Notified;

        protected override void FireEvent(IEvent objEvent)
        {
            if (Notified != null)
            {
                switch (objEvent.Type)
                {
                    case "Notify":  // Notification encodée
                        Notified(this, new Notification_EventArgs { Action = objEvent["Action"].Decode(), JData = objEvent["Data"].Decode() });
                        return;
                    case "JNotify":  // Notification directe de puis js, sans encodage hexa
                        Notified(this, new Notification_EventArgs { Action = objEvent["Action"], JData = objEvent["Data"] });
                        return;
                }
            }
            base.FireEvent(objEvent);
        }

        public string Client_Id()
        {
            if (ClientId == "") ClientId = Guid.NewGuid().ToString("N");
            return ClientId;
        }

        public string JSNotif(string Action, object Data = null, char Quote = '\'')
        {
            if (ClientId == "") ClientId = Guid.NewGuid().ToString("N");
            var SB = new StringBuilder("Notify(");
            SB.Append(Quote);
            SB.Append(ClientId);
            SB.Append(Quote);
            SB.Append(',');
            SB.Append(Quote);
            SB.Append(Action.Encode());
            SB.Append(Quote);
            SB.Append(',');
            SB.Append(Quote);
            SB.Append(Data == null ? "" : Json.To_Json(Data).Encode());
            SB.Append(Quote);
            SB.Append(')');
            return SB.ToString();
        }
        #endregion

    }

    #endregion


}
