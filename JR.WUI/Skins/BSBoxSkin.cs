using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Forms.Skins;

namespace JR.WUI
{
    /// <summary>
    /// HtmlBox Skin
    /// </summary>
    [ToolboxBitmapAttribute(typeof(BSBox), "BSBox.bmp"), Serializable()]
    public class BSBoxSkin : FrameControlSkin
    {
    }
}
