using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common;

namespace WAF.UI
{
    partial class Col_Chooser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visual WebGui Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.B_Ok = new Gizmox.WebGUI.Forms.Button();
            this.panel1 = new Gizmox.WebGUI.Forms.Panel();
            this.G_Source = new Gizmox.WebGUI.Forms.FlowLayoutPanel();
            this.C_Page = new Gizmox.WebGUI.Forms.ComboBox();
            this.label1 = new Gizmox.WebGUI.Forms.Label();
            this.panel2 = new Gizmox.WebGUI.Forms.Panel();
            this.B_Default = new Gizmox.WebGUI.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // B_Ok
            // 
            this.B_Ok.Dock = Gizmox.WebGUI.Forms.DockStyle.Right;
            this.B_Ok.Location = new System.Drawing.Point(236, 0);
            this.B_Ok.Name = "B_Ok";
            this.B_Ok.Size = new System.Drawing.Size(137, 25);
            this.B_Ok.TabIndex = 0;
            this.B_Ok.Text = "Ok";
            this.B_Ok.Click += new System.EventHandler(this.B_Ok_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.B_Default);
            this.panel1.Controls.Add(this.B_Ok);
            this.panel1.Dock = Gizmox.WebGUI.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 398);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(373, 25);
            this.panel1.TabIndex = 5;
            // 
            // G_Source
            // 
            this.G_Source.AutoScroll = true;
            this.G_Source.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.G_Source.FlowDirection = Gizmox.WebGUI.Forms.FlowDirection.TopDown;
            this.G_Source.Location = new System.Drawing.Point(0, 39);
            this.G_Source.Name = "G_Source";
            this.G_Source.Size = new System.Drawing.Size(373, 359);
            this.G_Source.TabIndex = 6;
            // 
            // C_Page
            // 
            this.C_Page.DropDownStyle = Gizmox.WebGUI.Forms.ComboBoxStyle.DropDownList;
            this.C_Page.FormattingEnabled = true;
            this.C_Page.Location = new System.Drawing.Point(79, 6);
            this.C_Page.Name = "C_Page";
            this.C_Page.Size = new System.Drawing.Size(285, 21);
            this.C_Page.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Pagination";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.C_Page);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = Gizmox.WebGUI.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(373, 39);
            this.panel2.TabIndex = 9;
            // 
            // B_Default
            // 
            this.B_Default.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.B_Default.Location = new System.Drawing.Point(0, 0);
            this.B_Default.Name = "B_Default";
            this.B_Default.Size = new System.Drawing.Size(236, 25);
            this.B_Default.TabIndex = 0;
            this.B_Default.Text = "Valeurs par d�faut";
            this.B_Default.Click += new System.EventHandler(this.B_Default_Click);
            // 
            // Col_Chooser
            // 
            this.Controls.Add(this.G_Source);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = Gizmox.WebGUI.Forms.FormBorderStyle.SizableToolWindow;
            this.Size = new System.Drawing.Size(373, 423);
            this.StartPosition = Gizmox.WebGUI.Forms.FormStartPosition.CenterParent;
            this.Text = "Options d\'affichage";
            this.Load += new System.EventHandler(this.Col_Chooser_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Button B_Ok;
        private Panel panel1;
        private FlowLayoutPanel G_Source;
        private ComboBox C_Page;
        private Label label1;
        private Panel panel2;
        private Button B_Default;


    }
}