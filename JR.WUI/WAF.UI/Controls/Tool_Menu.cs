using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Resources;

namespace WAF.UI
{
    public class Tool_Menu : ToolBarButton
    {
        public Tool_Menu(ContextMenu Menu, string Text, string Image)
        {
            this.Text = Text;
            if (Image != "") this.Image = new ImageResourceHandle(Image);
            this.Style = ToolBarButtonStyle.DropDownButton;
            this.DropDownMenu = Menu;
        }
    }
}
