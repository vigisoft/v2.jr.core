using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common;

namespace WAF.UI
{
    partial class Edit_Bar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visual WebGui UserControl Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Edit_Bar));
            this.P_Edit = new Gizmox.WebGUI.Forms.Panel();
            this.B_Add = new Gizmox.WebGUI.Forms.Button();
            this.B_Remove = new Gizmox.WebGUI.Forms.Button();
            this.B_Edit = new Gizmox.WebGUI.Forms.Button();
            this.toolTip1 = new Gizmox.WebGUI.Forms.ToolTip();
            this.B_Refresh = new Gizmox.WebGUI.Forms.Button();
            this.B_Cols = new Gizmox.WebGUI.Forms.Button();
            this.Tools = new Gizmox.WebGUI.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new Gizmox.WebGUI.Forms.Panel();
            this.P_Edit.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // P_Edit
            // 
            this.P_Edit.Controls.Add(this.B_Add);
            this.P_Edit.Controls.Add(this.B_Remove);
            this.P_Edit.Controls.Add(this.B_Edit);
            this.P_Edit.Dock = Gizmox.WebGUI.Forms.DockStyle.Left;
            this.P_Edit.Location = new System.Drawing.Point(0, 0);
            this.P_Edit.Name = "P_Edit";
            this.P_Edit.Size = new System.Drawing.Size(80, 25);
            this.P_Edit.TabIndex = 1;
            // 
            // B_Add
            // 
            this.B_Add.CustomStyle = "F";
            this.B_Add.FlatStyle = Gizmox.WebGUI.Forms.FlatStyle.Flat;
            this.B_Add.Image = new Gizmox.WebGUI.Common.Resources.IconResourceHandle(resources.GetString("B_Add.Image"));
            this.B_Add.Location = new System.Drawing.Point(0, 0);
            this.B_Add.Name = "B_Add";
            this.B_Add.Size = new System.Drawing.Size(25, 23);
            this.B_Add.TabIndex = 0;
            this.toolTip1.SetToolTip(this.B_Add, "Ajouter");
            // 
            // B_Remove
            // 
            this.B_Remove.CustomStyle = "F";
            this.B_Remove.FlatStyle = Gizmox.WebGUI.Forms.FlatStyle.Flat;
            this.B_Remove.Image = new Gizmox.WebGUI.Common.Resources.IconResourceHandle(resources.GetString("B_Remove.Image"));
            this.B_Remove.Location = new System.Drawing.Point(25, 0);
            this.B_Remove.Name = "B_Remove";
            this.B_Remove.Size = new System.Drawing.Size(25, 23);
            this.B_Remove.TabIndex = 0;
            this.toolTip1.SetToolTip(this.B_Remove, "Supprimer");
            // 
            // B_Edit
            // 
            this.B_Edit.CustomStyle = "F";
            this.B_Edit.FlatStyle = Gizmox.WebGUI.Forms.FlatStyle.Flat;
            this.B_Edit.Image = new Gizmox.WebGUI.Common.Resources.IconResourceHandle(resources.GetString("B_Edit.Image"));
            this.B_Edit.Location = new System.Drawing.Point(50, 0);
            this.B_Edit.Name = "B_Edit";
            this.B_Edit.Size = new System.Drawing.Size(25, 23);
            this.B_Edit.TabIndex = 0;
            this.toolTip1.SetToolTip(this.B_Edit, "Editer");
            // 
            // B_Refresh
            // 
            this.B_Refresh.CustomStyle = "F";
            this.B_Refresh.FlatStyle = Gizmox.WebGUI.Forms.FlatStyle.Flat;
            this.B_Refresh.Image = new Gizmox.WebGUI.Common.Resources.IconResourceHandle(resources.GetString("B_Refresh.Image"));
            this.B_Refresh.Location = new System.Drawing.Point(6, 0);
            this.B_Refresh.Name = "B_Refresh";
            this.B_Refresh.Size = new System.Drawing.Size(25, 23);
            this.B_Refresh.TabIndex = 0;
            this.toolTip1.SetToolTip(this.B_Refresh, "Actualiser");
            // 
            // B_Cols
            // 
            this.B_Cols.CustomStyle = "F";
            this.B_Cols.FlatStyle = Gizmox.WebGUI.Forms.FlatStyle.Flat;
            this.B_Cols.Image = new Gizmox.WebGUI.Common.Resources.IconResourceHandle(resources.GetString("B_Cols.Image"));
            this.B_Cols.Location = new System.Drawing.Point(31, 0);
            this.B_Cols.Name = "B_Cols";
            this.B_Cols.Size = new System.Drawing.Size(25, 23);
            this.B_Cols.TabIndex = 0;
            this.toolTip1.SetToolTip(this.B_Cols, "Options d\'affichage");
            // 
            // Tools
            // 
            this.Tools.BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.FixedSingle;
            this.Tools.BorderWidth = new Gizmox.WebGUI.Forms.BorderWidth(1, 0, 1, 0);
            this.Tools.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.Tools.FlowDirection = Gizmox.WebGUI.Forms.FlowDirection.RightToLeft;
            this.Tools.Location = new System.Drawing.Point(80, 0);
            this.Tools.Margin = new Gizmox.WebGUI.Forms.Padding(0, 0, 10, 0);
            this.Tools.Name = "Tools";
            this.Tools.Size = new System.Drawing.Size(597, 25);
            this.Tools.TabIndex = 2;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.B_Cols);
            this.flowLayoutPanel1.Controls.Add(this.B_Refresh);
            this.flowLayoutPanel1.Dock = Gizmox.WebGUI.Forms.DockStyle.Right;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(677, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(65, 25);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // Edit_Bar
            // 
            this.BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.FixedSingle;
            this.BorderWidth = new Gizmox.WebGUI.Forms.BorderWidth(0, 0, 0, 1);
            this.Controls.Add(this.Tools);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.P_Edit);
            this.Size = new System.Drawing.Size(742, 25);
            this.Text = "Nav_Bar";
            this.P_Edit.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel P_Edit;
        private ToolTip toolTip1;
        public Button B_Remove;
        public Button B_Edit;
        public Button B_Add;
        private Panel flowLayoutPanel1;
        public Button B_Refresh;
        public FlowLayoutPanel Tools;
        public Button B_Cols;


    }
}