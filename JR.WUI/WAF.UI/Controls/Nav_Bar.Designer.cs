using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common;

namespace WAF.UI
{
    partial class Nav_Bar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visual WebGui UserControl Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Nav_Bar));
            this.flowLayoutPanel1 = new Gizmox.WebGUI.Forms.Panel();
            this.B_Last = new Gizmox.WebGUI.Forms.Button();
            this.B_Next = new Gizmox.WebGUI.Forms.Button();
            this.B_Prev = new Gizmox.WebGUI.Forms.Button();
            this.B_First = new Gizmox.WebGUI.Forms.Button();
            this.flowLayoutPanel2 = new Gizmox.WebGUI.Forms.Panel();
            this.B_Db = new Gizmox.WebGUI.Forms.Button();
            this.toolTip1 = new Gizmox.WebGUI.Forms.ToolTip();
            this.C_Message = new Gizmox.WebGUI.Forms.Label();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.B_Last);
            this.flowLayoutPanel1.Controls.Add(this.B_Next);
            this.flowLayoutPanel1.Controls.Add(this.B_Prev);
            this.flowLayoutPanel1.Controls.Add(this.B_First);
            this.flowLayoutPanel1.Dock = Gizmox.WebGUI.Forms.DockStyle.Right;
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Symbol", 8.25F);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(631, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(111, 25);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // B_Last
            // 
            this.B_Last.CustomStyle = "F";
            this.B_Last.FlatStyle = Gizmox.WebGUI.Forms.FlatStyle.Flat;
            this.B_Last.Image = new Gizmox.WebGUI.Common.Resources.IconResourceHandle(resources.GetString("B_Last.Image"));
            this.B_Last.Location = new System.Drawing.Point(86, 0);
            this.B_Last.Name = "B_Last";
            this.B_Last.Size = new System.Drawing.Size(25, 23);
            this.B_Last.TabIndex = 0;
            this.toolTip1.SetToolTip(this.B_Last, "Dernier �l�ment");
            // 
            // B_Next
            // 
            this.B_Next.CustomStyle = "F";
            this.B_Next.FlatStyle = Gizmox.WebGUI.Forms.FlatStyle.Flat;
            this.B_Next.Image = new Gizmox.WebGUI.Common.Resources.IconResourceHandle(resources.GetString("B_Next.Image"));
            this.B_Next.Location = new System.Drawing.Point(61, 0);
            this.B_Next.Name = "B_Next";
            this.B_Next.Size = new System.Drawing.Size(25, 23);
            this.B_Next.TabIndex = 0;
            this.toolTip1.SetToolTip(this.B_Next, "Suivant");
            // 
            // B_Prev
            // 
            this.B_Prev.CustomStyle = "F";
            this.B_Prev.FlatStyle = Gizmox.WebGUI.Forms.FlatStyle.Flat;
            this.B_Prev.Image = new Gizmox.WebGUI.Common.Resources.IconResourceHandle(resources.GetString("B_Prev.Image"));
            this.B_Prev.Location = new System.Drawing.Point(36, 0);
            this.B_Prev.Name = "B_Prev";
            this.B_Prev.Size = new System.Drawing.Size(25, 23);
            this.B_Prev.TabIndex = 0;
            this.toolTip1.SetToolTip(this.B_Prev, "Pr�c�dent");
            // 
            // B_First
            // 
            this.B_First.CustomStyle = "F";
            this.B_First.FlatStyle = Gizmox.WebGUI.Forms.FlatStyle.Flat;
            this.B_First.Image = new Gizmox.WebGUI.Common.Resources.IconResourceHandle(resources.GetString("B_First.Image"));
            this.B_First.Location = new System.Drawing.Point(11, 0);
            this.B_First.Name = "B_First";
            this.B_First.Size = new System.Drawing.Size(25, 23);
            this.B_First.TabIndex = 0;
            this.toolTip1.SetToolTip(this.B_First, "Premier �l�ment");
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.B_Db);
            this.flowLayoutPanel2.Dock = Gizmox.WebGUI.Forms.DockStyle.Left;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(34, 25);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // B_Db
            // 
            this.B_Db.CustomStyle = "F";
            this.B_Db.FlatStyle = Gizmox.WebGUI.Forms.FlatStyle.Flat;
            this.B_Db.Image = new Gizmox.WebGUI.Common.Resources.IconResourceHandle(resources.GetString("B_Db.Image"));
            this.B_Db.Location = new System.Drawing.Point(0, 0);
            this.B_Db.Name = "B_Db";
            this.B_Db.Size = new System.Drawing.Size(25, 23);
            this.B_Db.TabIndex = 0;
            this.toolTip1.SetToolTip(this.B_Db, "Contenu base de donn�es");
            // 
            // C_Message
            // 
            this.C_Message.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.C_Message.Location = new System.Drawing.Point(34, 0);
            this.C_Message.Name = "C_Message";
            this.C_Message.Size = new System.Drawing.Size(35, 13);
            this.C_Message.TabIndex = 2;
            this.C_Message.Text = "label1";
            this.C_Message.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Nav_Bar
            // 
            this.BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.FixedSingle;
            this.BorderWidth = new Gizmox.WebGUI.Forms.BorderWidth(0, 0, 0, 1);
            this.Controls.Add(this.C_Message);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Size = new System.Drawing.Size(742, 25);
            this.Text = "Nav_Bar";
            this.Load += new System.EventHandler(this.Nav_Bar_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel flowLayoutPanel1;
        private Panel flowLayoutPanel2;
        private ToolTip toolTip1;
        private Label C_Message;
        public Button B_Last;
        public Button B_Next;
        public Button B_Prev;
        public Button B_First;
        public Button B_Db;


    }
}