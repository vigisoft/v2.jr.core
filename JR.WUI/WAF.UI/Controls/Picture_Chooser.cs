#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Common.Resources;
using Gizmox.WebGUI.Forms;
using JR.Saf;

#endregion

namespace WAF.UI
{
    public partial class Picture_Chooser : Form
    {
        public Picture_Chooser()
        {
            InitializeComponent();
        }

        private void Col_Chooser_Load(object sender, EventArgs e)
        {
            B_Remove.Enabled = Current_Name != "";
            C_Pic.Image = Current_Picture;

            // Si pas d'image proposer directement le tÚlÚchargement
            if (Current_Name == "") D_Upload.ShowDialog();
        }

        private void B_Ok_Click(object sender, EventArgs e)
        {
            Close();
        }

        public ResourceHandle Current_Picture { get; set; }
        public string Current_Name { get; set; }
        public static void Choose (ResourceHandle Current_Picture, string Current_Name, EventHandler On_Change)
        {
            Picture_Chooser C = new Picture_Chooser { Current_Picture = Current_Picture, Current_Name = Current_Name };
            Action<object, EventArgs> On_Close = (sender, e) =>
            {
                Picture_Chooser D = sender as Picture_Chooser;
                if (!D.Remove_Picture && !D.Change_Picture) return;
                On_Change(sender, e);
            };
            C.ShowDialog (new EventHandler (On_Close));
        }

        private void D_Upload_FileOk(object sender, CancelEventArgs e)
        {
            for (int intIndex = 0; intIndex < D_Upload.Files.Count; intIndex++)
            {
                string Upload_Name = ((Gizmox.WebGUI.Common.Resources.HttpPostedFileHandle)(D_Upload.Files[intIndex])).PostedFileName;
                string File_Name = Path.Combine(Main.Real_File_Name("Upload"), Guid.NewGuid().ToString() + Path.GetExtension(Upload_Name));
                try
                {
                    D_Upload.Files[intIndex].SaveAs(File_Name);
                    Remove_Picture = false;
                    Change_Picture = true;
                    Current_Name = File_Name;
                    Close();
                }
                catch
                {
                }
                break;
            }
        }

        private void On_Remove(object sender, EventArgs e)
        {
            Form D = sender as Form;
            if (D.DialogResult != DialogResult.OK) return;
            Remove_Picture = true;
            Change_Picture = false;
            Close();
        }

        public bool Remove_Picture = false;
        public bool Change_Picture = false;


        private void B_Remove_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Voulez-vous supprimer cette image ?", "Suppression image", MessageBoxButtons.OKCancel, On_Remove);
        }

        private void B_Upload_Click(object sender, EventArgs e)
        {
            D_Upload.ShowDialog();
        }

    }
}