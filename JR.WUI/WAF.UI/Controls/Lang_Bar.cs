#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Common.Resources;
using Gizmox.WebGUI.Forms;
using JR.WUI;

#endregion

namespace WAF.UI
{
    public partial class Lang_Bar : UserControl
    {
        public Lang_Bar()
        {
            InitializeComponent();
        }

        private void Nav_Bar_Load(object sender, EventArgs e)
        {
            foreach (string L in Main.Language_List)
            {
                RadioButton R = new RadioButton();
                R.Text = "";
                R.Image = new IconResourceHandle(string.Format ("{0}.png", L.ToLower()));
                R.Dock = DockStyle.Left;
                R.Width = 50;
                R.ImageAlign = ContentAlignment.MiddleLeft;
                R.CheckedChanged += R_CheckedChanged;
                R.Tag = L;
                this.Controls.Insert (0, R);
            }
        }

        public Action <string> On_Lang { get; set; }
        void R_CheckedChanged(object sender, EventArgs e)
        {
            if (On_Lang == null) return;
            RadioButton R = sender as RadioButton;
            if (!R.Checked) return;
            On_Lang(R.Tag as string);
        }

        public string Lang
        {
            get
            {
                foreach (RadioButton R in Controls)
                {
                    if (R == null) continue;
                    if (R.Checked) return R.Tag as string;
                }
                return "";
            }
            set
            {
                foreach (RadioButton R in Controls)
                {
                    if (R == null) continue;
                    if (JR.Strings.Like (value, R.Tag as string))
                    {
                        R.Checked = true;
                        break;
                    }
                }
            }
        }
    }
}
