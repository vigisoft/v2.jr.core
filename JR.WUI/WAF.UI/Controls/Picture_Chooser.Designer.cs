using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common;

namespace WAF.UI
{
    partial class Picture_Chooser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visual WebGui Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new Gizmox.WebGUI.Forms.Panel();
            this.B_Remove = new Gizmox.WebGUI.Forms.Button();
            this.B_Upload = new Gizmox.WebGUI.Forms.Button();
            this.D_Upload = new Gizmox.WebGUI.Forms.OpenFileDialog();
            this.C_Pic = new Gizmox.WebGUI.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.C_Pic)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.B_Remove);
            this.panel1.Controls.Add(this.B_Upload);
            this.panel1.Dock = Gizmox.WebGUI.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 333);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(338, 25);
            this.panel1.TabIndex = 5;
            // 
            // B_Remove
            // 
            this.B_Remove.Dock = Gizmox.WebGUI.Forms.DockStyle.Right;
            this.B_Remove.Location = new System.Drawing.Point(16, 0);
            this.B_Remove.Name = "B_Remove";
            this.B_Remove.Size = new System.Drawing.Size(161, 25);
            this.B_Remove.TabIndex = 6;
            this.B_Remove.Text = "Supprimer cette image";
            this.B_Remove.Click += new System.EventHandler(this.B_Remove_Click);
            // 
            // B_Upload
            // 
            this.B_Upload.Dock = Gizmox.WebGUI.Forms.DockStyle.Right;
            this.B_Upload.Location = new System.Drawing.Point(177, 0);
            this.B_Upload.Name = "B_Upload";
            this.B_Upload.Size = new System.Drawing.Size(161, 25);
            this.B_Upload.TabIndex = 6;
            this.B_Upload.Text = "Modifier cette image";
            this.B_Upload.Click += new System.EventHandler(this.B_Upload_Click);
            // 
            // D_Upload
            // 
            this.D_Upload.FileOk += new System.ComponentModel.CancelEventHandler(this.D_Upload_FileOk);
            // 
            // C_Pic
            // 
            this.C_Pic.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.C_Pic.Location = new System.Drawing.Point(0, 0);
            this.C_Pic.Name = "C_Pic";
            this.C_Pic.Size = new System.Drawing.Size(338, 333);
            this.C_Pic.SizeMode = Gizmox.WebGUI.Forms.PictureBoxSizeMode.CenterImage;
            this.C_Pic.TabIndex = 6;
            this.C_Pic.TabStop = false;
            // 
            // Picture_Chooser
            // 
            this.Controls.Add(this.C_Pic);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = Gizmox.WebGUI.Forms.FormBorderStyle.SizableToolWindow;
            this.Size = new System.Drawing.Size(338, 358);
            this.StartPosition = Gizmox.WebGUI.Forms.FormStartPosition.CenterParent;
            this.Text = "Choix d\'une image";
            this.Load += new System.EventHandler(this.Col_Chooser_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.C_Pic)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private OpenFileDialog D_Upload;
        private Button B_Remove;
        private Button B_Upload;
        private PictureBox C_Pic;


    }
}