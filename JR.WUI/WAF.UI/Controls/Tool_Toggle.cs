using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Resources;

namespace WAF.UI
{
    public class Tool_Toggle:ToolBarButton
    {
        public Tool_Toggle(string Text, string Image)
        {
            this.Text = Text;
            if (Image != "") this.Image = new ImageResourceHandle(Image);
            this.Style = ToolBarButtonStyle.ToggleButton;
        }
    }

}
