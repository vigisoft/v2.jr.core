#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;
using JR.WUI;

#endregion

namespace WAF.UI
{
    public partial class Col_Chooser : Form
    {
        public Col_Chooser()
        {
            InitializeComponent();
        }

        public ListView LV { get; set; }
        public string Resource_Name { get; set; }

        private void Col_Chooser_Load(object sender, EventArgs e)
        {
            string Paging = JR.WUI.User_State.Current.Get_State(Resource_Name + ".PAGE");
            Action<string, string> Add = (Name, Val) =>
                {
                    C_Page.Items.Add(new List_Item<string>(Name, Val));
                    if (Paging == Val) C_Page.SelectedIndex = C_Page.Items.Count-1;
                };
            Add("Par d�faut", "");
            Add("20 lignes", "20");
            Add("50 lignes", "50");
            Add("100 lignes", "100");
            Add("500 lignes", "500");
            Add("Tout afficher", "-1");

            Dictionary<string, ColumnHeader> L = new Dictionary<string, ColumnHeader>();
            string Col_List = JR.WUI.User_State.Current.Get_State(Resource_Name + ".COLS");
            foreach (ColumnHeader C in LV.Columns)
            {
                string Tag = C.Tag as string;
                if (Tag == null) continue;
                L.Add (Tag, C);
            }

            List<Gizmox.WebGUI.Forms.Component> dragtargets = new List<Gizmox.WebGUI.Forms.Component>();
            Action<ColumnHeader, bool> Add_Source = (Col, Checked) =>
                {
                    CheckBox CB = new CheckBox();
                    CB.Height = 18;
                    CB.Text = Col.Text;
                    CB.Checked = Checked;
                    CB.Tag = Col;
                    G_Source.Controls.Add(CB);
                    dragtargets.Add(CB);
                };

            // Remettre dans l'ordre
            foreach (string Tag in Col_List.Split(','))
            {
                if (!L.ContainsKey(Tag)) continue;
                Add_Source(L[Tag], true);
                L.Remove(Tag);
            }

            foreach (ColumnHeader C in LV.Columns)
            {
                string Tag = C.Tag as string;
                if (Tag == null) continue;
                if (!L.ContainsKey(Tag)) continue;
                Add_Source(L[Tag], Col_List == "");
            }

            foreach (CheckBox CB in G_Source.Controls)
            {
                CB.AllowDrag = true;
                CB.AllowDrop = true;
                CB.DragTargets = dragtargets.ToArray();
                CB.DragDrop += new DragEventHandler(CB_DragDrop);
            }

        }

        void CB_DragDrop(object sender, DragEventArgs e)
        {
            DragDropEventArgs dde = e as DragDropEventArgs;
            CheckBox dropitem = dde.ExplicitTarget as CheckBox;
            CheckBox dragitem = dde.Source as CheckBox;
            if (dropitem == dragitem) return;
            int New_Row = G_Source.Controls.IndexOf (dropitem);
            G_Source.Controls.Remove(dragitem);
            G_Source.Controls.Insert (New_Row, dragitem);
        }

        private void B_Ok_Click(object sender, EventArgs e)
        {
            StringBuilder SB = new StringBuilder();
            foreach (CheckBox CB in G_Source.Controls)
            {
                if (!CB.Checked) continue;
                if (SB.Length > 0) SB.Append(',');
                SB.Append((CB.Tag as ColumnHeader).Tag);
            }
            JR.WUI.User_State.Current.Set_State(Resource_Name + ".COLS", SB.ToString());
            JR.WUI.User_State.Current.Set_State(Resource_Name + ".PAGE",List_Item<string>.Get_Id (C_Page.SelectedItem));
            DialogResult = DialogResult.OK;
            Close();
        }

        private void B_Default_Click(object sender, EventArgs e)
        {
            JR.WUI.User_State.Current.Set_State(Resource_Name + ".COLS", "");
            JR.WUI.User_State.Current.Set_State(Resource_Name + ".PAGE", "");
            DialogResult = DialogResult.OK;
            Close();
        }


    }
}