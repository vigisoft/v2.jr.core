#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Common.Resources;
using Gizmox.WebGUI.Forms;

#endregion

namespace WAF.UI
{
    public partial class Nav_Bar : UserControl
    {
        public Nav_Bar()
        {
            InitializeComponent();
        }

        public void Warning(string Message)
        {
            C_Message.Text = Message;
            C_Message.ForeColor = Color.Red;
        }

        public void Message(string Message)
        {
            C_Message.Text = Message;
            C_Message.ForeColor = Color.DarkBlue;
        }

        public void Clear_Message()
        {
            C_Message.Text = "";
        }

        private void Nav_Bar_Load(object sender, EventArgs e)
        {
            Clear_Message();
        }

        public void Enable(bool State, params Control[] Buttons)
        {
            Action<Control> Enable = (C) =>
            {
                Button B = C as Button;
                if (B == null) return;
                B.Enabled = State;
                if (B.Image == null) return;
                string File = B.Image.File;
                File = File.Replace("-Disable.png", ".png");
                if (!State) File = File.Replace(".png", "-Disable.png");
                if (B.Image.File == File) return;
                B.Image = new IconResourceHandle(File);
            };
            foreach (Control C in Buttons)
            {
                Enable(C);
            }
        }

    }
}