using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common;

namespace WAF.UI
{
    partial class Inline_Edit_Bar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visual WebGui UserControl Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Inline_Edit_Bar));
            this.flowLayoutPanel2 = new Gizmox.WebGUI.Forms.Panel();
            this.B_Add = new Gizmox.WebGUI.Forms.Button();
            this.B_Remove = new Gizmox.WebGUI.Forms.Button();
            this.toolTip1 = new Gizmox.WebGUI.Forms.ToolTip();
            this.Tools = new Gizmox.WebGUI.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.B_Add);
            this.flowLayoutPanel2.Controls.Add(this.B_Remove);
            this.flowLayoutPanel2.Dock = Gizmox.WebGUI.Forms.DockStyle.Left;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(52, 25);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // B_Add
            // 
            this.B_Add.CustomStyle = "F";
            this.B_Add.FlatStyle = Gizmox.WebGUI.Forms.FlatStyle.Flat;
            this.B_Add.Image = new Gizmox.WebGUI.Common.Resources.IconResourceHandle(resources.GetString("B_Add.Image"));
            this.B_Add.Location = new System.Drawing.Point(0, 0);
            this.B_Add.Name = "B_Add";
            this.B_Add.Size = new System.Drawing.Size(25, 23);
            this.B_Add.TabIndex = 0;
            this.toolTip1.SetToolTip(this.B_Add, "Ajouter");
            // 
            // B_Remove
            // 
            this.B_Remove.CustomStyle = "F";
            this.B_Remove.FlatStyle = Gizmox.WebGUI.Forms.FlatStyle.Flat;
            this.B_Remove.Image = new Gizmox.WebGUI.Common.Resources.IconResourceHandle(resources.GetString("B_Remove.Image"));
            this.B_Remove.Location = new System.Drawing.Point(25, 0);
            this.B_Remove.Name = "B_Remove";
            this.B_Remove.Size = new System.Drawing.Size(25, 23);
            this.B_Remove.TabIndex = 0;
            this.toolTip1.SetToolTip(this.B_Remove, "Supprimer");
            // 
            // Tools
            // 
            this.Tools.BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.FixedSingle;
            this.Tools.BorderWidth = new Gizmox.WebGUI.Forms.BorderWidth(1, 0, 1, 0);
            this.Tools.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.Tools.FlowDirection = Gizmox.WebGUI.Forms.FlowDirection.RightToLeft;
            this.Tools.Location = new System.Drawing.Point(52, 0);
            this.Tools.Margin = new Gizmox.WebGUI.Forms.Padding(0, 0, 10, 0);
            this.Tools.Name = "Tools";
            this.Tools.Size = new System.Drawing.Size(690, 25);
            this.Tools.TabIndex = 2;
            // 
            // Inline_Edit_Bar
            // 
            this.BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.FixedSingle;
            this.BorderWidth = new Gizmox.WebGUI.Forms.BorderWidth(0, 0, 0, 1);
            this.Controls.Add(this.Tools);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Size = new System.Drawing.Size(742, 25);
            this.Text = "Nav_Bar";
            this.flowLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel flowLayoutPanel2;
        private ToolTip toolTip1;
        public Button B_Remove;
        public Button B_Add;
        public FlowLayoutPanel Tools;


    }
}