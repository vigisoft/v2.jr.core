using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using Gizmox.WebGUI.Forms;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using Gizmox.WebGUI.Common.Interfaces;
using JR.DBO;
using WAF.UI.Edit;

namespace WAF.UI
{

    public class Grid_Helper<T>:I_Indexer  where T:JRO_Row, I_Editable, new()
    {
        ListView _LV;
        I_Grid<T> Page;
        public Chapter_Helper Chapter_Helper 
        {
            get
            {
                I_Chapter IC = this as I_Chapter;
                if (IC == null) return null;
                return IC.Helper;
            }
        }

        public Item_Inline_Editor Inline_Editor;

        string Unique_Resource_Name
        {
            get
            {
                string Resource_Name = Page.GetType().FullName;
                if (Chapter_Helper != null) Resource_Name += "." + Chapter_Helper.Context.ToString();
                return Resource_Name;
            }
        }

        public Grid_Helper(I_Grid<T> Page, Item_Inline_Editor Editor = null)
        {
            this.Page = Page;
            _LV = Get_Control ("Grid") as ListView;
            if (_LV == null) return;
            _LV.GridLines = true;
            _LV.UseInternalPaging = true;
            _LV.ListViewItemSorter = new List_View_Sorter(_LV);
            Inline_Editor = Editor;
            if (Inline_Editor != null)
            {
                Inline_Editor.Indexer = this;
                _LV.SelectedIndexChanged += Grid_DoubleClick;
                Inline_Editor.On_Valid += Inline_Editor_Save;
            }

            // Associer les boutons
            Associate("B_Add", B_Add_Click);
            Associate("B_Edit", B_Edit_Click);
            Associate("B_Remove", B_Remove_Click);
            Associate("B_Refresh", B_Refresh_Click);
            Associate("B_Cols", B_Cols_Click);
            _LV.DoubleClick += Grid_DoubleClick;

        }

        void Associate(string Name, EventHandler Handler)
        {
            Control C = Get_Control(Name);
            if (C == null) return;
            C.Click += Handler;
        }

        Control Get_Control(string Name)
        {
            foreach (var C in ((Control)Page).Controls.Find(Name, true))
            {
                return C;
            }
            return null;
        }

        public void Prepare(bool Autosize, bool Sort)
        {
            Prepare(Autosize, Sort, 0);
        }
        public void Prepare (bool Autosize, bool Sort, int Page_Size)
        {
            if (_LV == null) return;
            if (Sort)
            {
                _LV.ListViewItemSorter = new List_View_Sorter(_LV);
            }
            if (Autosize && (_LV.Items.Count > 0))
            {
                for (int I = 1; I < _LV.Columns.Count; I++)
                {
                    if (_LV.Columns[I].Type == ListViewColumnType.Text)
                    {
                        int Init_Size = _LV.Columns[I].Width;
                        _LV.AutoResizeColumn(I, ColumnHeaderAutoResizeStyle.ColumnContent);
                        int New_Size = _LV.Columns[I].Width;
                        if (New_Size < Init_Size) _LV.Columns[I].Width = Init_Size;
                    }
                }
            }

            string Paging = JR.WUI.User_State.Current.Get_State(Unique_Resource_Name + ".PAGE");
            if (Paging != "") Page_Size = JR.Convert.To_Integer(Paging);
            switch (Page_Size)
            {
                case 0:
                    if (_LV.Items.Count > 1000)
                    {
                        _LV.ItemsPerPage = 30;      // Pages de petite taille pour �viter d'avoir ascenseur + pagination
                    }
                    else
                    {
                        _LV.ItemsPerPage = 1000;      // Pas de pagination, donc seulement ascenceur
                    }
                    break;
                case -1:    // Pas de pagination du tout mais limiter quand m�me
                    _LV.ItemsPerPage = 10000;
                    break;
                default:
                    _LV.ItemsPerPage = Page_Size;      // Pages de petite taille pour bien voir qu'il manque des �l�ments
                    break;
            }

            // Pr�parer la liste des colonnes personnalis�e
            //string Col_List = "NOM,PRENOM,LOGIN,SOCIETE,SECTEUR,FONCTION,SUPPORT,EMAIL,DTINSCRIPTION,DERVISITE,VISITES,DERACTIVATION,DESACTIVATION,TELECH,NIVEAU";
            string Col_List = JR.WUI.User_State.Current.Get_State(Unique_Resource_Name + ".COLS");

            //string Col_List = "LOGIN,SOCIETE,NOM,PRENOM,DERACTIVATION,DESACTIVATION,TELECH,NIVEAU";
            bool All_Cols = Col_List == "";
            //string Col_List = "NOM,PRENOM,LOGIN,SOCIETE,SECTEUR,FONCTION,SUPPORT,EMAIL,DTINSCRIPTION,DERVISITE,VISITES,DERACTIVATION,DESACTIVATION,TELECH,NIVEAU";

            // Application de la visibilit� et du rang d'affichage des colonnes
            foreach (ColumnHeader C in _LV.Columns)
            {
                C.DisplayIndex = C.Index;
                C.Visible = All_Cols || (C.Tag as string == null);
            }

            if (!All_Cols)
            {
                int D_Index = 0;
                foreach (ColumnHeader C in _LV.Columns)
                {
                    if (C.Tag as string != null) break;
                    D_Index++;
                }
                foreach (string Col in Col_List.Split(','))
                {
                    foreach (ColumnHeader C in _LV.Columns)
                    {
                        string Name = C.Tag as string;
                        if (Name == null) continue;
                        if (Name == Col)
                        {
                            C.DisplayIndex = D_Index;
                            C.Visible = true;
                            break;
                        }
                    }
                    D_Index++;
                }
            }
        }

        public Gizmox.WebGUI.Forms.Control List
        {
            get { return _LV; }
        }

        public bool Select_Next()
        {
            if (_LV == null) return false;
            if (!Has_Next) return false;
            _LV.SelectedIndex++;
            //_LV.EnsureVisible(_LV.SelectedIndex);
            //if (After_Selected_Change != null) After_Selected_Change();
            return true;
        }

        public bool Select_Previous()
        {
            if (_LV == null) return false;
            if (!Has_Previous) return false;
            _LV.SelectedIndex--;
            //if (After_Selected_Change != null) After_Selected_Change();
            //_LV.EnsureVisible(_LV.SelectedIndex);
            return true;
        }

        public bool Has_Previous
        {
            get { return (_LV != null) && _LV.SelectedIndex > 0; }
        }

        public bool Has_Next
        {
            get { return (_LV != null) && _LV.SelectedIndex < _LV.Items.Count - 1; }
        }


        T Selected
        {
            get
            {
                if (_LV.SelectedIndex < 0) return null;
                return _LV.SelectedItem.Tag as T;
            }
        }

        void On_Column_Tool_Close(object sender, EventArgs e)
        {
            Col_Chooser D = sender as Col_Chooser;
            if (D.DialogResult != DialogResult.OK) return;
            Page.Refresh_All();
        }

        private void B_Cols_Click(object sender, EventArgs e)
        {
            Col_Chooser C = new Col_Chooser { LV = _LV, Resource_Name = Unique_Resource_Name};
            C.ShowDialog(_LV.Form, On_Column_Tool_Close);
        }

        private void Grid_DoubleClick(object sender, EventArgs e)
        {
            Edit (Selected);
        }

        private void B_Add_Click(object sender, EventArgs e)
        {
            Edit(Page.New_Row);
        }

        private void B_Edit_Click(object sender, EventArgs e)
        {
            Edit(Selected);
        }

        private void On_Remove (object sender, EventArgs e)
        {
            Form D = sender as Form;
            if (D.DialogResult != DialogResult.OK) return;
            T R = Selected;
            if (R == null) return;
            R.Remove();

            // Supprimer du datatable et de la liste
            Page.List.Remove(R);
            _LV.SelectedItem.Remove();
        }

        private void B_Remove_Click(object sender, EventArgs e)
        {
            T R = Selected;
            if (R == null) return;
            if (!R.Removable)
            {
                MessageBox.Show("Suppression interdite");
                return;
            }
            MessageBox.Show("Voulez vous supprimer d�finitivement " + R.Name + " ?", "Suppression " + R.ToString(), MessageBoxButtons.OKCancel, On_Remove);
        }

        private void B_Refresh_Click(object sender, EventArgs e)
        {
            Page.Refresh_All();
        }

        private void Editor_Close(object sender, EventArgs e)
        {
            WAF.UI.Edit.Item_Editor D = sender as WAF.UI.Edit.Item_Editor;
            if (D.New_Item)
            {
                if (D.Helper.Save_Count > 0) Add_Row(D.Item as T);
            }
            else
            {
                foreach (ListViewItem I in _LV.Items)
                {
                    T R = I.Tag as T;
                    if (R.Is_Dirty)
                    {
                        I.SubItems.Clear();
                        Page.Bind(I);
                        I.Update();
                        R.Is_Dirty = false;
                    }
                }
            }
        }

        private void Inline_Editor_Save (object sender, EventArgs e)
        {
            WAF.UI.Edit.Item_Inline_Editor D = sender as WAF.UI.Edit.Item_Inline_Editor;
            if (D.New_Item)
            {
                if (D.Helper.Save_Count > 0) Add_Row(D.Item as T);
            }
            else
            {
                foreach (ListViewItem I in _LV.Items)
                {
                    T R = I.Tag as T;
                    if (R.Is_Dirty)
                    {
                        I.SubItems.Clear();
                        Page.Bind(I);
                        I.Update();
                        R.Is_Dirty = false;
                    }
                }
            }
        }

        void Edit(T Item)
        {
            if (Item == null) return;
            if (Inline_Editor == null)
            {
                var D = Item.New_Editor();
                D.Indexer = this;
                D.ShowDialog(Editor_Close);
            }
            else
            {
                var D = Inline_Editor;
                D.Go_To(Item);
            }
        }

        void Add_Row (T Row)
        {
            Page.Bind(_LV.Items.Add(new ListViewItem { Tag = Row }));
        }

        public void Bind ()
        {
            _LV.Items.Clear();
            if (Page.List == null) return;
            Prepare(true, true);
            foreach (T DR in Page.List)
            {
                Add_Row(DR);
            }
        }

        public int Item_Count
        {
            get { return _LV.Items.Count; }
        }

        public int Item_Index (object Row)
        {
            foreach (ListViewItem I in _LV.Items)
            {
                if (I.Tag == Row) return I.Index;
            }
            return -1;
        }

        public object Item(int Index)
        {
            if (Index < 0 || Index >= _LV.Items.Count) return null;
            return _LV.Items[Index].Tag;
        }
    }

    public interface I_Editable
    {
        WAF.UI.Edit.Item_Editor New_Editor();
    }

    public interface I_Grid<T> where T : JRO_Row, new()
    {
        JRO_List<T> List { get; }
        void Refresh_All();
        T New_Row { get; }
        void Bind(ListViewItem Item);
    }
}
