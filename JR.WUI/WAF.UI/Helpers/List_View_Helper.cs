using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using Gizmox.WebGUI.Forms;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace WAF.UI
{

    public class ListViewSub_Value : ListViewItem.ListViewSubItem
    {
        string Get_Text(object Value, string Format)
        {
            if (Value is DateTime && (DateTime)Value == DateTime.MinValue) return "-";
            if (Format == "") return Value.ToString();
            return string.Format(Format, Value);
        }
        public ListViewSub_Value(object Value, string Format)
        {
            this.Value = Value;
            this.Text = Get_Text(Value, Format);
        }
        public ListViewSub_Value(object Value)
            : this(Value, "")
        {
        }
        public object Value;
    }

    public class List_View_Sorter : IComparer
    {
        ListView _LV;
        int _Col;
        bool _Desc = false;
        CaseInsensitiveComparer _Comparer = new CaseInsensitiveComparer();
        public List_View_Sorter(ListView objListView)
        {
            _LV = objListView;
            _LV.ColumnClick += new ColumnClickEventHandler(_LV_ColumnClick);
        }

        void _LV_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            _Col = e.Column;
            _Desc = _LV.Columns[_Col].SortOrder == SortOrder.Descending;
            _LV.Sort();
        }

        #region IComparer Membres

        public int Compare(object x, object y)
        {
            int Result = 0;
            ListViewItem Lx = x as ListViewItem;
            ListViewItem Ly = y as ListViewItem;
            ListViewItem.ListViewSubItem Lsx = Lx.SubItems[_Col] as ListViewItem.ListViewSubItem;
            ListViewItem.ListViewSubItem Lsy = Ly.SubItems[_Col] as ListViewItem.ListViewSubItem;
            if (Lsx is ListViewSub_Value)
            {
                ListViewSub_Value Ldsx = Lsx as ListViewSub_Value;
                ListViewSub_Value Ldsy = Lsy as ListViewSub_Value;
                Result = _Comparer.Compare(Ldsx.Value, Ldsy.Value);
            }
            else
            {
                Result = _Comparer.Compare(Lsx.Text, Lsy.Text);
            }
            return _Desc ? -Result : Result;
        }
        #endregion
    }


    public class List_View_Helper<T>:I_List_Selection<T>
    {
        ListView _LV;
        public List_View_Helper(ListView List)
        {
            _LV = List;
            if (_LV == null) return;
            _LV.GridLines = true;
            _LV.UseInternalPaging = true;
        }

        public void Prepare(bool Autosize, bool Sort)
        {
            Prepare(Autosize, Sort, 0);
        }
        public void Prepare (bool Autosize, bool Sort, int Page_Size)
        {
            if (_LV == null) return;
            if (Sort)
            {
                _LV.ListViewItemSorter = new List_View_Sorter(_LV);
            }
            if (Autosize && (_LV.Items.Count > 0))
            {
                for (int I = 1; I < _LV.Columns.Count; I++)
                {
                    if (_LV.Columns[I].Type == ListViewColumnType.Text)
                    {
                        int Init_Size = _LV.Columns[I].Width;
                        _LV.AutoResizeColumn(I, ColumnHeaderAutoResizeStyle.ColumnContent);
                        int New_Size = _LV.Columns[I].Width;
                        if (New_Size < Init_Size) _LV.Columns[I].Width = Init_Size;
                    }
                }
            }
            if (Page_Size == 0)
            {
                if (_LV.Items.Count > 1000)
                {
                    _LV.ItemsPerPage = 30;      // Pages de petite taille pour �viter d'avoir ascenseur + pagination
                }
                else
                {
                    _LV.ItemsPerPage = 1000;      // Pas de pagination, donc seulement ascenceur
                }
            }
            else
            {
               _LV.ItemsPerPage = Page_Size;      // Pages de petite taille pour bien voir qu'il manque des �l�ments
            }
        }

        public Gizmox.WebGUI.Forms.Control List
        {
            get { return _LV; }
        }

        public bool Select_Next()
        {
            if (_LV == null) return false;
            if (!Has_Next) return false;
            _LV.SelectedIndex++;
            //_LV.EnsureVisible(_LV.SelectedIndex);
            if (After_Selected_Change != null) After_Selected_Change();
            return true;
        }

        public bool Select_Previous()
        {
            if (_LV == null) return false;
            if (!Has_Previous) return false;
            _LV.SelectedIndex--;
            if (After_Selected_Change != null) After_Selected_Change();
            //_LV.EnsureVisible(_LV.SelectedIndex);
            return true;
        }

        public bool Has_Previous
        {
            get { return (_LV != null) && _LV.SelectedIndex > 0; }
        }

        public bool Has_Next
        {
            get { return (_LV != null) && _LV.SelectedIndex < _LV.Items.Count - 1; }
        }


        #region I_List_Selection<T> Membres

        public delegate T Get_Selection();
        public Get_Selection Current_Selection = null;


        public delegate void Selection_Change();
        public Selection_Change After_Selected_Change = null;

        public delegate void Selected_Refresh ();
        public Selected_Refresh On_Selected_Refresh = null;

        public T Selected
        {
            get { return (Current_Selection == null)?default(T):Current_Selection (); }
        }

        public void Refresh_Selected()
        {
            if (On_Selected_Refresh != null) On_Selected_Refresh();
        }

        #endregion
    }
}
