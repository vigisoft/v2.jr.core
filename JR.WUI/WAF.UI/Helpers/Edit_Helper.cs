using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using Gizmox.WebGUI.Forms;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Common.Resources;
using JR.DB;

namespace WAF.UI.Edit
{

    public class Tab_Helper
    {
        public I_Tab Tab { get; set; }

        public Tab_Helper(I_Tab Tab)
        {
            this.Tab = Tab;
            Bind();
        }

        public UserControl Control
        {
            get
            {
                return Tab as UserControl;
            }
        }

        public int Save_Count = 0;
        bool _Modif = false;
        internal bool Modif
        {
            get { return _Modif; }
            set
            {
                _Modif = value;
                Animate_Buttons();
                Tab.Treat_Modif(_Modif);
            }
        }

        bool _Is_New = false;
        internal bool Is_New
        {
            get { return _Is_New; }
            set
            {
                _Is_New = value;
                Modif = false;
            }
        }

        void Animate_Buttons()
        {
            bool Can_Move = !(Modif || Is_New);
            Enable(Modif, B_Cancel);
            //Enable(!Modif, B_Close);
            //Enable(!Can_Move, B_Save);
        }

        void Enable(bool State, params Control [] Buttons)
        {
            Action<Control> Enable = (C) =>
                {
                    Button B = C as Button;
                    if (B == null) return;
                    B.Enabled = State;
                    if (B.Image == null) return;
                    string File = B.Image.File;
                    File = File.Replace("-Disable.png", ".png");
                    if (!State) File = File.Replace(".png", "-Disable.png");
                    if (B.Image.File == File) return;
                    B.Image = new IconResourceHandle (File);
                };
            foreach (Control C in Buttons)
            {
                Enable(C);
            }
        }

        public void On_Modif(object sender, EventArgs e)
        {
            Modif = true;
        }

        public void Clear_Modif()
        {
            Modif = false;
        }

        Control B_Close, B_Save, B_Valid, B_Cancel;

        public bool Can_Quit (Action After_Quit)
        {
            Action <object, EventArgs> On_Quit = (O,e) =>
                {
                    Form F = O as Form;
                    switch (F.DialogResult)
                    {
                        case DialogResult.Yes:
                            if (!Tab.Write ()) return;
                            break;
                        case DialogResult.No:
                            break;
                        default:
                            return;
                    }
                    After_Quit();
                };
            if (!Modif) return true;
            MessageBox.Show("Voulez vous enregistrer vos modifications ?", "Modifications non enregistrées",
                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation, new EventHandler(On_Quit));
            return false;
        }

        private void B_Close_Click(object sender, EventArgs e)
        {
            Control.ParentForm.Close();
        }

        private void B_Cancel_Click(object sender, EventArgs e)
        {
            Tab.Cancel();
            Tab.Redisplay();
        }

        private void B_Valid_Click(object sender, EventArgs e)
        {
            if (!Tab.Write()) return;
            Control.ParentForm.Close();
        }

        private void B_Save_Click(object sender, EventArgs e)
        {
            if (!Tab.Write()) return;
            Modif = false;

            // Prevenir le dialogue parent
            Control C = this.Control.Parent;
            while (C != null)
            {
                Item_Inline_Editor E = C as Item_Inline_Editor;
                if (E != null)
                {
                    E.Valid_Form();
                    break;
                }
                C = C.Parent;
            }
        }

        //void Control_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        //{
        //    e.Cancel = !Can_Quit;
        //}

        void Bind()
        {
            Func<string, Control> Get_Control = (Name) =>
            {
                foreach (var C in Control.Controls.Find(Name, true))
                {
                    return C;
                }

                return new Control();
            };

            B_Close = Get_Control("B_Close");
            B_Save = Get_Control("B_Save");
            B_Cancel = Get_Control("B_Cancel");
            B_Valid = Get_Control("B_Valid");
            B_Close.Click += new EventHandler(B_Close_Click);
            B_Valid.Click += new EventHandler(B_Valid_Click);
            B_Save.Click += new EventHandler(B_Save_Click);
            B_Cancel.Click += new EventHandler(B_Cancel_Click);
            //Control.Validating += new System.ComponentModel.CancelEventHandler(Control_Validating);
        }

    }

    public interface I_Tab
    {
        Tab_Helper Helper { get; }
        object Current_Item { get; }
        bool Write();
        void Cancel();
        void Display();
        void Redisplay();
        void Treat_Modif(bool Modif_State);
    }

    public class Dialog_Helper
    {
        Control D;
        public List<I_Tab> Pages = new List<I_Tab>();
        public Dialog_Helper(Control Form)
        {
            D = Form;
        }

            //D.KeyDown += new KeyEventHandler(D_KeyDown);
            //D.KeyPreview = true;


        public I_Dialog Dialog
        {
            get { return D as I_Dialog; }
        }

        //void D_KeyDown(object sender, KeyEventArgs e)
        //{
        //    switch (e.KeyCode)
        //    {
        //        case Keys.Escape:
        //            //e.SuppressKeyPress = true;
        //            if (!Can_Quit) return;
        //            D.Close();
        //            break;
        //        case Keys.F1:
        //            //if (Write_And_Next()) e.SuppressKeyPress = true;
        //            break;
        //    }
        //}

        public bool Can_Quit (Action After_Quit)
        {
            foreach (I_Tab P in Pages)
            {
                if (!P.Helper.Can_Quit (After_Quit)) return false;
            }
            return true;
        }

        public int Save_Count
        {
            get
            {
                int Count = 0;
                foreach (I_Tab P in Pages)
                {
                    Count += P.Helper.Save_Count;
                }
                return Count;
            }
            set
            {
                foreach (I_Tab P in Pages)
                {
                    P.Helper.Save_Count = value;
                }
            }
        }

    }

    public interface I_Dialog
    {
        Dialog_Helper Helper { get; }
        bool Go_To(object Item);
        void Display_Title();
    }

    public interface I_Indexer
    {
        int Item_Count { get; }
        int Item_Index (object Row);
        object Item(int Index);
    }

}
