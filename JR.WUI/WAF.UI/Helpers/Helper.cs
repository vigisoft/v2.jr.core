using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using Gizmox.WebGUI.Forms;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Common.Resources;

namespace WAF.UI
{
    public static class Helper
    {
        public static void Set_Text(Control Control, string Text)
        {
            if (Control is Label || Control is Button)
            {
                Control.Text = Text.Replace("&", "&&");
            }
            else Control.Text = Text;
        }
    }
}
