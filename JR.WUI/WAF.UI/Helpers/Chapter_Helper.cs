using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using Gizmox.WebGUI.Forms;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Common.Resources;

namespace WAF.UI
{
    public class Chapter_Helper
    {
        public I_Chapter Chapter;
        public string Title;
        public object Context = null;

        Type _Chapter_Type;
        public Chapter_Helper(string Title, Type Chapter_Type, object Context)
        {
            this.Context = Context;
            _Chapter_Type = Chapter_Type;
            this.Title = Title;
        }

        public Control Create_Page()
        {
            Bar_Tools.Clear();
            Status_Tools.Clear();
            Chapter = Activator.CreateInstance(_Chapter_Type, new object [] {this}) as I_Chapter;
            View = Chapter as Control;
            return View;
        }

        public void Finalize_Chapter()
        {
            // Mettre � jour le tab
            Desktop.Set_Current_Tab (Tab);
        }

        public List<ToolStripItem> Bar_Tools = new List<ToolStripItem>();
        public List<ToolStripItem> Status_Tools = new List<ToolStripItem>();
        // Desktop courant
        public I_Desktop Desktop;

        // Vue principale
        public Control View;

        // Onglet de gauche
        public Control Tab;

        public void Add_Excel_Tool(EventHandler Handler)
        {
            Add_Tool_Button ("Exporter vers Excel", "xl.gif", Handler);
        }

        public ToolStripButton Add_Tool_Button(string Name, string Icon, EventHandler Handler)
        {
            ToolStripButton TB = new ToolStripButton(Name, Icon != "" ? new ImageResourceHandle(Icon) : null, Handler);
            Add_Tool(TB);
            return TB;
        }

        public void Add_Tool (ToolStripItem Tool)
        {
            Bar_Tools.Add(Tool);
            //Bar_Tools.Add(new ToolStripSeparator());
        }

        //public void Add_Refresh_Tool(EventHandler Handler)
        //{
        //    Tool_Button Excel_Tool = new Tool_Button("Actualiser", "");
        //    //Excel_Tool.Image = new ImageResourceHandle("xl.gif");
        //    Excel_Tool.Click += Handler;
        //    Tools.Add(Excel_Tool);
        //    Tools.Add(new Tool_Separator());
        //}

        public void Export_To_Excel(ListView List, string File_Name)
        {
            Export_To_Excel(List, File_Name, "");
        }

        // Outil d'exportation Excel
        public void Export_To_Excel(ListView List, string File_Name, string Tag_Title)
        {
            StringBuilder SB = new StringBuilder ();
            bool Append_Id = false;
            if (Tag_Title != "")
            {
                Append_Id = true;
                SB.Append(Tag_Title);
                SB.Append('\t');
            }
            foreach (ColumnHeader CH in List.Columns)
            {
                string Title = CH.Text;
                if (Title == "")
                {
                    if (CH.Tag is string)
                    {
                        Title = CH.Tag.ToString();
                    } else if (CH.Tag is JR.I_Public_Ident)
                    {
                        Title = ((JR.I_Public_Ident)CH.Tag).Public_Name;
                    }
                }
                SB.Append(Title);
                SB.Append('\t');
            }
            SB.AppendLine();
            foreach (ListViewItem LI in List.Items)
            {
                if (Append_Id)
                {
                    if (LI.Tag != null)
                    {
                        if (LI.Tag is JR.I_Public_Ident)
                        {
                            SB.Append(((JR.I_Public_Ident)LI.Tag).Public_Id);
                        }
                        else
                        {
                            SB.Append(LI.Tag.ToString());
                        }
                    }
                    SB.Append('\t');
                }
                foreach (ListViewItem.ListViewSubItem SI in LI.SubItems)
                {
                    SB.Append(SI.Text);
                    SB.Append('\t');
                }
                SB.AppendLine();
            }
            Desktop.Download_Bytes(Encoding.Default.GetBytes(SB.ToString ()),"application/vnd.ms-excel", File_Name);

        }
    }
}
