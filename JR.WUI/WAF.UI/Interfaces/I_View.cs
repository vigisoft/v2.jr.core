using System;
using System.Collections.Generic;
using System.Text;

namespace WAF.UI
{
    // Interface d'un bureau
    public interface I_View
    {
        // Appel� suite � changement de contexte global
        void After_Context_Change(string Event, params object[] Params);

        // Appel� sur changement de filtre
        void Apply_Filter();

        // Appel� au chargement
        void First_Display();

        // Appel� au rafraichissement
        void Refresh_Display();

        // Appel� � l'effacement
        void Cleanup();

        // Exporte le contenu
        void Export_Content();

    }
}
