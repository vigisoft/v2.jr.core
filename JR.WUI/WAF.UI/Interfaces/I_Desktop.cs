using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Interfaces;

namespace WAF.UI
{
    // Interface d'un bureau
    public interface I_Desktop : IGatewayComponent,IRegisteredComponent
    {
        // Barre d'outils
        ToolStrip Tool_Strip { get; }

        // Menu principal
        MainMenu Menu{get;}

        // Barre de status
        StatusStrip Status_Bar{get;}

        // Treeview
        TreeView Nav_Tree { get; }

        // Panneau principal
        Control Current_Panel { get;}
        void Set_Current_Panel (Control Control);

        // Panneau de gauche
        Control Current_Tab { get;}
        void Set_Current_Tab(Control Control);

        // Form principal
        Form Form { get;}

        // Outils g�n�raux
        List<ToolStripItem> Tool_Items { get; }

        // Infos status g�n�rales
        List<ToolStripItem> Status_Items { get; }

        // Appel� � la fermeture de session
        void Cleanup();

        // Chapitre courant
        Chapter_Helper Chapter { get;set;}

        // Titre courant
        string Title { get; set; }
        // Notification de changement de contexte
        void Change_Context (string Event, params object [] Params);

        // T�l�chargement d'un fichier / d'un contenu
        bool Download_Bytes(byte [] Content, string Content_Type, string Dest_Name);

        // T�l�chargement d'un fichier / d'un contenu
        bool Download_File (string Source_Name, string Content_Type, string Dest_Name);

        // Retourne une action en attente (commande initiale par exemple)
        object Pop_Action();
        string Pending_Action { get; }

    }
}
