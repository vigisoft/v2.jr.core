using System;
using System.Collections.Generic;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Gateways;

namespace JR.WUI
{
	/// <summary>
	/// Classe de gestion de la page principale (portail)
	/// </summary>

	public class Downloader
	{
        public byte[] Bytes;
        public string Dest_File_Type;
        public string Dest_File_Name;
        public static bool Download_Bytes(byte[] Content, string Content_Type, string Dest_Name)
        {
            if (Content.Length == 0) return false;
            VWGContext.Current.Session["Pending_Download"] = new Downloader { Bytes = Content, Dest_File_Type = (Content_Type == "" ? "application/octet-stream" : Content_Type), Dest_File_Name = Dest_Name };
            LinkParameters Lp = new LinkParameters();
            Lp.Target = "_self";
            Link.Open(new GatewayReference(VWGContext.Current.MainForm, "DOWNLOAD"), Lp);
            return true;
        }

        public static void Treat_Download (Gizmox.WebGUI.Common.Interfaces.IContext Context)
        {
            var D = VWGContext.Current.Session["Pending_Download"] as Downloader;
            if (D == null) return;
            HttpResponse Response = Context.HttpContext.Response;
            Response.ClearHeaders();
            Response.ContentType = D.Dest_File_Type;
            Response.AppendHeader("Content-disposition", "attachment;filename=\"" + D.Dest_File_Name + "\"");
            Response.OutputStream.Write(D.Bytes, 0, D.Bytes.Length);
            Response.End();
        }

        // TÚlÚchargement d'un fichier / d'un contenu
        public static bool Download_File (string Source_Name, string Content_Type, string Dest_Name)
        {
            if (!System.IO.File.Exists(Source_Name)) return false;
            return Download_Bytes(System.IO.File.ReadAllBytes(Source_Name), Content_Type, Dest_Name);
        }

    }
}
