using System;
using System.Collections.Generic;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Gateways;

namespace JR.WUI
{
	/// <summary>
	/// Classe de gestion de la page principale (portail)
	/// </summary>

	public static class Gateway
	{

        public static bool Process(Gizmox.WebGUI.Common.Interfaces.IContext Context, string Action)
        {
            var C = Action.Scan();
            switch (C.Next("_"))
            {
                case "DOWNLOAD":
                    {
                        Downloader.Treat_Download(Context);
                        return true;
                    }
                case "TEMPPAGE":
                    {
                        Temporary_Page.Treat_Temp_Page(Context, C.Tail());
                        return true;
                    }
            }
            return false;
        }
    }
}
