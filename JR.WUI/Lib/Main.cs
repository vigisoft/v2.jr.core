using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Web;
using System.Web.Util;
using System.Data;
using Gizmox.WebGUI.Forms;

namespace JR.WUI
{

	/// <summary>
	/// Version all�g�e du Framework sp�cifique WEB
    /// Supprime des notions incompatibles, comme la langue
    ///  ou le nom d'application unique
	/// </summary>
	public class Main:JR.Saf.Main
	{

		// Charge les espaces et les chemins
        // Charge les fichiers de configuration
        public static bool Register(string Version, string Vendor, string Trigram)
        {
            Solver += Web_Solver;
            Before_Load_Config += Main_Before_Load_Config;
            string Root = HttpContext.Current == null ? "" : HttpContext.Current.Server.MapPath("~");
            if (Root.Length > 2 && Root[Root.Length - 1] == '\\') Root = Root.Remove(Root.Length - 1);
            return Register_Software(Version, Vendor, Root, Trigram);
        }

        static void Main_Before_Load_Config(object sender, EventArgs e)
        {
            // Charger le dico par d�faut
            UI_Dico.Load();
        }

        // Chemin racine du site
        public static string Site_Path (string File_Name)
        {
            return Base_Path(File_Name);
        }

        public static void Restart_Web_Server()
        {
            JR.OS.Kill_Program();
            System.IO.File.SetCreationTimeUtc(Site_Path("web.config"), DateTime.UtcNow);
            System.IO.File.SetLastWriteTimeUtc(Site_Path("web.config"), DateTime.UtcNow);
        }

        static string Web_Solver(string Key, string Param)
        {
            switch (JR.Strings.Official(Key))
            {
                case "WLW":  // Retourne le local web file
                    {
                        try
                        {
                            HttpContext H = HttpContext.Current;
                            string File_Name = H.Server.MapPath(Param);
                            if (!File.Exists(File_Name)) return "";
                            return File.ReadAllText(File_Name);
                        }
                        catch { }
                        return "";
                    }
                case "HTML":  // Retourne le texte converti en HTML
                    {
                        return HttpUtility.HtmlEncode(Param);
                    }
                case "LMTH":  // Retourne le HTML converti en text
                    {
                        return HttpUtility.HtmlDecode(Param);
                    }
            }
            return null;
        }

        public static string Encode_Url_Param(string Param)
        {
            if (Param.IndexOfAny("&@".ToCharArray()) < 0) return Param;
            return Param.Replace("&", "0z26").Replace("@", "0z64");
        }

        public static string Decode_Url_Param(string Param)
        {
            if (Param.IndexOf("0z") < 0) return Param;
            return Param.Replace("0z26", "&").Replace("0z64", "@");
        }

        public static string Js(string Source)
        {
            if (Source == null) return "";
            return Source.Replace("'", @"\'").Replace(@"""", @"\""");
        }

        public string Get_Arg(string Key)
        {
            if (VWGContext.Current.Arguments[Key] != null) return VWGContext.Current.Arguments[Key].ToString();
            return "";
        }

        // Retourne le chemin virtuel de l'application
        // /admin par exemple
        // Attention dans une m�me application le chemin peut �tre diff�rent (cas de admin.smmof.fr equivalent a www.smmof.fr/admin)
        public static string Web_Root
        {
            get
            {
                string App_Path = HttpContext.Current.Request.ApplicationPath;
                return App_Path == "/" ? "" : App_Path;
            }
        }

        // Ajoute devant l'URL le chemin �ventuel de l'application
        // /admin par exemple
        public static string Web_URL(string URL)
        {
            return Web_Root + URL;
        }

        public static string Resource_URL(string Full_Path)
        {
            if (Full_Path == "") return "";
            string File_Name = Path.GetFileName(Full_Path);
            string Dir_Name = HttpUtility.UrlEncode(JR.Files.Get_Directory_Name(Full_Path));
            string App_Path = HttpContext.Current.Request.ApplicationPath;
            return Web_URL(string.Format("/res/{0}?P={1}", File_Name, Dir_Name));
        }

	}
}



 