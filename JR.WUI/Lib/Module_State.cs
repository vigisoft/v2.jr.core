using System;
using System.Collections.Generic;
using System.Web.SessionState;
using System.Web;
using Gizmox.WebGUI.Common.Interfaces;
using Gizmox.WebGUI.Forms;
using System.Text;

namespace JR.WUI
{
	/// <summary>
	/// Classe de gestion de la page principale (portail)
	/// </summary>

    public interface I_Module : IControl
    {
        Module_State Module { get;}
        void Event_Receive (string Event_Name, params object[] Params);
    }

	public class Module_State
	{
        static long Id_Generator = 0;
        public long Id;
        public string Key {get;set;}
        public I_Module Module;
        public I_Module Parent;
        public List<I_Module> Children = new List<I_Module>();
        public Form Form
        {
            get { return Parent as Form; }
        }
        public Session_State Session { get; private set; }
        public Module_State(string Key, I_Module Module)
        {
            Spy_Events = false;
            Id_Generator++;
            Id = Id_Generator;
            this.Key = Key;
            this.Module = Module;
            Session = Session_State.Current;
            Session.Register(Module);

            // Rattachement des modules entre eux
            if (Module is Form)
            {
                // Callback de fermeture
                Application.ThreadSuspend += new EventHandler(Application_ThreadSuspend);
                Parent = Module;
            }
            else
            {
                I_Module Top = VWGContext.Current.ActiveForm.Context.MainForm as I_Module;
                if (Top == null) throw new Exception(string.Format("Le Module {0} n'a pas de Form enregistr�", Key));
                Parent = Top;
                Top.Module.Children.Add(Module);
            }
        }

        void Application_ThreadSuspend(object sender, EventArgs e)
        {
            Exit();
        }

        // Envoi d'un signal � tous les modules de la fenetre et terminer par moi
        public void Raise(bool Me, string Event_Name, params object[] Params)
        {
            foreach (I_Module M in Parent.Module.Children)
            {
                if (M == this) continue;
                M.Module.Send(Event_Name, Params);
            }
            if (Me)
            {
                Send(Event_Name, Params);
            }
        }

        // Envoi d'un signal � tous les modules enfants et terminer par moi
        public void Raise_Down(string Event_Name, params object[] Params)
        {
            foreach (I_Module M in Children)
            {
                M.Module.Send(Event_Name, Params);
            }
            Send(Event_Name, Params);
        }

        public void Refresh(bool Me)
        {
            Raise(Me, Module_Event.Refresh);
        }

        public void Exit()
        {
            foreach (I_Module M in Children)
            {
                M.Module.Exit();
            }
            Send(Module_Event.Exit);
            Parent.Module.Children.Remove(this.Module);
        }

        public bool Spy_Events {get;set;}

        void Send (string Event_Name, params object[] Params)
        {
            if (Spy_Events)
            {
                StringBuilder SB = new StringBuilder();
                SB.AppendFormat("{0}-{1}-{2}", Key, Id, Event_Name);
                foreach (var P in Params)
                {
                    SB.AppendLine (" - " + P.ToString());
                }
                System.Diagnostics.Debug.WriteLine(SB.ToString());
            }
            Module.Event_Receive(Event_Name, Params);
        }

    }

    public class Module_Event
    {
        public const string Init = "@INIT";
        public const string Refresh = "@REFRESH";
        public const string Exit = "@EXIT";
    }
}
