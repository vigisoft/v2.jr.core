﻿using System;
using System.Web;

using Gizmox.WebGUI;
using Gizmox.WebGUI.Common.Interfaces;
using Gizmox.WebGUI.Hosting;
using Gizmox.WebGUI.Server;

// To register your custom router in web.config, you will replace the standard handlers or httpHandlers registration with the
// following, depending on if you are using integrated mode pipeline (handlers) or classic mode pipeline (httpHandlers) on
// your application pool on your deployed application.
//      <httpHandlers>
//        <add verb="*" path="*.wgx" type="VWGSupportIE11.IE11Router,VWGSupportIE11" />
//      </httpHandlers>
//      <handlers>
//        <add name="wgx" verb="*" path="*.wgx" type="VWGSupportIE11.IE11Router,VWGSupportIE11" />
//      </handlers>
//
namespace JR.WUI
{
    public class IE11Router : Gizmox.WebGUI.Server.Router
    {
        protected override HostHttpHandler GetHttpHandler(HostContext objHostContext, Context objContext)
        {
            // Get the current request type
            RequestType enmRequestType = ((IRequestParams)objContext.Request).Type;
            HostHttpHandler objHandler = base.GetHttpHandler(objHostContext, objContext);
            if (enmRequestType == RequestType.Preload)
            {
                objHandler = new IE11Preload(objContext);
            }
            return objHandler;
        }
    }

    public class IE11Preload : Gizmox.WebGUI.Server.Preload
    {
        public IE11Preload(Context objContext)
            : base(objContext)
        {
        }

        protected override bool IsSupportedBrowser(HostContext objHostContext)
        {
            bool blnIsSupported = base.IsSupportedBrowser(objHostContext);
            if (!blnIsSupported)
            {
                if (!CommonUtils.IsNullOrEmpty(objHostContext.Request.UserAgent))
                {
                    // Adjust IE11 detection strings at will. This example is based on:
                    // http://blogs.msdn.com/b/ieinternals/archive/2013/09/21/internet-explorer-11-user-agent-string-ua-string-sniffing-compatibility-with-gecko-webkit.aspx
                    if (objHostContext.Request.UserAgent.IndexOf("Windows NT") != -1
                        && objHostContext.Request.UserAgent.IndexOf("rv:11.0") != -1)
                        blnIsSupported = true; // IE11
                }
            }
            return blnIsSupported;
        }
    }
}