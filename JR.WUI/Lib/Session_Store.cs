using System;
using System.Collections.Generic;
using System.Web.SessionState;
using System.Web;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Interfaces;

namespace JR.WUI
{
	/// <summary>
	/// Stockage de donn�es dans les cookies
	/// </summary>

	public class Session_Store
	{
		public Session_Store (IControl Control = null)
		{
            Unique_Ref = UR(Control);
        }

        static string UR(IControl Control)
        {
            if (Control == null) Control = VWGContext.Current.MainForm;
            return string.Format("WUI.{2}.{1}.{0}", Control.Name, Control.GetType(), Main.App_Name);
        }

        public string Unique_Ref {get;set;}

        public string this[string Key]
        {
            get { return VWGContext.Current.Cookies[string.Format("{0}.{1}", Unique_Ref, Key)] ?? ""; }
            set { VWGContext.Current.Cookies[string.Format("{0}.{1}", Unique_Ref, Key)] = value; }
        }

        public int Int(string Key)
        {
            return JR.Convert.To_Integer(this[Key]);
        }

        public bool Bool(string Key)
        {
            return JR.Convert.To_Boolean(this[Key]);
        }

        public double Float(string Key)
        {
            return JR.Convert.To_Float(this[Key]);
        }

        public DateTime Date(string Key)
        {
            return JR.Convert.To_Time(this[Key]);
        }

        public long Long(string Key)
        {
            return JR.Convert.To_Long(this[Key]);
        }

        public void Set(string Key, object Value)
        {
            this[Key] = JR.Convert.To_String (Value);
        }

        public static Session_Store Global
        {
            get { return new Session_Store(); }
        }
    }
    public static class SS_Extension
    {
        public static Session_Store Store(this IControl Control)
        {
            return new Session_Store(Control);
        }
    }

}
