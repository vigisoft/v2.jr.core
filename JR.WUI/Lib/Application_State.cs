using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using System.Resources;
using System.Configuration;
using System.Data;
using JR;
using System.Xml;
using System.IO;
using System.Threading;
using Gizmox.WebGUI.Common.Resources;
using Gizmox.WebGUI.Common.Interfaces;
using System.Text;


namespace JR.WUI
{

    /// <summary>
    /// Classe de gestion du projet principal
    /// </summary>
    public class Application_State
    {

        List<Session_State> _Session_States = new List<Session_State>();						// Liste des portails ouverts
        static Application_State _Current = null;

        public Application_State(string Version, string Vendor, string Trigram = "WEB")
        {

            // Enregistrer le static gateway qui retourne le script d'init du main form
            var Static_Handle = new StaticGatewayResourceHandle("VWG.Post_Init", typeof(Post_Init));

            _Current = this;
            Main.Before_Load_Config += new EventHandler(Main_Before_Load_Config);
            Main.After_Load_Config += new EventHandler(Main_After_Load_Config);
            Main.Register(Version, Vendor, Trigram);

            // Signaler le d�marrage de l'application
            Logs.Log(Main.Try_Get_String("APP.STARTING"));
        }

        void Main_After_Load_Config(object sender, EventArgs e)
        {
            After_Load_Config();
        }

        void Main_Before_Load_Config(object sender, EventArgs e)
        {
            Before_Load_Config();
        }

        public Application_State()
            : this("1.0.1", "Vigisoft")
        {
        }

        DateTime _Start_Time = DateTime.Now;
        public DateTime Start_Time
        {
            get { return _Start_Time; }
        }

        // Methode � sp�cialiser pour g�rer la fin de l'application
        public virtual void On_Remove()
        {
        }

        public static void Release_Current()
        {
            if (_Current == null) return;
            _Current.On_Remove();
            _Current = null;
        }

        public Application_State End()
        {
            Release_Current();
            return null;
        }

        public List<Session_State> Session_States
        {
            get { return _Session_States; }
        }

        public static Application_State Current 			// Retrouve le dernier projet courant
        {
            get { return _Current; }
            set { _Current = value; }
        }

        // M�thode � sp�cialiser pour retourner une session
        public virtual Session_State On_New_Session()
        {
            return new Session_State();
        }

        public void Create_Session(HttpSessionState Session)
        {
            Session_State Sess = On_New_Session();
            _Session_States.Add(Sess);
            Session["JR.WEB.SESSION"] = Sess;
        }

        public void Release_Session(HttpSessionState Session)
        {
            Session_State Sess = (Session_State)Session["JR.WEB.SESSION"];
            if (Sess == null) return;
            Sess.Clear();
            Session["JR.WEB.SESSION"] = null;
            _Session_States.Remove(Sess);
        }

        public void Restart_Application()
        {
            string Web_Config = HttpContext.Current.Server.MapPath("~/web.config");
            try
            {
                File.SetLastWriteTime(Web_Config, DateTime.Now);
            }
            catch { };
        }

        // Declench� avant lecture des fichiers de configuration
        public virtual void Before_Load_Config()
        {
        }

        // Declench� apr�s lecture des fichiers de configuration
        public virtual void After_Load_Config()
        {
        }

        public static string Modifieur
        {
            get { return User_State.Current.Name; }
        }

        public void Error(HttpContext Context)
        {
            // Code qui s'ex�cute lorsqu'une erreur non g�r�e se produit
            Logs.Err("Erreur globale application [global.asax]");
            if (Context.Request != null)
            {
                Logs.Err(Context.Request.Url.OriginalString);
            }
            Logs.Err(Context.Error.Message);
            Logs.Err(Context.Error.Source);
            Logs.Err(Context.Error.StackTrace);

            if (Context.Error.InnerException != null)
            {
                Logs.Err("Exception interne");
                Logs.Err(Context.Error.InnerException.Message);
                Logs.Err(Context.Error.InnerException.Source);
                Logs.Err(Context.Error.InnerException.StackTrace);
            }
        }

        public virtual void Load_Post_Init(Init_Scripter S)
        {
            S.Load_Css("~/vwg/bootstrap4.min.css");
            S.Load_Css("~/vwg/fa/css/all.min.css");
            S.Load_Css("~/vwg/bs.css?v=@STAMP");
            S.Load_Script("~/vwg/bootstrap4.min.js");
            S.Load_Script("~/vwg/cn.js?v=@STAMP");
        }

        public class Post_Init : IStaticGateway
        {
            public IStaticGatewayHandler GetGatewayHandler(IContext objContext)
            {
                objContext.HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                objContext.HttpContext.Response.ContentType = "text/javascript";
                var S = new Init_Scripter();
                Application_State.Current.Load_Post_Init(S);
                objContext.HttpContext.Response.Write(S.Content);
                return null;
            }
        }

    }

    public class Init_Scripter
    {
        StringBuilder Script_Content = new StringBuilder();
        public void Append(string Script, params object[] Params)
        {
            if (Params.Length == 0)
            {
                Script_Content.AppendLine(Script);
            }
            else
            {
                Script_Content.AppendLine(string.Format(Script, Params));
            }
        }
        public static string To_URL (string Value)
        {
            return Value.Replace("@STAMP", Main.Stamp).Replace("~/", "../../../../../../../../");
        }
        public void Load_Css(string URL)
        {
            string Code = "{{var Tag=document.createElement('link');Tag.rel='stylesheet';Tag.type='text/css';Tag.href='"
                + To_URL (URL) + "';document.getElementsByTagName('head')[0].appendChild(Tag);}}";
            Append(Code);
        }
        public void Load_Script(string URL)
        {
            string Code = "$.ajax({url: '" + To_URL(URL) + "',dataType:'script',cache:true});";
            Append(Code);
        }
        public string Content
        {
            get { return Script_Content.ToString(); }
        }
    }

}



