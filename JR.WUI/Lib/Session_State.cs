using System;
using System.Collections.Generic;
using System.Web.SessionState;
using System.Web;
using Gizmox.WebGUI.Forms;
using JR.WUI.WAF;
using Gizmox.WebGUI.Common.Interfaces;

namespace JR.WUI
{
	/// <summary>
	/// Classe de gestion de la page principale (portail)
	/// </summary>

	public class Session_State
	{
		public Session_State ()
		{
            HttpContext H = HttpContext.Current;
            H.Session["JR.WEB.SESSION"] = this;
        }

        // Adresse IP de d�tenteur de session
        string _Host_Address = "";
        public string Host_Address
		{
			get
            {
                if (_Host_Address == "") _Host_Address = HttpContext.Current.Request.UserHostAddress;
                return _Host_Address;
            }
		}

        public string Http_Host
        {
            get
            {
                return HttpContext.Current.Request.ServerVariables["HTTP_HOST"];
            }
        }


		public virtual void On_Clear ()
		{
		}
        public virtual void On_Connect()
        {
        }

        public virtual void On_Disconnect()
        {
        }


		// Nettoie les ressources du portail avant fermeture
		public void Clear ()
		{
			On_Clear ();
		}

		public static Session_State Current
		{
            get
            {
                Session_State S = (Session_State)HttpContext.Current.Session["JR.WEB.SESSION"];
                return S;
            }
		}

        // M�thode � sp�cialiser pour retourner un utilisateur
        public virtual User_State On_New_User(Session_State Session)
        {
            return new User_State(Session);
        }

        User_State _User = null;	// Utilisateur courant
        public User_State User
		{
			get
            {
                if (_User == null)
                {
                    _User = On_New_User(this);
                }
                return _User;
            }
		}

        string _Domain = "";   // Domaine de la demande initiale
        public string Domain
        {
            get
            {
                if (_Domain == "")
                {
                    _Domain = HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
                }
                return _Domain;
            }
        }

		// Ressources de la session, accessible sous forme d'un indexer
		Dictionary<string, object> _Depot = new Dictionary<string,object>();
        public object this[string Index]
        {
            get
            {
                object Value = null;
                _Depot.TryGetValue(Index, out Value);
                return Value;
            }

            set
            {
                if (value == null)
                {
                    _Depot.Remove(Index);
                    return;
                }
                _Depot [Index] = value;
            }
        }


        // Gestion de la langue
        string _Language = "FR";
        public string Language
        {
            get { return _Language; }
            set { _Language = value.ToUpper(); }
        }

        // Gestion de la position de navigation
        string _Navigation = "";
        public string Navigation
        {
            get { return _Navigation; }
            set
            {
                _Navigation = value; 
            }
        }

        public virtual void Navigate_To(string Context)
        {
            Navigation = Context;
        }

        // Retourne une chaine UI localis�e
        public string Local_Ui(string Key)
        {
            return Main.Get_String(Language, "UI", Key);
        }

        // Retourne une chaine UI localis�e et compatible Javascript
        public string Local_Js_Ui(string Key)
        {
            return Main.Js(Main.Get_String(Language, "UI", Key));
        }

        // Indique � la prochaine vue si elle s'appelle FROM
        // d'aller sur To
        string _Route = "";
        public string Route
        {
            get
            {
                return _Route;
            }
            set { _Route = value; }
        }

        public void Clear_Route()
        {
            _Route = "";
        }

        // DEsktop courant
        //I_Desktop _Desktop = null;

        public static I_Desktop Current_Desktop => VWGContext.Current.MainForm as I_Desktop;
        //public static I_Desktop Desktop
        //{
        //    get { return Current._Desktop; }
        //    set { Current._Desktop = value; }
        //}

        public List<I_Module> Modules = new List<I_Module>();
        public void Register(I_Module Module)
        {
            Modules.Add(Module);
        }

        public void Unregister(I_Module Module)
        {
            Modules.Remove (Module);
        }

        public string Get_Cookie(string Key)
        {
            return VWGContext.Current.Cookies[Key];
        }

        public void Set_Cookie(string Key, string Value, int Days = 300)
        {
            VWGContext.Current.Cookies[Key] = Value;
        }

    }
}
