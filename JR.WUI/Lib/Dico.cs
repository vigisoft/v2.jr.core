﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JR.WUI
{
    public static class UI_Dico
    {
        public static void Add(string Key, string French, string English = "", string Spanish = "")
        {
            Main.Set_String(French, "FR", Key);
            if (English != "") Main.Set_String(English, "EN", Key);
            if (Spanish != "") Main.Set_String(Spanish, "SP", Key);
        }
        public static void Load()
        {
            Add("UI.LOGOUTBOX.OUT", "Vous n'êtes plus identifié", "You are not identified");
            Add("UI.LOGONBOX.ERR", "[[ Identifiants non valides ]]", "[[ Identification failed ]]");
            Add("UI.BUTTON.YES", "Oui", "Yes");
            Add("UI.BUTTON.NO", "Non", "No");
        }
    }
}
