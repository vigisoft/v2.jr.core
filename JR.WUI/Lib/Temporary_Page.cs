using System;
using System.Collections.Generic;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Gateways;
using System.Text;

namespace JR.WUI
{
	/// <summary>
	/// Classe de gestion de la page principale (portail)
	/// </summary>

    public class Temporary_Page
	{

        public static Dictionary<string, Temporary_Page> Pages = new Dictionary<string, Temporary_Page>();

        public DateTime Expiration;
        public string Content;
        public string Content_Type;

        static void Check_Pages ()
        {
            var To_Remove = new List<string>();
            foreach (var T in Pages)
            {
                if (T.Value.Expiration > DateTime.UtcNow) continue;
                To_Remove.Add(T.Key);
            }
            foreach (var T in To_Remove)
            {
                Pages.Remove (T);
            }
        }
        public static string Display_Content (string Content, string Content_Type, int Minutes = 10)
        {
            Check_Pages();
            if (Content.Length == 0) return "";
            var T = new Temporary_Page { Content = Content, Content_Type = Content_Type, Expiration = DateTime.UtcNow.AddMinutes (Minutes)};
            string Key = Guid.NewGuid().ToString();
            Pages[Key] = T;
            if (VWGContext.Current == null)
            {
                return "/VWG/Temp_Page.ashx?K=" + Key;
            } else
            {
                return new GatewayReference(VWGContext.Current.MainForm, "TEMPPAGE_" + Key).ToString();
            }
        }

        public static void Treat_Temp_Page(Gizmox.WebGUI.Common.Interfaces.IContext Context, string Key)
        {
            Temporary_Page T = null;
            if (!Pages.TryGetValue(Key, out T)) return;
            HttpResponse Response = Context.HttpContext.Response;
            Response.ClearHeaders();
            Response.ContentType = T.Content_Type;
            Response.ContentEncoding = Encoding.UTF8;
            Response.Write(T.Content);
            Response.FlushAsync();
            //.End();
        }

        public static void Treat_Temp_Page(HttpResponse Response, string Key)
        {
            Temporary_Page T = null;
            if (!Pages.TryGetValue(Key, out T)) return;
            Response.ClearHeaders();
            Response.ContentType = T.Content_Type;
            Response.ContentEncoding = Encoding.UTF8;
            Response.Write(T.Content);
            Response.FlushAsync();
            //Response.End();
        }

    }
}
