﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JR.DBO;
using Gizmox.WebGUI.Forms;
using System.Data;
using JR.WUI.WAF.Edit;
using JR.WUI;
using System.Text;

namespace JR.WUI
{
    public class JRO_Binder
    {

        public JRO_Binder()
        {
            Lang = "FR";
            Displaying = false;
            On_Change = new EventHandler(OnChange);
        }

        public JRO_Row O { get; set; }
        public Control Parent { get; set; }

        public bool Displaying { get; set; }
        public EventHandler On_Modif;

        public void Load_Text(params Control[] Controls)
        {
            foreach (var C in Controls)
            {
                string Field = C.Name.Substring(2);
                C.TextChanged -= On_Change;
                C.Text = O.String(Field);
                C.TextChanged += On_Change;
            }
        }

        public void Load_Format(Control C, string Format, bool Empty_If_Default = true)
        {
            C.TextChanged -= On_Change;
            C.Text = O.Format(Format, Empty_If_Default);
            C.TextChanged += On_Change;
        }

        public void Load_Html(params CKEditor[] Controls)
        {
            foreach (var C in Controls)
            {
                string Field = C.Name.Substring(2);
                C.TextChanged -= On_Change;
                C.Text = O.String(Field);
                C.TextChanged += On_Change;
            }
        }

        public void Load_Counter(Control C, int Empty_Value = 0)
        {
            string Field = C.Name.Substring(2);
            int Count = O.Int(Field);
            C.TextChanged -= On_Change;
            if (Count == Empty_Value)
            {
                C.Text = "";
            }
            else
            {
                C.Text = Count.ToString();
            }
            C.TextChanged += On_Change;
        }

        public void Load_Prefix(string Prefix, Control C_Prefix, Control C)
        {
            string Field = C.Name.Substring(2);
            C_Prefix.Text = Prefix;
            string Value = O.String(Field);
            C.TextChanged -= On_Change;
            if (Prefix.Length < Value.Length)
            {
                C.Text = Value.Substring(Prefix.Length);
            } else
            {
                C.Text = "";
            }
            C.TextChanged += On_Change;
            C.Tag = Prefix;
        }

        public void Save_Prefix(Control C)
        {
            string Field = C.Name.Substring(2);
            O.Set<string>(Field, C.Tag.ToString() + C.Text);
        }

        EventHandler On_Change;
        void OnChange(object sender, EventArgs e)
        {
            if (Cancel_Modif) return;
            if (On_Modif != null) On_Modif(sender, e);
        }

        public void Save_Text(params Control[] Controls)
        {
            foreach (var C in Controls)
            {
                string Field = C.Name.Substring(2);
                O.Set<string>(Field, C.Text);
            }
        }

        
        public void Save_Text(string Options, params Control[] Controls)
        {
            foreach (var C in Controls)
            {
                string Field = C.Name.Substring(2);
                O.Set<string>(Field, JR.Strings.Treat(C.Text, Options));
            }
        }

        public void Save_Html(params CKEditor[] Controls)
        {
            foreach (var C in Controls)
            {
                string Field = C.Name.Substring(2);
                O.Set<string>(Field, C.Text);
            }
        }

        public void Save_Counter(Control C, int Empty_Value = 0)
        {
            string Field = C.Name.Substring(2);
            bool Error;
            int Value = JR.Convert.To_Integer(C.Text, out Error);
            if (Error || Value == Empty_Value)
            {
                O.Set_Null (Field);
            }
            else
            {
                O.Set<int>(Field, Value);
            }
        }

        public void Save_Date(Control C, string Format)
        {
            string Field = C.Name.Substring(2);
            DateTime Val = JR.Time.Parse(C.Text, Format);
            O.Set<DateTime>(Field, Val);
        }

        public void Load(NumericUpDown Control)
        {
            string Field = Control.Name.Substring(2);
            int V = O.Int(Field);
            Control.ValueChanged -= On_Change;
            try
            {
                Control.Value = V;
            }
            catch { }
            Control.ValueChanged += On_Change;
        }

        public void Load_Flag(CheckBox Control, bool Negate = false)
        {
            string Field = Control.Name.Substring(2);
            Control.CheckedChanged -= On_Change;
            Control.Checked = O.Has_Flag(Field.Replace("__", ":")) ? !Negate : Negate;
            Control.CheckedChanged += On_Change;
        }

        public void Load_Bool(CheckBox Control, bool Value)
        {
            Control.CheckedChanged -= On_Change;
            Control.Checked = Value;
            Control.CheckedChanged += On_Change;
        }

        public void Save_Flag(CheckBox Control, bool Negate = false)
        {
            string Field = Control.Name.Substring(2);
            O.Set_Flag(Control.Checked ? !Negate : Negate, Field.Replace("__", ":"));
        }

        DateTime No_Date_Value = DateTime.Today.AddSeconds(1);

        public void Load(DateTimePicker Control)
        {
            string Field = Control.Name.Substring(2);
            DateTime D = O.Date(Field);
            bool No_Date = D == DateTime.MinValue;
            Control.ValueChanged -= On_Change;
            if (Control.ShowCheckBox) Control.Checked = !No_Date;
            if (No_Date)
            {
                Control.Value = No_Date_Value;
            }
            else
            {
                try
                {
                    Control.Value = D;
                }
                catch { }
            }
            Control.ValueChanged += On_Change;
        }
        public void Save(DateTimePicker Control)
        {
            string Field = Control.Name.Substring(2);
            DateTime New_Value = Control.Value;
            if (Control.ShowCheckBox)
            {
                if (!Control.Checked) New_Value = DateTime.MinValue;
            } else
            {
                if (New_Value == No_Date_Value) New_Value = DateTime.MinValue;
            }
            O.Set<DateTime>(Field, New_Value.Date);
        }
        public void Save(NumericUpDown Control)
        {
            string Field = Control.Name.Substring(2);
            O.Set<int>(Field, (int)Control.Value);
        }

        public void Load(ComboBox Control)
        {
            string Field = Control.Name.Substring(2);
            Control.SelectedIndexChanged -= On_Change;
            Control.Text = O.String(Field);
            Control.SelectedIndexChanged += On_Change;
        }

        public void Load(Control Control, JR.Enums Enums)
        {
            string Field = Control.Name.Substring(2);
            List_Items<string> LM = new List_Items<string>();
            foreach (var A in Enums.List)
            {
                LM.Add(A.Lib_Fr, A.Id);
            }
            if (Control is ComboBox)
            {
                ComboBox C = Control as ComboBox;
                C.SelectedIndexChanged -= On_Change;
                C.Items.Clear();
                C.Items.AddRange(LM.Content);
                C.SelectedIndex = LM.Get_Row(O.String(Field));
                C.SelectedIndexChanged += On_Change;
            }
            else
            {
                Control.Text = Enums.Get_By_Id (O.String(Field)).Lib_Fr;
            }
        }

        public void Load(CheckBox Control)
        {
            string Field = Control.Name.Substring(2);
            bool V = O.Bool(Field);
            Control.CheckedChanged -= On_Change;
            try
            {
                Control.Checked = V;
            }
            catch { }
            Control.CheckedChanged += On_Change;
        }

        public void Save(CheckBox Control)
        {
            string Field = Control.Name.Substring(2);
            O.Set<bool>(Field, Control.Checked);
        }

        public void Save(CheckBox Control, string Checked_Val, string Unchecked_Val)
        {
            string Field = Control.Name.Substring(2);
            O.Set<string>(Field, Control.Checked ? Checked_Val : Unchecked_Val);
        }

        public void Save(ComboBox Control)
        {
            string Field = Control.Name.Substring(2);
            if (Control.DropDownStyle == ComboBoxStyle.DropDown)
            {
                O.Set<string>(Field, Control.Text);
            }
            else
            {
                O.Set<string>(Field, List_Item<string>.Get_Id(Control.SelectedItem));
            }
        }

        List<TextBox> Lang_Box = new List<TextBox>();

        bool Cancel_Modif = false;
        public string Lang {get;set;}
        public void Switch_Lang(string Lang)
        {
            this.Lang = Lang;
            Cancel_Modif = true;
            foreach (var B in Lang_Box)
            {

                //B.Sel_Value(Lang);
            }
            Cancel_Modif = false;
        }


        public void Save_Parent(ComboBox Control)
        {
            O.Parent_Id = List_Item<Guid>.Get_Id(Control.SelectedItem);
        }

        public void Save_Relation(ComboBox Control)
        {
            string Field = "R#" + Control.Name.Substring(2);
            O.Set<Guid>(Field, List_Item<Guid>.Get_Id(Control.SelectedItem));
        }

        public void Load_Relation(ComboBox Control, DataTable Table, bool Accept_Empty = true)
        {
            string Field = "R#" + Control.Name.Substring(2);
            List_Items<Guid> LM = new List_Items<Guid>();
            if (Accept_Empty) LM.Add("-", Guid.Empty);
            foreach (DataRow A in Table.Rows)
            {
                LM.Add(A[1].ToString(), A.Field<Guid>(0));
            }
            Control.SelectedIndexChanged -= On_Change;
            Control.Items.Clear();
            Control.Items.AddRange(LM.Content);
            if (LM.Count == 1)
            {
                Control.SelectedIndex = 0;
                Control.Enabled = false;
            } else
            {
                Control.Enabled = true;
                Control.SelectedIndex = LM.Get_Row(O.Guid(Field));
            }
            Control.SelectedIndexChanged += On_Change;
        }

        public void Load_Relations(CheckedListBox Control, DataTable Table)
        {
            string Field = Control.Name.Substring(2);
            Control.ItemCheck -= Control_ItemCheck;
            Control.Items.Clear();
            List_Items<Guid> Checked = new List_Items<Guid>();
            List_Items<Guid> Unchecked = new List_Items<Guid>();
            IList<Guid> Links = O.Get_Relations(Field);
            foreach (DataRow A in Table.Rows)
            {
                Guid Id = A.Field<Guid>(0);
                string Name = A[1].ToString();
                if (Links.Contains(Id))
                {
                    Checked.Add(Name, Id);
                }
                else
                {
                    Unchecked.Add(Name, Id);
                }
            }
            Control.Items.AddRange(Checked.Content);
            for (int R = 0; R < Checked.Count; R++)
            {
                Control.SetItemChecked(R, true);
            }
            Control.Items.AddRange(Unchecked.Content);
            Control.ItemCheck += Control_ItemCheck;
        }

        // Enregistre les relations cumulées de plusieurs listes de checked box
        public void Save_Merge_Relations(params CheckedListBox[] Controls)
        {
            List<Guid> L = new List<Guid>();
            string Field = "";
            foreach (var C in Controls)
            {
                Field = C.Name.Substring(2);
                foreach (var I in C.CheckedItems)
                {
                    L.Add(List_Item<Guid>.Get_Id(I));
                }
            }
            O.Set_Relations(Field, L);
        }

        public void Save_Relations(CheckedListBox Control)
        {
            string Field = Control.Name.Substring(2);
            List<Guid> L = new List<Guid>();
            foreach (var I in Control.CheckedItems)
            {
                L.Add(List_Item<Guid>.Get_Id(I));
            }
            O.Set_Relations(Field, L);
        }

        void Control_ItemCheck(object objSender, ItemCheckEventArgs objArgs)
        {
            On_Change(objSender, objArgs);
        }


        public void Load_Parent(ComboBox Control, DataTable Table)
        {
            List_Items<Guid> LM = new List_Items<Guid>();
            foreach (DataRow A in Table.Rows)
            {
                LM.Add(A[1].ToString(), A.Field<Guid>(0));
            }
            Control.SelectedIndexChanged -= On_Change;
            Control.Items.Clear();
            Control.Items.AddRange(LM.Content);
            Control.SelectedIndex = LM.Get_Row(O.Parent_Id);
            Control.SelectedIndexChanged += On_Change;
        }

        public void Load_Parent <T> (ComboBox Control, JRO_List<T> Table) where T:JRO_Row, new()
        {
            List_Items<Guid> LM = new List_Items<Guid>();
            foreach (T A in Table)
            {
                LM.Add(A.Name, A.Id);
            }
            Control.SelectedIndexChanged -= On_Change;
            Control.Items.Clear();
            Control.Items.AddRange(LM.Content);
            Control.SelectedIndex = LM.Get_Row(O.Parent_Id);
            Control.SelectedIndexChanged += On_Change;
        }

        //public string S_Nouveau
        //{
        //    get { return string.Format("{0}_{1:yyMMddHHmmss}", O.ToString(), DateTime.Now); }
        //}

        public bool Remove()
        {
            if (MessageBox.Show("Voulez vous supprimer définitivement " + O.Name + " ?",
                "Suppression" + O.ToString(),
                MessageBoxButtons.OKCancel) != DialogResult.OK) return false;
            O.Remove();
            if (Tab != null) Tab.Helper.Save_Count++;
            MessageBox.Show("Suppression effectuée avec succès");
            return true;
        }

        public JR.WUI.WAF.Edit.I_Tab Tab
        {
            get { return Parent as JR.WUI.WAF.Edit.I_Tab; }
        }

        public void Display()
        {
            Clear_Errors();
            if (Parent == null) return;
            Parent.Show();
            if (Tab == null) return;
            var H = Tab.Helper;
            I_Dialog D = Parent.Form as I_Dialog;
            if (D != null) D.Display_Title();
            if (H != null)
            {
                H.Clear_Modif();
                H.Is_New = O.Is_New;
            }
        }

        public bool Write()
        {
            bool Done = O.Save(Application_State.Modifieur);
            if (Tab != null) Tab.Helper.Save_Count++;
            return Done;
        }

        public class Error
        {
            public Control Field;
            public string Message;
            public object Old_State;
        }
        public List<Error> Errors = new List<Error>();
        public int Error_Count
        {
            get { return Errors.Count; }
        }
        public void Err(Control Field, string Mess)
        {
            // Une seule erreur par champ
            foreach (var E in Errors)
            {
                if (E.Field == Field) return;
            }

            Errors.Add(new Error { Field = Field, Message = Mess });
        }

        public void Clear_Errors()
        {
            Errors.Reverse();  // Pour restituer correctement les couleurs qd un champ a eu plusieurs erreurs
            foreach (var E in Errors)
            {
                E.Field.BorderColor = (BorderColor)E.Old_State;
            }
            Errors.Clear();
        }

        public bool Show_Errors()
        {
            StringBuilder SB = new StringBuilder();
            foreach (var E in Errors)
            {
                E.Old_State = E.Field.BorderColor;
                E.Field.BorderColor = new BorderColor(System.Drawing.Color.Red);
                if (E.Message != null) SB.AppendLine(E.Message);
            }
            if (Errors.Count > 0)
            {
                MessageBox.Show(SB.ToString());
            }
            return Errors.Count > 0;
        }

        public void Check_Unicity(string Message, params Control[] Fields)
        {
            StringBuilder Field_Name = new StringBuilder();
            StringBuilder Value = new StringBuilder();
            foreach (var Field in Fields)
            {
                if (Field_Name.Length > 0) Field_Name.Append(" + ");
                Field_Name.Append(Field.Name.Substring(2));
                Value.Append(Field.Text.Trim());
            }
            if (Value.Length == 0) return;
            if (O.Is_Unique(Field_Name.ToString(), Value.ToString())) return;
            bool First = true;
            foreach (var Field in Fields)
            {
                Err(Field, First ? Message : null);
                First = false;
            }
        }

        public void Check_Format(string Validation_Expression, string Message, Control Field)
        {
            string Value = Field.Text.Trim();
            if (Value == "") return;
            string Field_Name = Field.Name.Substring(2);
            if (JR.Strings.Match(Value, Validation_Expression)) return;
            Err(Field, Message);
        }

        public void Check_Email(string Message, Control Field)
        {
            string Value = Field.Text.Trim();
            if (Value == "") return;
            string Field_Name = Field.Name.Substring(2);
            if (JR.Mails.Is_Valid_Address(Value)) return;
            Err(Field, Message);
        }

        public void Check_Len(int Min_Len, params Control[] Controls)
        {
            foreach (var C in Controls)
            {
                if (C.Text.Trim().Length < Min_Len)
                {
                    Err(C, Min_Len == 1 ? "Valeur obligatoire" : string.Format("Longueur minimale {0} caractères", Min_Len));
                }
            }
        }

    }
}
