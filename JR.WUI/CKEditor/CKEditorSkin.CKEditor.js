﻿
// Populated in Preload
var mobjApp = null;
var mstrControlId = null;         // VWG Control ID
var mstrCKScript = null;        // CKEditor's script path

// Populated in Instanciate
var mobjCKEditor = null;        // CKEditor's instance
var mobjTextNode = null;        // The text node that will be replaced by CKEditor.
var mobjBodyNode = null;        // The body node around CKEditor
var mobjDocumentNode = null;    // The document node around CKEditor
var mobjVWGNode = null;         // The VWG node that holds the attributes.
var mobjVWGDocument = null;     // The document that contains our Html (the place where this JS resource can be found).
var mobjVWGFrame = null;        // The VWG IFrame that contains our HtmlBox
var mobjDummyDiv = null;        // Div originally shown to prevent flickering
var mobjEditDiv = null;         // Div containing the editor, initially not visible

var mstrCKConfig = null;        // The CKEditor's config


// Create an instance of the editor
function CKEditor_Instanciate(objWindow) {

    // the IFrame object containing the Html (TRG_x)
    mobjVWGFrame = objWindow.frameElement;
    // the VWG node that contains the Frame (VWG_x)
    mobjVWGNode = mobjApp.Web_GetWebElement(mobjApp.Web_GetWebId(mstrControlId), mobjApp);
    // the document that contains this resource
    mobjVWGDocument = mobjApp.Web_GetTargetElementByDataId(mstrControlId, mobjApp).contentWindow;
    // the Div and body nodes
    mobjDocumentNode = objWindow.document;
    mobjBodyNode = objWindow.document.body;
    mobjDummyDiv = mobjApp.Web_GetElementById("dummyDiv", objWindow);
    mobjEditDiv = mobjApp.Web_GetElementById("editDiv", objWindow);
    var mobjTextNode = mobjApp.Web_GetElementById("editor1", objWindow);
    if (mobjTextNode) {
        // if (mobjEditDiv) {
        if (!mobjApp.Aux_IsNullOrEmpty(mstrControlID)) {
            var strText = mobjApp.Data_GetAttribute(mstrControlID, "Attr.Text", "");
            if (mobjApp.Aux_IsNullOrEmpty(strText)) {
                strText = "";
            }
            // Attr.Text is Base64 encoded text
            mobjApp.Web_SetInnerText(mobjTextNode, CKEditor_Decode(strText), 1);

            mstrCKConfig = mobjApp.Data_GetAttribute(mstrControlID, "CKCONFIG", "");
            if (mobjApp.Aux_IsNullOrEmpty(mstrCKConfig)) {
                mstrCKConfig = "{ resize_enabled: false }";
            }
        }
        CKEDITOR.on('instanceReady', function (ev) {
            CKEditor_InstanceReady(ev, window);
        });
        var config = "config = " + mstrCKConfig + ";";
        eval(config);
        mobjCKEditor = CKEDITOR.replace('editor1', config);

        // Fait claquer instantanément le blur pour forcer la lecture de la valeur courante
        CKEDITOR.focusManager._.blurDelay = 0;
        // mobjCKEditor = CKEDITOR.appendTo('editDiv', config, strText);
    }

}

function CKEditor_InstanceReady(ev, objWindow) {
    // CKEditor_Resize(null);
    // mobjApp.Web_SetDisplayBlock(mobjDummyDiv, false)
    // mobjApp.Web_SetDisplayBlock(mobjEditDiv, false)
    // mobjDummyDiv.style.display = "none";
    // mobjEditDiv.style.display = "block";
    mobjDummyDiv.className = "CKEditor-Hidden";
    mobjEditDiv.className = "CKEditor-Frames";
    mobjCKEditor.execCommand("maximize", "");
    CKEditor_Resize(null);

    // By default, CKEditor has a 100 ms delay before firing the blur event. 
    // The forceBlur event on the other hand fires immediately. 
    // Replace blur function (event handler) with forceBlur function (event handler) to have focus firing immediately.
    mobjCKEditor.on('blur', function (ev) { CKEditor_Blur(ev); });
    mobjCKEditor.on('focus', function (ev) { CKEditor_Focus(ev); });
    mobjCKEditor.on('beforeSetMode', function (ev) { CKEditor_ModeChanged(ev); });
    mobjCKEditor.on('readOnly', function (ev) { CKEditor_ReadOnlyChanged(ev); });
    mobjCKEditor.on('afterSetData', function (ev) { CKEditor_AfterSetData(ev); });

    // Syncronize properties with server-side
    //CKEditor_OnModeChanged(mobjCKEditor.getMode().name, false); // getmode n'existe plus
    CKEditor_OnReadOnlyChanged(mobjCKEditor.readOnly, true);
}

function CKEditor_AfterSetData(ev) {
}

// Resize CKEditor control to the size of the VWG control
// CKEditor only supports dock-fill by calling it's maximize command. 
// If Maximize is to be added to the control's command, either by adding the button, or by not ignore
// in execute command, the "manual" resizing mechanism must be reactivated.
function CKEditor_Resize(objWindow) {
    return true;
    var x = mobjApp.Layout_GetWidth(mobjVWGNode);
    var y = mobjApp.Layout_GetHeight(mobjVWGNode);
    mobjCKEditor.resize(x, y, false);
}

// Submit is not called directly, it is implicit by a full POST of the page
// which in turn will fire Unload, which will handle the valuechange to the server.
function CKEditor_Submit(objWindow) {
}

// Fired by CKEditor when mode is changed (source / wysiwyg).
// Syncronize to server-side
function CKEditor_ModeChanged(ev) {
    CKEditor_OnModeChanged(ev.data.newMode, true);
}

// Fired when CKEditor enters or leaves Readonly mode.
// Synchronize to server-side
function CKEditor_ReadOnlyChanged(ev) {
    CKEditor_OnReadOnlyChanged(ev.editor.readOnly, true);
}

// Syncronize Readonly mode change to server-side
function CKEditor_OnReadOnlyChanged(blnReadOnly, blnCritical) {
    var objEvent = mobjApp.Events_CreateEvent(mstrControlId, "CKReadOnly", null, true);
    mobjApp.Events_SetEventAttribute(objEvent, "ReadOnly", (blnReadOnly ? "true" : "false"));
    if (blnCritical)
        mobjApp.Events_RaiseEvents();
}

// Syncronize mode change to server-side
function CKEditor_OnModeChanged(strNewMode, blnCritical) {
    var objEvent = mobjApp.Events_CreateEvent(mstrControlId, "ModeChanged", null, true);
    mobjApp.Events_SetEventAttribute(objEvent, "NewMode", strNewMode);
    if (blnCritical)
        mobjApp.Events_RaiseEvents();
}

// On Blur, fire valuechange if editor is dirty, then fire LostFocus.
function CKEditor_Blur(ev) {
    CKEditor_ValueChanged(false, false);
    var objEvent = mobjApp.Events_CreateEvent(mstrControlId, "CKLostFocus", null, true);
    if (mobjApp.Data_IsCriticalEvent(mstrControlId, mobjApp.mcntEventLostFocusId))
        mobjApp.Events_RaiseEvents();
}

// Unload will fire when leaving page *AND* when CKEditor's save button is clicked.
// onsubmit for the form does not fire when using CKEDITOR.replace() method as
// the editable textarea is replaced with an editable frame instead.
function CKEditor_Unload(objWindow) {
    var objEvent = mobjApp.Events_CreateEvent(mstrControlId, "CKUnload", null, true);
    CKEditor_ValueChanged(true, false);
}

function CKEditor_Focus(ev) {
    var objEvent = mobjApp.Events_CreateEvent(mstrControlId, "CKGotFocus", null, true);
    if (mobjApp.Data_IsCriticalEvent(mstrControlId, mobjApp.mcntEventGotFocusId))
        mobjApp.Events_RaiseEvents();
}

// ValueChanged event will be fired when:
//      - Control is unloaded (always fired critical)
//      - When control looses focus (implicit critical firing if CKLostFocus is registered). 
// It will fire critically if TextChanged event is registered.
function CKEditor_ValueChanged(blnCritical, blnForce) {
    // Attr.Text should be base64 encoded
    var mstrOldData = mobjApp.Data_GetAttribute(mstrControlID, "Attr.Text", "");
    var mstrData = CKEditor_Encode(mobjCKEditor.getData());
    if (mstrOldData != mstrData) {
        var objValueChangeEvent = mobjApp.Events_CreateEvent(mstrControlId, "ValueChange", null, true);
        if (mobjApp.Aux_IsNullOrEmpty(mstrData))
            mstrData = "";
        mobjApp.Data_SetAttribute(mstrControlID, "Attr.Text", mstrData);
        mobjApp.Events_SetEventAttribute(objValueChangeEvent, "Attr.Text", mstrData);
        if (blnCritical || mobjApp.Data_IsCriticalEvent(mstrControlId, mobjApp.mcntEventValueChangeId)) {
            mobjApp.Events_RaiseEvents();
        }
    }
    else if (blnForce) {
        var objValueChangeEvent = mobjApp.Events_CreateEvent(mstrControlId, "ForcedSave", null, true);
        if (mobjApp.Data_IsCriticalEvent(mstrControlId, mobjApp.mcntEventAfterLabelEdit))
            mobjApp.Events_RaiseEvents();
    }
}

// Invokes setData of the data to edit.
// Called each time Text property is set on the control.
// Attr.Text should be 64bit encoded
function CKEditor_InvokeSetData(strControlId, strData) {
    if (mobjApp && mobjCKEditor) {
        mobjApp.Data_SetAttribute(mstrControlID, "Attr.Text", strData);
        mobjCKEditor.setData(CKEditor_Decode(strData));
    }
}
// Invokes focus on the editing area of the editor
// See: http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#focus
function CKEditor_InvokeFocus(strControlId) {
    if (mobjApp && mobjCKEditor) {
        mobjCKEditor.focus();
    }
}

// Invokes Save + TextChanged on the editor's data.
function CKEditor_InvokeSave(strControlId, strForce) {
    if (mobjApp && mobjCKEditor) {
        CKEditor_ValueChanged(true, (strForce == "1"));
    }
}

// Execute a CKEditor command
// See: http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#execCommand
function CKEditor_InvokeExecCommand(strControlId, strCommand, strData) {
    if (mobjApp && mobjCKEditor) {
        // Ignore the maximize command. 
        if (strCommand != 'maximize') {
            mobjCKEditor.execCommand(strCommand, strData);
            CKEditor_ValueChanged(false, false);
        }
    }
}

// Add a command definition to the editor
// See: http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#addCommand
function CKEditor_InvokeAddCommand(strControlId, strCommandName, strCommandDefinition) {
    if (mobjApp && mobjCKEditor) {
        var funcDef = "funcDef = { exec: " + strCommandDefinition + "}";
        funcDef = eval(funcDef);
        mobjCKEditor.addCommand(strCommandName, funcDef);
    }
}

// Add a Css snippet to the editor's contents
// See: http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#addCss
function CKEditor_InvokeAddCss(strControlId, strCSS) {
    if (mobjApp && mobjCKEditor) {
        mobjCKEditor.addCss(strCSS);
        CKEditor_ValueChanged(false, false);
    }
}

// Add a remove format filter
// See: http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#addRemoveFormatFilter
function CKEditor_InvokeAddRemoveFormatFilter(strControlId, strFunction) {
    if (mobjApp && mobjCKEditor) {
        var funcDef = strFunction;
        funcDev = eval(funcDef);
        mobjCKEditor.addRemoveFormatFilter(funcDef);
    }
}

// Set editor mode
// See: http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#setMode
function CKEditor_InvokeSetMode(strControlId, strNewMode) {
    if (mobjApp && mobjCKEditor) {
        mobjCKEditor.setMode(strNewMode);
    }
}

// Insert an Html element
// See: http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#insertElement
function CKEditor_InvokeInsertElement(strControlId, strHtmlElement) {
    if (mobjApp && mobjCKEditor) {
        var element = CKEDITOR.dom.element.createFromHtml(strHtmlElement);
        mobjCKEditor.insertElement(element);
        CKEditor_ValueChanged(false, false);
    }
}

// Insert Html
// See: http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#insertHtml
function CKEditor_InvokeInsertHtml(strControlId, strHtml) {
    if (mobjApp && mobjCKEditor) {
        mobjCKEditor.insertHtml(strHtml);
    }
}

// Insert Text
// See: http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#insertText
function CKEditor_InvokeInsertText(strControlId, strText) {
    if (mobjApp && mobjCKEditor) {
        mobjCKEditor.insertText(strText);
        CKEditor_ValueChanged(false,false);
    }
}

// Set Editor's readonly mode
// See: http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#setReadOnly
// Note that the CKEditor ReadOnly switch is reversed in setReadOnly, and actually means EditingAllowed
function CKEditor_InvokeSetReadOnly(strControlId, blnMode) {
    if (mobjApp && mobjCKEditor) {
        mobjCKEditor.setReadOnly(blnMode);
    }
}

/******************************************************************************************
*
*  Wrapper methods around the necessary Encoding / Decoding while transmitting data between
*  server-side and client-side of CKEditor.
*
*  Base64 encoding is necessary to transfer non-Xml friendly portions of CKEditor's value/text
*  Utf8 encoding is necessary because of lacking support for full Utf8 in client side browsers.
*
*  When transfering value from serverside and receiving at client (Decode):
*       - First Base64decode, then Utf8Decode
*  When populating value that will be sent to server (Encode):
*       - First Utf8Encode, then Base64encode
*
*  Notes:
*       Base64Encode / Decode alone will fail for double-byte characters
*       like Chineese ("你好吗?"). UTF8 encoding the text in the event
*       attributes (or response) and UTF8 decode the text before assigning
*       it to the CKEditor's text is the way to go.
*******************************************************************************************/
function CKEditor_Encode(input) {
    return CKEditor_EncodeBase64(CKEditor_utf8_encode(input));
}
function CKEditor_Decode(input) {
    return CKEditor_utf8_decode(CKEditor_DecodeBase64(input));
}
/******************************************************************************************
* Base64 encoding / decoding
* Code from: http://decodebase64.com/
*******************************************************************************************/
function CKEditor_EncodeBase64(input) {
    var b64array = "ABCDEFGHIJKLMNOP" +
           "QRSTUVWXYZabcdef" +
           "ghijklmnopqrstuv" +
           "wxyz0123456789+/" +
           "=";

    var base64 = "";
    var hex = "";
    var chr1, chr2, chr3 = "";
    var enc1, enc2, enc3, enc4 = "";
    var i = 0;

    do {
        chr1 = input.charCodeAt(i++);
        chr2 = input.charCodeAt(i++);
        chr3 = input.charCodeAt(i++);

        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;

        if (isNaN(chr2)) {
            enc3 = enc4 = 64;
        } else if (isNaN(chr3)) {
            enc4 = 64;
        }

        base64 = base64 +
            b64array.charAt(enc1) +
            b64array.charAt(enc2) +
            b64array.charAt(enc3) +
            b64array.charAt(enc4);
        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";
    } while (i < input.length);
    return base64;
}

function CKEditor_DecodeBase64(input) {
    var b64array = "ABCDEFGHIJKLMNOP" +
           "QRSTUVWXYZabcdef" +
           "ghijklmnopqrstuv" +
           "wxyz0123456789+/" +
           "=";


    var output = "";
    var hex = "";
    var chr1, chr2, chr3 = "";
    var enc1, enc2, enc3, enc4 = "";
    var i = 0;

    // Remove illegal base64 chars
    var base64test = /[^A-Za-z0-9\+\/\=]/g;
    if (base64test.exec(input)) {
        alert("Error in Base64Decode");
    }
    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

    do {
        enc1 = b64array.indexOf(input.charAt(i++));
        enc2 = b64array.indexOf(input.charAt(i++));
        enc3 = b64array.indexOf(input.charAt(i++));
        enc4 = b64array.indexOf(input.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        output = output + String.fromCharCode(chr1);

        if (enc3 != 64) {
            output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
            output = output + String.fromCharCode(chr3);
        }

        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";

    } while (i < input.length);
    return output;
}

/******************************************************************************************
*
*  UTF-8 data encode / decode
*  http://www.webtoolkit.info/ - http://www.webtoolkit.info/javascript-utf8.html
*
*  Notes for CKEditor - Base64Encode / Decode alone will fail for double-byte characters
*                       like Chineese ("你好吗?"). UTF8 encoding the text in the event
*                       attributes (or response) and UTF8 decode the text before assigning
*                       it to the CKEditor's text is the way to go.
*******************************************************************************************/
function CKEditor_utf8_encode(string) {
    string = string.replace(/\r\n/g, "\n");
    var utftext = "";

    for (var n = 0; n < string.length; n++) {

        var c = string.charCodeAt(n);

        if (c < 128) {
            utftext += String.fromCharCode(c);
        }
        else if ((c > 127) && (c < 2048)) {
            utftext += String.fromCharCode((c >> 6) | 192);
            utftext += String.fromCharCode((c & 63) | 128);
        }
        else {
            utftext += String.fromCharCode((c >> 12) | 224);
            utftext += String.fromCharCode(((c >> 6) & 63) | 128);
            utftext += String.fromCharCode((c & 63) | 128);
        }

    }

    return utftext;
}

function CKEditor_utf8_decode(utftext) {
    var string = "";
    var i = 0;
    var c = c1 = c2 = 0;

    while (i < utftext.length) {

        c = utftext.charCodeAt(i);

        if (c < 128) {
            string += String.fromCharCode(c);
            i++;
        }
        else if ((c > 191) && (c < 224)) {
            c2 = utftext.charCodeAt(i + 1);
            string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
            i += 2;
        }
        else {
            c2 = utftext.charCodeAt(i + 1);
            c3 = utftext.charCodeAt(i + 2);
            string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            i += 3;
        }

    }

    return string;
}

/******************************************************************************************
* Preload initializations.
*******************************************************************************************/
// Get reference to parent's mobjApp for current window
var objCurrentWindow = window;
while (!mobjApp && objCurrentWindow) {
    if (objCurrentWindow.mobjApp) {
        mobjApp = objCurrentWindow.mobjApp;
    }
    else {
        objCurrentWindow = objCurrentWindow.parent;
    }
}

// Get control's ID
if (mstrControlId == null) {
    mstrControlId = mobjApp.Web_GetQueryStringparam(document.location.href, "id");
}

// Load CKEditor's script, if not loaded already.
if (typeof CKDITOR == 'undefined') {
    mstrCKScript = mobjApp.Web_GetQueryStringparam(document.location.href, "source");
    var mTmpSource = '<scr' + 'ipt type="text/javascript" src="' + mstrCKScript + '"></scr' + 'ipt>';
    window.document.write(mTmpSource);
}

