// Find the CKEditor's contents node.
// Within this node, there is the Html resource for the custom control, including
// all the variables declared, like mobjVWGNode, mobjCKEditor, as well as all the
// JavaScript functions.
function CKEditor_GetDocumentNode(strControlId) {
    var objCKNode = null;
    var objCurrentWindow = window;
    var objApp = null;
    while (!objApp && objCurrentWindow) {
        if (objCurrentWindow.mobjApp) {
            objApp = objCurrentWindow.mobjApp;
        }
        else {
            objCurrentWindow = objCurrentWindow.parent;
        }
    }
    if (objApp) {
        var objVWGNode = objApp.Web_GetWebElement(objApp.Web_GetWebId(strControlId), objApp);
        if (objVWGNode) {
            try {
                objCKNode = objApp.Web_GetTargetElementByDataId(strControlId, objApp).contentWindow;
            }
            catch (e)
            { }
        }
    }
    return objCKNode;
}

// Wrapper around Server-Side Invoke calls to CKEditor's resources
// This wrapper should be part of the global resources of the skin (Presentation=Browser, PresentationRole=BrowserCode)

// Because of slow loading of CKEditor's resources, we support a delayed invocations
function CKEditor_InvokeWrapper(strControlId, strFunction, strParameter, strExtraParameters, strDelayMilliseconds) {
    if ((typeof strDelayMilliseconds == 'undefined') || strDelayMilliseconds == "" || strDelayMilliseconds == 0)
        CKEditor_InvokeWrapperExecute(strControlId, strFunction, strParameter, strExtraParameters);
    else {
        // a function variable is necessary for sucessful obscuring
        var func = CKEditor_InvokeWrapperExecute;
        setTimeout(function () { func(strControlId, strFunction, strParameter, strExtraParameters); }, strDelayMilliseconds);
    }
}
// The execution
function CKEditor_InvokeWrapperExecute(strControlId, strFunction, strParameter, strExtraParameters) {
    var objCKNode = CKEditor_GetDocumentNode(strControlId);
    if (objCKNode && objCKNode.mobjCKEditor) {
        switch (strFunction) {
            case 'InvokeSave':
                {
                    objCKNode.CKEditor_InvokeSave(strControlId, strParameter);
                    break;
                }
            case 'SetData':
                {
                    objCKNode.CKEditor_InvokeSetData(strControlId, strParameter);
                    break;
                }
            case 'Focus':
                {
                    objCKNode.CKEditor_InvokeFocus(strControlId );
                    break;
                }
            case 'ExecCommand':
                {
                    objCKNode.CKEditor_InvokeExecCommand(strControlId, strParameter, strExtraParameters);
                    break;
                }
            case 'AddCommand':
                {
                    objCKNode.CKEditor_InvokeAddCommand(strControlId, strParameter, strExtraParameters);
                    break;
                }
            case 'AddCss':
                {
                    objCKNode.CKEditor_InvokeAddCss(strControlId, strParameter);
                    break;
                }
            case 'AddRemoveFormatFilter':
                {
                    objCKNode.CKEditor_InvokeAddRemoveFormatFilter(strControlId, strParameter);
                    break;
                }
            case 'SetEditMode':
                {
                    objCKNode.CKEditor_InvokeSetMode(strControlId, strParameter);
                    break;
                }
            case 'SetReadOnly':
                {
                    objCKNode.CKEditor_InvokeSetReadOnly(strControlId, (strParameter == "True" ? true : false));
                    break;
                }
                
            case 'InsertElement':
                {
                    objCKNode.CKEditor_InvokeInsertElement(strControlId, strParameter);
                    break;
                }
            case 'InsertHtml':
                {
                    objCKNode.CKEditor_InvokeInsertHtml(strControlId, strParameter);
                    break;
                }
            case 'InsertText':
                {
                    objCKNode.CKEditor_InvokeInsertText(strControlId, strParameter);
                    break;
                }
        }
    }
}


// Custom ClientUpdateHandler that does nothing
// Necessary to prevent full subcontrol redraw on partial postbacks.
function CKEditor_CUH() {
}

