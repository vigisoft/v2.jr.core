#region Using

using System;
using System.Text;
using System.Data;
using System.Drawing;
using System.ComponentModel;
using System.Collections.Generic;


using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Forms.Skins;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms.Design;
using Gizmox.WebGUI.Common.Interfaces;
using Gizmox.WebGUI.Common.Extensibility;
using Gizmox.WebGUI.Hosting;

using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Design;


#endregion

namespace JR.WUI
{
	/// <summary>
	/// Summary description for CKEditor
	/// </summary>
	[ToolboxItem(true)]
	[Serializable()]
	[Skin(typeof(CKEditorSkin))]
	public partial class CKEditor : HtmlBox, IBindableComponent 
	{

		#region Constructor 
		public CKEditor()
		{
			this.mobjConstructing = true;
			InitializeComponent();
			this.CKConfig = new CKEditorConfig(this.CKBasePath);
			this.CKConfig.resize_enabled = false;
			this.CKConfig.autoUpdateElement = true;
			this.mobjConstructing = false;
		}
		#endregion 

		#region privates
		#region Fields
		private CKEditorConfig mobjCKEditorConfig;
		private string mstrBasePath;
		private string mstrScript;
		private bool mobjConstructing = false;
		private string mstrEditorMode;
		private bool mboolEditorReadOnly;
		private int mintInvokeMethodDelay;


		#endregion

		#region PrivateFunctions
        private string ExpandRelativePath(string strRelativePath)
        {
            string strReturnPath = strRelativePath;
            // To support design mode
            if (DesignMode)
                return strReturnPath;
            if (strReturnPath == null || strReturnPath.Length == 0)
                return strReturnPath;
            string strAbsPathBase = this.Context.HostContext.HttpContext.Request.ApplicationPath;
            if (!strAbsPathBase.EndsWith("/"))
                strAbsPathBase += "/";
            if (strReturnPath.Contains("~/"))
                strReturnPath = strReturnPath.Replace("~/", strAbsPathBase);
            else if (strReturnPath == null || strReturnPath.Length == 0)
            {
                strReturnPath = strAbsPathBase;
            }
            else if (!strReturnPath.StartsWith("/") && !strReturnPath.Contains(":"))
            {
                strReturnPath = strAbsPathBase + strReturnPath;
            }
            return strReturnPath;
        }

		// Call Update() on control if in design mode.
		// Used in most property setters.
		private void DoDesignerUpdate()
		{
			if (!this.mobjConstructing && (this.Context == null || this.DesignMode))
				this.Update();
		}

		public string base64Encode(string data)
		{
			try
			{
				byte[] encData_byte = new byte[data.Length];
				encData_byte = System.Text.Encoding.UTF8.GetBytes(data);
				string encodedData = System.Convert.ToBase64String(encData_byte);
				return encodedData;
			}
			catch (Exception e)
			{
				throw new Exception("Error in base64Encode" + e.Message);
			}
		}

		public string base64Decode(string data)
		{
			try
			{
				System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
				System.Text.Decoder utf8Decode = encoder.GetDecoder();

				byte[] todecode_byte = System.Convert.FromBase64String(data);
				int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
				char[] decoded_char = new char[charCount];
				utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
				string result = new String(decoded_char);
				return result;
			}
			catch (Exception e)
			{
				throw new Exception("Error in base64Decode" + e.Message);
			}
		}

		#endregion
		#endregion

		#region Public CKEditor Methods

		// Invokes client-side Execute a CKEditor command
		// Example: this.CKExecCommand("bold", "");
		public void CKInvokeExecCommand(string strCommand, string strData)
		{
			this.InvokeMethodWithId("CKEditor_InvokeWrapper", "ExecCommand", strCommand, strData, this.CKInvocationDelay );
		}

		// Invokes client-side editor.addCommand command
		// see http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#addCommand
		// Example: this.CKInvokeAddCommand("test", "function (editor) { alert(editor.name);}");
		public void CKInvokeAddCommand(string strCommandName, string strCommandDefinition)
		{
			this.InvokeMethodWithId("CKEditor_InvokeWrapper", "AddCommand", strCommandName, strCommandDefinition, this.CKInvocationDelay );
		}

		// Invokes client-side editor.addCss command
		// See http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#addCss
		// Example: this.CKInvokeAddCss('body { background-color: grey; }');
		public void CKInvokeAddCss(string strCss)
		{
            this.InvokeMethodWithId("CKEditor_InvokeWrapper", "AddCss", strCss, null, this.CKInvocationDelay);
		}

		// See http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#addRemoveFormatFilter
		// Example this.CKInvokeAddRemoveFormatFilter(
		//      "function( element ) {return !( element.is( 'span' ) && CKEDITOR.tools.isEmpty( element.getAttributes() ) );}");
		public void CKInvokeAddRemoveFormatFilter(string strFunction)
		{
            this.InvokeMethodWithId("CKEditor_InvokeWrapper", "AddRemoveFormatFilter", strFunction, null, this.CKInvocationDelay);
		}

		// Get editor's edit mode
		// See: http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#setMode
		// Example: string strMode = this.CKGetEditMode();
		public string CKGetEditMode()
		{
			return mstrEditorMode;
		}

		// Set editor's edit mode to either source or wysiwyg
		// See: http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#setMode
		// Example: this.CKSetEditMode("source");
		public void CKSetEditMode(string strNewMode)
		{
            this.InvokeMethodWithId("CKEditor_InvokeWrapper", "SetEditMode", strNewMode, null, this.CKInvocationDelay);
		}

		// Set focus, by first setting focus to VWG Node, then to the CKEditor
		public void CKFocus()
		{
			// this.Focus();
			// this.InvokeMethodWithId("document.frames[0].CKEditor_InvokeFocus");
            this.InvokeMethodWithId("CKEditor_InvokeWrapper", "Focus", null, null, this.CKInvocationDelay);
		}

		// Insert an Html element
		// See: http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#insertElement
		// Example: this.CKInvokeInsertElement( "<img src=\"hello.png\" border=\"0\" title=\"Hello\" />");
		public void CKInvokeInsertElement(string strHtmlElement)
		{
            this.InvokeMethodWithId("CKEditor_InvokeWrapper", "InsertElement", strHtmlElement, null, this.CKInvocationDelay);
		}

		// Insert Html
		// See: http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#insertHtml
		// Example: this.CKInvokeInsertHtml("<p>This is a new paragraph.</p>");
		public void CKInvokeInsertHtml(string strHtml)
		{
            this.InvokeMethodWithId("CKEditor_InvokeWrapper", "InsertHtml", strHtml, null, this.CKInvocationDelay);
		}

		// Insert Text
		// See: http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#insertText
		// Example: this.CKInvokeInsertText("This is a text\nand this is line 2");
		public void CKInvokeInsertText(string strText)
		{
            this.InvokeMethodWithId("CKEditor_InvokeWrapper", "InsertText", strText, null, this.CKInvocationDelay);
		}

		// Set the ReadOnly mode of the editor
		// See: http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#setReadOnly
		// Example: this.CKSetReadOnly(false);
		public void CKSetReadOnly(bool blnReadOnly)
		{
            this.InvokeMethodWithId("CKEditor_InvokeWrapper", "SetReadOnly", blnReadOnly, null, this.CKInvocationDelay);
		}

        // Invoke Save of data. Will raise TextChanged critically if value has changed.
        // Example: this.CKInvokeSave;
        public void CKInvokeSave()
        {
            this.InvokeMethodWithId("CKEditor_InvokeWrapper", "InvokeSave", "0", null, this.CKInvocationDelay);
        }
        // Invoke Save of data. If blnForce=true, will raise TextChanged critically even if value has not changed.
        // Example: this.CKInvokeSave;
        public void CKInvokeSave(bool blnForce)
        {
            this.InvokeMethodWithId("CKEditor_InvokeWrapper", "InvokeSave", (blnForce ? "1" : "0"), null, this.CKInvocationDelay );
        }

		// Get the ReadOnly mode of the editor
		// See: http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html#readOnly
		// Example: bool blnReadOnly = this.CKGetReadOnly();
		public bool CKGetReadOnly()
		{
			return mboolEditorReadOnly;
		}
		#endregion 

		#region Hiding HtmlBox properties
		// Prevent design time serialization and setting of certain HtmlBox properties that could interfere 
		// with CKEditor's rendering.

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public override string Html { get { return ""; } }
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool IsWindowless { get { return false; } }
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public override string Url { get { return ""; } }
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public override string Path { get { return ""; } }
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public override Gizmox.WebGUI.Common.Resources.ResourceHandle Resource { get { return null; } }
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public HtmlBoxType Type { get { return HtmlBoxType.HTML; } }
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public string ContentType { get { return "text/html"; } }
		   

		#endregion 

		#region CKEditor Config Properties
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public CKEditorConfig CKConfig
		{
			get
			{
				if (mobjCKEditorConfig == null)
					mobjCKEditorConfig = new CKEditorConfig(this.CKBasePath);
				return mobjCKEditorConfig ;
			}
			set 
			{ 
				mobjCKEditorConfig  = value;
				this.DoDesignerUpdate();
			}
		}

		/// <summary>
		/// <p>
		///		Sets or gets the virtual path to the editor's directory. It is
		///		relative to the current page. Values starting without "/" will
		///		be relative to current app's virtual folder.
		/// </p>
		/// <p>
		///		The default value is "CKeditor", which would point to ckeditor folder within app's virtual folder.
		/// </p>
		/// <p>
		///		The base path can be also set in the Web.config file using the 
		///		appSettings section. Just set the "CKeditor:BasePath" for that. 
		///		For example:
		///		<code>
		///		&lt;configuration&gt;
		///			&lt;appSettings&gt;
		///				&lt;add key="CKeditor:BasePath" value="/scripts/CKeditor" /&gt;
		///			&lt;/appSettings&gt;
		///		&lt;/configuration&gt;
		///		</code>
		/// </p>
		/// </summary>
		[DefaultValue("ckeditor")]
		[Category("CKEditor Basic Settings")]
		[Description("Sets or gets the virtual path to the editor's directory.")]
		public string CKBasePath
		{
			get
			{
				if (mstrBasePath == null)
					mstrBasePath = ConfigurationManager.AppSettings["CKeditor:BasePath"];

				mstrBasePath = ( mstrBasePath == null ? "CKeditor/" : mstrBasePath);
				return (mstrBasePath.EndsWith("/") ? mstrBasePath.Remove(mstrBasePath.Length - 1) : mstrBasePath);
			}
			set 
			{
				if (value != null && value.EndsWith("/"))
					mstrBasePath = value.Remove(value.Length - 1);
				else 
					mstrBasePath = value;
				this.DoDesignerUpdate();
			}
		}

		/// <summary>
		/// <p>
		///		Sets or gets the name of the CKEditor's script, normally ckeditor.js
		/// </p>
		/// <p>
		///		The default value is "ckeditor.js".
		/// </p>
		/// <p>
		///		The script name can be also set in the Web.config file using the 
		///		appSettings section. Just set the "CKeditor:Script" for that. 
		///		For example:
		///		<code>
		///		&lt;configuration&gt;
		///			&lt;appSettings&gt;
		///				&lt;add key="CKeditor:Script" value="ckeditor.js" /&gt;
		///			&lt;/appSettings&gt;
		///		&lt;/configuration&gt;
		///		</code>
		/// </p>
		/// </summary>
		[DefaultValue("ckeditor.js")]
		[Category("CKEditor Basic Settings")]
		[Description("Sets or gets the name of the CKEditor's script, normally ckeditor.js")]
		public string CKScript
		{
			get
			{
				if (mstrScript == null)
					mstrScript = ConfigurationManager.AppSettings["CKeditor:Script"];

				return (mstrScript == null ? "ckeditor.js" : mstrScript);
			}
			set 
			{ 
				mstrScript = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description(@"Sets the ""class"" attribute to be used on the body element of the editing area. This can be useful when reusing the original CSS
file you're using on your live website and you want to assing to the editor the same class name you're using for the region 
that'll hold the contents. In this way, class specific CSS rules will be enabled.")]
		[DefaultValue("")]
		public string CKBodyClass 
		{ 
			get { return this.CKConfig.bodyClass; } 
			set 
			{ 
				this.CKConfig.bodyClass = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Other Settings")]
		[Description(@"Sets the ""id"" attribute to be used on the body element of the editing area. This can be useful when reusing the original CSS 
file you're using on your live website and you want to assing to the editor the same id you're using for the region 
that'll hold the contents. In this way, id specific CSS rules will be enabled.")]
		[DefaultValue("")]
		public string CKBodyId 
		{ 
			get { return this.CKConfig.bodyId; }
			set
			{
				this.CKConfig.bodyId = value; 
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Other Settings")]
		[Description("Whether to show the browser native context menu when the CTRL or the META (Mac) key is pressed while opening the context menu.")]
		[DefaultValue(true)]
		public bool CKBrowserContextMenuOnCtrl 
		{ 
			get { return this.CKConfig.browserContextMenuOnCtrl; }
			set
			{
				this.CKConfig.browserContextMenuOnCtrl = value;
				this.DoDesignerUpdate();
			} 
		}


		[PersistenceMode(PersistenceMode.Attribute)]
		[Category("CKEditor Basic Settings")]
		[Description("The CSS file(s) to be used to apply style to the contents. It should reflect the CSS used in the final pages where the contents are to be used.")]
		[DefaultValue("~/ckeditor/contents.css")]
		[Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
		public string CKContentsCss
		{
			get
			{
				string retVal = string.Empty;
				foreach (string item in this.CKConfig.contentsCss) retVal += item + "\n";
				if (retVal.EndsWith(",")) retVal = retVal.Remove(retVal.Length - 1);
				return retVal;
			}
			set {
				string[] cssFiles = value.Replace("\t", string.Empty).Split(new char[] { ',', '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
				this.CKConfig.contentsCss = cssFiles;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description(@"The writing direction of the language used to write the editor contents. Allowed values are: 
'ui' - which indicate content direction will be the same with the user interface language direction;
'ltr' - for Left-To-Right language (like English);
'rtl' - for Right-To-Left languages (like Arabic).")]
		[DefaultValue(typeof(contentsLangDirections), "Ui")]
		public contentsLangDirections CKContentsLangDirection 
		{ 
			get { return this.CKConfig.contentsLangDirection; }
			set
			{
				this.CKConfig.contentsLangDirection = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Other Settings")]
		[Description("Language code of the writting language which is used to author the editor contents.")]
		[DefaultValue("")]
		public string CKContentsLanguage 
		{
			get { return this.CKConfig.contentsLanguage; }
			set
			{
				this.CKConfig.contentsLanguage = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Basic Settings")]
		[Description(@"The URL path for the custom configuration file to be loaded. If not overloaded with inline configurations, it defaults 
to the ""config.js"" file present in the root of the CKEditor installation directory.
CKEditor will recursively load custom configuration files defined inside other custom configuration files.")]
		[DefaultValue("config.js")]
		public string CKCustomConfig
		{
			get { return this.CKConfig.customConfig; }
			set
			{
				this.CKConfig.customConfig = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Basic Settings")]
		[Description("The language to be used if CKEDITOR.config.language is left empty and it's not possible to localize the editor to the user language.")]
		[DefaultValue("en")]
		public string CKDefaultLanguage 
		{
			get { return this.CKConfig.defaultLanguage; }
			set
			{
				this.CKConfig.defaultLanguage = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description(@"The guideline to follow when generating the dialog buttons. There are 3 possible options:
'OS' - the buttons will be displayed in the default order of the user's OS;
'ltr' - for Left-To-Right order;
'rtl' - for Right-To-Left order.")]
		[DefaultValue(typeof(DialogButtonsOrder), "OS")]
		public DialogButtonsOrder CKDialogButtonsOrder 
		{
			get { return this.CKConfig.dialog_buttonsOrder; }
			set
			{
				this.CKConfig.dialog_buttonsOrder = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description(@"Disables the built-in spell checker while typing natively available in the browser (currently Firefox and Safari only).
Even if word suggestions will not appear in the CKEditor context menu, this feature is useful to help quickly identifying misspelled words.
This setting is currently compatible with Firefox only due to limitations in other browsers.")]
		[DefaultValue(true)]
		public bool CKDisableNativeSpellChecker 
		{
			get { return this.CKConfig.disableNativeSpellChecker; }
			set
			{
				this.CKConfig.disableNativeSpellChecker = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description(@"Disables the ""table tools"" offered natively by the browser (currently Firefox only) to make quick table editing operations, 
like adding or deleting rows and columns.")]
		[DefaultValue(true)]
		public bool CKDisableNativeTableHandles 
		{
			get { return this.CKConfig.disableNativeTableHandles; }
			set
			{
				this.CKConfig.disableNativeTableHandles = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description("Disables the ability of resize objects (image and tables) in the editing area.")]
		[DefaultValue(false)]
		public bool CKDisableObjectResizing 
		{
			get { return this.CKConfig.disableObjectResizing; }
			set
			{
				this.CKConfig.disableObjectResizing = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description("Disables inline styling on read-only elements.")]
		[DefaultValue(false)]
		public bool CKDisableReadonlyStyling 
		{
			get { return this.CKConfig.disableReadonlyStyling; }
			set
			{
				this.CKConfig.disableReadonlyStyling = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description("Sets the doctype to be used when loading the editor content as HTML.")]
		[DefaultValue(@"<!DOCTYPE html>")]
		public string CKDocType 
		{
			get { return this.CKConfig.docType; }
			set
			{
				this.CKConfig.docType = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description("Whether to render or not the editing block area in the editor interface.")]
		[DefaultValue(true)]
		public bool CKEditingBlock 
		{
			get { return this.CKConfig.editingBlock; }
			set
			{
				this.CKConfig.editingBlock = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description(@"The e-mail address anti-spam protection option. The protection will be applied when creating or modifying e-mail links through the editor interface.
Two methods of protection can be choosed: 
The e-mail parts (name, domain and any other query string) are assembled into a function call pattern. Such function must be provided by 
the developer in the pages that will use the contents. 
Only the e-mail address is obfuscated into a special string that has no meaning for humans or spam bots, but which is properly rendered and accepted by the browser.
Both approaches require JavaScript to be enabled.")]
		[DefaultValue("")]
		public string CKEmailProtection 
		{
			get { return this.CKConfig.emailProtection; }
			set
			{
				this.CKConfig.emailProtection = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description(@"Allow context-sensitive tab key behaviors, including the following scenarios: 
When selection is anchored inside table cells:
If TAB is pressed, select the contents of the ""next"" cell. If in the last cell in the table, add a new row to it and focus its first cell.
If SHIFT+TAB is pressed, select the contents of the ""previous"" cell. Do nothing when it's in the first cell.")]
		[DefaultValue(true)]
		public bool CKEnableTabKeyTools 
		{
			get { return this.CKConfig.enableTabKeyTools; }
			set
			{
				this.CKConfig.enableTabKeyTools = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description(@"Sets the behavior for the ENTER key. It also dictates other behaviour rules in the editor, like whether the <br> element is to be used as a paragraph separator when indenting text.
The allowed values are the following constants, and their relative behavior: 
CKEDITOR.ENTER_P (1): new <p> paragraphs are created;
CKEDITOR.ENTER_BR (2): lines are broken with <br> elements;
CKEDITOR.ENTER_DIV (3): new <div> blocks are created.
Note: It's recommended to use the CKEDITOR.ENTER_P value because of its semantic value and correctness. The editor is optimized for this value.")]
		[DefaultValue(typeof(EnterMode), "P")]
		public EnterMode CKEnterMode 
		{
			get { return this.CKConfig.enterMode; }
			set
			{
				this.CKConfig.enterMode = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Basic Settings")]
		[Description("Whether to use HTML entities in the output.")]
		[DefaultValue(true)]
		public bool CKEntities 
		{
			get { return this.CKConfig.entities; }
			set
			{
				this.CKConfig.entities = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Basic Settings")]
		[Description(@"An additional list of entities to be used. It's a string containing each entry separated by a comma.
Entities names or number must be used, exclusing the ""&"" preffix and the "";"" termination.")]
		[DefaultValue("#39")]
		public string CKEntitiesAdditional 
		{
			get { return this.CKConfig.entities_additional; }
			set
			{
				this.CKConfig.entities_additional = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Basic Settings")]
		[Description(@"Whether to convert some symbols, mathematical symbols, and Greek letters to HTML entities. This may be more relevant for users typing text written in Greek.
The list of entities can be found at the W3C HTML 4.01 Specification, section 24.3.1.")]
		[DefaultValue(true)]
		public bool CKEntitiesGreek 
		{
			get { return this.CKConfig.entities_greek; }
			set
			{
				this.CKConfig.entities_greek = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Basic Settings")]
		[Description(@"Whether to convert some Latin characters (Latin alphabet No. 1, ISO 8859-1) to HTML entities.
The list of entities can be found at the W3C HTML 4.01 Specification, section 24.2.1.")]
		[DefaultValue(true)]
		public bool CKEntitiesLatin 
		{
			get { return this.CKConfig.entities_latin; }
			set
			{
				this.CKConfig.entities_latin = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Basic Settings")]
		[Description(@"List of additional plugins to be loaded. 
This is a tool setting which makes it easier to add new plugins, whithout having to touch and possibly breaking the CKEDITOR.config.plugins setting.")]
		[DefaultValue("")]
		public string CKExtraPlugins 
		{
			get { return this.CKConfig.extraPlugins; }
			set
			{
				this.CKConfig.extraPlugins = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Basic Settings")]
		[Description(@"The location of an external file browser, that should be launched when ""Browse Server"" button is pressed.
If configured, the ""Browse Server"" button will appear in Link, Image and Flash dialogs.")]
		[DefaultValue("")]
		public string CKFilebrowserBrowseUrl 
		{
			get { return this.CKConfig.filebrowserBrowseUrl; }
			set
			{
				this.CKConfig.filebrowserBrowseUrl = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Basic Settings")]
		[Description(@"The location of an external file browser, that should be launched when ""Browse Server"" button is pressed in the Flash dialog.
If not set, CKEditor will use CKEDITOR.config.filebrowserBrowseUrl.")]
		[DefaultValue("")]
		public string CKFilebrowserFlashBrowseUrl 
		{
			get { return this.CKConfig.filebrowserFlashBrowseUrl; }
			set
			{
				this.CKConfig.filebrowserFlashBrowseUrl = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Basic Settings")]
		[Description("The location of a script that handles file uploads in the Flash dialog. If not set, CKEditor will use CKEDITOR.config.filebrowserUploadUrl.")]
		[DefaultValue("")]
		public string CKFilebrowserFlashUploadUrl 
		{
			get { return this.CKConfig.filebrowserFlashUploadUrl; }
			set
			{
				this.CKConfig.filebrowserFlashUploadUrl = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Basic Settings")]
		[Description(@"The location of an external file browser, that should be launched when ""Browse Server"" button is pressed in the Link tab of Image dialog.
If not set, CKEditor will use CKEDITOR.config.filebrowserBrowseUrl.")]
		[DefaultValue("")]
		public string CKFilebrowserImageBrowseLinkUrl 
		{
			get { return this.CKConfig.filebrowserImageBrowseLinkUrl; }
			set
			{
				this.CKConfig.filebrowserImageBrowseLinkUrl = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Basic Settings")]
		[Description(@"The location of an external file browser, that should be launched when ""Browse Server"" button is pressed in the Image dialog.
If not set, CKEditor will use CKEDITOR.config.filebrowserBrowseUrl.")]
		[DefaultValue("")]
		public string CKFilebrowserImageBrowseUrl 
		{
			get { return this.CKConfig.filebrowserImageBrowseUrl; }
			set
			{
				this.CKConfig.filebrowserImageBrowseUrl = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Basic Settings")]
		[Description("The location of a script that handles file uploads in the Image dialog. If not set, CKEditor will use CKEDITOR.config.filebrowserUploadUrl.")]
		[DefaultValue("")]
		public string CKFilebrowserImageUploadUrl 
		{
			get { return this.CKConfig.filebrowserImageUploadUrl; }
			set
			{
				this.CKConfig.filebrowserImageUploadUrl = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Basic Settings")]
		[Description(@"The location of a script that handles file uploads. If set, the ""Upload"" tab will appear in ""Link"", ""Image"" and ""Flash"" dialogs.")]
		[DefaultValue("")]
		public string CKFilebrowserUploadUrl 
		{
			get { return this.CKConfig.filebrowserUploadUrl; }
			set
			{
				this.CKConfig.filebrowserUploadUrl = value;
				this.DoDesignerUpdate();
			}
		}


		[Category("CKEditor Other Settings")]
		[Description(@"The ""features"" to use in the file browser popup window.")]
		[DefaultValue("location=no,menubar=no,toolbar=no,dependent=yes,minimizable=no,modal=yes,alwaysRaised=yes,resizable=yes,scrollbars=yes")]
		[Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
		public string CKFilebrowserWindowFeatures 
		{ 
			get { return this.CKConfig.filebrowserWindowFeatures; }
			set
			{
				this.CKConfig.filebrowserWindowFeatures = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Other Settings")]
		[Description(@"Whether a filler text (non-breaking space entity -  ) will be inserted into empty block elements in HTML output, 
this is used to render block elements properly with line-height; When a function is instead specified, 
it'll be passed a CKEDITOR.htmlParser.element to decide whether adding the filler text by expecting a boolean return value.")]
		[DefaultValue(true)]
		public bool CKFillEmptyBlocks 
		{ 
			get { return this.CKConfig.fillEmptyBlocks; }
			set
			{
				this.CKConfig.fillEmptyBlocks = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Other Settings")]
		[Description("The text to be displayed in the Font combo is none of the available values matches the current cursor position or text selection.")]
		[DefaultValue("")]
		public string CKFontDefaultLabel 
		{ 
			get { return this.CKConfig.font_defaultLabel; }
			set
			{
				this.CKConfig.font_defaultLabel = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Basic Settings")]
		[Description(@"The list of fonts names to be displayed in the Font combo in the toolbar. Entries are separated by semi-colons (;),
while it's possible to have more than one font for each entry, in the HTML way (separated by comma).
A display name may be optionally defined by prefixing the entries with the name and the slash character.")]
		[DefaultValue(@"Arial/Arial, Helvetica, sans-serif;
Comic Sans MS/Comic Sans MS, cursive;
Courier New/Courier New, Courier, monospace;
Georgia/Georgia, serif;
Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;
Tahoma/Tahoma, Geneva, sans-serif;
Times New Roman/Times New Roman, Times, serif;
Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;
Verdana/Verdana, Geneva, sans-serif")]
		[Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
		public string CKFontNames 
		{ 
			get { return this.CKConfig.font_names; }
			set
			{
				this.CKConfig.font_names = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Other Settings")]
		[Description("The text to be displayed in the Font Size combo is none of the available values matches the current cursor position or text selection.")]
		[DefaultValue("")]
		public string CKFontSizeDefaultLabel 
		{ 
			get { return this.CKConfig.fontSize_defaultLabel; }
			set
			{
				this.CKConfig.fontSize_defaultLabel = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Basic Settings")]
		[Description(@"The list of fonts size to be displayed in the Font Size combo in the toolbar. Entries are separated by semi-colons (;).
Any kind of ""CSS like"" size can be used, like ""12px"", ""2.3em"", ""130%"", ""larger"" or ""x-small"".
A display name may be optionally defined by prefixing the entries with the name and the slash character.")]
		[DefaultValue("8/8px;9/9px;10/10px;11/11px;12/12px;14/14px;16/16px;18/18px;20/20px;22/22px;24/24px;26/26px;28/28px;36/36px;48/48px;72/72px")]
		[Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
		public string CKFontSizeSizes 
		{ 
			get { return this.CKConfig.fontSize_sizes; }
			set
			{
				this.CKConfig.fontSize_sizes = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Other Settings")]
		[Description(@"Force the respect of CKEDITOR.CKConfig.enterMode as line break regardless of the context, 
E.g. If CKEDITOR.CKConfig.enterMode is set to CKEDITOR.ENTER_P, press enter key inside a 'div' will create a new paragraph with 'p' instead of 'div'.")]
		[DefaultValue(false)]
		public bool CKForceEnterMode 
		{ 
			get { return this.CKConfig.forceEnterMode; }
			set
			{
				this.CKConfig.forceEnterMode = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Basic Settings")]
		[Description("Whether to force all pasting operations to insert on plain text into the editor, loosing any formatting information possibly available in the source text.")]
		[DefaultValue(false)]
		public bool CKForcePasteAsPlainText 
		{ 
			get { return this.CKConfig.forcePasteAsPlainText; }
			set
			{
				this.CKConfig.forcePasteAsPlainText = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Basic Settings")]
		[Description(@" Whether to force using ""&"" instead of ""&amp;"" in elements attributes values,
it's not recommended to change this setting for compliance with the W3C XHTML 1.0 standards (C.12, XHTML 1.0).")]
		[DefaultValue(false)]
		public bool CKForceSimpleAmpersand 
		{ 
			get { return this.CKConfig.forceSimpleAmpersand; }
			set
			{
				this.CKConfig.forceSimpleAmpersand = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Basic Settings")]
		[Description(@" A list of semi colon separated style names (by default tags) representing the style definition for each entry to be displayed
in the Format combo in the toolbar. Each entry must have its relative definition configuration in a setting named
""format_(tagName)"". For example, the ""p"" entry has its definition taken from config.format_p.
")]
		[DefaultValue("p;h1;h2;h3;h4;h5;h6;pre;address;div")]
		[Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
		public string CKFormatTags 
		{ 
			get { return this.CKConfig.format_tags; }
			set
			{
				this.CKConfig.format_tags = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Basic Settings")]
		[Description(@"Indicates whether the contents to be edited are being inputted as a full HTML page. 
A full page includes the <html>, <head> and <body> tags. 
The final output will also reflect this setting, including the <body> contents only if this setting is disabled.")]
		[DefaultValue(false)]
		public bool CKFullPage 
		{ 
			get { return this.CKConfig.fullPage; }
			set
			{
				this.CKConfig.fullPage = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Other Settings")]
		[Description("Whether escape HTML when editor update original input element.")]
		[DefaultValue(true)]
		public bool CKHtmlEncodeOutput 
		{ 
			get { return this.CKConfig.htmlEncodeOutput; }
			set
			{
				this.CKConfig.htmlEncodeOutput = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Other Settings")]
		[Description(@"Whether the editor must output an empty value ("""") if it's contents is made by an empty paragraph only.")]
		[DefaultValue(true)]
		public bool CKIgnoreEmptyParagraph 
		{ 
			get { return this.CKConfig.ignoreEmptyParagraph; }
			set
			{
				this.CKConfig.ignoreEmptyParagraph = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Other Settings")]
		[Description("Padding text to set off the image in preview area.")]
		[DefaultValue("")]
		public string CKImagePreviewText 
		{ 
			get { return this.CKConfig.image_previewText; }
			set
			{
				this.CKConfig.image_previewText = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Other Settings")]
		[Description("Whether to remove links when emptying the link URL field in the image dialog.")]
		[DefaultValue(true)]
		public bool CKImageRemoveLinkByEmptyURL 
		{ 
			get { return this.CKConfig.image_removeLinkByEmptyURL; }
			set
			{
				this.CKConfig.image_removeLinkByEmptyURL = value;
				this.DoDesignerUpdate();
			}
		}

		[TypeConverter(typeof(StringArrayConverter))]
		[PersistenceMode(PersistenceMode.Attribute)]
		[Category("CKEditor Other Settings")]
		[Description("List of classes to use for indenting the contents. If it's null, no classes will be used and instead the #indentUnit and #indentOffset properties will be used.")]
		[DefaultValue(typeof(Array), null)]
		public string[] CKIndentClasses 
		{ 
			get { return this.CKConfig.indentClasses; }
			set
			{
				this.CKConfig.indentClasses = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description("Size of each indentation step")]
		[DefaultValue(40)]
		public int CKIndentOffset 
		{ 
			get { return this.CKConfig.indentOffset; }
			set
			{
				this.CKConfig.indentOffset = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description("Unit for the indentation style")]
		[DefaultValue("px")]
		public string CKIndentUnit 
		{ 
			get { return this.CKConfig.indentUnit; }
			set
			{
				this.CKConfig.indentUnit = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Basic Settings")]
		[Description(@"The user interface language localization to use. If empty, the editor automatically localize the editor to the user language,
if supported, otherwise the CKEDITOR.CKConfig.defaultLanguage language is used.")]
		[DefaultValue("")]
		public string CKLanguage
		{ 
			get { return this.CKConfig.language; }
			set
			{
				this.CKConfig.language = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description("Whether to transform MS Word outline numbered headings into lists.")]
		[DefaultValue(false)]
		public bool CKPasteFromWordNumberedHeadingToList 
		{ 
			get { return this.CKConfig.pasteFromWordNumberedHeadingToList; }
			set
			{
				this.CKConfig.pasteFromWordNumberedHeadingToList = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description("Whether to prompt the user about the clean up of content being pasted from MS Word.")]
		[DefaultValue(false)]
		public bool CKPasteFromWordPromptCleanup 
		{ 
			get { return this.CKConfig.pasteFromWordPromptCleanup; }
			set
			{
				this.CKConfig.pasteFromWordPromptCleanup = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Other Settings")]
		[Description(@"Whether to ignore all font related formatting styles, including: 
font size;
font family;
font foreground/background color.")]
		[DefaultValue(true)]
		public bool CKPasteFromWordRemoveFontStyles 
		{ 
			get { return this.CKConfig.pasteFromWordRemoveFontStyles; }
			set
			{
				this.CKConfig.pasteFromWordRemoveFontStyles = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description(@"Whether to remove element styles that can't be managed with the editor. Note that this doesn't handle the font specific styles,
which depends on the CKEDITOR.CKConfig.pasteFromWordRemoveFontStyles setting instead.")]
		[DefaultValue(false)]
		public bool CKPasteFromWordRemoveStyles 
		{ 
			get { return this.CKConfig.pasteFromWordRemoveStyles; }
			set
			{
				this.CKConfig.pasteFromWordRemoveStyles = value;
				this.DoDesignerUpdate();
			} 
		}

		[TypeConverter(typeof(StringArrayConverter))]
		[PersistenceMode(PersistenceMode.Attribute)]
		[Category("CKEditor Other Settings")]
		[Description("List of regular expressions to be executed over the input HTML, indicating HTML source code that matched must not present in WYSIWYG mode for editing.")]
		[DefaultValue(typeof(Array), null)]
		public string[] CKProtectedSource 
		{ 
			get { return this.CKConfig.protectedSource; }
			set
			{
				this.CKConfig.protectedSource = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Other Settings")]
		[Description(@"The dialog contents to removed. It's a string composed by dialog name and tab name with a colon between them. 
Separate each pair with semicolon (see example). Note: All names are case-sensitive. 
Note: Be cautious when specifying dialog tabs that are mandatory, like ""info"", 
dialog functionality might be broken because of this!")]
		[DefaultValue("")]
		public string CKRemoveDialogTabs 
		{ 
			get { return this.CKConfig.removeDialogTabs; }
			set
			{
				this.CKConfig.removeDialogTabs = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description(@"List of plugins that must not be loaded. This is a tool setting which makes it easier to avoid loading plugins definied 
in the CKEDITOR.CKConfig.plugins setting, whithout having to touch it and potentially breaking it.")]
		[DefaultValue("")]
		public string CKRemovePlugins 
		{ 
			get { return this.CKConfig.removePlugins; }
			set
			{
				this.CKConfig.removePlugins = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description(@"The directions to which the editor resizing is enabled. Possible values are ""both"", ""vertical"" and ""horizontal"".")]
		[DefaultValue(typeof(ResizeDir), "Both")]
		public ResizeDir CKResizeDir 
		{ 
			get { return this.CKConfig.resize_dir; }
			set
			{
				this.CKConfig.resize_dir = value;
				this.DoDesignerUpdate();
			}
		}

		//[Category("CKEditor Other Settings")]
		//[Description("Whether to enable the resizing feature. If disabled the resize handler will not be visible.")]
		//[DefaultValue(true)]
		//public bool CKResizeEnabled 
		//{ 
		//    get { return this.CKConfig.resize_enabled; } 
		//    set { this.CKConfig.resize_enabled = value; }
		//}

//        [Category("CKEditor Other Settings")]
//        [Description("The maximum editor height, in pixels, when resizing it with the resize handle.")]
//        [DefaultValue(3000)]
//        public int CKResizeMaxHeight 
//        { 
//            get { return this.CKConfig.resize_maxHeight; } 
//            set { this.CKConfig.resize_maxHeight = value; }
//        }

//        [Category("CKEditor Other Settings")]
//        [Description("The maximum editor width, in pixels, when resizing it with the resize handle.")]
//        [DefaultValue(3000)]
//        public int CKResizeMaxWidth 
//        { 
//            get { return this.CKConfig.resize_maxWidth; } 
//            set { this.CKConfig.resize_maxWidth = value; } 
//        }

//        [Category("CKEditor Other Settings")]
//        [Description(@"The minimum editor height, in pixels, when resizing it with the resize handle. 
//Note: It fallbacks to editor's actual height if that's smaller than the default value.")]
//        [DefaultValue(250)]
//        public int CKResizeMinHeight 
//        { 
//            get { return this.CKConfig.resize_minHeight; } 
//            set { this.CKConfig.resize_minHeight = value; } 
//        }

//        [Category("CKEditor Other Settings")]
//        [Description(@"The minimum editor width, in pixels, when resizing it with the resize handle. 
//Note: It fallbacks to editor's actual width if that's smaller than the default value.")]
//        [DefaultValue(750)]
//        public int CKResizeMinWidth 
//        { 
//            get { return this.CKConfig.resize_minWidth; } 
//            set { this.CKConfig.resize_minWidth = value; } 
//        }

		[Category("CKEditor Other Settings")]
		[Description("If enabled (true), turns on SCAYT automatically after loading the editor.")]
		[DefaultValue(false)]
		public bool CKScaytAutoStartup 
		{ 
			get { return this.CKConfig.scayt_autoStartup; }
			set
			{
				this.CKConfig.scayt_autoStartup = value;
				this.DoDesignerUpdate();
			} 
		}

		//[Category("CKEditor Other Settings")]
		//[Description("ID of bottom cntrol's shared")]
		//[DefaultValue("")]
		//public string CKSharedSpacesBottom 
		//{ 
		//    get { return this.CKConfig.sharedSpacesBottom; } 
		//    set { this.CKConfig.sharedSpacesBottom = value; } 
		//}

		//[Category("CKEditor Other Settings")]
		//[Description("ID of top cntrol's shared")]
		//[DefaultValue("")]
		//public string CKSharedSpacesTop 
		//{ 
		//    get { return this.CKConfig.sharedSpacesTop; } 
		//    set { this.CKConfig.sharedSpacesTop = value; } 
		//}

		[Category("CKEditor Other Settings")]
		[Description(@"Just like the CKEDITOR.CKConfig.enterMode setting, it defines the behavior for the SHIFT+ENTER key.
The allowed values are the following constants, and their relative behavior: 
CKEDITOR.ENTER_P (1): new <p> paragraphs are created;
CKEDITOR.ENTER_BR (2): lines are broken with <br> elements;
CKEDITOR.ENTER_DIV (3): new <div> blocks are created.")]
		[DefaultValue(typeof(EnterMode), "P")]
		public EnterMode CKShiftEnterMode 
		{ 
			get { return this.CKConfig.shiftEnterMode; }
			set
			{
				this.CKConfig.shiftEnterMode = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Basic Settings")]
		[Description("The skin to load. It may be the name of the skin folder inside the editor installation path, or the name and the path separated by a comma.")]
		[DefaultValue("kama")]
		public string CKSkin 
		{ 
			get { return this.CKConfig.skin; }
			set
			{
				this.CKConfig.skin = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Other Settings")]
		[Description(@"The mode to load at the editor startup. It depends on the plugins loaded. By default, the ""wysiwyg"" and ""source"" modes are available.")]
		[DefaultValue(typeof(StartupMode), "Wysiwyg")]
		public StartupMode CKStartupMode 
		{ 
			get { return this.CKConfig.startupMode; }
			set
			{
				this.CKConfig.startupMode = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Other Settings")]
		[Description(@"Whether to automaticaly enable the ""show block"" command when the editor loads. (StartupShowBlocks in FCKeditor)")]
		[DefaultValue(false)]
		public bool CKStartupOutlineBlocks 
		{ 
			get { return this.CKConfig.startupOutlineBlocks; }
			set
			{
				this.CKConfig.startupOutlineBlocks = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description(@"Whether to automatically enable the ""show borders"" command when the editor loads. (ShowBorders in FCKeditor)")]
		[DefaultValue(true)]
		public bool CKStartupShowBorders 
		{ 
			get { return this.CKConfig.startupShowBorders; }
			set
			{
				this.CKConfig.startupShowBorders = value;
				this.DoDesignerUpdate();
			}
		}

				[PersistenceMode(PersistenceMode.Attribute)]
		[Category("CKEditor Other Settings")]
		[Description(@"The ""styles definition set"" to use in the editor. They will be used in the styles combo and the Style selector of the div container.
The styles may be defined in the page containing the editor, or can be loaded on demand from an external file. In the second case, 
if this setting contains only a name, the styles definition file will be loaded from the ""styles"" folder inside the styles plugin folder.
Otherwise, this setting has the ""name:url"" syntax, making it possible to set the URL from which loading the styles file.
Previously this setting was available as config.stylesCombo_stylesSet")]
		[DefaultValue("default")]
		[Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
		public string CKStylesSet
		{
			get
			{
				string retVal = string.Empty;
				foreach (string item in this.CKConfig.stylesSet) retVal += item + ",";
				if (retVal.EndsWith(",")) retVal = retVal.Remove(retVal.Length - 1);
				return retVal;
			}
			set
			{
				if (value.Contains("{") && value.Contains("}"))
					this.CKConfig.stylesSet = new string[] { value };
				else
				{
					this.CKConfig.stylesSet = value.Replace("\t", string.Empty).Split(new char[] { ',', '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
					this.DoDesignerUpdate();
				}
			}
		}

		[Category("CKEditor Other Settings")]
		[Description(@"Intructs the editor to add a number of spaces (&nbsp;) to the text when hitting the TAB key.
If set to zero, the TAB key will be used to move the cursor focus to the next element in the page, out of the editor focus.")]
		[DefaultValue(0)]
		public int CKTabSpaces 
		{ 
			get { return this.CKConfig.tabSpaces; }
			set
			{
				this.CKConfig.tabSpaces = value;
				this.DoDesignerUpdate();
			} 
		}

		[PersistenceMode(PersistenceMode.Attribute)]
		[Category("CKEditor Other Settings")]
		[Description("The list of templates definition files to load.")]
		[DefaultValue("~/ckeditor/plugins/templates/templates/default.js")]
		[Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
		public string CKTemplatesFiles
		{
			get
			{
				string retVal = string.Empty;
				foreach (string item in this.CKConfig.templates_files) retVal += item + ",";
				if (retVal.EndsWith(",")) retVal = retVal.Remove(retVal.Length - 1);
				return retVal;
			}
			set 
			{
				this.CKConfig.templates_files = value.Replace("\t", string.Empty).Split(new char[] { ',', '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description(@"Whether the ""Replace actual contents"" checkbox is checked by default in the Templates dialog.")]
		[DefaultValue(true)]
		public bool CKTemplatesReplaceContent 
		{ 
			get { return this.CKConfig.templates_replaceContent; }
			set
			{
				this.CKConfig.templates_replaceContent = value;
				this.DoDesignerUpdate();
			} 
		}

		[PersistenceMode(PersistenceMode.Attribute)]
		[Category("CKEditor Basic Settings")]
		[Description("The toolbox (alias toolbar) definition. It is a toolbar name or an array of toolbars (strips), each one being also an array, containing a list of UI items.")]
		[DefaultValue("Full")]
		[Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
		public string CKToolbar
		{
			get
			{
				if (this.CKConfig.toolbar is string) return (string)this.CKConfig.toolbar;
				string retVal = string.Empty;
				try
				{
					string[] retValTab = new string[((object[])this.CKConfig.toolbar).Length];
					object[] ob = (object[])this.CKConfig.toolbar;
					for (int i = 0; i < ob.Length; i++)
					{
						object item = ob[i];
						if (item is string) retValTab[i] = (string)item;
						else
						{
							object[] itemTab = (object[])item;
							string concatValue = string.Empty;
							for (int j = 0; j < itemTab.Length; j++)
								concatValue += (string)itemTab[j] + "|";
							if (!string.IsNullOrEmpty(concatValue))
								concatValue = concatValue.Remove(concatValue.Length - 1);
							retValTab[i] = concatValue;
						}
					}
					foreach (string item in retValTab) retVal += item + "\r\n";
					if (retVal.EndsWith("\r\n")) retVal = retVal.Remove(retVal.Length - 2);
				}
				catch { }
				return retVal;
			}
			set
			{
				value = value.Trim();
				if (value.StartsWith("[") && value.EndsWith("]"))
				{
					this.CKConfig.toolbar = value.Replace("\t", string.Empty).Replace("\r", string.Empty).Replace("\n", string.Empty);
					return;
				}
				string[] valueTab = value.Split(new string[] { "\r\n" }, int.MaxValue, StringSplitOptions.RemoveEmptyEntries);
				if (valueTab.Length == 1 && !valueTab[0].Contains("|"))
				{
					this.CKConfig.toolbar = valueTab[0];
					return;
				}
				object[] retVal = new object[valueTab.Length];
				try
				{
					for (int i = 0; i < valueTab.Length; i++)
					{
						string[] item = valueTab[i].Split(new char[] { '|' });
						for (int k = 0; k < item.Length; k++)
							item[k] = item[k].Trim();
						if (item.Length == 1 && item[0] == "/") retVal[i] = item[0];
						else retVal[i] = item;
					}
				}
				catch { }
				this.CKConfig.toolbar = retVal;
				this.DoDesignerUpdate();
			}
		}

		[PersistenceMode(PersistenceMode.Attribute)]
		[Category("CKEditor Basic Settings")]
		[Description(@"The toolbar definition. It is an array of toolbars (strips), each one being also an array, containing a list of UI items.
Note that this setting is composed by ""toolbar_"" added by the toolbar name, which in this case is called ""Basic"".
This second part of the setting name can be anything. You must use this name in the CKEDITOR.config.toolbar setting,
so you instruct the editor which toolbar_(name) setting to you.")]
		[DefaultValue("Bold|Italic|-|NumberedList|BulletedList|-|Link|Unlink|-|About")]
		[Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
		public string CKToolbarBasic
		{
			get
			{
				string retVal = string.Empty;
				try
				{
					string[] retValTab = new string[this.CKConfig.toolbar_Basic.Length];
					object[] ob = this.CKConfig.toolbar_Basic;
					for (int i = 0; i < ob.Length; i++)
					{
						object item = ob[i];
						if (item is string) retValTab[i] = (string)item;
						else
						{
							object[] itemTab = (object[])item;
							string concatValue = string.Empty;
							for (int j = 0; j < itemTab.Length; j++)
								concatValue += (string)itemTab[j] + "|";
							if (!string.IsNullOrEmpty(concatValue))
								concatValue = concatValue.Remove(concatValue.Length - 1);
							retValTab[i] = concatValue;
						}
					}
					foreach (string item in retValTab) retVal += item + "\r\n";
					if (retVal.EndsWith("\r\n")) retVal = retVal.Remove(retVal.Length - 2);
				}
				catch { }
				return retVal;
			}
			set
			{
				value = value.Trim();
				if (value.StartsWith("[") && value.EndsWith("]"))
				{
					this.CKConfig.toolbar_Basic = new object[] { value.Replace("\t", string.Empty).Replace("\r", string.Empty).Replace("\n", string.Empty) };
					return;
				}
				string[] valueTab = value.Split(new string[] { "\r\n" }, int.MaxValue, StringSplitOptions.RemoveEmptyEntries);
				object[] retVal = new object[valueTab.Length];
				try
				{
					for (int i = 0; i < valueTab.Length; i++)
					{
						string[] item = valueTab[i].Split(new char[] { '|' });
						for (int k = 0; k < item.Length; k++)
							item[k] = item[k].Trim();
						if (item.Length == 1 && item[0] == "/") retVal[i] = item[0];
						else retVal[i] = item;
					}
				}
				catch { }
				this.CKConfig.toolbar_Basic = retVal;
				this.DoDesignerUpdate();
			}
		}

		[PersistenceMode(PersistenceMode.Attribute)]
		[Category("CKEditor Basic Settings")]
		[Description("This is the default toolbar definition used by the editor. It contains all editor features.")]
		[DefaultValue(@"Source|-|Save|NewPage|Preview|-|Templates
Cut|Copy|Paste|PasteText|PasteFromWord|-|Print|SpellChecker|Scayt
Undo|Redo|-|Find|Replace|-|SelectAll|RemoveFormat
Form|Checkbox|Radio|TextField|Textarea|Select|Button|ImageButton|HiddenField
/
Bold|Italic|Underline|Strike|-|Subscript|Superscript
NumberedList|BulletedList|-|Outdent|Indent|Blockquote|CreateDiv
JustifyLeft|JustifyCenter|JustifyRight|JustifyBlock
BidiLtr|BidiRtl
Link|Unlink|Anchor
Image|Flash|Table|HorizontalRule|Smiley|SpecialChar|PageBreak|Iframe
/
Styles|Format|Font|FontSize
TextColor|BGColor
ShowBlocks|-|About")]
		[Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
		public string CKToolbarFull
		{
			get
			{
				string retVal = string.Empty;
				try
				{
					string[] retValTab = new string[this.CKConfig.toolbar_Full.Length];
					object[] ob = this.CKConfig.toolbar_Full;
					for (int i = 0; i < ob.Length; i++)
					{
						object item = ob[i];
						if (item is string) retValTab[i] = (string)item;
						else
						{
							object[] itemTab = (object[])item;
							string concatValue = string.Empty;
							for (int j = 0; j < itemTab.Length; j++)
								concatValue += (string)itemTab[j] + "|";
							if (!string.IsNullOrEmpty(concatValue))
								concatValue = concatValue.Remove(concatValue.Length - 1);
							retValTab[i] = concatValue;
						}
					}
					foreach (string item in retValTab) retVal += item + "\r\n";
					if (retVal.EndsWith("\r\n")) retVal = retVal.Remove(retVal.Length - 2);
				}
				catch { }
				return retVal;
			}
			set
			{
				value = value.Trim();
				if (value.StartsWith("[") && value.EndsWith("]"))
				{
					this.CKConfig.toolbar_Full = new object[] { value.Replace("\t", string.Empty).Replace("\r", string.Empty).Replace("\n", string.Empty) };
					return;
				}
				string[] valueTab = value.Split(new string[] { "\r\n" }, int.MaxValue, StringSplitOptions.RemoveEmptyEntries);
				object[] retVal = new object[valueTab.Length];
				try
				{
					for (int i = 0; i < valueTab.Length; i++)
					{
						string[] item = valueTab[i].Split(new char[] { '|' });
						for (int k = 0; k < item.Length; k++)
							item[k] = item[k].Trim();
						if (item.Length == 1 && item[0] == "/") retVal[i] = item[0];
						else retVal[i] = item;
					}
				}
				catch { }
				this.CKConfig.toolbar_Full = retVal;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description("This is the default toolbar definition used by the editor. It contains all editor features.")]
		[DefaultValue(true)]
		public bool CKToolbarCanCollapse 
		{ 
			get { return this.CKConfig.toolbarCanCollapse; }
			set
			{
				this.CKConfig.toolbarCanCollapse = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description(@"The ""theme space"" to which rendering the toolbar. For the default theme, the recommended options are ""top"" and ""bottom"".")]
		[DefaultValue(typeof(ToolbarLocation), "Top")]
		public ToolbarLocation CKToolbarLocation 
		{ 
			get { return this.CKConfig.toolbarLocation; }
			set
			{
				this.CKConfig.toolbarLocation = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description("Whether the toolbar must start expanded when the editor is loaded.")]
		[DefaultValue(true)]
		public bool CKToolbarStartupExpanded 
		{ 
			get { return this.CKConfig.toolbarStartupExpanded; }
			set
			{
				this.CKConfig.toolbarStartupExpanded = value;
				this.DoDesignerUpdate();
			} 
		}

		[Category("CKEditor Other Settings")]
		[Description(@"Indicates that some of the editor features, like alignment and text direction, should used the ""computed value""
of the featureto indicate it's on/off state, instead of using the ""real value"".
If enabled, in a left to right written document, the ""Left Justify"" alignment button will show as active,
even if the aligment style is not explicitly applied to the current paragraph in the editor.")]
		[DefaultValue(true)]
		public bool CKUseComputedState 
		{ 
			get { return this.CKConfig.useComputedState; }
			set
			{
				this.CKConfig.useComputedState = value;
				this.DoDesignerUpdate();
			} 
		}

		[TypeConverter(typeof(WebColorConverter))]
		[Category("CKEditor Basic Settings")]
		[Description("Specifies the color of the user interface. Works only with the Kama skin.")]
		[DefaultValue(typeof(System.Drawing.Color), "LightGray")]
		public System.Drawing.Color CKUIColor
		{
			get
			{
				if (this.CKConfig.uiColor == "#D3D3D3") return System.Drawing.Color.FromName("LightGray");
				return System.Drawing.ColorTranslator.FromHtml(this.CKConfig.uiColor);
			}
			set 
			{
				this.CKConfig.uiColor = "#" + value.R.ToString(@"x2") + value.G.ToString(@"x2") + value.B.ToString(@"x2");
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description("To attach a function to CKEditor events.")]
		[DefaultValue(null)]
		public List<object> CKEditorEventHandler 
		{ 
			get { return this.CKConfig.CKEditorEventHandler; }
			set
			{
				this.CKConfig.CKEditorEventHandler = value;
				this.DoDesignerUpdate();
			}
		}

		[Category("CKEditor Other Settings")]
		[Description("To attach a function to an event in a single editor instance")]
		[DefaultValue(null)]
		public List<object> CKEditorInstanceEventHandler 
		{ 
			get { return this.CKConfig.CKEditorInstanceEventHandler; }
			set
			{
				this.CKConfig.CKEditorInstanceEventHandler = value;
				this.DoDesignerUpdate();
			}
		}
		#endregion

		#region Properties 
		[Description("The value of CKEditor.")]
		[DefaultValue(null)]
		//[System.ComponentModel.Localizable(true)]
		[System.ComponentModel.Bindable(true)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				if (base.Text != value)
				{
					base.Text = value;
					// As we have no control whatsoever over the contents of the Html being loaded
					// the safest way to guarantee it will reach CKEditor as intended, it's base64encoded
					// in all communications between server and client.
					this.InvokeMethodWithId("CKEditor_InvokeWrapper", "SetData", base64Encode( value), null, this.CKInvocationDelay );
				}
			}
		}

		[DefaultValue(0)]
		[Category("CKEditor Basic Settings")]
		[Description("Milliseconds to delay server-client invocations to allow CKEditor to (re)load it's resources")]
		public int CKInvocationDelay
		{
			get
			{
				return mintInvokeMethodDelay;
			}
			set
			{
				if (value < 0)
					mintInvokeMethodDelay = 0;
				else
					mintInvokeMethodDelay = value;
			}
		}


		#endregion 

		#region Methods


		/// <summary>
		/// Raises the <see cref="E:GotFocus"/> event.
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		internal protected virtual void OnCKGotFocus(EventArgs e)
		{

			EventHandler objEventHandler = this.CKGotFocusHandler;
			if (objEventHandler != null)
			{
				objEventHandler(this, new EventArgs());
			}
		}

		/// <summary>
		/// Raises the <see cref="E:LostFocus"/> event.
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void OnCKLostFocus(EventArgs e)
		{
			EventHandler objEventHandler = this.CKLostFocusHandler;
			if (objEventHandler != null)
			{
				objEventHandler(this, new EventArgs());
			}
		}

        /// <summary>
        /// Raises the ForcedSave event
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnCKForcedSave(EventArgs e)
        {
            EventHandler objEventHandler = this.CKForcedSaveHandler;
            if (objEventHandler != null)
            {
                objEventHandler(this, new EventArgs());
            }
        }
		#endregion 

		#region Events

		protected override EventTypes GetCriticalEvents()
		{
			EventTypes enmEvents = base.GetCriticalEvents();

			if (this.CKGotFocusHandler  != null)
			{
				enmEvents |= EventTypes.GotFocus;
			}
			if (this.CKLostFocusHandler != null)
			{
				enmEvents |= EventTypes.LostFocus;
			}
            if (this.CKForcedSaveHandler != null)
            {
                enmEvents |= EventTypes.AfterLabelEdit;  // We use AfterLabelEdit event type for our event.
            }   
			return enmEvents;
		}
		/// <summary>
		/// Occurs when the control recives focus.
		/// </summary>
		public event EventHandler CKGotFocus
		{
			add
			{
				this.AddCriticalHandler(CKEditor.CKGotFocusEvent, value);
			}
			remove
			{
				this.RemoveCriticalHandler(CKEditor.CKGotFocusEvent, value);
			}
		}

		/// <summary>
		/// Gets the hanlder for the GotFocus event.
		/// </summary>
		private EventHandler CKGotFocusHandler
		{
			get
			{
				return (EventHandler)this.GetHandler(CKEditor.CKGotFocusEvent);
			}
		}

		/// <summary>
		/// The GotFocus event registration.
		/// </summary>
		private static readonly SerializableEvent CKGotFocusEvent = SerializableEvent.Register("CKGotFocus", typeof(EventHandler), typeof(CKEditor ));



		/// <summary>
		/// Occurs when the control loses focus.
		/// </summary>
		public event EventHandler CKLostFocus
		{
			add
			{
				this.AddCriticalHandler(CKEditor.CKLostFocusEvent, value);
			}
			remove
			{
				this.RemoveCriticalHandler(CKEditor.CKLostFocusEvent, value);
			}
		}

		/// <summary>
		/// Gets the hanlder for the LostFocus event.
		/// </summary>
		private EventHandler CKLostFocusHandler
		{
			get
			{
				return (EventHandler)this.GetHandler(CKEditor.CKLostFocusEvent);
			}
		}

        /// <summary>
        /// The LostFocus event registration.
        /// </summary>
        private static readonly SerializableEvent CKLostFocusEvent = SerializableEvent.Register("CKLostFocus", typeof(EventHandler), typeof(CKEditor));

        /// <summary>
        /// Occurs when the InvokeSave called, but there was no text change
        /// </summary>
        public event EventHandler CKForcedSave
        {
            add
            {
                this.AddCriticalHandler(CKEditor.CKForcedSaveEvent, value);
            }
            remove
            {
                this.RemoveCriticalHandler(CKEditor.CKForcedSaveEvent, value);
            }
        }

        /// <summary>
        /// Gets the hanlder for the ForcedSave event.
        /// </summary>
        private EventHandler CKForcedSaveHandler
        {
            get
            {
                return (EventHandler)this.GetHandler(CKEditor.CKForcedSaveEvent);
            }
        }

        /// <summary>
        /// The ForcedSave event registration.
        /// </summary>
        private static readonly SerializableEvent CKForcedSaveEvent = SerializableEvent.Register("CKForcedSave", typeof(EventHandler), typeof(CKEditor));


		protected override void FireEvent(IEvent objEvent)
		{
		  switch (objEvent.Type)
		  {
			  case "CKUnload":
				  break;

			case "ValueChange":
				// Set base.Text to prevent the implicit update to client from happening, which would trigger multiple loops.
                // Setting base.Text will call OnTextChanged(), so no need to make that call here.
				base.Text = base64Decode( objEvent[Gizmox.WebGUI.WGAttributes.Text]);
				// OnTextChanged(EventArgs.Empty);
				break;
            case "ForcedSave":
                // InvokeSave called, but Text value has not changed.
                this.OnCKForcedSave(EventArgs.Empty);
                break;
			case "ModeChanged":
				mstrEditorMode = objEvent["NewMode"];
				break;
			case "CKReadOnly":
				if (objEvent["ReadOnly"] == "true")
					mboolEditorReadOnly = true;
				else
					mboolEditorReadOnly = false;
				break;
			case "CKGotFocus":
				// Raise the got focus event.
				this.OnCKGotFocus(EventArgs.Empty);
				break;
			case "CKLostFocus":
				// Raise the got focus event.
				this.OnCKLostFocus(EventArgs.Empty);
				break;
			default:
				base.FireEvent(objEvent);
				break;
			}
		}


		#endregion

		#region Rendering 

		/// <summary>
		/// Gets the source.
		/// </summary>
		/// <value>The source.</value>
		protected override string Source
		{
			get
			{
				CKEditorSkin objCKEditor = this.Skin as CKEditorSkin;
				if (objCKEditor != null)
				{
					return string.Format("Resources.{0}{1}?source={2}/{3}&id={4}",
							objCKEditor.HtmlResource.ToString(),
							this.Context.Config.DynamicExtension,
							this.ExpandRelativePath (this.CKBasePath),
							this.CKScript,
							this.ID.ToString());
				}

				return base.Source;
			}
		}

		// Custom ClientUpdateHandler that does nothing is needed to prevent unnecessary 
		// redraws of our custom control.
		protected override string ClientUpdateHandler
		{
			get
			{
				return "CKEditor_CUH";
			}
		}
		protected override void RenderUpdatedAttributes(IContext objContext, IAttributeWriter objWriter, long lngRequestID)
		{
			base.RenderUpdatedAttributes(objContext, objWriter, lngRequestID);

			RenterCKAttributes(objWriter, lngRequestID, false, objContext );
		}

		protected override void RenderAttributes(IContext objContext, IAttributeWriter objWriter)
		{
			base.RenderAttributes(objContext, objWriter);

			RenterCKAttributes(objWriter, 0, true, objContext );
		}

		// Remove the Maximize Toolbar command, should it be on current toolbar.
		private Array RemoveMaximize( Array orgArray)
		{
			if (orgArray == null)
				return orgArray ;

			for (int i = 0; i < orgArray.Length; i++)
			{
				object objValue = orgArray.GetValue(i);
				if (objValue.GetType() == typeof(Array) || objValue.GetType() == (new Object[] {}).GetType())
				{
					orgArray.SetValue(RemoveMaximize((Array) orgArray.GetValue(i)), i);
				}
				else {
					if (orgArray.GetValue(i).GetType() == typeof(string))
					{
						string strValue =  ((string)orgArray.GetValue(i)).ToLower();
						if (strValue == "maximize")
							orgArray.SetValue("", i);
					}
				}
			}
			return orgArray;
		}

		private void RenterCKAttributes(IAttributeWriter objWriter, long lngRequestID, bool blnForceRender, IContext objContext)
		{
			string strText = this.Text;

			if (blnForceRender || this.IsDirtyAttributes(lngRequestID, AttributeType.Control))
			{

				// objWriter.WriteAttributeString(WGAttributes.Text, "<p>Simple text</p>");
				if (blnForceRender)
				{
					// We only need to render Text attribute on full updates, as updating it handles it's update seperately.
					// Removes the need to call this.Update() when Text property is set server-side.
					objWriter.WriteAttributeString(Gizmox.WebGUI.WGAttributes.Text, base64Encode( strText));

					// Before rendering config, go through all web paths and expand those who are relative
					for (int i = 0; i < this.CKConfig.contentsCss.Length; i++)
						this.CKConfig.contentsCss[i] = this.ExpandRelativePath(this.CKConfig.contentsCss[i]);
					this.CKConfig.filebrowserBrowseUrl = this.ExpandRelativePath(this.CKConfig.filebrowserBrowseUrl);
					this.CKConfig.filebrowserFlashBrowseUrl = this.ExpandRelativePath(this.CKConfig.filebrowserFlashBrowseUrl);
					this.CKConfig.filebrowserFlashUploadUrl = this.ExpandRelativePath(this.CKConfig.filebrowserFlashUploadUrl);
					this.CKConfig.filebrowserImageBrowseLinkUrl = this.ExpandRelativePath(this.CKConfig.filebrowserImageBrowseLinkUrl);
					this.CKConfig.filebrowserImageBrowseUrl = this.ExpandRelativePath(this.CKConfig.filebrowserImageBrowseUrl);
					this.CKConfig.filebrowserImageUploadUrl = this.ExpandRelativePath(this.CKConfig.filebrowserImageUploadUrl);
					this.CKConfig.filebrowserUploadUrl = this.ExpandRelativePath(this.CKConfig.filebrowserUploadUrl);
					for (int i = 0; i < this.CKConfig.templates_files.Length; i++)
						this.CKConfig.templates_files[i] = this.ExpandRelativePath(this.CKConfig.templates_files[i]);

					// We don't want Maximize to be available in the editor
					// we already set maximized and must keep it that way.
					// Client-side, we also ignore the maximize command to the editor, should it be invoked.
					Array cfgArray = null;
					if (this.CKConfig.toolbar.GetType() == typeof(Array) || this.CKConfig.toolbar.GetType() == (new Object[] {}).GetType())
						cfgArray = (Array) this.CKConfig.toolbar;
					else if (this.CKConfig.toolbar.GetType() == typeof(string))
					{
						if ((String)this.CKConfig.toolbar == "Full")
							cfgArray  = this.CKConfig.toolbar_Full;
						else if ((string)this.CKConfig.toolbar == "Basic")
							cfgArray  = this.CKConfig.toolbar_Basic;
					}
					if (cfgArray != null)
					{
						this.CKConfig.toolbar = RemoveMaximize(cfgArray);
					}
					objWriter.WriteAttributeString("CKCONFIG", JSONSerializer.ToJavaScriptObjectNotation(this.CKConfig));
				}
			}
		}

		#endregion 
	}
}