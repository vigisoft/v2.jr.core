﻿namespace SphinxClassLibrary.IO
{
    using System.IO;
    using System.Net;


    public class SphinxBinaryWriter : System.IO.BinaryWriter
    {
        private System.Text.Encoding encoding;

        public SphinxBinaryWriter(Stream _stream, string _encoding = "UTF-8")
            : base(_stream, System.Text.Encoding.GetEncoding(_encoding))
        {
            encoding = System.Text.Encoding.GetEncoding(_encoding);
        }

        bool _Private_Buffer = false;
        public SphinxBinaryWriter(string _encoding = "UTF-8")
            : base(new MemoryStream (), System.Text.Encoding.GetEncoding(_encoding))
        {
            encoding = System.Text.Encoding.GetEncoding(_encoding);
            _Private_Buffer = true;
        }

        //public Stream Stream { get { return BaseStream; } }

        public void WriteShort(short _v) { Write(IPAddress.HostToNetworkOrder(_v)); }

        public void WriteInt(int _v) { Write(IPAddress.HostToNetworkOrder(_v)); }

        public void WriteLong(long _v) { Write(IPAddress.HostToNetworkOrder(_v)); }

        public void WriteFloat(float _v)
        {
            IntFloatUnion ifu = new IntFloatUnion();
            ifu.f = _v;
            WriteInt(ifu.i);
        }

        public void WriteStr(string _v)
        {
            if (_v == null)
                WriteInt(0);
            else
            {
                byte[] bytes = encoding.GetBytes(_v);
                Write(IPAddress.HostToNetworkOrder(bytes.Length));
                Write(bytes);
            }
        }

        public void WriteStream(MemoryStream _s)
        {
            long pos = _s.Position;
            _s.Position = 0;
            _s.CopyTo(this.BaseStream);
            _s.Position = pos;
        }

        public byte[] Content
        {
            get
            {
                byte [] C = new byte [BaseStream.Length];
                BaseStream.Position = 0;
                this.BaseStream.Read(C, 0, (int)BaseStream.Length);
                return C;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_Private_Buffer) BaseStream.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
