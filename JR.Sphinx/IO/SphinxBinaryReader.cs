﻿namespace SphinxClassLibrary.IO
{
    using System.IO;
    using System.Net;


    public class SphinxBinaryReader : System.IO.BinaryReader
    {
        System.Text.Encoding Encoding = null;
        public SphinxBinaryReader(Stream _stream, string _encoding = "UTF-8")
            : base(_stream, System.Text.Encoding.GetEncoding(_encoding))
        {
            Encoding = System.Text.Encoding.GetEncoding(_encoding);
        }

        bool _Private_Buffer = false;
        public SphinxBinaryReader(byte [] Buffer, string _encoding = "UTF-8")
            : this(new MemoryStream (Buffer), _encoding)
        {
            _Private_Buffer = true;
        }

        //public Stream Stream { get { return BaseStream; } }

        public short ReadShort() { return IPAddress.NetworkToHostOrder(ReadInt16()); }

        public int ReadInt() { return IPAddress.NetworkToHostOrder(ReadInt32()); }

        public long ReadLong() { return IPAddress.NetworkToHostOrder(ReadInt64()); }

        public float ReadFloat()
        {
            IntFloatUnion ifu = new IntFloatUnion();
            ifu.i = IPAddress.NetworkToHostOrder(ReadInt32());
            return ifu.f;
        }

        public string ReadStr()
        {
            int c = IPAddress.NetworkToHostOrder(ReadInt32());
            return (c > 0) ? Encoding.GetString(ReadBytes(c)) : string.Empty;
        }

        protected override void  Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_Private_Buffer) BaseStream.Dispose ();
            }
 	        base.Dispose(disposing);
        }
    }
}
