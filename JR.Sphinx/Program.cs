﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SphinxClassLibrary;

namespace SphinxTest
{
    class Program
    {
        static void Main(string[] args)
        {
            SphinxClient target = new SphinxClient("www.photo12.com", 9312);
            //SphinxClient target = new SphinxClient();
            target.Mode = MatchModes.SPH_MATCH_EXTENDED;
            target.Limit = 1000;
            target.MaxMatches = 1000;
            Console.WriteLine("Nom de la base (distrib / site ) ? ");
            Console.Write(">> ");
            string Base = Console.ReadLine();
            if (Base == "") return;
            do
            {
                Console.Write(">> ");
                string Q = Console.ReadLine();
                if (Q == "") break;
                //target.SelectList = "*, YEAR(NOW()) AS YY";
                //target.AddFilterRange ("YY", 2010L, 2012L, false);

                Result r = target.Query(Q, Base);
                StringBuilder SB = new StringBuilder();
                foreach (var R in r.Matches)
                {
                    SB.Append(R.Id);
                    foreach (var A in R.Attributes)
                    {
                        SB.AppendFormat(" | {0}:{1}", A.Key, A.Value.RawValue);
                    }
                    SB.AppendLine();
                }
                Console.WriteLine(SB);
                Console.WriteLine("total:{0}", r.TotalFound.ToString());
                foreach (var W in r.Words)
                {
                    Console.WriteLine("word:{0}", W.Word);
                }
            } while (true);
        }
    }
}
