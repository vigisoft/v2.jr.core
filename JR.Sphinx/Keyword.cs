﻿namespace SphinxClassLibrary
{
    public class Keyword
    {
        public string Tokenized { get; set; }
        public string Normalized { get; set; }
        public int NumDocs { get; set; }
        public int NumHits { get; set; }

        public override string ToString() => $"Tokenized={Tokenized} Normalized={Normalized} NumDocs={NumDocs} NumHits={NumHits}";
    }
}
