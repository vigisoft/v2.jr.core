﻿namespace SphinxClassLibrary.Net
{
    using System;
    using System.Net.Sockets;
    using SphinxClassLibrary.IO;
using System.IO;


    class SphinxConnection : TcpClient
    {

        public SphinxBinaryWriter Writer {get;set;}
        public SphinxBinaryReader Reader {get;set;}

        public SphinxConnection(string _host, int _port, string Encoding)
        {
            if (_host == "") _host = "127.0.0.1";   // Sinon pb de résolution dns avec localhost prend 1 seconde
            Connect(_host, _port);
            Writer = new SphinxBinaryWriter(GetStream(), Encoding);
            Reader = new SphinxBinaryReader(GetStream(), Encoding);
            Writer.WriteInt(1); // dummy write (Nagle)
            if (Reader.ReadInt() < 1) throw new SphinxClientException("Server version < 1");
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposing) return;
            Writer.Close();
            Reader.Close();
            if (Connected) Close();

        }

        public void Write(byte [] Buffer)
        {
           GetStream().Write (Buffer,0, Buffer.Length);
        }

    }
}
