﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Specialized;
using JR.Pop3;

namespace JR.Mail
{
    [Serializable]
    public class SMailMessage
    {
        [Serializable]
        public class BinAttachment
        {
            public byte [] Content;
            public string Name;
            public string MediaType;
            public string ContentId;
        }

        [Serializable]
        public class BinAlternateView
        {
            public string Body;
            public Uri BaseUri;
            public string MediaType;
            public string ContentId;
        }

        public string Subject;
        public System.Text.Encoding SubjectEncoding;
        public bool IsBodyHtml;
        public string Body;
        public string From;
        public string Sender;
        public string From_Name;
        public string Sender_Name;
        public System.Text.Encoding BodyEncoding;
        public List<BinAttachment> Attachments = new List<BinAttachment>();
        public List<BinAlternateView> AlternateViews = new List<BinAlternateView>();

        // Recherche du contenu alternatif dans le cas d'un double encodage text / html
        static void Find_Alternatives(RxMailMessage RM, Action<RxMailMessage> On_Found)
        {
            foreach (RxMailMessage M in RM.Entities)
            {
                if (M.MediaMainType == "text")
                {
                    On_Found(M);
                } else
                {
                    Find_Alternatives(M, On_Found);
                }
            }
        }

        public static byte[] Serialize_From(RxMailMessage Mess)
        {
            using (System.IO.MemoryStream mStr = new System.IO.MemoryStream())
            {
                try
                {
                    SMailMessage BM = new SMailMessage();
                    BM.Subject = Mess.Subject;
                    BM.SubjectEncoding = Mess.SubjectEncoding;
                    BM.IsBodyHtml = Mess.IsBodyHtml;
                    BM.Body = Mess.Body;
                    BM.BodyEncoding = Mess.BodyEncoding;
                    //foreach (AlternateView E in Mess.AlternateViews)
                    //{
                    //    var BA = new BinAttachment();
                    //    BA.Content = JR.Files.Full_Stream(E.ContentStream);
                    //    BA.Name = "!alternate";
                    //    BA.MediaType = E.ContentType.MediaType;
                    //    BA.ContentId = E.ContentId;
                    //    BM.Attachments.Add(BA);
                    //}

                    Action<RxMailMessage> Treat_Alternative = (E) =>
                    {
                        BinAlternateView BA = new BinAlternateView();
                        BA.Body = E.Body;   // On récupère le body et non le stream sinon ca marche pas
                        BA.MediaType = E.ContentType.MediaType;
                        BA.ContentId = E.ContentId;
                        BM.AlternateViews.Add(BA);
                    };

                    Find_Alternatives(Mess, Treat_Alternative);
                    foreach (Attachment AT in Mess.Attachments)
                    {
                        BinAttachment BA = new BinAttachment();
                        BA.Content = JR.Files.Full_Stream(AT.ContentStream);
                        BA.Name = AT.Name;
                        BA.MediaType = AT.ContentType.MediaType;
                        BA.ContentId = AT.ContentId;
                        BM.Attachments.Add(BA);
                    }

                    BinaryFormatter format = new BinaryFormatter();
                    format.Serialize(mStr, BM);
                }
                catch { }
                return mStr.GetBuffer();
            }
        }

        public static byte[] Serialize_From_Mail (MailMessage Mess)
        {
            using (System.IO.MemoryStream mStr = new System.IO.MemoryStream())
            {
                try
                {
                    SMailMessage BM = new SMailMessage();
                    BM.Subject = Mess.Subject;
                    BM.SubjectEncoding = Mess.SubjectEncoding;
                    BM.IsBodyHtml = Mess.IsBodyHtml;
                    BM.Body = Mess.Body;
                    BM.BodyEncoding = Mess.BodyEncoding;
                    BM.From = Mess.From.Address;
                    BM.Sender = Mess.Sender.Address;
                    BM.From_Name = Mess.From.DisplayName;
                    BM.Sender_Name = Mess.Sender.DisplayName;
                    foreach (AlternateView E in Mess.AlternateViews)
                    {
                        var BA = new BinAttachment();
                        BA.Content = JR.Files.Full_Stream(E.ContentStream);
                        BA.Name = "!alternate";
                        BA.MediaType = E.ContentType.MediaType;
                        BA.ContentId = E.ContentId;
                        BM.Attachments.Add(BA);
                        foreach (var AT in E.LinkedResources)
                        {
                            BinAttachment LR = new BinAttachment();
                            LR.Content = JR.Files.Full_Stream(AT.ContentStream);
                            LR.Name = "!linked";
                            LR.MediaType = AT.ContentType.MediaType;
                            LR.ContentId = AT.ContentId;
                            BM.Attachments.Add(LR);
                        }
                    }
                    foreach (Attachment AT in Mess.Attachments)
                    {
                        BinAttachment BA = new BinAttachment();
                        BA.Content = JR.Files.Full_Stream(AT.ContentStream);
                        BA.Name = AT.Name;
                        BA.MediaType = AT.ContentType.MediaType;
                        BA.ContentId = AT.ContentId;
                        BM.Attachments.Add(BA);
                    }
                    BinaryFormatter format = new BinaryFormatter();
                    format.Serialize(mStr, BM);
                }
                catch { }
                return mStr.GetBuffer();
            }
        }

        public static MailMessage Create_From(byte[] Data)
        {
            MailMessage M = new MailMessage();
            try
            {
                BinaryFormatter format = new BinaryFormatter();
                System.IO.MemoryStream mStr = new System.IO.MemoryStream(Data);
                SMailMessage BM = (SMailMessage)format.Deserialize(mStr);
                M.Subject = BM.Subject;
                M.SubjectEncoding = BM.SubjectEncoding;
                if (BM.Sender != null) M.Sender = new MailAddress(BM.Sender, BM.Sender_Name);
                if (BM.From != null) M.From = new MailAddress(BM.From, BM.From_Name);
                AlternateView Html_View = null;
                foreach (BinAlternateView BA in BM.AlternateViews)
                {
                    var AV = AlternateView.CreateAlternateViewFromString(BA.Body, null, BA.MediaType);
                    if (BA.MediaType.Contains("html")) Html_View = AV;
                    AV.ContentId = BA.ContentId;
                    AV.BaseUri = BA.BaseUri;
                    M.AlternateViews.Add(AV);
                }
                {
                    AlternateView AV = null;
                    foreach (BinAttachment BA in BM.Attachments.ToArray())
                    {
                        if (BA.Name == "!alternate")
                        {
                            AV = new AlternateView(new MemoryStream(BA.Content), BA.MediaType);
                            if (BA.MediaType.Contains("html")) Html_View = AV;
                            AV.ContentId = BA.ContentId;
                            M.AlternateViews.Add(AV);
                        }
                        else if (AV != null && BA.Name == "!linked") // Image liée à l'alternateview
                        {
                            var LR = new LinkedResource(new MemoryStream(BA.Content), BA.MediaType) { ContentId = BA.ContentId, TransferEncoding = System.Net.Mime.TransferEncoding.Base64 };
                            AV.LinkedResources.Add(LR);
                        }
                        else if (Html_View != null && BA.ContentId.Contains("@")) // Image liée au body html
                        {
                            var LR = new LinkedResource(new MemoryStream(BA.Content), BA.MediaType) { ContentId = BA.ContentId, TransferEncoding = System.Net.Mime.TransferEncoding.Base64 };
                            Html_View.LinkedResources.Add(LR);
                        }
                        else
                        {
                            var AT = new Attachment(new MemoryStream(BA.Content), BA.Name, BA.MediaType);
                            AT.ContentId = BA.ContentId;
                            M.Attachments.Add(AT);
                        }
                    }
                }
                if (M.AlternateViews.Count == 0)  // !!!! important : Encodage du body que si pas d'alternative
                {
                    M.IsBodyHtml = BM.IsBodyHtml;
                    M.Body = BM.Body;
                    M.BodyEncoding = BM.BodyEncoding;
                }
            }
            catch { };
            return M;
        }


    }

}
