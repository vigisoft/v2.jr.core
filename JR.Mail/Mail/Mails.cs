using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Threading;

namespace JR
{
    public class Mails
    {
        public static string Get_Type (string File_Name)
        {
            switch (JR.Files.Get_Extension(File_Name).ToUpper())
            {
                case ".JPG":
                case ".JPEG":
                    return MediaTypeNames.Image.Jpeg;
                case ".GIF":
                    return MediaTypeNames.Image.Gif;
                case ".TIFF":
                    return MediaTypeNames.Image.Tiff;
                case ".HTM":
                case ".HTML":
                    return MediaTypeNames.Text.Html;
                case ".XML":
                    return MediaTypeNames.Text.Xml;
                case ".TXT":
                case ".LOG":
                    return MediaTypeNames.Text.Plain;
                case ".ZIP":
                    return MediaTypeNames.Application.Zip;
                case ".PDF":
                    return MediaTypeNames.Application.Pdf;
                case ".RTF":
                    return MediaTypeNames.Application.Rtf;
                case ".XLS":
                    return "application/excel";
            }
            return MediaTypeNames.Application.Octet;
        }

        // AJoute un fichier � un message
        public static void Attach_File_To_Mail (string File_Name, MailMessage Mail)
        {
            // Un bug de formatage des dates interne au framework (4.0) nous oblige � forcer temporairement une culture neutre
            // Sinon il met d�c pour d�cembre et �a plante
            CultureInfo CI = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            Attachment A  = new Attachment (File_Name, Get_Type (File_Name));
            A.ContentDisposition.CreationDate = File.GetCreationTime(File_Name);
            A.ContentDisposition.ModificationDate = File.GetLastWriteTime(File_Name);
            A.ContentDisposition.ReadDate = File.GetLastAccessTime(File_Name);
            Mail.Attachments.Add(A);
            Thread.CurrentThread.CurrentCulture = CI;
        }

        public static bool Is_Valid_Address(string Email)
        {
            return Regex.IsMatch(Email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"); 
        }

        public static void Contact(string Email)
        {
            try { System.Diagnostics.Process.Start(string.Format("mailto:{0}", Email)); }
            catch { }
        }
    }
}
