﻿using System;
using System.Collections.Generic;
using System.Linq;
using JR.DBO;
using JR.Saf;
using System.IO;
using System.Data;

namespace JR.CMS
{
    public class Resource
    {

        static Dictionary<string, string> Temp_Directories = new Dictionary<string, string>();

        public static void Delete_Cache(string File_Name)
        {
            JR.Files.Safe_Delete(File_Name);
            // Supprimer toutes les déclinaisons du cache
            try
            {
                var P = File_Name.Paths();
                string Dir_Name = Path.Combine(P.Directory, "cache");
                if (!Directory.Exists(Dir_Name)) return;
                string Pattern = P.File_Prefix + "-*" + P.Extension;
                foreach (var F in Directory.GetFiles(Dir_Name, Pattern))
                {
                    JR.Files.Safe_Delete(F);
                }
            }
            catch { }
        }

        public static string Import(string Source, string Destination, bool Move)
        {
            string Source_File = Main.Real_File_Name(Source);
            if (Source.Contains(@":TEMP\"))
            {
                string Temp_Dir = JR.Files.Get_Directory_Name (Source_File);
                Temp_Directories[Temp_Dir] = Temp_Dir;
            }
            string Dest_File = Main.Real_File_Name(Destination);
            // Copier source dans destination
            if (Move)
            {
                JR.Files.Safe_Move(Source_File, Dest_File);

                // Supprimer le cache
                Delete_Cache(Source_File);

            }
            else
            {
                if (Net_File.Is_Remote(Source))
                {
                    Net_File.Download(Source_File, Dest_File);
                }
                else
                {
                    JR.Files.Safe_Copy(Source_File, Dest_File);
                }
            }

            return "OK";
        }

        public static string Remove(string File_Name)
        {
            JR.Files.Safe_Delete(Main.Real_File_Name(File_Name));
            return "OK";
        }

        public static string Move(string Source, string Destination)
        {
            return Import(Source, Destination, true);
        }

        public static string Cleanup()
        {
            // Supprimer tous les fichiers référencés dans la table OBJ_Cleaner
            // ainsi que leur cache
            using (var DR = Model.Current.DB.New_Reader ("select * from OBJ_Cleaner"))
            {
                while (DR.Read_Atomic())
                {
                    long Id = DR.Next_Long;
                    string Id_Property = DR.Next_String;
                    string File_Name = Main.Real_File_Name(DR.Next_String);
                    Delete_Cache(File_Name);
                    Model.Current.DB.Execute("delete from OBJ_Cleaner where id=@0", Id);
                }
            }

            // Supprime tous les fichiers temporaires
            foreach (string D in Temp_Directories.Values)
            {
                foreach (string F in Directory.GetFiles (D))
                {
                    JR.Files.Safe_Delete(F);
                }
            }
            Temp_Directories.Clear();

            return "OK";
        }

        public static string List (string Path, bool Recurse)
        {
            string Dir = Main.Real_File_Name(Path);
            List<string> L = new List<string>();
            foreach (var FF in Directory.GetFiles(Dir, "*.*", Recurse?SearchOption.AllDirectories:SearchOption.TopDirectoryOnly))
            {
                L.Add(FF.Substring (Dir.Length));
            }
            L.Sort();
            return JR.Strings.UnSplit(L.ToArray());
        }
    }
}