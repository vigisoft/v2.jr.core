﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JR.DBO;

namespace JR.CMS
{
    public class Model:JRO_Model
    {
        public JRO_Class Cl_Config;
        public JRO_Class Cl_Link;
        public JRO_Class Cl_Request;

        public JRO_Class Cl_Fo_Config;
        public JRO_Class Cl_Fo_Request;

        public JRO_Class Cl_Selection;

        public JRO_Object Fo_Config;
        public JRO_Object Fo_Link;
        public JRO_Object Fo_Request;
        public JRO_Object Fo_Selection;

        public Model(string Key):base(Key)
        {
            CMS = this;
        }

        public override void Build ()
        {
            base.Build();
            Cl_Config = Def_Class("CF", "CMS_Config");
            Cl_Selection = Def_Class("CS", "CMS_Selection");
            Cl_Link = Def_Extended_Class("CL", "CMS_Link", "CMS_Link");
            Cl_Request = Def_Extended_Class("CR", "CMS_Request", "CMS_Request");
            Cl_Fo_Config = Cl_Fo.Def_Class("CF", "Config Folder", "C,CF");
            Cl_Fo_Request = Cl_Fo.Def_Class("CR", "Request Folder", "C,CR");
            Fo_Config = Cl_Fo_Config.Def_Object("CMS$CONFIG", Fo_App, "Config", "S");
            Fo_Link = Cl_Fo.Def_Object("CMS$LINKS", Fo_App, "Links", "S");
            Fo_Request = Cl_Fo.Def_Object("CMS$REQUESTS", Fo_App, "Requests", "S");
            Fo_Selection = Cl_Fo.Def_Object("CMS$SELECTIONS", Fo_App, "Selections", "S");
            Cl_Link.Default_Parent = Fo_Link;
            Cl_Selection.Default_Parent = Fo_Selection;
            Cl_Request.Default_Parent = Fo_Request;
        }

        public static Model CMS {get;private set;}

    }
}
