﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using JR.Graph;
using System.IO;
using System.Text;

namespace JR.CMS
{
    /// <summary>
    /// Description résumée de Jpg_Handler
    /// </summary>
    public class Res_Handler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            HttpResponse Response = context.Response;
            HttpRequest Req = context.Request;
            int Height = JR.Convert.To_Integer(context.Request["H"]);
            int Width = JR.Convert.To_Integer(context.Request["W"]);
            string Mode = JR.Strings.Official(context.Request["M"]);
            string Base_Path = context.Request.QueryString["P"]??"";
            string Mime_Type = context.Request.QueryString["T"];
            // Passer le /res
            JR.Strings.Cursor C = new Strings.Cursor(context.Request.CurrentExecutionFilePath);
            C.Next_Word ("/res");
            string Base_File = C.Tail();
            string File_Name = JR.Saf.Main.Real_File_Name(Base_Path + Base_File);
            System.IO.FileInfo FI = new System.IO.FileInfo(File_Name);

            // Retourne les infos de base sur le fichier
            if (Mode == "INFO")
            {
                long F_Size = -1;
                DateTime F_Date = DateTime.MinValue;
                int F_Width = 0;
                int F_Height = 0;

                if (FI.Exists)
                {
                    F_Size = FI.Length;
                    F_Date = FI.LastWriteTimeUtc;
                    JR.Graph.Images.Get_Image_Info(File_Name, out F_Width, out F_Height);
                }
                Response.Write($"S={F_Size};D={JR.Convert.To_String(F_Date)};W={F_Width};H={F_Height}");
                Response.End();
                return;
            }
            if (!FI.Exists)
            {
                Response.StatusCode = 404;
                Response.StatusDescription = $"Bad resource [{File_Name}]";
                Response.End ();
                return;
            }
            var P = File_Name.Paths();
            string Extension = P.Extension.ToLower();
            if (Height + Width > 0)
            {
                string Directory = P.Directory;
                string Prefix = P.File_Prefix;
                string Size_Name = $"{Directory}/cache/{Prefix}-{Width}x{Height}{Mode}{Extension}";
                if (File.Exists(Size_Name))
                {
                    File_Name = Size_Name;
                }
                else if (JR.Graph.Images.Resize_Image(File_Name, Size_Name, Width, Height, Mode:Mode))
                {
                    File_Name = Size_Name;
                }
                // sinon on renvoie l'original
            }
            if (Mime_Type != null)
            {
                Response.ContentType = Mime_Type;
            } else
            {
                switch (Extension)
                {
                    case ".jpg":
                    case ".pgn":
                    case ".gif":
                    case ".jpeg":
                        Response.ContentType = "image";
                        break;
                    default:
                        break;
                }
            }
            Response.Cache.SetCacheability(HttpCacheability.Public);
            Response.Cache.SetMaxAge (new TimeSpan(10,0,0,0));
            Response.TransmitFile(File_Name);
            Response.End();
        }

        public bool IsReusable => true;
        public static void Use() { }
    }
}