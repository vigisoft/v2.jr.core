using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.IO;
using System.Security.Cryptography;
using System.Collections.ObjectModel;

namespace JR.Graph
{
    // Manipulation d'images
    public class Images
    {
        static ImageCodecInfo ICodecInfo = null;
        static EncoderParameters Default_EncParameters;

        static EncoderParameters Encoder_Parameters(long Quality, long Color_Depth)
        {
            EncoderParameters EPS = new EncoderParameters(2);
            EPS.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, Quality);
            EPS.Param[1] = new EncoderParameter(System.Drawing.Imaging.Encoder.ColorDepth, Color_Depth);
            return EPS;
        }

        static EncoderParameters Encoder_Parameters(long Quality)
        {
            EncoderParameters EPS = new EncoderParameters(1);
            EPS.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, Quality);
            return EPS;
        }

        static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }

        public static bool Save(Image Image, string File_Name, int Quality)
        {
            Init_Lib();
            string Ext = JR.Files.Get_Extension(File_Name).ToLower();
            foreach (ImageCodecInfo Codec in ImageCodecInfo.GetImageEncoders())
            {
                if (Codec.FilenameExtension.ToLower().Contains(Ext))
                {
                    try
                    {
                        Image.Save(File_Name, Codec, Encoder_Parameters(Quality));
                        return true;
                    }
                    catch (Exception e)
                    {
                        if (e.InnerException == null)
                        {
                            using (Bitmap Bmp = new Bitmap(Image.Width, Image.Height))
                            {
                                using (Graphics G = Graphics.FromImage(Bmp))
                                {
                                    G.DrawImage(Image, 0, 0, Image.Width, Image.Height);
                                }
                                Bmp.Save(File_Name, Codec, Encoder_Parameters(Quality));
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public static bool Rewrite_Bitmap(Image Image, string File_Name, int Quality)
        {
            var P = File_Name.Paths();
            string New_Name = Path.Combine(P.Directory, Guid.NewGuid().ToString() + P.Extension);
            if (Quality > 0)
            {
                Save(Image, New_Name, Quality);
            }
            else
            {
                Image.Save(New_Name);
            }
            Image.Dispose();
            File.Delete(File_Name);
            File.Move(New_Name, File_Name);
            return true;
        }

        public static void Init_Lib()
        {
            if (ICodecInfo != null) return;
            ICodecInfo = GetEncoderInfo("image/jpeg");
            Default_EncParameters = Encoder_Parameters(90);
        }

        // Compresse l'image
        public static bool Compress_Jpg(string Source_File_Name, string Dest_File_Name, int Quality = 0)
        {
            Init_Lib();
            try
            {
                using (Bitmap Bmp = new Bitmap(Source_File_Name))
                {
                    Bmp.Save(Dest_File_Name, ICodecInfo, Quality == 0 ? Default_EncParameters : Encoder_Parameters(Quality));
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        // Fait tourner un jpg ou bien fait un flip
        public static bool Rotate_Jpg(string Source_File_Name, string Dest_File_Name, RotateFlipType Transformation)
        {
            Init_Lib();
            try
            {
                using (Bitmap Bmp = new Bitmap(Source_File_Name))
                {
                    Bmp.RotateFlip(Transformation);
                    Bmp.Save(Dest_File_Name, ICodecInfo, Default_EncParameters);
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        // Fait tourner un jpg ou bien fait un flip
        // Mais pr�serve le bas
        public static bool Rotate_Jpg(string Source_File_Name, string Dest_File_Name, RotateFlipType Transformation, int Preserve_Bottom)
        {
            if (Preserve_Bottom <= 0)
            {
                return Rotate_Jpg(Source_File_Name, Dest_File_Name, Transformation);
            }
            Init_Lib();
            try
            {
                using (Bitmap Bmp = new Bitmap(Source_File_Name), Top = Bmp.Clone(new Rectangle(0, 0, Bmp.Width, Bmp.Height - Preserve_Bottom), PixelFormat.Format24bppRgb))
                {
                    Top.RotateFlip(Transformation);
                    using (Bitmap Dest = new Bitmap(Top.Width, Top.Height + Preserve_Bottom))
                    {
                        using (Graphics Gd = Graphics.FromImage(Dest))
                        {
                            Gd.DrawImage(Top,
                               new Rectangle(0, 0, Top.Width, Top.Height),   // dest rectangle
                               new Rectangle(0, 0, Top.Width, Top.Height), // source rectangle
                               GraphicsUnit.Pixel);
                            Rectangle Bottom_Rect = new Rectangle(0, Top.Height, Top.Width, Preserve_Bottom);
                            // On preleve un pixel en bas � droite pour completer au cas ou
                            Color Background = Bmp.GetPixel(Bmp.Width - 3, Bmp.Height - 3);
                            Gd.FillRectangle(new SolidBrush(Background), Bottom_Rect);
                            Gd.DrawImage(
                               Bmp,
                               new Rectangle(0, Top.Height, Bmp.Width, Preserve_Bottom),   // dest rectangle
                               new Rectangle(0, Bmp.Height - Preserve_Bottom, Bmp.Width, Preserve_Bottom), // source rectangle
                               GraphicsUnit.Pixel);
                            Dest.Save(Dest_File_Name, ICodecInfo, Default_EncParameters);
                        }
                    }
                }
            }
            catch
            {
                return false;
            }
            return true;
        }


        public static bool Resize_Image(string Source_File_Name, string Dest_File_Name, int Max_Size, bool Best_Quality = false, int Dpi = 0, int Quality = 0)
        {
            return Resize_Image(Source_File_Name, Dest_File_Name, Max_Size, Max_Size, Best_Quality, Dpi, Quality);
        }

        // Certains fichiers indiquent une rotation au niveau EXIF, il faut alors inverser width et height
        static bool Is_Rotated(Bitmap Src)
        {
            foreach (var Prop in Src.PropertyItems)
            {
                if (Prop.Id != 274) continue;
                // if (!(Prop.Id == 0x0112 || Prop.Id == 5029 || Prop.Id == 274)) continue;
                switch (Prop.Value[0])
                {
                    //case 2: // RotateFlipType.RotateNoneFlipX;
                    //case 3: // RotateFlipType.Rotate180FlipNone;
                    //case 4: // RotateFlipType.Rotate180FlipX;
                    case 5: // RotateFlipType.Rotate90FlipX;
                    case 6: // RotateFlipType.Rotate90FlipNone;
                    case 7: // RotateFlipType.Rotate270FlipX;
                    case 8: // RotateFlipType.Rotate270FlipNone;
                        return true;
                }
                break;
            }
            return false;
        }

        static void Auto_Rotate(Bitmap Src)
        {
            int Prop_Id = 0;
            foreach (var Prop in Src.PropertyItems)
            {

                if (Prop.Id != 274) continue;
                // if (!(Prop.Id == 0x0112 || Prop.Id == 5029 || Prop.Id == 274)) continue; // Autres possibilit�s, mais plut�t sur les vignettes
                bool Check()
                {
                    switch (Prop.Value[0])
                    {
                        case 2: Src.RotateFlip(RotateFlipType.RotateNoneFlipX); return true;
                        case 3: Src.RotateFlip(RotateFlipType.Rotate180FlipNone); return true;
                        case 4: Src.RotateFlip(RotateFlipType.Rotate180FlipX); return true;
                        case 5: Src.RotateFlip(RotateFlipType.Rotate90FlipX); return true;
                        case 6: Src.RotateFlip(RotateFlipType.Rotate90FlipNone); return true;
                        case 7: Src.RotateFlip(RotateFlipType.Rotate270FlipX); return true;
                        case 8: Src.RotateFlip(RotateFlipType.Rotate270FlipNone); return true;
                    }
                    return false;
                }
                if (Check()) Prop_Id = Prop.Id;
                break;
            }
            if (Prop_Id != 0)
            {
                Src.RemovePropertyItem(Prop_Id);
            }
        }

        public static bool Get_Image_Info(string Source_File_Name, out int Width, out int Height)
        {
            Width = 0; Height = 0;
            try
            {
                using (Bitmap Src = new Bitmap(Source_File_Name))
                {
                    if (Is_Rotated(Src))
                    {
                        Width = Src.Height;
                        Height = Src.Width;
                    }
                    else
                    {
                        Width = Src.Width;
                        Height = Src.Height;
                    }
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }


        public static bool Resize_Image(string Source_File_Name, string Dest_File_Name, int Max_Width, int Max_Height, bool Best_Quality = false, int Dpi = 0, int Quality = 0, string Mode = "")
        {
            try
            {
                using (Bitmap Src = new Bitmap(Source_File_Name))
                {
                    return Resize_Bitmap(Src, Dest_File_Name, Max_Width, Max_Height, Best_Quality, Dpi, Quality, Mode);
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool Resize_Image(string Source_File_Name, Stream Dest_Stream, int Max_Width, int Max_Height, bool Best_Quality = false, int Dpi = 0, int Quality = 0)
        {
            try
            {
                using (Bitmap Src = new Bitmap(Source_File_Name))
                {
                    return Resize(Src, Dest_Stream, Max_Width, Max_Height, Best_Quality, Dpi, Quality);
                }
            }
            catch
            {
                return false;
            }
        }
        public static bool Resize_Bitmap(Bitmap Src, Stream Dest_Stream, int Max_Width, int Max_Height, bool Best_Quality = false, int Dpi = 0, int Quality = 0, string Mode = "")
        {
            return Resize(Src, Dest_Stream, Max_Width, Max_Height, Best_Quality, Dpi, Quality, Mode);
        }

        public static void Reduce(ref int Width, ref int Height, int Max_Width, int Max_Height)
        {
            // r�duire en X
            if (Max_Width > 0 && Width > Max_Width)
            {
                Height = (Height * Max_Width) / Width;
                Width = Max_Width;
            }

            // r�duire en Y
            if (Max_Height > 0 && Height > Max_Height)
            {
                Width = (Width * Max_Height) / Height;
                Height = Max_Height;
            }
        }

        // Dans le cas du zoom max_width et max_height doivent �tre respect�s
        public static void Reduce_Zoom(ref int Width, ref int Height, int Max_Width, int Max_Height, out int Z_X, out int Z_Y, out int Z_Width, out int Z_Height)
        {
            Z_X = 0;
            Z_Y = 0;
            Z_Width = Width;
            Z_Height = Height;

            // r�duire en X
            if (Max_Width > 0)
            {
                int Proposed_Height = (Height * Max_Width) / Width;
                if (Max_Height <= 0 || Proposed_Height >= Max_Height)
                {
                    if (Max_Height > 0)
                    {
                        Z_Height = (Max_Height * Width) / Max_Width;
                        Z_Y = (Height - Z_Height) / 2;
                        Height = Max_Height;
                    }
                    else
                    {
                        Height = Proposed_Height;
                    }
                    Width = Max_Width;
                    return;
                }
            }

            // r�duire en Y
            if (Max_Height > 0)
            {
                int Proposed_Width = (Width * Max_Height) / Height;
                if (Max_Width <= 0 || Proposed_Width >= Max_Width)
                {
                    if (Max_Width > 0)
                    {
                        Z_Width = (Max_Width * Height) / Max_Height;
                        Z_X = (Width - Z_Width) / 2;
                        Width = Max_Width;
                    }
                    else
                    {
                        Width = Proposed_Width;
                    }
                    Height = Max_Height;
                    return;
                }
            }
        }

        static bool Resize(Bitmap Src, object Output, int Max_Width, int Max_Height, bool Best_Quality = false, int Dpi = 0, int Quality = 0, string Mode = "")
        {
            Init_Lib();
            try
            {
                Auto_Rotate(Src);
                bool Zoom_Mode = Mode == "Z";
                int Nwidth = Src.Width;
                int Nheight = Src.Height;
                int Z_X = 0, Z_Y = 0, Z_Width = 0, Z_Height = 0;
                if (Zoom_Mode)
                {
                    Reduce_Zoom(ref Nwidth, ref Nheight, Max_Width, Max_Height, out Z_X, out Z_Y, out Z_Width, out Z_Height);
                }
                else
                {
                    Reduce(ref Nwidth, ref Nheight, Max_Width, Max_Height);
                }

                using (Bitmap Dest = new Bitmap(Nwidth, Nheight))
                {
                    using (Graphics Gd = Graphics.FromImage(Dest))
                    {
                        Gd.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        Gd.SmoothingMode = SmoothingMode.HighQuality;
                        Gd.CompositingQuality = CompositingQuality.HighQuality;
                        if (Best_Quality && (Src.Width * Src.Height < 10000 * 10000))  // necessite plus de memoire !!
                        {
                            Gd.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        }

                        if (Zoom_Mode)
                        {
                            Gd.DrawImage(Src, new Rectangle(0, 0, Nwidth, Nheight),   // dest rectangle
                               new Rectangle(Z_X, Z_Y, Z_Width, Z_Height), // source rectangle
                               GraphicsUnit.Pixel);
                        }
                        else
                        {

                            Gd.DrawImage(Src, new Rectangle(0, 0, Nwidth, Nheight),   // dest rectangle
                               new Rectangle(0, 0, Src.Width, Src.Height), // source rectangle
                               GraphicsUnit.Pixel);
                        }
                        if (Dpi > 0)
                        {
                            Dest.SetResolution((float)Dpi, (float)Dpi);
                        }

                        string Dest_File_Name = Output as string;
                        if (Dest_File_Name != null)
                        {
                            JR.Files.Check_Directory(Dest_File_Name);
                            System.IO.File.Delete(Dest_File_Name);
                            if (Quality > 0)
                            {
                                using (EncoderParameters E = Encoder_Parameters(Quality))
                                {
                                    Dest.Save(Dest_File_Name, ICodecInfo, E);
                                }
                            }
                            else
                            {
                                Dest.Save(Dest_File_Name, ICodecInfo, Default_EncParameters);
                            }
                            return true;
                        }

                        Stream Dest_Stream = Output as Stream;
                        if (Dest_Stream != null)
                        {
                            if (Quality > 0)
                            {
                                using (EncoderParameters E = Encoder_Parameters(Quality))
                                {
                                    Dest.Save(Dest_Stream, ICodecInfo, E);
                                }
                            }
                            else
                            {
                                Dest.Save(Dest_Stream, ICodecInfo, Default_EncParameters);
                            }
                            return true;
                        }

                    }
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static bool Resize_Bitmap(Bitmap Src, string Dest_File_Name, int Max_Width, int Max_Height, bool Best_Quality = false, int Dpi = 0, int Quality = 0, string Mode = "")
        {
            return Resize(Src, Dest_File_Name, Max_Width, Max_Height, Best_Quality, Dpi, Quality, Mode);
        }

    }
}
