using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Collections.Generic;

namespace JR.Graph
{

    public class Exif
    {
        //typedef bool (CALLBACK* METAENUMPROC)(const char *key, const char *value, void *user);
        public delegate bool Enum_CallBack(string Key, string Value, ref long User);

        //[DllImport("JR.exiv.dll", CallingConvention = CallingConvention.Cdecl)]
        //public static extern void VariantInit(IntPtr addrofvariant);
        //[DllImport("JR.exiv.dll", CallingConvention = CallingConvention.Cdecl)]
        //public static extern int VariantClear(IntPtr addrofvariant);
        //EXIVSIMPLE_API HIMAGE OpenFileImage(const char *file);
        [DllImport("JR.exiv.dll", CallingConvention=CallingConvention.Cdecl)]
        public static extern long OpenFileImage(string File);
        //EXIVSIMPLE_API HIMAGE OpenMemImage(const BYTE *data, unsigned int size);
        [DllImport("JR.exiv.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern long OpenMemImage(byte[] Buffer, int Size);
        //EXIVSIMPLE_API void FreeImage(HIMAGE img);
        [DllImport("JR.exiv.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void FreeImage(long img);
        //EXIVSIMPLE_API int SaveImage(HIMAGE img);
        [DllImport("JR.exiv.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int SaveImage(long img);
        //EXIVSIMPLE_API int ImageSize(HIMAGE img);
        [DllImport("JR.exiv.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int ImageWidth(long img);
        [DllImport("JR.exiv.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int ImageHeight(long img);
        //EXIVSIMPLE_API int ImageData(HIMAGE img, BYTE *buffer, unsigned int size);
        [DllImport("JR.exiv.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int ImageSize(long img);
        //EXIVSIMPLE_API int ImageData(HIMAGE img, BYTE *buffer, unsigned int size);
        [DllImport("JR.exiv.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int ImageData(long img, byte[] Buffer, int Size);
        //EXIVSIMPLE_API int ReadMeta(HIMAGE img, const char *key, char *buff, int buffsize);
        [DllImport("JR.exiv.dll", CallingConvention = CallingConvention.Cdecl)]
//        public static extern int ReadMeta(int img, string Key, string Buff, int BuffSize);
        public static extern int ReadMeta(long img, string Key, string Buffer, int BuffSize);
        //EXIVSIMPLE_API int EnumMeta(HIMAGE img, METAENUMPROC proc, void *user);
        [DllImport("JR.exiv.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int EnumMeta(long img, Enum_CallBack Proc, ref long User);
        //EXIVSIMPLE_API int AddMeta(HIMAGE img, const char *key, const char *val, DllTypeId type);
        [DllImport("JR.exiv.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int AddMeta(long img, string Key, string Val, int Type);
        //EXIVSIMPLE_API int ModifyMeta(HIMAGE img, const char *key, const char *val, DllTypeId type);
        [DllImport("JR.exiv.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int ModifyMeta(long img, string Key, string Val, int Type);
        //EXIVSIMPLE_API int RemoveMeta(HIMAGE img, const char *key);
        [DllImport("JR.exiv.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int RemoveMeta(long img, string Key);
        //EXIVSIMPLE_API int ClearMeta(HIMAGE img);
        [DllImport("JR.exiv.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int ClearAllMeta(long img);

        public const int T_InvalidTypeId = 0;
        public const int T_UnsignedByte = 1;
        public const int T_AsciiString = 2;
        public const int T_UnsignedShort = 3;
        public const int T_UnsignedLong = 4;
        public const int T_UnsignedRational = 5;
        public const int T_Invalid6 = 6;
        public const int T_Undefined = 7;
        public const int T_SignedShort = 8;
        public const int T_SignedLong = 9;
        public const int T_SignedRational = 10;
        public const int T_String = 11;
        public const int T_Date = 12;
        public const int T_Tim  = 13;
        public const int T_Comment = 14;
        public const int T_Directory = 15;
        public const int T_LastTypeId = 16;

        // Helper functions
        public static int Add_Meta(long img, string Key, string Val)
        {
            if (Val != "") return AddMeta (img, Key, Val, 0);
            return 0;
        }

        public static int Modify_Meta(long img, string Key, string Val)
        {
            if (Val != "") return ModifyMeta(img, Key, Val, 0);
            return 0;
        }

    }

}