using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.IO;

namespace JR.Graph
{
    public class Iptc_Reader
    {
        public enum Mode {None = 0, Classic = 1 , All = 2};
        public Iptc_Reader(string File, Mode Mode = Mode.Classic)
        {
            Width = 0;
            Height = 0;
            Valid = false;
            Dic = new Dictionary<string, string>();

            try
            {
                using (var S = new FileStream(File, FileMode.Open, FileAccess.Read))  // Passer par un stream pour forcer la cloture du fichier sans attendre le GC
                {
                    var Decoder = BitmapDecoder.Create(S, BitmapCreateOptions.None, BitmapCacheOption.OnDemand);
                    Valid = true;
                    Width = Decoder.Frames[0].PixelWidth;
                    Height = Decoder.Frames[0].PixelHeight;
                    Dic["file"] = JR.Files.Get_File_Name(File);
                    Dic["width"] = Width.ToString();
                    Dic["height"] = Height.ToString();
                    var M = Decoder.Frames[0].Metadata as BitmapMetadata;
                    if (Mode != Mode.None && M != null)
                    {
                        bool Classic_Mode = Mode == Mode.Classic;
                        void Extract(BitmapMetadata Metadata, string Query)
                        {
                            foreach (string relativeQuery in Metadata)
                            {
                                string fullQuery = Query + relativeQuery;
                                var metadataQueryReader = Metadata.GetQuery(relativeQuery) ?? string.Empty;
                                var innerBitmapMetadata = metadataQueryReader as BitmapMetadata;
                                if (innerBitmapMetadata != null)
                                {
                                    Extract(innerBitmapMetadata, fullQuery);
                                }
                                else
                                {
                                    var Location = Metadata.Location.ToLower();
                                    if (Classic_Mode)
                                    {
                                        bool Is_Ok()
                                        {
                                            if (Metadata.Format.Match("iptc")) return true;
                                            if (Location.Match("/xmp/dc:")) return true;
                                            return false;
                                        }
                                        if (!Is_Ok()) continue;
                                    }
                                    var Key = Metadata.Location + relativeQuery;
                                    if (Metadata.Format == "iptc") // /app13/{ushort=0}/{ulonglong=61857348781060}/iptc/{str=Coded Character Set}
                                    {
                                        if (Classic_Mode)
                                        {
                                            var C = relativeQuery.Scan();
                                            C.Next_Word("str=");
                                            if (C.End) continue;
                                            Key = C.Next("}").Replace(" ", "").ToUpper();
                                            // Normaliser les noms
                                            switch (Key)
                                            {
                                                case "ORIGINATINGPROGRAM":
                                                    Key = "PROGRAM";
                                                    break;
                                                case "ORIGINALTRANSMISSIONREFERENCE":
                                                    Key = "TRANSMISSIONREFERENCE";
                                                    break;
                                                case "SUPPLEMENTALCATEGORY":
                                                    Key = "SUPPCATEGORY";
                                                    break;
                                                case "COUNTRY/PRIMARYLOCATIONNAME":
                                                    Key = "COUNTRYNAME";
                                                    break;
                                                case "COPYRIGHTNOTICE":
                                                    Key = "COPYRIGHT";
                                                    break;
                                                case "BY-LINE":
                                                    Key = "BYLINE";
                                                    break;
                                                case "BY-LINETITLE":
                                                    Key = "BYLINETITLE";
                                                    break;
                                                case "WRITER/EDITOR":
                                                    Key = "WRITER";
                                                    break;
                                                case "CODEDCHARACTERSET":
                                                    Key = "CHARACTERSET";
                                                    break;
                                            }
                                        }
                                        if (metadataQueryReader is byte[])
                                        {
                                            Dic[Key] = JR.Strings.Assembled<byte>('/', (byte[])metadataQueryReader);
                                        }
                                        else
                                            if (metadataQueryReader is string[])
                                            {
                                                Dic[Key] = JR.Strings.Assembled<string>('/', (string[])metadataQueryReader);
                                            }
                                            else
                                                Dic[Key] = metadataQueryReader.ToString().Trim();
                                    }
                                    else if (metadataQueryReader is string)
                                    {
                                        if (Classic_Mode)
                                        {
                                            Key = "xmp." + Location.Replace("/xmp/dc:","");
                                        }
                                        var T = metadataQueryReader.ToString().Trim();
                                        Dic[Key] = T;
                                    }
                                }
                            }
                        };
                        Extract(M, "");
                    }
                }
            }
            catch
            {
            }

        }

        public bool Valid { get; set; }

        public Dictionary<string, string> Dic { get; set; }

        public string this[string Key]
        {
            get
            {
                string Val = "";
                if (!Dic.TryGetValue(Key.ToUpper(), out Val)) return "";
                return Val;
            }
        }

        public string FFO_Credit
        {
            get { return this["CREDIT"]; }
        }
        public string FFO_Byline
        {
            get { return this["BYLINE"]; }
        }
        public string FFO_Source
        {
            get { return this["SOURCE"]; }
        }
        public string FFO_Program
        {
            get { return this["PROGRAM"]; }
        }
        public string FFO_Contact
        {
            get { return this["CONTACT"]; }
        }
        public string FFO_OTR
        {
            get { return this["TRANSMISSIONREFERENCE"]; }
        }
        public string FFO_ObjectName
        {
            get { return this["OBJECTNAME"]; }
        }
        public string FFO_Headline
        {
            get { return this["HEADLINE"]; }
        }
        public string FFO_Caption
        {
            get { return this["CAPTION"]; }
        }

        public string FFO_FixtureId
        {
            get { return this["FIXTUREID"]; }
        }
        public string FFO_City
        {
            get { return this["CITY"]; }
        }
        public string FFO_CountryName
        {
            get { return this["COUNTRYNAME"]; }
        }
        public string FFO_SpecialInstructions
        {
            get { return this["SPECIALINSTRUCTIONS"]; }
        }
        public string FFO_CopyrightNotice
        {
            get { return this["COPYRIGHT"]; }
        }

        public string FFO_Category
        {
            get { return this["CATEGORY"]; }
        }

        public string DateCreated
        {
            get { return this["DATECREATED"]; }
        }

        public string[] FFO_Categories
        {
            get { return this["SUPPLEMENTALCATEGORY"].Split('/'); }
        }

        public string[] FFO_Keywords
        {
            get { return this["KEYWORDS"].Split('/'); }
        }

        public DateTime FFO_DateCreated
        {
            get
            {
                char[] Date = JR.Time.Null_Time.ToString("yyyyMMdd").ToCharArray();
                int Idx = 0;
                foreach (char C in DateCreated)
                {
                    if (char.IsDigit(C))
                    {
                        Date[Idx] = C;
                        Idx++;
                    }
                    if (Idx >= Date.Length) break;
                }
                try
                {
                    return DateTime.ParseExact(new string(Date), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                }
                catch
                {
                }
                return JR.Time.Null_Time;
            }
        }

        public string All_Keywords(char Separator)
        {
            return this["KEYWORDS"].Replace('/', Separator);
        }

        public int Width { get; set; }
        public int Height { get; set; }
    }
}