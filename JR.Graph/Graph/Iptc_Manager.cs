using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using JR.Saf;
using System.Diagnostics;

namespace JR.Graph
{

    public class Iptc_Manager : IDisposable
    {
        string Pic_File = "";
        bool Temp_File = false;
        public Iptc_Manager(string File)
        {
            if (!System.IO.File.Exists(File)) return;
            Pic_File = File;
        }

        public Iptc_Manager(byte[] Buffer, int Size)
        {
            if (Buffer == null || Buffer.Length < 10) return;
            try
            {
                var File_Name = Main.Real_File_Name (new Guid().ToString() + ".jpg", Main.Temp_Space);
                File.WriteAllBytes(File_Name, Buffer);
                Temp_File = true;
                Pic_File = File_Name;
            }
            catch { }
        }

        public bool Valid
        {
            get
            {
                return Pic_File.Length > 0;
            }
        }

        void Run_Exiv2 (Action<Process> Prepare, Action<Process> Complete = null)
        {
            try
            {
                using (var P = new Process())
                {
                    P.StartInfo.UseShellExecute = false;
                    P.StartInfo.FileName = Main.Parameter("exiv2.path");
                    P.StartInfo.CreateNoWindow = true;
                    Prepare(P);
                    P.Start();
                    P.WaitForExit(10000);
                    if (Complete != null) Complete(P);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        void Apply_Actions()
        {
            if (Clear_All)
            {
               Run_Exiv2((P) =>
               {
                   P.StartInfo.Arguments = "-da " + Pic_File;
               });
            }
            {
                var Action_File = Main.Real_File_Name(Guid.NewGuid().ToString() + ".txt", Main.Temp_Space);
                JR.Files.Safe_Write(Action_File, Actions.ToArray());
                Run_Exiv2((P) =>
                {
                    P.StartInfo.Arguments = "-m " + Action_File + " " + Pic_File;
                });
            }
        }

        IEnumerable<string> List_Tags()
        {
            var Output_File = Main.Real_File_Name(Guid.NewGuid().ToString() + ".txt", Main.Temp_Space);
            Run_Exiv2((P) =>
            {
                P.StartInfo.Arguments = "-pi " + Pic_File;
                P.StartInfo.RedirectStandardOutput = true;
                P.StartInfo.StandardOutputEncoding = Encoding.Default;
            },
            (P)=> { File.WriteAllText(Output_File, P.StandardOutput.ReadToEnd(), Encoding.UTF8); }
            );
            var Content = JR.Files.Safe_Read(Output_File);
            JR.Files.Safe_Delete(Output_File);
            return Content;
        }


        List<string> Actions = new List<string>();
        bool Clear_All = false;
        public void Save(string Dest = "")
        {
            if (!Valid) return;
            if (Dest != "")
            {
                if (!JR.Files.Safe_Copy(Pic_File, Dest)) return;
                Pic_File = Dest;
            }
            Apply_Actions();
        }

        public byte[] Read_Data()
        {
            if (!Valid) return null;
            return File.ReadAllBytes(Pic_File);
        }

        public int Remove_Meta(string Key)
        {
            Actions.Add(string.Format("del {0}", Key));
            return 1;
        }

        // Retourner false pour interrompre l'enumeration
        bool Enum_Data(string Key, string Value)
        {
            //Console.WriteLine("{0}={1}", Key, Value);
            if (Value == null || Value == ""||Value.Trim() == "") return true;
            string Val = Value;
            string K = Key.ToUpper();
            if (_Dic.ContainsKey(K))
            {
                if (_Dic[K] == Val) return true; // false;
                _Dic[K] += ("/" + Val);
            }
            else
            {
                _Dic[K] = Val;
            }
            return true;
        }
        Dictionary<string, string> _Dic;
        static Encoding E_Iso = Encoding.GetEncoding("iso-8859-1");
        static Encoding E_Utf = Encoding.UTF8;
        static Encoding E_Mac = Encoding.GetEncoding(10000);
        public Dictionary<string, string> Dic
        {
            get
            {
                if (_Dic == null)
                {
                    _Dic = new Dictionary<string, string>();
                    {
                        var SB = new StringBuilder();
                        string Key = "";
                        void Push()
                        {
                            if (Key != "")
                            {
                                Enum_Data(Key, SB.ToString());
                                SB.Clear();
                                Key = "";
                            }
                        }
                        foreach (var S in List_Tags())
                        {
                            if (S.StartsWith("Iptc."))
                            {
                                Push();
                                Key = S.Substring(0, 45).Trim();
                                SB.Append(S.Substring(60));
                            }
                            else
                            {
                                SB.AppendLine();
                                SB.Append(S);
                            }
                        }
                        Push();
                    }

                    if (Auto_Translate)
                    {
                        string CharacterSet = "";
                        if (Dic.TryGetValue("IPTC.ENVELOPE.CHARACTERSET", out CharacterSet))
                        {
                            Func<string, string> Translator = null;
                            if (CharacterSet.StartsWith("27 37"))
                            {
                                Translator = (Src) =>
                                {
                                    if (Src == null || Src == "") return Src;
                                    byte[] B = Encoding.Default.GetBytes(Src);
                                    return E_Utf.GetString(B);
                                };
                            }
                            if (Translator != null)
                            {
                                var Keys = _Dic.Keys.Where((K) => K.StartsWith("IPTC")).ToArray();
                                foreach (var Key in Keys)
                                {
                                   _Dic[Key] = Translator(_Dic[Key]);
                                }
                                Translated = true;
                            }

                        }
                    }
                }
                return _Dic;
            }
        }

        public string this[string Key]
        {
            get
            {
                string Val = "";
                if (!Dic.TryGetValue(Key.ToUpper(), out Val)) return "";
                return Val;
            }
            set
            {
                Remove_Meta(Key);
                if (value != "") Modify_Meta(Key, value);
            }
        }

        //public int Read_Meta(string Key, string Buff, int BuffSize)
        //{
        //    if (!Valid) return 0;
        //    return Exif.ReadMeta(Id, Key, Buff, BuffSize);
        //}

        public int Add_Meta(string Key, string Val)
        {
            Actions.Add(string.Format("add {0} {1}", Key, Val));
            return 1;
        }

        public int Modify_Meta(string Key, string Val)
        {
            Actions.Add(string.Format("set {0} {1}", Key, Val));
            return 1;
        }

        public int Erase_All_Meta()
        {
            Clear_All = true;
            return 1;
        }

        #region IDisposable Membres

        public void Dispose()
        {
            if (!Valid) return;
            if (Temp_File) JR.Files.Safe_Delete(Pic_File);
            Pic_File = "";
        }

        #endregion

        public string FFO_Credit
        {
            get { return this["Iptc.Application2.Credit"]; }
            set { this["Iptc.Application2.Credit"] = value; }
        }
        public string FFO_Author
        {
            get { return this["Iptc.Application2.Author"]; }
        }
        public string FFO_Byline
        {
            get { return this["Iptc.Application2.Byline"]; }
            set { this["Iptc.Application2.Byline"] = value; }
        }
        public string FFO_Source
        {
            get { return this["Iptc.Application2.Source"]; }
            set { this["Iptc.Application2.Source"] = value; }
        }
        public string FFO_OTR
        {
            get { return this["Iptc.Application2.TransmissionReference"]; }
            set { this["Iptc.Application2.TransmissionReference"] = value; }
        }
        public string FFO_ObjectName
        {
            get { return this["Iptc.Application2.ObjectName"]; }
            set { this["Iptc.Application2.ObjectName"] = value; }
        }
        public string FFO_EditStatus
        {
            get { return this["Iptc.Application2.EditStatus"]; }
            set { this["Iptc.Application2.EditStatus"] = value; }
        }
        public string FFO_Headline
        {
            get { return this["Iptc.Application2.Headline"]; }
            set { this["Iptc.Application2.Headline"] = value; }
        }
        public string FFO_Caption
        {
            get { return this["Iptc.Application2.Caption"]; }
            set { this["Iptc.Application2.Caption"] = value; }
        }

        public string FFO_FixtureId
        {
            get { return this["Iptc.Application2.FixtureId"]; }
            set { this["Iptc.Application2.FixtureId"] = value; }
        }
        public string FFO_City
        {
            get { return this["Iptc.Application2.City"]; }
            set { this["Iptc.Application2.City"] = value; }
        }
        public string FFO_CountryName
        {
            get { return this["Iptc.Application2.CountryName"]; }
            set { this["Iptc.Application2.CountryName"] = value; }
        }
        public string FFO_SpecialInstructions
        {
            get { return this["Iptc.Application2.SpecialInstructions"]; }
            set { this["Iptc.Application2.SpecialInstructions"] = value; }
        }
        public string FFO_CopyrightNotice
        {
            get { return this["Iptc.Application2.Copyright"]; }
            set { this["Iptc.Application2.Copyright"] = value; }
        }

        public string FFO_Category
        {
            get { return this["Iptc.Application2.Category"]; }
            set { this["Iptc.Application2.Category"] = value; }
        }

        public string DateCreated
        {
            get { return this["Iptc.Application2.DateCreated"]; }
            set { this["Iptc.Application2.DateCreated"] = value; }
        }

        public string ReleaseDate
        {
            get { return this["Iptc.Application2.ReleaseDate"]; }
            set { this["Iptc.Application2.ReleaseDate"] = value; }
        }

        public string[] FFO_Categories
        {
            get { return this["Iptc.Application2.SuppCategory"].Split('/'); }
            set
            {
                Remove_Meta("Iptc.Application2.SuppCategory");
                foreach (string Val in value)
                {
                    Add_Meta("Iptc.Application2.SuppCategory", Val);
                }
            }
        }

        public string[] FFO_Keywords
        {
            get { return this["Iptc.Application2.Keywords"].Split('/'); }
            set
            {
                if (!Valid) return;
                Remove_Meta("Iptc.Application2.Keywords");
                foreach (string Val in value)
                {
                    string V = Val.Trim();
                    if (V.Length == 0) continue;
                    Add_Meta("Iptc.Application2.Keywords", V);
                }
            }
        }

        public DateTime FFO_ReleaseDate
        {
            get
            {
                char[] Date = JR.Time.Null_Time.ToString("yyyyMMdd").ToCharArray();
                int Idx = 0;
                foreach (char C in this["Iptc.Application2.ReleaseDate"])
                {
                    if (char.IsDigit(C))
                    {
                        Date[Idx] = C;
                        Idx++;
                    }
                    if (Idx >= Date.Length) break;
                }
                try
                {
                    return DateTime.ParseExact(new string(Date), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                }
                catch
                {
                }
                return JR.Time.Null_Time;
            }
            set
            {
                if (!Valid) return;
                Remove_Meta("Iptc.Application2.ReleaseDate");
                if (value == JR.Time.Null_Time) return;
                Modify_Meta("Iptc.Application2.ReleaseDate", value.ToString("yyyy-MM-dd"));
            }
        }


        public DateTime FFO_DateCreated
        {
            get
            {
                char[] Date = JR.Time.Null_Time.ToString("yyyyMMdd").ToCharArray();
                int Idx = 0;
                foreach (char C in this["Iptc.Application2.DateCreated"])
                {
                    if (char.IsDigit(C))
                    {
                        Date[Idx] = C;
                        Idx++;
                    }
                    if (Idx >= Date.Length) break;
                }
                try
                {
                    return DateTime.ParseExact(new string(Date), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                }
                catch
                {
                }
                return JR.Time.Null_Time;
            }
            set
            {
                if (!Valid) return;
                Remove_Meta("Iptc.Application2.DateCreated");
                if (value == JR.Time.Null_Time) return;
                Modify_Meta("Iptc.Application2.DateCreated", value.ToString("yyyy-MM-dd"));
            }
        }

        public string All_Keywords(char Separator)
        {
            return this["Iptc.Application2.Keywords"].Replace('/', Separator);
        }

        //public int Width
        //{
        //    get
        //    {
        //        if (!Valid) return 0;
        //        return Exif.ImageWidth(Id);
        //    }
        //}
        //public int Height
        //{
        //    get
        //    {
        //        if (!Valid) return 0;
        //        return Exif.ImageHeight(Id);
        //    }
        //}

        public bool Auto_Translate { get; set; } = false;
        public bool Translated { get; set; } = false;
    }

}