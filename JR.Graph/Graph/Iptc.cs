using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;

namespace JR.Graph
{

    public class Iptc : IDisposable
    {
        long _Id = -1;
        public Iptc(string File)
        {
            _Id = Exif.OpenFileImage(File);
        }

        public Iptc(byte[] Buffer, int Size)
        {
            _Id = Exif.OpenMemImage(Buffer, Size);
        }

        public bool Valid
        {
            get
            {
                return _Id > 0;
            }
        }

        public void Save()
        {
            if (!Valid) return;
            Exif.SaveImage(_Id);
        }

        public int Size
        {
            get
            {
                if (!Valid) return 0;
                return Exif.ImageSize(_Id);
            }
        }

        public int Data(byte[] Buffer, int Size)
        {
            if (!Valid) return 0;
            return Exif.ImageData(_Id, Buffer, Size);
        }

        public int Remove_Meta(string Key)
        {
            if (!Valid) return 0;
            return Exif.RemoveMeta(_Id, Key);
        }

        // Retourner false pour interrompre l'enumeration
        bool Enum_Data(string Key, string Value, ref long User)
        {
            //Console.WriteLine("{0}={1}", Key, Value);
            if (Value == null || Value == ""||Value.Trim() == "") return true;
            string Val = Value;
            string K = Key.ToUpper();
            if (_Dic.ContainsKey(K))
            {
                if (_Dic[K] == Val) return true; // false;
                _Dic[K] += ("/" + Val);
            }
            else
            {
                _Dic[K] = Val;
            }
            return true;
        }
        Dictionary<string, string> _Dic;
        static Encoding E_Iso = Encoding.GetEncoding("iso-8859-1");
        static Encoding E_Utf = Encoding.UTF8;
        static Encoding E_Mac = Encoding.GetEncoding(10000);
        public Dictionary<string, string> Dic
        {
            get
            {
                if (_Dic == null)
                {
                    _Dic = new Dictionary<string, string>();
                    long User = 1;
                    if (Valid) Exif.EnumMeta(_Id, new Exif.Enum_CallBack(Enum_Data), ref User);
                    if (Auto_Translate)
                    {
                        string CharacterSet = "";
                        if (Dic.TryGetValue("IPTC.ENVELOPE.CHARACTERSET", out CharacterSet))
                        {
                            Func<string, string> Translator = null;
                            if (CharacterSet.StartsWith("27 37"))
                            {
                                Translator = (Src) =>
                                {
                                    if (Src == null || Src == "") return Src;
                                    byte[] B = Encoding.Default.GetBytes(Src);
                                    return E_Utf.GetString(B);
                                };
                            }
                            if (Translator != null)
                            {
                                var Keys = _Dic.Keys.Where((K) => K.StartsWith("IPTC")).ToArray();
                                foreach (var Key in Keys)
                                {
                                   _Dic[Key] = Translator(_Dic[Key]);
                                }
                                Translated = true;
                            }

                        }
                    }
                }
                return _Dic;
            }
        }

        public string this[string Key]
        {
            get
            {
                string Val = "";
                if (!Dic.TryGetValue(Key.ToUpper(), out Val)) return "";
                return Val;
            }
            set
            {
                if (!Valid) return;
                Exif.RemoveMeta(_Id, Key);
                if (value != "") Exif.Modify_Meta(_Id, Key, value);
            }
        }

        public int Read_Meta(string Key, string Buff, int BuffSize)
        {
            if (!Valid) return 0;
            return Exif.ReadMeta(_Id, Key, Buff, BuffSize);
        }

        public int Add_Meta(string Key, string Val)
        {
            if (!Valid) return 0;
            return Exif.Add_Meta(_Id, Key, Val);
        }

        public int Modify_Meta(string Key, string Val)
        {
            if (!Valid) return 0;
            return Exif.Modify_Meta(_Id, Key, Val);
        }

        public int Erase_All_Meta()
        {
            if (!Valid) return 0;
            return Exif.ClearAllMeta(_Id);
        }

        #region IDisposable Membres

        public void Dispose()
        {
            if (!Valid) return;
            Exif.FreeImage(_Id);
            _Id = -1;
        }

        #endregion

        public string FFO_Credit
        {
            get { return this["Iptc.Application2.Credit"]; }
            set { this["Iptc.Application2.Credit"] = value; }
        }
        public string FFO_Author
        {
            get { return this["Iptc.Application2.Author"]; }
        }
        public string FFO_Byline
        {
            get { return this["Iptc.Application2.Byline"]; }
            set { this["Iptc.Application2.Byline"] = value; }
        }
        public string FFO_Source
        {
            get { return this["Iptc.Application2.Source"]; }
            set { this["Iptc.Application2.Source"] = value; }
        }
        public string FFO_OTR
        {
            get { return this["Iptc.Application2.TransmissionReference"]; }
            set { this["Iptc.Application2.TransmissionReference"] = value; }
        }
        public string FFO_ObjectName
        {
            get { return this["Iptc.Application2.ObjectName"]; }
            set { this["Iptc.Application2.ObjectName"] = value; }
        }
        public string FFO_EditStatus
        {
            get { return this["Iptc.Application2.EditStatus"]; }
            set { this["Iptc.Application2.EditStatus"] = value; }
        }
        public string FFO_Headline
        {
            get { return this["Iptc.Application2.Headline"]; }
            set { this["Iptc.Application2.Headline"] = value; }
        }
        public string FFO_Caption
        {
            get { return this["Iptc.Application2.Caption"]; }
            set { this["Iptc.Application2.Caption"] = value; }
        }

        public string FFO_FixtureId
        {
            get { return this["Iptc.Application2.FixtureId"]; }
            set { this["Iptc.Application2.FixtureId"] = value; }
        }
        public string FFO_City
        {
            get { return this["Iptc.Application2.City"]; }
            set { this["Iptc.Application2.City"] = value; }
        }
        public string FFO_CountryName
        {
            get { return this["Iptc.Application2.CountryName"]; }
            set { this["Iptc.Application2.CountryName"] = value; }
        }
        public string FFO_SpecialInstructions
        {
            get { return this["Iptc.Application2.SpecialInstructions"]; }
            set { this["Iptc.Application2.SpecialInstructions"] = value; }
        }
        public string FFO_CopyrightNotice
        {
            get { return this["Iptc.Application2.Copyright"]; }
            set { this["Iptc.Application2.Copyright"] = value; }
        }

        public string FFO_Category
        {
            get { return this["Iptc.Application2.Category"]; }
            set { this["Iptc.Application2.Category"] = value; }
        }

        public string DateCreated
        {
            get { return this["Iptc.Application2.DateCreated"]; }
            set { this["Iptc.Application2.DateCreated"] = value; }
        }

        public string ReleaseDate
        {
            get { return this["Iptc.Application2.ReleaseDate"]; }
            set { this["Iptc.Application2.ReleaseDate"] = value; }
        }

        public string[] FFO_Categories
        {
            get { return this["Iptc.Application2.SuppCategory"].Split('/'); }
            set
            {
                Exif.RemoveMeta(_Id, "Iptc.Application2.SuppCategory");
                foreach (string Val in value)
                {
                    Exif.Add_Meta(_Id, "Iptc.Application2.SuppCategory", Val);
                }
            }
        }

        public string[] FFO_Keywords
        {
            get { return this["Iptc.Application2.Keywords"].Split('/'); }
            set
            {
                if (!Valid) return;
                Exif.RemoveMeta(_Id, "Iptc.Application2.Keywords");
                foreach (string Val in value)
                {
                    string V = Val.Trim();
                    if (V.Length == 0) continue;
                    Exif.Add_Meta(_Id, "Iptc.Application2.Keywords", V);
                }
            }
        }

        public DateTime FFO_ReleaseDate
        {
            get
            {
                char[] Date = JR.Time.Null_Time.ToString("yyyyMMdd").ToCharArray();
                int Idx = 0;
                foreach (char C in this["Iptc.Application2.ReleaseDate"])
                {
                    if (char.IsDigit(C))
                    {
                        Date[Idx] = C;
                        Idx++;
                    }
                    if (Idx >= Date.Length) break;
                }
                try
                {
                    return DateTime.ParseExact(new string(Date), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                }
                catch
                {
                }
                return JR.Time.Null_Time;
            }
            set
            {
                if (!Valid) return;
                Exif.RemoveMeta(_Id, "Iptc.Application2.ReleaseDate");
                if (value == JR.Time.Null_Time) return;
                Exif.Modify_Meta(_Id, "Iptc.Application2.ReleaseDate", value.ToString("yyyy-MM-dd"));
            }
        }


        public DateTime FFO_DateCreated
        {
            get
            {
                char[] Date = JR.Time.Null_Time.ToString("yyyyMMdd").ToCharArray();
                int Idx = 0;
                foreach (char C in this["Iptc.Application2.DateCreated"])
                {
                    if (char.IsDigit(C))
                    {
                        Date[Idx] = C;
                        Idx++;
                    }
                    if (Idx >= Date.Length) break;
                }
                try
                {
                    return DateTime.ParseExact(new string(Date), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                }
                catch
                {
                }
                return JR.Time.Null_Time;
            }
            set
            {
                if (!Valid) return;
                Exif.RemoveMeta(_Id, "Iptc.Application2.DateCreated");
                if (value == JR.Time.Null_Time) return;
                Exif.Modify_Meta(_Id, "Iptc.Application2.DateCreated", value.ToString("yyyy-MM-dd"));
            }
        }

        public string All_Keywords(char Separator)
        {
            return this["Iptc.Application2.Keywords"].Replace('/', Separator);
        }

        public int Width
        {
            get
            {
                if (!Valid) return 0;
                return Exif.ImageWidth(_Id);
            }
        }
        public int Height
        {
            get
            {
                if (!Valid) return 0;
                return Exif.ImageHeight(_Id);
            }
        }

        public bool Auto_Translate { get; set; } = false;
        public bool Translated { get; set; } = false;
    }

}