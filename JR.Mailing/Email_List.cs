﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JR.Mailing
{
    public class Email_List:Dictionary<string, string>
    {
        public Email_List (string Initial_List = "")
        {
            Add_Emails(Initial_List);
        }
        string[] To_List(string Text)
        {
            return Text.Replace(' ', ';').Replace(',', ';').Replace("\r\n", ";").Replace("\t", ";").Split(';');
        }

        public void Add_Emails(string Email_List)
        {
            foreach (string S in To_List(Email_List.ToLower()))
            {
                string Email = S.Trim();
                if (Email == "" || Email == "??") continue;
                if (!JR.Mails.Is_Valid_Address(Email)) Email = "?? " + Email;
                if (ContainsKey(Email)) continue;
                Add (Email, null);
            }
        }

        public string [] Emails
        {
            get 
            {
                var L = Keys.ToList();
                L.Sort();
                return L.ToArray();
            }
        }

        public List<string> Valid_List
        {
            get
            {
                var L = new List<string>();
                foreach (string S in Emails)
                {
                    if (S.StartsWith("??")) continue;
                    L.Add(S);
                }
                return L;
            }
        }

        public string Full_Content
        {
            get
            {
                var SB = new StringBuilder();
                foreach (string S in Emails)
                {
                    if (SB.Length > 0) SB.Append(';');
                    SB.Append(S);
                }
                return SB.ToString();
            }
        }

    }
}
