﻿using JR.Saf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using RazorEngine;
using RazorEngine.Templating;
using System.Text.RegularExpressions;

namespace JR.Mailing
{
    public class Mail_Template : MailMessage
    {
        //public string Body = "";
        //public string Subject = "";
        //public string From = "";
        //public string Sender = "";
        //public string To = "";
        public Bag Bag = new Bag();
        static IRazorEngineService RZS = null;

        Dictionary<string, LinkedResource> Cids = new Dictionary<string,LinkedResource>();
        string Cid_Suffix = string.Format("@{0:N}", Guid.NewGuid());


        public void Generate(string File_Template)
        {
            bool Is_New_Code;
            string Code = Main.Universal_Cached_File(File_Template, out Is_New_Code);

            string HTML_Body = "";

            IsBodyHtml = true;
            BodyEncoding = System.Text.Encoding.Default;
            if (RZS == null)
            {
                var Config = new RazorEngine.Configuration.TemplateServiceConfiguration();
                Config.Debug = Main.Bool_Parameter("RAZOR.MAILING.DEBUG");
                RZS = RazorEngine.Templating.RazorEngineService.Create(Config);
                Is_New_Code = true;
            }
            if (Is_New_Code)
            {
                HTML_Body = RZS.RunCompile(Code, File_Template, this.GetType(), this);
            } else
            {
                HTML_Body = RZS.Run(File_Template, this.GetType(), this).Trim();
            }

            // Ajouter deux alternate views
            var Text_Alt = AlternateView.CreateAlternateViewFromString(HTML_Body.To_Plain_Text(), System.Text.Encoding.ASCII, "text/plain");
            AlternateViews.Add(Text_Alt);
            var Html_Alt = AlternateView.CreateAlternateViewFromString(HTML_Body, System.Text.Encoding.Default, "text/html");
            foreach (var LR in Cids)
            {
                Html_Alt.LinkedResources.Add(LR.Value);
            }
            AlternateViews.Add(Html_Alt);

            //Body = RazorEngine.Razor.Parse<Mail_Template>(Code, this).Trim();


        }
        public string Include (string File_Name)
        {
            foreach (var S in JR.Files.Safe_Read(JR.Saf.Main.Real_File_Name(File_Name), false))
            {
                return S;
            }
            return "";
        }

        public bool Send ()
        {
            bool Success = false;
            try
            {
                using (System.Net.Mail.SmtpClient Client = new System.Net.Mail.SmtpClient(Main.Parameter("SMTP_Server")))
                {
                    Client.Send(this);
                }
                Success = true;
            }
            catch
            {
            }
            foreach (var Att in Attachments) Att.Dispose();
            return Success;
        }

        public string To_Html (string Source)
        {
            string[] Lines = Source.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            if (Lines.Length < 2) return HttpUtility.HtmlEncode(Source);
            var SB = new StringBuilder();
            foreach (string P in Lines)
            {
                if (SB.Length > 0) SB.Append("<br/>");
                SB.Append(HttpUtility.HtmlEncode(P));
            }
            return SB.ToString();
        }

        public string To_Base64(string File_Name)
        {
            var File_Content = JR.Files.Full_File(File_Name);
            return "data:" + JR.Files.Mime_String(File_Name) + ";base64," + System.Convert.ToBase64String(File_Content);
        }

        public string To_Cid(string File_Name, string URL)
        {
            var Temp_File = System.IO.Path.GetTempFileName();
            var Paths = JR.Files.Paths(File_Name);
            var Content_Id = (Paths.File + Cid_Suffix).ToLower();
            LinkedResource LR = null;
            if (!Cids.TryGetValue(Content_Id, out LR))
            {
                try
                {
                    Net_File.Download(URL, Temp_File);
                    LR = new LinkedResource(Temp_File, JR.Files.Mime_String(File_Name)) { ContentId = Content_Id, TransferEncoding = System.Net.Mime.TransferEncoding.Base64 };
                    Cids.Add(Content_Id, LR);
                }
                catch
                {
                }
            }
            JR.Files.Safe_Delete(Temp_File);
            return "cid:" + Content_Id;
        }

        public string To_Cid(string File_Name)
        {

            var Paths = JR.Files.Paths(File_Name);
            var Content_Id = (Paths.File + Cid_Suffix).ToLower();
            LinkedResource LR = null;
            if (!Cids.TryGetValue(Content_Id, out LR))
            {
                try
                {
                    LR = new LinkedResource(File_Name, JR.Files.Mime_String(File_Name)) {ContentId = Content_Id, TransferEncoding = System.Net.Mime.TransferEncoding.Base64 };
                    Cids.Add(Content_Id, LR);
                }
                catch
                {
                }
            }
            return "cid:" +  Content_Id;
        }

        public int Schedule(JR.DB.Dbase Db, Guid Owner, List<string> EM_Dest, string EM_Control)
        {
            // Supprimer la session precedente
            Db.Exec("delete from JR$Mailing where source=@0", Owner);
            // Créer une nouvelle session
            var S_Id = Db.Insert_Request("insert into JR$MAILING (source, stream,status, d_create, EM_Wait, EM_Control) values (@0,@1,'S', getdate(), @2,@3)",
                Owner, JR.Mail.SMailMessage.Serialize_From_Mail(this), JR.Serial<List<string>>.To_Json (EM_Dest), EM_Control);
            return (int)S_Id;
        }

        public bool Send_Preview(string Address)
        {
            bool Success = false;
            var Stream = JR.Mail.SMailMessage.Serialize_From_Mail(this);
            var MM = JR.Mail.SMailMessage.Create_From(Stream);
            MM.To.Clear();
            MM.To.Add(Address);
            try
            {
                using (System.Net.Mail.SmtpClient Client = new System.Net.Mail.SmtpClient(Main.Parameter("SMTP_Server")))
                {
                    Client.Send(MM);
                }
                Success = true;
            }
            catch
            {
            }
            foreach (var Att in MM.Attachments) Att.Dispose();
            return Success;
        }

    }
    public class Mail_Template<T> : Mail_Template
    {
        public T M;
    }
}