﻿using JR.Saf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace JR.Mailing
{
    public class Mail_Generator
    {
        public string Body = "";
        public string Subject = "";
        public string From = "";
        public string Sender = "";
        public string To = "";
        public Bag Bag = new Bag();
        public void Generate(string File_Template)
        {
            bool Is_New_Code;
            string Code = Main.Universal_Cached_File(File_Template, out Is_New_Code);
            Body = RazorEngine.Razor.Parse<Mail_Generator>(Code, this).Trim();
        }
        public MailMessage Mail
        {
            get
            {
                var M = new MailMessage();
                if (From != "") M.From = new MailAddress(From);
                if (Sender != "") M.Sender = new MailAddress(Sender);
                if (To != "") M.To.Add(To);
                M.Subject = Subject;
                M.Body = Body;
                M.IsBodyHtml = true;
                M.BodyEncoding = System.Text.Encoding.Default;
                return M;
            }
        }
        public string Include (string File_Name)
        {
            foreach (var S in JR.Files.Safe_Read(JR.Saf.Main.Real_File_Name(File_Name), false))
            {
                return S;
            }
            return "";
        }
    }
    public class Mail_Generator<T>:Mail_Generator
    {
        public T M;
    }
}