﻿using JR.DB;
using System.Data.Common;
using Microsoft.Data.SqlClient;

namespace JR.SQL_Server
{
    public static class Injector
    {

        // ne pas utiliser en .Net Framework
        public static bool Used { get; private set; } = false;
        public static void Use()
        {
            if (Used) return;
            JR.Saf.Logs.Log("Boostraping MSSQL");
            SQL_Db.Register(new MSSql_Prov());
            Used = true;
        }

        class MSSql_Prov : I_Sql_Provider
        {
            public DbDataAdapter New_Adapter(string Request, DbConnection Connection)
            {
                return new SqlDataAdapter(Request, Connection as SqlConnection);
            }

            public DbConnection New_Connection(string Connection_String)
            {
                return new SqlConnection(Connection_String + ";TrustServerCertificate=true");
            }

            public bool Provides(string Connection_String, out string Mode)
            {
                Mode = "MSSQL";
                return Connection_String.ToLower().Contains("data source=");
            }

        }


    }
}
