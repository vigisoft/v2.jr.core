﻿namespace fleXdoc.ServiceClient
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.btnBrowseDataXMLFile = new System.Windows.Forms.Button();
            this.txtTemplate = new System.Windows.Forms.TextBox();
            this.txtDataXmlFile = new System.Windows.Forms.TextBox();
            this.dlgOpenFile = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnBuild = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.rdMTOM = new System.Windows.Forms.RadioButton();
            this.rdText = new System.Windows.Forms.RadioButton();
            this.txtEndpointBase = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtOutputFolder = new System.Windows.Forms.TextBox();
            this.btnBrowseOutputFolder = new System.Windows.Forms.Button();
            this.dlgOpenFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.cbValidation = new System.Windows.Forms.CheckBox();
            this.cbOpenAfterBuild = new System.Windows.Forms.CheckBox();
            this.lblDocLink = new System.Windows.Forms.LinkLabel();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCulture = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboOutputFormat = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnRetrieve = new System.Windows.Forms.Button();
            this.btnDefaultSettings = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnBrowseDataXMLFile
            // 
            this.btnBrowseDataXMLFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseDataXMLFile.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnBrowseDataXMLFile.Location = new System.Drawing.Point(499, 35);
            this.btnBrowseDataXMLFile.Name = "btnBrowseDataXMLFile";
            this.btnBrowseDataXMLFile.Size = new System.Drawing.Size(63, 23);
            this.btnBrowseDataXMLFile.TabIndex = 3;
            this.btnBrowseDataXMLFile.Text = "Browse...";
            this.btnBrowseDataXMLFile.UseVisualStyleBackColor = true;
            this.btnBrowseDataXMLFile.Click += new System.EventHandler(this.btnBrowseDataXMLFile_Click);
            // 
            // txtTemplate
            // 
            this.txtTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTemplate.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtTemplate.Location = new System.Drawing.Point(107, 10);
            this.txtTemplate.Name = "txtTemplate";
            this.txtTemplate.Size = new System.Drawing.Size(455, 20);
            this.txtTemplate.TabIndex = 1;
            // 
            // txtDataXmlFile
            // 
            this.txtDataXmlFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDataXmlFile.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtDataXmlFile.Location = new System.Drawing.Point(107, 37);
            this.txtDataXmlFile.Name = "txtDataXmlFile";
            this.txtDataXmlFile.Size = new System.Drawing.Size(386, 20);
            this.txtDataXmlFile.TabIndex = 2;
            // 
            // dlgOpenFile
            // 
            this.dlgOpenFile.DefaultExt = "xml";
            this.dlgOpenFile.FileName = "data.xml";
            this.dlgOpenFile.Filter = "XML files|*.xml|All files|*.*";
            this.dlgOpenFile.Title = "Browse for data XML file";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "&Template:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "&Data XML file:";
            // 
            // btnBuild
            // 
            this.btnBuild.Location = new System.Drawing.Point(16, 299);
            this.btnBuild.Name = "btnBuild";
            this.btnBuild.Size = new System.Drawing.Size(139, 23);
            this.btnBuild.TabIndex = 9;
            this.btnBuild.Text = "Build";
            this.btnBuild.UseVisualStyleBackColor = true;
            this.btnBuild.Click += new System.EventHandler(this.btnBuild_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.rdMTOM);
            this.groupBox1.Controls.Add(this.rdText);
            this.groupBox1.Controls.Add(this.txtEndpointBase);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(16, 180);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(546, 98);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Service";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "&Method:";
            // 
            // rdMTOM
            // 
            this.rdMTOM.AutoSize = true;
            this.rdMTOM.Location = new System.Drawing.Point(100, 69);
            this.rdMTOM.Name = "rdMTOM";
            this.rdMTOM.Size = new System.Drawing.Size(112, 17);
            this.rdMTOM.TabIndex = 12;
            this.rdMTOM.Text = "Streamed (MTOM)";
            this.rdMTOM.UseVisualStyleBackColor = true;
            // 
            // rdText
            // 
            this.rdText.AutoSize = true;
            this.rdText.Location = new System.Drawing.Point(100, 46);
            this.rdText.Name = "rdText";
            this.rdText.Size = new System.Drawing.Size(95, 17);
            this.rdText.TabIndex = 11;
            this.rdText.Text = "Buffered (Text)";
            this.rdText.UseVisualStyleBackColor = true;
            // 
            // txtEndpointBase
            // 
            this.txtEndpointBase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndpointBase.Location = new System.Drawing.Point(100, 19);
            this.txtEndpointBase.Name = "txtEndpointBase";
            this.txtEndpointBase.Size = new System.Drawing.Size(424, 20);
            this.txtEndpointBase.TabIndex = 10;
            this.txtEndpointBase.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "&Endpoint URL:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 93);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "&Output folder:";
            // 
            // txtOutputFolder
            // 
            this.txtOutputFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOutputFolder.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtOutputFolder.Location = new System.Drawing.Point(107, 90);
            this.txtOutputFolder.Name = "txtOutputFolder";
            this.txtOutputFolder.Size = new System.Drawing.Size(386, 20);
            this.txtOutputFolder.TabIndex = 4;
            // 
            // btnBrowseOutputFolder
            // 
            this.btnBrowseOutputFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseOutputFolder.Location = new System.Drawing.Point(500, 88);
            this.btnBrowseOutputFolder.Name = "btnBrowseOutputFolder";
            this.btnBrowseOutputFolder.Size = new System.Drawing.Size(62, 23);
            this.btnBrowseOutputFolder.TabIndex = 5;
            this.btnBrowseOutputFolder.Text = "Browse...";
            this.btnBrowseOutputFolder.UseVisualStyleBackColor = true;
            this.btnBrowseOutputFolder.Click += new System.EventHandler(this.btnBrowseOutputFolder_Click);
            // 
            // dlgOpenFolder
            // 
            this.dlgOpenFolder.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // statusStrip1
            // 
            this.statusStrip1.AutoSize = false;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 341);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(574, 22);
            this.statusStrip1.TabIndex = 11;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // cbValidation
            // 
            this.cbValidation.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbValidation.Location = new System.Drawing.Point(12, 118);
            this.cbValidation.Name = "cbValidation";
            this.cbValidation.Size = new System.Drawing.Size(110, 17);
            this.cbValidation.TabIndex = 6;
            this.cbValidation.Text = "&Validation:";
            this.cbValidation.UseVisualStyleBackColor = true;
            // 
            // cbOpenAfterBuild
            // 
            this.cbOpenAfterBuild.AutoSize = true;
            this.cbOpenAfterBuild.Checked = true;
            this.cbOpenAfterBuild.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbOpenAfterBuild.Location = new System.Drawing.Point(171, 303);
            this.cbOpenAfterBuild.Name = "cbOpenAfterBuild";
            this.cbOpenAfterBuild.Size = new System.Drawing.Size(101, 17);
            this.cbOpenAfterBuild.TabIndex = 13;
            this.cbOpenAfterBuild.TabStop = false;
            this.cbOpenAfterBuild.Text = "O&pen after build";
            this.cbOpenAfterBuild.UseVisualStyleBackColor = true;
            // 
            // lblDocLink
            // 
            this.lblDocLink.AutoSize = true;
            this.lblDocLink.Location = new System.Drawing.Point(291, 304);
            this.lblDocLink.Name = "lblDocLink";
            this.lblDocLink.Size = new System.Drawing.Size(43, 13);
            this.lblDocLink.TabIndex = 0;
            this.lblDocLink.TabStop = true;
            this.lblDocLink.Tag = "";
            this.lblDocLink.Text = "Doclink";
            this.lblDocLink.Visible = false;
            this.lblDocLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblDocLink_LinkClicked);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "&Culture:";
            // 
            // txtCulture
            // 
            this.txtCulture.Location = new System.Drawing.Point(107, 64);
            this.txtCulture.Name = "txtCulture";
            this.txtCulture.Size = new System.Drawing.Size(69, 20);
            this.txtCulture.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(182, 67);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "(leave empty for default)";
            // 
            // comboOutputFormat
            // 
            this.comboOutputFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboOutputFormat.FormattingEnabled = true;
            this.comboOutputFormat.Location = new System.Drawing.Point(107, 142);
            this.comboOutputFormat.Name = "comboOutputFormat";
            this.comboOutputFormat.Size = new System.Drawing.Size(121, 21);
            this.comboOutputFormat.TabIndex = 17;
            this.comboOutputFormat.UseWaitCursor = true;
            this.comboOutputFormat.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 145);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Output &Format:";
            this.label8.UseWaitCursor = true;
            this.label8.Visible = false;
            // 
            // btnRetrieve
            // 
            this.btnRetrieve.Location = new System.Drawing.Point(234, 141);
            this.btnRetrieve.Name = "btnRetrieve";
            this.btnRetrieve.Size = new System.Drawing.Size(68, 23);
            this.btnRetrieve.TabIndex = 19;
            this.btnRetrieve.Text = "Retrieve";
            this.btnRetrieve.UseVisualStyleBackColor = true;
            this.btnRetrieve.UseWaitCursor = true;
            this.btnRetrieve.Visible = false;
            this.btnRetrieve.Click += new System.EventHandler(this.btnRetrieve_Click);
            // 
            // btnDefaultSettings
            // 
            this.btnDefaultSettings.Location = new System.Drawing.Point(467, 299);
            this.btnDefaultSettings.Name = "btnDefaultSettings";
            this.btnDefaultSettings.Size = new System.Drawing.Size(95, 23);
            this.btnDefaultSettings.TabIndex = 20;
            this.btnDefaultSettings.TabStop = false;
            this.btnDefaultSettings.Text = "Default Settings";
            this.btnDefaultSettings.UseVisualStyleBackColor = true;
            this.btnDefaultSettings.Click += new System.EventHandler(this.btnDefaultSettings_Click);
            // 
            // frmMain
            // 
            this.AcceptButton = this.btnBuild;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 363);
            this.Controls.Add(this.btnDefaultSettings);
            this.Controls.Add(this.btnRetrieve);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboOutputFormat);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtCulture);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblDocLink);
            this.Controls.Add(this.cbOpenAfterBuild);
            this.Controls.Add(this.cbValidation);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnBrowseOutputFolder);
            this.Controls.Add(this.txtOutputFolder);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnBuild);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtDataXmlFile);
            this.Controls.Add(this.txtTemplate);
            this.Controls.Add(this.btnBrowseDataXMLFile);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "fleXdoc Service Client";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBrowseDataXMLFile;
        private System.Windows.Forms.TextBox txtTemplate;
        private System.Windows.Forms.TextBox txtDataXmlFile;
        private System.Windows.Forms.OpenFileDialog dlgOpenFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnBuild;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtEndpointBase;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton rdMTOM;
        private System.Windows.Forms.RadioButton rdText;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtOutputFolder;
        private System.Windows.Forms.Button btnBrowseOutputFolder;
        private System.Windows.Forms.FolderBrowserDialog dlgOpenFolder;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.CheckBox cbValidation;
        private System.Windows.Forms.CheckBox cbOpenAfterBuild;
        private System.Windows.Forms.LinkLabel lblDocLink;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCulture;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboOutputFormat;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnRetrieve;
        private System.Windows.Forms.Button btnDefaultSettings;
    }
}