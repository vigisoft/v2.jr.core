﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace fleXdoc.ServiceClient
{
    internal static class IOHelper
    {
        public static void CopyStream(Stream source, Stream target)
        {
            long srcPos = (source.CanSeek ? source.Position : long.MinValue);
            long destPos = (target.CanSeek ? target.Position : long.MinValue);

            const int bufSize = 0x1000;
            byte[] buf = new byte[bufSize];
            int bytesRead = 0;
            while ((bytesRead = source.Read(buf, 0, bufSize)) > 0)
            {
                target.Write(buf, 0, bytesRead);
            }

            // Seek streams back to original position
            if (target.CanSeek)
            {
                target.Position = destPos;
            }
            if (source.CanSeek)
            {
                source.Position = srcPos;
            }
        }
    }
}
