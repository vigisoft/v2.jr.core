﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.IO;

namespace fleXdoc.ServiceClient
{
    internal interface IConfigManager
    {
        void Load();
        void Save();
        void ResetDefaults();
        Config Config { get; set; }
    }

    internal class Config
    {
        public string Template {get;set;}
        public string RenderCulture {get;set;}
        public string DataFilePath { get; set; }
        public string OutputPath { get; set; }
        public bool Validate { get; set; }
        public string EndpointAddressBase { get; set; }
        public bool MTOM { get; set; }
        public string OutputFormat { get; set; }

        public Config()
        {
            // Set defaults
            Template = "test/OrderTemplate.docx";
            RenderCulture = string.Empty;
            DataFilePath = "orderData.xml";
            OutputPath = Path.GetTempPath();
            Validate = true;
            EndpointAddressBase = "http://localhost/fleXdoc/Service.svc";
            MTOM = true;
            OutputFormat = string.Empty;
        }
    }

    internal class RegistryConfigManager : IConfigManager
    {
        public RegistryConfigManager()
        {
            ResetDefaults(); // Instantiates the Config-property
        }

        public void Load()
        {
            Config cfg = new Config();
            using (RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\RobertTeKaat\fleXdoc\ServiceClient", false))
            {
                if (key != null)
                {
                    cfg.Template = (string)key.GetValue("Template", cfg.Template);
                    cfg.RenderCulture = (string)key.GetValue("RenderCulture", cfg.RenderCulture);
                    cfg.DataFilePath = (string)key.GetValue("DataFilePath", cfg.DataFilePath);
                    cfg.OutputPath = (string)key.GetValue("OutputPath", cfg.OutputPath);
                    cfg.Validate = ((int)key.GetValue("Validation", cfg.Validate ? 1 : 0) == 1);
                    cfg.EndpointAddressBase = (string)key.GetValue("EndpointAddressBase", cfg.EndpointAddressBase);
                    cfg.MTOM = ((int)key.GetValue("MTOM", cfg.MTOM ? 1 : 0) == 1);
                    cfg.OutputFormat = (string)key.GetValue("OutputFormat", cfg.OutputFormat);
                }
            }
            Config = cfg;
        }

        public void Save()
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\RobertTeKaat\fleXdoc\ServiceClient", true);
            if (key == null)
            {
                key = Registry.CurrentUser.OpenSubKey("Software", true)
                    .CreateSubKey("RobertTeKaat")
                    .CreateSubKey("fleXdoc")
                    .CreateSubKey("ServiceClient");
            }

            using (key)
            {
                Config cfg = Config;
                key.SetValue("Template", cfg.Template, RegistryValueKind.String);
                key.SetValue("RenderCulture", cfg.RenderCulture, RegistryValueKind.String);
                key.SetValue("DataFilePath", cfg.DataFilePath, RegistryValueKind.String);
                key.SetValue("OutputPath", cfg.OutputPath, RegistryValueKind.String);
                key.SetValue("Validation", cfg.Validate ? 1 : 0, RegistryValueKind.DWord);
                key.SetValue("EndpointAddressBase", cfg.EndpointAddressBase, RegistryValueKind.String);
                key.SetValue("MTOM", cfg.MTOM ? 1 : 0, RegistryValueKind.DWord);
                key.SetValue("OutputFormat", cfg.OutputFormat, RegistryValueKind.String);
            }
        }

        public void ResetDefaults()
        {
            Config = new Config();
        }

        public Config Config
        {
            get;
            set;
        }
    }
}
