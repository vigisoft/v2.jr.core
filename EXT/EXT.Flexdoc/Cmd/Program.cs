﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using fleXdoc.Api;
using System.Threading;

namespace fleXdoc.Cmd
{
    /// <summary>
    /// To test FlexDoc, you must:
    ///  1. Set Cmd as StartUp Project
    ///  2. Use this argument-string (through Cmd's project properties-window, Debug-tab):
    ///     OrderTemplate.docx orderData.xml order.docx
    ///  3. Debug the solution (play)
    ///  4. In bin/debug you will now find the generated document: Order.docx
    /// </summary>
    class Program
    {
        static int Main(string[] args)
        {
            if (args.Length < 3)
            {
                Console.WriteLine("usage: FlexDoc.Cmd <template-file> <data-file> <output-file> [ns-prefix] [ns]");
                return 1;
            }

            string nsPrefix = args.Length > 3 ? args[3] : null;
            string ns = args.Length > 4 ? args[4] : null;

            try
            {
                // Create a DocBuilder for the template
                using (DocBuilder builder = new DocBuilder(args[0]))
                {
                    // Use the DocBuilder create the document for the specified data-file
                    builder.Build(args[1], nsPrefix, ns, args[2], Thread.CurrentThread.CurrentUICulture, true);
                }

                Console.WriteLine("document successfully generated (docx)");
                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("\nerror when building the document: " + ex.Message);
#if DEBUG
                Console.ReadLine();
#endif
                return 2;
            }

        }
    }
}
