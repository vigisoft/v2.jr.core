﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace fleXdoc.Service
{
    /// <summary>
    /// 
    /// </summary>
    /// <example>
    /// 
  //    <configSections>
  //  <section name="fleXdoc" type="fleXdoc.Service.fleXdocConfig" />
  //</configSections>


  //   <fleXdoc>
  //  <outputConvertors>
  //    <outputConvertor format="doc" type="fleXdocContrib.AsposeWords.FormatConversion, fleXdocContrib.AsposeWords">
  //      <args>
  //        <arg name="Licence" value="" />
  //      </args>
  //    </outputConvertor>
  //    <outputConvertor format="odt" type="fleXdocContrib.AsposeWords.FormatConversion, fleXdocContrib.AsposeWords">
  //      <args>
  //        <arg name="Licence" value="" />
  //      </args>
  //    </outputConvertor>
  //    <outputConvertor format="pdf" type="fleXdocContrib.AsposeWords.FormatConversion, fleXdocContrib.AsposeWords">
  //      <args>
  //        <arg name="Licence" value="" />
  //      </args>
  //    </outputConvertor>
  //    <outputConvertor format="pdf-a" type="fleXdocContrib.AsposeWords.FormatConversion, fleXdocContrib.AsposeWords">
  //      <args>
  //        <arg name="Licence" value="" />
  //      </args>
  //    </outputConvertor>
  //    <outputConvertor format="xps" type="fleXdocContrib.AsposeWords.FormatConversion, fleXdocContrib.AsposeWords">
  //      <args>
  //        <arg name="Licence" value="" />
  //      </args>
  //    </outputConvertor>
  //  </outputConvertors>
  //</fleXdoc>
    /// </example>
    public class fleXdocConfig : ConfigurationSection
    {
        [ConfigurationProperty("outputConvertors", IsDefaultCollection = true)]
        public OutputConvertorCollection OutputConvertors
        {
            get { return (OutputConvertorCollection)base["outputConvertors"]; }
        }
    }

    public class OutputConvertor : ConfigurationElement
    {
        [ConfigurationProperty("format", IsKey = true, IsRequired = true)]
        public string Format
        {
            get { return (string)this["format"]; }
            set { this["format"] = value; }
        }

        [ConfigurationProperty("type", IsRequired = true)]
        public string Type
        {
            get { return (string)this["type"]; }
            set { this["type"] = value; }
        }

        [ConfigurationProperty("args", IsDefaultCollection = true)]
        public OutputConvertorArgumentCollection Arguments
        {
            get { return (OutputConvertorArgumentCollection)base["args"]; }
        }
    }

    public class OutputConvertorCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new OutputConvertor();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((OutputConvertor)element).Format;
        }

        protected override bool ThrowOnDuplicate
        {
            get { return true; }
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        protected override string ElementName
        {
            get { return "outputConvertor"; }
        }
    }

    public class OutputConvertorArgumentCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new OutputConvertorArgument();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((OutputConvertorArgument)element).Name;
        }

        protected override bool ThrowOnDuplicate
        {
            get { return true; }
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        protected override string ElementName
        {
            get { return "arg"; }
        }
    }

    public class OutputConvertorArgument : ConfigurationElement
    {
        [ConfigurationProperty("name", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }

        [ConfigurationProperty("value")]
        public string Value
        {
            get { return (string)this["value"]; }
            set { this["value"] = value; }
        }
    }

    //public sealed class EmployeeCollection : ConfigurationElementCollection
    //{
    //    protected override string ElementName
    //    {

    //        get { return "EmployeeElement"; }

    //    }

    //    public EmployeeElement this[int index]
    //    {

    //        get { return (EmployeeElement)BaseGet(index); }

    //        set
    //        {

    //            if (BaseGet(index) != null)
    //            {

    //                BaseRemoveAt(index);

    //            }

    //            BaseAdd(index, value);

    //        }

    //    }

    //    new public EmployeeElement this[string employeeID]
    //    {

    //        get { return (EmployeeElement)BaseGet(employeeID); }

    //    }

    //    public bool ContainsKey(string key)
    //    {

    //        bool result = false;

    //        object[] keys = BaseGetAllKeys();

    //        foreach (object obj in keys)
    //        {

    //            if ((string)obj == key)
    //            {

    //                result = true;

    //                break;

    //            }

    //        }

    //        return result;

    //    }

    //}


}
