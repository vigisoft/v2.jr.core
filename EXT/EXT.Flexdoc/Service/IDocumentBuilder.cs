﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;

namespace fleXdoc.Service
{
    [ServiceContract(Namespace="urn:fleXdoc")]
    public interface IDocumentBuilder
    {
        [OperationContract]
        Stream BuildDocument(BuildDocumentRequest request);

        //[OperationContract]
        //SupportedOutputFormats GetSupportedOutputFormats();
    }
}
