@echo off

rem *** This batch file creates .NET classes from the types defined in the XML Schema�s (XSD's)
rem *** that are included with this project. After modifying any of the schema's, make sure this
rem *** tool is executed to regenerate the classes.
rem *** NB: Before running this tool, first check out GeneratedObjects.cs.

svcutil *.xsd /dconly /s /edb /n:*,fleXdoc.Service /out:GeneratedObjects.cs