﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.XPath;
using System.Globalization;

namespace fleXdoc.Api
{
    internal class BuildOptions
    {
        public XPathNavigator Data { get; set; }
        public string NamespacePrefix { get; set; }
        public string NamespaceUri { get; set; }
        public bool ValidatePackage { get; set; }
        public CultureInfo RenderCulture { get; set; }
    }
}
