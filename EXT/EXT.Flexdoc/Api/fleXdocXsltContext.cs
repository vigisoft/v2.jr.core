﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Xsl;
using System.Xml;
using System.Xml.XPath;

namespace fleXdoc.Api
{
    public class fleXdocXsltContext : XsltContext
    {
        #region Private instance variables and properties
        public XPathNodeIterator CurrentNode { get; set; }
        #endregion

        #region Constructor(s)
        public fleXdocXsltContext(NameTable table)
            : base(table)
        {
            if (!this.HasNamespace("fd"))
            {
                this.AddNamespace("fd", "urn:fleXdocXsltContext"); // Only the prefix matters...
            }
        }
        #endregion

        public override IXsltContextFunction ResolveFunction(string prefix, string name, System.Xml.XPath.XPathResultType[] ArgTypes)
        {
            // Support for custom xpath functions
            if (prefix == "fd"
                && name == "current")
            {
                return new fleXdocXslCurrentNodeFunction();
            }

            return null;
        }

        #region Plumbing
        public override int CompareDocument(string baseUri, string nextbaseUri)
        {
            return 0;
        }

        public override bool PreserveWhitespace(System.Xml.XPath.XPathNavigator node)
        {
            return true;
        }
        public override IXsltContextVariable ResolveVariable(string prefix, string name)
        {
            return null;
        }

        public override bool Whitespace
        {
            get { return true; }
        }
        #endregion
    }
}
