﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fleXdoc.Api
{
    /// <summary>
    /// Event to resolve uri's to resources. This is usually nessecary when the template itself was supplied
    /// as a stream or when the template is not located in the current directory of the process.
    /// </summary>
    /// <param name="sender">Sender of this event</param>
    /// <param name="args">Specifies the original resource Uri to be resolved</param>
    /// <returns>The new resolved resource uri or null if the resource uri could not be resolved.</returns>
    public delegate string ResolveResourceUriEventHandler(object sender, ResolveResourceUriEventArgs args);
    
    [Serializable]
    public class ResolveResourceUriEventArgs : EventArgs
    {
        private string _resourceUri;

        public ResolveResourceUriEventArgs(string resourceUri)
        {
            _resourceUri = resourceUri;
        }

        public string ResourceUri
        {
            get { return _resourceUri; }
        }
    }
}
