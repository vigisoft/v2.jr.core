﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Xsl;
using System.Xml.XPath;

namespace fleXdoc.Api
{

    public class fleXdocXslCurrentNodeFunction : IXsltContextFunction
    {
        public object Invoke(XsltContext xsltContext, object[] args, XPathNavigator docContext)
        {
            fleXdocXsltContext fdCtx = xsltContext as fleXdocXsltContext;
            if (fdCtx != null)
            {
                XPathNodeIterator currentNode = fdCtx.CurrentNode;
                return currentNode;
            }

            throw new NotSupportedException("fleXdocXslCurrentNodeFunction requires an Xslt-context of type fleXdocXsltContext");
        }

        #region Plumbing
        public XPathResultType[] ArgTypes
        {
            get { return null; }
        }

        public int Maxargs
        {
            get { return 0; }
        }

        public int Minargs
        {
            get { return 0; }
        }

        public XPathResultType ReturnType
        {
            get { return XPathResultType.NodeSet; }
        }
        #endregion
    }
}
