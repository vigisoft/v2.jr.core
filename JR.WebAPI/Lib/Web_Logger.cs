﻿using Microsoft.AspNetCore.Http.Extensions;
using System.Text;
using System.Web;

namespace JR.WebAPI
{
    public class Web_Logger
    {
        object R_Lock = new object();
        long R_Id = 0;
        int Log_Rsp_Body = Saf.Main.Int_Parameter("RESPONSE.BODY.LOG");
        int Log_Req_Body = Saf.Main.Int_Parameter("REQUEST.BODY.LOG");
        bool Log_Rsp_Headers = Saf.Main.Bool_Parameter("RESPONSE.HEADERS.LOG");
        bool Log_Req_Headers = Saf.Main.Bool_Parameter("REQUEST.HEADERS.LOG");
        void Log_Request(HttpContext C, out string Id, string Req_Body)
        {
            lock (R_Lock)
            {
                R_Id++;
                Id = $"[{R_Id}]";
            }
            var SB = new StringBuilder(Id);
            var R = C.Request;
            var URL = R.GetDisplayUrl(); //            HttpUtility.UrlDecode(R.Path + R.QueryString);
            SB.Append($" [{R.Method}] [{C.Connection.RemoteIpAddress?.ToString()}] {URL}");
            var Sep = URL.Contains("?") ? "&" : "?";
            if (R.HasFormContentType)
            {
                foreach (var K in R.Form.Keys)
                {
                    SB.Append(Sep);
                    SB.Append(K);
                    SB.Append("=");
                    SB.Append(R.Form[K]);
                    Sep = "&";
                }
            }
            if (Log_Req_Headers)
            {
                foreach (var H in R.Headers)
                {
                    SB.AppendLine();
                    SB.Append($"  {H.Key}={H.Value}");
                }
            }
            if (Req_Body?.Length > 0)
            {
                SB.AppendLine();
                SB.Append($"  Body:{Req_Body}");
            }
            Saf.Logs.File_Log("http", SB.ToString());
        }

        void Log_Response(HttpContext C, string Id, long Len, string Body)
        {
            var R = C.Response;
            var SB = new StringBuilder(Id);
            SB.Append($" [{R.StatusCode}] len:{Len}");
            if (Log_Rsp_Headers)
            {
                foreach (var H in R.Headers)
                {
                    SB.AppendLine();
                    SB.Append($"  {H.Key}={H.Value}");
                }
            }
            if (Body?.Length > 0)
            {
                SB.AppendLine();
                SB.Append($"  Body:{Body}");
            }
            Saf.Logs.File_Log("http", SB.ToString());


            if (R.StatusCode >= 400)
            {
                var RQ = C.Request;
                var URL = RQ.GetDisplayUrl(); //            HttpUtility.UrlDecode(R.Path + R.QueryString);
                SB.AppendLine();
                SB.Append($" [{RQ.Method}] [{C.Connection.RemoteIpAddress?.ToString()}] {URL}");
                Saf.Logs.File_Log("http_err", SB.ToString());
            }
        }

        HashSet<string> Ignored_Req = null;

        public void Ignore_Log(string Model)
        {
            if (Ignored_Req == null) Ignored_Req = new HashSet<string>();
            Ignored_Req.Add(Model.ToLower());
        }

        bool Is_Ignored (HttpContext ctx)
        {
            if (Ignored_Req == null) return false;
            var Path = ctx.GetEndpoint()?.DisplayName;
            if (Path == null) return false;
            foreach (var R in Ignored_Req) if (Path.ToLower().EndsWith(R)) return true;
            return false;
        }
        public async Task Invoke(HttpContext context, RequestDelegate next)
        {
            string Req_Body = null;
            if (Is_Ignored(context)) 
            {
                await next(context);
                return;
            }
            if (Log_Req_Body > 0)
            {
                var New_Req_Body = new MemoryStream();
                await context.Request.Body.CopyToAsync(New_Req_Body);
                New_Req_Body.Seek(0, SeekOrigin.Begin);
                Req_Body = await new StreamReader(New_Req_Body).ReadToEndAsync();
                New_Req_Body.Seek(0, SeekOrigin.Begin);
                context.Request.Body = New_Req_Body;
                if (Log_Req_Body > 1 && Req_Body.Length > Log_Req_Body) Req_Body = Req_Body.Substring(0, Log_Req_Body);
            }

            Log_Request(context, out var Id, Req_Body);
            var Original_Rsp_Body = context.Response.Body;
            MemoryStream New_Rsp_Body = null;
            if (Log_Rsp_Body > 0)
            {
                New_Rsp_Body = new MemoryStream();
                context.Response.Body = New_Rsp_Body;
            }
            long Rsp_Len = 0;
            string? Rsp_Body = null;
            try
            {
                // Call the next delegate/middleware in the pipeline.
                await next(context);
            }
            finally
            {
                if (Log_Rsp_Body > 0)
                {
                    New_Rsp_Body.Seek(0, SeekOrigin.Begin);
                    Rsp_Body = await new StreamReader(New_Rsp_Body).ReadToEndAsync();
                    Rsp_Len = New_Rsp_Body.Length;
                    New_Rsp_Body.Seek(0, SeekOrigin.Begin);
                    await New_Rsp_Body.CopyToAsync(Original_Rsp_Body);
                    if (Log_Rsp_Body > 1 && Rsp_Body.Length > Log_Rsp_Body) Rsp_Body = Rsp_Body.Substring(0, Log_Rsp_Body);
                }
            }
            Log_Response(context, Id, Rsp_Len, Rsp_Body);
        }
    }
}
