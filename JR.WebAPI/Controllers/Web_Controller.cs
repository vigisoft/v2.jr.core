﻿using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.Text;

namespace JR.WebAPI
{
    // tag nécessaire pour ne pas planter la génération de doc swagger, qui sinon scanne toutes les méthodes
    [ApiExplorerSettings(IgnoreApi = true)]

    public class Web_Controller : Controller
    {

        static HashSet<int> Public_Ports = null;
        public bool Block_Public_Ports()
        {
            if (Public_Ports == null)
            {

                var PP = new HashSet<int>();
                PP.Add(80);
                PP.Add(443);
                foreach (var P in JR.Saf.Main.Parameter("public.ports").Split_List()) PP.Add(P.To_Integer());
                Public_Ports = PP;
            }
            var Port = HttpContext.Request.Host.Port?? (Request.IsHttps ? 443:80);
            if (Public_Ports.Contains(Port))
            {
                var URL = Request.GetDisplayUrl();
                var Mess= $" [{Request.Method}] [{Request.HttpContext.Connection.RemoteIpAddress?.ToString()}] {URL}";
                JR.Saf.Logs.File_Log("blocked", Mess);
                throw new Exception($"rejected port {Port}");
            }
            return false;
        }
        public string Arg(string Keys, string Default, bool Lower = true)
        {
            foreach (var Key in Keys.Split('|'))
            {
                if (Key == "jsbody" && Request.HasJsonContentType())
                {
                    using (var SR = new StreamReader(Request.Body))
                    {
                        return SR.ReadToEndAsync().GetAwaiter().GetResult();
                        // return SR.ReadToEnd(); IIS impose parfois des appels asynchrone
                    }
                }
                if ((Request.HasFormContentType && Request.Form.TryGetValue(Key, out var Val)) || Request.Query.TryGetValue(Key, out Val))
                {
                    var Value = Val.FirstOrDefault() ?? "";
                    if (Value != "") return Lower ? Value.Trim().ToLower() : Value.Trim();
                }
                if (Request.RouteValues.Count > 2)    // Il y a toujours au moins les clés action et controller
                {
                    switch (Key)
                    {
                        case "action":
                        case "controller":
                            continue;
                    }
                    if (Request.RouteValues.TryGetValue(Key, out var OVal))
                    {
                        var O_Value = OVal?.To_String().Trim() ?? "";
                        return Lower ? O_Value.ToLower() : O_Value;
                    }
                }
            }
            return Default;
        }

        public void Loop_Args(Action<string, string> On_Arg)
        {
            foreach (var Arg in Request.Query)
            {
                if (Arg.Key == null) continue;
                On_Arg(Arg.Key, Arg.Value);
            }
            if (Request.HasFormContentType)
            {
                foreach (var Arg in Request.Form)
                {
                    On_Arg(Arg.Key, Arg.Value);
                }
            }
            foreach (var Arg in Request.RouteValues)
            {
                switch (Arg.Key)
                {
                    case null:
                    case "action":
                    case "controller":
                        continue;
                }
                On_Arg(Arg.Key, Arg.Value?.ToString() ?? "");
            }
        }

        public bool Populate_From_Body(object O) => O.Populate(Arg("jsbody", "", false));

        public async Task<bool> Populate_From_Body_Async(object O)
        {
            if (!Request.HasJsonContentType()) return false;
            string Body = null;
            using var SR = new StreamReader(Request.Body);
            Body = await SR.ReadToEndAsync();
            if (Body == null) return false;
            return O.Populate(Body);
        }
        public Web_Call New_Call() => new Web_Call(this);
    }

    public class Web_Call
    {
        static object Lock = new object();
        static long Counter = 0;

        string Sign;
        Web_Controller WC;

        public List<string> Errors = new();
        StringBuilder Logger = new();
        string Request;

        public Web_Call(Web_Controller WC)
        {
            this.WC = WC;
            lock (Lock)
            {
                Counter++;
                Sign = "[" + Counter + "] ";
            }
            Request = WC.Request.Path.ToString();
            var SB = new StringBuilder($"{Sign} {Request}");
            WC.Loop_Args((A, V) =>
            {
                SB.AppendLine();
                SB.Append("  ");
                SB.Append(A);
                SB.Append("=");
                SB.Append(V);
            });
            JR.Saf.Logs.Log(SB.ToString());
        }

        public IActionResult OK_JS(object O)
        {
            JR.Saf.Logs.Log(Sign + Logger.ToString());
            var R = WC.Ok(O.To_Json(Indented: WC.Arg("formated", "0").To_Boolean(), Include_Defaults: WC.Arg("null", "1").To_Boolean()));
            //WC.Response.ContentType = "application/json; charset=utf-8";
            return R;
        }

        public IActionResult Problem(Exception E = null)
        {
            if (E != null)
            {
                Err($"Internal error executing {Request}");
                Err(E.Message);
                Err(E.StackTrace);
            }
            JR.Saf.Logs.Log(Sign + Logger.ToString());
            var R = WC.Problem(Errors.Assembled_With("\r\n"));
            return R;
        }
        public IActionResult Problem(params string[] Messages)
        {
            foreach (var M in Messages) Err(M);
            JR.Saf.Logs.Log(Sign + Logger.ToString());
            var R = WC.Problem(Errors.Assembled_With("\r\n"));
            return R;
        }

        public bool Err(string Message)
        {
            Errors.Add(Message);
            Logger.AppendLine("  " + Message);
            return false;
        }

        public void Trac(string Message) => Logger.AppendLine("  " + Message);

    }

}
