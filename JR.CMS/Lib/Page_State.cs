using System;
using System.Collections.Generic;
using System.Web.SessionState;
using System.Web.UI;
using System.Web;

namespace JR.CMS
{

	/// <summary>
	/// H�berge les informations li�es � une page
	/// </summary>
	public class Page_State
	{

        public string Uid { get; set; } = "";

        int _Tick = 1;
        public void Compare_Tick (int Tick)
        {
            Is_Refresh = (_Tick > Tick);
        }

        public int New_Tick
        {
            get
            {
                _Tick++;
                return _Tick; 
            }
        }

        public bool Is_Refresh { get; private set; } = false;

        public View View { get; set; } = null;

        // Methode bas�e sur l'URL mapping
        public void Bind_View(Page Page, Func<View, string, Module> New_Module = null)
        {
            if (View == null)
            {
                string Name = null;
                string Lang = null;
                List<string[]> Params = new List<string[]>();
                foreach (var Route in Page.RouteData.Values)
                {
                    var Value = Route.Value as string;
                    if (Value == null) continue;
                    switch (Route.Key)
                    {
                        case "VN":
                            Name = Value;
                            break;
                        case "VL":
                            Lang = Value;
                            break;
                        case "VP":
                            if (Value != "")
                            {
                                int R = 1;
                                foreach (string K in Value.Split('/'))
                                {
                                    Params.Add(new string[] { string.Format("VP{0}", R), HttpUtility.UrlDecode(Main.Decode_Url_Param(K)) });
                                    R++;
                                }
                            }
                            break;
                        default:
                            Params.Add(new string[] { Route.Key, HttpUtility.UrlDecode(Value) });
                            break;
                    }
                }

                foreach (string K in Page.Request.QueryString.AllKeys)
                {
                    Params.Add(new string[] { K, Main.Decode_Url_Param(Page.Request.QueryString[K]) });
                }

                if (Name == null) return;
                if (Lang != null)
                {
                    Session.Force_Language(Lang);
                }

                View = new View(Name, Params);
                if (Lang != null)
                {
                    View.Explicit_Language = true;
                }
            }
            View.Bind(this, Page, New_Module);
        }

        // Ressources de la page, accessible sous forme d'un indexer
        //Dictionary<string, object> _Depot = new Dictionary<string, object>();
        public object this[string Index]
        {
            get
            {
                object Value = null;
                Value = State[Index];
                return Value;
            }

            set
            {
                if (value == null)
                {
                    State.Remove(Index);
                    return;
                }
                State.Add(Index, value);
            }
        }

        public Session_State Session { get; set; } = null;

        public virtual void On_Clear()
        {
        }

        public void Clear()
        {
            On_Clear();
        }

        public StateBag State;

        public class Context
        {
            public virtual void Persist(View View)
            {
            }
        }

	}

}
