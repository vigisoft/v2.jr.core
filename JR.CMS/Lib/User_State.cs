using System;
using System.Collections.Generic;

namespace JR.CMS
{
	/// <summary>
	/// gestion de l'utilisateur du portail
	/// </summary>
	public class User_State
	{
        public User_State (Session_State Session)
		{
            this.Session = Session;
		}

    	public static User_State Current
		{
            get
            {
                return Session_State.Current.User;
            }
		}

        public Session_State Session { get; } = null;
        public bool Connected { get; set; } = false;
        public int Level { get; set; } = 0;

        public void Connect(string Name, string Uid, int Level = 1)
        {
            this.Name = Name;
            this.Uid = Uid;
            Connected = true;
            this.Level = Level;
            Session.Change_User();
            On_Connect();
        }

        public virtual void On_Connect()
        {
        }

        public virtual void On_Disconnect()
        {
        }

        public void Disconnect()
        {
            Name = "?";
            Uid = "";
            Connected = false;
            Level = 0;
            On_Disconnect();
            Session.Change_User();
        }
        public string Name { get; private set; } = "?";
        public string Uid { get; private set; } = "";

        public void Append_Key(string Key, char Status)
        {
            Keys.Append_Key(Key, Status);
        }

        public JR.Saf.Keys Keys { get; } = new JR.Saf.Keys();

        public void Init_Keys()
        {
            Keys.Init_Keys();
        }

        public void Remove_Key(string Key)
        {
            Keys.Remove_Key(Key);
        }

        public bool Has_Key(string Key, char Status)
        {
            return Keys.Has_Key(Key, Status);
        }
    }
}
