﻿using System;
using System.Web;
using System.Web.UI;
//using System.Web.UI.Design;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using System.ComponentModel;
using System.ComponentModel.Design;


    /// <summary>
    /// Contrôle à insérer dans une page pour établir le lien avec VWF
    /// </summary>
    [DefaultProperty("Uid")]
    //Designer(typeof(VWF.Link_Designer)),
    //ToolboxData("<{0}:Page_Link runat=server></{0}:Page_Link>")]
    public class Refresh_Mgr : System.Web.UI.WebControls.WebControl
    {
        [Browsable(false)]
        string Uid
        {
            get
            {
                return Page.Request.PhysicalPath + "." + UniqueID;
            }
        }

        // Génère un nouveau tick et le stocke dans le session state
        string Gen_Tick()
        {
            HttpSessionState S = HttpContext.Current.Session;
            string Sess_Tick = S[Uid] as string;
            if (Sess_Tick == null)
            {
                Sess_Tick = "1";

            }
            else
            {
                long S_Tick = 0;
                Int64.TryParse(Sess_Tick, out S_Tick);
                Sess_Tick = (S_Tick + 1).ToString();
            }
            S[Uid] = Sess_Tick;
            return Sess_Tick;
        }

        /// <summary>
        /// Génère ce contrôle dans le paramètre de sortie spécifié.
        /// </summary>
        /// <param name="output"> Le writer HTML vers lequel écrire </param>
        protected override void Render(HtmlTextWriter output)
        {
            if (this.Site != null && this.Site.DesignMode)
            {
                output.Write("[Refresh_Mgr : " + this.ID + "]");
            }
            else
            {
                Page.ClientScript.RegisterHiddenField("_Refresh_Mgr", Gen_Tick());
            };
        }

        bool _Tested = false;
        void Test_Refresh()
        {
            if (_Tested) return;
            _Tested = true;
            HttpSessionState S = HttpContext.Current.Session;
            HttpRequest R = HttpContext.Current.Request;
            string Sess_Tick = S[Uid] as string;
            if (Sess_Tick != null)
            {
                long Page_Tick = Convert.ToInt64(R["_Refresh_Mgr"]);
                long S_Tick = 0;
                Int64.TryParse(Sess_Tick, out S_Tick);
                _Is_Refresh = (Page_Tick < S_Tick);
            }
            else
            {
                _Is_Refresh = false;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            Test_Refresh();
            base.OnLoad(e);
        }

        bool _Is_Refresh = false;
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool Is_Refresh
        {
            get
            {
                Test_Refresh();
                return _Is_Refresh;
            }
        }

    }
