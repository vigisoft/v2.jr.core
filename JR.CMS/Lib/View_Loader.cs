using System;
using System.Collections.Generic;

namespace JR.CMS
{

    public class View_Loader
    {

        public View_Def V = null;
        public Module_Def M = null;
        List<string []> Params = new List<string []>();
        public string Path_Format = "{0}";

        public void Build()
        {
            Load();
            Flush();
            View_Def.Build_Views();
        }

        public virtual void Load()
        {
        }


        void Flush()
        {
            if (V == null) return;
            if (Params.Count > 0)
            {
                if (M == null) { V.Params = Params; } else { M.Params = Params; }
                Params = new List<string []>();
            }
            // La vue ~ est la racine qui fournit les valeurs par d�faut
            if (V.Parent == null && V.Name != "~") V.Parent = "~";
        }

        public void P(string Param, string Value)
        {
            Params.Add(new string[] { Param, Value});
        }

        public void Register_UI_Dic (string File_Name)
        {
            View_Def.Register_Dic(JR.Saf.Main.Real_File_Name(File_Name));
        }

        public View_Def Load_View(string File, Action<View, Module> Loader = null)
        {
            var File_Name = System.Web.HttpContext.Current.Server.MapPath(string.Format(Path_Format, File));
            if (!System.IO.File.Exists(File_Name)) return null;
            var Source = System.IO.File.ReadAllText(File_Name);
            var File_Path = JR.Files.Get_Directory_Name(File_Name);
            string Combined_Path (string Path)
            {
                return System.IO.Path.Combine(File_Path, Path);
            }

            // Charger les inclusions de code
            var Code_Dic = new Dictionary<string, string>();
            var C = JR.Files.Get_File_Name(File_Name).Scan();
            var Root_File = C.Next(".");
            foreach (var Include in System.IO.Directory.EnumerateFiles (File_Path, Root_File + "_*.cshtml"))
            {
                Code_Dic.Add(JR.Files.Get_File_Name(Include).ToLower().Replace(".cshtml",""), JR.Files.Safe_Read_All(Include) );
            }

            void Treat_View(string Attributes, string Content)
            {
                Flush();
                V = new View_Def();
                M = null;
                V.Source_Date = JR.Files.Safe_Date(File_Name);
                V.Source_File = File_Name;
                V.Loader = Loader;
                foreach (var Att in JR.Web.Html_Parser.Parse_Attributes(Attributes))
                {
                    string Val = Att.Value.Trim();
                    string Key = Att.Key.ToUpper();
                    switch (Key)
                    {
                        case "NAME":
                            V.Name = Val;
                            break;
                        case "ROUTE":
                            V.Routes.Add(Val);
                            break;
                        case "PARENT":
                            V.Parent = Val;
                            break;
                        case "TITLE":
                            V.Title = Val;
                            break;
                        case "VISIBILITY":
                            V.Visibility = Val;
                            break;
                        case "PAGE":
                            V.Page = Val;
                            break;
                        case "DISC":
                            V.Diconnected_Redir = Val;
                            break;
                        case "DIC":
                            V.Dic = Combined_Path(Val);
                            //P("$TITLE", "{@D:PAGE.TITLE}");
                            //P("$DESCRIPTION", "{@D:PAGE.DESC}");
                            break;
                        case "CLASS":
                            V.Class = Val;
                            break;
                        default:
                            P(Key, Val);
                            break;
                    }
                }

                void Treat_View_Inner_Text(string Text)
                {
                    var T = Text.Trim();
                    P("INNER", T);
                }

                void Treat_View_Tag(string Tag, string Tag_Attributes, string Tag_Content)
                {
                    void Push_Param (string P_Name, string P_Value, Dictionary<string, string> P_Attr)
                    {
                        if (P_Attr.Count == 0)
                        {
                            P(P_Name, P_Value);
                        }
                        else
                        {
                            foreach (var A in P_Attr)
                            {
                                if (A.Key.StartsWith("?"))
                                {
                                    P(P_Name + A.Key + ":" + A.Value, P_Value);
                                }
                                else
                                {
                                    P(P_Name + "." + A.Key, A.Value);
                                }
                            }
                        }
                    }

                    bool Inner_Detected = false;
                    void Treat_Module_Inner_Text (string Text)
                    {
                        if (Inner_Detected) return;
                        P("INNER", Text.Trim());
                        Inner_Detected = true;
                        M.Is_Razor = true;
                    }

                    Dictionary<string, string> Tag_Att = JR.Web.Html_Parser.Parse_Attributes(Tag_Attributes);
                    string Name = Tag.ToUpper();
                    string NVal = Tag_Content.Trim();
                    switch (Name)
                    {
                        case "ROUTE":
                            V.Routes.Add(NVal);
                            break;
                        case "PARAM":
                            P(Tag_Att["NAME"], NVal);
                            break;
                        case "INC":
                            V.Inc = NVal;
                            break;
                        case "SCRIPT":
                            V.Script = NVal;
                            break;
                        case "CSS":
                            V.Css = NVal;
                            break;
                        case "M":
                            {
                                Flush();
                                Inner_Detected = false;
                                V.Modules.Add(M = new Module_Def());
                                foreach (var MA in Tag_Att)
                                {
                                    string Val = MA.Value.Trim();
                                    string Key = MA.Key.ToUpper();
                                    switch (Key)
                                    {
                                        case "NAME":
                                            M.Name = Val;
                                            break;
                                        case "CLASS":
                                            M.Class = Val;
                                            break;
                                        case "ID":
                                            M.Id = Val;
                                            break;
                                        case "CONTROL":
                                            M.Control = Val;
                                            if (!M.Control.Contains("."))
                                            {
                                                M.Control = string.Format("~/Core/UI/{0}.ascx", M.Control);
                                            }
                                            break;
                                        case "TARGET":
                                            M.Target = Val;
                                            break;
                                        case "DIALOG":
                                            M.Dialog = Val;
                                            break;
                                        case "VISIBILITY":
                                            M.Visibility = Val;
                                            break;
                                        case "INNER":
                                            {
                                                string Inner_Code = "";
                                                if (Code_Dic.TryGetValue((Root_File + "_" + Val).ToLower(), out Inner_Code ))
                                                {
                                                    Treat_Module_Inner_Text(Inner_Code);
                                                }
                                            }
                                            break;
                                        case "DIC":
                                            M.Dic = Combined_Path(Val);
                                            break;
                                        case "CODE":
                                            M.Is_Razor = true;
                                            P(Key, Val);
                                            break;
                                        default:
                                            P(Key, Val);
                                            break;
                                    }
                                }

                                void Treat_Module_Tag (string Mod_Tag, string Mod_Tag_Attributes, string Mod_Tag_Content)
                                {
                                    Push_Param(Mod_Tag.ToUpper(), Mod_Tag_Content.Trim(), JR.Web.Html_Parser.Parse_Attributes(Mod_Tag_Attributes));
                                }

                                JR.Web.Html_Parser.Parse_Prefix(Tag_Content, "M:", Treat_Module_Tag, Treat_Module_Inner_Text);
                            }
                            break;
                        case "CODE":
                            V.Is_Razor = true;
                            Push_Param(Name, NVal, Tag_Att);
                            break;
                        default:
                            Push_Param(Name, NVal, Tag_Att);
                            break;
                    }

                }

                JR.Web.Html_Parser.Parse_Prefix(Content, "V:", Treat_View_Tag, Treat_View_Inner_Text);
                V.Register();

            };


            JR.Web.Html_Parser.Parse_Tags(Source, "V:V", Treat_View);
            return V;
        }

    }
}
