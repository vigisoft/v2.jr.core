﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using JR.Graph;
using System.IO;

namespace JR.CMS
{
    /// <summary>
    /// Description résumée de Jpg_Handler
    /// </summary>
    public class Sys_Handler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            HttpResponse Response = context.Response;
            HttpRequest Req = context.Request;
            try
            {
                Response.ContentType = "text";
                Response.Write(Main.Universal_String(context.Request["KEY"]));
            }
            catch { }
            Response.End();
        }

        public bool IsReusable
        {
            get { return true; }
        }
    }
}