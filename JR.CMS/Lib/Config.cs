using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using JR.DBO;
using JR.Saf;

namespace JR.CMS
{
    public class Config
    {
        public string Key = "";
        JRO_Object Set;
        public Config(string Key_Value)
        {
            if (Model.CMS == null) return;
            Key = Key_Value.ToUpper();
            Set = Model.CMS.Fo_Config.Get_Related (Key);
            if (Set != null) Set.Refresh (10.0);
        }

        public string Get_String(params object[] Keys)
        {
            if (Set == null) return "";
            StringBuilder SB = new StringBuilder();
            foreach (object O in Keys)
            {
                if (SB.Length > 0) SB.Append('.');
                SB.Append(O);
            }
            return Set.Get_Property(SB.ToString());
        }

        public void Set_String(string Value, params object[] Keys)
        {
            if (Set == null) return;
            StringBuilder SB = new StringBuilder();
            foreach (object O in Keys)
            {
                if (SB.Length > 0) SB.Append('.');
                SB.Append(O);
            }
            Set.Add_Property(SB.ToString(), Value);
            Set.Save();
        }

        public static string Get_String(string Config_Key, string Key)
        {
            var C = new Config(Config_Key);
            return C.Get_String(Key);
        }
        public static void Set_String(string Config_Key, string Key, string Value)
        {
            var C = new Config(Config_Key);
            C.Set_String(Value, Key);
        }
    }
}
