using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JR.Saf;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Globalization;
using System.Dynamic;
using JR.CMS.Core.UI;

namespace JR.CMS
{

    public interface I_View
    {
        View View { get; set; }
    }

    // Une vue est un assemblage de modules avec un jeu de param�tres
    public class View
    {
        public View(string V_Name, params string[] V_Params)
        {
            Explicit_Language = false;
            Name = JR.Strings.Official(V_Name);
            Params.Set_String(Name, "NAME");
            int I = 1;
            foreach (string Param in V_Params)
            {
                Params.Set_String(Param, "P" + I);
                I++;
            }
        }

        public View(string V_Name, ICollection<string[]> V_Params)
        {
            Explicit_Language = false;
            Name = JR.Strings.Official(V_Name);
            Params.Set_String(Name, "NAME");
            Params.Set_String(V_Params.Count.ToString(), "ARGS.COUNT");
            foreach (var Param in V_Params)
            {
                Params.Set_String(Param[1], Param[0]);
            }
        }

        public Page_State Page_State { get; set; } = null;

        bool? Load_State = null;

        public bool Is_Razor = false;   // Contient du code razor


        // Dictionnaire de chaines localis�e
        public JR.Strings.Dic Local_Dic { get; set; }

        // Indique si le dictionnaire a �t� modifi� par rapport � la pr�c�dente instance de la vue
        // Utile pour recompiler le razor
        public bool New_Local_Dic { get; set; }

        // Indique si la langue est explicitement s�lectionn�e par le parametre VL ou par l'URL ex  : /FR
        public bool Explicit_Language { get; set; }


        public Page Web_Page { get; set; }

        public View_Def Def;

        // relie les vues et les modules � leurs impl�mentations physiques
        // Tout en filtrant selon les cl�s
        public void Bind(Page_State State, Page Page, Func<View, string, Module> New_Module)
        {
            Web_Page = Page;
            if (Page is I_View)
            {
                ((I_View)Page).View = this;
            }
            if (Load_State == null)
            {
                Load_State = Load(State, New_Module, out Def);
            }
            if (Load_State == false) return;
            Page_State = State;
            Page_State.View = this;
            Page_Context = null;
            //if (Title == "") Title = Main.Parameter("APP_TITLE") + "::" + _Name +
            //    (State.Session.Navigation==""?"":("::"+ State.Session.Navigation));


            // Chargement du dico local
            // Le dico est ensuite immuable pour une m�me vue
            if (Local_Dic == null && Def.Dic != null)
            {
                bool Is_New_Dic = false;
                Local_Dic = new JR.Strings.Dic(Main.Universal_Cached_File(Universal(Def.Dic), out Is_New_Dic, Signature));
                New_Local_Dic = Is_New_Dic;
            }

            // Detection du script manager
            Script_Manager = Page.FindControl("SCRIPT_MANAGER") as ScriptManager;

            // Ajout du Async_Manager
            if (Script_Manager != null)
            {
                Async_Manager = Page.LoadControl("~/Core/UI/Async_Manager.ascx") as Async_Manager;
                Async_Manager.ID = "ASYNC_MANAGER";
                Async_Manager.V = this;
                Page.Form.Controls.AddAt (2,Async_Manager);
            }

            On_Store_Meta = null;
            foreach (Module M in Modules.Values)
            {
                // Ne pas charger les modules qui ne g�rent pas l'async par exemple
                if (M.Dont_Load) continue;

                //if (!M.Is_Allowed(State.Session.User.Keys)) continue;
                string[] Ids = M.Target.Split('.');
                if (Ids.Length == 0) continue;

                // Chargement du dico local
                // Le dico est ensuite immuable pour une m�me vue
                if (M.Local_Dic == null && M.Def.Dic != null)
                {
                    bool Is_New_Dic = false;
                    M.Local_Dic = new JR.Strings.Dic(Main.Universal_Cached_File(Universal(M.Def.Dic), out Is_New_Dic, M.Signature));
                    M.New_Local_Dic = Is_New_Dic || New_Local_Dic;
                }

                Control Parent;
                bool Top_Module = Ids.Length == 1;
                bool Init_Module = false;
                if (Top_Module)
                {
                    if (Ids[0] == "")
                    {
                        Init_Module = M.Name.Like("INIT");
                        Parent = Page.Form;
                    }
                    else
                    {
                        Parent = Page.FindControl(Ids[0]);
                    }
                }
                else
                {
                    Module P;
                    Modules.TryGetValue(Ids[0], out P);
                    if (P == null) continue;
                    if (P.Control == null) continue;
                    Parent = (Control)P.Control;
                    for (int I = 1; I < Ids.Length; I++)
                    {
                        if (Ids[I] == "*")
                        {
                            break;
                        }
                        else
                        {
                            Parent = Parent.FindControl(Ids[I]);
                        }
                    }
                }
                if (Parent == null) continue;
                if (M.Control_File == "")
                {
                    if (M.Is_Razor)
                    {
                        M.Control_File = "~/Core/UI/Razor.ascx";
                    } else
                    {
                        M.Control_File = "~/Core/UI/Html.ascx";
                    }
                }
                Control C = Page.LoadControl(M.Control_File);
                C.ID = M.Name;
                if (C is I_Module)
                {
                    M.Control = (I_Module)C;
                    M.Control.M = M;
                }

                // Encapsuler dans un div si la classe est != nodiv
                if (M.Class != "" && M.Class.ToUpper() != "NODIV")
                {
                    System.Web.UI.WebControls.Panel Div = new System.Web.UI.WebControls.Panel();
                    //if (M.Class == "") M.Class = (Top_Module ? "Module" : "Sub_Module");
                    Div.CssClass = M.Class;
                    Div.Controls.Add(C);
                    if (M.Id != "") Div.ID = M.Id;
                    C = Div;
                }

                if (Init_Module)
                {
                    Page.Controls.AddAt(0, C);
                }
                else
                // Encapsuler dans un dialogue
                if (M.Dialog_Def != "")
                {
                    Control D = Page.LoadControl(M.Dialog_Def);
                    M.Dialog = D as Module.I_Dialog;
                    M.Dialog.Bind(M);
                    Parent.Controls.Add(D);
                }
                else
                {
                    Parent.Controls.Add(C);
                }
            }

            if (!In_Async)
            {
                // Ajout de la class globale
                if (Class != "")
                {
                    Page.Form.Attributes["class"] = Class;
                }

                // Ajout des inc
                var SCRIPT_Inc = Page.FindControl("SCRIPT_INC");
                foreach (string Inc_File in Inc)
                {
                    HtmlGenericControl HI = new HtmlGenericControl("script");
                    HI.Attributes.Add("type", "text/javascript");
                    HI.Attributes.Add("src", Universal(Inc_File));
                    if (SCRIPT_Inc != null)
                    {
                        SCRIPT_Inc.Controls.AddAt(0, HI);
                    }
                    else
                    {
                        Page.Header.Controls.AddAt(0, HI);
                    }
                }

                // Ajout des css
                var CSS_Inc = Page.FindControl("CSS_INC");
                foreach (string Css_File in Css)
                {
                    var HL = new Literal { Mode = LiteralMode.PassThrough };
                    HL.Text = string.Format("<link href='{0}' rel='stylesheet'>", Main.Stamp_Url(Universal(Css_File)));
                    if (CSS_Inc != null)
                    {
                        CSS_Inc.Controls.Add(HL);
                    }
                    else
                    {
                        Page.Header.Controls.Add(HL);
                    }
                }

                // Ajout du script
                if (Scripts != "")
                {
                    HtmlGenericControl HS = new HtmlGenericControl("script");
                    HS.Attributes.Add("type", "text/javascript");
                    HS.InnerHtml = Universal(Scripts);
                    Page.Header.Controls.Add(HS);
                }
            }
        }

        public void Restore_Meta(string Key, string Value)
        {
            switch (Key)
            {
                case "$TITLE":
                    Title = Value;
                    break;
                case "$DESC":
                    Description = Value;
                    break;
            }
        }

        public Action<string, string> On_Store_Meta = null;
        public void Set_Title(string Value, params object[] Values)
        {
            if (Value == "")
            {
                Value = Page_State.Session.Local_Ui(Name + ".$TITLE", Default_Value: "?");
                if (Value == "?" && Local_Dic != null) { Value = Universal(Local_Dic["$TITLE"]); }
            }
            if (Values.Length > 0)
            {
                Title = string.Format(Value, Values);
            }
            else
            {
                Title = Value;
            }
            if (On_Store_Meta != null) On_Store_Meta("$TITLE", Title);
        }
        public void Set_Description(string Value, params object[] Values)
        {
            if (Value == "")
            {
                Value = Page_State.Session.Local_Ui(Name + ".$DESC", Default_Value: "?");
                if (Value == "?" && Local_Dic != null) { Value = Universal(Local_Dic["$DESC"]); }
            }
            if (Values.Length > 0)
            {
                Description = string.Format(Value, Values);
            }
            else
            {
                Description = Value;
            }
            if (On_Store_Meta != null) On_Store_Meta("$DESC", Title);
        }

        public ScriptManager Script_Manager { get; set; }
        public Core.UI.Async_Manager Async_Manager { get; set; }

        Generic_Dictionnary Params = new Generic_Dictionnary(false);
        public string Name { get; private set; } = "";

        public string Signature
        {
            get
            {
                return string.Format("{0}.{1}", Name, Page_State.Session.Language);
            }
        }

        public string Body_Class
        {
            get
            {
                var SB = new StringBuilder(Page_State.Session.Language);
                var P_Def = Def;
                while (Def != null && Def.Name != "~")
                {
                    SB.Insert(0, Def.Name + " ");
                    Def = Def.Parent_Def;
                }
                return SB.ToString();
            }
        }

        Dictionary<string, Module> _Modules = new Dictionary<string, Module>();
        public Dictionary<string, Module> Modules
        {
            get { return _Modules; }
        }

        public string Parameter(params string[] Keys)
        {
            string V = "";
            if (Try_Parameter(out V, null, Keys)) return V;
            return "";
        }

        public string Parameter(Module Module, params string[] Keys)
        {
            string V = "";
            if (Try_Parameter(out V, Module, Keys)) return V;
            return "";
        }

        public string Lang_Format(string Format, object Value)
        {
            if (Value == null) return "";
            if (Format == "") return JR.Convert.To_String(Value);
            return string.Format(Page_State.Session.Culture, "{0:" + Format + "}", Value);
        }

        public bool Try_Parameter(out string Value, Module Module, params string[] Keys)
        {
            Value = "";
            // Si pas de cl�
            if (Keys.Length == 0) return false;

            // Si la derni�re cl� n'est pas de la forme "?:", retourner la valeur du param�tre brut
            string Last_Key = Keys[Keys.Length - 1];
            if (Last_Key.Length < 3 || Last_Key[1] != ':')
            {
                return Params.Try_Get_String(out Value, Keys);
            }

            // Si la valeur a d�j� �t� calcul�e, la retourner
            // (Ceci fait qu'on ne peut pas retourner directement une table, sinon ca retourne sa d�finition)
            if (Params.Try_Get_String(out Value, Keys)) return true;
            Value = "";

            Strings.Cursor C = new Strings.Cursor(Last_Key);
            char Mode = C.Next_Char();

            string Source_Key = "";

            // Tron�onne la last Key
            Action Parse_Key = () =>
            {
                C.Goto(0);
                string Prefix = C.Next(".");
                Source_Key = Keys.Length == 2 ? Keys[0] + "." + Prefix : Prefix;
            };

            // Retourne la valeur du param�tre source
            Func<string> Source = () =>
            {
                string Src = "";
                if (Params.Try_Get_String(out Src, Source_Key)) return Universal(Src, false);
                return "";
            };

            switch (Mode)
            {
                // DataTable
                case 'T':
                    {
                        Parse_Key();

                        // Si pas de point on retourne le nom de l'objet dans le bag
                        if (C.End)
                        {
                            if (!Bag.ContainsKey(Source_Key)) return false;
                            Value = Source_Key;
                            return true;
                        }

                        // Chercher la DataTable dans le Bag de la vue
                        object T = Bag[Source_Key];

                        // Si pas trouv� on la charge � partir de la valeur du param�tre
                        if (T == null)
                        {
                            string Src = Source();
                            if (Src == "") return false;
                            T = Main.Universal_Table(Src);
                            Bag[Source_Key] = T;
                        }

                        DataTable DT = T as DataTable;

                        if (DT == null || DT.Rows.Count == 0) return false;

                        JR.DB.DB_Row DR = new DB.DB_Row(DT.Rows[0]);
                        string Member = C.Next(":");
                        string Format = C.Tail();
                        Value = Lang_Format(Format, DR.Value(Member));

                        // Stocker la valeur calcul�e pour acc�l�rer les appels ult�rieurs
                        Params.Set_String(Value, Keys);

                        return true;
                    }

                // Dictionnary
                case 'D':
                    {
                        // Passer le :
                        C.Skip();
                        string Key = C.Tail();
                        string Full_Key = Keys.Length == 2 ? Keys[0] + "." + Key : Key;
                        string Lang = Page_State.Session.Language;

                        // Essayer la version sp�cialis�e
                        Value = Main.Try_Get_String(Lang, "UI", string.Format("{0}.{1}", Name, Full_Key));

                        // Si vide, essayer la version globale
                        if (Value == "")
                        {
                            Value = Main.Try_Get_String(Lang, "UI", Full_Key);
                        }

                        if (Value == "") return false;

                        // Stocker la valeur calcul�e pour acc�l�rer les appels ult�rieurs
                        Params.Set_String(Value, Keys);

                        return true;
                    }

                // Local dic
                case 'L':
                    {
                        // Passer le :
                        C.Skip();
                        string Key = C.Tail();

                        // Chercher d'abord dans le local_dic du module, puis dans celui de la vue
                        if (Module != null)
                        {
                            if (Module.Local_Dic !=null) Value = Module.Local_Dic[Key];
                        }
                        if (Value == "") Value = Local_Dic[Key];

                        if (Value == "") return false;

                        // Stocker la valeur calcul�e pour acc�l�rer les appels ult�rieurs
                        Params.Set_String(Value, Keys);

                        return true;
                    }
            }
            Value = "";
            return false;

        }

        public void Set_Parameter(string Value, params string[] Keys)
        {
            if (Keys.Length == 0) return;
            if (Page_State != null)
            {
                string Last_Key = Keys[Keys.Length - 1];
                string Session_Lang = "." + Page_State.Session.Language;
                if (Last_Key.EndsWith(Session_Lang))
                {
                    List<string> K = new List<string>(Keys).GetRange(0, Keys.Length - 1);
                    K.Add(Last_Key.Remove(Last_Key.Length - Session_Lang.Length));
                    Params.Set_String(Value, K.ToArray());
                }
                else
                {
                    int I_Cond = Last_Key.IndexOf('?');
                    if (I_Cond > 0)
                    {
                        JR.Strings.Cursor C = new Strings.Cursor(Last_Key.Substring(I_Cond + 1));
                        string P_Name = C.Next(":");
                        string P_Val = C.Tail();
                        if (JR.Strings.Like(Parameter(P_Name), P_Val))
                        {
                            List<string> K = new List<string>(Keys).GetRange(0, Keys.Length - 1);
                            K.Add(Last_Key.Substring(0, I_Cond));
                            Params.Set_String(Value, K.ToArray());
                        }
                    }
                }
            }
            Params.Set_String(Value, Keys);
        }

        public string this[string Param]
        {
            get
            {
                return Parameter(Param);
            }
            set
            {
                Set_Parameter(value, Param);
            }
        }
        Bag _Bag = new Bag();
        public Bag Bag
        {
            get { return _Bag; }
        }

        public string Universal_Param(string Param, bool Translate = true)
        {
            if (Param == "") return null;
            return Universal(this[Param], Translate);
        }

        public string Universal(string Value, bool Translate = true, Module Module = null, Func<string, string> Spec_Solver = null)
        {
            Func<string, string> Solver = null;
            Solver = (Param) =>
            {
                // Un parametre est de la forme {@Param}
                if (Param == "") return null;
                //                if (Param.Length < 2) return null;
                switch (Param[0])
                {
                    // variable locale
                    case '$':   // Nouvelle forme pour compatibilit� �diteur RAZOR
                    case '@':
                        break;
                    // Fonction sp�cialis�e
                    case '(':
                        {
                            Strings.Cursor C = new Strings.Cursor(Param);
                            C.Skip();
                            string Function = C.Next(")");
                            string Rest = Solver(C.Tail());
                            if (Rest == null) return null;
                            return Main.Web_Solver(Function, Rest);
                        }
                    default:
                        if (Spec_Solver != null) return Spec_Solver(Param);
                        return null;
                }
                string P = Param.Substring(1);
                if (P == "L")
                {
                    return Page_State.Session.Language;
                }
                else if (P == "B")
                {
                    return Main.Stamp;
                }
                else if (P[P.Length - 1] == '_') // _ final indique qu'il faut ajouter la langue
                {
                    P = P + Page_State.Session.Language;
                }
                // Il n'y a jamais besoin de translater les param�tres
                //else
                //{
                //    P = Main.Universal_String(P);  // Les parametres sont toujours translat�s car generalement courts
                //}
                string V = "";
                if (Module != null)
                {
                    if (Try_Parameter(out V, Module, Module.Name, P)) return Universal(V, false, Module);
                }
                if (Try_Parameter(out V, null, P)) return Universal(V, false);
                return null;
            };
            string Result = JR.Strings.Generic_Text(Value, Solver);
            return Translate ? Universal(Main.Universal_String(Result), false) : Result;
        }

        // Traduction de chaine qui est ensuite statique pour la dur�e de vie d'un module compil� (razor)
        public string Static_Translation(string Value, Module Module, char Open_Sep, char Close_Sep)
        {
            Func<string, string> Solver = null;
            Solver = (Param) =>
            {
                // Un parametre est de la forme {@Param}
                if (Param.Length < 2) return null;
                switch (Param[0])
                {
                    // variable locale
                    case '$':
                    case '@':
                        break;
                    // Fonction sp�cialis�e
                    case '(':
                        {
                            Strings.Cursor C = new Strings.Cursor(Param);
                            C.Skip();
                            string Function = C.Next(")");
                            string Rest = Solver(C.Tail());
                            if (Rest == null) return null;
                            return Main.Web_Solver(Function, Rest);
                        }
                    default:
                        return null;
                }
                string P = Param.Substring(1);
                if (P == "L")
                {
                    return Page_State.Session.Language;
                }
                else if (P == "B")
                {
                    return Main.Stamp;
                }
                else if (P[P.Length - 1] == '_') // _ final indique qu'il faut ajouter la langue
                {
                    P = P + Page_State.Session.Language;
                }
                string V = "";
                if (Module != null)
                {
                    if (Try_Parameter(out V, Module, Module.Name, P)) return Static_Translation(V, Module, Open_Sep, Close_Sep);
                }
                if (Try_Parameter(out V, null, P)) return Static_Translation(V, null, Open_Sep, Close_Sep);
                return null;
            };
            string Result = JR.Strings.Generic_Text(Value, Solver, Open_Sep, Close_Sep);
            return Result;
        }


        public string Translated(string Source)
        {
            return Params.Translated(Source);
        }

        public string Prefix_Translated(string Source, string Prefix)
        {
            return Params.Prefix_Translated(Source, Prefix);
        }

        //JR.Saf.Door _Door;
        public string Visibility = "";
        //public bool Is_Allowed(Keys Keys)
        //{
        //    if (Visibility != "" && !JR.Convert.To_Boolean(Main.Universal_String(Visibility))) return false;
        //    return _Door.Is_Allowed(Keys);
        //}

        public string Page = "";    // Fichier aspx
        public List<string> Css = new List<string>();
        public List<string> Inc = new List<string>();
        public string Scripts = "";
        public string Class = "";
        public string Disconnect_Redir = "";
        public string Title = "";
        public string Description = "";

        bool Load(Page_State State, Func<View, string, Module> New_Module, out View_Def V)
        {

            V = View_Def.View_By_Name(Name);
            if (V == null) return false;

            // Tracer l'appel de page
            if (Session_Log.Log_Sessions)
            {
                State.Session.Session_Log.Log_Page();
            }

            Page_State = State;
            foreach (var P in V.Params)
            {
                Set_Parameter(P[1], P[0]);
            }
            Disconnect_Redir = Translated(V.Diconnected_Redir);
            if (!State.Session.User.Connected && Disconnect_Redir != "")
            {
                Web_Page.Response.Redirect(Disconnect_Redir, true);
                return false;
            }

            Page = Translated(V.Page);
            foreach (string S in Translated(V.Css).Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
            {
                //Css.Insert(0, S); // On empile � l'envers car ensuite on restitue � l'envers
                Css.Add(S); // On empile � l'envers car ensuite on restitue � l'envers
            }
            foreach (string S in Translated(V.Inc).Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
            {
                Inc.Insert(0, S); // On empile � l'envers car ensuite on restitue � l'envers
            }
            Scripts = Translated(V.Script);
            Class = Translated(V.Class);
            Visibility = Translated(V.Visibility);
            //_Door = new JR.Saf.Door(Translated(V.Access_Keys));
            Title = Translated(V.Title);
            Is_Razor = V.Is_Razor;

            if (V.Loader != null) V.Loader(this, null);

            Module_Def MD;
            string Module_Name;
            for (int I = 0; I < V.Modules.Count; I++)
            {
                MD = V.Modules[I];
                Module_Name = MD.Name;
                if (Module_Name == "") Module_Name = string.Format("MOD_{0}", I);
                Module_Def Overload = null;

                // Substitution �ventuelle du module par un module commun par un nom VIEW@MODULE
                string[] Path = MD.Name.Split('/');
                if (Path.Length == 3)
                {
                    View_Def Redirection = View_Def.View_By_Name(Path[1]);
                    if (Redirection != null)
                    {
                        foreach (Module_Def MRD in Redirection.Modules)
                        {
                            if (MRD.Name == Path[2])
                            {
                                Overload = MD;
                                MD = MRD;
                                Module_Name = Path[0];
                                break;
                            }
                        }
                    }
                }

                Module M;
                bool Surcharge = false;
                Surcharge = Modules.TryGetValue(Module_Name, out M);
                if (!Surcharge)
                {
                    M = New_Module == null ? new Module(this, Module_Name) : New_Module(this, Module_Name);
                    Modules.Add(Module_Name, M);
                    M.Def = MD;
                }

                void Apply_Params(Module_Def Mod)
                {
                    if (Mod.Is_Razor) M.Is_Razor = true;
                    if (Mod.Params != null)
                    {
                        foreach (var P in Mod.Params)
                        {
                            Set_Parameter(Prefix_Translated(P[1], Module_Name), M.Name, P[0]);
                        }
                    }
                    if (!Surcharge)
                    {
                        if (Mod.Control != "") M.Control_File = Prefix_Translated(Mod.Control, Module_Name);
                        //if (Mod.Access_Keys != "") M.Set_Security(Prefix_Translated(Mod.Access_Keys, Module_Name));
                        if (Mod.Dialog != "") M.Dialog_Def = Prefix_Translated(Mod.Dialog, Module_Name);
                        if (Mod.Target != "") M.Target = Prefix_Translated(Mod.Target, Module_Name);
                        if (Mod.Visibility != "") M.Visibility = Translated(Prefix_Translated(Mod.Visibility, Module_Name));
                        if (Mod.Class != "") M.Class = Prefix_Translated(Mod.Class, Module_Name);
                        if (Mod.Id != "") M.Id = Prefix_Translated(Mod.Id, Module_Name);
                    }
                }
                Apply_Params(MD);
                if (Overload != null) Apply_Params(Overload);

                if (V.Loader != null) V.Loader(this, M);

            }
            return true;
        }

        public Module Module_By_Key (string Key)
        {
            Module M;
            Modules.TryGetValue(JR.Strings.Official(Key), out M);
            return M;
        }
        // Recherche un module particulier
        public T Find_Module<T>(string Key) where T : Control, I_Module
        {
            var M = Module_By_Key(Key);
            if (M == null) return null;
            if (M.Control == null) return null;
            if (!(M.Control is T)) return null;
            return (T)M.Control;
        }

        // Recherche un module particulier mais avec moins de contrainte
        public Control Find_Control(string Key)
        {
            var M = Module_By_Key(Key);
            if (M == null) return null;
            if (M.Control == null) return null;
            if (!(M.Control is Control)) return null;
            return (Control)M.Control;
        }

        // Recherche tous les modules particuliers
        public List<T> Find_Modules<T>() where T : Control, I_Module
        {
            List<T> L = new List<T>();
            foreach (Module M in Modules.Values)
            {
                if ((M.Control != null) && (M.Control is T)) L.Add((T)M.Control);
            }
            return L;
        }

        public class View_Event
        {
            public string Name;
            public object[] Params;
        }

        public View_Event Event = null;

        public delegate void Event_Listening(string Event_Name, params object[] Params);

        public Event_Listening On_Event = null;

        public void Raise_Module_Event(Module Source, string Event_Name, params object[] Params)
        {
            var C = Event_Name.Scan();
            var Ev_Name = C.Next("@");
            var Ev_Module = C.Tail();
            void Raise (Module M)
            {
                I_Event_Module EM = M.Control as I_Event_Module;
                if (EM == null) return;
                EM.Event_Receive(Ev_Name, Params);
            }
            if (Ev_Module != "")
            {
                var M = Module_By_Key(Ev_Module);
                if (M != null) Raise(M);
                return;
            }
            else
            {
                foreach (Module M in Modules.Values)
                {
                    if (M == Source) continue;
                    Raise(M);
                }
            }
            if (On_Event == null) return;
            On_Event(Event_Name, Params);
        }

        public void Raise_Event(string Event_Name, params object[] Params)
        {
            Event = new View_Event { Name = Event_Name, Params = Params };
            Raise_Module_Event(null, Event_Name, Params);
        }

        // Ajout d'�v�nements � la pile des �v�nements � propager lors du complete_load
        List<View_Event> Async_Events = new List<View_Event>();
        public void Raise_Async_Event(string Event_Name, params object[] Params)
        {
            Async_Events.Add(new View_Event { Name = Event_Name, Params = Params });
        }

        public bool Show_Modal(string Module, params object[] Context)
        {
            foreach (Module M in Modules.Values)
            {
                if (M.Name == Module && M.Dialog != null)
                {
                    M.Dialog.Show(M, Context);
                    return true;
                }
            }
            return false;
        }

        public void Hide(params string[] Module_Names)
        {
            foreach (string S in Module_Names)
            {
                Module M = Modules[S];
                if (M != null) M.Hide();
            }
        }

        public bool In_Async
        {
            get { return Script_Manager != null && Script_Manager.IsInAsyncPostBack; }
        }

        StringBuilder Async_JS;
        public void Execute_Async_JS(string Code)
        {
            if (Script_Manager == null) return;
            if (Async_JS == null) Async_JS = new StringBuilder();
            Async_JS.Append(Code);
            if (!Code.EndsWith(";")) Async_JS.Append(";");
        }

        // A Appeler sur le LoadCompleted de la page pour dispatcher les �v�nements asynchrones
        public void Complete_Load()
        {
            foreach (var E in Async_Events)
            {
                try
                {
                    Event = E;
                    Raise_Module_Event(null, E.Name, E.Params);
                }
                catch { }
            }
            Async_Events.Clear();
        }

        // A Appeler sur le PreRenderCompleted de la page pour forcer l'execution des scripts asynchrones et 
        //  la g�n�ration des balises
        public void Complete_PreRender()
        {
            if (!In_Async)
            {
                if (Local_Dic != null)
                {
                    if (Title == "") Title = Universal(Local_Dic["$TITLE"]);
                    if (Description == "") Description = Universal(Local_Dic["$DESC"]);
                }
                if (Title == "") Title = Page_State.Session.Local_Ui(Name + ".$TITLE");
                if (Description == "") Description = Page_State.Session.Local_Ui(Name + ".$DESC");
                if (Title == "") Title = Name;
            }
            Web_Page.Title = Title;
            if (Description != "") Web_Page.MetaDescription = Description;

            if (Async_JS != null)
            {
                if (In_Async)
                {
                    var Panel = Async_Manager.Async_Panel;
                    Panel.Update();
                    Script_Manager.RegisterDataItem(Panel, Async_JS.ToString());
                    Panel.Update();
                }
                else
                {
                    Web_Page.ClientScript.RegisterStartupScript(this.GetType(),"VIEW_SCRIPT", Async_JS.ToString(), true);
                }
                Async_JS = null;
            }

            if (Page_Context != null)
            {
                Page_Context.Persist(this);
            }
        }


        // Gestion d'un contexte d'appel
        public Page_State.Context Page_Context = null;

    }
}
