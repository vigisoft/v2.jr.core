using System.Collections.Generic;

namespace JR.CMS
{
	/// <summary>
	/// Classe de gestion du loggin de visite de session
	/// </summary>

	public class Content_Cache
	{

        public class Entry
        {
            public Dictionary<string, string> Datas;
            public string Content;
            public string Dependency;
            public void Push(string Key, string Value)
            {
                if (Datas == null) Datas = new Dictionary<string, string>();
                Datas[Key] = Value;
            }
        }

        public Dictionary<string, Entry> Entries = new Dictionary<string, Entry>();

        public Entry Get_Entry (string Key, string Dependency)
        {
            lock (Entries)
            {
                Entry E;
                if (Entries.TryGetValue(Key, out E)) return E;
                E = new Entry { Dependency = Dependency };
                Entries.Add(Key, E);
                return E;
            }
        }

        public void Purge (string Dependencies = "")
        {
            var Removals = new List<string>();
            lock (Entries)
            {
                if (Dependencies == ""|| Dependencies == null)
                {
                    Entries.Clear();
                    return;
                }
                foreach (var E in Entries)
                {
                    if (E.Value.Dependency.Match(Dependencies)) Removals.Add(E.Key);
                }
                foreach (var E in Removals)
                {
                    Entries.Remove(E);
                }
            }
        }

	}
}
