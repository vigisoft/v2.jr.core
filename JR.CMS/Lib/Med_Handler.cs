﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using JR.Graph;
using System.IO;

namespace JR.CMS
{
    /// <summary>
    /// Description résumée de Jpg_Handler
    /// </summary>
    public class Med_Handler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            HttpResponse Response = context.Response;
            HttpRequest Req = context.Request;
            int Height = JR.Convert.To_Integer(context.Request["H"]);
            int Width = JR.Convert.To_Integer(context.Request["W"]);
            string File_Name = context.Server.MapPath("~/" + context.Request["P"]);
            System.IO.FileInfo FI = new System.IO.FileInfo(File_Name);
            if (!FI.Exists)
            {
                Response.StatusCode = 404;
                Response.StatusDescription = string.Format ("Bad media [{0}]", File_Name);
                Response.End ();
                return;
            }
            if (Height+Width > 0)
            {
                var P = File_Name.Paths();
                string Size_Name = string.Format ("{0}/cache/{1}-{2}x{3}{4}", P.Directory, P.File_Prefix, Width, Height, P.Extension);
                if (File.Exists(Size_Name))
                {
                    File_Name = Size_Name;
                }
                else if (JR.Graph.Images.Resize_Image(File_Name, Size_Name, Width, Height))
                {
                    File_Name = Size_Name;
                }
                // sinon on renvoie l'original
            }
            Response.ContentType = "image";
            Response.TransmitFile(File_Name);
            Response.End();
        }

        public bool IsReusable
        {
            get { return true; }
        }
    }
}