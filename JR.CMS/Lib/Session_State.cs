using System;
using System.Collections.Generic;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web;
using System.Data;
using System.Globalization;
using System.Net.Mail;

namespace JR.CMS
{
	/// <summary>
	/// Classe de gestion de la page principale (portail)
	/// </summary>

	public class Session_State
	{
		public Session_State ()
		{
            Detect_Language();
            HttpContext H = HttpContext.Current;
            H.Session["JR.CMS.SESSION"] = this;
            Session_Log = new Session_Log ();
            Ip = H.Request.UserHostAddress;
        }

        public Content_Cache S_Cache = new Content_Cache();
        public string Ip = "";
        public Session_Log Session_Log { get; set; }
        void Detect_Language()
        {
            Explicit_Language = false;
            Language = Default_Language;
            string Host = HttpContext.Current.Request.Url.Host.ToUpper();
            foreach (string L in Main.Language_List)
            {
                string Domain = Main.Parameter("DOMAIN." + L).ToUpper();
                if (Domain == null) continue;
                if (Host == Domain)
                {
                    Explicit_Language = true;
                    Language = L;
                    break;
                }
            }
            Browser_Language = Get_Browser_Language;
        }

        public static string Language_By_Request (HttpRequest Request)
        {
            string Language = Main.Language_List[0];
            string Host = Request.Url.Host.ToUpper();
            foreach (string L in Main.Language_List)
            {
                string Domain = Main.Parameter("DOMAIN." + L).ToUpper();
                if (Domain == null) continue;
                if (Host == Domain)
                {
                    Language = L;
                    break;
                }
            }
            return Language;
        }

        // Adresse IP de d�tenteur de session
        string _Host_Address = "";
        public string Host_Address
		{
			get
            {
                if (_Host_Address == "") _Host_Address = HttpContext.Current.Request.UserHostAddress;
                return _Host_Address;
            }
		}

        public static string Current_Ip
        {
            get
            {
                return HttpContext.Current.Request.UserHostAddress;
            }
        }


        public virtual void On_Clear()
		{
		}

		// Nettoie les ressources du portail avant fermeture
		public void Clear ()
		{
			On_Clear ();
		}

		public static Session_State Current
		{
            get
            {
                var HS = HttpContext.Current.Session;
                return (Session_State)HS["JR.CMS.SESSION"];
            }
		}

        // M�thode � sp�cialiser pour retourner un utilisateur
        public virtual User_State On_New_User(Session_State Session)
        {
            return new User_State(Session);
        }

        User_State _User = null;	// Utilisateur courant
        public User_State User
		{
			get
            {
                if (_User == null)
                {
                    _User = On_New_User(this);
                }
                return _User;
            }
		}

        public virtual void After_Change_User ()
        { }

        public void Change_User ()
        {
            S_Cache.Purge();
            After_Change_User();
        }

        string _Domain = "";   // Domaine de la demande initiale
        public string Domain
        {
            get
            {
                if (_Domain == "")
                {
                    _Domain = HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
                }
                return _Domain;
            }
        }

        public string Http_Host
        {
            get
            {
                return HttpContext.Current.Request.ServerVariables["HTTP_HOST"];
            }
        }

		// Ressources de la session, accessible sous forme d'un indexer
		Dictionary<string, object> _Depot = new Dictionary<string,object>();
        public object this[string Index]
        {
            get
            {
                object Value = null;
                _Depot.TryGetValue(Index, out Value);
                return Value;
            }

            set
            {
                if (value == null)
                {
                    _Depot.Remove(Index);
                    return;
                }
                _Depot [Index] = value;
            }
        }

        public int User_Level { get { return User == null ? 0 : User.Level; } }
        public Content_Cache.Entry Get_Content_Cache (bool Global, string Key, string Dependency)
        {
            string Prefix = Language + User_Level;
            return Global ? Application_State.Current.A_Cache.Get_Entry(Prefix + Key, Dependency) : S_Cache.Get_Entry(Prefix + Key, Dependency);
        }

        // Gen�rateur d'UID
        int _Uid_Gen = 1;

        // Gestion des pages
        Dictionary<string, Page_State> _Pages = new Dictionary<string,Page_State>();   // Liste des pages ouvertes

        // 
        protected T Get_Page<T> (System.Web.UI.StateBag State, string Key) where T:Page_State, new()
        {
            T Page = null;

            // Retrouver la r�f�rence de page
            object Page_Ref = State["_Page_Uid"];
            if (Page_Ref == null)
            {
                // Effacement de l'ancienne page portant la m�me cl�
                Page_State P = null;
                _Pages.TryGetValue(Key, out P);
                if (P != null) P.Clear();
            }
            else
            {
                Page_State P = null;
                _Pages.TryGetValue(Key, out P);
                if (P is T && ((string)Page_Ref == P.Uid))
                {
                    Page = (T)P;
                }
                else
                {
                    if (P != null) P.Clear();
                }
            }

            if (Page == null)
            {
                Page = new T();
                _Pages[Key] = Page;
                _Uid_Gen++;
                Page.Uid = _Uid_Gen.ToString();
                Page.Session = this;
            }
            else
            {
                int Old_Tick = (int)State["_Page_Tick"];
                Page.Compare_Tick(Old_Tick);
            }

            // Stocker la r�f�rence de la page et le tick
            State["_Page_Uid"] = Page.Uid;
            State["_Page_Tick"] = Page.New_Tick;
            Page.State = State;
            return Page;
        }

        // Retrouve ou cr�� un nouveau Page_State
        public static T Load_Page <T>(System.Web.UI.StateBag State, string Key) where T:Page_State,new()
        {
            // Identifier la session
            return Session_State.Current.Get_Page<T>(State, Key);
        }

        public string Language {get;set;}
        public string Browser_Language { get; set; }
        public bool Explicit_Language { get; set; }

        public bool French
        {
            get { return Language == "FR"; }
        }

        public virtual void Force_Language(string Langue)
        {
            Language = Langue;
        }


        string Get_Browser_Language
        {
            get
            {
                var UL = HttpContext.Current.Request.UserLanguages;
                if (UL != null)
                {
                    foreach (string L in UL)
                    {
                        foreach (string LL in Main.Language_List)
                        {
                            if (L.ToUpper().Contains(LL)) return LL;
                        }
                    }
                }
                return Language == null ? "FR":Language;
            }
        }

        public string Default_Language
        {
            get
            {
              return Main.Language_List[0];
            }
        }

        public int Language_Row
        {
            get
            {
                int R = 0;
                foreach (var Lang in Application_State.Current.Language_List)
                {
                    if (Lang == Language) return R;
                    R++;
                }
                return 0;
            }
        }


        public CultureInfo Culture
        {
            get
            {
                switch (Language)
                {
                    case "FR":
                        return CultureInfo.GetCultureInfo("fr-FR");
                    case "SP":
                        return CultureInfo.GetCultureInfo("es-ES");
                    case "CN":
                        return CultureInfo.GetCultureInfo("zh-CN");
                    case "DE":
                        return CultureInfo.GetCultureInfo("de-DE");
                    case "RU":
                        return CultureInfo.GetCultureInfo("ru-RU");
                    case "AR":
                        return CultureInfo.GetCultureInfo("ar-EG");
                }
                return CultureInfo.GetCultureInfo("en-US");
            }
        }

        // Retourne une chaine UI localis�e
        public string Local_Ui(string Key, bool Neutral = false, string Default_Value = null)
        {
            if (Default_Value == null)
            {
                return Neutral ? Main.Get_String("UI", Key) : Main.Get_String(Language, "UI", Key);
            }
            string Val;
            if (Neutral)
            {
                if (!Main.Try_Get_String(out Val, "UI", Key)) return Default_Value;
            }
            else
            {
                if (!Main.Try_Get_String(out Val, Language, "UI", Key)) return Default_Value;

            }
            return Val;
        }

        // Retourne une chaine UI localis�e et compatible Javascript
        public string Local_Js_Ui(string Key)
        {
            return Main.Js(Main.Get_String(Language, "UI", Key));
        }

        // Indique � la prochaine vue si elle s'appelle FROM
        // d'aller sur To
        //string _Route = "";
        //public string Route
        //{
        //    get
        //    {
        //        return _Route;
        //    }
        //    set { _Route = value; }
        //}

        //public void Clear_Route()
        //{
        //    _Route = "";
        //}

        // Dernier fichier � t�l�charger
        public class Download_Info
        {
            public string Physical_File_Name {get;set;}
            public string Client_File_Name { get; set; }
            public string Mime_Type { get; set; }
            public bool Auto_Delete { get; set; }
            public string Encode
            {
                get
                {
                    return HttpUtility.UrlEncode (string.Format("{0}~{1}~{2}~{3}", Physical_File_Name, Client_File_Name, Mime_Type, Auto_Delete));
                }
            }
            public static Download_Info Decode(string Param)
            {
                string [] Chars = JR.Strings.Split (HttpUtility.UrlDecode (Param));
                if (Chars.Length < 4) return null;
                Download_Info Dl = new Download_Info();
                Dl.Physical_File_Name = Chars[0];
                Dl.Client_File_Name = Chars[1];
                Dl.Mime_Type = Chars[2];
                Dl.Auto_Delete = JR.Convert.To_Boolean (Chars[3]);
                return Dl;
            }
        }

        Download_Info _File_Download = null;

        public Download_Info Get_Requested_Download()
        {
            Download_Info Info = _File_Download;
            //_File_Download = null;
            return Info;
        }

        public static Download_Info Get_Requested_Download(HttpRequest Request)
        {
            string File = Request.Params["File"];
            if (File == null) return null;
            return Download_Info.Decode(File);
        }

        public virtual void Request_Download
            (Page Page, string Physical_Name, string Client_File_Name,
             string Mime_Type, bool Auto_Delete)
        {
            Download_Info D = new Download_Info();
            D.Physical_File_Name = Physical_Name;
            D.Client_File_Name = Client_File_Name;
            D.Mime_Type = Mime_Type;
            D.Auto_Delete = Auto_Delete;
            _File_Download = D;
            //string Download_URL = Page.ResolveClientUrl("~/Controls/Download_Frame.aspx");
            string Download_URL = Page.ResolveClientUrl("~/Handlers/Dld.ashx") + "?FILE=" + D.Encode;
            //HttpContext.Current.Response.Redirect(Download_URL, true);

            //LiteralControl D_Frame = new LiteralControl("<iframe height=\"2px\" width = \"2px\" src=\"" + Download_URL + "\"></iframe>");
            //Control.Controls.Add(D_Frame);

            HtmlGenericControl D_Frame = Page.FindControl("DOWNLOAD_FRAME") as HtmlGenericControl;
            if (D_Frame == null) return;
            D_Frame.Attributes["src"] = Download_URL;
            UpdatePanel UP = Page.FindControl ("DOWNLOAD_PANEL") as UpdatePanel;
            if (UP != null) UP.Update();

        }

        // Affiche une page dans une frame cach�e d�di�e si elle existe
        public void Call_Hidden_Page
            (Page Page, string URL)
        {
            HtmlGenericControl H_Frame = Page.FindControl("HIDDEN_FRAME") as HtmlGenericControl;
            if (H_Frame == null) return;
            H_Frame.Attributes["src"] = URL;
        }

        // Transforme une table en une liste tri�e en ne conservant que le �l�ments de la langue courante
        public List<string []>  Get_Local_Table (DataTable DT, int First_Language_Col)
        {
            int Lang_Count = Main.Language_List.Count;
            int Col_Count = First_Language_Col +1 +(DT.Columns.Count-1-First_Language_Col)/Lang_Count;
            int Lang_Index = Lang_Count -1;
            while (Lang_Index>0)
            {
                if (Main.Language_List[Lang_Index] == Language) break;
                Lang_Index--;
            }
            
            List<string []> L = new List<string[]>();
            foreach (DataRow DR in DT.Rows)
            {
                string [] Line = new string [Col_Count];
                for (int Col = 0; Col < Math.Min (First_Language_Col,DT.Columns.Count); Col++)
                {
                    Line[Col] = DR[Col].ToString();
                }
                int L_Col = First_Language_Col;
                for (int Col = First_Language_Col; Col < DT.Columns.Count; Col+=Lang_Count)
                {
                    string Val = "";
                    if (Col + Lang_Index < DT.Columns.Count) Val = DR[Col + Lang_Index].ToString();
                    if (Val == "")
                    {
                        Val = DR[Col].ToString();
                    }
                    Line[L_Col] = Val;
                    L_Col++;
                }
                L.Add (Line);
            }
            return L;
        }

        string Get_PUI_Key(string Name, bool User_Level)
        {
            if (User_Level)
            {
                return string.Format("PUI.{0}.{1}", User.Uid, Name);
            }
            else
            {
                return string.Format("PUI.{0}", Name);
            }
        }

        public void Remove_Cookie (string Name, bool User_Level = false)
        {
            string Key = Get_PUI_Key(Name, User_Level);
            HttpContext.Current.Response.Cookies.Remove(Key);

            // La seule mani�re d'effacer un cookie est de mettre son expiration dans le pass�
            HttpContext.Current.Response.Cookies.Set(new HttpCookie(Key, null) { Expires = DateTime.Now.AddYears(-10) });

        }

        public void Persist_Cookie(string Name, string Value, bool User_Level = false, int Days=30000)
        {
            string Key = Get_PUI_Key (Name, User_Level);
            HttpContext.Current.Response.Cookies.Set (new HttpCookie(Key, Value) { Expires=DateTime.Now.AddDays(Days) });
        }

        public string Get_Cookie(string Name, bool User_Level = false)
        {
            string Key = Get_PUI_Key(Name, User_Level);
            HttpCookie C = HttpContext.Current.Request.Cookies.Get (Key);
            if (C == null) return "";
            return C.Value;
        }

        public static bool Send_Mail(string To, string From, string Sender, string Subject, string Body, params string[] Attachments)
        {
            try
            {
                using (MailMessage Mail = new System.Net.Mail.MailMessage())
                {
                    Mail.Subject = Subject;
                    Mail.Body = Body;
                    if (Body.StartsWith("<") && Body.EndsWith(">"))
                    {
                        Mail.IsBodyHtml = true;
                        Mail.BodyEncoding = System.Text.Encoding.Default;
                    }
                    else
                    {
                        Mail.IsBodyHtml = false;
                        Mail.BodyEncoding = System.Text.Encoding.Default;
                    }
                    foreach (var T in To.Split(','))
                    {
                        if (T.StartsWith("bcc:"))
                        {
                            Mail.Bcc.Add(T.Substring(4));
                        }
                        else if (T.StartsWith("cc:"))
                        {
                            Mail.CC.Add(T.Substring(3));
                        }
                        else
                        {
                            Mail.To.Add(T);
                        }
                    }
                    Mail.From = new MailAddress(From);
                    if (Sender != null)
                    {
                        Mail.Sender = new MailAddress(Sender);
                    }
                    else
                    {
                        Mail.Sender = Mail.From;
                    }
                    foreach (string File in Attachments)
                    {
                        Mail.Attachments.Add(new Attachment(File));
                    }
                    return Application_State.Send(Mail);
                }
            }
            catch
            {
                return false;
            }
        }


	}
}
