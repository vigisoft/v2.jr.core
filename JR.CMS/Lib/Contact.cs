﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JR.DBO;
using JR.Saf;
using System.IO;

namespace JR.CMS
{
    public class Contact
    {
        public static void Register_Request(string Email, string Name, string Coord, string Ref, string Subject, string Message)
        {
            if (Model.CMS == null || Model.CMS.Cl_Request == null) return;
            JRO_Row R = Model.CMS.Cl_Request.Create_Row(Name);
            R.Set<DateTime>("Date_Request", DateTime.Now);
            R.Set<int>("Priority", 0);
            R.Set<string>("State", "N"); 
            R.Set<string> ("Message", Message); 
            R.Set<string> ("Subject", Subject);
            R.Set<string>("Ref_Contact", Ref);
            R.Set<string>("Email", Email); 
            R.Set<string> ("Coord", Coord);
            R.Save();
        }
    }
}