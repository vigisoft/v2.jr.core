using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web.Routing;

namespace JR.CMS
{
    public class Module_Def
    {
        public string Name = "";  // Name
        public string Class = "";       // Css class name
        public string Id = "";       // Id
        public string Dic = null;     // Fichier dictionnaire
        public string Visibility = ""; // Condition de visibilit�
        public string Target = "";  // Zone d'atterissage dans la vue
        public string Control = ""; // Controle � charger
        //public string Access_Keys = "";  // Liste de cl�s qui autorisent l'acc�s
        public List<string []> Params = new List<string []>();    // Liste de param�tres
        public string Dialog = "";       // Control dialog li�
        public bool Is_Razor = false;    // Contains razor
    }

    public class View_Def
    {
        public View_Def()
        {
        }
        public string Name = "";         // Nom de la vue
        public string Parent;       // Vue parente (h�ritage)
        public View_Def Parent_Def;
        public string Title = "";   // Titre de la page
        public string Visibility = ""; // Condition de visibilit�
        //public string Access_Keys = "";  // Liste de cl�s qui autorisent l'acc�s
        public string Page = "";    // Fichier aspx
        public string Css = "";    // Fichier css complementaires
        public string Script = "";    // Scripts complementaires
        public string Inc = "";    // Fichier script inc complementaires
        public string Class = "";       // Css class name
        public string Dic = null;     // Fichier dictionnaire
        public string Diconnected_Redir = "";  // URL � rediriger si pas connecte
        public List<string []> Params = new List<string []>();    // Liste de param�tres
        public List<Module_Def> Modules = new List<Module_Def>();
        public Action<View, Module> Loader = null;  // Se d�clenche avant render d'une vue et d'un module pour compl�ter ou renseigner les donn�es
        public string Source_File = "";
        public DateTime Source_Date = DateTime.MinValue;
        public List<string> Routes = new List<string>();
        public bool Is_Razor = false;   // Contains razor

        static Dictionary<string, View_Def> Views = new Dictionary<string, View_Def>();
        public static string Build_Time = DateTime.Now.ToString("yyyyMMddHHmmss");

        public bool Sources_Modified
        {
            get
            {
                if (Source_Modified) return true;
                if (Parent_Def == null) return false;
                return Parent_Def.Sources_Modified;
            }
        }

        public bool Source_Modified
        {
            get
            {
                return JR.Files.Safe_Date(Source_File) != Source_Date;
            }
        }

        void Add_Routes()
        {

            // La route par defaut est le nom de la vue
            if (Routes.Count == 0 && !Name.StartsWith("~"))
            {
                Routes.Add(Name.ToLower());
            }
            int I_Route = 0;
            void Append_Route (string Route)
            {
                I_Route++;
                var Full_Route = Route == "" ? "" : (Route + "/{*VP}");
                var Route_Name = Name + I_Route;
                if (Application_State.Force_Lang_URL)
                {
                    RouteTable.Routes.MapPageRoute(Route_Name, Full_Route, "~/Core/Lang_Redir.aspx", false, new RouteValueDictionary { { "VN", Name } });
                }
                else
                {
                    RouteTable.Routes.MapPageRoute(Route_Name, Full_Route, Page, false, new RouteValueDictionary { { "VN", Name } });
                }
                if (Full_Route != "") Full_Route = "/" + Full_Route;
                foreach (var Lang in Application_State.Current.Languages)
                {
                    string Lang_Key = Lang.Key.ToUpper();
                    RouteTable.Routes.MapPageRoute(Route_Name + "." + Lang_Key, Lang_Key + Full_Route, Page, false, new RouteValueDictionary { { "VN", Name }, { "VL", Lang_Key } });
                }
            }

            var Langs = Application_State.Current.Language_List;
            foreach (var Route in Routes)
            {
                var L_Route = Route.Split_List(',');
                if (L_Route.Count > 1)
                {
                    for (int R=0; R < L_Route.Count;R++)
                    {
                        var Lang_Route = L_Route[R];
                        for (int L=0; L < Langs.Length; L++)
                        {
                            if (L == R) continue;
                            Application_State.Declare_Route_Translation(Langs[L], Lang_Route, L_Route[Math.Min(L, L_Route.Count-1)]);
                        }
                        Append_Route(Lang_Route);
                    }
                }
                else
                {
                    Append_Route(Route);
                }
            }
        }

        // Reconstruit l'arborescence des vues
        public static void Build_Views()
        {

            foreach (View_Def V in Views.Values)
            {
                Compute_Child_View(V);
                V.Add_Routes();
            }
        }


        // complete les vues filles
        static void Compute_Child_View(View_Def View)
        {
            if ((View.Parent == null) || (View.Parent == "")) return;
            View_Def Parent;
            Views.TryGetValue(View.Parent, out Parent);
            View.Parent = "";
            if (Parent == null) return;
            if (Parent.Parent != "") Compute_Child_View(Parent);
            View.Parent_Def = Parent;

            // Les parametres du parent sont appliqu�s sauf si ils existent deja dans l'enfant
            if (View.Visibility == "") View.Visibility = Parent.Visibility;
            if (View.Title == "") View.Title = Parent.Title;
            //if (View.Access_Keys == "") View.Access_Keys = Parent.Access_Keys;
            if (View.Page == "") View.Page = Parent.Page;
            if (Parent.Css != "") { View.Css = Parent.Css + (View.Css == "" ? "" : ";" + View.Css); }
            if (Parent.Script != "") { View.Script = Parent.Script + View.Script; }
            if (Parent.Inc != "") { View.Inc = Parent.Inc + (View.Inc == "" ? "" : ";" + View.Inc); }
            if (Parent.Class != "") { View.Class = Parent.Class + (View.Class == "" ? "" : " " + View.Class); }
            if (View.Diconnected_Redir == "") View.Diconnected_Redir = Parent.Diconnected_Redir;
            if (Parent.Is_Razor) View.Is_Razor = true;

            // Insertion des parametres parent en premier
            if (Parent.Params.Count > 0)
            {
                View.Params.InsertRange(0, Parent.Params);
            }

            // Insertion des modules parents en premier
            // Remplacement des modules parents qui sont en double par leur enfant
            List<Module_Def> L = new List<Module_Def>();
            foreach (Module_Def M in Parent.Modules)
            {
                if (M.Name != "")
                {
                    Module_Def Doublon = View.Get_Module(M.Name);
                    if (Doublon != null)
                    {
                        View.Modules.Remove(Doublon);
                        L.Add(Doublon);
                        continue;
                    }
                }
                L.Add(M);
            }
            View.Modules.InsertRange(0, L);

        }

        private Module_Def Get_Module(string Name)
        {
            foreach (Module_Def M in Modules)
            {
                if (M.Name == Name) return M;
            }
            return null;
        }

        public static View_Def View_By_Name(string Name)
        {
            string N = Name.Official();
            View_Def V = null;
            if (!Views.TryGetValue(N, out V)) return null;
            return V;
        }

        // Melange de deux modules
        static void Merge(Module_Def Dest, Module_Def Source)
        {
            if (Source.Visibility != "") Dest.Visibility = Source.Visibility;
            if (Source.Target != "") Dest.Target = Source.Target;
            if (Source.Control != "") Dest.Control = Source.Control;
            if (Source.Dialog != "") Dest.Dialog = Source.Dialog;
            //if (Source.Access_Keys != "") Dest.Access_Keys = Source.Access_Keys;
            if ((Source.Params != null) && (Source.Params.Count > 0))
            {
                Dest.Params.AddRange(Source.Params);
            }
            if (Source.Is_Razor) Dest.Is_Razor = true;
        }

        // Melange de deux vues
        static void Merge(View_Def Dest, View_Def Source)
        {
            if (Source.Title != "") Dest.Title = Source.Title;
            if (Source.Visibility != "") Dest.Visibility = Source.Visibility;
            //if (Source.Access_Keys != "") Dest.Access_Keys = Source.Access_Keys;
            if (Source.Page != "") Dest.Page = Source.Page;
            if (Source.Css != "") Dest.Css = Source.Css;
            if (Source.Script != "") Dest.Script = Source.Script;
            if (Source.Inc != "") Dest.Inc = Source.Inc;
            if (Source.Class != "") Dest.Class = Source.Class;
            if (Source.Diconnected_Redir != "") Dest.Diconnected_Redir = Source.Diconnected_Redir;
            if ((Source.Params != null) && (Source.Params.Count > 0))
            {
                Dest.Params.AddRange(Source.Params);
            }
            if (Source.Is_Razor) Dest.Is_Razor = true;

            // On ne fusionne que les modules dont le nom est non vide
            if (Source.Modules != null)
            {
                foreach (Module_Def M in Source.Modules)
                {
                    bool Found = false;
                    if (M.Name != "")
                    {
                        foreach (Module_Def D in Dest.Modules)
                        {
                            if (D.Name == "") continue;
                            if (D.Name == M.Name)
                            {
                                Merge(D, M);
                                Found = true;
                                break;
                            }
                        }
                    }
                    if (!Found) Dest.Modules.Add(M);
                }
            }
        }

        public void Register()
        {
            if (Views.ContainsKey(Name))
            {
                Merge(Views[Name], this);
            }
            else
            {
                Views[Name] = this;
            };
        }

        public class Dic_File
        {
            DateTime Date = DateTime.MinValue;
            string File_Name;
            public void Check ()
            {
                var D = JR.Files.Safe_Date(File_Name);
                if (Date == D) return;
                JR.Saf.Main.Dico.Load_Xls_Dictionary(File_Name);
                Date = D;
            }
            public Dic_File (string File)
            {
                File_Name = File;
                Check();
            }
        }
        static List<Dic_File> Dics = new List<Dic_File>();

        public static void Register_Dic (string File_Name)
        {
            Dics.Add(new Dic_File(File_Name));
        }

        public static void Reload_Dics ()
        {
            lock (Dics)
            {
                foreach (var D in Dics)
                {
                    D.Check();
                }
            }
        }

        public static DateTime Reconf_Version = DateTime.Now;

        public static bool Check_Reconf (ref DateTime Current_Version, Action Check)
        {
            if (Current_Version == Reconf_Version) return false;
            Check();
            Current_Version = Reconf_Version;
            return true;
        }

        static void Purge_Caches (string Dependencies)
        {
            Application_State.Current.A_Cache.Purge(Dependencies);
            foreach (var S in Application_State.Current.Session_States) S.S_Cache.Purge(Dependencies);
        }

        public static void Reconf (string Dependencies = "")
        {
            lock (Application_State.Current)
            {
                if (Dependencies == "")
                {
                    Reconf_Version = DateTime.Now;
                    Reload_Dics();
                }
                Purge_Caches(Dependencies);
            }
        }

        // Options 
        public static bool Razor_Translate = false;

    }
}
