using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using JR;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;


namespace JR.CMS
{
    // ensemble d'outils pour g�rer un dialogue
    public class Dialog
    {
        public Dialog ()
        {
        }

        public static Dialog Load_From(string File)
        {
            Dialog D = JR.Serial<Dialog>.From_Xml(System.IO.File.ReadAllText (File, Encoding.Default));
            Field Previous = null;
            foreach (Field F in D.Fields)
            {
                F.Complete();
                F.Previous = Previous;
                Previous = F;
            }
            return D;
        }

        public bool Save_To(string File)
        {
            try
            {
                System.IO.File.WriteAllText (File, JR.Serial<Dialog>.To_Xml (this), Encoding.Default);
                return true;
            }
            catch
            {
            }
            return false;
        }

        public class Field
        {
            public string Key = "";
            public string Title = "";
            public string Get_Title
            {
                get { return Main.Universal_String (this.Title) + (Required ? " (*)" :""); }
            }
            public string Value = "";
            public string Nature = "";
            public string Definition = "";
            public string Constraint = "";
            public string Check = "";
            public bool Required = false;
            public bool Read_Only = false;
            public bool Internal = false;
            public string Error_Message = "";
            public string Get_Error
            {
                get { return In_Error? Error_Message:""; }
            }
            [NonSerialized, XmlIgnore]
            public Dictionary<string, string> Values;
            [NonSerialized, XmlIgnore]
            public Control Control;
            [NonSerialized, XmlIgnore]
            public Field Previous;
            [NonSerialized, XmlIgnore]
            public bool In_Error = false;

            // Complete les �l�ments manquants
            public void Complete()
            {
                Nature = Nature.ToUpper();
                switch (Nature)
                {
                    case "LIST":
                    case "RADIO":
                    case "CHECK":
                        {
                            Values = new Dictionary<string,string>();
                            string [] Conf = JR.Strings.Split (Main.Universal_String(Definition));
                            if (Conf.Length == 1)
                            {
                                DataTable Dt = Main.Universal_Table(Definition);
                                foreach (DataRow DR in Dt.Rows)
                                {
                                    Values.Add(DR[0].ToString(), DR[1].ToString());
                                }
                                return;
                            }
                            string Key = "";
                            foreach (string C in Conf)
                            {
                                if (Key == "")
                                {
                                    Key = C;
                                }
                                else
                                {
                                    Values.Add(Key, C);
                                    Key = "";
                                }
                            }
                        }
                        break;
                }
            }

            Control Req_Validator(Control Validated)
            {
                RequiredFieldValidator V = new RequiredFieldValidator();
                V.ControlToValidate = Validated.ID;
                V.ValidationGroup = "DIALOG";
                V.Text = "[X]";
                V.CssClass = "error";
                V.Display = ValidatorDisplay.Dynamic;
                return V;
            }

            CustomValidator Custom_V;
            Control Cust_Validator(Control Validated)
            {
                Custom_V = new CustomValidator();
                Custom_V.ControlToValidate = Validated.ID;
                Custom_V.ValidationGroup = "DIALOG";
                Custom_V.ValidateEmptyText = false;
                Custom_V.Text = Error_Message;
                Custom_V.CssClass = "error";
                Custom_V.Display = ValidatorDisplay.Dynamic;
                return Custom_V;
            }

            public void Display_Value(ControlCollection Controls, ControlCollection Error_Controls, Custom_Display Display)
            {
                Controls.Clear();
                if (Error_Controls != null)  Error_Controls.Clear();
                switch (Nature)
                {
                    case "LIST":
                        {
                            DropDownList D = new DropDownList ();
                            Control = D;
                            D.CssClass = "Input";
                            D.Attributes["style"] = "min-width:50%";
                            D.ID = "LIST";
                            Controls.Add(D);
                            D.Enabled = !Read_Only;
                            D.Items.Add(new ListItem("", ""));
                            foreach (KeyValuePair<string, string> T in Values)
                            {
                                D.Items.Add(new ListItem(T.Value, T.Key));
                            }
                        }
                        break;
                    case "MTEXT":
                    case "PASS":
                    case "TEXT":
                        {
                            TextBox T = new TextBox();
                            T.CssClass = "Input";
                            Control = T;
                            T.Attributes["style"] = "width:98%";
                            Controls.Add(T);
                            T.ID = Nature;
                            T.Text = Value;
                            T.ReadOnly = Read_Only;
                            switch (Nature)
                            {
                                case "MTEXT":
                                    T.TextMode = TextBoxMode.MultiLine;
                                    break;
                                case "TEXT":
                                    T.TextMode = TextBoxMode.SingleLine;
                                    break;
                                case "PASS":
                                    T.TextMode = TextBoxMode.Password;
                                    break;
                            }
                        }
                        break;
                    case "CHECK":
                        {
                            CheckBox T = new CheckBox();
                            T.CssClass = "Input";
                            Control = T;
                            T.Attributes["style"] = "width:98%";
                            T.ID = Nature;
                            Controls.Add(T);
                            T.Enabled = !Read_Only;
                            foreach (KeyValuePair<string, string> K in Values)
                            {
                                T.Checked = (Value == K.Key);
                                T.Text = K.Value;
                                break;
                            }
                        }
                        break;
                    case "CUSTOM":
                        {
                            if (Display != null)
                            {
                                Control = Display(this);
                                if (Control != null) Controls.Add(Control);
                            }
                        }
                        break;
                    case "SEPARATOR":
                        {
                            Label L = new Label();
                            L.CssClass = "Input";
                            L.Text = Value;
                            Controls.Add(L);
                        }
                        break;
                }
                if (Constraint != "")
                {
                        RegularExpressionValidator V = new RegularExpressionValidator();
                        V.Display = ValidatorDisplay.Dynamic;
                        V.ControlToValidate = Control.ID;
                        V.ValidationExpression = Constraint;
                        V.Text = Error_Message;
                        V.ValidationGroup = "DIALOG";
                        Error_Controls.Add(V);
                }
                if (Check != "")
                {
                    Error_Controls.Add(Cust_Validator(Control));
                }
                if (Required)
                {
                    Error_Controls.Add(Req_Validator(Control));
                }
            }

            public void Read_Value(Custom_Validation Validation_Callback)
            {
                In_Error = false;
                switch (Nature)
                {
                    case "LIST":
                        {
                            DropDownList D = Control as DropDownList;
                            Value = D.Text;
                        }
                        break;
                    case "MTEXT":
                    case "PASS":
                    case "TEXT":
                        {
                            TextBox T = Control as TextBox;
                            Value = T.Text;
                        }
                        break;
                    case "CHECK":
                        {
                            Value = "";
                            CheckBox T = Control as CheckBox;
                            foreach (KeyValuePair<string, string> K in Values)
                            {
                                if (T.Checked)
                                {
                                    Value = K.Key;
                                }
                                break;
                            }
                        }
                        break;
                }
                if (Check  == "") return;
                if (Validation_Callback != null) Signal_Error(Validation_Callback(this));
            }

            public void Signal_Error (string Message)
            {
                Custom_V.Text = Message;
                Custom_V.IsValid = (Message == "");
                In_Error = !Custom_V.IsValid;
            }
        }
        public List<Field> Fields = new List<Field>();

        public bool Read_Fields(Custom_Validation Validation_Callback)
        {
            bool Error = false;
            foreach (Field F in Fields)
            {
                F.Read_Value(Validation_Callback);
                if (F.In_Error) Error = true;
            }
            return Error;
        }

        public static void Save_Sample ()
        {
            Dialog D = new Dialog();
            Field F = new Field ();
            D.Fields.Add(F);
            F = new Field ();
            D.Fields.Add(F);
            D.Save_To ("c:\\dialog.xml");
        }

        public delegate string Custom_Validation(Field Field);
        public delegate Control Custom_Display (Field Field);

        public Field this[string Key]
        {
            get
            {
                foreach (Field F in Fields)
                {
                    if (JR.Strings.Like (F.Key, Key)) return F;
                }
                return null;
            }
        }

        public string Title = "";
        public string Get_Title
        {
            get { return Main.Universal_String(this.Title); }
        }
    }
}
