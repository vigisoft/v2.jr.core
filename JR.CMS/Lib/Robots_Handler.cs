﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using JR.Graph;
using System.IO;

namespace JR.CMS
{
    /// <summary>
    /// Description résumée de Jpg_Handler
    /// </summary>
    public class Robots_Handler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            HttpResponse Response = context.Response;
            HttpRequest Req = context.Request;
            string Language = Session_State.Language_By_Request (Req);
            Response.ContentType = "text/plain";
            Response.TransmitFile(Main.Real_File_Name (string.Format ("robots-{0}.txt", Language)));
        }

        public bool IsReusable
        {
            get { return true; }
        }
    }
}