using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Web;
using System.Web.Util;
using System.Data;
using System.Text;

namespace JR.CMS
{

	/// <summary>
	/// Version all�g�e du Framework sp�cifique WEB
    /// Supprime des notions incompatibles, comme la langue
    ///  ou le nom d'application unique
	/// </summary>
	public class Main:JR.Saf.Main
	{

		// Charge les espaces et les chemins
        // Charge les fichiers de configuration
        public static bool Register(string Version, string Vendor)
        {
            Solver += Web_Solver;
            Before_Load_Config += new EventHandler(Main_Before_Load_Config);
            var Result = Register_Software(Version, Vendor, HttpContext.Current.Server.MapPath("~"), "WEB");
            return Result;
        }

        static void Main_Before_Load_Config(object sender, EventArgs e)
        {
            Add_Space("#CORE", HttpContext.Current.Server.MapPath("~/Core") + @"\{F}", true);
        }

        // Chemin racine du site
        public static string Site_Path (string File_Name)
        {
            return Base_Path(File_Name);
        }

        public static void Restart_Web_Server(bool Hard_Reboot = false)
        {
            if (Hard_Reboot) JR.OS.Kill_Program();
            System.IO.File.SetCreationTimeUtc(Site_Path("web.config"), DateTime.UtcNow);
            System.IO.File.SetLastWriteTimeUtc(Site_Path("web.config"), DateTime.UtcNow);
        }

        // Retourne le chemin virtuel de l'application
        // /admin par exemple
        // Attention dans une m�me application le chemin peut �tre diff�rent (cas de admin.smmof.fr equivalent a www.smmof.fr/admin)
        public static string Web_Root
        {
            get
            {
                string App_Path = HttpContext.Current.Request.ApplicationPath;
                return App_Path == "/" ? "" : App_Path;
            }
        }

        // Ajoute devant l'URL le chemin �ventuel de l'application
        // /admin par exemple
        public static string Web_URL(string URL)
        {
            return Web_Root + URL;
        }

        public static string Resource_URL(string Full_Path, int Width = 0, int Height = 0)
        {
            if (Full_Path == "") return "";
            var P = Full_Path.Paths();
            StringBuilder SB = new StringBuilder(Main.Parameter("RES.ROOT_URL"));
            SB.AppendFormat("/res/{0}?P={1}", P.File, P.Directory_URL);
            if (Width + Height != 0)
            {
                if (Width != 0) SB.AppendFormat("&W={0}", Width);
                if (Height != 0) SB.AppendFormat("&H={0}", Height);
            }
            return SB.ToString();
        }

        internal static string Web_Solver(string Key, string Param)
        {
            switch (JR.Strings.Official(Key))
            {
                case "WLW":  // Retourne le local web file
                    {
                        try
                        {
                            HttpContext H = HttpContext.Current;
                            string File_Name = H.Server.MapPath(Param);
                            if (!File.Exists(File_Name)) return "";
                            return File.ReadAllText(File_Name);
                        }
                        catch { }
                        return "";
                    }
                case "CFG":  // Retourne un item de config
                    {
                        try
                        {
                            string[] P = JR.Strings.Split(Param);
                            return Config.Get_String(Universal_String(P[0]), Universal_String(P[1]));
                        }
                        catch { }
                        return "";
                    }
                case "RES":  // Retourne l'url de resource associ�e � un fichier
                    {
                        return Resource_URL(Param);
                    }
                case "H":  // Retourne le texte converti en HTML
                case "HTML":  // Retourne le texte converti en HTML
                    {
                        return HttpUtility.HtmlEncode(Param);
                    }
                case "HA":  // Retourne le texte converti en HTML sauf si c'en est deja
                case "HTMLAUTO":  // Retourne le texte converti en HTML
                    {
                        // Si contient du html retourner sans modif
                        int Tag = Param.IndexOf('<');
                        if (Tag >= 0 && Param.IndexOf('>') > Tag) return Param;

                        // Sinon, splitter par ligne et si plus d'une ligne entourer de <P>
                        string[] Lines = Param.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                        if (Lines.Length < 2) return HttpUtility.HtmlEncode(Param);
                        StringBuilder SB = new StringBuilder();
                        foreach (string P in Lines)
                        {
                            //SB.Append("<p>");
                            SB.Append(HttpUtility.HtmlEncode(P));
                            //SB.Append("</p>");
                            SB.Append("<br/>");
                        }
                        return SB.ToString();
                    }
                case "HH":  // Retourne le texte converti en HTML sauf si c'en est deja, limit� au premier paragraphe
                    {
                        // Si contient du html retourner sans modif
                        int Tag = Param.IndexOf('<');
                        if (Tag >= 0 && Param.IndexOf('>') > Tag)
                        {
                            var C = new JR.Strings.Cursor(Param);
                            return C.Next_Word("<br/>");
                        }

                        // Sinon, splitter par ligne et si plus d'une ligne entourer de <P>
                        string[] Lines = Param.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                        if (Lines.Length < 2) return HttpUtility.HtmlEncode(Param);
                        return HttpUtility.HtmlEncode(Lines[0]);
                    }
                case "HD":  // Retourne le texte converti en HTML
                case "HTMLD":  // Retourne le texte converti en HTML
                    {
                        return HttpUtility.HtmlDecode(Param);
                    }
                case "G":  
                case "GUID":  // Retourne une repr�sentation valide de GUID pour pas planter dans les requ�tes
                    {
                        return JR.Convert.To_Guid (Param).ToString();
                    }
                case "LMTH":  // Retourne le HTML converti en text
                    {
                        return HttpUtility.HtmlDecode(Param);
                    }
                case "UD":  // Retourne le texte converti en HTML
                case "URLD":  // Retourne le texte converti en HTML
                    {
                        return HttpUtility.UrlDecode(Param);
                    }
                case "U":  // Retourne le texte converti en HTML
                case "URL":  // Retourne le texte converti en HTML
                    {
                        return HttpUtility.UrlEncode(Param);
                    }
                case "LNGU":  // URL Courante dans une langue particuli�re
                    {
                        Uri URI = HttpContext.Current.Request.Url;
                        string Domain = Main.Parameter("DOMAIN." + Param);
                        if (Domain == null) return URI.ToString();
                        return URI.AbsoluteUri.Replace(URI.Host, Domain);
                    }
            }
            return null;
        }

        public static string Encode_Url_Param(string Param)
        {
            if (Param.IndexOfAny("&@".ToCharArray()) < 0) return Param;
            return Param.Replace("&", "0z26").Replace("@", "0z64");
        }

        public static string Decode_Url_Param(string Param)
        {
            if (Param.IndexOf("0z") < 0) return Param;
            return Param.Replace("0z26", "&").Replace("0z64", "@");
        }

        public static string Js(string Source)
        {
            if (Source == null) return "";
            return Source.Replace("'", @"\'").Replace(@"""", @"\""");
        }

	}
}



 