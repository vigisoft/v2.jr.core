using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using JR;
using System.Net.Mail;

namespace JR.CMS
{

	/// <summary>
	/// Classe de gestion du projet principal
	/// </summary>
	public class Application_State
	{

        List<Session_State> _Session_States = new List<Session_State>();						// Liste des portails ouverts
        static Application_State _Current = null;
        public Content_Cache A_Cache = new Content_Cache();

        public Application_State(string Version, string Vendor)
        {
            _Current = this;
            Main.Before_Load_Config += new EventHandler((s,e) => Before_Load_Config());
            Main.Register(Version, Vendor);
            Session_Log.Init();
            Main.Solver += Web_Solver;
            Force_Lang_URL = Main.Bool_Parameter("CMS.FORCE_LANG_URL");
            SMTP_Server = Main.Parameter("SMTP_Server");


            // Signaler le d�marrage de l'application
            Logs.Log("APP_STARTING");

        }

        // Declench� avant lecture des fichiers de configuration
        public virtual void Before_Load_Config()
        {
        }

        public Application_State():this("1.0.1","Vigisoft")
        {
        }

        DateTime _Start_Time = DateTime.Now;
        public DateTime Start_Time
        {
            get { return _Start_Time; }
        }

        // Methode � sp�cialiser pour g�rer la fin de l'application
        public virtual void On_Remove()
        {
        }


        static Dictionary<string, string> Dic_Routes = new Dictionary<string, string>();
        public static void Declare_Route_Translation (string Lang, string Route, string New_Route)
        {
            var Key = Lang + Route.Scan().Next("/?");
            Dic_Routes[Key] = New_Route.Scan().Next("/?");
        }
        public static string Translate_Route (string Lang, string Route)
        {
            if (Dic_Routes.Count == 0) return Route;
            var C = Route.Scan();
            var Key = Lang + C.Next("/?");
            var Sep = C.Separator;
            string New_Page;
            if (!Dic_Routes.TryGetValue(Key, out New_Page)) return Route;
            var Args = C.Tail();
            if (Args.Length > 0) return New_Page + Sep + Args;
            return New_Page;
        }

        public static bool Force_Lang_URL = false;

        public static void Release_Current()
        {
            if (_Current == null) return;
            _Current.On_Remove();
            _Current = null;
        }

        public Application_State End()
        {
            Release_Current();
            Logs.Log("APP_STOPPED");
            return null;
        }

		public List<Session_State> Session_States
		{
			get {return _Session_States;}
		}

		public static Application_State Current 			// Retrouve le dernier projet courant
		{
            get { return _Current; }
        	set	{_Current = value;}
}

        // M�thode � sp�cialiser pour retourner une session
        public virtual Session_State On_New_Session ()
        {
            return new Session_State ();
        }

        public void Session_Start(HttpSessionState Session)
        {
            Create_Session (Session);
        }

        public void Session_End(HttpSessionState Session)
        {
            Release_Session(Session);
        }

        public void Error(HttpContext Context)
        {
            // Code qui s'ex�cute lorsqu'une erreur non g�r�e se produit
            Logs.Err("Erreur globale application [global.asax]");
            if (Context.Request != null)
            {
                Logs.Err(Context.Request.Url.OriginalString);
            }
            Logs.Err(Context.Error.Message + " (" + Context.Error.HResult + ")");
            Logs.Err(Context.Error.Source);
            Logs.Err(Context.Error.StackTrace);

            if (Context.Error.InnerException != null)
            {
                Logs.Err("Exception interne");
                Logs.Err(Context.Error.InnerException.Message);
                Logs.Err(Context.Error.InnerException.Source);
                Logs.Err(Context.Error.InnerException.StackTrace);
            }
        }


        public void Create_Session(HttpSessionState Session)
        {
            Session_State Sess = On_New_Session();
            _Session_States.Add(Sess);
            Session["JR.CMS.SESSION"] = Sess;
        }

        public void Release_Session(HttpSessionState Session)
        {
            Session_State Sess = (Session_State)Session["JR.CMS.SESSION"];
            if (Sess == null) return;
            Sess.Clear ();
            Session["JR.CMS.SESSION"] = null;
            _Session_States.Remove (Sess);
        }

        public class Language_Info
        {
            public string Key;
            public string Name;
            public string URL;
        }

        string[] Langs = null;
        public string[] Language_List
        {
            get
            {
                if (Langs == null) Langs = JR.Strings.Split(Main.Parameter("LANGUAGES"));
                return Langs;
            }
        }

        Dictionary<string, Language_Info> _Languages;
        public Dictionary<string, Language_Info> Languages
        {
            get
            {
                if (_Languages == null)
                {
                    _Languages = new Dictionary<string, Language_Info>();
                    foreach (string Lang in Language_List)
                    {
                        Language_Info LI = new Language_Info();
                        LI.Key = Lang;
                        LI.Name = Main.Get_String(Lang, "UI", "LANG.NAME");
                        LI.URL = Main.Parameter(Lang + ".URL");
                        _Languages.Add(Lang, LI);
                    }
                }
                return _Languages;
            }
        }

        static string Web_Solver(string Key, string Param)
        {
            switch (JR.Strings.Official(Key))
            {
                case "PUI":  // ui string
                    {
                        return Session_State.Current.Local_Ui(Param);
                    }
                case "LNG":  // Current language
                    {
                        // syntaxes : [LNG], [LNG:FR], [LNG:regular expression]
                        if (Param == "") return Session_State.Current.Language;
                        return Session_State.Current.Language.Match (Param, true)?"1":"0";
                    }
                case "SCN":  // Session connection = no
                    return (Session_State.Current.User.Connected ^ (Param == "!")) ? "0" : "1";

                case "CNX":  // connected

                    // syntaxes : [CNX], [CNX:0], [CNX:1], [CNX:false], ...
                    if (Param == "") Param = "1";
                    bool Result = Session_State.Current.User.Connected ^ !Param.To_Boolean();
                    return Result ? "1" : "0";
            }
            return null;
        }


        public string SMTP_Server { get; set; }

        // Envoi d'un mail avec un flag qui indique en retour si ca s'est bien pass�
        public static bool Send(System.Net.Mail.MailMessage Mail)
        {
            try
            {
                string Mail_Redirection = JR.CMS.Main.Parameter("Mail_Redirection");
                if (Mail_Redirection != "")
                {
                    MailAddress Addr = new MailAddress(Mail_Redirection, Mail.To.ToString().Replace("@", "_AT_"));
                    Mail.To.Clear();
                    Mail.To.Add(Addr);
                }
                using (var Client = new System.Net.Mail.SmtpClient(Current.SMTP_Server))
                {
                    Client.Send(Mail);
                }
                return true;
            }
            catch
            {
            }
            return false;

        }

        public static string Execute (string Source_Code, object Me = null, string Signature = "")
        {
            var M = new Module(null, "");
            M.Bag["Me"] = Me;
            return M.Execute(Source_Code, Signature);
        }

    }
	
}
