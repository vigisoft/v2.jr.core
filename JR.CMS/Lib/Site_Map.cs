using System;
using System.Collections.Generic;
using System.Data;
using JR;
using System.Xml;
using System.Xml.Linq;

namespace JR.CMS
{

	/// <summary>
	/// Classe de gestion du projet principal
    //        <?xml version="1.0" encoding="UTF-8"?>

    //        <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    //   <url>

    //      <loc>http://www.example.com/</loc>

    //      <lastmod>2005-01-01</lastmod>

    //      <changefreq>monthly</changefreq>

    //      <priority>0.8</priority>

    //   </url>

    //</urlset> 
    /// </summary>
	public class Site_Map
	{

        XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";  
        XElement Map;
        public Site_Map()
        {
            Map = new XElement(ns + "urlset", new XAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9"));
        }

        public string Root_Url = "";
        public void Add_Page (string Loc, string LastMod, string Priority, string ChangeFreq)
        {
            string Full_Url = Root_Url + Loc;
            XElement Url = new XElement(ns + "url", new XElement(ns + "loc", Full_Url));
            if (LastMod != "") Url.Add (new XElement(ns + "lastmod", LastMod));
            if (Priority != "") Url.Add (new XElement(ns + "priority", Priority));
            if (ChangeFreq != "") Url.Add (new XElement(ns + "changefreq", ChangeFreq));
            Map.Add(Url);
        }

        public void Save(string File_Name)
        {
            JR.Files.Check_Directory(File_Name);
            Map.Save(File_Name);
        }
    }


    /// <summary>
    /// Classe de gestion du projet principal
    //<?xml version="1.0" encoding="UTF-8"?>

    //<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    //   <sitemap>

    //      <loc>http://www.example.com/sitemap1.xml.gz</loc>

    //      <lastmod>2004-10-01T18:23:17+00:00</lastmod>

    //   </sitemap>

    //   <sitemap>

    //      <loc>http://www.example.com/sitemap2.xml.gz</loc>

    //      <lastmod>2005-01-01</lastmod>

    //   </sitemap>

    //</sitemapindex>
    /// </summary>
    public class Site_Map_Index
    {

        XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
        XElement Index;
        public Site_Map_Index()
        {
            Index = new XElement(ns + "sitemapindex", new XAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9"));
        }

        public string Root_Url = "";
        public void Add_Map(string Loc, string LastMod)
        {
            string Full_Url = Root_Url + Loc;
            XElement Url = new XElement(ns + "sitemap", new XElement(ns + "loc", Full_Url));
            if (LastMod != "") Url.Add(new XElement(ns + "lastmod", LastMod));
            Index.Add(Url);
        }

        public void Save(string File_Name)
        {
            JR.Files.Check_Directory(File_Name);
            Index.Save(File_Name);
        }
    }
    
    
    



	
}
