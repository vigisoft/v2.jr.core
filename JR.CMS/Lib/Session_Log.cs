using System;
using System.Collections.Generic;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web;
using System.Data;
using System.Globalization;
using JR.DB;

namespace JR.CMS
{
	/// <summary>
	/// Classe de gestion du loggin de visite de session
	/// </summary>

	public class Session_Log
	{

        // Active le logging des pages et sessions
        public static bool Log_Sessions = false;
        public static bool Log_Countries = false;


        Dictionary<string, string> Pages = new Dictionary<string, string>();

        long Id_Session;
        public void Log_Page(string Params = "")
        {
            bool Is_Robot = false;
            var R = HttpContext.Current.Request;
            // Si premi�re page logger l'ouverture de session
            if (Pages.Count == 0)
            {
                string Agent = R.UserAgent.ToLower();
                if (Agent.Contains("bot") || Agent.Contains("spid") || Agent.Contains("crawl"))
                {
                    Is_Robot = true;
                    return;
                }
                string IP = R.UserHostAddress;
                string Language = "";
                if (R.UserLanguages != null)
                {
                    foreach (string S in R.UserLanguages)
                    {
                        Language = S; break;
                    }
                }
                string URL = R.Url.OriginalString;
                Id_Session = (long)App_Db.Insert("insert into LOG_Session (date,ip,language,agent,URL) values (getdate(),@0,@1,@2,@3)", IP, Language, Agent, URL);
                if (Log_Countries)
                {
                    JR.GEO.Session_Geo.Update_Geo_Infos(IP, Id_Session);
                }
            }
            if (Is_Robot) return;
            string State = User_State.Current.Connected?"C":"";
            string Page = R.Url.PathAndQuery;
            if (Pages.ContainsKey (Page + State)) return;
            Pages[Page + State] = Page;
            App_Db.Execute("insert into Log_Page (ID_Session, Page, State, Params) values (@0,@1,@2,@3)", Id_Session, Page, State, Params);
        }

        public static void Init ()
        {
            Log_Sessions = Main.Bool_Parameter("SESSIONS.LOG");
            Log_Countries = Main.Bool_Parameter("SESSIONS.LOG_COUNTRIES");
            if (Log_Countries) JR.GEO.Session_Geo.Update_Geo_Db();
            var Keep = Main.Int_Parameter("SESSIONS.KEEP_MONTHS");
            if (Keep > 0)
            {
                var Date_Min = DateTime.Now.Date.AddMonths(-Keep);
                App_Db.Execute("delete from Log_Session where date < @0", Date_Min);
            }
        }
	}
}
