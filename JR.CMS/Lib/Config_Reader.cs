using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using JR.DB;

namespace JR.CMS
{
    public class Config_Reader
    {
        Config Config;
        string Lang = "FR";
        public Config_Reader(string Key)
        {
            Config = new Config(Key);
            Lang = Session_State.Current.Language;
        }

        public string Get_String(params object[] Keys)
        {
            return Config.Get_String (Keys, Lang);
        }

        public int Get_Int(params object[] Keys)
        {
            return JR.Convert.To_Integer (Get_String(Keys));
        }
        public double Get_Float(params object[] Keys)
        {
            return JR.Convert.To_Float(Get_String(Keys));
        }
        public bool Get_Bool(params object[] Keys)
        {
            return JR.Convert.To_Boolean(Get_String(Keys));
        }
        public DateTime Get_Date(params object[] Keys)
        {
            return JR.Convert.To_Time(Get_String(Keys));
        }

        public Link Get_Link(params object[] Keys)
        {
            return new Link { Name = Get_String(Keys, "NAME"), URL = Get_String(Keys, "URL"), Target = Get_String(Keys, "TARGET"), Icon = Get_String(Keys, "ICON") };
        }

        public class Link
        {
            public string Name { get; set; }
            public string URL { get; set; }
            public string Target { get; set; }
            public string Icon { get; set; }
        }

        public string [] Get_List(params object[] Keys)
        {
            return Get_String(Keys).Split(',');
        }

    }
}
