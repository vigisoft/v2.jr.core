using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using JR.DBO;
using JR.Saf;

namespace JR.CMS
{
    public static class Links
    {
        public const string CSS_FA_4 = "<link href='//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet'>";
        public const string CSS_BS_3 = "<link href='//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' rel='stylesheet'>";
        public const string CSS_BST_3 = "<link href='//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css' rel='stylesheet'>";
        public const string CSS_JQUI = "<link href='/Core/Css/jquery-ui.min.css' rel='stylesheet'>";
        public const string CSS_JQUIT = "<link href='/Core/Css/jquery-ui.theme.min.css' rel='stylesheet'>";
        public const string JS_BS_3 = "<script src='//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>";
        public const string JS_JQ_1 = "<script src='//code.jquery.com/jquery-1.11.3.min.js'></script>";
        public const string JS_JQM_1 = "<script src='//code.jquery.com/jquery-migrate-1.2.1.min.js'></script>";
        public const string JS_JQUI_1 = "<script src='/Core/Scripts/jquery-ui-1-11-4.min.js'></script>";
        public const string JS_JQ_3 = "<script src='//code.jquery.com/jquery-3.3.1.min.js'></script>";
        public const string JS_JQM_3 = "<script src='//code.jquery.com/jquery-migrate-3.0.1.min.js'></script>";
        public const string JS_JQUI_3 = "<script src='/Core/Scripts/jquery-ui-1-12-1.min.js'></script>";
        public const string JS_MDZR_2 = "<script src='//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.8.3.js'></script>";
        public const string JS_CMS = "<script src='/Core/Js/JR.CMS.js'></script>";
    }
}
