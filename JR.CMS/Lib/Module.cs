using System;
using System.Collections.Generic;
using System.Text;
using JR.Saf;
using System.Web.UI;
using System.Data;
using RazorEngine.Templating;
using RazorEngine.Text;
using System.Web;
using System.Dynamic;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace JR.CMS
{
    public interface I_Module
    {
        Module M { get; set; }
    }

    public interface I_Module_Include
    {
        string Get_Output();
    }

    public interface I_Event_Module : I_Module
    {
        void Event_Receive(string Event_Name, params object[] Params);
    }

    public class Module
    {
        View _Parent;
        string _Name;
        public Module(View Parent, string Name)
        {
            _Parent = Parent;
            _Name = Name;
        }

        public Page_State Page
        {
            get { return _Parent.Page_State; }
        }

        Bag _Bag = new Bag();
        public Bag Bag
        {
            get { return _Bag; }
        }

        public string Language
        {
            get
            {
                return Session.Language;
            }
        }

        public string Ui_Format(string Key, params object[] Params)
        {
            return Session.Language.Lang_Format(Ui(Key), Params);
        }

        public string Format(string Format_String, params object[] Params)
        {
            return Session.Language.Lang_Format(Format_String, Params);
        }

        public Session_State Session
        {
            get { return _Parent.Page_State.Session; }
        }

        public View Parent
        {
            get { return _Parent; }
        }

        public View View
        {
            get { return _Parent; }
        }

        public I_Dialog Dialog { get; set; }
        public string Name
        {
            get { return _Name; }
        }
        public string Class = "";   // Classe css
        public string Id = "";   // Id html
        public string Visibility = "";
        public string Target = ""; // Zone d'atterissage
        public string Control_File = ""; // Controle � charger
        public I_Module Control; // Control li�
        public string Dialog_Def = "";
        public Module_Def Def = null;
        public bool Is_Include = false;  // Indique que la sortie du module est incluse dans un autre module

        public bool No_Async = false;   // Indique que le controle n'a pas besoin d'�tre charg� pendant un async postback

        // Indique que le module peut �tre ignor�, par exemple lors de requ�te asynchrone
        public bool Dont_Load
        {
            get
            {
                if (In_Async && (bool)No_Async) return true;
                return false;
            }

        }

        public string Signature
        {
            get
            {
                return string.Format("{0}.{1}.{2}.{3}", Parent.Name, Name, Language, View_Def.Build_Time);
            }
        }

        // Dictionnaire de chaines localis�e
        public JR.Strings.Dic Local_Dic { get; set; }

        // Indique si le dictionnaire a �t� modifi� par rapport � la pr�c�dente instance du module
        // Utile pour recompiler le razor
        public bool New_Local_Dic { get; set; }

        //JR.Saf.Door _Door;
        //public bool Is_Allowed(Keys Keys)
        //{
        //    if (_Door == null) return true;
        //    if (Visibility != "" && !Main.Universal_String(Visibility).To_Boolean()) return false;
        //    return _Door.Is_Allowed(Keys);
        //}

        //public void Set_Security(string Access_Rules)
        //{
        //    _Door = new JR.Saf.Door(Access_Rules);
        //}

        public string this[string Param]
        {
            get
            {
                string P = _Parent.Parameter(this, Name, Param);
                if (P != "") return P;
                return _Parent.Parameter(Param);
            }
            set
            {
                _Parent.Set_Parameter(value, _Name, Param);
            }
        }

        public string L(string Key, params object [] Params)
        {
            string Value = "";
            if (Local_Dic != null) Value = Local_Dic[Key];
            if (Value == "" && View.Local_Dic != null) Value = View.Local_Dic[Key];
            if (Value == "") return "";
            if (Params.Length > 0) return string.Format(Value, Params);
            return Value;
        }

        public DataTable Get_Table(string Key)
        {
            string Src = Universal_Param(Key).Trim();
            if (Src.StartsWith("@"))
            {
                return JR.Convert.To_Table(this[Src.Substring(1)]);
            }
            else if (Src.StartsWith("T:"))
            {

                return Get_Bag(Src) as DataTable;
            }
            else
            {
                return Main.Universal_Table(Src);
            }
        }

        public string Solve(string Function, string Param)
        {
            return Main.Web_Solver(Function, Param);
        }

        public string Universal(string Value, bool Translate = true, Func<string, string> Spec_Solver = null)
        {
            return _Parent.Universal(Value, Translate, this, Spec_Solver);
        }

        public string Universal_Param(string Param, bool Translate = true)
        {
            if (Param == "") return null;
            return Universal(this[Param], Translate);
        }

        public object Get_Bag(string Key)
        {
            if (Bag.ContainsKey(Key)) return Bag[Key];
            return Parent.Bag[Key];
        }

        string Expanded_Key (string Key)
        {
            if (Key.Length < 2) return Key;
            switch (Key[0])
            {
                case '.': return string.Concat(_Parent.Name, Key);
            }
            return Key;
        }

        public string Def_Ui(string Key, string Default_Value = "")
        {
            return Session.Local_Ui(Expanded_Key(Key), false, Default_Value);
        }

        public string Def_NUi(string Key, string Default_Value = "")
        {
            return Session.Local_Ui(Expanded_Key(Key), true, Default_Value);
        }

        public string Ui(string Key, params object[] Params)
        {
            if (Params.Length > 0)
            {
                return string.Format(Session.Local_Ui(Expanded_Key(Key)), Params);
            }
            return Session.Local_Ui(Expanded_Key(Key));
        }

        public string NUi(string Key, params object[] Params)
        {
            if (Params.Length > 0)
            {
                return string.Format(Session.Local_Ui(Expanded_Key(Key)), Params, true);
            }
            return Session.Local_Ui(Expanded_Key(Key), true);
        }

        public T Link_Sub_Control<T>(string Module_Name, Control Container) where T : Control, I_Module
        {
            T Child = Parent.Find_Module<T>(Module_Name);
            if (Child == null) return null;
            Container.Controls.Add(Child);
            return Child;
        }

        public void Raise_Event(string Event_Name, params object[] Params)
        {
            Parent.Raise_Module_Event(this, Event_Name, Params);
        }

        // Retourne le module container d'un control quelconque
        public static Module Current(Control Child)
        {
            Control Parent = Child;
            while (Parent != null && !(Parent is Page))
            {
                if (Parent is I_Module)
                {
                    return (Parent as I_Module).M;
                }
                Parent = Parent.Parent;
            }
            return null;
        }

        public interface I_Dialog
        {
            void Bind(Module Content);
            IList<object> Dialog_Context { get; set; }
            object Dialog_Status { get; set; }
            void Close(object Status);
            void Show(Module Content, params object[] Context);
            void Update();
            string Title { get; set; }
        }

        public interface I_Dialog_Content : I_Module
        {
            void Close();
            void Show();
        }

        public void Relink(System.Web.UI.Control C)
        {
            C.ID = Name;
            Control = (I_Module)C;
            Control.M = this;
        }

        public void Hide()
        {
            Control C = Control as System.Web.UI.Control;
            if (C == null) return;
            C.Visible = false;
        }

        public IEncodedString Lang_URL(string New_Lang)
        {
            string URI = HttpContext.Current.Request.Url.AbsoluteUri;
            return new RawString((URI + "/").Replace("/" + Language + "/", "/" + New_Lang + "/").Remove(URI.Length));
        }

        public bool Is_URL(string URL_Pattern)
        {
            string Path = HttpContext.Current.Request.Url.PathAndQuery.ToLower();
            return Path.EndsWith(URL_Pattern.ToLower());
        }

        public string Res_URL(string Full_Path, int Width = 0, int Height = 0)
        {
            return Main.Resource_URL(Full_Path, Width, Height);
        }

        public bool In_Async
        {
            get { return View.In_Async; }
        }
        public void Execute_Async_JS(string Code)
        {
            View.Execute_Async_JS(Code);
        }

        static IRazorEngineService RZS = null;
        static object RZS_Lock = new Object();
        public virtual Type RZ_Template_Type
            {
            get { return typeof(Razor_Template<>); }
            }

        IRazorEngineService Razor_Service
        {
            get 
            {
                lock (RZS_Lock)
                {
                    if (RZS == null)
                    {
                        var Config = new RazorEngine.Configuration.TemplateServiceConfiguration();
                        Config.BaseTemplateType = RZ_Template_Type;
                        Config.Debug = Main.Bool_Parameter("RAZOR.CMS.DEBUG");
                        RZS = RazorEngine.Templating.RazorEngineService.Create(Config);
                    }
                }
                return RZS;
            }
        }

        public bool Is_Razor = false;

        // Retourne le code de la vue + celui du module
        string Razor_Code
        {
            get
            {
                var SB = new StringBuilder(_Parent.Parameter("Inner"));
                SB.Append(_Parent.Parameter("Code"));
                if (SB.Length > 0) SB.AppendLine();
                SB.Append(_Parent.Parameter(this, Name, "Inner"));
                SB.Append(_Parent.Parameter(this, Name, "Code"));
                if (!View_Def.Razor_Translate) return SB.ToString();
                string Seps = _Parent.Parameter(this, Name, "Seps");
                char Open_Sep = '{';
                char Close_Sep = '}';
                if (Seps.Length==2)
                {
                    Open_Sep = Seps[0];
                    Close_Sep = Seps[1];
                }
                return _Parent.Static_Translation(SB.ToString(), this, Open_Sep, Close_Sep);
            }

        }

        public bool Compile(bool Quite = false)
        {
            try
            {
                Razor_Service.Compile(Razor_Code, Signature, this.GetType());
                return true;
            }
            catch (Exception E)
            {
                if (!Quite)
                {
                    Logs.Err("Compiling Error URL = {0}, Module={1}", System.Web.HttpContext.Current.Request.Url.AbsoluteUri, this.Name);
                    Logs.Except(E);
                }
                return false;
            }
        }

        public string Render_Part = "";
        public object Render_Context = null;
        public bool Render_Canceled = false;
        public string Render_Partial_Content (string Part, object Context = null)
        {
            var Old_Part = Render_Part;
            var Old_Context = Render_Context;
            Render_Part = Part;
            Render_Context = Context;
            var Content = Parse();
            Render_Part = Old_Part;
            Render_Context = Old_Context;
            return Content;
        }

        public string Parse(bool Quite = false)
        {
            try
            {
                var S = Signature;
                var T = GetType();
                if (!Razor_Service.IsTemplateCached(S, T))
                {
                    Razor_Service.Compile(Razor_Code, S, T);
                }
                return Razor_Service.Run(S, T, this).Trim();
            }
            catch (Exception E)
            {
                if (!Quite)
                {
                    Logs.Err("Parsing Error URL = {0}, Module={1}", System.Web.HttpContext.Current.Request.Url.AbsoluteUri, this.Name);
                    Logs.Except(E);
                }
            }
            return "";
        }

        public string Get_Content()
        {
            string Cache_Level = this["$cache"].ToLower();
            Content_Cache.Entry E = null;
            if (Cache_Level != "" && Cache_Level != "0")
            {
                string Cache_Dependency = this["$cache_dep"];
                var SB = new StringBuilder(Parent.Name + Name);
                bool Global = false;
                foreach (var C in Cache_Level)
                {
                    switch (C)
                    {
                        case 'a':
                            Global = true;break;
                        case 'u':
                            SB.Append(HttpContext.Current.Request.Url.PathAndQuery);
                            break;
                        case 'e':
                            SB.Append(JR.Json.To_Json(View.Event));
                            break;
                    }

                }
                E = Session.Get_Content_Cache(Global, SB.ToString().To_Letters(), Cache_Dependency);
                if (E.Content != null)
                {
                    if (E.Datas != null)
                    {
                        foreach (var D in E.Datas)
                        {
                            Parent.Restore_Meta(D.Key, D.Value);
                        }
                    }
                    return E.Content;
                }
            }

            if (New_Local_Dic) Compile();
            bool Quite = JR.Convert.To_Boolean(this["$Quite"]);
            var Previous_Store = Parent.On_Store_Meta;
            Parent.On_Store_Meta = null;
            if (E != null)
            {
                Parent.On_Store_Meta = E.Push;
            }
            var Content = Parse(Quite);
            if (E != null)
            {
                E.Content = Content;
            }
            Parent.On_Store_Meta = Previous_Store;
            return Content;
        }


        // Execution spontann�e d'un script
        public string Execute (string Code, string Signature = "")
        {
            var T = GetType();
            if (Signature == "") Signature = Guid.NewGuid().ToString();
            if (!Razor_Service.IsTemplateCached(Signature, T))
            {
                Razor_Service.Compile(Code, Signature, T);
            }
            return Razor_Service.Run(Signature, T, this).Trim();
        }

        public class MyHtmlHelper
        {
            public IEncodedString Raw(string rawString)
            {
                return new RawString(rawString);
            }
        }

        // Exemple de personalisation RAZOR
        public class Razor_Template<T> : TemplateBase<T> where T:Module
        {
            public Razor_Template()
            {
                Html = new MyHtmlHelper();
            }

            public IEncodedString AutoRaw(string rawString)
            {
                return new RawString(rawString.To_Html_Auto());
            }

            public IEncodedString HeaderRaw(string rawString)
            {
                return new RawString(rawString.To_Html_Auto_Header());
            }

            public MyHtmlHelper Html { get; set; }

            public T m
            {
                get { return Model; }
            }
            public View v
            {
                get { return Model.View; }
            }

            public new IEncodedString Include(string Module_Name)
            {
                Module Inc;
                Model.View.Modules.TryGetValue(Module_Name, out Inc);
                if (Inc == null || !(Inc.Control is I_Module_Include)) return new RawString("");
                Inc.Is_Include = true;
                // Transmission du context
                Inc.Render_Part = "";
                Inc.Render_Context = Model.Render_Context;
                return new RawString((Inc.Control as I_Module_Include).Get_Output());
            }

        }


        public void Set_Input(Control C, string Key, string Validation_Group = null)
        {
            if (C is HtmlControl)
            {
                (C as HtmlControl).Attributes["placeholder"] = Ui(Key);
            }
            var R_Field = C.Parent.FindControl(C.ID.Replace("C_", "R_")) as RequiredFieldValidator;
            if (R_Field != null)
            {
                R_Field.ControlToValidate = C.ID;
                R_Field.CssClass = "text-danger";
                R_Field.ErrorMessage = Ui(Key.Replace(".", ".R_"));
                R_Field.ValidationGroup = Validation_Group ?? Name;
                R_Field.Display = ValidatorDisplay.Dynamic;
                R_Field.Attributes["data-error-style"] = "input-error";
            }
            var E_Field = C.Parent.FindControl(C.ID.Replace("C_", "E_")) as BaseValidator;
            if (E_Field != null)
            {
                E_Field.ControlToValidate = C.ID;
                E_Field.CssClass = "text-danger";
                E_Field.ErrorMessage = Ui(Key.Replace(".", ".E_"));
                E_Field.ValidationGroup = Validation_Group ?? Name;
                E_Field.Display = ValidatorDisplay.Dynamic;
                E_Field.Attributes["data-error-style"] = "input-error";
                if (R_Field != null) E_Field.Attributes["data-no-error-reset"] = "1";
                var RE_Field = E_Field as RegularExpressionValidator;
                if (RE_Field != null)
                {
                    if (Key.EndsWith("MAIL")) RE_Field.ValidationExpression = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                }
            }
        }

        public void Init_Input(HtmlControl C, string Key, string Value = null)
        {
            C.Attributes["placeholder"] = Ui(Key);
            if (Value != null)
            {
                var CT = C as HtmlInputControl;
                if (CT != null) CT.Value = Value;
            }
        }

        public class Input_Helper
        {
            public Input_Helper (Module M, string Error_Class = "input-error")
            {
                this.M = M;
                this.Error_Class = " " + Error_Class;
            }
            public string Error_Class = "";
            public bool Error
            {
                get { return Error_Fields.Count > 0; }
            }
            public Module M = null;
            HashSet<HtmlControl> Error_Fields = new HashSet<HtmlControl>();
            StringBuilder SB = new StringBuilder();

            void Show_Error (HtmlControl C, bool In_Error)
            {
                SB.AppendFormat("$('#{0}').{2}Class('{1}');", C.ClientID, Error_Class, In_Error?"add":"remove");
                SB.AppendLine();
                if (In_Error) Error_Fields.Add(C);
            }

            void Show_Text (HtmlGenericControl E, string Text)
            {
                SB.AppendFormat("$('#{0}').text('{1}');", E.ClientID, Text.Replace("'", @"\'"));
                SB.AppendLine();
            }

            public bool Set_Error(HtmlGenericControl E, HtmlControl C, string Message = "")
            {
                bool In_Error = Message.Length > 0;
                if (Error_Fields.Contains(C)) return In_Error;
                Show_Error(C, In_Error);
                Show_Text(E, Message);
                return In_Error;
            }

            public bool Set_Error(HtmlControl C, bool In_Error)
            {
                if (Error_Fields.Contains(C)) return In_Error;
                Show_Error(C, In_Error);
                return In_Error;
            }

            public bool Show_Errors()
            {
                M.Execute_Async_JS(SB.ToString());
                return Error;
            }
        }
    }
}
