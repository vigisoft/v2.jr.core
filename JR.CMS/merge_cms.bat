for %%F in (\distrib\cms\v2013\bin\*.dll) do del bin\%%~nF.dll
for %%F in (\distrib\cms\v2013\bin\fr\*.dll) do del /s bin\%%~nF.dll
del global.asa
del sitemap*.xml
del sitemaps\*.xml
del local\spaces.ini
del /S bin\ajax*.resources.dll
del bin\*.pdb
del bin\*.xml
md core
xcopy /y /s P:\DISTRIB\CMS\V2013\Core .\Core
