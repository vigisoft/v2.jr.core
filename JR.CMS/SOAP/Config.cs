﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.Diagnostics;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace JR.CMS.SOAP
{

[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Web.Services.WebServiceBindingAttribute(Name="ConfigSoap", Namespace="http://tempuri.org/")]
public partial class Config : System.Web.Services.Protocols.SoapHttpClientProtocol {
    
    //private System.Threading.SendOrPostCallback ReadOperationCompleted;
    
    //private System.Threading.SendOrPostCallback WriteOperationCompleted;
    
    /// <remarks/>
    public Config() {
        this.Url = "http://localhost:49293/Core/Service/Config.asmx";
    }
    
    ///// <remarks/>
    //public event ReadCompletedEventHandler ReadCompleted;
    
    ///// <remarks/>
    //public event WriteCompletedEventHandler WriteCompleted;
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/Read", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    public Key_Value[] Read(string Key) {
        object[] results = this.Invoke("Read", new object[] {
                    Key});
        return ((Key_Value[])(results[0]));
    }
    
    ///// <remarks/>
    //public System.IAsyncResult BeginRead(string Key, System.AsyncCallback callback, object asyncState) {
    //    return this.BeginInvoke("Read", new object[] {
    //                Key}, callback, asyncState);
    //}
    
    ///// <remarks/>
    //public Key_Value[] EndRead(System.IAsyncResult asyncResult) {
    //    object[] results = this.EndInvoke(asyncResult);
    //    return ((Key_Value[])(results[0]));
    //}
    
    ///// <remarks/>
    //public void ReadAsync(string Key) {
    //    this.ReadAsync(Key, null);
    //}
    
    ///// <remarks/>
    //public void ReadAsync(string Key, object userState) {
    //    if ((this.ReadOperationCompleted == null)) {
    //        this.ReadOperationCompleted = new System.Threading.SendOrPostCallback(this.OnReadOperationCompleted);
    //    }
    //    this.InvokeAsync("Read", new object[] {
    //                Key}, this.ReadOperationCompleted, userState);
    //}
    
    //private void OnReadOperationCompleted(object arg) {
    //    if ((this.ReadCompleted != null)) {
    //        System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
    //        this.ReadCompleted(this, new ReadCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
    //    }
    //}
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/Write", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    public bool Write(string Key, Key_Value[] Vals) {
        object[] results = this.Invoke("Write", new object[] {
                    Key,
                    Vals});
        return ((bool)(results[0]));
    }
    
    ///// <remarks/>
    //public System.IAsyncResult BeginWrite(string Key, Key_Value[] Vals, System.AsyncCallback callback, object asyncState) {
    //    return this.BeginInvoke("Write", new object[] {
    //                Key,
    //                Vals}, callback, asyncState);
    //}
    
    ///// <remarks/>
    //public bool EndWrite(System.IAsyncResult asyncResult) {
    //    object[] results = this.EndInvoke(asyncResult);
    //    return ((bool)(results[0]));
    //}
    
    ///// <remarks/>
    //public void WriteAsync(string Key, Key_Value[] Vals) {
    //    this.WriteAsync(Key, Vals, null);
    //}
    
    ///// <remarks/>
    //public void WriteAsync(string Key, Key_Value[] Vals, object userState) {
    //    if ((this.WriteOperationCompleted == null)) {
    //        this.WriteOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWriteOperationCompleted);
    //    }
    //    this.InvokeAsync("Write", new object[] {
    //                Key,
    //                Vals}, this.WriteOperationCompleted, userState);
    //}
    
    //private void OnWriteOperationCompleted(object arg) {
    //    if ((this.WriteCompleted != null)) {
    //        System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
    //        this.WriteCompleted(this, new WriteCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
    //    }
    //}
    
    ///// <remarks/>
    //public new void CancelAsync(object userState) {
    //    base.CancelAsync(userState);
    //}
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
public partial class Key_Value {
    
    private string keyField;
    
    private string valueField;
    
    /// <remarks/>
    public string Key {
        get {
            return this.keyField;
        }
        set {
            this.keyField = value;
        }
    }
    
    /// <remarks/>
    public string Value {
        get {
            return this.valueField;
        }
        set {
            this.valueField = value;
        }
    }
}

///// <remarks/>
//[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1")]
//public delegate void ReadCompletedEventHandler(object sender, ReadCompletedEventArgs e);

///// <remarks/>
//[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1")]
//[System.Diagnostics.DebuggerStepThroughAttribute()]
//[System.ComponentModel.DesignerCategoryAttribute("code")]
//public partial class ReadCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
    
//    private object[] results;
    
//    internal ReadCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
//            base(exception, cancelled, userState) {
//        this.results = results;
//    }
    
//    /// <remarks/>
//    public Key_Value[] Result {
//        get {
//            this.RaiseExceptionIfNecessary();
//            return ((Key_Value[])(this.results[0]));
//        }
//    }
//}

///// <remarks/>
//[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1")]
//public delegate void WriteCompletedEventHandler(object sender, WriteCompletedEventArgs e);

///// <remarks/>
//[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1")]
//[System.Diagnostics.DebuggerStepThroughAttribute()]
//[System.ComponentModel.DesignerCategoryAttribute("code")]
//public partial class WriteCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
    
//    private object[] results;
    
//    internal WriteCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
//            base(exception, cancelled, userState) {
//        this.results = results;
//    }
    
//    /// <remarks/>
//    public bool Result {
//        get {
//            this.RaiseExceptionIfNecessary();
//            return ((bool)(this.results[0]));
//        }
//    }
//}

    
}