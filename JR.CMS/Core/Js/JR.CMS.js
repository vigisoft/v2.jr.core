﻿// Surcharger la méthode qui affiche les erreurs
function Customize_ASP_Validation() {
    if (typeof (Page_ClientValidate) !== "undefined") {
        Net_ValidatorUpdateDisplay = ValidatorUpdateDisplay;
        ValidatorUpdateDisplay = Custom_ValidatorUpdateDisplay;
    }
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(Custom_ValidatorUpdateAll);
}
function Custom_ValidatorUpdateAll(sender, args) {
}

function Custom_ValidatorUpdateDisplay(val) {
    Net_ValidatorUpdateDisplay(val);
    var error_style = $(val).attr('data-error-style');
    if (typeof (error_style) !== "string") return;
    if (val.isvalid) {
        if ($(val).attr('data-no-error-reset') !== undefined) return;
        $('#' + val.controltovalidate).removeClass(error_style);
    } else {
        $('#' + val.controltovalidate).addClass(error_style);
    }
}

$(document).ready(function () {
    Customize_ASP_Validation();
    $('[data-append-to]').each(function () {
        $(this).appendTo($(this).attr('data-append-to'));
    });
});