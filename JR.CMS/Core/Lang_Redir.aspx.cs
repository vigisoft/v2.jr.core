﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JR.CMS.Core
{
    public partial class Lang_Redir : System.Web.UI.Page
    {
        protected override void OnInit(EventArgs e)
        {
            Response.Redirect("~/" + JR.CMS.Session_State.Current.Language + Request.Url.PathAndQuery);
        }
    }
}