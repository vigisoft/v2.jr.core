﻿USE [JRO_DB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [CMS_Request](
	[Id] [uniqueidentifier] NOT NULL,
	[Date_Request] [datetime] NULL,
	[Date_Action] [datetime] NULL,
	[Date_Remind] [datetime] NULL,
	[Follower] [nvarchar](max) NULL,
	[Priority] [int] NULL,
	[Comment] [nvarchar](max) NULL,
	[Message] [nvarchar](max) NULL,
	[Subject] [nvarchar](max) NULL,
	[Ref_Contact] [nvarchar](100) NULL,
	[Email] [nvarchar](max) NULL,
	[Coord] [nvarchar](max) NULL,
 CONSTRAINT [PK_CMS_Request] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [CMS_Link](
	[Id] [uniqueidentifier] NOT NULL,
	[Title_FR] [nvarchar](max) NULL,
	[Title_EN] [nvarchar](max) NULL,
	[Desc_FR] [nvarchar](max) NULL,
	[Desc_EN] [nvarchar](max) NULL,
	[URL] [nvarchar](max) NULL,
	[Target] [nvarchar](max) NULL,
	[Link_Date] [datetime] NULL,
 CONSTRAINT [PK_CMS_Link] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [V$CMS_Request]
AS
SELECT     dbo.CMS_Request.*, dbo.OBJ.Alias, dbo.OBJ.Name, dbo.OBJ.Id_Parent, dbo.OBJ.Id_Class, 
                      dbo.OBJ.Tick, dbo.OBJ.Build, dbo.OBJ.Layer, dbo.OBJ.Protection, dbo.OBJ.State, dbo.OBJ.Flags
FROM         dbo.CMS_Request INNER JOIN
                      dbo.OBJ ON dbo.CMS_Request.Id = dbo.OBJ.Id
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [V$CMS_Link]
WITH SCHEMABINDING 
AS
SELECT     dbo.CMS_Link.Id,
 dbo.CMS_Link.Title_FR,
 dbo.CMS_Link.Desc_FR,
 dbo.CMS_Link.URL, dbo.CMS_Link.Target,dbo.CMS_Link.Link_Date, 
 ISNULL(dbo.CMS_Link.Title_EN,dbo.CMS_Link.Title_FR) as Title_EN,
 ISNULL(dbo.CMS_Link.Desc_EN,dbo.CMS_Link.Desc_FR) as Desc_EN,
 dbo.OBJ.Alias, dbo.OBJ.Name, dbo.OBJ.Id_Parent, dbo.OBJ.Id_Class, dbo.OBJ.Tick, dbo.OBJ.Build, dbo.OBJ.Layer, dbo.OBJ.Protection, dbo.OBJ.State, dbo.OBJ.Flags
FROM dbo.CMS_Link INNER JOIN
  dbo.OBJ ON dbo.CMS_Link.Id = dbo.OBJ.Id
GO
ALTER TABLE [CMS_Link]  WITH CHECK ADD  CONSTRAINT [FK_CMS_Link_OBJ] FOREIGN KEY([Id])
REFERENCES [OBJ] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [CMS_Link] CHECK CONSTRAINT [FK_CMS_Link_OBJ]
GO
ALTER TABLE [CMS_Request]  WITH CHECK ADD  CONSTRAINT [FK_CMS_Request_OBJ] FOREIGN KEY([Id])
REFERENCES [OBJ] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [CMS_Request] CHECK CONSTRAINT [FK_CMS_Request_OBJ]
GO
