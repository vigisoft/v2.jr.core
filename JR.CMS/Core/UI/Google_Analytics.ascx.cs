﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JR.CMS.Core.UI
{
    public partial class Google_Analytics : System.Web.UI.UserControl, I_Module
    {
        protected string Domain_Clause = "";
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            M.No_Async = true;
            string Domain = M["DOM"];
            if (Domain != "")
            {
                Domain_Clause = string.Format(@"_gaq.push(['_setDomainName', '{0}']);_gaq.push(['_setAllowLinker', true]);", Domain);
            }
        }
        #region I_Module Membres
        public JR.CMS.Module M { get; set; }
        #endregion
    }
}