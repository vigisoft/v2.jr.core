﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace JR.CMS.Core.UI
{
    public partial class Script_Box : System.Web.UI.UserControl, I_Module
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!IsPostBack)
            {
                string Includes = M["INCLUDES"];
                foreach (string I in Includes.Split(';'))
                {
                    string Path = I.Trim();
                    if (Path == "") continue;
                    ScriptManager.RegisterClientScriptInclude(this, this.GetType(), I, I);
                }
                string Code = M["CODE"].Trim();
                if (Code != "")
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), string.Format("{0}.Code", M.Id), Code, true);
                }
            }
        }

        #region I_Module Membres
        public JR.CMS.Module M { get; set; }
        #endregion

    }
}