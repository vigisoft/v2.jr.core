﻿using System;
using System.Web.UI;

namespace JR.CMS.Core.UI
{
    public partial class Async_Manager : System.Web.UI.UserControl
    {
        class Event_Arg
        {
            public string e;
            public object a;
        }
        class Event_Args
        {
            public string e;
            public object [] a;
        }
        protected override void OnLoad(EventArgs e)
        {
            if (IsPostBack && V.Script_Manager.IsInAsyncPostBack && V.Script_Manager.AsyncPostBackSourceElementID == "AM_Panel")// && Request["__EVENTTARGET"] == "AM_Panel")
            {
                string Args = Request["__EVENTARGUMENT"];
                if (Args.EndsWith("]}"))
                {
                    var Ev = new Event_Args();
                    if (JR.Json.Populate(Ev, Args))
                    {
                        if (Ev.a == null)
                        {
                            V.Raise_Async_Event(Ev.e);
                        }
                        else
                        {
                            V.Raise_Async_Event(Ev.e, Ev.a);
                        }
                    }
                }
                else
                {
                    var Ev = new Event_Arg();
                    if (JR.Json.Populate(Ev, Args ))
                    {
                        if (Ev.a == null)
                        {
                            V.Raise_Async_Event(Ev.e);
                        }
                        else
                        {
                            V.Raise_Async_Event(Ev.e, Ev.a);
                        }
                    }
                }
            }
        }
        public View V { get; set; }
        public UpdatePanel Async_Panel
        {
            get { return AM_Panel; }
        }
    }
}