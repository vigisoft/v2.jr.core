﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JR.CMS.Core.UI
{
    public partial class Panel_Box : System.Web.UI.UserControl, I_Module
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Controls.Clear();

            // La syntaxe d'une map est : Nom class [Nom class [], Nom class [], ...], Nom class [], ...
            Strings.Cursor C = new Strings.Cursor(M["MAP"]);
            Action<Control> Scan = (Parent) =>{};
            Scan = (Parent) => {
                    string Def = C.Next(",[").Trim ();
                    if (Def == "") return;
                    Panel Div = new Panel();
                    Strings.Cursor DC = new Strings.Cursor(Def);
                    Div.ID = DC.Next (" ");
                    Div.CssClass = DC.Tail ();
                    Parent.Controls.Add (Div);
                    if (C.End || C.Separator == ',')
                    {
                        return;
                    } else if (C.Separator == '[')
                    {
                        while (!C.End && C.Separator != ']')
                        {
                            Scan(Div);
                        }
                    }
                };
            while (!C.End)
            {
                Scan(this);
            }

        }

       #region I_Module Membres
        public JR.CMS.Module M {get;set;}
        #endregion
    }
}