﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RazorUpdate.ascx.cs" Inherits="JR.CMS.Core.UI.RazorUpdate_Box" %>
<asp:UpdatePanel ID="UP_Panel" runat="server" UpdateMode="Always">
<ContentTemplate>
    <asp:Literal runat="server" ID="L_Content" Mode="PassThrough"></asp:Literal>
</ContentTemplate>
</asp:UpdatePanel>
