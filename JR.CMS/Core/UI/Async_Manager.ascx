﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Async_Manager.ascx.cs" Inherits="JR.CMS.Core.UI.Async_Manager" EnableViewState="false" %>
<asp:UpdatePanel ID="AM_Panel" runat="server" ClientIDMode="Static" UpdateMode="Conditional" EnableViewState="false">
</asp:UpdatePanel>
<script>
    var AM_In_Async = false;
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(pageLoaded);

    function pageLoaded(sender, args) {
        AM_In_Async = true;
        var dataItems = args.get_dataItems();
        var Scr = dataItems['AM_Panel'];
        if (Scr && Scr != '')
            try { eval(Scr); } finally { }
        AM_In_Async = false;
    }
    function AM_Notify(event, args) {
        window.event = {};  // pour ne pas planter __doPostBack, qui teste window.event
        __doPostBack('AM_Panel', JSON.stringify({ e: event, a: args }));
        //theForm.__EVENTTARGET.value = "";
    }
</script>
