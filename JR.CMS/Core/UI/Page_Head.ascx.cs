﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace JR.CMS.Core.UI
{
    public partial class Page_Head : System.Web.UI.UserControl, I_Module
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            
            string Lang = M.Session.Language;
            string Title = M["TITLE." + Lang];
            string Desc = M["DESCRIPTION." + Lang];
            string Styles = M["INCLUDES"];
            string Includes = M["INCLUDES"];
            string Code = M["SCRIPT"].Trim();

            if (!IsPostBack)
            {
                foreach (string I in Includes.Split(';'))
                {
                    string Path = I.Trim();
                    if (Path == "") continue;
                    ScriptManager.RegisterClientScriptInclude(this, this.GetType(), I, I);
                }
                if (Code != "")
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), string.Format("{0}.Code", M.Id), Code, true);
                }
            }
        }

        #region I_Module Membres
        public JR.CMS.Module M { get; set; }
        #endregion

    }
}