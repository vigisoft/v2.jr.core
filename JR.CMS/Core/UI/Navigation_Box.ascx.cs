﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace JR.CMS.Core.UI
{
    public partial class Navigation_Box : System.Web.UI.UserControl, I_Module
    {


        public override void RenderControl(HtmlTextWriter writer)
        {
            int Access_Check = -1;
            bool Late_Translation = JR.Convert.To_Boolean(M["LATE_TRANS"]);

            Func<DataRow, bool> Access_Ok = (Row) =>
            {
                if (Access_Check <= 0) return true;
                JR.Saf.Door Door = new JR.Saf.Door(Row[Access_Check - 1].ToString());
                return Door.Is_Allowed(M.Session.User.Keys);
            };
            Func<string, string> Late_Translate = (Source) =>
            {
                return Late_Translation ? M.Universal(Source) : Source;
            };

            string Header_Template = M.Universal_Param ("HEADER_TPL");
            string Footer_Template = M.Universal_Param ("FOOTER_TPL");
            string New_Line_Template = M.Universal_Param ("NEW_LINE_TPL");
            string Title_Template = M.Universal_Param ("TITLE_TPL");
            string Bottom_Template = M.Universal_Param("BOTTOM_TPL");
            int First_Row = 0;
            string Item_Template = M.Universal_Param("ITEM_TPL");
            int Max_Col = JR.Convert.To_Integer(M["MAX_COLUMN"]);
            DataTable DT = M.Get_Table ("SOURCE");
            if (DT == null || DT.Rows.Count == 0) return;
            int Last_Row = DT.Rows.Count - 1;
            base.RenderControl(writer);
            Access_Check = JR.Convert.To_Integer(M["ACCESS_ROW"]);
            if (Access_Check > DT.Columns.Count) Access_Check = 0;
            if (!Access_Ok(DT.Rows[0])) return;
            if (Header_Template != "")
            {
                writer.Write(Header_Template);
            }
            if (Title_Template != "")
            {
                writer.Write(Late_Translate(string.Format(Title_Template, DT.Rows[0].ItemArray)));
                First_Row++;
            }
            if (Bottom_Template != "")
            {
                Bottom_Template = Late_Translate(string.Format(Bottom_Template, DT.Rows[Last_Row].ItemArray));
                Last_Row--;
            }
            int Col = 0;
            for (int I = First_Row; I <= Last_Row; I++)
            {
                if (!Access_Ok(DT.Rows[I])) continue;
                writer.Write(Late_Translate(string.Format(Item_Template.Replace("{R}", I.ToString()), DT.Rows[I].ItemArray)));
                Col++;
                if (Col == Max_Col && I < Last_Row)
                {
                    if (New_Line_Template != "") writer.Write(New_Line_Template.Replace("{R}", I.ToString()));
                    Col = 0;
                };
            }
            if (Bottom_Template != "") writer.Write(Bottom_Template);
            if (Footer_Template != "") writer.Write(Footer_Template);
        }

        #region I_Module Membres
        public JR.CMS.Module M { get; set; }
        #endregion
    }
}