﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace JR.CMS.Core.UI
{
    public partial class Html_Box : System.Web.UI.UserControl, I_Module
    {
        public override void RenderControl(HtmlTextWriter writer)
        {
            base.RenderControl(writer);
            writer.Write("<div " + M["ATTRIB"] + " >");
            string Src = M.Universal_Param ("CONTENT");
            writer.Write(Src);
            writer.Write("</div>");
        }

        #region I_Module Membres
        public JR.CMS.Module M { get; set; }
        #endregion

    }
}