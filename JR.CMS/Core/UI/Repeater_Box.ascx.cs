﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.XPath;
using JR.DB;

namespace JR.CMS.Core.UI
{
    public partial class Repeater_Box : System.Web.UI.UserControl, I_Module
    {

        public override void RenderControl(HtmlTextWriter writer)
        {
            int Lang_Pos = M.Session.Language_Row;
            int Access_Check = -1;
            bool Late_Translation = JR.Convert.To_Boolean(M["LATE_TRANS"]);

            Func<string, string> Late_Translate = (Source) =>
            {
                return Late_Translation ? M.Universal(Source) : Source;
            };

            Func<string, string> Lang_Filtered = (Value) =>
            {
                JR.Strings.Cursor C = new Strings.Cursor(Value);
                string R = "";
                C.Next("{");
                for (int I = 0; I <= Lang_Pos; I++)
                {
                    if (!C.End) R = C.Next("|}");
                }
                return "{" + R + "}";
            };

            Func<string, string> Lang_Select = (Pattern) =>
            {
                if (Pattern.Length < 4) return Pattern;
                // Chercher une pattern {xx|xx}
                Match Match = Regex.Match(Pattern, @"\{([0-9])+(\|([0-9])+)+\}");
                if (!Match.Success) return Pattern;
                StringBuilder SB = new StringBuilder(Pattern);
                while (Match.Success)
                {
                    SB.Replace(Match.Value, Lang_Filtered(Match.Value));
                    Match = Match.NextMatch();
                }
                return SB.ToString();
            };

            string Full_Template = M.Universal_Param("TPL");
            string Header_Template = "";
            string Footer_Template = "";
            string New_Line_Template = "";
            string Title_Template = "";
            string Bottom_Template = "";
            string Item_Template = "";
            string Init_Template = "";
            string Exit_Template = "";

            // Le template global permet une imbrication sans erreur xml, par exemple
            //<tpl>
            //  <div>
            //  <BT>
            //  </BT>
            //  <IT>
            //  </IT>
            //  <NT>
            //  </NT>
            //  <TT>
            //  </TT>
            //  </div>
            //</tpl>
            if (Full_Template != "")
            {
                StringBuilder SB = new StringBuilder(Full_Template);
                int Split = -1;
                Func<string, string> Extract = (Key) =>
                    {
                        int Deb = Full_Template.IndexOf("<" + Key + ">");
                        if (Deb < 0) return "";
                        int Fin = Full_Template.IndexOf("</" + Key + ">", Deb+4);
                        if (Fin < 0) return "";
                        Split = Deb;
                        int L = Key.Length;
                        string Val = Full_Template.Substring(Deb + 2 + L, Fin - Deb - 2 -L);
                        Full_Template = Full_Template.Remove(Deb, Fin - Deb + 3 +L);
                        return Val;
                    };
                Title_Template = Extract ("T:0");
                Item_Template = Extract ("T:I");
                New_Line_Template = Extract ("T:L");
                Bottom_Template = Extract ("T:B");
                if (Split > 0)
                {
                    Header_Template = Full_Template.Substring (0,Split);
                    Footer_Template = Full_Template.Substring (Split);
                }
            }
            else
            {
                Header_Template = M.Universal_Param("HEADER_TPL");
                Footer_Template = M.Universal_Param("FOOTER_TPL");
                New_Line_Template = M.Universal_Param("NEW_LINE_TPL");
                Title_Template = Lang_Select(M.Universal_Param("TITLE_TPL"));
                Bottom_Template = Lang_Select(M.Universal_Param("BOTTOM_TPL"));
                Item_Template = Lang_Select(M.Universal_Param("ITEM_TPL"));
            }
            Init_Template = M.Universal_Param("INIT");
            Exit_Template = M.Universal_Param("EXIT");

            
            int First_Row = 0;
            int Max_Col = JR.Convert.To_Integer(M["MAX_COLUMN"]);
            int Last_Row = 0;
            bool Table_Mode = M["SOURCE"] != "";
            DataTable DT = null;
            XmlNodeList XD = null;
            string Group_Col = M["GROUP_COL"];

            string Sub_Select = M["XML_COLSELECT"];
            if (Table_Mode)
            {
                DT = M.Get_Table("SOURCE");
                if (DT == null || DT.Rows.Count == 0) return;
                Last_Row = DT.Rows.Count - 1;
            }
            else
            {
                string Select = M.Universal_Param ("XML_SELECT");
                XmlNode N = Main.Universal_Doc(M.Universal_Param ("XML_SOURCE")).DocumentElement;
                if (Select == "")
                {
                    XD = N.ChildNodes;
                }
                else
                {
                    XD = N.SelectNodes(Select);
                }
                Last_Row = XD.Count -1;
                if (M["SHOWXML"] != "")
                {
                    Logs.Log("Repeater {0}", M.Name);
                    Logs.Log(" Select = {0}, Col_Select={1}", Select, Sub_Select);
                    Logs.Log(" Xml={0}", N.OuterXml);
                }
            }


            base.RenderControl(writer);

            DB_Row Row = null;
            //object[] Row = null;
            int Row_Index = -1;

            Action<int> Load_Xml_Row = (R) =>
            {
                XmlNodeList Sub_Values = Sub_Select == ""? XD[R].ChildNodes:XD[R].SelectNodes(Sub_Select);
                DT = new DataTable();
                int I = 0;
                foreach (XmlNode V in Sub_Values)
                {
                    DT.Columns.Add(string.Format ("C{0}", I));
                    I++;
                }
                List<string> Vals = new List<string>();
                foreach (XmlNode V in Sub_Values)
                {
                    Vals.Add(V.Value);
                }
                Row = new DB_Row (DT.Rows.Add(Vals.ToArray()));
            };

            Action Read_Next_Row = () =>
            {
                Row_Index++;
                if (Table_Mode)
                {
                    Row = new DB_Row (DT.Rows[Row_Index]);
                }
                else
                {
                    Load_Xml_Row(Row_Index);
                }
            };
            Action Read_First_Row = () =>
            {
                if (Table_Mode)
                {
                    Row = new DB_Row (DT.Rows[0]);
                }
                else
                {
                    Load_Xml_Row(0);
                }
            };

            Action<string> Write_Row = (Template) =>
            {
                Func<string, string> Solver = (Param) =>
                {
                    // Un parametre est de la forme {Key[:Format]}
                    if (Param == "") return null;
                    switch (Param)
                    {
                        // {index} retourne l'index de la ligne courante
                        case "index":
                            return Row_Index.ToString();
                        default:
                            {
                                Strings.Cursor C = new Strings.Cursor(Param);
                                string Key = C.Next(":");
                                string Format = C.Tail();
                                if (Format == "")
                                {
                                    return JR.Convert.To_String(Row.Value(Key));
                                }
                                else
                                {
                                    return M.Parent.Lang_Format (Format, Row.Value(Key));
                                }
                            }
                    }
                };
                writer.Write(M.Universal(Template, Late_Translation, Solver));
            };

            Access_Check = JR.Convert.To_Integer(M["ACCESS_ROW"]);
            Func<bool> Access_Ok = () =>
            {
                if (Access_Check > Row.Col_Count) Access_Check = 0;
                if (Access_Check <= 0) return true;
                JR.Saf.Door Door = new JR.Saf.Door(Row.String (Access_Check - 1));
                return Door.Is_Allowed(M.Session.User.Keys);
            };

            string Group_Val = "";
            Read_First_Row();
            if (!Access_Ok()) return;
            if (Init_Template != "") writer.Write(Init_Template);
            if (Header_Template != "")
            {
                if (Group_Col != "")
                {
                    Group_Val = Row[Group_Col];
                    Write_Row(Header_Template);
                }
                else
                {
                    writer.Write(Header_Template);
                }
            }
            if (Title_Template != "")
            {
                Read_Next_Row();
                Write_Row (Title_Template);
                First_Row++;
            }
            if (Bottom_Template != "")
            {
                Last_Row--;
            }

            int Col = 0;
            for (int I = First_Row; I <= Last_Row; I++)
            {
                Read_Next_Row();
                if (!Access_Ok()) continue;
                if (Group_Col != "")
                {
                    string Old_Group_Val = Group_Val;
                    Group_Val = Row[Group_Col];
                    if (Old_Group_Val != Group_Val)
                    {
                        if (Footer_Template != "") Write_Row(Footer_Template);
                        if (Header_Template != "") Write_Row(Header_Template);
                    }
                }
                Write_Row(Item_Template);
                Col++;
                if (Col == Max_Col && I < Last_Row)
                {
                    if (New_Line_Template != "") Write_Row(New_Line_Template);
                    Col = 0;
                };
            }
            if (Bottom_Template != "")
            {
                Read_Next_Row();
                Write_Row(Bottom_Template);
            }
            if (Footer_Template != "")
            {
                if (Group_Col != "")
                {
                    Write_Row(Footer_Template);
                }
                else
                {
                    writer.Write(Footer_Template);
                }
            }
            if (Exit_Template != "") writer.Write(Exit_Template);
        }

        #region I_Module Membres
        public JR.CMS.Module M { get; set; }
        #endregion
    }
}