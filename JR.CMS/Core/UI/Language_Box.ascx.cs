﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;

namespace JR.CMS.Core.UI
{
    public partial class Language_Box : System.Web.UI.UserControl, I_Module
    {
        string Asp_Url;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            string Link_Tpl = M["LINK_TPL"];
            string Image_Tpl = M["IMAGE_TPL"];
            string Head_Tpl = M["HEADER_TPL"];
            string Sep_Tpl = M["SEPARATOR_TPL"];
            string Foot_Tpl = M["FOOTER_TPL"];
            Asp_Url = M["ASP_URL"];

            bool First = true;
            if (Head_Tpl != "")
            {
                LiteralControl L = new LiteralControl(Head_Tpl);
                this.Controls.Add(L);
            }
            foreach (var Lang in Application_State.Current.Languages.Values)
            {
                if (Lang.Key == M.Session.Language) continue;
                if (!First && Sep_Tpl != "")
                {
                    LiteralControl L = new LiteralControl(Sep_Tpl);
                    this.Controls.Add(L);
                }
                First = false;
                if (Link_Tpl != "")
                {
                    LinkButton L = new LinkButton();
                    L.ID = "L_" + Lang.Key;
                    L.Text = Lang.Name;
                    L.Click += new EventHandler(L_Click);
                    L.CausesValidation = false;
                    this.Controls.Add(L);
                }
                if (Image_Tpl != "")
                {
                    ImageButton I = new ImageButton();
                    I.ID = "I_" + Lang.Key;
                    I.ImageUrl = string.Format(Image_Tpl, Lang.Key, Lang.Name);
                    I.Click += new ImageClickEventHandler(I_Click);
                    I.CausesValidation = false;
                    this.Controls.Add(I);
                }
            }
            if (Foot_Tpl != "")
            {
                LiteralControl L = new LiteralControl(Foot_Tpl);
                this.Controls.Add(L);
            }
        }

        void I_Click(object sender, ImageClickEventArgs e)
        {
            L_Click(sender, EventArgs.Empty);
        }

        void L_Click(object sender, EventArgs e)
        {
            string Lang = ((Control)sender).ID;
            Lang = Lang.Substring(Lang.Length - 2);
            M.Session.Force_Language(Lang);
            if (Asp_Url != "")
            {
                M.Session.Call_Hidden_Page(this.Page, string.Format(Asp_Url, M.Session.Language));
            }
            else
            {
                // Remplacer le /FR par /EN par exemple
                // Par construction la langue est toujours placée devant l'URL dans le cas d'une langue explicite
                if (M.Parent.Explicit_Language)
                {
                    string Suffix = Request.Url.PathAndQuery.Substring(3);
                    Response.Redirect(string.Format("~/{0}{1}", Lang, Suffix));
                }
                else
                {
                    // Redemander simplement la page
                    Response.Redirect(Request.Url.AbsoluteUri);
                }
            }
        }

       #region I_Module Membres
        public JR.CMS.Module M {get;set;}
        #endregion
    }
}