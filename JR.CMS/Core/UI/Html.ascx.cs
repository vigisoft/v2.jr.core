﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace JR.CMS.Core.UI
{
    public partial class Html : System.Web.UI.UserControl, I_Module
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Controls.Clear();

            string Include = M.Universal_Param ("INCLUDE");

            string Content = M["CONTENT"];
            if (Include != "")
            {
                Content = Main.Universal_Ressource("CLF", Include) + Content;
            }

            if (!JR.Convert.To_Boolean(M["PLACES"]))
            {
                Controls.Add(new LiteralControl(Content));
                return;
            }

            // Les sous modules sont repérés par [nom] avec nom qui ne comporte que lettres et chiffres
            Strings.Cursor C = new Strings.Cursor(Content);
            Action<string> Add_Literal = (Code) =>
            {
                if (Code == "") return;
                Controls.Add(new LiteralControl(Code));
            };
            Action<string> Add_Place = (Name) =>
            {
                Controls.Add(new PlaceHolder {ID=Name});
            };
            while (!C.End)
            {
                int Start = C.Index;
                string Prefix = C.Next("[");
                string Name = C.Next("]");
                if (C.Found)
                {
                    if (Name.Length > 2 && JR.Strings.Is_Valid_ID (Name))
                    {
                        Add_Literal(Prefix);
                        Add_Place (Name);
                        continue;
                    }
                }
                C.Index = Start;
                Add_Literal(C.Tail());
            }

        }

        public override void RenderControl(HtmlTextWriter writer)
        {
            Strings.Dic Datas = M.Local_Dic;
            string Data = Data = M.Universal_Param("DATA");
            if (Data != "") Datas = new Strings.Dic(Data);

            Func<string, string> Value = (Key) =>
            {
                return Datas[Key];
            };
            bool Solver_Paused = false;
            foreach (Control CC in Controls)
            {
                if (CC is LiteralControl)
                {
                    LiteralControl L = CC as LiteralControl;
                    if (Datas == null || Datas.Is_Empty)
                    {
                        writer.Write(M.Universal(L.Text));
                    }
                    else
                    {
                        Func<string, string> Solver = (Param) =>
                        {
                            // Un parametre est de la forme {Key[:Format]}
                            if (Param == "") return null;
                            if (Param == "--")
                            {
                                Solver_Paused = !Solver_Paused;
                                return "";
                            }
                            if (Solver_Paused) return null;
                            return Value(Param);
                        };
                        writer.Write(M.Universal(L.Text, true, Solver));
                    }
                }
                else
                {
                    PlaceHolder PH = CC as PlaceHolder;
                    if (PH != null)
                    {
                        foreach (Control CCC in PH.Controls)
                        {
                            if (CC.Visible && CCC.Visible) CCC.RenderControl(writer);
                        }
                    }
                    else
                    {
                        if (CC.Visible) CC.RenderControl(writer);
                    }
                }
            }

        }


        #region I_Module Membres
        public JR.CMS.Module M { get; set; }
        #endregion

    }
}