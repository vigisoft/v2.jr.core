﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JR.CMS.Core.UI
{
    public partial class Rotator_Render : System.Web.UI.Page
    {
        protected override void Render(HtmlTextWriter writer)
        {
            C_Rotator.RenderControl(writer);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            C_Rotator.AdvertisementFile = Request["PUB_FILE"];
        }

    }
}