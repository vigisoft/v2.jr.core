﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JR.CMS;
using JR.DB;

namespace JR.CMS.Core.UI
{
    public partial class Razor_Box : System.Web.UI.UserControl, I_Event_Module, I_Module_Include
    {

        // 0= normal, pas dans un update panel
        // 1= le contenu est dans un update panel, il est rafraichi dans tous les cas
        // 2= le contenu est dans un update panel, il n'est rafraichi que sur appel explicite à $update
        int Async_Mode = 0;
        bool Redraw = false;

        protected override void OnLoad(EventArgs e)
        {
            if (M.Is_Include)
            {
                this.Visible = false;
                return;
            }
            Async_Mode = M["$Async"].To_Integer();
            M.No_Async = Async_Mode == 0;  // Les modules razor ne sont pas asynchrones par défaut
            UP_Panel.Visible = Async_Mode > 0;
            L_Content.Visible = Async_Mode == 0;
            var Append_To = M["data-append-to"];
            if (Append_To != "")
            {
                UP_Panel.Attributes["data-append-to"] = Append_To;
            }
            Redraw = !M.In_Async || Async_Mode != 2;
        }

        void Render_Async()
        {
            L_Async_Content.Text = M.Get_Content();
            if (M.Render_Canceled) return;
            UP_Panel.Update();
        }
        protected override void OnPreRender(EventArgs e)
        {
            if (M.Is_Include)
            {
                this.Visible = false;
                return;
            }
            if (!Redraw) return;
            switch (Async_Mode)
            {
                case 2:
                case 1:
                    {
                        Render_Async();
                    }
                    break;
                case 0:
                    {
                        L_Content.Text = M.Get_Content();
                    }
                    break;
            }
        }


        #region I_Module Membres
        public JR.CMS.Module M { get; set; }
        #endregion


        // Retourne le contenu pour un include
        public string Get_Output()
        {
            return M.Get_Content();
        }

        public void Event_Receive(string Event_Name, params object[] Params)
        {
            if (Async_Mode < 2) return;
            switch (Event_Name)
            {
                case "$render":
                    // PAsser uniquement le premier paramètre qui peut être un expendoObject
                    L_Async_Content.Text = M.Render_Partial_Content("", Params.Length == 0?null:Params[0]);
                    if (M.Render_Canceled) return;
                    UP_Panel.Update();
                    break;
                default:
                    Render_Async();
                    break;
            }
            // Les paramètres peuvent être récupéré dans v.Event
            // A ce niveau on est dans le load_complete, donc juste avant les prerenders
        }
    }
}