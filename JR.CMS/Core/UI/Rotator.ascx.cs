﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;

namespace JR.CMS.Core.UI
{
    public partial class Rotator : System.Web.UI.UserControl, I_Module
    {
        protected string Pub_File;
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            string Ref_File = M.Universal_Param ("Ref_File");
            Pub_File = M.Universal_Param("Pub_File");
            string Img_Prefix = M["Img_Prefix"];

            // Si le fichier référence (ancien format est plus récent que le fichier xml
            // Reconstruire le fichier xml

            if (Ref_File != "")
            {
                string Real_Ref = Server.MapPath(Ref_File);
                string Real_Pub = Server.MapPath(Pub_File);
                if (File.GetLastWriteTimeUtc(Real_Ref) > File.GetLastWriteTimeUtc(Real_Pub))
                {
                    string[] Ref = File.ReadAllLines(Real_Ref);
                    int L = 3;  // On saute les 3 premières lignes
                    Func<string> Pop = () =>
                        {
                            if (L >= Ref.Length) return "";
                            L++;
                            return (Ref[L - 1]).Trim();
                        };
                    //
                    //REDIRECT /images_pub/redirect.asp
                    //BORDER 0
                    //*
                    ///images_pub/23_21_arphand.gif
                    //http://www.arphand.net
                    //ARPHAND
                    //20
                    ///images_pub/24_33_gueant.gif
                    //http://www.galart.com/pro/gueant
                    //GUEANT
                    //1
                    //
                    //
                    XmlTextWriter Doc = new XmlTextWriter (Real_Pub, Encoding.Default);
                    Doc.WriteStartElement("Advertisements");
                    while (L < Ref.Length)
                    {
                        string ImageUrl = Pop();
                        string NavigateUrl = Pop();
                        string AlternateText = Pop();
                        int Impressions = JR.Convert.To_Integer (Pop());
                        if (Impressions == 0) break;
                        if (Impressions < 2) continue;
                        Doc.WriteStartElement("Ad");
                        Doc.WriteStartElement("ImageUrl");
                        Doc.WriteString (Img_Prefix + ImageUrl);
                        Doc.WriteEndElement();
                        Doc.WriteStartElement("NavigateUrl");
                        Doc.WriteString (NavigateUrl);
                        Doc.WriteEndElement();
                        Doc.WriteStartElement("AlternateText");
                        Doc.WriteString (AlternateText);
                        Doc.WriteEndElement();
                        Doc.WriteStartElement("Impressions");
                        Doc.WriteValue (Impressions);
                        Doc.WriteEndElement();
                        Doc.WriteEndElement();
                    }
                    Doc.WriteEndElement();
                    Doc.Flush();
                    Doc.Close();
                }
            }
        }
        public Module M { get; set; }
    }
}