﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Razor.ascx.cs" Inherits="JR.CMS.Core.UI.Razor_Box" EnableViewState="false" ViewStateMode="Disabled" %>
<asp:UpdatePanel ID="UP_Panel" runat="server" UpdateMode="Conditional" ClientIDMode="Predictable">
<ContentTemplate>
<asp:Literal runat="server" ID="L_Async_Content" Mode="PassThrough" EnableViewState="False" ViewStateMode="Disabled"></asp:Literal>
</ContentTemplate>
</asp:UpdatePanel>
<asp:Literal runat="server" ID="L_Content" Mode="PassThrough" EnableViewState="False" ViewStateMode="Disabled"></asp:Literal>


