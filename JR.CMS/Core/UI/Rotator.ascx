﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Rotator.ascx.cs" Inherits="JR.CMS.Core.UI.Rotator" %>
<asp:Panel ID="P_Pub" runat="server"></asp:Panel>
<script type="text/javascript">
    jQuery.noConflict();

    (function ($) {
        $(function () {
            $("#<%=P_Pub.ClientID%>").load("/Core/UI/Rotator_Render.aspx", "Pub_File=<%=Pub_File%>&v=" + Math.random() + "");
            setInterval(function () {
                $("#<%=P_Pub.ClientID%>").load("/Core/UI/Rotator_Render.aspx", "Pub_File=<%=Pub_File%>&v=" + Math.random() + "");
        }, <%=M["Tempo"]%>000);
        });
    })(jQuery);
</script>

