﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JR.CMS;
using JR.DB;

namespace JR.CMS.Core.UI
{
    public partial class RazorUpdate_Box : System.Web.UI.UserControl, I_Module
    {
        string Signature = "";
        bool Defer_Parsing = false;
        bool Quite = false;
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (M.New_Local_Dic) M.Compile();
            Defer_Parsing = JR.Convert.To_Boolean(M["$Defer"]);
            Quite = JR.Convert.To_Boolean(M["$Quite"]);
            if (!Defer_Parsing) L_Content.Text = M.Parse(Quite);
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (Defer_Parsing) L_Content.Text = M.Parse(Quite);
            base.OnPreRender(e);
        }


        #region I_Module Membres
        public JR.CMS.Module M { get; set; }
        #endregion

    }
}