﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace JR.CMS.Core.UI
{
    public partial class Collection_Box : System.Web.UI.UserControl, I_Module
    {

        bool Access_Ok(DataRow Row)
        {
            if (Access_Check <= 0) return true;
            JR.Saf.Door Door = new JR.Saf.Door(Row[Access_Check - 1].ToString());
            return Door.Is_Allowed(M.Session.User.Keys);
        }

        bool Sub_Access_Ok(string[] Params)
        {
            if (Sub_Access_Check <= 0) return true;
            if (Sub_Access_Check > Params.Length) return true;
            JR.Saf.Door Door = new JR.Saf.Door(Sub_Access_Check_Prefix + Params[Sub_Access_Check - 1]);
            return Door.Is_Allowed(M.Session.User.Keys);
        }

        string Late_Translate(string Source)
        {
            return Late_Translation ? JR.Saf.Main.Universal_String(Source) : Source;
        }

        int Access_Check = -1;
        int Sub_Access_Check = -1;
        string Sub_Access_Check_Prefix = "";
        bool Late_Translation = false;
        public override void RenderControl(HtmlTextWriter writer)
        {
            Late_Translation = JR.Convert.To_Boolean(M["LATE_TRANS"]);

            // Liste des langues séparées par une ,
            DataTable DT = Main.Universal_Table(M["DEF"]);
            if (DT != null && DT.Rows.Count > 0)
            {
                List<string[]> SL = Session_State.Current.Get_Local_Table(DT, 1);
                foreach (string[] LV in SL)
                {
                    if (LV[0] != "")
                    {
                        M[LV[0]] = LV[1];
                    }
                }
            }

            string Header_Template = M["HEADER_TPL"];
            string Top_Template = M["TOP_TPL"];
            string Bottom_Template = M["BOTTOM_TPL"];
            string Footer_Template = M["FOOTER_TPL"];
            string Chapter_Head_Template = M["CHAP_HEAD_TPL"];
            string Chapter_Foot_Template = M["CHAP_FOOT_TPL"];
            string Next_Chapter_Template = M["NEXT_CHAP_TPL"];
            string Collection_Template = M["COLL_TPL"];
            string Next_Collection_Template = M["NEXT_COLL_TPL"];
            string Next_Line_Template = M["NEXT_LINE_TPL"];
            int Max_Col = JR.Convert.To_Integer(M["MAX_COLUMN"]);
            DT = Main.Universal_Table(M["SOURCE"] + "@CHAPTER");
            if (DT == null || DT.Rows.Count == 0) return;
            List<string[]> XT = Session_State.Current.Get_Local_Table(DT, 1);
            int First_Row = 0;
            int Last_Row = XT.Count - 1;
            base.RenderControl(writer);
            Access_Check = JR.Convert.To_Integer(M["ACCESS_ROW"]);
            Sub_Access_Check = JR.Convert.To_Integer(M["SUB_ACCESS_ROW"]);
            Sub_Access_Check_Prefix = M["SUB_ACCESS_PREFIX"];
            if (Access_Check > XT.Count) Access_Check = 0;
            if (!Access_Ok(DT.Rows[0])) return;
            if (Header_Template != "")
            {
                writer.Write(Late_Translate(Header_Template));
            }
            if (Top_Template != "")
            {
                writer.Write(Late_Translate(string.Format(Top_Template, XT[0])));
                First_Row++;
            }
            if (Bottom_Template != "")
            {
                Bottom_Template = Late_Translate(string.Format(Bottom_Template, XT[Last_Row]));
                Last_Row--;
            }
            for (int I = First_Row; I <= Last_Row; I++)
            {
                if (!Access_Ok(DT.Rows[I])) continue;
                writer.Write(Late_Translate(string.Format(Chapter_Head_Template, XT[I])));
                DataTable CT = Main.Universal_Table(M["SOURCE"] + "@" + XT[I][0]);
                if (CT == null || CT.Rows.Count == 0) continue;
                List<string[]> CL = Session_State.Current.Get_Local_Table(CT, 2);
                int Col = 0;
                for (int C = 0; C < CL.Count; C++)
                {
                    string[] Params = new string[XT[I].Length + CL[C].Length];
                    XT[I].CopyTo(Params, 0);
                    CL[C].CopyTo(Params, XT[I].Length);
                    if (!Sub_Access_Ok(Params)) continue;
                    writer.Write(Late_Translate(string.Format(Collection_Template, Params)));
                    if (Next_Collection_Template != "" && C != CL.Count) writer.Write(Next_Collection_Template);
                    Col++;
                    if (Col == Max_Col && C < CL.Count - 1)
                    {
                        if (Next_Line_Template != "") writer.Write(Next_Line_Template);
                        Col = 0;
                    };
                }
                writer.Write(Late_Translate(string.Format(Chapter_Foot_Template, XT[I])));
                if (Next_Chapter_Template != "" && I != Last_Row) writer.Write(Next_Chapter_Template);
            }
            if (Bottom_Template != "")
            {
                writer.Write(Bottom_Template);
            }
            if (Footer_Template != "")
            {
                writer.Write(Late_Translate(Footer_Template));
            }
        }



        #region I_Module Membres
        public JR.CMS.Module M { get; set; }
        #endregion
    }
}