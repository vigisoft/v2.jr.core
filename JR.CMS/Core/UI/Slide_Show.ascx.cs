﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JR.DB;
using System.IO;

namespace JR.CMS.Core.UI
{
    public partial class Slide_Show : System.Web.UI.UserControl, I_Module, I_Module_Include
    {
        const string Script_3 = @"
    		$(function(){{
			$('#{TS_ID}').slides({{
				preload: true,
				preloadImage: '/img/loading.gif',
                generatePagination: false,
                crossfade:true,
                container:'{TS_ID}_container',
                effect:'slide,fade',
				play: 7000,
                fadeSpeed:800,
                {TS_OPT}
			}});
		}});";

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!IsPostBack)
            {
                ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "slides.inc", "/Core/Scripts/slides.min.jquery.js");
                string Script = JR.Strings.Generic_Text(Script_3, "TS_ID", M.Id, "TS_OPT", M["Options"]);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), M.Id + "slides.function", Script, true);
            }
        }

        public override void RenderControl(HtmlTextWriter writer)
        {
            if (M.Is_Include) return;
            base.RenderControl(writer);
            Render_Control(writer);
        }

        void Render_Control(HtmlTextWriter writer)
        {

            int Access_Check = -1;
            bool Late_Translation = JR.Convert.To_Boolean(M["LATE_TRANS"]);

            Func<DataRow, bool> Access_Ok = (Row) =>
            {
                if (Access_Check <= 0) return true;
                JR.Saf.Door Door = new JR.Saf.Door(Row[Access_Check - 1].ToString());
                return Door.Is_Allowed(M.Session.User.Keys);
            };
            Func<string, string> Late_Translate = (Source) =>
            {
                return Late_Translation ? M.Universal(Source) : Source;
            };

            DataTable DT = M.Get_Table ("SOURCE");
            if (DT == null || DT.Rows.Count == 0) return;

            Action<string, int> Write_Row = (Template, Row_Index) =>
            {
                DB_Row Row = new DB_Row(DT.Rows[Row_Index]);
                Func<string, string> Solver = (Param) =>
                {
                    // Un parametre est de la forme {Key[:Format]}
                    if (Param == "") return null;
                    switch (Param)
                    {
                        // {index} retourne l'index de la ligne courante
                        case "index":
                            return Row_Index.ToString();
                        default:
                            {
                                Strings.Cursor C = new Strings.Cursor(Param);
                                string Key = C.Next(":");
                                string Format = C.Tail();
                                if (Format == "")
                                {
                                    return JR.Convert.To_String(Row.Value(Key));
                                }
                                else
                                {
                                    return string.Format("{0:" + Format + "}", Row.Value(Key));
                                }
                            }
                    }
                };
                writer.Write(M.View.Universal(Template, Late_Translation, M, Solver));
            };


            
            int Last_Row = DT.Rows.Count - 1;
            int First_Row = 0;
            Access_Check = JR.Convert.To_Integer(M["ACCESS_ROW"]);
            if (Access_Check > DT.Columns.Count) Access_Check = 0;
            if (!Access_Ok(DT.Rows[0])) return;
            writer.Write("<div class=\"" + M.Id + "_container" + "\">");
            string Item_Template = M.Universal_Param ("TPL");
            int Col = 0;
            for (int I = First_Row; I <= Last_Row; I++)
            {
                if (!Access_Ok(DT.Rows[I])) continue;
                Write_Row(Item_Template, I);
                Col++;
            }
            writer.Write("</div>");
        }


        #region I_Module Membres
        public JR.CMS.Module M {get;set;}
        #endregion


        public string Get_Output()
        {
            using (var SW = new StringWriter())
            {
                using (HtmlTextWriter HTW = new HtmlTextWriter(SW))
                {
                    Render_Control(HTW);
                    return SW.ToString();
                }
            }
        }
    }
}