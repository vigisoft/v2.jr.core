﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;

namespace JR.CMS.Core.UI
{
    public partial class Logo_Box : System.Web.UI.UserControl, I_Module
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            I_Logo.Alt = M["ALT_TEXT"];
            I_Logo.Src = I_Logo.ResolveUrl(M.Universal_Param("IMAGE_URL"));
        }

        #region I_Module Membres
        public JR.CMS.Module M { get; set; }
        #endregion
    }
}