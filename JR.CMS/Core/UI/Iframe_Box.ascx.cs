﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JR.CMS.Core.UI
{
    public partial class Iframe_Box : System.Web.UI.UserControl, I_Module
    {

        protected string frame_name;
        protected void Page_Init(object sender, EventArgs e)
        {
            frame_name = C_Frame.ClientID;
            string Url = M.Universal(M["URL"]);
            if (!Url.ToLower().StartsWith("http"))
            {
                Url = VirtualPathUtility.MakeRelative
                     (Request.AppRelativeCurrentExecutionFilePath, Url);
            }
            C_Frame.Attributes["src"] = Url;
        }

        public override void RenderControl(HtmlTextWriter writer)
        {
            base.RenderControl(writer);
            string Hidden_Frame = M["HIDDEN_FRAME"];
            if (Hidden_Frame != "")
            {
                Response.Write(string.Format("<iframe id=\"{0}\" name=\"{0}\" style=\"display:none\"></iframe>", Hidden_Frame));
            }
        }

        #region I_Module Membres
        public JR.CMS.Module M { get; set; }
        #endregion
    }
}