using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Forms;

namespace JR.WUI
{
    public class List_Item<T>
    {
        string _Name;
        T _Id;
        public List_Item(string Name, T Id)
        {
            _Name = Name;
            _Id = Id;
        }
        public override string ToString()
        {
            return _Name;
        }
        public T Id
        {
            get { return _Id; }
        }
        public static T Get_Id(object Item)
        {
            if (Item == null) return default(T);
            if (Item is List_Item<T>) return (Item as List_Item<T>).Id;
            return default(T);
        }
    }

    public class List_Items<T>
    {
        List<List_Item<T>> _List = new List<List_Item<T>>();
        public void Add(string Name, T Id)
        {
            _List.Add(new List_Item<T>(Name, Id));
        }

        public int Count
        {
            get { return _List.Count; }
        }
        public object[] Content
        {
            get { return _List.ToArray(); }
        }

        public int Get_Row(T Id)
        {
            int Row = 0;
            foreach (List_Item<T> I in _List)
            {
                if (I.Id.Equals(Id)) return Row;
                Row++;
            }
            return 0;
        }
    }

    public static class List_View_Helper
    {
        public static void Prepare(ListView LV, bool Autosize, bool Sort)
        {
            if (Sort)
            {
                LV.ListViewItemSorter = new ListViewItemSorter(LV);
            }
            if (Autosize && (LV.Items.Count > 0))
            {
                for (int I = 1; I < LV.Columns.Count; I++)
                {
                    if (LV.Columns[I].Type == ListViewColumnType.Text)
                    {
                        int Init_Size = LV.Columns[I].Width;
                        LV.AutoResizeColumn(I, ColumnHeaderAutoResizeStyle.ColumnContent);
                        int New_Size = LV.Columns[I].Width;
                        if (New_Size < Init_Size) LV.Columns[I].Width = Init_Size;
                    }
                }
            }
        }
    }
}
