using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace JR.Win
{

    public interface I_List_Item
    {
        string Name {get;}
        bool Checked { get; set; }
    }


    public class List_Item<T>:I_List_Item
    {
        string _Name;
        T _Id;
        public List_Item(string Name, T Id)
        {
            _Name = Name;
            _Id = Id;
            Checked = false;
        }
        public override string ToString()
        {
            return _Name;
        }
        public string Name
        {
            get
            {
                return _Name;
            }
        }
        public bool Checked { get; set; }
        public T Id
        {
            get { return _Id; }
        }
        public static T Get_Id(object Item)
        {
            if (Item == null) return default(T);
            if (Item is List_Item<T>) return (Item as List_Item<T>).Id;
            return default(T);
        }
        public static string Get_Name(object Item)
        {
            if (Item == null) return "";
            if (Item is List_Item<T>) return (Item as List_Item<T>).Name;
            return "";
        }
        public static int Get_Index(IEnumerable Collection, T Id, int Default_Index = -1)
        {
            int I = 0;
            foreach (var Item in Collection)
            {
                List_Item<T> LI = Item as List_Item<T>;
                if (LI != null && (LI.Id != null && LI.Id.Equals(Id) || Id == null && LI.Id == null)) return I;
                I++;
            }
            return Default_Index;
        }

    }

    public class List_Items<T>
    {
        List<List_Item<T>> _List = new List<List_Item<T>>();
        public void Add(string Name, T Id)
        {
            _List.Add(new List_Item<T>(Name, Id));
        }

        public void Add(List_Item<T> Item)
        {
            _List.Add(Item);
        }

        public int Count
        {
            get { return _List.Count; }
        }
        public object[] Content
        {
            get { return _List.ToArray(); }
        }

        public int Get_Row(T Id)
        {
            int Row = 0;
            foreach (List_Item<T> I in _List)
            {
                if (I.Id.Equals(Id)) return Row;
                Row++;
            }
            return 0;
        }

        public List_Item<T> Get_Item(T Id)
        {
            return _List[Get_Row(Id)];
        }

        public static List_Items<T> From_Items(IEnumerable List)
        {
            List_Items<T> L = new List_Items<T>();
            foreach (var I in List)
            {
                L.Add(I as List_Item<T>);
            }
            return L;
        }
        public static List_Items<string> From_Enums (JR.Enums Enums)
        {
            List_Items<string> L = new List_Items<string>();
            foreach (var I in Enums.List)
            {
                L.Add(new List_Item<string>(I.Lib_Fr, I.Id));
            }
            return L;
        }
    }

    public static class Item_Helper
    {
        public static void Add_Item<T>(this CheckedListBox CB, string Name, T Id) => CB.Items.Add(new List_Item<T>(Name, Id));
        public static void Add_Item<T>(this ComboBox CB, string Name, T Id) => CB.Items.Add(new List_Item<T>(Name, Id));
        public static void Sel_Item<T>(this ComboBox CB, T Id, int Default_Index = -1) => CB.SelectedIndex = List_Item<T>.Get_Index(CB.Items, Id, Default_Index);
        public static T Get_Sel<T>(this ComboBox CB) => List_Item<T>.Get_Id(CB.SelectedItem);
        public static void Read_Sel<T>(this ComboBox CB, out T Sel) => Sel = List_Item<T>.Get_Id(CB.SelectedItem);

    }
}


