using System;
using System.Collections.Generic;

namespace JR.Win
{
	/// <summary>
	/// gestion de l'utilisateur du portail
	/// </summary>
	public class User_State
	{
        Session_State _Session = null;
		public User_State (Session_State Session)
		{
            _Session = Session;
		}

    	public static User_State Current
		{
            get
            {
                return Session_State.Current.User;
            }
		}

        public Session_State Session
        {
            get { return _Session; }
        }

        bool _Connected = false;
        public bool Connected
        {
            get { return _Connected; }
        }

        public void Connect(string Name, string Uid)
        {
            _Name = Name;
            _Uid = Uid;
            _Connected = true;
        }

        public void Disconnect()
        {
            _Name = "?";
            _Uid = "";
            _Connected = false;
        }

        protected string _Name = "?";
        public string Name
        {
            get { return _Name; }
        }

        static char[] Dots = new char[] { ' ', ',' };
        public string Revert_Name
        {
            get
            {
                int Dot = _Name.IndexOfAny(Dots);
                if (Dot < 1) return _Name;
                return _Name.Substring(Dot + 1) + _Name.Substring(0, Dot); 
            }
        }

        string _Uid = "";
        public string Uid
        {
            get { return _Uid; }
        }

        JR.Saf.Keys _Keys = new JR.Saf.Keys();
        public void Append_Key(string Key, char Status)
        {
            _Keys.Append_Key(Key, Status);
        }

        public JR.Saf.Keys Keys
        {
            get { return _Keys; }
        }
        public void Init_Keys()
        {
            _Keys.Init_Keys();
        }

        public void Remove_Key(string Key)
        {
            _Keys.Remove_Key(Key);
        }

        public bool Has_Key(string Key, char Status)
        {
            return _Keys.Has_Key(Key, Status);
        }
    }
}
