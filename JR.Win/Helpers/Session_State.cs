using System;
using System.Collections.Generic;

namespace JR.Win
{
	/// <summary>
	/// Classe de gestion de la page principale (portail)
	/// </summary>

	public class Session_State
	{
		public Session_State ()
		{
            _Current = this;
        }

        static Session_State _Current;
		public static Session_State Current
		{
            get
            {
                if (_Current == null) _Current = new Session_State ();
                return _Current;
            }
		}

        // M�thode � sp�cialiser pour retourner un utilisateur
        public virtual User_State On_New_User(Session_State Session)
        {
            return new User_State(Session);
        }

        User_State _User = null;	// Utilisateur courant
        public User_State User
		{
			get
            {
                if (_User == null)
                {
                    _User = On_New_User(this);
                }
                return _User;
            }
		}

		// Ressources de la session, accessible sous forme d'un indexer
		Dictionary<string, object> _Depot = new Dictionary<string,object>();
        public object this[string Index]
        {
            get
            {
                object Value = null;
                _Depot.TryGetValue(Index, out Value);
                return Value;
            }

            set
            {
                if (value == null)
                {
                    _Depot.Remove(Index);
                    return;
                }
                _Depot [Index] = value;
            }
        }


        // Gestion de la langue
        string _Language = "FR";
        public string Language
        {
            get { return _Language; }
            set { _Language = value.ToUpper(); }
        }


        // Retourne une chaine UI localis�e
        public string Local_Ui(string Key)
        {
            return Main.Get_String(Language, "UI", Key);
        }

	}
}
