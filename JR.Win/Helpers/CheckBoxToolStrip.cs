﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JR.Win.Helpers
{
    public partial class CheckBoxToolStrip : ToolStripControlHost
    {
        public CheckBoxToolStrip()
            : base(new CheckBox())
        {
        }
    }
}
