﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JR.Win.Controls
{
    public partial class Inch_Box : UserControl
    {
        public Inch_Box()
        {
            InitializeComponent();
        }

        bool Bound = false;
        List<NumericUpDown> Lcm = new List<NumericUpDown>();
        List<NumericUpDown> Linch = new List<NumericUpDown>();
        public void Bind(params NumericUpDown[] Fields)
        {
            if (Bound) return;
            Bound = true;
            foreach (var C in Fields)
            {
                Lcm.Add(C);
                NumericUpDown N = new NumericUpDown();
                N.Anchor = C.Anchor;
                N.Dock = C.Dock;
                N.Width = C.Width;
                N.Height = C.Height;
                N.Top = C.Top;
                N.Left = C.Left;
                N.Minimum = C.Minimum;
                N.Maximum = C.Maximum;
                N.DecimalPlaces = C.DecimalPlaces;
                Linch.Add(N);
                N.Visible = false;
                C.Parent.Controls.Add(N);
                N.ValueChanged += N_ValueChanged;
                C.ValueChanged += C_ValueChanged;
            }
        }

        void Adjust()
        {
            for (int I=0; I< Lcm.Count;I++)
            {
                Lcm[I].Visible = R_cm.Checked;
                Linch[I].Visible = !R_cm.Checked;
            }
        }

        void N_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown N = sender as NumericUpDown;
            for (int I = 0; I < Linch.Count; I++)
            {
                if (N == Linch[I])
                {
                    decimal V = N.Value * 2.54m;
                    Lcm[I].Value = V;
                };
            }
        }

        void C_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown C = sender as NumericUpDown;
            for (int I = 0; I < Lcm.Count; I++)
            {
                if (C == Lcm[I])
                {
                    decimal V = C.Value / 2.54m;
                    Linch[I].Value = V;
                };
            }
        }

        private void R_cm_CheckedChanged(object sender, EventArgs e)
        {
            Adjust();
        }

    }
}
