﻿namespace JR.Win
{
    partial class Pin_Box
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Pin_Box));
            this.B = new System.Windows.Forms.Button();
            this.Images = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // B
            // 
            this.B.FlatAppearance.BorderSize = 0;
            this.B.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.B.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.B.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.B.Location = new System.Drawing.Point(-1, 1);
            this.B.Name = "B";
            this.B.Size = new System.Drawing.Size(16, 16);
            this.B.TabIndex = 0;
            this.B.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.B.UseVisualStyleBackColor = true;
            this.B.Click += new System.EventHandler(this.B_Click);
            // 
            // Images
            // 
            this.Images.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Images.ImageStream")));
            this.Images.TransparentColor = System.Drawing.Color.Transparent;
            this.Images.Images.SetKeyName(0, "B_Cancel.Image.png");
            this.Images.Images.SetKeyName(1, "B_Close_Image.png");
            // 
            // Pin_Box
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.B);
            this.Name = "Pin_Box";
            this.Size = new System.Drawing.Size(16, 16);
            this.Load += new System.EventHandler(this.Pin_Box_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button B;
        private System.Windows.Forms.ImageList Images;
    }
}
