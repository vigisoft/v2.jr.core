﻿namespace JR.Win
{
    partial class Item_Box
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Item_Box));
            this.C_Item = new System.Windows.Forms.FlowLayoutPanel();
            this.B_Edit = new System.Windows.Forms.Button();
            this.C_Item.SuspendLayout();
            this.SuspendLayout();
            // 
            // C_Item
            // 
            this.C_Item.AutoScroll = true;
            this.C_Item.BackColor = System.Drawing.SystemColors.Window;
            this.C_Item.Controls.Add(this.B_Edit);
            this.C_Item.Dock = System.Windows.Forms.DockStyle.Fill;
            this.C_Item.Location = new System.Drawing.Point(0, 0);
            this.C_Item.Name = "C_Item";
            this.C_Item.Size = new System.Drawing.Size(493, 205);
            this.C_Item.TabIndex = 0;
            // 
            // B_Edit
            // 
            this.B_Edit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.B_Edit.Image = ((System.Drawing.Image)(resources.GetObject("B_Edit.Image")));
            this.B_Edit.Location = new System.Drawing.Point(0, 0);
            this.B_Edit.Margin = new System.Windows.Forms.Padding(0);
            this.B_Edit.Name = "B_Edit";
            this.B_Edit.Size = new System.Drawing.Size(28, 21);
            this.B_Edit.TabIndex = 0;
            this.B_Edit.UseVisualStyleBackColor = true;
            this.B_Edit.Click += new System.EventHandler(this.B_Edit_Click);
            // 
            // Item_Box
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.C_Item);
            this.Name = "Item_Box";
            this.Size = new System.Drawing.Size(493, 205);
            this.C_Item.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel C_Item;
        private System.Windows.Forms.Button B_Edit;
    }
}
