﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Globalization;

namespace JR.Win
{
    public class Num_Box : System.Windows.Forms.TextBox
    {
        protected override void OnTextChanged(EventArgs e)
        {
            Check_Num();
        }

        void Check_Num()
        {
            var V = JR.Convert.To_Decimal(Text.Trim(), out bool Error);
            if (Error)
            {
                ForeColor = Color.Red;
            }
            else
            {
                ForeColor = Parent.ForeColor;
                if (V != Prev_Value)
                {
                    Prev_Value = V;
                    ValueChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        public string Format { get; set; } = "#0.#";
        public bool Zero_Empty = true;

        public bool Is_Zero => Text.To_Float() == 0;
        public bool In_Error = false;

        //double Prev_Value = 0;
        decimal Prev_Value = 0;
        //public double Value
        //{
        //    get { return Text.To_Float(); }
        //    set
        //    {
        //        Prev_Value = value;
        //        Text = value == 0.0 ? "" : value.ToString(Format, CultureInfo.InvariantCulture);
        //    }
        //}
        public decimal Value
        {
            get { return Text.To_Decimal(); }
            set
            {
                Prev_Value = value;
                Text = value == 0 ? "" : value.ToString(Format, CultureInfo.InvariantCulture);
            }
        }

        public event EventHandler ValueChanged;

    }
}
