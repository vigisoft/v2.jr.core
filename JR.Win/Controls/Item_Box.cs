﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JR.Win
{
    public partial class Item_Box : UserControl,INotifyPropertyChanged
    {
        public Item_Box()
        {
            Read_Only = false;
            InitializeComponent();
        }

        public List<I_List_Item> List = new List<I_List_Item>();

        public IList<I_List_Item> Checked_List
        {
            get 
            {
                return List.Where(I => I.Checked == true).ToList<I_List_Item>();
            }
        }

        public bool Read_Only {get;set;}

        public void Change ()
        {
            if (PropertyChanged == null) return;
            PropertyChanged(this, new PropertyChangedEventArgs("VALUE"));
        }

        public void Edit()
        {
            Item_Selector IS = new Item_Selector { List=List};
            if (IS.ShowDialog() != DialogResult.OK) return;
            Display();
            Change();
        }

        Control New_Label (I_List_Item Item)
        {
            Label L = new Label();
            L.AutoSize = true;
            L.BackColor = System.Drawing.SystemColors.ControlLightLight;
            L.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            L.Margin = new System.Windows.Forms.Padding(3);
            L.Text = Item.ToString();
            L.Tag = Item;
            return L;
        }

        public void Display()
        {
            List<Control> Preserved = new List<Control>();
            foreach (Control C in C_Item.Controls)
            {
                if (!(C is Label)) Preserved.Add(C);
                C.Visible = !Read_Only;
            }
            C_Item.Controls.Clear();
            foreach (var V in List)
            {
                if (!V.Checked) continue;
                C_Item.Controls.Add(New_Label (V));
            }
            C_Item.Controls.AddRange(Preserved.ToArray());
        }

        private void B_Edit_Click(object sender, EventArgs e)
        {
            if (Read_Only) return;
            Edit();
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
