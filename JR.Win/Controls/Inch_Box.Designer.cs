﻿namespace JR.Win.Controls
{
    partial class Inch_Box
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.R_inch = new System.Windows.Forms.RadioButton();
            this.R_cm = new System.Windows.Forms.RadioButton();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // R_inch
            // 
            this.R_inch.AutoSize = true;
            this.R_inch.Location = new System.Drawing.Point(48, 3);
            this.R_inch.Name = "R_inch";
            this.R_inch.Size = new System.Drawing.Size(45, 17);
            this.R_inch.TabIndex = 287;
            this.R_inch.Text = "inch";
            this.R_inch.UseVisualStyleBackColor = true;
            this.R_inch.CheckedChanged += new System.EventHandler(this.R_cm_CheckedChanged);
            // 
            // R_cm
            // 
            this.R_cm.AutoSize = true;
            this.R_cm.Checked = true;
            this.R_cm.Location = new System.Drawing.Point(3, 3);
            this.R_cm.Name = "R_cm";
            this.R_cm.Size = new System.Drawing.Size(39, 17);
            this.R_cm.TabIndex = 286;
            this.R_cm.TabStop = true;
            this.R_cm.Text = "cm";
            this.R_cm.UseVisualStyleBackColor = true;
            this.R_cm.CheckedChanged += new System.EventHandler(this.R_cm_CheckedChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.R_cm);
            this.flowLayoutPanel1.Controls.Add(this.R_inch);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(109, 39);
            this.flowLayoutPanel1.TabIndex = 288;
            // 
            // Inch_Box
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "Inch_Box";
            this.Size = new System.Drawing.Size(109, 39);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton R_inch;
        private System.Windows.Forms.RadioButton R_cm;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}
