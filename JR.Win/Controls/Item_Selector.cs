﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JR.Win
{
    public partial class Item_Selector : Form
    {
        public Item_Selector()
        {
            InitializeComponent();
        }

        public List<I_List_Item> List { get; set; }

        private void B_Ok_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            foreach (CheckBox CB in Panel.Controls)
            {
                I_List_Item E = CB.Tag as I_List_Item;
                if (E.Checked == CB.Checked) continue;
                E.Checked = CB.Checked;
                DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            Close();
        }

        private void B_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Item_Selector_Load(object sender, EventArgs e)
        {
            List<Control> CBs = new List<Control>();
            foreach (bool Checked in new bool [] {true,false})
            {
                foreach (var E in List)
                {
                    if (E.Checked != Checked) continue;
                    CheckBox CB = new CheckBox();
                    CB.Text = E.Name;
                    CB.Tag = E;
                    CB.Checked = E.Checked;
                    CB.Dock = DockStyle.Top;
                    CBs.Add(CB);
                }
            }
            CBs.Reverse(); // Inverser car le TOP inverse
            Panel.Controls.AddRange(CBs.ToArray());
        }
    }
}
