﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JR.Win
{
    public partial class Pin_Box : UserControl
    {
        public Pin_Box()
        {
            InitializeComponent();
        }

        bool _Pined = false;
        public bool Pined
        {
            get
            {
                return _Pined;
            }
            set
            {
                if (_Pined == value) return;
                Refresh_State(value);
            }
        }

        private void B_Click(object sender, EventArgs e)
        {
            Refresh_State(!_Pined);
        }

        private void Pin_Box_Load(object sender, EventArgs e)
        {
            Refresh_State(_Pined);
        }

        private void Refresh_State(bool State)
        {
            bool Old_Pined = _Pined;
            _Pined = State;
            B.ImageIndex = _Pined ? 1 : 0;
            if (Pined_Change != null && Old_Pined != _Pined)
            {
                Pined_Change(this, EventArgs.Empty);
            }
        }

        public event EventHandler Pined_Change;

    }
}
