﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace JR.Win
{
    public class TextBox : System.Windows.Forms.TextBox
    {
        protected override void OnKeyDown(System.Windows.Forms.KeyEventArgs e)
        {
            if (e.Control && (e.KeyCode == System.Windows.Forms.Keys.A))
            {
                this.SelectAll();
                e.SuppressKeyPress = true;
                e.Handled = true;
            }
            else
                base.OnKeyDown(e);
        }

        Dictionary<string, string> Texts = new Dictionary<string,string>();
        string Selected_Lang = "";
        string Default_Lang = "";

        // Le texte s'affiche en blanc si il est égal à celui de la langue par défaut
        public void Set_Value(string Lang, string Value)
        {
            // Stocker la langue par défaut (la première écrite)
            if (Default_Lang == "") Default_Lang = Lang;

            // Si /= de langue par défaut et texte identique mettre à blanc
            if (Lang != Default_Lang && Value == Texts[Default_Lang])
            {
                Value = "";
            }
            Texts[Lang] = Value;
            if (Selected_Lang == "") Sel_Value(Lang);

            // Afficher la valeur
            if (Lang == Selected_Lang) Text = Value;
        }

        // retourne le texte si pas langue par défaut et valeur vide, retourner la valeur de la langue par défaut
        public string Get_Value (string Lang)
        {
            // Lire le texte affiché
            string Current_Text = (Lang == Selected_Lang)? Text:Texts[Lang];

            // Langue par défaut le retourner inchangé
            if (Lang == Default_Lang) return Current_Text;

            // Si vide retourner le texte par défaut
            if (Current_Text == "") return (Default_Lang == Selected_Lang)? Text:Texts[Default_Lang];
            return Current_Text;
        }

        public void Sel_Value(string Lang)
        {
            if (Lang == Selected_Lang) return;
            if (Selected_Lang != "") Texts[Selected_Lang] = Text;
            Text = Texts[Lang];
            Selected_Lang = Lang;
            switch (Selected_Lang)
            {
                case "EN":
                    BackColor = Color.LightYellow;
                    break;
                case "FR":
                    BackColor = Color.LightCyan;
                    break;
                case "CN":
                    BackColor = Color.LavenderBlush;
                    break;
            }
        }

    }
}
