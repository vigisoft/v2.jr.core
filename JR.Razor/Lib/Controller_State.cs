﻿using System;
using System.Collections.Generic;
using System.Text;
using JR;
using JR.Saf;
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// Surcharge module
/// </summary>

namespace JR.Web
{
    public class Controller_State : Controller
    {

        Session_State _WS = null;
        public Session_State WS
        {
            get
            {
                if (_WS == null) _WS = Session_State.From_Context(HttpContext);
                return _WS;
            }
        }
        public Script_Builder S = new Script_Builder();

        public string Result(bool Return_State = true, bool Reload = false)
        {
            if (Reload) S.Reload();
            if (Return_State) S.Call("Set_User_State", WS.Ticks_Cookie);
            return S.ToString();
        }


        public string Error_Class = "input-error";
        public bool Error => Error_Fields.Count > 0;
        HashSet<string> Error_Fields = new HashSet<string>();
        Script_Builder Err_SB = new Script_Builder();

        void Show_Error(string C, bool In_Error)
        {
            Err_SB.Set_Class(C, Error_Class, In_Error);
            if (In_Error) Error_Fields.Add(C);
        }

        void Show_Text(string E, string Text) => Err_SB.Set_Text(E, Text);

        protected bool Set_Error(string E, string C, string Key, bool In_Error) => Set_Error(E, C, In_Error ? WS.Local_Ui(Key) : "");

        protected bool Set_Error(string E, string C, string Message = "")
        {
            bool In_Error = Message.Length > 0;
            if (Error_Fields.Contains(C)) return In_Error;
            Show_Error(C, In_Error);
            Show_Text(E, Message);
            return In_Error;
        }

        protected bool Set_Error(string C, bool In_Error)
        {
            if (Error_Fields.Contains(C)) return In_Error;
            Show_Error(C, In_Error);
            return In_Error;
        }

        protected bool Show_Errors()
        {
            S.Exec(Err_SB.ToString());
            return Error;
        }

        public string Ui(string Key, params object[] Params)
        {
            if (Params.Length > 0)
            {
                return string.Format(WS.Local_Ui(Key), Params);
            }
            return WS.Local_Ui(Key);
        }

        protected bool Set_Spam_Error(string C_Err, string Key)
        {
            if (Set_Error(C_Err, C_Err, WS.Next_Send > DateTime.UtcNow ? string.Format(WS.Local_Ui(Key), 5) : ""))
            {
                WS.Next_Send = DateTime.UtcNow.AddSeconds(5);
                return true;
            }
            return false;
        }

        protected void Protect_Send()
        {
            WS.Next_Send = DateTime.UtcNow.AddSeconds(5);
        }

        public string URL(string Page, bool Canonical = false) => Application_State.URL(WS.Language, Page, Canonical);

    }

}