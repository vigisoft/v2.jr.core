﻿using JR.Saf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace JR.Web
{
    public class Application_State
    {

        public static Func<Session_State> New_Session = () => new Session_State();
        public static Func<Session_State, User_State> New_User = (S) => new User_State(S);
        public static Func<View_State> New_View = () => new View_State();

        public static bool Send_Mail(JR.Mail.I_Mail M) => Send_Mail(M.To, M.From, M.Sender, M.Subject, M.Body, M.Attachments.ToArray());

        public static bool Send_Mail(string To, string From, string Sender, string Subject, string Body, params string[] Attachments)
        {
            try
            {
                using (var Mail = new MailMessage())
                {
                    Mail.Subject = Subject;
                    Mail.Body = Body;
                    if (Body.IndexOf('<') < 3 && Body.LastIndexOf('>') > Body.Length-3)
                    {
                        Mail.IsBodyHtml = true;
                        Mail.BodyEncoding = System.Text.Encoding.Default;
                    }
                    else
                    {
                        Mail.IsBodyHtml = false;
                        Mail.BodyEncoding = System.Text.Encoding.Default;
                    }
                    Mail.To.Add(To);
                    Mail.From = new MailAddress(From);
                    if (Sender != null)
                    {
                        Mail.Sender = new MailAddress(Sender);
                    }
                    else
                    {
                        Mail.Sender = Mail.From;
                    }
                    foreach (string File in Attachments)
                    {
                        Mail.Attachments.Add(new Attachment(File));
                    }
                    return Application_State.Send(Mail);
                }
            }
            catch
            {
                return false;
            }
        }

        public static Ref_Parameter SMTP_Server = new Ref_Parameter("SMTP_Server");

        // Envoi d'un mail avec un flag qui indique en retour si ca s'est bien passé
        public static bool Send(System.Net.Mail.MailMessage Mail)
        {
            try
            {
                string Mail_Redirection = JR.Saf.Main.Parameter("Mail_Redirection");
                if (Mail_Redirection != "")
                {
                    MailAddress Addr = new MailAddress(Mail_Redirection, Mail.To.ToString().Replace("@", "_AT_"));
                    Mail.To.Clear();
                    Mail.To.Add(Addr);
                }
                using (var Client = new System.Net.Mail.SmtpClient(SMTP_Server.Value))
                {
                    Client.Send(Mail);
                }
                return true;
            }
            catch
            {
            }
            return false;

        }

        public class Dic_File
        {
            DateTime Date = DateTime.MinValue;
            string File_Name;
            public void Check()
            {
                var D = JR.Files.Safe_Date(File_Name);
                if (Date == D) return;
                JR.Saf.Main.Dico.Load_Xls_Dictionary(File_Name);
                Date = D;
            }
            public Dic_File(string File)
            {
                File_Name = JR.Saf.Main.Real_File_Name(File);
                Check();
            }
        }
        static List<Dic_File> Dics = new List<Dic_File>();

        public static void Register_Dic(string File_Name)
        {
            Dics.Add(new Dic_File(File_Name));
        }

        public static void Reload_Dics()
        {
            lock (Dics)
            {
                foreach (var D in Dics)
                {
                    D.Check();
                }
            }
        }

        public static DateTime Reconf_Version = DateTime.Now;

        public static bool Check_Reconf(ref DateTime Current_Version, Action Check)
        {
            if (Current_Version == Reconf_Version) return false;
            Check();
            Current_Version = Reconf_Version;
            return true;
        }

        static Dictionary<string, Action> Clearables = new Dictionary<string, Action>();
        public static void Register_Clearable(string Key, Action A) => Clearables[Key] = A;
        public static void Reconf(string Dependencies = "")
        {
            lock (Dics)
            {
                if (Dependencies == "")
                {
                    Reconf_Version = DateTime.Now;
                    Reload_Dics();
                    foreach (var A in Clearables) A.Value();
                }
                else
                {
                    if (Clearables.TryGetValue(Dependencies, out Action A)) A();
                }
            }
        }
        public static void Restart_Web_Server(bool Hard_Reboot = false)
        {
            if (Hard_Reboot) JR.OS.Kill_Program();
            JR.Files.Force_Date("web.config", DateTime.UtcNow);
        }

        static Dictionary<string, string> Dic_Routes = new Dictionary<string, string>();
        protected static void Declare_Route_Translation(string Lang, string Route, string New_Route)
        {
            Route = Route.Scan().Next("/?");
            New_Route = New_Route.Scan().Next("/?");
            var Key = Lang + Route;
            Dic_Routes[Key] = New_Route;
            Dic_Routes[New_Route] = Route;
        }
        public static string Translate_Route(string Lang, string Route)
        {
            if (Dic_Routes.Count == 0) return Route;
            var C = Route.Scan();
            var Key = Lang + C.Next("/?");
            var Sep = C.Separator;
            string New_Page;
            if (!Dic_Routes.TryGetValue(Key, out New_Page)) return Route;
            var Args = C.Tail();
            if (Args.Length > 0) return New_Page + Sep + Args;
            return New_Page;
        }

        public static Ref_Parameter Can_URL = new Ref_Parameter("Canonical_URL");
        public static string Canonical_URL => Can_URL.Value;

        public static string Get_Official_Verb (string URL_Path)
        {
            var P = URL_Path.ToLower().Split('/');
            if (P.Length < 2) return null;
            if (P.Length == 2) return "";
            if (!Dic_Routes.TryGetValue(P[2], out string R)) return P[2];
            return R;
        }

        public static string URL(string Lang, string Page, bool Canonical = false)
        {
            var SB = new StringBuilder();
            if (Canonical) SB.Append(Canonical_URL);
            SB.Append("/");
            SB.Append(Lang.ToLower());
            if (Page.Length > 1)    // permet de gérer "" ou "~"
            {
                SB.Append("/");
                SB.Append(Translate_Route(Lang, Page));
            }
            return SB.ToString().ToLower();
        }

        public static string Local_Ui(string Key, string Language = null, string Default_Value = null)
        {
            if (Default_Value == null)
            {
                return Main.Get_String(Language, "UI", Key);
            }
            if (!Main.Try_Get_String(out string Val, Language, "UI", Key)) return Default_Value;
            return Val;
        }

    }
}
