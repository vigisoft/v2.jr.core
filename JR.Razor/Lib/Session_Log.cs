using System;
using System.Collections.Generic;
using JR.DB;
using JR.Saf;
using Microsoft.AspNetCore.Http;

namespace JR.Web
{
    /// <summary>
    /// Classe de gestion du loggin de visite de session
    /// </summary>

    public class Session_Log
    {

        // Active le logging des pages et sessions
        public static bool Log_Sessions = false;
        public static bool Log_Countries = false;


        Dictionary<string, string> Pages = new Dictionary<string, string>();
        public Session_State WS;

        long Id_Session;
        public void Log_Page(HttpRequest R, string Params = "")
        {
            bool Is_Robot = false;
            // Si premi�re page logger l'ouverture de session
            if (Pages.Count == 0)
            {
                string Agent = R.Headers["User-agent"].ToString().ToLower();
                if (Agent.Contains("bot") || Agent.Contains("spid") || Agent.Contains("crawl"))
                {
                    Is_Robot = true;
                    return;
                }
                string IP = WS.Ip ?? "";
                string Language = "";
                var Languages = R.Headers["Accept-Language"];
                foreach (var L in Languages)
                {
                    foreach (string S in L.Split(','))
                    {
                        Language = S; break;
                    }
                }
                string URL = R.Scheme + "://" + R.Host + R.Path;
                Id_Session = (long)App_Db.Insert("insert into LOG_Session (date,ip,language,agent,URL) values (getdate(),@0,@1,@2,@3)", IP, Language, Agent, URL);
                if (Log_Countries)
                {
                    JR.GEO.Session_Geo.Update_Geo_Infos(IP, Id_Session);
                }
            }
            if (Is_Robot) return;
            string State = WS.User.Connected ? "C" : "";
            string Page = R.Path + R.QueryString.ToString();
            if (Pages.ContainsKey(Page + State)) return;
            Pages[Page + State] = Page;
            App_Db.Execute("insert into Log_Page (ID_Session, Page, State, Params) values (@0,@1,@2,@3)", Id_Session, Page, State, Params);
        }

        public static void Init()
        {
            Log_Sessions = Main.Bool_Parameter("SESSIONS.LOG");
            Log_Countries = Main.Bool_Parameter("SESSIONS.LOG_COUNTRIES");
            if (Log_Countries) JR.GEO.Session_Geo.Update_Geo_Db();
            var Keep = Main.Int_Parameter("SESSIONS.KEEP_MONTHS");
            if (Keep > 0)
            {
                var Date_Min = DateTime.Now.Date.AddMonths(-Keep);
                App_Db.Execute("delete from Log_Session where date < @0", Date_Min);
            }
        }
    }
}
