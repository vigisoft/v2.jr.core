﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;
using JR;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.RazorPages;
using JR.Saf;
using Microsoft.Extensions.Primitives;
using Microsoft.AspNetCore.Html;

namespace JR.Web
{
    public class View_State : Page
    {

        static long Id_Gen = 0;
        public string Uid;
        public string Name;

        public void Trace(string Mess) => Debug.WriteLine(Uid + " : " + Mess);

        public void Navigate_To(string Page)
        {
            Layout = null;
            Response.Redirect(URL(Page));
        }
        public bool Init(string Name, bool Condition = true, Model_State Model = null)
        {
            if (!Condition)
            {
                Navigate_To("~");
                return false;
            }
            this.Name = Name;
            if (Uid != null)
            {
                Trace("!!! Already initialized!!!");
                return true;
            }
            Id_Gen++;
            Uid = $"V_{this.GetType().Name}@{Id_Gen}";
            LG = JR.Saf.Lang.By_Key(RouteData.Values["LG"].ToString());
            WS.Force_Language(LNG);
            Title = Def_Ui(".$TITLE", Default_Value: "?");
            Description = Def_Ui(".$DESC", Default_Value: "?");
            if (Session_Log.Log_Sessions)
            {
                WS.Sess_Log?.Log_Page(this.Request);
            }
            if (Model != null) Model.V = this;
            return true;
        }

        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
        }

        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute] public Session_State WS { get; set; }

        public User_State WU => WS.User;

        string Focus = null;
        public void Focus_On(string Element) => Focus = Element;

        public string Get_Cookie(string Name, bool User_Level = false) => WS.Get_Cookie(Name, User_Level);

        public string STAMP => Main.Stamp;

        public JR.Saf.Lang LG { get; set; } = JR.Saf.Lang.FR;
        public string LNG => LG.Key;
        public bool French => LG == JR.Saf.Lang.FR;

        public HtmlString Raw(string Html) => new HtmlString(Html);
        public HtmlString AutoRaw(string Html) => new HtmlString(Html.To_Html_Auto());

        public string URL(string Page, bool Canonical = false) => Application_State.URL(LNG, Page, Canonical);

        public bool Noindex = false;

        string Expanded_Key(string Key)
        {
            if (Key.Length < 2) return Key;
            switch (Key[0])
            {
                case '.': return string.Concat(Name, Key);
            }
            return Key;
        }

        public string Def_Ui(string Key, string Default_Value = "") => Application_State.Local_Ui(Expanded_Key(Key), LG.Key, Default_Value);

        public string Def_NUi(string Key, string Default_Value = "") => Application_State.Local_Ui(Expanded_Key(Key), null, Default_Value);

        public string Ui(string Key, params object[] Params)
        {
            if (Params.Length > 0)
            {
                return string.Format(Application_State.Local_Ui(Expanded_Key(Key), LG.Key), Params);
            }
            return Application_State.Local_Ui(Expanded_Key(Key), LG.Key);
        }

        public string NUi(string Key, params object[] Params)
        {
            if (Params.Length > 0)
            {
                return string.Format(Application_State.Local_Ui(Expanded_Key(Key)), Params);
            }
            return Application_State.Local_Ui(Expanded_Key(Key));
        }

        public int QY_Count => Request.Query.Count;
        public string QY(string Key, string Default = "")
        {
            if (Request.Query.TryGetValue(Key, out StringValues V)) return V.ToString();
            if (RouteData.Values.TryGetValue(Key, out object O)) return O.ToString();
            return Default;
        }

        public string Title = "";
        public string Description = "";

        public void Set_Title(params string[] Titles)
        {
            var T = Titles.Where(T => !string.IsNullOrEmpty(T));
            if (!T.Any()) return;
            Title = T.Assembled_With(" - ").Replace("...", ".").Replace("/", "-");
        }

        public static View_State From(string Lng, Controller_State Ctrl)
        {
            var V = Application_State.New_View();
            V.WS = Ctrl.WS;
            V.LG = Lang.By_Key(Lng);
            return V;

        }

        public HtmlString Pop_Script()
        {
            if (Focus != null)
            {
                Script.Call("Set_Focus_On", Focus);
            }
            return new HtmlString(Script.Code(true));
        }

        public Script_Builder Script = new Script_Builder();

        public virtual string Lang_Verb(string Verb, string Lang) => Verb;

        Dictionary<Lang, string> Forced_Canon = new();

        public bool Is_Robot(bool Default = true)
        {
            try
            {
                var UA = Request.Headers["User-Agent"].ToString();
                if (UA.Length < 10) return true;
                UA = UA.ToLower();
                if (UA.Contains("bot")) return true;
                if (UA.Contains("spid")) return true;
                if (UA.Contains("craw")) return true;
                if (UA.Contains("track")) return true;
                return false;
            }
            catch
            {
                return Default;
            }
        }

        public void Set_Canonical(Lang L, string Url) => Forced_Canon[L] = Url;

        public void Single_Canonical()
        {
            foreach (var L in Lang.List)
            {
                if (L == LG) continue;
                Forced_Canon.Add(L, null);
            }
        }

        public Dictionary<Lang, string> Canonicals(out int Count, string LNGS = null)
        {
            var Canons = new Dictionary<Lang, string>();
            var L_Count = 0;
            var Verb = Application_State.Get_Official_Verb(Request.Path.ToString());
            var Args = Request.QueryString.ToString();
            if (Verb != null)
            {
                void Add(Lang L)
                {
                    string Canon = null;
                    if (!Forced_Canon.TryGetValue(L, out Canon))
                    {
                        Canon = Application_State.URL(L.Key, Lang_Verb(Verb, L.Key), true) + Args;
                    }
                    if (Canon == null) return;
                    if (!Canons.ContainsValue(Canon)) L_Count++;
                    Canons[L] = Canon;
                }
                if (LNGS == null)
                {
                    foreach (var L in Lang.List) Add(L);
                }
                else
                {
                    foreach (var L in LNGS.Split(',')) Add(Lang.By_Key(L));
                }
            }
            Count = L_Count;
            return Canons;
        }

        public static View_State From_Model (dynamic Model) => Model as View_State ?? (Model as Model_State).V;
    }

}
