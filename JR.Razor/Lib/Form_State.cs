﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;
using JR;
using System.Reflection;
using System.Dynamic;
using Microsoft.AspNetCore.Mvc.RazorPages;
using JR.Saf;
using Microsoft.Extensions.Primitives;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace JR.Web
{
    public class Form_State : PageModel
    {

        protected string Get_Param(string Key, string Default = "", bool Trim = true)
        {
            if (!HttpContext.Request.Form.TryGetValue(Key, out StringValues V)) return Default;
            if (Trim) return V[0].Trim(); else return V[0];
        }

        protected bool Checked_Param(string Key) => HttpContext.Request.Form.ContainsKey(Key);

        protected Script_Builder Script = new();
        protected IActionResult Script_Response => Content(Script.ToString());

        protected Session_State WS;

        protected void Navigate_To(string URL) => Script.Navigate_To(URL);

        protected void Exec(string Code) => Script.Exec(Code);

        protected void Set_Visible(string C, bool Visible) => Script.Set_Visible(C, Visible);

        protected void Set_Enabled(string C, bool Enabled) => Script.Set_Enabled(C, Enabled);
        protected void Set_Text(string C, string Text) => Script.Set_Text(C, Text);
        protected void Set_Value(string C, string Text) => Script.Set_Value(C, Text);

        protected void Reload() => Script.Reload();

        public string Error_Class = "input-error";
        public bool Error => Error_Fields.Count > 0;

        readonly HashSet<string> Error_Fields = new();
        readonly Script_Builder Err_SB = new();

        void Show_Error(string C, bool In_Error)
        {
            Err_SB.Set_Class(C, Error_Class, In_Error);
            if (In_Error) Error_Fields.Add(C);
        }

        void Show_Text(string E, string Text) => Err_SB.Set_Text(E, Text);


        protected bool Set_Error(string E, string C, string Key, bool In_Error) => Set_Error(E, C, In_Error ? Ui(Key) : "");

        protected bool Set_Error(string E, string C, string Message = "")
        {
            bool In_Error = Message.Length > 0;
            if (Error_Fields.Contains(C)) return In_Error;
            Show_Error(C, In_Error);
            Show_Text(E, Message);
            return In_Error;
        }

        protected bool Set_Error(string C, bool In_Error)
        {
            if (Error_Fields.Contains(C)) return In_Error;
            Show_Error(C, In_Error);
            return In_Error;
        }

        protected bool Show_Errors()
        {
            Script.Exec(Err_SB.ToString());
            return Error;
        }
        public JR.Saf.Lang LG { get; set; } = JR.Saf.Lang.FR;
        public string LNG => LG.Key;

        public string Ui(string Key, params object[] Params)
        {
            if (Params.Length > 0)
            {
                return string.Format(Application_State.Local_Ui(Key, LG.Key), Params);
            }
            return Application_State.Local_Ui(Key, LG.Key);
        }
        public string URL(string Page, bool Canonical = false) => Application_State.URL(LNG, Page, Canonical);

        protected void Load()
        {
            WS = Session_State.From_Context(HttpContext);
            LG = JR.Saf.Lang.By_Key(RouteData.Values["LG"].ToString());
        }

        // Balise form :
        // <form id="login_form" method="post" data-ajax="true" data-ajax-method="post" data-ajax-url="@Login_Form.Post_Url(V)" data-ajax-complete="Eval_Xhr">

        // Utilisation sans contexte
        // Appeler Load() au début du OnPost :
        // Adapter Post_URL
        //    public static string Post_Url(Web_View V) => Post_Url(V, "login");


        // Utilisation avec contexte
        // Coder un type Ctx dérivé de Context et son chargement
        //  class Ctx : Context
        //  {
        //        public string V_Name;
        //        public override void Load(Web_View V)
        //        {
        //            V_Name = V.Name;
        //        }
        //  }
        //
        // Appeler Load <Context> () au début du OnPost :
        // if (!Load(out Ctx C)) return new NoContentResult();
        //
        // Adapter Post_URL
        //    public static string Post_Url(Web_View V) => Post_Url<Ctx>(V, "login");

        public class Context
        {
            public int Id;
            public virtual void Load(View_State V) { }
        }

        protected bool Load<T>(out T Ctx) where T : Context
        {
            Load();
            var Id = RouteData.Values["ID"].ToString().To_Integer();
            if (Id > 0 && Id <= WS.Context_Pool.Count)
            {
                Ctx = WS.Context_Pool[Id - 1] as T;
                return true;
            }
            Ctx = null;
            return false;
        }

        protected static string Post_Url<T>(View_State V, string Form_Name) where T : Context, new()
        {
            var C = new T();
            C.Load(V);
            lock (V.WS.Context_Pool)
            {
                V.WS.Context_Pool.Add(C);
                C.Id = V.WS.Context_Pool.Count;
            }
            return $"/form/l{V.LNG}/id{C.Id}/{Form_Name}".ToLower();
        }

        protected static string Post_Url(View_State V, string Form_Name)
        {
            return $"/form/l{V.LNG}/id0/{Form_Name}".ToLower();
        }

        protected bool Set_Spam_Error(string C_Err, string Key)
        {
            if (Set_Error(C_Err, C_Err, WS.Next_Send > DateTime.UtcNow ? string.Format(WS.Local_Ui(Key), 5) : ""))
            {
                WS.Next_Send = DateTime.UtcNow.AddSeconds(5);
                return true;
            }
            return false;
        }

        protected void Protect_Send()
        {
            WS.Next_Send = DateTime.UtcNow.AddSeconds(5);
        }


    }

}
