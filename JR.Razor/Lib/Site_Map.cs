using System;
using System.Collections.Generic;
using System.Data;
using JR;
using System.Xml;
using System.Xml.Linq;
using System.Text;

namespace JR.Web
{

    /// <summary>
    /// Classe de gestion du projet principal
    //        <?xml version="1.0" encoding="UTF-8"?>

    //        <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    //   <url>

    //      <loc>http://www.example.com/</loc>

    //      <lastmod>2005-01-01</lastmod>

    //      <changefreq>monthly</changefreq>

    //      <priority>0.8</priority>

    //   </url>

    //</urlset> 
    /// </summary>
    public class Site_Map
    {

        StringBuilder S = new StringBuilder();

        XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
        XNamespace im = "http://www.google.com/schemas/sitemap-image/1.1";
        XElement Map;
        public Site_Map()
        {
            S.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            S.AppendLine("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\">");
        }

        public class Site_Image
        {
            public string Loc;
            public string Caption = "";
            public string Legend = "";
            public string License = "";
        }

        public string Root_Url = "";
        public void Add_Page(string Loc, string LastMod, string Priority, string ChangeFreq, params Site_Image[] Images)
        {
            S.AppendLine("<url>");
            S.AppendLine($"<loc>{Root_Url + Loc.To_Html()}</loc>");
            if (LastMod != "") S.AppendLine($"<lastmod>{LastMod}</lastmod>");
            if (Priority != "") S.AppendLine($"<priority>{Priority}</priority>");
            if (ChangeFreq != "") S.AppendLine($"<changefreq>{ChangeFreq}</changefreq>");
            foreach (var I in Images)
            {
                S.AppendLine("<image:image>");
                S.AppendLine($"<image:loc>{I.Loc}</image:loc>");
                if (I.Caption != "") S.AppendLine($"<image:caption>{I.Caption.To_Html()}</image:caption>");
                if (I.Legend != "") S.AppendLine($"<image:title>{I.Legend.To_Html()}</image:title>");
                if (I.License != "") S.AppendLine($"<image:license>{I.License}</image:license>");
                S.AppendLine("</image:image>");
            }
            S.AppendLine("</url>");
        }

        public void Save(string File_Name)
        {
            S.AppendLine("</urlset>");
            JR.Files.Safe_Write(File_Name, S.ToString(), Encoding.UTF8);
        }
    }


    /// <summary>
    /// Classe de gestion du projet principal
    //<?xml version="1.0" encoding="UTF-8"?>

    //<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    //   <sitemap>

    //      <loc>http://www.example.com/sitemap1.xml.gz</loc>

    //      <lastmod>2004-10-01T18:23:17+00:00</lastmod>

    //   </sitemap>

    //   <sitemap>

    //      <loc>http://www.example.com/sitemap2.xml.gz</loc>

    //      <lastmod>2005-01-01</lastmod>

    //   </sitemap>

    //</sitemapindex>
    /// </summary>
    public class Site_Map_Index
    {

        XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
        XElement Index;
        public Site_Map_Index()
        {
            Index = new XElement(ns + "sitemapindex", new XAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9"));
        }

        public string Root_Url = "";
        public void Add_Map(string Loc, string LastMod)
        {
            string Full_Url = Root_Url + Loc;
            XElement Url = new XElement(ns + "sitemap", new XElement(ns + "loc", Full_Url));
            if (LastMod != "") Url.Add(new XElement(ns + "lastmod", LastMod));
            Index.Add(Url);
        }

        public void Save(string File_Name)
        {
            JR.Files.Check_Directory(File_Name);
            Index.Save(File_Name);
        }
    }







}
