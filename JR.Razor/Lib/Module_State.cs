﻿using JR;
using Microsoft.AspNetCore.Html;

namespace JR.Web
{
    public class Module_State : Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {

        public View_State V => View_State.From_Model(this.Model);

        public Session_State WS => V?.WS;
        public User_State WU => V?.WS.User;


        public string URL(string Page) => V.URL(Page);
        public HtmlString Raw(string Html) => new HtmlString(Html);
        public HtmlString AutoRaw(string Html) => new HtmlString(Html.To_Html_Auto());

        public JR.Saf.Lang LG => V.LG;
        public string LNG => LG.Key;
        public string lng => LG.Key.ToLower();
        public bool French => LNG == "FR";

        public string Def_Ui(string Key, string Default_Value = "") => V.Def_Ui(Key, Default_Value);

        public string Def_NUi(string Key, string Default_Value = "") => V.Def_NUi(Key, Default_Value);

        public string Ui(string Key, params object[] Params) => V.Ui(Key, Params);

        public string NUi(string Key, params object[] Params) => V.NUi(Key, Params);

        public string QY(string Key, string Default = "") => V.QY(Key, Default);

        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
        }

        public void Focus_On(string Element) => V.Focus_On(Element);

    }
}
