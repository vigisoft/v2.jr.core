﻿using JR.Saf;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace JR.Web
{
    public class Session_State
    {
        public string Ip = "";

        public virtual void On_Clear()
        {
        }

        // Nettoie les ressources du portail avant fermeture
        public void Clear() => On_Clear();

        User_State _User = null;	// Utilisateur courant
        public User_State User
        {
            get
            {
                if (_User == null)
                {
                    _User = Application_State.New_User(this);
                }
                return _User;
            }
        }

        public virtual void After_Change_User()
        { }

        public void Change_User()
        {
            Major_Update();
            After_Change_User();
        }

        // Ressources de la session, accessible sous forme d'un indexer
        Dictionary<string, object> _Depot = new Dictionary<string, object>();
        public object this[string Index]
        {
            get
            {
                object Value = null;
                _Depot.TryGetValue(Index, out Value);
                return Value;
            }

            set
            {
                if (value == null)
                {
                    _Depot.Remove(Index);
                    return;
                }
                _Depot[Index] = value;
            }
        }

        public int User_Level { get { return User == null ? 0 : User.Level; } }


        public string Language { get; private set; } = "FR";
        public string Browser_Language { get; set; }
        public bool Explicit_Language { get; set; }

        public bool French => Language == "FR";

        public virtual void Force_Language(string Langue)
        {
            Language = Langue;
        }


        //string Get_Browser_Language
        //{
        //    get
        //    {
        //        var UL = HttpContext.Current.Request.UserLanguages;
        //        if (UL != null)
        //        {
        //            foreach (string L in UL)
        //            {
        //                foreach (string LL in Main.Language_List)
        //                {
        //                    if (L.ToUpper().Contains(LL)) return LL;
        //                }
        //            }
        //        }
        //        return Language == null ? "FR" : Language;
        //    }
        //}

        public string Default_Language
        {
            get
            {
                return Main.Language_List[0];
            }
        }


        // Retourne une chaine UI localisée
        public string Local_Ui(string Key, bool Neutral = false, string Default_Value = null)
        {
            if (Default_Value == null)
            {
                return Neutral ? Main.Get_String("UI", Key) : Main.Get_String(Language, "UI", Key);
            }
            string Val;
            if (Neutral)
            {
                if (!Main.Try_Get_String(out Val, "UI", Key)) return Default_Value;
            }
            else
            {
                if (!Main.Try_Get_String(out Val, Language, "UI", Key)) return Default_Value;

            }
            return Val;
        }

        // Retourne une chaine UI localisée et compatible Javascript
        //public string Local_Js_Ui(string Key)
        //{
        //    return Main.Js(Main.Get_String(Language, "UI", Key));
        //}

        // Indique à la prochaine vue si elle s'appelle FROM
        // d'aller sur To
        //string _Route = "";
        //public string Route
        //{
        //    get
        //    {
        //        return _Route;
        //    }
        //    set { _Route = value; }
        //}

        //public void Clear_Route()
        //{
        //    _Route = "";
        //}

        // Dernier fichier à télécharger
        public class Download_Info
        {
            public string Physical_File_Name { get; set; }
            public string Client_File_Name { get; set; }
            public string Mime_Type { get; set; }
            public bool Auto_Delete { get; set; }
            public string Encode
            {
                get
                {
                    return HttpUtility.UrlEncode(string.Format("{0}~{1}~{2}~{3}", Physical_File_Name, Client_File_Name, Mime_Type, Auto_Delete));
                }
            }
            public static Download_Info Decode(string Param)
            {
                string[] Chars = JR.Strings.Split(HttpUtility.UrlDecode(Param));
                if (Chars.Length < 4) return null;
                Download_Info Dl = new Download_Info();
                Dl.Physical_File_Name = Chars[0];
                Dl.Client_File_Name = Chars[1];
                Dl.Mime_Type = Chars[2];
                Dl.Auto_Delete = JR.Convert.To_Boolean(Chars[3]);
                return Dl;
            }
        }

        //Download_Info _File_Download = null;

        //public Download_Info Get_Requested_Download()
        //{
        //    Download_Info Info = _File_Download;
        //    //_File_Download = null;
        //    return Info;
        //}

        //public static Download_Info Get_Requested_Download(HttpRequest Request)
        //{
        //    string File = Request.Query["File"];
        //    if (File == null) return null;
        //    return Download_Info.Decode(File);
        //}

        // 2 compteurs pour identifier les changements d'état dans la session.
        // le premier est majeur => provoque un rechargement de toutes les pages de la session
        // le deuxieme est mineur => provoque une mise à jour des données
        static long Major_Gen = Stopwatch.GetTimestamp();
        public static object Lock = new object();

        static long New_Major ()
        {
            lock (Lock)
            {
                Major_Gen++;
                return Major_Gen;
            }
        }

        long Major_Tick = New_Major();
        long Minor_Tick = 0;

        public void Major_Update()
        {
            Major_Tick = New_Major();
            Minor_Tick = 0;
        }
        public void Minor_Update() => Minor_Tick++;

        public virtual string Get_Ticks() => "";
        public string Ticks_Cookie => $"{Major_Tick}.{Minor_Tick}.{Get_Ticks()}";


        string Get_PUI_Key(string Name, bool User_Level)
        {
            if (User_Level) return $"PUI.{User.Uid}.{Name}";
            return $"PUI.{Name}";
        }

        // Cookie cache
        protected Dictionary<string, string> Cookies = new Dictionary<string, string>();
        List<(string Key, string Value, int Days)> Pending_Cookies = new List<(string Key, string Value, int Days)>();


        void Treat_Pending_Cookies(Action<string, string, int> Add, Action<string> Remove)
        {
            lock (Pending_Cookies)
            {
                foreach (var C in Pending_Cookies)
                {
                    if (C.Value == null)
                    {
                        Remove(C.Key);
                    }
                    else
                    {
                        Add(C.Key, C.Value, C.Days);
                    }
                }
                Pending_Cookies.Clear();
            }
        }

        public string Remove_Cookie(string Name, bool User_Level)
        {
            var Key = Get_PUI_Key(Name, User_Level);
            if (Cookies.Remove(Key))
            {
                lock (Pending_Cookies)
                {
                    Pending_Cookies.Add((Key: Key, Value: null, Days: 0));
                }
            }
            return Key;
            //// La seule manière d'effacer un cookie est de mettre son expiration dans le passé
            //HttpContext.Current.Response.Cookies.Set(new HttpCookie(Key, null) { Expires = DateTime.Now.AddYears(-10) });

        }

        public string Persist_Cookie(string Name, string Value, bool User_Level, int Days = 30000)
        {
            var Key = Get_PUI_Key(Name, User_Level);
            Cookies[Key] = Value;
            lock (Pending_Cookies)
            {
                Pending_Cookies.Add((Key: Key, Value: Value, Days: Days));
            }
            return Key;
            //HttpContext.Current.Response.Cookies.Set(new HttpCookie(Key, Value) { Expires = DateTime.Now.AddDays(Days) });
        }

        public string Get_Cookie(string Name, bool User_Level)
        {
            string Key = Get_PUI_Key(Name, User_Level);
            if (Cookies.TryGetValue(Key, out string Value)) return Value;
            return null;
        }

        private static Task Set_Cookies(object context)
        {
            var Context = context as HttpContext;
            var WS = Context?.Items["SESSION"] as Session_State;
            if (WS == null) return Task.CompletedTask;
            var Cook = Context.Response.Cookies;
            WS.Treat_Pending_Cookies((Key, Value, Days) => Cook.Append(Key, Value, new CookieOptions { Expires = DateTime.Now.AddDays(Days) }),
                (Key) => Cook.Delete(Key));
            return Task.CompletedTask;
        }

        DateTime Expiration;
        public bool Is_Expired(DateTime Now)
        {
            if (Now < Expiration) return false;
            On_Remove();
            return true;
        }

        public void On_Remove()
        {
        }



        static Dictionary<string, Session_State> Dic = new Dictionary<string, Session_State>();

        public Session_Log Sess_Log = null;
        public static Session_State From_Context(HttpContext Context)
        {
            var S = Context.Session;
            bool Purge_Sessions = false;
            var Now = DateTime.UtcNow;

            // si la session est vide, il faut créer au moins une valeur pour qu'elle soit persistée
            if (!S.TryGetValue("SESSION", out byte[] B))
            {
                S.Set("SESSION", new byte[] { });
                Purge_Sessions = true;
            }

            // Supprimer les sessions expirées
            if (Purge_Sessions)
            {
                var Purge_List = new List<string>();
                lock (Dic)
                {
                    foreach (var DS in Dic)
                    {
                        if (DS.Value.Is_Expired(Now)) Purge_List.Add(DS.Key);
                    }
                    foreach (var P in Purge_List) Dic.Remove(P);
                }
            }

            if (Context.Request.Query.TryGetValue("restoresessionid", out Microsoft.Extensions.Primitives.StringValues V))
            {
                var Old_S_Id = V.FirstOrDefault();
                if (Dic.TryGetValue(Old_S_Id, out Session_State Old_WS) && !Old_WS.Is_Expired(Now))
                {
                    Dic[S.Id] = Old_WS;
                }
            }
            // Si pas de session pour l'id ou session expirée créer une nouvelle session
            if (!Dic.TryGetValue(S.Id, out Session_State WS) || WS.Is_Expired(Now))
            {
                WS = Application_State.New_Session();
                lock (Dic)
                {
                    Dic[S.Id] = WS;
                }
                // Charger l'adresse IP
                WS.Ip = Context.Connection.RemoteIpAddress.ToString();
                if (Session_Log.Log_Sessions) WS.Sess_Log = new Session_Log { WS = WS };
                // Mettre en cache les cookies
                foreach (var Cook in Context.Request.Cookies)
                {
                    if (Cook.Key.StartsWith("PUI"))
                    {
                        WS.Cookies.Add(Cook.Key, Cook.Value);
                    }
                }
                WS.User.Autolog();
            }
            WS.Expiration = Now.AddMinutes(60);
            Context.Items["SESSION"] = WS;
            Context.Response.OnStarting(Set_Cookies, Context);
            return WS;
        }

        // Liste de contextes d'exécution (par exemple pour les Web_Forms) associés à la session
        public List<object> Context_Pool = new List<object>();

        public DateTime Next_Send = DateTime.MinValue;


        public Dictionary<Guid, Tuple<DateTime, object>> View_Contexts = new Dictionary<Guid, Tuple<DateTime, object>>();
        public void Push_View_Context(Guid Gid, object Context)
        {
            var UtcNow = DateTime.UtcNow;
            lock (View_Contexts)
            {
                var Oldies = new List<Guid>();
                foreach (var V in View_Contexts)
                {
                    if (V.Value.Item1 < UtcNow) Oldies.Add(V.Key);
                }
                foreach (var O in Oldies) View_Contexts.Remove(O);
            }
            View_Contexts[Gid] = new Tuple<DateTime, object>(UtcNow.AddMinutes(5), Context);
        }

        public object Get_View_Context(Guid Gid)
        {
            lock (View_Contexts)
            {
                if (View_Contexts.TryGetValue(Gid, out Tuple<DateTime, object> Context)) return Context.Item2;
            }
            return null;
        }
        public MarkupString Physical_Content(string Name)
        {
            return (MarkupString)JR.Files.Safe_Read_All(JR.Saf.Main.Real_File_Name(Name), Encoding.Default); // E_Iso);
        }


    }
}
