﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JR.Web
{
    public class User_State
    {
        public User_State(Session_State Session)
        {
            this.Session = Session;
        }

        public Session_State Session { get; } = null;
        public bool Connected { get; set; } = false;
        public int Level { get; set; } = 0;

        public void Connect(string Name, string Uid, int Level = 1)
        {
            this.Name = Name;
            this.Uid = Uid;
            Connected = true;
            this.Level = Level;
            Session.Change_User();
            On_Connect();
        }

        public virtual void On_Connect()
        {
        }

        public virtual void On_Disconnect()
        {
        }

        public virtual void Autolog() { }

        public void Disconnect()
        {
            Name = "?";
            Uid = "";
            Connected = false;
            Level = 0;
            On_Disconnect();
            Session.Change_User();
        }
        public string Name { get; private set; } = "?";
        public string Uid { get; private set; } = "";

    }
}
